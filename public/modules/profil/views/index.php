{_layHeader}
	<div id="legalitas">
		<div class="container-fluid">
			<div class="row">
				<div id="imglegalitas" class="profil">
					<center>
						<h1 class="bold">PROFIL GaneshaBisa</h1>
						<br/>
						<p style="width:50%">
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
						</p>
					</center>
				</div>
							
				<div class="konten">
					<div class="row col-sm-6">
						<p>
							<h4>Makin Terasa Manfaatnya.</h4>

							Beramal lebih berhubungan dengan cara pandang manusia terhadap kehidupan. Sekecil apapun yang kita berikan untuk kepentingan kemanusiaan, kontribusi itu pasti tetap memiliki makna yang besar. Bahkan, kalaupun tidak memiliki kekayaan materi, kita bisa menjadi penyantun lewat sumbangan pemikiran dan tenaga.

							Alangkah lebih ramah dan bermaknanya wajah dunia ini jika kita semakin tergerak menjadi penyantun. Jika gerakan seperti ini kian meluas, niscaya kemiskinan, ketertinggalan dan kebodohan di Bumi Pertiwi ini tidak terus menjadi-jadi.

							Di tengah pergulatan melawan kemiskinan, ketertinggalan dan kebodohan, ternyata masih ada setitik asa yang tersisa. Ada sebagian kita yang mau berbagi. Mereka yang memiliki semangat menyisihkan sebagian miliknya untuk kalangan tak berpunya.

							Didirikan 1 Maret 1987, Yayasan Dana Sosial al Falah (GaneshaBisa) telah dirasakan manfaatnya di lebih dari 25 propinsi di Indonesia. Paradigma prestasi GaneshaBisa sebagai lembaga pendayagunaan dana yang amanah dan profesional, menjadikannya sebagai lembaga pengelola zakat, infaq, dan sedekah (ZIS) terpercaya di Indonesia.

							Lebih dari 161.000 donatur dengan berbagai potensi, kompetensi, fasilitas, dan otoritas dari kalangan birokrasi, profesional, swasta, dan masyarakat umum telah terajut bersama GaneshaBisa membentuk komunitas peduli dhuafa. Mereka, dengan segala kemampuan terbaiknya, telah memberikan kontribusi, cinta, dan kepedulian dalam membangun negeri ini.
						</p>
					</div>
					<div class="row col-sm-6">
						<p style="padding-left:10px">
							GaneshaBisa yang dikukuhkan menjadi Lembaga Amil Zakat Nasional oleh Menteri Agama Republik Indonesia dengan SK No.523 tanggal 10 Desember 2001 menjadi entitas yang menaruh perhatian mendalam pada kemanusiaan yang universal. Melalui Divisi Penyaluran GaneshaBisa semakin meneguhkan pendayagunaan dana Anda secara syar’i, efisien, efektif & produktif.

							Sebagai lembaga pengelola dana ZIS yang makin terasa manfaatnya, Insya Allah GaneshaBisa akan menjadi mitra terpercaya Anda
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
{_layFooter}