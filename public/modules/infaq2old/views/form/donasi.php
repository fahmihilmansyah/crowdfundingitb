<div class="col-md-3">
    &nbsp;
</div>
<div class="col-md-6">
    <center>
        <h4 class="sm-bold">Masukan Minimal Donasi</h4>
        <p>Donasi minimal Rp. 20.000 serta kelipatan ribuan ( misal 50.000, 82.000, 105.000, dst. )</p>

        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart('', array('id' => 'fmdonasi'), $hidden_form);
        ?>

        <div>
            <?php
            $name = 'donationNominal';
            $value = empty($this->session->userdata('prevnomDonation')) ? "" :
                $this->session->userdata('prevnomDonation');

            $extra = 'id="donationNominal" 
						  class="form-control don-input" placeholder="Nominal Donasi"';

            echo form_input($name, $value, $extra);
            ?>
        </div>
        <div>
            <label class="agreement">
                <div>
                    <?php
                    $data = array(
                        'name' => 'agreement',
                        'id' => 'agreement',
                        'value' => 'accept',
                        'checked' => FALSE,
                    );

                    echo form_checkbox($data);
                    ?>
                    Donasi Sebagai Anonim
                </div>
            </label>
        </div>
        <br clear="all"/>
        <div>
            <?php
            $name = 'donationCommment';
            $value = empty($this->session->userdata('prevcomDonation')) ? "" :
                $this->session->userdata('prevcomDonation');
            $extra = 'id="donationCommment" 
						  class="form-control mini-height" 
						  placeholder="Tuliskan Komentar"';

            echo form_textarea($name, $value, $extra);
            ?>
        </div>

        <div>
            <input type="hidden" name="otherJenis" value="infaq">
            <input type="submit" name="subnominal" class="btn btn-success" style="width:100%; margin-top:10px"
                   value="Lanjut">
        </div>
    </center>
    <?php
    echo form_close();
    ?>
</div>
<div class="col-md-3">
    &nbsp;
</div>