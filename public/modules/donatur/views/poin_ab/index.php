<div class="mydonation">

	<div class="mydonation-head">

		<div class="row col-md-12">

			<h2 class="bold">POIN AKSI BAIK</h2>

		</div>

	</div>

	<div class="mydonation-body">

		<div>

			<table class="mydonation-table">

				<tr>

					<td><img src="<?php URI::baseURL(); ?>assets/images/donatur/ipoin.png"></td>

					<td>

						<h2 class="bold" style="font-size: 15%; font-size: 3vw"><?php $total = (int)$point;

                            echo number_format($total,0,",",".");?> pt</h2>

						<span class="sm-bold">TOTAL POIN</span>

					</td>

				</tr>

			</table>

		</div>

		<hr/>

		<br clear="all"/>

		<div>

			<p>

				Anda bisa menukarkan poin aksi baik ini dengan berbagai macam merchandise menarik di Niatbaik Store

			</p>

			<p>

				Riwayat aksi baik anda:

			</p>

			<table 

					id="mutasi_donasi" 

					class="table table-striped table-bordered dt-responsive nowrap" 

					width="100%" 

			>

			<thead>

				<tr>

					<th>Jumlah </th>

					<th>Deskripsi</th>

					<th>Waktu</th>

				</tr>

			</thead>

			<tbody>

                <?php foreach ($trx as $r):?>

				<tr class="text-center">

					<td><?php echo number_format($r->uniqcode,0,",",".") ;?></td>

                    <td><?php echo $r->nama_trx;?></td>

                    <td><?php echo $r->edtd_at;?></td>

				</tr>

                <?php endforeach;?>

			</tbody>

			</table>

		</div>

	</div>

	<!-- <div class="dompet-ab-topup-btn">

		<center><a class="bold">TUKARKAN</a></center>

	</div> -->

</div>

<br clear="all"/>

<br clear="all"/>

<br clear="all"/>