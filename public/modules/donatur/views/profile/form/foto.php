<form method="post" enctype="multipart/form-data">
<div class="profile-tab-foto">
<div class="uibutton">
    <?php 
        $img = !empty($default["img"]) ? $default["img"] :'noimage.png'; 
        $url_image = base_url() . "assets/images/articles/" . $img;
    ?>
    <div class="col-md-12 mb20">
    	<img id="uploadPreview2" src="<?php echo $url_image; ?>" class="upload w100"/>
    </div>
    <?php 
    	$value = !empty($default["img"]) ? $default["img"] : '';
    	$extra = 'class="form-control"';
    	echo form_hidden('img', $value, $extra); 
    ?>
    <div class="col-md-12">
        <div class="myuibutton">
        <p>Pilih Gambar</p>
            <?php 
            	$extra = "id='uploadImage2' onchange='PreviewImage(2);'";
            	echo form_upload('uploadedimages[]','', $extra);
            ?>
        </div>
    </div>
	<p class='label-error'></p>
</div>
</div>
<br clear="all" />
<p class="p-center40">
    Gambar yang diupload disarankan berukuran 200px x 200px.
    dan memiliki format <b>PNG</b>, <b>JPG</b>, atau <b>JPEG</b>
</p>
<br clear="all" />
<div>
    <center>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <input type="submit" name="fotosubmit" class="btn btn-success" value="SAVE">
    </center>
</div>
</form>