<form class="form-inline" method="post">
     <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">

         <label for="fname">
             Nama Lengkap
         </label>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<?php 
				$name  = 'fname';
				$value = !empty($info[0]->fname) ? $info[0]->fname : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi nama disini"';

				echo form_input($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email">
			Email
		</label>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<?php 
				$name  = 'email';
				$value = !empty($info[0]->email) ? $info[0]->email : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_input($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" >
			No Telpon
		</label>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<?php 
				$name  = 'notel';
				$value = !empty($info[0]->phone) ? $info[0]->phone : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_input($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" >
			Info
		</label>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<?php 
				$name  = 'info';
				$value = !empty($info[0]->info) ? $info[0]->info : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_textarea($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-3">
			&nbsp;
		</label>
		<div class="col-xs-12 col-sm-12 col-md-9">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
			<input type="submit" name="infosubmit" class="btn btn-success pull-right" value="SAVE">
		</div>
	</div>
</form>