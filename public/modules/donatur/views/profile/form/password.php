<form class="form-inline" method="post">
     <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-4">
			Password Lama
		</label>
		<div class="col-xs-12 col-sm-12 col-md-8">
			<?php 
				$name  = 'pwdold';
				$value = !empty($default["name"]) ? $default["name"] : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_password($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-4">
			Password Baru
		</label>
		<div class="col-xs-12 col-sm-12 col-md-8">
			<?php 
				$name  = 'pwdnew';
				$value = !empty($default["name"]) ? $default["name"] : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_password($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-4">
			Konfirmasi Passwrod
		</label>
		<div class="col-xs-12 col-sm-12 col-md-8">
			<?php 
				$name  = 'pwdrenew';
				$value = !empty($default["name"]) ? $default["name"] : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_password($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-3">
			&nbsp;
		</label>
		<div class="col-xs-12 col-sm-12 col-md-9">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
			<input type="submit" name="pwdsubmit" class="btn btn-success pull-right" value="SAVE">
		</div>
	</div>
</form>