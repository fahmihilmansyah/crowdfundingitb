<h2 class="bold">OVERVIEW</h2>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <img src="<?php URI::baseURL(); ?>assets/images/donatur/overviewdonasi.png">
            </div>
            <div class="col-md-6">
                <h3 style="color: #09A59D">Rp. <?php echo number_format($donasi[0]->jml_nominal,0,",",".")?></h3>
                <p style="color: #adadad; font-size:11px;">
                    Total donasi yang anda lakukan. <br/> Terima kasih telah membantu untuk berdonasi.
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <img src="<?php URI::baseURL(); ?>assets/images/donatur/overviewdompet.png">
            </div>
            <div class="col-md-6">
                <h3 style="color: #09A59D">Rp. <?php echo number_format($saldo['amount'],0,",",".")?></h3>
                <p style="color: #adadad; font-size:11px;">
                    Total saldo yang anda miliki di dompet.
                </p>
            </div>
        </div>
    </div>
</div>