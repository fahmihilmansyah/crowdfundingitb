<div class="mydonation">

<!--	<div class="mydonation-head">-->

		<div class="row col-md-12">

			<h3 class="bold">DONASI SAYA</h3>

		</div>

<!--	</div>-->

	<div class="mydonation-body">

		<div>

			<table class="mydonation-table">

				<tr>

					<td><img src="<?php URI::baseURL(); ?>assets/images/donatur/idonation.png"></td>

					<td>

						<h2 class="bold" style="font-size: 15%; font-size: 3vw">Rp. <?php $total = (int)empty($donation)?0:$donation;

                            echo number_format($total,0,",",".");?></h2>

						<span class="sm-bold">TOTAL DONASI</span>

					</td>

				</tr>

			</table>

		</div>

		<hr/>

		<br clear="all"/>

		<div>

			<table 

					id="mutasi_donasi" 

					class="table table-striped table-bordered dt-responsive nowrap" 

					width="100%" 

			>

			<thead>

				<tr>

					<th>Jumlah </th>

					<th>Deskripsi</th>

					<th>Waktu</th>

				</tr>

			</thead>

			<tbody><?php foreach ($trx as $r):?>

                <tr class="text-center">

                    <td><?php echo number_format($r->nomuniq,0,",",".") ;?></td>

                    <td><?php echo $r->nama_trx;?></td>

                    <td><?php echo $r->edtd_at;?></td>

                </tr>

            <?php endforeach;?>

			</tbody>

			</table>

		</div>

	</div>

</div>

<br clear="all"/>

<br clear="all"/>

<br clear="all"/>