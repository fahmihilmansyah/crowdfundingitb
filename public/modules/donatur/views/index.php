<?php
$url = implode("/",$this->uri->segment_array());
$dats = array('urllink'=>$url);
$this->session->set_userdata($dats);
?>
{_layHeader}

<link href="<?php URI::baseURL(); ?>assets/css/main.konten.css" rel="stylesheet">
<section class="grey" style="padding-top: 60px;">
<div class="container-fluid grey">

	<div class="container">

		<div class="row">

			<br clear="all"/>

			<div class="col-md-12">

				{_widget}

			</div>

			<div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {_content}
                    </div>
                </div>
			</div>

		</div>

	</div>

</div>
</section>

{_layFooter}



<script type="text/javascript">

	$(document).ready(function(){

		$("ul.profile-menu-list > li").hover(

			function()

			{

		    	$(this).find('a').addClass("active");

		    	var classname  = $(this).find('a').children('div').children("i").attr("menu");

		    	var rcondition = $(this).find('a').children('div').children("i").attr("remove");

		    	

		    	$(this).find('a').children('div').children("i").addClass(classname);

			},

			function()

			{

		        $(this).find('a').removeClass("active");

		    	var classname = $(this).find('a').children('div').children("i").attr("menu");

		    	var rcondition = $(this).find('a').children('div').children("i").attr("remove");



		    	$(this).find('a').children('div').children("i").removeClass(classname);			

		    }

		);

	});

	

	function PreviewImage(no) {

	    var oFReader = new FileReader();

	    oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);



	    oFReader.onload = function (oFREvent) {

	        document.getElementById("uploadPreview"+no).src = oFREvent.target.result;

	    };

	}



	$('#mutasi_donasi').DataTable( {

    	responsive: true

	});

</script>