<div class="card">
<div class="card-body">
<div class="row">
    <div class="col-md-4 text-center">
        <?php if(!empty($indexs[0]->img)):?>
            &nbsp;<img src="<?php echo base_url('assets/images/donatur/profile/'.$indexs[0]->img);?>" class="img-thumbnail" style="position: relative; top: -20px;height:140px;width:140px">
        <?php else: ?>
            &nbsp;<img src="<?php echo $indexs[0]->picture_url ;?>" class="img-thumbnail" style="position: relative; top: -20px">
        <?php endif; ?>
        <a href="<?php echo base_url('overview'); ?>" class="btn btn-success form-control">
            Overview
        </a>
    </div>
    <div class="col-md-8">
        <h4 style="color: #09A59D"><?php echo $indexs[0]->fname; ?></h4>
        <hr/>
        <ul class="profile-menu-list">

            <li>

                <a <?php echo $menu; ?> href="<?php echo base_url('donatur/profile')?>">

                    <div>

                        <i class="profile" menu="profile-w"></i>

                        <span>Edit Profile</span>

                    </div>

                </a>

            </li>

            <li>

                <a <?php echo $menu; ?> href="<?php echo base_url('donatur/mydonation')?>">

                    <div>

                        <i class="donasiku" menu="donasiku-w"></i><span>Donasi Saya</span>

                    </div>

                </a>

            </li>

            <li>

                <a <?php echo $menu; ?> href="<?php echo base_url('donatur/dompetku')?>">

                    <div>

                        <i class="dompet" menu="dompet-w"></i><span>Dompet Saya</span>

                    </div>

                </a>

            </li>


            <!--			<li>-->
            <!---->
            <!--				<a --><?php //echo $menu; ?><!-- href="--><?php //echo base_url('donatur/mypoin')?><!--">-->
            <!---->
            <!--					<div>-->
            <!---->
            <!--						<i class="poin" menu="poin-w"></i><span>Point Aksi Baik</span>-->
            <!---->
            <!--					</div>-->
            <!---->
            <!--				</a>-->
            <!---->
            <!--			</li>-->

        </ul>
    </div>
</div>
</div>
</div>