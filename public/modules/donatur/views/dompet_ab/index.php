<div class="dompetab">
	<div class="dompetab-head">
		<div class="row col-md-12">
			<h3 class="bold">DOMPET SAYA</h3>
		</div>
	</div>
	<div class="dompetab-body">
		<div>
			<table class="dompetab-table">
				<tr>
					<td><img src="<?php URI::baseURL(); ?>assets/images/donatur/idompet.png"></td>
					<td>
						<h2 class="bold" style="font-size: 15%; font-size: 3vw">Rp. <?php  echo number_format(empty($saldo['amount'])?0:$saldo['amount'],0,",",".");?></h2>
						<span class="sm-bold">SALDO</span>
					</td>
				</tr>
			</table>
		</div>
		<br clear="all"/>
		<div class="tab-info">
			<!-- Nav tabs -->
			<?php if(!empty($this->session->flashdata('error'))):?>
            <div class="alert alert-danger">
              <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php endif;?>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="mutasi-tab" data-toggle="tab" href="#mutasi" role="tab" aria-controls="mutasi" aria-selected="true">Mutasi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Tambah Saldo</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="mutasi" role="tabpanel" aria-labelledby="mutasi-tab">
                    {_fmutasi}
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    {_ftsaldo}
                </div>
            </div>

		</div>
	</div>
</div>
<br clear="all"/>
<br clear="all"/>
<br clear="all"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
<script>
    $("#respemb").load("<?php echo base_url('/pembayaran/index?isnodepo=true') ?>");
    // $('#denomnms').number( true, 0, '', '.' );
    var fnf = document.getElementById("denomnms");
    fnf.addEventListener('keyup', function (evt) {
        var n = parseInt(this.value.replace(/\D/g, ''), 10);
        fnf.value = n.toLocaleString("id-ID");
    }, false);

</script>