<div>
    <p>Riwayat transaksi dompet aksi baik</p>
    <div class="table-responsive">
        <table
                id="mutasi_donasi"
                class="table table-striped table-bordered table-sm"
                width="100%"
        >
            <thead>
            <tr>
                <th>No Transaksi</th>
                <th>Jumlah</th>
                <th>Jenis</th>
                <th>Deskripsi</th>
                <th>Waktu</th>
            </tr>
            </thead>
            <tbody><?php foreach ($trx as $r): ?>
                <tr class="text-center">
                    <td><?php echo $r->no_transaksi; ?></td>
                    <td><?php echo number_format($r->nomuniq, 0, ",", "."); ?></td>
                    <td><?php echo $r->jenistrx; ?></td>
                    <td><?php echo $r->nama_trx; ?></td>
                    <td><?php echo $r->edtd_at; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>