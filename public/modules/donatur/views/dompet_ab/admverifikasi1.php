{_layHeader}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Verifikasi Data</h2>
            <hr>
            <table class="table table-stripped">
                <thead>
                <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Tujuan Bank</th>
                    <th>Nominal</th>
                    <th>Status</th>
                    <th>action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                //echo "<pre>";
                //print_r($donasi);
                foreach($donasi as $r):?>
                    <tr>
                        <td><?php echo $r->namaDonatur;?></td>
                        <td><?php echo $r->emailDonatur;?></td>
                        <td><?php echo $r->namaBank;?></td>
                        <td><?php echo number_format($r->nomuniq,0,"","."); ?></td>
                        <td><?php echo $r->status?></td>
                        <td><a href="<?php echo base_url('donatur/doVerifikasi/'.$r->no_transaksi); ?>" class="btn btn-success btn-sm">verified</a></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="6" class="text-center"><b>Data berikut masih belum di proses, jika ingin memproses secara manual silahkan klik "verified" </b></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
{_layFooter}