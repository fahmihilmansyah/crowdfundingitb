<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class donatur extends NBFront_Controller

{



    /**

     * ----------------------------------------

     * #NB Controller

     * ----------------------------------------

     * #author            : Fahmi Hilmansyah

     * #time created    : 4 January 2017

     * #last developed  : Fahmi Hilmansyah

     * #last updated    : 4 January 2017

     * ----------------------------------------

     */



    private $_modList = array('donatur/donatur_model' => "mdl_donatur");



    protected $_data = array();

    private $_ap_ori;

    private $_ap_thumb;

    private $def;

    private $iddonaturs;

    private $_SESSION;

    private $saldo=0;
    function __construct()

    {

        parent::__construct();



        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

        $this->_ap_ori = FCPATH . 'assets/images/donatur/profile/';

        $this->_ap_thumb = FCPATH . 'assets/images/donatur/profile/thumbnails/';

        $this->setActiveMenu();
	    
        // REDIRECT IF SESSION EXIST

        $this->nbauth_front->lookingForAuten();
        
        $this->_SESSION   = !empty($this->session->userdata(LGI_KEY . "login_info")) ? 
                                   $this->session->userdata(LGI_KEY . "login_info") : '';

        $this->iddonaturs = empty($this->_SESSION['userid'])? '' :$this->_SESSION['userid'];

        $kondisi['where'] = array(

            'nb_donaturs_d.donatur' => $this->iddonaturs,

        );
        $kondisi['join'] = array('table' => 'nb_donaturs', 'condition'=>'nb_donaturs_d.donatur = nb_donaturs.id', 'type'=>'left');

        $this->def = $this->mdl_donatur->custom_query('nb_donaturs_d', $kondisi)->result();

    }



    private function modelsApp()

    {

        $this->load->model($this->_modList);
        $ses = $this->session->userdata(LGI_KEY . "login_info");
        $HEADER = array('TOKEN-KEY: DDTEKNOPAYMENT', 'APP-KEY: APPKEYDDTEKNO', 'token-login: ' . $ses['mupays_token']);
        $datasaldo = Fhhlib::_curl_exexc_with_header($this->_data['mupays_url'] . "/mupays/transaksi/getsaldo", null, 'GET', $HEADER);
        $datasaldo = json_decode($datasaldo, true);
        $data['saldo_user'] = null;
        if ($datasaldo['rc'] == '0000') {
            $this->saldo = $datasaldo['data'];
        }

    }



    private function setActiveMenu()

    {

        $uri = $this->uri->segment(2);



        $this->_data["menu"] = '';

        $this->_data["profile"] = (!empty($uri) && $uri == 'profile') ? 'profile-w' : '';

        $this->_data["campaign"] = (!empty($uri) && $uri == 'campaign') ? 'campaign-w' : '';

        $this->_data["donasiku"] = (!empty($uri) && $uri == 'donasiku') ? 'donasiku-w' : '';

        $this->_data["dompet"] = (!empty($uri) && $uri == 'dompet') ? 'dompet-w' : '';

        $this->_data["poin"] = (!empty($uri) && $uri == 'poin') ? 'poin-w' : '';

    }



    public function index($data = array())

    {

        $data = $this->_data;



        $this->session->userdata("menu", "profile");

        $donaturs = $this->iddonaturs;

        $kondisi['where'] = array(

            'nb_trans_donation.donatur' => $donaturs,

            'nb_trans_donation.status' => "'verified'",

        );

        $kondisi['join'] = array("table" => "nb_donaturs_d", "condition" => "nb_donaturs_d.donatur = nb_trans_donation.donatur", "type" => "left");

        $select = "count(nb_trans_donation.donatur)jml_camp, sum(nb_trans_donation.nomuniq) jml_nominal, sum(uniqcode)jml_point";

        $data['donasi'] = $this->mdl_donatur->custom_query('nb_trans_donation', $kondisi, $select)->result();

        $totalpoint = 0;
        $qry = $this->mdl_donatur->getTrxPoint($donaturs)->result();
        foreach ($qry as $row) {
            $totalpoint += $row->uniqcode;
        }
        $data['point'] = $totalpoint;

        $kondisi1['where'] = array(

            'nb_donaturs_saldo.donatur' => $donaturs,

        );

        $data['saldo'] =  $this->saldo;

        $data['indexs'] = $this->def;

        $data["_content"] = $this->parser->parse('donatur/overview/index', $data, TRUE);



        $data["_widget"] = $this->parser->parse('donatur/widget', $data, TRUE);



        $this->parser->parse("index", $data);

    }



    public function profile($data = array())

    {

        if ($_POST) {

            $this->nbauth = new nbauth();

            $dataupdate = array();

            if (!empty($this->input->post('infosubmit'))) {

                $fname = $this->input->post('fname');

                $email = $this->input->post('email');

                $notel = $this->input->post('notel');

                $info = $this->input->post('info');

                $dataupdate = array(

                    'fname' => $fname,

                    // 'email' => $email,

                    'phone' => $notel,

                    'info' => $info

                );

                $where = array('nb_donaturs_d.donatur' => $this->iddonaturs);

                $this->mdl_donatur->custom_update('nb_donaturs_d', $dataupdate, $where);
                $this->session->set_flashdata('success','Berhasil ubah data diri');
                redirect('myprofile');
            }

            if (!empty($this->input->post('pwdsubmit'))) {



                $pwdold = $this->input->post("pwdold");

                $pwdnew = $this->input->post("pwdnew");

                $pwdrenew = $this->input->post("pwdrenew");

                if ($pwdnew != $pwdrenew) {

                    die('password not match');

                }

                $kondisi1['where'] = array(

                    'nb_donaturs.id' => $this->iddonaturs,

                    'nb_donaturs.pwd' => $this->db->escape($this->nbauth->getCustomHash($pwdold)),

                );

                $cekpwd = $this->mdl_donatur->custom_query('nb_donaturs', $kondisi1)->result();

                if (count($cekpwd) == 0) {

                    die('Cannot Find Your Password');

                }

                $dataupdate = array(

                    'pwd' => $this->nbauth->getCustomHash($pwdnew),

                    'edtd_at' => date("Y-m-d H:i:s"),

                );

                $where = array('nb_donaturs.id' => $this->iddonaturs);

                $this->mdl_donatur->custom_update('nb_donaturs', $dataupdate, $where);
                $this->session->set_flashdata('success','Berhasil ubah Password');
                redirect('myprofile');

            }

            if (!empty($this->input->post('fotosubmit'))) {
                // SETTING UPLOAD

                $this->upload->initialize(array(

                    "upload_path" => $this->_ap_ori,

                    "allowed_types" => "png|jpg|jpeg|gif",

                    "overwrite" => TRUE,

                    "encrypt_name" => TRUE,

                    "remove_spaces" => TRUE,

                    "max_size" => 30000,

                    "xss_clean" => FALSE,

                    "file_name" => array("image_" . date('Ymdhis'))

                ));

                if ($_FILES) {

                    if ($this->upload->do_multi_upload("uploadedimages")) {

                        $return = $this->upload->get_multi_upload_data();



                        $dataimg['img'] = $return[0]["orig_name"];



                        resize_img($this->_ap_thumb, $return);

                        /*// DELETE IMAGE FROM FOLDER

                        unlink($this->_ap_ori . $this->input->post("img"));

                        unlink($this->_ap_thumb . $this->input->post("img"));*/
			$where = array('nb_donaturs_d.donatur' =>  $this->iddonaturs);
                        $this->mdl_donatur->custom_update('nb_donaturs_d', $dataimg, $where);
                        $this->session->set_flashdata('success','Berhasil ubah gambar');
                        redirect('myprofile');
                    } else {

                        redirect('/cms/error/error_upload');

                    }

                }
            }
        }

        $data = $this->_data;

        $kondisi1['where'] = array('nb_donaturs_d.donatur' => $this->iddonaturs);
	$kondisi1['join'] = array('table'=>'nb_donaturs', 'condition'=>'nb_donaturs.id = nb_donaturs_d.donatur', 'type'=>"left");

        $data['info'] = $this->mdl_donatur->custom_query('nb_donaturs_d', $kondisi1)->result();

        $data["_finfo"] = $this->parser->parse('donatur/profile/form/info', $data, TRUE);

        $data["_fpwd"] = $this->parser->parse('donatur/profile/form/password', $data, TRUE);

        $data["_ffoto"] = $this->parser->parse('donatur/profile/form/foto', $data, TRUE);



        $data["_content"] = $this->parser->parse('donatur/profile/index', $data, TRUE);

        $data['indexs'] = $this->def;

        $data["_widget"] = $this->parser->parse('donatur/widget', $data, TRUE);



        $this->parser->parse("index", $data);

    }



    public function mycampaign()

    {

        die("Gak usah bro");

        $data = $this->_data;



        $data["_content"] = $this->parser->parse('donatur/my_campaign/index', $data, TRUE);

        $data['indexs'] = $this->def;

        $data["_widget"] = $this->parser->parse('donatur/widget', $data, TRUE);



        $this->parser->parse("index", $data);

    }





    public function mydonation()

    {

        $donaturs = $this->iddonaturs;

        $data = $this->_data;

        $kondisi['where'] = array(

            'nb_donaturs.id' => $donaturs,

            'nb_trans_donation.status' => "'verified'",
            'nb_trans_program.status' => "'verified'",
            // 'upper(nb_trans_program.jenistrx) not in'=>"('DEPOSIT')",
        );

        $kondisi['join'] = array(

            array("table" => "nb_trans_program", "condition" => "nb_trans_program.donatur = nb_donaturs.id", "type" => "left"),

            array("table" => "nb_trans_donation", "condition" => "nb_trans_donation.donatur = nb_donaturs.id", "type" => "left"),


        );

        $select = "sum(nb_trans_program.nomuniq) jml_nomuniq_program, sum(nb_trans_donation.nomuniq) jml_nomuniq_donation";

        $totaldonasi = 0;
        $qry = $this->mdl_donatur->getTrx($donaturs)->result();
        foreach ($qry as $row) {
            $totaldonasi += $row->nominal + $row->uniqcode;
        }
        $data['donation'] = $totaldonasi;
        // $this->mdl_donatur->custom_query('nb_donaturs', $kondisi, $select)->result();
        /*echo $this->db->last_query();
        die();*/
        $data['indexs'] = $this->def;

        $data['trx'] = $this->mdl_donatur->getTrx($donaturs)->result();

        $data["_content"] = $this->parser->parse('donatur/my_donation/index', $data, TRUE);



        $data["_widget"] = $this->parser->parse('donatur/widget', $data, TRUE);



        $this->parser->parse("index", $data);

    }



    public function dompetku()

    {
        
        if ($_POST) {

            $donatur = $this->iddonaturs;
            $iddonatur = $this->iddonaturs;

            $jenis = $this->input->post('otherJenis');

            $nominal = $this->input->post('nominaldeposit');
            $nominal = str_replace(".", '', $this->input->post('nominaldeposit'));

            $comment = "";

            $donationName = $this->_SESSION['fname'];

            $donationEmail = $this->_SESSION['email'];

            $donationPhone = $this->def[0]->phone;

            $methodpay = $this->input->post('donationPay');
            $error = "";
            if(empty($jenis)){
                $error .= '- Kesalahan sistem, hubungin administrator. <br>';
            }
//            if($nominal < 49999 ||  !is_numeric($nominal)){
//                $error .= '- Nominal dana harus minimal Rp.50.000 <br>';
//            }
            if(empty($methodpay)){
                $error .= '- Metode pembayaran harus dipilih. <br>';
            }
            if(empty($donationEmail) || empty($donationPhone) || strlen($donationPhone) < 9){
                $error .= '- Email / Telp harus diisi. Silakan edit profil anda. <br>';
            }
            if(!empty($error)){
                $dataerror = array('error'=>$error);
                $this->session->set_flashdata($dataerror);
                $preflink = $this->session->userdata('urllink');
                redirect($preflink);
                // redirect('mywallet');
                exit;
            }
            $notrx = date('Ymd') . sprintf('%05d', $this->geninc());

            $uniqcode = ($nominal);

            $insertData = array(

                'no_transaksi' => $notrx,

                'nominal' => $nominal,

                'trx_date' => date('Y-m-d'),

                'jenistrx' => $jenis,

                'donatur' => $donatur,/*ubah donaturnya jika login maka akan mengambil session loginnya*/

                'bank' => $methodpay,

                'comment' => $comment,

                'uniqcode' => $nominal,

                'namaDonatur' => $donationName,

                'emailDonatur' => $donationEmail,

                'telpDonatur' => $donationPhone,

                'nomuniq' => $nominal,

                'validDate' => date('Y-m-d', strtotime('+2 days')),

                'crtd_at' => date("Y-m-d H:i:s"),

                'edtd_at' => date("Y-m-d H:i:s"),
                'month' => date("m"),
                'year' => date("Y"),

            );

            $this->mdl_donatur->custom_insert("nb_trans_program", $insertData);



            $datadonat = array(

                'doneDonation' => $notrx,

            );

//            $this->session->set_userdata($datadonat);
//
//            redirect('invoice/program/'.$notrx);

            $idipg = Fhhlib::uuidv4Nopad();

            $arrParam['idipg'] = $idipg;
            $expmethod = explode('|', $methodpay);
            if ($expmethod[0] == 'FINPAY') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = ($donationPhone);
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                $arrParam['items'] = "Pembayaran Deposit";
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                    }
                }
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = "Pembayaran Deposit";//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_program',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

                $this->mdl_donatur->custom_insert("nb_trans_ipg", $insipgtrx);
                $res = Fhhlib::finpayIPG($arrParam);
//            $res = $this->finpayIPG($nominal,$donationEmail,$iddonatur,$donationPhone,$donationName,$notrx);
                if (!empty($res)) {
                    if ($res['status_code'] == '00') {
                        if (!empty($res['landing_page'])) {
                            redirect($res['landing_page'], 'refresh');
                        } elseif (!empty($res['redirect_url'])) {
                            redirect($res['redirect_url'], 'refresh');
                        } else {
                            $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                            $this->session->set_flashdata($dataerror);
                            redirect('donasi/index/' . $id);
                        }
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('donasi/index/' . $id);
                        exit;
                    }
                }
//                header("Location: ".$res);
                print_r($res);
                exit;
//            print_r($res);exit;
            }

            elseif ($expmethod[0] == 'MUPAYS') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = ($donationPhone);
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                $arrParam['token_login'] = !empty($this->_SESSION['mupays_token'])?$this->_SESSION['mupays_token']:"";
                $arrParam['items'] = "Top Up MuPays ";
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                    }
                }
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = "Top Up MuPays ";//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes
                $arrParam['type'] = "topup";//in minutes

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_donation',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

                $this->mdl_donatur->custom_insert("nb_trans_ipg", $insipgtrx);
                $res = Fhhlib::devfinpayIPG($arrParam);
//            $res = $this->finpayIPG($nominal,$donationEmail,$iddonatur,$donationPhone,$donationName,$notrx);
                if (!empty($res)) {
                    if ($res['rc'] == '0000') {
                        if (!empty($res['data']['no_va'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['hasil'] = $res['data'];
                            return $this->parser->parse("view_vatrx", $datas);
                        }
                        if (!empty($res['data']['landing_page'])) {
                            return redirect($res['data']['landing_page'], 'refresh');
//                            $datas = $this->_data;
//                            $datas['hasil'] = $res['data'];
//                            return $this->parser->parse("view_vatrx", $datas);
                        }
                        if (!empty($res['data']['qris'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['url_img'] = $res['data']['qris']['url_img'];
                            $datas['nominal'] = $nominal;
                            return $this->parser->parse("view_qris", $datas);
                        }
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('donasi/index/' . $id);
                        exit;
                    }
                }
//                header("Location: ".$res);
                print_r($res);
                exit;
//            print_r($res);exit;
            }
            elseif($expmethod[0] == 'E2PAY') {
                $cariipg =  $this->mdl_donatur->custom_query('nb_partner_ipg', ['where'=>['id' => $this->db->escape('E2PAY')]])->result();
                $key = !empty($cariipg[0]->key) ? $cariipg[0]->key : "v1XdZSLMs9";
                $merchant_id = !empty($cariipg[0]->merchant_key) ? $cariipg[0]->merchant_key : "IF00294";
                $merchantkey =$key;//'v1XdZSLMs9';
                $merchantcode =$merchant_id;//'IF00294';
                $arrParam['url_host'] = $cariipg[0]->url_host;
                $arrParam['MerchantCode'] = $merchantcode;
                $arrParam['PaymentId'] = $expmethod[1];
                $arrParam['RefNo'] = $notrx;
                $arrParam['Amount'] =$nominal."00";
                $arrParam['Currency'] = 'IDR';
                $arrParam['ProdDesc'] = "Pembayaran Deposit";
                $arrParam['UserName'] = $donationName;
                $arrParam['UserEmail'] = $donationEmail;
                $arrParam['UserContact'] = $donationPhone;
                $signatures = Fhhlib::E2Pay_signature($merchantkey.$merchantcode.$arrParam['RefNo'].$arrParam['Amount'].$arrParam['Currency']);
                $arrParam['Signature'] = $signatures;
                $arrParam['Remark'] = '';
                $arrParam['ResponseURL'] = base_url('trxipg/responsee2pay');
                $arrParam['BackendURL'] = base_url('trxipg/backende2pay');

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_program',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

//                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $this->mdl_donatur->custom_insert("nb_trans_e2pay", $insipgtrx);
                $datas = $this->_data;
                $datas['parame2pay']=$arrParam;
////            print_r($datas);
                return $this->parser->parse("view_e2pay", $datas);

                $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . $res);
                $this->session->set_flashdata($dataerror);
                redirect('donatur/dompetku');

            }else{
                $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . $res);
                $this->session->set_flashdata($dataerror);
                redirect('donatur/dompetku');
            }
        } else {

            $data = $this->_data;

            $donaturs = $this->iddonaturs;
            

            $kondisi['where'] = array(

                'nb_donaturs_saldo.donatur' => $donaturs,

            );

            $data['saldo'] =$this->saldo;



            $data['trx'] = $this->mdl_donatur->getTrxDeposit($donaturs)->result();

            $bank = $this->mdl_donatur->custom_query("nb_bank")->result();

            $data['bank'] = $bank;

            $data["_fmutasi"] = $this->parser->parse('donatur/dompet_ab/form/mutasi', $data, TRUE);

            $data["_ftsaldo"] = $this->parser->parse('donatur/dompet_ab/form/tsaldo', $data, TRUE);

            $data["_fpencairan"] = $this->parser->parse('donatur/dompet_ab/form/pencairan', $data, TRUE);

            $data['indexs'] = $this->def;

            $data["_content"] = $this->parser->parse('donatur/dompet_ab/index', $data, TRUE);



            $data["_widget"] = $this->parser->parse('donatur/widget', $data, TRUE);



            $this->parser->parse("index", $data);

        }

    }



    public function mypoin()

    {

        $data = $this->_data;

        $donaturs = $this->iddonaturs;

        

        $totalpoint = 0;
        $qry = $this->mdl_donatur->getTrxPoint($donaturs)->result();
        foreach ($qry as $row) {
            $totalpoint += $row->uniqcode;
        }
        $data['point'] = $totalpoint;

        $data['trx'] = $this->mdl_donatur->getTrxPoint($donaturs)->result();

        $data['indexs'] = $this->def;

        $data["_content"] = $this->parser->parse('donatur/poin_ab/index', $data, TRUE);



        $data["_widget"] = $this->parser->parse('donatur/widget', $data, TRUE);



        $this->parser->parse("index", $data);

    }



    private function genUniq($nominal = 0)

    {

        $condition['where'] = array(

            $this->db->escape($nominal) . ' BETWEEN' => ' minlimit and maxlimit'

        );

        $generate = $this->mdl_donatur->custom_query("nb_incuniq", $condition)->result();

        $uniqid = $generate[0]->id;

        $uniqinc = $generate[0]->uniqinc + 1;

        if ($uniqinc > $generate[0]->maxuniq) {

            $uniqinc = $generate[0]->minuniq;

        }

        $this->mdl_donatur->custom_update("nb_incuniq", array('uniqinc' => $uniqinc), array('id' => $uniqid));

        return $uniqinc;

    }



    private function geninc()

    {

        $kondisi['where'] = array('id' => $this->db->escape(1));

        $generate = $this->mdl_donatur->custom_query("nb_inc", $kondisi)->result();

        $tgl = $generate[0]->tgl;

        $uniqinc = 0;

        if ($tgl != date("Y-m-d")) {

            $uniqinc = $uniqinc + 1;

            //echo $uniqinc."||".$tgl;

        } else {

            $uniqinc = $generate[0]->inc + 1;

            //echo "else : ".$uniqinc;

        }

        $this->mdl_donatur->custom_update("nb_inc", array('inc' => $uniqinc, 'tgl' => date("Y-m-d")), array('id' => $this->db->escape(1)));

        //echo $this->db->last_query();

        return $uniqinc;

    }



    function summary()

    {



        $iddonasi = $this->session->userdata('doneDonation');



        $kondisi['where'] = array('nb_trans_program.no_transaksi' => $this->db->escape($iddonasi));

        $kondisi['join'] = array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_program.bank", "type" => "left");

        $summary = $this->mdl_donatur->custom_query("nb_trans_program", $kondisi)->result();

        $data = $this->_data;

        $data['summary'] = $summary;

        //$this->sendMail($summary);

        $this->parser->parse("donatur/dompet_ab/summary", $data);

    }



    /*untuk proses verifikasi manual transaksi  mulai dari transaksi zakat dan deposit

        =========STARAT============

    */

    function admverifikasi()

    {

        $data = $this->_data;

        $kondisi['where'] = array('status not in' => "('verified')");

        $kondisi['join'] = array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_program.bank", "type" => "left");

        $summary = $this->mdl_donatur->custom_query("nb_trans_program", $kondisi)->result();

        $data['donasi'] = $summary;

        $this->parser->parse("donatur/dompet_ab/admverifikasi", $data);

    }



    function doVerifikasi($id = null)

    {

        if (empty($id)) {

            redirect('donatur/admverifikasi');

        }

        $kondisi['where'] = array("nb_trans_program.no_transaksi" => $this->db->escape($id), "nb_trans_program.status" => $this->db->escape("unverified"));

        $cekdonasi = $this->mdl_donatur->custom_query("nb_trans_program", $kondisi)->result();

        if (count($cekdonasi) != 1) {

            die("cannot proses");

        } else {

            $nomuniq = $cekdonasi[0]->nomuniq;

            $notransaksi = $cekdonasi[0]->no_transaksi;

            $iddonatur = $cekdonasi[0]->donatur;

            $jenistrx = $cekdonasi[0]->jenistrx;

            /*Kondisi untuk melakukan transaksi melalui deposit*/

            if (strtoupper($jenistrx) == "DEPOSIT") {

                $this->trxDeposit($iddonatur, $nomuniq, $notransaksi, "DEPOSIT");

            } else {

                $this->trxDeposit($iddonatur, $nomuniq, $notransaksi, "TRX");

            }

            /*update nb_trans_program*/

            $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));

            $where = array("nb_trans_program.no_transaksi" => ($id),);

            $this->mdl_donatur->custom_update("nb_trans_program", $data, $where);

            redirect('donatur/admverifikasi/');

        }

    }


    private function deleteNoTrx($notrx=null){
    $deldata = array('no_transaksi' => $notrx);
    $this->zakatmdl->custom_delete("nb_trans_program", $deldata);
    }

    function trxDeposit($iddonatur = null, $nominal = null, $notransaksi = null, $jenis = null)

    {

        $kondisi['where'] = array("nb_donaturs_saldo.donatur" => $this->db->escape($iddonatur));

        $ceksaldo = $this->mdl_donatur->custom_query("nb_donaturs_saldo", $kondisi)->result();

        if (count($ceksaldo) != 1) {

            die("cannot proses");

        } else {

            $balanceAmount = 0;

            $prevBlance = $ceksaldo[0]->balanceAmount;

            if ($jenis == "DEPOSIT") {

                $balanceAmount = (int)$nominal + (int)$ceksaldo[0]->balanceAmount;

                $data = array('donaturs' => $iddonatur,

                    'jenistrx' => "DEPO",

                    'id_transaksi' => $notransaksi,

                    'prevBalance' => $prevBlance,

                    'amount' => $nominal,

                    'balance' => $balanceAmount,

                    'crtd_at' => date("Y-m-d H:i:s"),

                    'edtd_at' => date("Y-m-d H:i:s"),

                );

                $this->mdl_donatur->custom_insert('nb_donaturs_trx', $data);

            } else {

                $balanceAmount = (int)$ceksaldo[0]->balanceAmount - (int)$nominal;

                if ($balanceAmount < 0) {

                    $dataerror = array('error'=>'- Saldo Tidak Cukup, segera isi dompet baik anda');
                    $this->session->set_flashdata($dataerror);
                    $preflink = $this->session->userdata('urllink');
                    redirect($preflink);
                    exit;
                die("Maaf Saldo Tidak Cukup");

                }

                $data = array('donaturs' => $iddonatur,

                    'jenistrx' => "TRX",

                    'id_transaksi' => $notransaksi,

                    'prevBalance' => $prevBlance,

                    'amount' => $nominal,

                    'balance' => $balanceAmount,

                    'crtd_at' => date("Y-m-d H:i:s"),

                    'edtd_at' => date("Y-m-d H:i:s"),

                );

                $this->mdl_donatur->custom_insert('nb_donaturs_trx', $data);

            }

            $data = array("prevBalance" => $prevBlance,

                'balanceAmount' => $balanceAmount, 

                'edtd_at' => date("Y-m-d H:i:s"));

            $where = array("nb_donaturs_saldo.donatur" => $iddonatur);

            $this->mdl_donatur->custom_update("nb_donaturs_saldo", $data, $where);



        }

    }

    /*=============END==================*/



}

