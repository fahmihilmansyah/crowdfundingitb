{_layHeader}
	<div id="pengurus">
		<div class="container-fluid" style="margin-bottom:30px !important">
			<div class="row">
				<div id="imgpengurus" class="susunan">
					<center>
						<h1 class="bold">SUSUNAN PENGURUS</h1>
						<br/>
						<p style="width:50%">
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
						</p>
					</center>
				</div>
							
				<div class="konten">
					<div class="row col-sm-6">
						<h3 class="sm-bold">PEMBINA</h3>
							<p>Prof. Mahmud Zaki MSc.</p>
							<p>Prof. DR. H. Moh Nuh DEA</p>
							<p>H.M. Farid Yahya</p>
							<p>Fauzi Salim Martak</p>

						<h3>PENGAWAS</h3>
							<p>Drs. Sugeng Praptoyo, Ak., MM., MH.</p>
							<p>Drs. H. Muhammad Taufiq A.B</p>
							<p>Ir. H. Abdul Ghaffar AS</p>


						<h3>PENGURUS</h3>
							<h4>Ketua:</h4>
							<p>Ir. H. Abdul Kadir Baraha</p>

							<h4>Sekretaris:</h4>
							<p>Shakib Abdullah</p>
					</div>
					<div class="row col-sm-6">
							<h4>Bendahara:</h4>
							<p>Shakib Abdullah</p>

							<h4>Direktur:</h4>
							<p>Jauhari Sani</p>

						<h3>DEWAN SYARIAH</h3>
							<p>Drs H.M Taufiq A. B</p>
							<p>Dr. H. Zainuddin Mz, Lc., MA</p>
							<p>Isa Saleh Kuddeh, M. Pd. I.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
{_layFooter}