<?php
/**
 * Created by PhpStorm.
 * Project : berzakat
 * User: fahmihilmansyah
 * Date: 12/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 15.06
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */

 ?>
{_layHeader}
<div class="row">
    <div class="col-md-12 ">
        <div style="padding: 20px; ">
            <img class="mx-auto d-block" id="qrisimg" style="
    display:block" src="<?php echo $url_img?>">
        </div>
    </div>
    <div class="col-md-12 text-center">
        <b><span> Silakan scan atau capture gambar QRIS untuk di upload di e-wallet Anda</span>
        <br>
        <span>Atau</span>
        <br>
        <a href="#" id="downloadqris">Download QRIS</a>
        </b>
    </div>
</div>
<script>
    $(document).ready(function () {

        $("#downloadqris").on('click',function () {
            var links = $("#qrisimg").attr('src');
            var splitse = links.split("/");
            var filenames = splitse[splitse.length - 1];
            var explfilenam =  filenames.split(".");
            filenames = filenames.replace(explfilenam[0],"QRISFile");
            myCanvas = document.getElementById("qrisimg");
            var link = document.createElement('a');
            link.href = links;  // use realtive url
            link.download = filenames;
            document.body.appendChild(link);
            link.click();
        });

    });
</script>
{_layFooter}