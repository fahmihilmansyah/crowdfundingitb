<div class="col-md-3">
	&nbsp;
</div>
<div class="col-md-6">
	<center>
		<h4 class="sm-bold">No HP Anda</h4>
		<p>Pastikan nomor ini aktif untuk menerima SMS status donasi Anda:</p>

	    <?php
		    $hidden_form = array('id' => !empty($id) ? $id : '');
		    echo form_open_multipart('', array('id' => 'fmdonasi'), $hidden_form);
	    ?>

		<div>
			<?php 
				$name  = 'donationNominal';
				$value =  empty($this->session->userdata('prevnomDonation'))? "" : 
								$this->session->userdata('prevnomDonation');

				$extra = 'id="donationNominal" 
						  class="form-control" 
						  placeholder="Masukan no HP anda, dengan format 08xxx atau 62xxx" style="text-align:center"';

				echo form_input($name, $value, $extra); 
			?>
		</div>

		<br clear="all"/>
		<h4 class="sm-bold">Pilih Metode Pembayaran</h4>


		<div>
			<label class="radio-payment">
				<?php 
					$data = array(
						'name'        => 'donationPay',
						'value'       => 0,
						'checked'     => (set_value('opt_email') === '0' ? TRUE : FALSE),
					);

					echo form_radio($data);
				?>
				<span>Dompet Aksi Baik</span>
			</label>

			<label class="radio-payment">
				<?php 
					$data = array(
						'name'        => 'donationPay',
						'value'       => 0,
						'checked'     => (set_value('opt_email') === '0' ? TRUE : FALSE),
					);

					echo form_radio($data);
				?>
				<span>Transfer BCA</span>
			</label>

			<label class="radio-payment">
				<?php 
					$data = array(
						'name'        => 'donationPay',
						'value'       => 0,
						'checked'     => (set_value('opt_email') === '0' ? TRUE : FALSE),
					);

					echo form_radio($data);
				?>
				<span>Transfer BNI</span>
			</label>

			<label class="radio-payment">
				<?php 
					$data = array(
						'name'        => 'donationPay',
						'value'       => 0,
						'checked'     => (set_value('opt_email') === '0' ? TRUE : FALSE),
					);

					echo form_radio($data);
				?>
				<span>Transfer BRI</span>
			</label>

			<label class="radio-payment">
				<?php 
					$data = array(
						'name'        => 'donationPay',
						'value'       => 0,
						'checked'     => (set_value('opt_email') === '0' ? TRUE : FALSE),
					);

					echo form_radio($data);
				?>
				<span>Transfer MANDIRI</span>
			</label>
		</div>

		<div>
			<button type="submit" class="btn btn-success" style="width:100%; margin-top:10px"> Lanjut</button>
		</div>	
	</center>
    <?php
    	echo form_close();
    ?>
</div>
<div class="col-md-3">
	&nbsp;
</div>