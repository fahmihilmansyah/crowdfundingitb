<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description File
 * 
 * @copyright	Copyright (c) DDTEKNO
 * @author		Fahmi Hilmansyah
 * @version		1.0
 */

class Welcome extends CI_Controller {
	public function index()
	{
		$this->load->view("index");
	}
}