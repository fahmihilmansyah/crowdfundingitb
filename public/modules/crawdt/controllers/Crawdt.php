<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Crawdt extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : No Reply
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */

    private $_modList = array(
        "kurban/kurban_model" => "infaqmdl",
        "ppob/kurban_model"=>'donasimdl'
    );

    protected $_data = array();


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();


        $this->load->library('myphpmailer');

    }


    private function modelsApp()

    {
        $this->load->model($this->_modList);
        $this->load->library('Fhhlib');

    }


    public function index($data = array())

    {
        $res = $this->_curl_exexc('http://localhost/testhtml/ddwidget/ddznews.php','','GET');
        echo "<pre>";
        print_r(json_decode($res,true));
        exit;
    }

    private function _curl_exexc($url = '', $postdata = '', $METHOD = 'POST', $HEADER = [])
    {
//        $postdata = http_build_query($data);
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADER);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }

    private function genUniq($nominal = 0)
    {
        $condition['where'] = array(
            $this->db->escape($nominal) . ' BETWEEN' => ' minlimit and maxlimit'
        );
        $generate = $this->infaqmdl->custom_query("nb_incuniq", $condition)->result();
        $uniqid = $generate[0]->id;
        $uniqinc = $generate[0]->uniqinc + 1;
        if ($uniqinc > $generate[0]->maxuniq) {
            $uniqinc = $generate[0]->minuniq;
        }
        $this->infaqmdl->custom_update("nb_incuniq", array('uniqinc' => $uniqinc), array('id' => $uniqid));
        return $uniqinc;
    }
}

