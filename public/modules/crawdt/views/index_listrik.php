<?php
/**
 * Created by PhpStorm.
 * Project : berzakat
 * User: fahmihilmansyah
 * Date: 06/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.34
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>
{_layHeader}

<div class="content">

    <div class="card">
        <div class="card-body">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-prabayar" role="tab" aria-controls="pills-prabayar" aria-selected="true">Prabayar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-pascabayar" role="tab" aria-controls="pills-pascabayar" aria-selected="false">Pascabayar</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-prabayar" role="tabpanel" aria-labelledby="pills-home-tab">
                    prabayar
                </div>
                <div class="tab-pane fade" id="pills-pascabayar" role="tabpanel" aria-labelledby="pills-profile-tab">
                    pascabayar
                </div>
            </div>
        </div>
    </div>

    {_layFooter}