{_layHeader}
<?php

$this->load->model(array('home/Home_model' => "home"));
?>

<!--<div class="featured-image" style="background-image:url(https://images.unsplash.com/photo-1488521787991-ed7bbaae773c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80);"></div>-->
<!--<div class="featured-images">-->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <?php $i = 0;
        foreach ($_data_carousel as $row):
            $actf = '';
            if ($i == 0) {
                $actf = 'active';
            }
            $i++;
            ?>
            <?php
            // $img = !empty($row->img) ? $row->img : "noimage.png";
            $img = !empty($row->img) ? $row->img : '';

            $img = URI::existsImage('slider/',
                $img,
                'noimg_front/',
                'no-slider.jpg');
            ?>
            <div class="carousel-item text-center <?php echo $actf ?>">
                <!--                <img class="d-block w-100 h-100" src="--><?php //echo $img
                ?><!--" alt="First slide">-->
                <?php if (!empty($row->link)): ?>
                    <a href="<?php echo count(explode('//', $row->link)) > 1 ? $row->link : '//' . $row->link ?>">
                        <img class="d-block w-100 h-100" src="<?php echo $img ?>" alt="First slide">
                    </a>
                <?php else:; ?>
                    <img class="d-block w-100 h-100" src="<?php echo $img ?>" alt="First slide">
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!--</div>-->
<div class="content">
    <?php
    if ($_loginstatus) {
        $sess = $this->session->userdata(LGI_KEY . "login_info");
        $imgpoto = '';
        if (!empty($indexs[0]->img)):
            $imgpoto = base_url('assets/images/donatur/profile/'.$indexs[0]->img);
        else:
            $imgpoto = $indexs[0]->picture_url;
        endif;
        ?>
        <div class="top-info">
            <div class="row">
                <div class="col" style="z-index: 2000;">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div style="border-right: 1px solid #8080804f" class="col-3">
                                    <img class="rounded-circle" height="65" width="65"
                                         src="<?php echo $imgpoto ?>">
                                </div>
                                <div class="col-9">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row align-items-center">
                                                <div class="col-3">
                                                    <img class="rounded" height="65" width="65"
                                                         src="<?php URI::baseURL(); ?>assets/images/logo/logomumuh2es.png">
                                                </div>
                                                <div class="col-6" style="display: block;">
                                                    <div><b>Saldo</b>
                                                    </div>
                                                    <div style="font-size: 24px; margin-top: -10px;">
                                                        <b>Rp. <?php echo number_format($saldo_user['amount'], 0, ',', '.') ?></b>
                                                    </div>
                                                    <div style="font-size: 12px; margin-top: -2px;">Saldo Bonus</div>
                                                </div>
                                                <div class="col-3 align-items-center">
                                                    <?php if (empty($sess['mupays_token'])): ?>
                                                        <a href="<?php echo base_url('/aktivasiwallet') ?>"
                                                           class="btn btn-sm btn-outline-warning"
                                                           style="margin-top:20px;">Aktivasi</a>
                                                    <?php else: ?>
                                                        <a style="font-size: 10px;"
                                                           href="<?php echo base_url('/donatur/dompetku') ?>"
                                                               class="btn btn-sm btn-outline-success">Top Up</a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if (!empty($ishide)): ?>
        <div class="top-info">
            <div class="row">
                <div class="col text-center">
                    &nbsp;
                </div>
                <?php if (!empty($ishide)): ?>
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <div class="title-font bold">
                                    Rp <?php echo number_format($_total_donasi[0]['total'], 0, ',', '.') ?></div>
                                <div class="text-muted text-smaller medium">Total Pendanaan</div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <div class="title-font bold"><?php echo number_format($_total_donasi[0]['donatur'], 0, ',', '.') ?></div>
                                <div class="text-muted text-smaller medium">Total Pemilik Dana</div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>

    <?php endif ?>

    <div class="h5 title-font bold text-center mt-20 mb-15">Pilih Cepat Pendanaan</div>
    <div class="row">
        <div class="col">
            <a href="<?php URI::baseURL(); ?>zakat" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/001-Zakat.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Zakat</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="<?php URI::baseURL(); ?>donasi" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/002-Donation.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Donasi</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="<?php URI::baseURL(); ?>wakaf" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/003-Wakaf.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Wakaf</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="<?php URI::baseURL(); ?>kurban" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/004-Kurban.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small"> Kurban</div>
                </div>
            </a>
        </div>
    </div>
    <!--    <div class="h5 title-font bold text-center mt-20 mb-15">Pilihan Pendanaan</div>-->
    <div class="row">
        <div class="col">
            <a href="<?php URI::baseURL(); ?>beasiswa" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/005-Yatim-Dhuafa.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Yatim</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="<?php URI::baseURL(); ?>asuransi" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/006-Proteksi.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Proteksi</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="<?php URI::baseURL(); ?>investasi" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/007-Investasi.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Investasi</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="#" id="more-mid-menu" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/more.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Lainnya</div>
                </div>
            </a>
        </div>
    </div>
    <?php if (!empty($lihat)): ?>
        <div class="h5 title-font bold text-center mt-20 mb-15">Pembayaran / Pembelian</div>
        <div class="row">
            <div class="col-2">
                <a href="<?php URI::baseURL(); ?>pulsa" class="link-block card">
                    <div class="card-body text-center">
                        <i class="icofont-smart-phone icofont-2x text-muted"></i>
                        <div style="font-size: 0.5rem;" class="text-center title-font semibold mt-10">Pulsa</div>
                    </div>
                </a>
            </div>
            <div class="col-2">
                <a href="<?php URI::baseURL(); ?>#" class="link-block card">
                    <div class="card-body text-center">
                        <i class="icofont-smart-phone icofont-2x text-muted"></i>
                        <div style="font-size: 0.5rem;" class="text-center title-font semibold mt-10">Paket data</div>
                    </div>
                </a>
            </div>
            <div class="col-2">
                <a href="<?php URI::baseURL(); ?>#" class="link-block card">
                    <div class="card-body text-center">
                        <i class="icofont-flash icofont-2x text-muted"></i>
                        <div style="font-size: 0.5rem;" class="text-center title-font semibold mt-10">Listrik PLN</div>
                    </div>
                </a>
            </div>
        </div>
    <?php endif; ?>
    <?php
    if (!empty($_data_main_campaign)) {
        ?>
        <div class="relative-box mt-20">
            <a href="<?php echo base_url('donasi/' . $_data_main_campaign[0]['slug']) ?>"
               class="link-block card card-donasi overflow-hidden"><?php

                $img = !empty($_data_main_campaign[0]['img']) ? $_data_main_campaign[0]['img'] : '';

                $img = URI::existsImage('campaign/thumbnails/',
                    $img,
                    'noimg_front/',
                    'no-amini.jpg');
                ?>
                <div class="bg-donasi"
                     style="background-image:url(<?php echo $img ?>);"></div>
                <div class="card-body">
                    <div class="mb-25 font-title bold judul-donasi">
                        <?php echo $_data_main_campaign[0]['title'] ?>
                        <?php
                        $imgadm = !empty($_data_main_campaign[0]['imgadm']) ? $_data_main_campaign[0]['imgadm'] : '';

                        $imgadm = URI::existsImage('admprofil/',
                            $imgadm,
                            'noimg_front/',
                            'no-amini.jpg');
                        ?>

                    </div>
                    <div class="mb-10">
                        <div class="pendana-wrapper mb-20">
                            <div class="pendana-box">
                                <div class="pendana-img title-font"
                                     style="background-image:url(<?php echo $imgadm ?>);"></div>
                                <div class="pendana-text">
                                    <div class="text-small">Program Pendanaan Dari</div>
                                    <div class="title-font semibold lh-n"> <?php echo !empty($_data_main_campaign[0]['fname']) ? $_data_main_campaign[0]['fname'] : "-"; ?></div>
                                    <div><i class="icofont-check verif-icon"></i> <span
                                                class="color-second text-small medium">Terverifikasi</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <div class="text-muted"><i class="icofont-"></i> Terkumpul</div>
                            <div class="bold">Rp <?php echo number_format($_data_main_campaign[0]['now']) ?></div>
                        </div>
                        <div class="col-5 text-right">
                            <div class="text-muted"><i class="icofont-clock"></i> Batas Waktu</div>
                            <div class="bold"><?php echo $_data_main_campaign[0]['selisih_validate'] ?> hari lagi</div>
                        </div>
                    </div>
                    <?php
                    $nomtarget = $_data_main_campaign[0]['target'];
                    $nomskr = $_data_main_campaign[0]['now'];
                    $pgbar = 0;
                    if ($nomtarget == 0) {
                        $pgbar = 0;
                    } elseif (($nomskr / $nomtarget) >= 1) {
                        $pgbar = 100;
                    } else {
                        $pgbar = ($nomskr / $nomtarget) * 100;
                    }
                    ?>
                    <div class="progress mt-5">
                        <div class="progress-bar bg-main progress-bar-striped progress-bar-animated"
                             role="progressbar" aria-valuenow="<?php echo $pgbar ?>" aria-valuemin="0"
                             aria-valuemax="100"
                             style="width: <?php echo $pgbar ?>%"></div>
                    </div>
                </div>
            </a>
            <div class="link-danai">
                <div class="row">
                    <div class="col">
                        <a href="<?php echo base_url('donasi/' . $_data_main_campaign[0]['slug']) ?>"
                           class="btn btn-block btn-info"><span>Detail</span></a>
                    </div>
                    <div class="col">
                        <?php $validate = strtotime($_data_main_campaign[0]['valid_date']);
                        if ($validate >= strtotime(date("Y-m-d"))) { ?>
                            <a href="<?php echo base_url('donasi/aksi/' . $_data_main_campaign[0]['id']); ?>"
                               class="btn btn-main form-control"><span>Danai Sekarang</span></a>
                        <?php } else { ?>
                            <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                        <?php } ?>
                    </div>
                </div>

            </div>
            <!--                <a href="danai.html" class="btn btn-main link-danai"><span>Danai Sekarang</span></a>-->
        </div>
    <?php } ?>
    <div class="text-center mt-30 mb-20">
        <div class="h5 title-font bold no-margin">Pendanaan Populer</div>
        <div>Pendanaan akan disalurkan kepada yang membutuhkan</div>
        <a href="<?php URI::baseURL(); ?>donasi" class="btn btn-main mt-10">Lihat Semua</a>
    </div>
    <?php
    if (!empty($_data_campaign_list_donasi)) {
        ?>
        <div class="owl-wrapper">
            <div class="owl-carousel owl-theme dana-populer">
                <?php
                foreach ($_data_campaign_list_donasi as $r) {
                    $img = !empty($r->img) ? $r->img : '';

                    $img = URI::existsImage('campaign/thumbnails/',
                        $img,
                        'noimg_front/',
                        'no-amini.jpg');
                    ?>
                    <?php
                    $imgadm = !empty($r->imgadm) ? $r->imgadm : '';

                    $imgadm = URI::existsImage('admprofil/',
                        $imgadm,
                        'noimg_front/',
                        'no-amini.jpg');
                    ?>
                    <?php
                    $now = !empty($r->now) ? $r->now : 0;
                    $target = !empty($r->target) ? $r->target : 0;

                    if ($target == 0) {
                        $hitung = 0;
                    } elseif (($now / $target) >= 1) {
                        $hitung = 100;
                    } else {
                        $hitung = ($now / $target) * 100;
                    }
                    ?>


                    <div class="item">
                        <div class="relative-box">
                            <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                               class="link-block card card-donasi overflow-hidden">
                                <div class="bg-donasi"
                                     style="background-image:url(<?php echo $img ?>);"></div>
                                <div class="card-body">
                                    <div class="mb-25 font-title bold judul-donasi">
                                        <?php
                                        $title = !empty($r->title) ? $r->title : "-";
                                        echo $title;
                                        ?>
                                    </div>
                                    <div class="mb-10">
                                        <div class="pendana-wrapper mb-20">
                                            <div class="pendana-box">
                                                <div class="pendana-img title-font"
                                                     style="background-image:url(<?php echo $imgadm ?>);"></div>
                                                <div class="pendana-text">
                                                    <div class="text-small">Program Pendanaan Dari</div>
                                                    <div class="title-font semibold lh-n"> <?php echo !empty($r->fname) ? $r->fname : "-"; ?></div>
                                                    <div><i class="icofont-check verif-icon"></i> <span
                                                                class="color-second text-small medium">Terverifikasi</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="text-muted"><i class="icofont-"></i> Terkumpul</div>
                                            <div class="bold">Rp <?php echo number_format($now, 0, ",", ".") ?></div>
                                        </div>
                                        <div class="col-5 text-right">
                                            <div class="text-muted"><i class="icofont-clock"></i> Batas Waktu</div>
                                            <div class="bold"><?php
                                                $selisih_validate = !empty($r->selisih_validate) ? $r->selisih_validate : 0;
                                                $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;
                                                echo $selisih_validate;
                                                ?> hari lagi
                                            </div>
                                        </div>
                                    </div>
                                    <div class="progress mt-5">
                                        <div class="progress-bar bg-main progress-bar-striped progress-bar-animated"
                                             role="progressbar" aria-valuenow="<?php echo $hitung ?>" aria-valuemin="0"
                                             aria-valuemax="100"
                                             style="width: <?php echo $hitung ?>%"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="link-danai">
                                <div class="row">
                                    <div class="col">
                                        <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                                           class="btn btn-block btn-info"><span>Detail</span></a>
                                    </div>
                                    <div class="col">
                                        <?php $validate = strtotime($r->valid_date);
                                        if ($validate >= strtotime(date("Y-m-d"))) { ?>
                                            <a href="<?php echo base_url('donasi/aksi/' . $r->id); ?>"
                                               class="btn btn-block btn-main form-control"><span>Danai Sekarang</span></a>
                                        <?php } else { ?>
                                            <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div class="text-center mt-30 mb-20">
        <div class="h5 title-font bold no-margin">Berita</div>
        <div>Berita terkini</div>
        <a href="<?php URI::baseURL(); ?>news/list" class="btn btn-main mt-10">Lihat Semua</a>
    </div>
    <?php
    if (!empty($_data_post_list)) {
        ?>

        <div class="owl-wrapper">
            <div class="owl-carousel owl-theme dana-populer">
                <?php foreach ($_data_post_list as $r) {
                    ?>
                    <?php
                    $img = '';
                    if (empty($r->img)):
                        $img = $r->img_urls;
                    else:
                        $img = !empty($r->img) ? $r->img : '';

                        $img = URI::existsImage('articles/',
                            $img,
                            'noimg_front/',
                            'no-amini.jpg');
                    endif;
                    ?>
                    <div class="item">
                        <div class="card overflow-hidden">
                            <div class="bg-donasi"
                                 style="background-image:url(<?php echo $img ?>);"></div>
                            <div class="card-body">
                                <div class="mb-25 font-title bold judul-donasi">
                                    <?php echo !empty($r->title) ? $r->title : "-"; ?>
                                </div>
                                <div class="mb-15 text-smaller">
                                    <div>
                                        <i class="text-muted icofont-user-alt-3 mr-5"></i> <?php echo $r->author ?>
                                    </div>
                                    <div>
                                        <i class="text-muted icofont-ui-calendar mr-5"></i> <?php echo date("d M Y", strtotime($r->post_date)) ?>
                                    </div>
                                </div>
                                <div>
                                    <?php
                                    echo !empty($r->desc_short) ? $r->desc_short : "-";
                                    ?>
                                </div>
                                <div class="mt-20">
                                    <a href="<?php URI::baseURL(); ?>news/<?php echo $r->slug ?>"
                                       class="btn btn-block btn-main"><span>Selengkapnya</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    {_layFooter}
