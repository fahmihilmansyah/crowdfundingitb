{_layHeader}
<?php

$this->load->model(array('home/Home_model' => "home"));
?>


<!-- Slider -->
<header style="background-image:url(https://images.unsplash.com/photo-1488521787991-ed7bbaae773c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80);"
        class="valign-wrapper">
    <div class="valign-box">
        <div class="container text-center">
            <h1 class="title-font bold">
                Selamat Datang di <br class="d-none d-md-block">
                Ganesha Bisa
            </h1>
            <p class="mt-20">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Si qua in iis corrigere voluit, deteriora
                fecit. Idque testamento cavebit is, qui nobis quasi oraculum ediderit nihil post mortem ad nos
                pertinere? Nam de isto magna dissensio est. Re mihi non aeque satisfacit, et quidem locis pluribus. At
                iste non dolendi status non vocatur voluptas. Duo Reges: constructio interrete. Quo modo autem
                philosophus loquitur?
            </p>
        </div>
    </div>
</header>

<div class="top-info">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card full-height">
                    <div class="card-body lg-pad full-height">
                        <div class="top-info-row full-height">
                            <div class="top-info-icon full-height valign-wrapper">
                                <div class="valign-box">
                                    <i class="icofont-diamond icofont-4x color-main"></i>
                                </div>
                            </div>
                            <div class="top-info-text full-height valign-wrapper">
                                <div class="valign-box">
                                    <div class="top-info-number bold title-font color-second"><span
                                                class="text-muted text-small">Rp</span> <?php echo number_format($_total_donasi[0]['total'], 0, ',', '.') ?>
                                    </div>
                                    <div class="text-muted medium">Total Pendanaan</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card full-height">
                    <div class="card-body lg-pad full-height">
                        <div class="top-info-row full-height">
                            <div class="top-info-icon full-height valign-wrapper">
                                <div class="valign-box">
                                    <i class="icofont-user-alt-4 icofont-4x color-main"></i>
                                </div>
                            </div>
                            <div class="top-info-text full-height valign-wrapper">
                                <div class="valign-box">
                                    <div class="top-info-number bold title-font color-second"><?php echo number_format($_total_donasi[0]['donatur'], 0, ',', '.') ?></div>
                                    <div class="text-muted medium">Pemberi Dana</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container">
        <div class="text-center h3 bold title-font mb-25 color-second">Pilihan Pendanaan</div>
        <div class="row">
            <div class="col-md-4">
                <a class="card link-block hoverable to-main-bg" href="donasi.html">
                    <div class="card-body lg-pad text-center">
                        <i class="icofont-gift icofont-7x"></i>
                        <div class="mt-20 title-font semibold h5">Donasi</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="card link-block hoverable to-main-bg" href="beasiswa.html">
                    <div class="card-body lg-pad text-center">
                        <i class="icofont-graduate-alt icofont-7x"></i>
                        <div class="mt-20 title-font semibold h5">Beasiswa</div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="card link-block hoverable to-main-bg" href="investasi.html">
                    <div class="card-body lg-pad text-center">
                        <i class="icofont-leaf icofont-7x"></i>
                        <div class="mt-20 title-font semibold h5">Investasi</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<?php
if (!empty($_data_main_campaign)) {
    ?>
    <section class="bg-grey">
        <div class="container">
            <div class="card overflow-hidden">
                <div class="row no-gutters">
                    <div class="col-md-5">
                        <?php

                        $img = !empty($_data_main_campaign[0]['img']) ? $_data_main_campaign[0]['img'] : '';

                        $img = URI::existsImage('campaign/thumbnails/',
                            $img,
                            'noimg_front/',
                            'no-amini.jpg');
                        ?>
                        <div class="bg-donasi"
                             style="background-image:url(<?php echo $img ?>);"></div>
                    </div>
                    <div class="col-md-7">
                        <div class="card-body">
                            <h5><span class="badge bg-second">Populer</span></h5>
                            <div class="mt-25 mb-25 font-title bold judul-donasi-big">
                                <?php echo $_data_main_campaign[0]['title'] ?>
                            </div>
                            <div class="mb-10"><i class="icofont-user-alt-4 mr-5 text-muted"></i>
                                <?php echo $_data_main_campaign[0]['name_userpost'] ?> </div>
                            <div class="row">
                                <div class="col-7">
                                    <div class="text-muted">Terkumpul</div>
                                    <div class="bold">Rp
                                        <?php echo number_format($_data_main_campaign[0]['now']) ?></div>
                                </div>
                                <div class="col-5 text-right">
                                    <div class="text-muted">Batas Waktu</div>
                                    <div class="bold">
                                        <?php echo $_data_main_campaign[0]['selisih_validate'] ?> hari lagi
                                    </div>
                                </div>
                            </div>

                            <?php
                            $nomtarget = $_data_main_campaign[0]['target'];
                            $nomskr = $_data_main_campaign[0]['now'];
                            $pgbar = 0;
                            if($nomskr > 0){
                                $pgbar = $nomtarget / $nomskr;
                            }
                            ?>
                            <div class="progress mt-5">
                                <div class="progress-bar bg-main progress-bar-striped progress-bar-animated"
                                     role="progressbar" aria-valuenow="<?php echo $pgbar ?>" aria-valuemin="0"
                                     aria-valuemax="100" style="width: <?php echo $pgbar ?>%"></div>
                            </div>
                            <div class="text-right mt-20">
                                <a href="detail-donasi.html" class="btn btn-light"><span>Lihat Detail</span></a>
<!--                                <a href="danai.html" class="btn btn-main"><span>Danai Sekarang</span></a>-->

                                <?php $validate = strtotime($_data_main_campaign[0]['valid_date']);
                                if ($validate >= strtotime(date("Y-m-d"))) { ?>
                                    <a href="<?php echo base_url('donasi/aksi/' . $_data_main_campaign[0]['id']); ?>"
                                       class="btn btn-block btn-main"><span>Danai Sekarang</span></a>
                                <?php } else { ?>
                                    <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php } ?>

<section>
    <div class="container">
        <div class="row mb-25">
            <div class="col-md-9">
                <div class="h3 bold title-font lh-1 color-second">Pendanaan Populer</div>
                <div class="text-muted mb-10">Pendanaan akan disalurkan kepada yang membutuhkan</div>
            </div>
            <div class="col-md-3 text-right">
                <a href="<?php URI::baseURL(); ?>donasi" class="btn btn-main"><span>Lihat Semua</span></a>
            </div>
        </div>
        <ul class="nav nav-pills populer-tab mb-20" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-donasi-tab" data-toggle="pill" href="#pills-donasi" role="tab"
                   aria-controls="pills-donasi" aria-selected="true">Donasi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-beasiswa-tab" data-toggle="pill" href="#pills-beasiswa" role="tab"
                   aria-controls="pills-beasiswa" aria-selected="false">Beasiswa</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-investasi-tab" data-toggle="pill" href="#pills-investasi" role="tab"
                   aria-controls="pills-investasi" aria-selected="false">Investasi</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-donasi" role="tabpanel" aria-labelledby="pills-donasi-tab">
                <div class="row">
                    <?php if (!empty($_data_campaign_list_donasi)) {
                        foreach ($_data_campaign_list_donasi as $r) {
                            ?>
                            <?php

                            $img = !empty($r->img) ? $r->img : '';

                            $img = URI::existsImage('campaign/thumbnails/',
                                $img,
                                'noimg_front/',
                                'no-amini.jpg');
                            ?>
                            <?php
                            $now = !empty($r->now) ? $r->now : 0;
                            $target = !empty($r->target) ? $r->target : 0;

                            if ($target == 0) {
                                $hitung = 0;
                            } elseif (($now / $target) >= 1) {
                                $hitung = 100;
                            } else {
                                $hitung = ($now / $target) * 100;
                            }
                            ?>
                            <div class="col-md-4">
                                <div class="card overflow-hidden" style="height: 100%;">
                                    <div class="bg-donasi" style="background-image:url(<?php echo $img ?>);"></div>
                                    <div class="card-body">
                                        <div class="mb-25 font-title bold judul-donasi">
                                            <?php
                                            $title = !empty($r->title) ? $r->title : "-";
                                            echo $title;
                                            ?>
                                        </div>
                                        <div class="mb-10"><i class="icofont-user-alt-4 mr-5 text-muted"></i> <?php
                                            $name_userpost = !empty($r->name_userpost) ? $r->name_userpost : "-";
                                            echo $r->name_userpost;
                                            ?></div>
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="text-muted"><i class="icofont-"></i> Terkumpul</div>
                                                <div class="bold">
                                                    Rp <?php echo number_format($now, 0, ",", ".") ?></div>
                                            </div>
                                            <div class="col-5 text-right">
                                                <div class="text-muted"><i class="icofont-clock"></i> Batas Waktu</div>
                                                <div class="bold"><?php
                                                    $selisih_validate = !empty($r->selisih_validate) ? $r->selisih_validate : 0;
                                                    $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;
                                                    echo $selisih_validate;
                                                    ?> hari lagi
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress mt-5">
                                            <div class="progress-bar bg-main progress-bar-striped progress-bar-animated"
                                                 role="progressbar" aria-valuenow="<?php echo $hitung ?>"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: <?php echo $hitung ?>%"></div>
                                        </div>
                                        <div class="mt-20">
                                            <div class="row">
                                                <div class="col-6">
                                                    <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                                                       class="btn btn-block btn-light"><span>Lihat Detail</span></a>
                                                </div>
                                                <div class="col-6">

                                                    <?php $validate = strtotime($r->valid_date);
                                                    if ($validate >= strtotime(date("Y-m-d"))) { ?>
                                                        <a href="<?php echo base_url('donasi/aksi/' . $r->id); ?>"
                                                           class="btn btn-block btn-main"><span>Danai Sekarang</span></a>
                                                    <?php } else { ?>
                                                        <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    } ?>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-beasiswa" role="tabpanel" aria-labelledby="pills-beasiswa-tab">
                <div class="row">
                    <?php if (!empty($_data_campaign_list_beasiswa)) {
                        foreach ($_data_campaign_list_beasiswa as $r) {
                            ?>
                            <?php

                            $img = !empty($r->img) ? $r->img : '';

                            $img = URI::existsImage('campaign/thumbnails/',
                                $img,
                                'noimg_front/',
                                'no-amini.jpg');
                            ?>
                            <?php
                            $now = !empty($r->now) ? $r->now : 0;
                            $target = !empty($r->target) ? $r->target : 0;

                            if ($target == 0) {
                                $hitung = 0;
                            } elseif (($now / $target) >= 1) {
                                $hitung = 100;
                            } else {
                                $hitung = ($now / $target) * 100;
                            }
                            ?>
                            <div class="col-md-4">
                                <div class="card overflow-hidden" style="height: 100%;">
                                    <div class="bg-donasi" style="background-image:url(<?php echo $img ?>);"></div>
                                    <div class="card-body">
                                        <div class="mb-25 font-title bold judul-donasi">
                                            <?php
                                            $title = !empty($r->title) ? $r->title : "-";
                                            echo $title;
                                            ?>
                                        </div>
                                        <div class="mb-10"><i class="icofont-user-alt-4 mr-5 text-muted"></i> <?php
                                            $name_userpost = !empty($r->name_userpost) ? $r->name_userpost : "-";
                                            echo $r->name_userpost;
                                            ?></div>
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="text-muted"><i class="icofont-"></i> Terkumpul</div>
                                                <div class="bold">
                                                    Rp <?php echo number_format($now, 0, ",", ".") ?></div>
                                            </div>
                                            <div class="col-5 text-right">
                                                <div class="text-muted"><i class="icofont-clock"></i> Batas Waktu</div>
                                                <div class="bold"><?php
                                                    $selisih_validate = !empty($r->selisih_validate) ? $r->selisih_validate : 0;
                                                    $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;
                                                    echo $selisih_validate;
                                                    ?> hari lagi
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress mt-5">
                                            <div class="progress-bar bg-main progress-bar-striped progress-bar-animated"
                                                 role="progressbar" aria-valuenow="<?php echo $hitung ?>"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: <?php echo $hitung ?>%"></div>
                                        </div>
                                        <div class="mt-20">
                                            <div class="row">
                                                <div class="col-6">
                                                    <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                                                       class="btn btn-block btn-light"><span>Lihat Detail</span></a>
                                                </div>
                                                <div class="col-6">

                                                    <?php $validate = strtotime($r->valid_date);
                                                    if ($validate >= strtotime(date("Y-m-d"))) { ?>
                                                        <a href="<?php echo base_url('donasi/aksi/' . $r->id); ?>"
                                                           class="btn btn-block btn-main"><span>Danai Sekarang</span></a>
                                                    <?php } else { ?>
                                                        <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    } ?>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-investasi" role="tabpanel" aria-labelledby="pills-investasi-tab">
                <div class="row">
                    <?php if (!empty($_data_campaign_list_investasi)) {
                        foreach ($_data_campaign_list_investasi as $r) {
                            ?>
                            <?php

                            $img = !empty($r->img) ? $r->img : '';

                            $img = URI::existsImage('campaign/thumbnails/',
                                $img,
                                'noimg_front/',
                                'no-amini.jpg');
                            ?>
                            <?php
                            $now = !empty($r->now) ? $r->now : 0;
                            $target = !empty($r->target) ? $r->target : 0;

                            if ($target == 0) {
                                $hitung = 0;
                            } elseif (($now / $target) >= 1) {
                                $hitung = 100;
                            } else {
                                $hitung = ($now / $target) * 100;
                            }
                            ?>
                            <div class="col-md-4">
                                <div class="card overflow-hidden">
                                    <div class="bg-donasi" style="background-image:url(<?php echo $img ?>);"></div>
                                    <div class="card-body">
                                        <div class="mb-25 font-title bold judul-donasi">
                                            <?php
                                            $title = !empty($r->title) ? $r->title : "-";
                                            echo $title;
                                            ?>
                                        </div>
                                        <div class="mb-10"><i class="icofont-user-alt-4 mr-5 text-muted"></i> <?php
                                            $name_userpost = !empty($r->name_userpost) ? $r->name_userpost : "-";
                                            echo $r->name_userpost;
                                            ?></div>
                                        <div class="row">
                                            <div class="col-7">
                                                <div class="text-muted"><i class="icofont-"></i> Terkumpul</div>
                                                <div class="bold">
                                                    Rp <?php echo number_format($now, 0, ",", ".") ?></div>
                                            </div>
                                            <div class="col-5 text-right">
                                                <div class="text-muted"><i class="icofont-clock"></i> Batas Waktu</div>
                                                <div class="bold"><?php
                                                    $selisih_validate = !empty($r->selisih_validate) ? $r->selisih_validate : 0;
                                                    $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;
                                                    echo $selisih_validate;
                                                    ?> hari lagi
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress mt-5">
                                            <div class="progress-bar bg-main progress-bar-striped progress-bar-animated"
                                                 role="progressbar" aria-valuenow="<?php echo $hitung ?>"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: <?php echo $hitung ?>%"></div>
                                        </div>
                                        <div class="mt-20">
                                            <div class="row">
                                                <div class="col-6">
                                                    <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                                                       class="btn btn-block btn-light"><span>Lihat Detail</span></a>
                                                </div>
                                                <div class="col-6">
                                                    <?php $validate = strtotime($r->valid_date);
                                                    if ($validate >= strtotime(date("Y-m-d"))) { ?>
                                                        <a href="<?php echo base_url('donasi/aksi/' . $r->id); ?>"
                                                           class="btn btn-block btn-main"><span>Danai Sekarang</span></a>
                                                    <?php } else { ?>
                                                        <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- PROGRAM CAMPAIGN -->


<section class="bg-grey">
    <div class="container">
        <div class="row mb-25">
            <div class="col-md-9">
                <div class="h3 bold title-font lh-1 color-second">Artikel Terbaru</div>
                <div class="text-muted mb-10">Berita terkini seputar ITB</div>
            </div>
            <div class="col-md-3 text-right">
                <a href="<?php URI::baseURL(); ?>news/list" class="btn btn-main"><span>Lihat Semua</span></a>
            </div>
        </div>
        <div class="row">
            <?php
            if (!empty($_data_post_list)) {
                foreach ($_data_post_list as $r) {
                    ?>
                    <?php

                    $img = !empty($r->img) ? $r->img : '';

                    $img = URI::existsImage('articles/thumbnails/',
                        $img,
                        'noimg_front/',
                        'no-amini.jpg');
                    ?>
                    <div class="col-md-4">
                        <div class="card overflow-hidden" style="height: 100%;">
                            <div class="bg-donasi"
                                 style="background-image:url(<?php echo $img ?>);"></div>
                            <div class="card-body">
                                <div class="mb-25 font-title bold judul-donasi">
                                    <?php echo !empty($r->title) ? $r->title : "-"; ?>
                                </div>
                                <div class="mb-15 text-smaller">
                                    <div>
                                        <i class="text-muted icofont-user-alt-3 mr-5"></i> <?php echo $r->author ?>
                                    </div>
                                    <div>
                                        <i class="text-muted icofont-ui-calendar mr-5"></i> <?php echo date("d M Y",strtotime($r->post_date))?>
                                    </div>
                                </div>
                                <div>
                                    <?php
                                    echo !empty($r->desc_short) ? $r->desc_short : "-";
                                    ?>
                                </div>
                                <div class="mt-20">
                                    <a href="<?php URI::baseURL(); ?>news/<?php echo $r->slug ?>"
                                       class="btn btn-block btn-main"><span>Selengkapnya</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</section>

<!-- NEWS UPDATE -->

{_layFooter}

<script type="text/javascript">
    select2_icon("program_menu");
</script>