<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Frame extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_modList = array('home/Home_model' => "home",
        'shadaqah/Shadaqah_model' => "shadaqah",
        'donatur/donatur_model' => "mdl_donatur"
    );

    private $def;

    protected $_data = array();
    private $_SESSION;


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();
        $this->_SESSION = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
            $this->session->userdata(LGI_KEY . "login_info") : '';

        $this->iddonaturs = empty($this->_SESSION['userid']) ? '' : $this->_SESSION['userid'];

        $kondisi['where'] = array(

            'nb_donaturs_d.donatur' => $this->iddonaturs,

        );
        $kondisi['join'] = array('table' => 'nb_donaturs', 'condition' => 'nb_donaturs_d.donatur = nb_donaturs.id', 'type' => 'left');

        $this->def = empty($this->_SESSION)?null:$this->mdl_donatur->custom_query('nb_donaturs_d', $kondisi)->result();

    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);

    }


    public function index($data = array())

    {
        $ses = $this->session->userdata(LGI_KEY . "login_info");
//	    echo LGI_KEY."<br>";
//	    print_r($ses);
//	    exit;
        $data = $this->_data;
        $data['indexs'] = $this->def;
        $data['_total_donasi'] = $this->home->getTotalDonasi()->result_array();
        $data["_data_carousel"] = $this->home->getDataSlide();
        $data["_data_main_campaign"] = $this->home->getDataCMain();
        // ----------------------------------------------------------------------------------------------------
        // STATIC PAGE DATE
        // ----------------------------------------------------------------------------------------------------
        $HEADER = array('TOKEN-KEY: DDTEKNOPAYMENT', 'APP-KEY: APPKEYDDTEKNO', 'token-login: ' . $ses['mupays_token']);
        $datasaldo = Fhhlib::_curl_exexc_with_header($this->_data['mupays_url'] . "/mupays/transaksi/getsaldo", null, 'GET', $HEADER);
        $datasaldo = json_decode($datasaldo, true);
        $data['saldo_user'] = null;
        if ($datasaldo['rc'] == '0000') {
            $data['saldo_user'] = $datasaldo['data'];
        }
//		$spagedata              = $this->shadaqah->getDataShadaq()->num_rows();
//		$data["spage_shadaq"]   = ($spagedata > 0) ? $this->shadaqah->getDataShadaq()->row_array() : array();
//
//		$spagedata              = $this->shadaqah->getDataInfaq()->num_rows();
//		$data["spage_infaq"]    = ($spagedata > 0) ? $this->shadaqah->getDataInfaq()->row_array() : array();
//
//		$spagedata              = $this->shadaqah->getDataZakat()->num_rows();
//		$data["spage_zakat"]    = ($spagedata > 0) ? $this->shadaqah->getDataZakat()->row_array() : array();
        // ----------------------------------------------------------------------------------------------------
        $data['_data_campaign_list_donasi'] = !empty($this->home->getCampaignKategori('', 5)->result()) ? $this->home->getCampaignKategori('', 5)->result() : [];
//        $data['_data_campaign_list_beasiswa'] = !empty($this->home->getCampaignKategori(2,5)->result()) ?$this->home->getCampaignKategori(2,5)->result():[];
//        $data['_data_campaign_list_investasi'] = !empty($this->home->getCampaignKategori(3,5)->result()) ?$this->home->getCampaignKategori(3,5)->result():[];
        $data["_data_campaign_list"] = !empty($this->home->getDataCList()->result()) ? $this->home->getDataCList()->result() : array();
        $data["_data_post_list"] = $this->home->getDataPList();

        $this->parser->parse("index", $data);

    }

}

