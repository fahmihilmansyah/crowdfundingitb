<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class pembayaran extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : No Reply
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */

    private $_modList = array(
        "aktivasiwallet/Kurban_model" => "mdl_donatur",
        "ppob/kurban_model"=>'donasimdl'
    );

    protected $_data = array();


    private $def;

    private $iddonaturs;

    private $_SESSION;

    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

    }


    private function modelsApp()

    {
//        $this->load->model($this->_modList);
        $this->load->library('Fhhlib');

    }


    public function index()
    {
        $data['isnodepo'] = !empty($_GET['isnodepo']) &&$_GET['isnodepo']=='true'?true:false ;
//        $data = $this->_data;
//        print_r($data);exit;
        return $this->load->view("index",$data);
    }

}

