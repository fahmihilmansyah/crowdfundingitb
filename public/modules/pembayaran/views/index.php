<?php
/**
 * Created by PhpStorm.
 * Project : berzakat
 * User: fahmihilmansyah
 * Date: 31/05/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 22.30
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$sessionsl = !empty($this->session->userdata(LGI_KEY . "login_info")) ? $this->session->userdata(LGI_KEY . "login_info")['email'] : '';
?>
<style>
    .ul {
        display: block;
        list-style-type: none;
    }

    .li {

        border-bottom: 1px solid black;
    }
    .rounded-right{
        border-bottom-right-radius: 1.25rem!important;
    }
</style>
<div class="accordion" id="accordionExample">
<?php if(!empty($_GET['pgtrx']) && !empty($_GET['jtrx'])): ?>
    <div class="">
        <div style=" margin-top: 5px; background-color: #1f7c4d; color: white; padding: 5px;"  data-toggle="collapse" data-target="#emoneys" >Pilih Pembayaran <i class="icofont icofont-caret-down float-right"></i></div>
        <div class="row collapse" data-parent="#accordionExample" id="emoneys">
            <?php
            $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>$_GET['jtrx'],'kode_va'=>$_GET['pgtrx']])->get()->result();
            foreach ($carilist as $r):
                ?>
                <div class="col-12 pilmetbyr">
                    <div class="row align-items-center" style="padding: 10px; cursor: pointer;">
                        <div class="col-3">
                            <?php if (!empty($r->img)): ?>
                                <img style="max-height: 60px !important;" width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                            <?php endif; ?>
                        </div>
                        <div class="col-6"><span class="nmbyr"> <?php echo $r->name ?> </span></div>
                        <div class="col-3">
                            <input name="tmpbyrs"  class="float-right nmdpay" value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>" type="radio">
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php else: ?>
    <div class="">
<div class="rounded-right" style="background-color: #1f7c4d; color: white; padding: 5px;  margin-top: 5px;"  data-toggle="collapse" data-target="#qriss" >QRIS <i class="icofont icofont-caret-down float-right"></i></div>
<div class="row collapse" data-parent="#accordionExample" id="qriss">
    <?php
    $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'qris'])->get()->result();
    foreach ($carilist as $r):
    ?>
    <div class="col-12 pilmetbyr">
        <div class="row align-items-center" style="padding: 10px; cursor: pointer;">
            <div class="col-3">
                <?php if (!empty($r->img)): ?>
                    <img style="max-height: 60px !important;" width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                <?php endif; ?>
            </div>
            <div class="col-6"><span class="nmbyr"><?php echo $r->name ?></span></div>
            <div class="col-3">
                <input name="tmpbyrs"  class="float-right nmdpay" value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>" type="radio">
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
    </div>
    <div class="">
<div class="rounded-right" style=" margin-top: 5px; background-color: #1f7c4d; color: white; padding: 5px;"  data-toggle="collapse" data-target="#emoneys" >E-Money <i class="icofont icofont-caret-down float-right"></i></div>
<div class="row collapse" data-parent="#accordionExample" id="emoneys">
    <?php
    $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'emoney'])->get()->result();
    foreach ($carilist as $r):
        ?>
        <div class="col-12 pilmetbyr">
            <div class="row align-items-center" style="padding: 10px; cursor: pointer;">
                <div class="col-3">
                    <?php if (!empty($r->img)): ?>
                        <img style="max-height: 60px !important;" width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                    <?php endif; ?>
                </div>
                <div class="col-6"><span class="nmbyr"> <?php echo $r->name ?> </span></div>
                <div class="col-3">
                    <input name="tmpbyrs"  class="float-right nmdpay" value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>" type="radio">
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
    </div>
    <div class="">
<div class="rounded-right" style=" margin-top: 5px; background-color: #1f7c4d; color: white; padding: 5px;"  data-toggle="collapse" data-target="#vtbs">Transfer Bank (Virtual Account) <i class="icofont icofont-caret-down float-right"></i></div>
<div class="row collapse" data-parent="#accordionExample" id="vtbs">
    <?php
    $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'va'])->get()->result();
    foreach ($carilist as $r):
    ?>
    <div class="col-12 pilmetbyr">
        <div class="row align-items-center" style="padding: 10px; cursor: pointer;">
            <div class="col-3">
                <?php if (!empty($r->img)): ?>
                    <img style="max-height: 60px !important;" width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                <?php endif; ?>
            </div>
            <div class="col-6"> <span class="nmbyr"><?php echo $r->name ?></span></div>
            <div class="col-3">
                <input name="tmpbyrs"  class="float-right nmdpay" value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>" type="radio">
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
    </div>
    <div class="">
<div class="rounded-right" style=" margin-top: 5px; background-color: #1f7c4d; color: white; padding: 5px;"  data-toggle="collapse" data-target="#vst">Setor Tunai <i class="icofont icofont-caret-down float-right"></i></div>
<div class="row collapse" data-parent="#accordionExample" id="vst">
    <?php
    $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'retail'])->get()->result();
    foreach ($carilist as $r):
    ?>
    <div class="col-12 pilmetbyr">
        <div class="row align-items-center" style="padding: 10px; cursor: pointer;">
            <div class="col-3">
                <?php if (!empty($r->img)): ?>
                    <img style="max-height: 60px !important;" width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                <?php endif; ?>
            </div>
            <div class="col-6"> <span class="nmbyr"><?php echo $r->name ?></span></div>
            <div class="col-3">
                <input name="tmpbyrs"  class="float-right nmdpay" value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>" type="radio">
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
    </div>
    <div class="">
        <div class="rounded-right" style="margin-top:5px; background-color: #1f7c4d; color: white; padding: 5px;" data-toggle="collapse" data-target="#dompet">Saldo <i class="icofont icofont-caret-down float-right"></i></div>
        <div class="row collapse" data-parent="#accordionExample" id="dompet">


            <?php
            $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'dompet'])->get()->result();
            foreach ($carilist as $r):
                ?>
                <?php if($r->kode_va == 'MUPAYS' && empty($sessionsl) && (!$isnodepo)): ?>
                <a href="<?php echo base_url('/register')?>" style="    text-decoration: none;
    color: black;" class="col-12 ">
                    <div class="row align-items-center" style="padding: 10px; cursor: pointer;">
                        <div class="col-3">
                            <?php if (!empty($r->img)): ?>
                                <img style="max-height: 60px !important;" width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                            <?php endif; ?>
                        </div>
                        <div class="col-6"><span class="nmbyr"> <?php echo $r->name ?> </span></div>
                        <div class="col-3">
                            <input name="tmpbyrs"  class="float-right nmdpay" value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>" type="radio">
                        </div>
                    </div>
                </a>
                <?php else: ?>
                <div class="col-12 pilmetbyr">
                    <div class="row align-items-center" style="padding: 10px; cursor: pointer;">
                        <div class="col-3">
                            <?php if (!empty($r->img)): ?>
                                <img style="max-height: 60px !important;" width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                            <?php endif; ?>
                        </div>
                        <div class="col-6"><span class="nmbyr"> <?php echo $r->name ?> </span></div>
                        <div class="col-3">
                            <input name="tmpbyrs"  class="float-right nmdpay" value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>" type="radio">
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
</div>

<div class="row m-1">
    <div class="col">
        <div style="display: none;" id="metterpils" class="col-12 bg-secondary rounded p-2">
            <div class="row text-white align-items-center">
                <div class="col-3" style="">
                    <img id="logobnk" class="bg-white rounded p-1" src="#" style="width: 80px; display: none;">
                </div>
                <div class="col-6" style="">
                    <span id="namapembayran"></span>
                </div>
                <div class="col-3 " style=""> &nbsp;
                    <label class="">Terpilih</label>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".pilmetbyr").on('click',function () {
       var foto = $(this).find('img').attr('src');
       var nmbyr = $(this).find('.nmbyr').html();
       var nmdpay = $(this).find('.nmdpay').val();

        $("input[name='tmpbyrs'][value='" + nmdpay + "']").prop('checked', true);
       if(foto){
           $("#logobnk").show();
       }else{
           $("#logobnk").hide();
       }
        $("#namapembayran").html(nmbyr);
        $("#logobnk").attr('src',foto);
        $("#paymetod").val(nmdpay);
        $("#pilbayar").attr('data-paymetod',nmdpay);
        $('.nav-menu2').removeClass('open');
        $("#metterpils").show();
        $('.collapse').collapse('hide')
    })

    var valdt = $("#pilbayar").attr('data-paymetod');
    if(valdt){
        $("input[name='tmpbyrs'][value='" + valdt + "']").prop('checked', true);
    }

    <?php if(!empty($_GET['pgtrx']) && $_GET['pgtrx']):?>
    $(".pilmetbyr").click();
    <?php endif;?>
</script>