<?php 
/**
* 
*/
class Shadaqah_model extends CI_Model
{
	function getDataShadaq()
	{
		$this->db->select("*");
		$this->db->from("spage_program");
		$this->db->where(array("tipe"=>"shadaqah", "is_active" => "1"));

		return $this->db->get();
	}

	function getDataInfaq()
	{
		$this->db->select("*");
		$this->db->from("spage_program");
		$this->db->where(array("tipe"=>"infaq", "is_active" => "1"));

		return $this->db->get();
	}

	function getDataZakat()
	{
		$this->db->select("*");
		$this->db->from("spage_program");
		$this->db->where(array("tipe"=>"zakat", "is_active" => "1"));

		return $this->db->get();
	}
}
