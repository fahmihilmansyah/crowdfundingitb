<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_model extends CI_Model {
	public function data_parent()
	{
		$this->db->select('*');
		$this->db->from('faq_parent');
		$this->db->order_by("display_first","desc");
		
		$query = $this->db->get();
		
		return $query;
	}

	public function getHeaderText($key = array())
	{
		$this->db->select("title");
		$this->db->where($key);
		return $this->db->get("faq_parent")->row();
	}

	public function data_child($key = array()){
		$this->db->select('faq_parent.title as parent_title, faq_child.*');
		$this->db->from('faq_parent');
		$this->db->join('faq_child', 'faq_parent.id = faq_child.parent');
		$this->db->order_by("faq_parent.title","asc");
		$this->db->where($key);

		$query = $this->db->get();
		
		return $query;
	}

	public function getKeySearch()
	{
		$this->db->select("id, key_search");
		return $this->db->get('faq_child');
	}

	function getkey()
    {
        $query = $this->db->get('faq_child');
        return $query->result();
    }


}