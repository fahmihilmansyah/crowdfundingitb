<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends NBFront_Controller {

	/**
	 * ----------------------------------------
	 * #NB Controller
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_modList   = array('Faq_model' => 'faq',);

	protected $_data    = array();

	function __construct()
	{
		parent::__construct();

		// -----------------------------------
		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN
		// -----------------------------------
		 $this->modelsApp();
	}

	private function modelsApp()
	{		
		$this->load->model($this->_modList);
	}

	public function index($data = array())
	{
		$data 			 	= $this->_data;

		$data["_content"] 	= $this->parser->parse('donatur/overview/index', $data, TRUE);
		
		
		$data["data_parent"] = !empty($this->faq->data_parent()->result_array()) ? 
									  $this->faq->data_parent()->result_array() : array() ;

		$key 				= array("display_first"=>"1");

		$datachild 			= !empty($this->faq->data_child($key)->result_array()) ? 
									  $this->faq->data_child($key)->result_array() : array() ;

		$data["faq"]["title"]	= !empty($this->faq->getHeaderText($key)) ? 
												 $this->faq->getHeaderText($key)->title : "PANDUAN UMUM";

		$data["faq"]["desc"] 	= $datachild;


		$this->parser->parse("index",$data);
	}

	public function getFaq()
	{
		$parent = !empty($this->input->get("parent")) ? $this->input->get("parent") : 0;

		$key 				= array("id" => $parent);

		$title  			= !empty($this->faq->getHeaderText($key)) ? 
					   				 $this->faq->getHeaderText($key)->title : "PANDUAN UMUM";

		$key 				= array("parent" => $parent);

		$datachild 			= !empty($this->faq->data_child($key)->result_array()) ? 
									 $this->faq->data_child($key)->result_array() : array();

		// echo $this->db->last_query(); exit();

		$html = '<div class="konten-faq" 
					  id="kontenshow' . $parent . '">';

		$html .= '<h3 class="panduan-umum">'. $title . '</h3>';

		if(!empty($datachild))
		{
			foreach ($datachild as $key => $row_c) {
				   $html .= '<div class="panel-group" id="accordion">
								<div class="panel panel-default no-border">
									<div class="panel-heading no-border">
										<h4 class="panel-title">
										<a 
											class="accordion-toggle" 
											data-toggle="collapse" 
											data-parent="#accordion" 
											href="#collapse<?php echo $key ?>"
										>
										  <b>'. $row_c["title"] . '</b>
										</a>
										</h4>
									</div>
									<div id="collapse<?php echo $key ?>" class="panel-collapse collapse in">
										  <div class="panel-body no-border bb">' .
											$row_c["description"]
										. '</div>
									</div>
								</div>
							</div>';
			}
		}else{
			$html .= "<div class='alert alert-warning'>Data yang anda cari tidak ditemukan</div>";
		}

		$data = array( "html" => $html , "crumbs_search" => $title);

		echo json_encode($data);
	}

	public function getFaqKeySearch()
	{
		$key_search = !empty($this->input->get("key_search")) ? $this->input->get("key_search") : 0;
		
		$key 				= array("key_search" => $key_search);

		$datachild 			= !empty($this->faq->data_child($key)->row_array()) ? 
									 $this->faq->data_child($key)->row_array() : array();

		// echo $this->db->last_query(); exit();

		$textid = (!empty($datachild["id"]) ? $datachild["id"] : 0);
		$html = '<div class="konten-faq" 
					  id="kontenshow' . $textid . '">';

		$texttitle = (!empty($datachild["parent_title"]) ? $datachild['parent_title'] : 'HASIL PENCARIAN KOSONG');
		$html .= '<h3 class="panduan-umum">'. $texttitle . '</h3>';

		if(!empty($datachild))
		{
				   $html .= '<div class="panel-group" id="accordion">
								<div class="panel panel-default no-border">
									<div class="panel-heading no-border">
										<h4 class="panel-title">
										<a 
											class="accordion-toggle" 
											data-toggle="collapse" 
											data-parent="#accordion" 
											href="#collapse<?php echo $key ?>"
										>
										  <b>'. $datachild["title"] . '</b>
										</a>
										</h4>
									</div>
									<div id="collapse<?php echo $key ?>" class="panel-collapse collapse in">
										  <div class="panel-body no-border bb">' .
											$datachild["description"]
										. '</div>
									</div>
								</div>
							</div>';
		}else{
			$html .= "<div class='alert alert-warning'>Data yang anda cari tidak ditemukan</div>";
		}

		$data = array( "html" => $html , "crumbs_search" => $texttitle);

		echo json_encode($data);
	}
	
	public function search(){
		$query  = $this->faq->getkey();
        $data = array();
        foreach ($query as $key => $value) 
        {
            $data[] = array('id_child' => $value->id, 'key_search' => $value->key_search);
        }
        echo json_encode($data);
	}

	public function getAutoComplete()
	{
		$result = $this->faq->getkey();

		$data = array();
		$i = 0;
		foreach ($result as $key => $value) {
			$data[$i]["name"] = $value->key_search;
			$data[$i]["code"] = $value->id;
			$i++;
		}

		echo json_encode($data);
	}
}
