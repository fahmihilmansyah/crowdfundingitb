<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Home extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_modList = array('home/Home_model' => "home",
        'shadaqah/Shadaqah_model' => "shadaqah",
        'donatur/donatur_model' => "mdl_donatur"
    );

    private $def;

    protected $_data = array();
    private $_SESSION;


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();
        $this->_SESSION = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
            $this->session->userdata(LGI_KEY . "login_info") : '';

        $this->iddonaturs = empty($this->_SESSION['userid']) ? '' : $this->_SESSION['userid'];

        $kondisi['where'] = array(

            'nb_donaturs_d.donatur' => $this->iddonaturs,

        );
        $kondisi['join'] = array('table' => 'nb_donaturs', 'condition' => 'nb_donaturs_d.donatur = nb_donaturs.id', 'type' => 'left');

        $this->def = empty($this->_SESSION)?null:$this->mdl_donatur->custom_query('nb_donaturs_d', $kondisi)->result();

    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);

    }


    public function index($data = array())

    {
        $ses = $this->session->userdata(LGI_KEY . "login_info");
//	    echo LGI_KEY."<br>";
//	    print_r($ses);
//	    exit;
        $data = $this->_data;
        $data['indexs'] = $this->def;
        $data['_total_donasi'] = $this->home->getTotalDonasi()->result_array();
        $data["_data_carousel"] = $this->home->getDataSlide();
        $data["_data_main_campaign"] = $this->home->getDataCMain();
        // ----------------------------------------------------------------------------------------------------
        // STATIC PAGE DATE
        // ----------------------------------------------------------------------------------------------------
        $HEADER = array('TOKEN-KEY: DDTEKNOPAYMENT', 'APP-KEY: APPKEYDDTEKNO', 'token-login: ' . $ses['mupays_token']);
        $datasaldo = Fhhlib::_curl_exexc_with_header($this->_data['mupays_url'] . "/mupays/transaksi/getsaldo", null, 'GET', $HEADER);
        $datasaldo = json_decode($datasaldo, true);
        $data['saldo_user'] = null;
        $data['link_aktivasi']='#';
        if ($datasaldo['rc'] == '0000') {
            $data['saldo_user'] = $datasaldo['data'];
            $data['link_aktivasi'] =$this->aktivwalet();
        }
//		$spagedata              = $this->shadaqah->getDataShadaq()->num_rows();
//		$data["spage_shadaq"]   = ($spagedata > 0) ? $this->shadaqah->getDataShadaq()->row_array() : array();
//
//		$spagedata              = $this->shadaqah->getDataInfaq()->num_rows();
//		$data["spage_infaq"]    = ($spagedata > 0) ? $this->shadaqah->getDataInfaq()->row_array() : array();
//
//		$spagedata              = $this->shadaqah->getDataZakat()->num_rows();
//		$data["spage_zakat"]    = ($spagedata > 0) ? $this->shadaqah->getDataZakat()->row_array() : array();
        // ----------------------------------------------------------------------------------------------------
        $datatoko = json_decode( $this->curlshoptalk("https://api.tokotalk.com/v1/shop/372930/products?limit=12&offset=NaN&join=promotion&sort=-sort,-id&query=available:1"),true);
        $datatokoikan = json_decode( $this->curlshoptalk("https://api.tokotalk.com/v1/shop/387942/products?limit=12&offset=NaN&join=promotion&sort=-sort,-id&query=available:1"),true);
        $datatokobuku = json_decode( $this->curlshoptalk("https://api.tokotalk.com/v1/shop/403132/products?limit=12&offset=NaN&join=promotion&sort=-sort,-id&query=available:1"),true);
        $data['_data_shoptokotalk'] = $datatoko['data'];
        $data['_data_shoptokotalkikan'] = $datatokoikan['data'];
        $data['_data_shoptokotalkbuku'] = $datatokobuku['data'];
        $data['_data_campaign_list_donasi'] = !empty($this->home->getCampaignKategori('', 5)->result()) ? $this->home->getCampaignKategori('', 5)->result() : [];
//        $data['_data_campaign_list_beasiswa'] = !empty($this->home->getCampaignKategori(2,5)->result()) ?$this->home->getCampaignKategori(2,5)->result():[];
//        $data['_data_campaign_list_investasi'] = !empty($this->home->getCampaignKategori(3,5)->result()) ?$this->home->getCampaignKategori(3,5)->result():[];
        $data["_data_campaign_list"] = !empty($this->home->getDataCList()->result()) ? $this->home->getDataCList()->result() : array();
        $data["_data_post_list"] = $this->home->getDataPList();
        $data['_data_post_list_promo'] =  $this->db->get_where('nb_post',array('type' => 'promosi','is_active'=>'1'))->result();
        $this->parser->parse("index", $data);

    }

    public function addsub()
    {
        $email = $this->input->post('email');
        if (!$this->home->get_email($email)->num_rows()) {

            $data = array(
                'email' => $email,
            );
            $this->home->subscribe($data);

            $config["username"] = MAIL_UNAME;
            $config["password"] = MAIL_PASS;
            $config["host"] = MAIL_HOST;
            $config["smtpAuth"] = MAIL_SMTP;
            $config["smtpType"] = MAIL_SMTP_TYPE;
            $config["port"] = MAIL_PORT;

            sendmail::ConfigUser($config);


            $data["emailTo"] = $email;
            $data["senderName"] = "GaneshaBisa";
            $data["subject"] = "Email Berlangganan";
            $data["type"] = "view";
            $data["email"] = $email;
            $data["konten"] = "home/berlangganan";

            sendmail::SendMail($data);
            $this->session->set_flashdata('msg',
                '<div class="alert alert-success">
                    <h4>Berhasil</h4>
                    <p>Email berhasil didaftar</p>
                </div>');
            redirect(site_url('#footer'));
        } else {
            $this->session->set_flashdata('msg',
                '<div class="alert alert-danger">
                    <h4>Gagal</h4>
                    <p>Email anda telah terdaftar</p>
                </div>');
            redirect(site_url('#footer'));
        }

    }
    private function curlshoptalk($urlsop=''){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $urlsop,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    function frame(){
        $id=$this->input->get('cid');
//        $where = ['']
        $data["_data_main_campaign"] = !empty($this->home->getCampaignPilih($id)->result_array()) ? $this->home->getCampaignPilih($id, 5)->result_array() : [];
        return $this->parser->parse("indexframe", $data);
    }
    function inframefrom(){
        $urllink = $_GET['ulinks'];
        $data = $this->_data;
        $data['urllink'] = $urllink;
        return $this->parser->parse("inframesapp", $data);
    }
    public function aktivwalet()
    {
        $data = $this->_data;
        $HEADER = array('TOKEN-KEY: DDTEKNOPAYMENT', 'APP-KEY: APPKEYDDTEKNO');
        $urlmupays = $this->_data['mupays_url'];
//        $token = $this->_curl_exexc("http://localhost/apiwebpg/public/mupays/partner/gettokenweb", null, 'GET', $HEADER);
        $token = Fhhlib::_curl_exexc_with_header($urlmupays . "/mupays/partner/gettokenweb", null, 'GET', $HEADER);
        $token = json_decode($token, true);
        return ($token['data']['landing_page']);
    }


    private function _curl_exexc($url = '', $postdata = '', $METHOD = 'POST', $HEADER = [])
    {
//        $postdata = http_build_query($data);
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADER);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }
}

