<?php
/**
 * Created by PhpStorm.
 * Project : berzakat
 * User: fahmihilmansyah
 * Date: 06/07/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 10.31
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="google-site-verification" content="z25T2qzRkrLtQViGARxao473Yf7WT5Qi63d7QfcIf58"/>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>/assets/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>/assets/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>/assets/icons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>/assets/icons/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url() ?>/assets/icons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <!-- Bootstrap core CSS -->
    <!--    <link href="-->

    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/css/heroic-features.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/css/rjcustoms.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/owl-carousel/owl.theme.default.min.css"
          rel="stylesheet">


    <!-- Bootstrap core JavaScript -->
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/jquery/jquery.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/js/rjcustoms.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '542214526701191');
        fbq('track', 'PageView');

        // $.ajaxSetup({
        //     beforeSend: function() {
        //         console.log('haloo');
        //         // show progress spinner
        //     },
        //     complete: function() {
        //         console.log('haloo complete');
        //         // hide progress spinner
        //     }
        // });

        function detectajax() {
            var proxied = window.XMLHttpRequest.prototype.send;
            var getel = document.getElementsByClassName('modalscren');
            window.XMLHttpRequest.prototype.send = function () {
                // console.log(getel);
                getel[0].classList.add("loadings");
                // getel[0].classList.add("loadingss");
                // console.log( arguments );
                //Here is where you can add any code to process the request.
                //If you want to pass the Ajax request object, pass the 'pointer' below
                var pointer = this
                var intervalId = window.setInterval(function () {
                    if (pointer.readyState != 4) {
                        return;
                    }

                    // getel[0].classList.remove("loading");

                    getel[0].classList.remove("loadings");
                    // console.log( pointer.responseText );
                    //Here is where you can add any code to process the response.
                    //If you want to pass the Ajax request object, pass the 'pointer' below
                    clearInterval(intervalId);

                }, 1);//I found a delay of 1 to be sufficient, modify it as you need.
                return proxied.apply(this, [].slice.call(arguments));
            };

        }

        detectajax();
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=542214526701191&ev=PageView&noscript=1"
        /></noscript>
    <title>MumuApps</title>
</head>
<body style="padding-bottom: 0; padding-top: 10px;">
<div class="content">
    <div class="relative-box" style="padding-bottom: 0;">
        <div
           class="link-block card card-donasi overflow-hidden"><?php

            $img = !empty($_data_main_campaign[0]['img']) ? $_data_main_campaign[0]['img'] : '';

            $img = URI::existsImage('campaign/thumbnails/',
                $img,
                'noimg_front/',
                'no-amini.jpg');
            ?>

            <?php if(!empty($_GET['withimgbanner']) && $_GET['withimgbanner'] == 'true'): ?>
            <div class="bg-donasi"
                 style="background-image:url(<?php echo $img ?>);"></div>
            <?php endif;?>
            <div class="card-body">
                <div class="mb-25 font-title bold judul-donasi">
                    <?php echo empty($_data_main_campaign[0]['title'])?'':$_data_main_campaign[0]['title'] ?>
                    <?php
                    $imgadm = !empty($_data_main_campaign[0]['imgadm']) ? $_data_main_campaign[0]['imgadm'] : '';

                    $imgadm = URI::existsImage('admprofil/',
                        $imgadm,
                        'noimg_front/',
                        'no-amini.jpg');
                    ?>

                </div>
                <div class="mb-10">
                    <div class="pendana-wrapper mb-20">
                        <div class="pendana-box">
                            <div class="pendana-img title-font"
                                 style="background-image:url(<?php echo $imgadm ?>);"></div>
                            <div class="pendana-text">
                                <div class="text-small">Program Pendanaan Dari</div>
                                <div class="title-font semibold lh-n"> <?php echo !empty($_data_main_campaign[0]['fname']) ? $_data_main_campaign[0]['fname'] : "-"; ?></div>
                                <div><i class="icofont-check verif-icon"></i> <span
                                            class="color-second text-small medium">Terverifikasi</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-7">
                        <div class="text-muted"><i class="icofont-"></i> Terkumpul</div>
                        <div class="bold">Rp <?php echo number_format(empty($_data_main_campaign[0]['now'])?0:$_data_main_campaign[0]['now']) ?></div>
                    </div>
                    <div class="col-5 text-right">
                        <div class="text-muted"><i class="icofont-clock"></i> Batas Waktu</div>
                        <div class="bold"><?php
                            $bataswaktu = empty($_data_main_campaign[0]['selisih_validate'])?0:$_data_main_campaign[0]['selisih_validate'];
                            echo $bataswaktu<0?0:$bataswaktu ?> hari lagi</div>
                    </div>
                </div>
                <?php
                $nomtarget = empty($_data_main_campaign[0]['target'])?0:$_data_main_campaign[0]['target'];
                $nomskr = empty($_data_main_campaign[0]['now'])?0:$_data_main_campaign[0]['now'];
                $pgbar = 0;
                if ($nomtarget == 0) {
                    $pgbar = 0;
                } elseif (($nomskr / $nomtarget) >= 1) {
                    $pgbar = 100;
                } else {
                    $pgbar = ($nomskr / $nomtarget) * 100;
                }
                ?>
                <div class="progress mt-5">
                    <div class="progress-bar bg-main progress-bar-striped progress-bar-animated"
                         role="progressbar" aria-valuenow="<?php echo $pgbar ?>" aria-valuemin="0"
                         aria-valuemax="100"
                         style="width: <?php echo $pgbar ?>%"></div>
                </div>
            </div>
        </div>
        <div class="link-danai">
            <?php if(!empty($_GET['withoriartikel']) && $_GET['withoriartikel'] == 'false'): ?>
            <div class="row">
                <div class="col">
                    <?php $validate = empty($_data_main_campaign[0]['valid_date'])?0:strtotime($_data_main_campaign[0]['valid_date']);
                    if ($validate >= strtotime(date("Y-m-d"))) { ?>
                        <a href="<?php echo base_url('donasi/aksi/' . $_data_main_campaign[0]['id']); ?>"
                           class="btn btn-main form-control"><span>Danai Sekarang</span></a>
                    <?php } else { ?>
                        <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                    <?php } ?>
                </div>
            </div>
            <?php else: ?>
            <div class="row">
                <div class="col-6">
                    <a href="<?php echo base_url('donasi/' . empty($_data_main_campaign[0]['slug'])?'':$_data_main_campaign[0]['slug']) ?>"
                       class="btn btn-block btn-info"><span>Detail</span></a>
                </div>
                <div class="col-6">
                    <?php $validate = empty($_data_main_campaign[0]['valid_date'])?0:strtotime($_data_main_campaign[0]['valid_date']);
                    if ($validate >= strtotime(date("Y-m-d"))) { ?>
                        <a href="<?php echo base_url('donasi/aksi/' . $_data_main_campaign[0]['id']); ?>"
                           class="btn btn-main form-control"><span>Danai Sekarang</span></a>
                    <?php } else { ?>
                        <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                    <?php } ?>
                </div>
            </div>
            <?php endif;?>

        </div>
        <!--                <a href="danai.html" class="btn btn-main link-danai"><span>Danai Sekarang</span></a>-->
    </div>
</div>
</body>
</html>
