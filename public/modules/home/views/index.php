{_layHeader}
<?php

$this->load->model(array('home/Home_model' => "home"));
?>

<!--<div class="featured-image" style="background-image:url(https://images.unsplash.com/photo-1488521787991-ed7bbaae773c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80);"></div>-->
<!--<div class="featured-images">-->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <?php $i = 0;
        foreach ($_data_carousel as $row):
            $actf = '';
            if ($i == 0) {
                $actf = 'active';
            }
            $i++;
            ?>
            <?php
            // $img = !empty($row->img) ? $row->img : "noimage.png";
            $img = !empty($row->img) ? $row->img : '';

            $img = URI::existsImage('slider/',
                $img,
                'noimg_front/',
                'no-slider.jpg');
            ?>
            <div class="carousel-item text-center <?php echo $actf ?>">
                <!--                <img class="d-block w-100 h-100" src="--><?php //echo $img
                ?><!--" alt="First slide">-->
                <?php if (!empty($row->link)): ?>
                    <a href="<?php echo count(explode('//', $row->link)) > 1 ? $row->link : '//' . $row->link ?>">
                        <img class="d-block w-100 h-100" src="<?php echo $img ?>" alt="First slide">
                    </a>
                <?php else:; ?>
                    <img class="d-block w-100 h-100" src="<?php echo $img ?>" alt="First slide">
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!--</div>-->
<div class="content">
    <?php
    if ($_loginstatus) {
        $sess = $this->session->userdata(LGI_KEY . "login_info");
        $imgpoto = '';
        if (!empty($indexs[0]->img)):
            $imgpoto = base_url('assets/images/donatur/profile/'.$indexs[0]->img);
        else:
            $imgpoto = $indexs[0]->picture_url;
        endif;
        ?>
        <div class="top-info">
            <div class="row">
                <div class="col" style="z-index: 2000;">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div style="border-right: 1px solid #8080804f" class="col-3">
                                    <img class="rounded-circle" height="65" width="65"
                                         src="<?php echo $imgpoto ?>">
                                </div>
                                <div class="col-9">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row align-items-center">
                                                <div class="col-3">
                                                    <img class="rounded" height="65" width="65"
                                                         src="<?php URI::baseURL(); ?>assets/images/icons/logoduit.png">
                                                </div>
                                                <div class="col-6" style="display: block;">
                                                    <div><b>Saldo</b>
                                                    </div>
                                                    <div style=" margin-top: -10px;">
                                                        <b>Rp. <?php echo number_format($saldo_user['amount'], 0, ',', '.') ?></b>
                                                    </div>
                                                    <div style="font-size: 12px; margin-top: -2px;">Saldo Bonus</div>
                                                </div>
                                                <div class="col-3 align-items-center">
                                                    <?php if (empty($sess['mupays_token'])): ?>
                                                        <a href="<?php echo $link_aktivasi ?>"
                                                           class="btn btn-sm btn-outline-warning"
                                                           style="margin-top:20px;">Aktivasi</a>
                                                    <?php else: ?>
                                                        <a style="font-size: 10px;"
                                                           href="<?php echo base_url('/donatur/dompetku') ?>"
                                                               class="btn btn-sm btn-outline-success">Top Up</a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if (!empty($ishide)): ?>
        <div class="top-info">
            <div class="row">
                <div class="col text-center">
                    &nbsp;
                </div>
                <?php if (!empty($ishide)): ?>
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <div class="title-font bold">
                                    Rp <?php echo number_format($_total_donasi[0]['total'], 0, ',', '.') ?></div>
                                <div class="text-muted text-smaller medium">Total Pendanaan</div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <div class="title-font bold"><?php echo number_format($_total_donasi[0]['donatur'], 0, ',', '.') ?></div>
                                <div class="text-muted text-smaller medium">Total Pemilik Dana</div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>

    <?php endif ?>

    <div class="h5 title-font bold text-center mt-20 mb-15">Pilih Cepat Pendanaan</div>
    <div class="row">
        <div class="col">
            <a href="<?php URI::baseURL(); ?>zakat" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/july update - icon-11.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Zakat</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="<?php URI::baseURL(); ?>infak" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/july update - icon-12.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Sedekah</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="<?php URI::baseURL(); ?>wakaf" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/july update - icon-13.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Wakaf</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="<?php URI::baseURL(); ?>akikahpartner/homepage" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/july update - icon-19.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small"> Akikah</div>
                </div>
            </a>
        </div>
    </div>
    <!--    <div class="h5 title-font bold text-center mt-20 mb-15">Pilihan Pendanaan</div>-->
    <div class="row">
        <div class="col">
            <a href="<?php URI::baseURL(); ?>beasiswa" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/july update - icon-15.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Yatim</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="<?php URI::baseURL(); ?>asuransi" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/july update - icon-16.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Proteksi</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="<?php URI::baseURL(); ?>investasi" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/july update - icon-17.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Investasi</div>
                </div>
            </a>
        </div>
        <div class="col">
            <a href="#" id="more-mid-menu" class="link-block card">
                <div class="card-body text-center">
                    <img src="<?php echo base_url() . 'assets/images/icons/more.png' ?>" width="50">
                    <div class="text-center title-font semibold mt-10 text-small">Lainnya</div>
                </div>
            </a>
        </div>
    </div>
    <?php if (!empty($lihat)): ?>
        <div class="h5 title-font bold text-center mt-20 mb-15">Pembayaran / Pembelian</div>
        <div class="row">
            <div class="col-2">
                <a href="<?php URI::baseURL(); ?>pulsa" class="link-block card">
                    <div class="card-body text-center">
                        <i class="icofont-smart-phone icofont-2x text-muted"></i>
                        <div style="font-size: 0.5rem;" class="text-center title-font semibold mt-10">Pulsa</div>
                    </div>
                </a>
            </div>
            <div class="col-2">
                <a href="<?php URI::baseURL(); ?>#" class="link-block card">
                    <div class="card-body text-center">
                        <i class="icofont-smart-phone icofont-2x text-muted"></i>
                        <div style="font-size: 0.5rem;" class="text-center title-font semibold mt-10">Paket data</div>
                    </div>
                </a>
            </div>
            <div class="col-2">
                <a href="<?php URI::baseURL(); ?>#" class="link-block card">
                    <div class="card-body text-center">
                        <i class="icofont-flash icofont-2x text-muted"></i>
                        <div style="font-size: 0.5rem;" class="text-center title-font semibold mt-10">Listrik PLN</div>
                    </div>
                </a>
            </div>
        </div>
    <?php endif; ?>
    <?php
    if (!empty($_data_main_campaign)) {
        ?>
        <div class="relative-box mt-20">
            <a href="<?php echo base_url('donasi/' . $_data_main_campaign[0]['slug']) ?>"
               class="link-block card card-donasi overflow-hidden"><?php

                $img = !empty($_data_main_campaign[0]['img']) ? $_data_main_campaign[0]['img'] : '';

                $img = URI::existsImage('campaign/thumbnails/',
                    $img,
                    'noimg_front/',
                    'no-amini.jpg');
                ?>
                <div class="bg-donasi"
                     style="background-image:url(<?php echo $img ?>);"></div>
                <div class="card-body">
                    <div class="mb-25 font-title bold judul-donasi">
                        <?php echo $_data_main_campaign[0]['title'] ?>
                        <?php
                        $imgadm = !empty($_data_main_campaign[0]['imgadm']) ? $_data_main_campaign[0]['imgadm'] : '';

                        $imgadm = URI::existsImage('admprofil/',
                            $imgadm,
                            'noimg_front/',
                            'no-amini.jpg');
                        ?>

                    </div>
                    <div class="mb-10">
                        <div class="pendana-wrapper mb-20">
                            <div class="pendana-box">
                                <div class="pendana-img title-font"
                                     style="background-image:url(<?php echo $imgadm ?>);"></div>
                                <div class="pendana-text">
                                    <div class="text-small">Program Pendanaan Dari</div>
                                    <div class="title-font semibold lh-n"> <?php echo !empty($_data_main_campaign[0]['fname']) ? $_data_main_campaign[0]['fname'] : "-"; ?></div>
                                    <div><i class="icofont-check verif-icon"></i> <span
                                                class="color-second text-small medium">Terverifikasi</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <div class="text-muted"><i class="icofont-"></i> Terkumpul</div>
                            <div class="bold">Rp <?php echo number_format($_data_main_campaign[0]['now']) ?></div>
                        </div>
                        <div class="col-5 text-right">
                            <div class="text-muted"><i class="icofont-clock"></i> Batas Waktu</div>
                            <div class="bold"><?php echo $_data_main_campaign[0]['selisih_validate'] < 0 ?0 :$_data_main_campaign[0]['selisih_validate']?> hari lagi</div>
                        </div>
                    </div>
                    <?php
                    $nomtarget = $_data_main_campaign[0]['target'];
                    $nomskr = $_data_main_campaign[0]['now'];
                    $pgbar = 0;
                    if ($nomtarget == 0) {
                        $pgbar = 0;
                    } elseif (($nomskr / $nomtarget) >= 1) {
                        $pgbar = 100;
                    } else {
                        $pgbar = ($nomskr / $nomtarget) * 100;
                    }
                    ?>
                    <div class="progress mt-5">
                        <div class="progress-bar bg-main progress-bar-striped progress-bar-animated"
                             role="progressbar" aria-valuenow="<?php echo $pgbar ?>" aria-valuemin="0"
                             aria-valuemax="100"
                             style="width: <?php echo $pgbar ?>%"></div>
                    </div>
                </div>
            </a>
            <div class="link-danai">
                <div class="row">
                    <div class="col">
                        <a href="<?php echo base_url('donasi/' . $_data_main_campaign[0]['slug']) ?>"
                           class="btn btn-block btn-info"><span>Detail</span></a>
                    </div>
                    <div class="col">
                        <?php $validate = strtotime($_data_main_campaign[0]['valid_date']);
                        if ($validate >= strtotime(date("Y-m-d"))) { ?>
                            <a href="<?php echo base_url('donasi/aksi/' . $_data_main_campaign[0]['id']); ?>"
                               class="btn btn-main form-control"><span>Danai Sekarang</span></a>
                        <?php } else { ?>
                            <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                        <?php } ?>
                    </div>
                </div>

            </div>
            <!--                <a href="danai.html" class="btn btn-main link-danai"><span>Danai Sekarang</span></a>-->
        </div>
    <?php } ?>
    <div class=" mt-30 mb-20">
        <a href="<?php URI::baseURL(); ?>donasi" class="float-right" style="text-decoration: none; color: #09A59D; padding-right: 10px;"> Semua</a>
        <div class="h5 title-font bold no-margin">Pendanaan Populer</div>
    </div>
    <?php
    if (!empty($_data_campaign_list_donasi)) {
        ?>
        <div class="owl-wrapper">
            <div class="owl-carousel owl-theme dana-populer">
                <?php
                foreach ($_data_campaign_list_donasi as $r) {
                    $img = !empty($r->img) ? $r->img : '';

                    $img = URI::existsImage('campaign/thumbnails/',
                        $img,
                        'noimg_front/',
                        'no-amini.jpg');
                    ?>
                    <?php
                    $imgadm = !empty($r->imgadm) ? $r->imgadm : '';

                    $imgadm = URI::existsImage('admprofil/',
                        $imgadm,
                        'noimg_front/',
                        'no-amini.jpg');
                    ?>
                    <?php
                    $now = !empty($r->now) ? $r->now : 0;
                    $target = !empty($r->target) ? $r->target : 0;

                    if ($target == 0) {
                        $hitung = 0;
                    } elseif (($now / $target) >= 1) {
                        $hitung = 100;
                    } else {
                        $hitung = ($now / $target) * 100;
                    }
                    ?>


                    <div class="item">
                        <div class="relative-box">
                            <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                               class="link-block card card-donasi overflow-hidden">
                                <div class="bg-donasi"
                                     style="background-image:url(<?php echo $img ?>);"></div>
                                <div class="card-body">
                                    <div class="mb-25 font-title bold judul-donasi">
                                        <?php
                                        $title = !empty($r->title) ? $r->title : "-";
                                        echo $title;
                                        ?>
                                    </div>
                                    <div class="mb-10">
                                        <div class="pendana-wrapper mb-20">
                                            <div class="pendana-box">
                                                <div class="pendana-img title-font"
                                                     style="background-image:url(<?php echo $imgadm ?>);"></div>
                                                <div class="pendana-text">
                                                    <div class="text-small">Program Pendanaan Dari</div>
                                                    <div class="title-font semibold lh-n"> <?php echo !empty($r->fname) ? $r->fname : "-"; ?></div>
                                                    <div><i class="icofont-check verif-icon"></i> <span
                                                                class="color-second text-small medium">Terverifikasi</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="text-muted"><i class="icofont-"></i> Terkumpul</div>
                                            <div class="bold">Rp <?php echo number_format($now, 0, ",", ".") ?></div>
                                        </div>
                                        <div class="col-5 text-right">
                                            <div class="text-muted"><i class="icofont-clock"></i> Batas Waktu</div>
                                            <div class="bold"><?php
                                                $selisih_validate = !empty($r->selisih_validate) ? $r->selisih_validate : 0;
                                                $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;
                                                echo $selisih_validate;
                                                ?> hari lagi
                                            </div>
                                        </div>
                                    </div>
                                    <div class="progress mt-5">
                                        <div class="progress-bar bg-main progress-bar-striped progress-bar-animated"
                                             role="progressbar" aria-valuenow="<?php echo $hitung ?>" aria-valuemin="0"
                                             aria-valuemax="100"
                                             style="width: <?php echo $hitung ?>%"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="link-danai">
                                <div class="row">
                                    <div class="col">
                                        <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                                           class="btn btn-block btn-info"><span>Detail</span></a>
                                    </div>
                                    <div class="col">
                                        <?php $validate = strtotime($r->valid_date);
                                        if ($validate >= strtotime(date("Y-m-d"))) { ?>
                                            <a href="<?php echo base_url('donasi/aksi/' . $r->id); ?>"
                                               class="btn btn-block btn-main form-control"><span>Danai Sekarang</span></a>
                                        <?php } else { ?>
                                            <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <style>
        .live__scroll {
            overflow-x: auto;
            overflow-y: hidden;
            white-space: nowrap;

            color: inherit;
            text-decoration: none;
        }
        .live__scroll .row{
            display:block;
            margin-left: 0px !important;
        }
        .live__scroll .live__scroll--box {
            display: inline-block;
            float: none;
            padding: 15px;
        }
        .card-text:last-child {
            white-space: normal;
        }
    </style>
    <div class="mt-30 mb-20">
        <div class="h5 title-font bold no-margin">Promo dan Peristiwa</div>
    </div>
    <div class="owl-wrapper">
        <div class="owl-carousel owl-theme dana-populer">
            <?php
            if(!empty($_data_post_list_promo)){
            foreach ($_data_post_list_promo as $r):
            $img = '';
            if (empty($r->img)):
                $img = $r->img_urls;
            else:
                $img = !empty($r->img) ? $r->img : '';

                $img = URI::existsImage('articles/',
                    $img,
                    'noimg_front/',
                    'no-amini.jpg');
            endif;
            ?>
            <div class="item">
                <div class="relative-box">
                    <a style="padding-bottom: 0px;" href="<?php echo $r->link_terkait;?>"
                       class="link-block card overflow-hidden">
                        <div class="bg-donasi"
                             style="background-image:url(<?php echo $img?>);"></div>
                    </a>
                </div>
            </div>
            <?php endforeach;
            }?>
        </div>
    </div>

    <div class="mt-30 mb-20">
        <img class="rounded" height="35"
             src="<?php URI::baseURL(); ?>assets/images/icons/july update - logo service-07.png">
        <div class="h5 mt-15 title-font bold no-margin">Sudut Buku</div>
        <div class="row"><div style="font-size: 14px;" class="col-10" >Khasanah keilmuan ke ruang baca Anda</div>
            <div class="col-2">
                <a href="<?php echo base_url('home/inframefrom') ?>?ulinks=https://toko.ly/mumubooks/home" class="float-right" style="text-decoration: none; color: #09A59D; padding-right: 10px; font-size: 14px;">Semua</a>
            </div>
        </div>
    </div>
    <div href="#" class="live__scroll">
        <div class="row">
            <?php $i=1; foreach ($_data_shoptokotalkbuku as $r):
                if($i <=7):
                    $currency = !empty($r['currency'])?$r['currency']:'';
                    ?>
                    <div class="col-6 live__scroll--box">
                        <a href="<?php echo base_url('home/inframefrom') ?>?ulinks=https://toko.ly/mumubooks/products/<?php echo $r['id']; ?>"  class="link-block">
                            <div class="card">
                                <img class="card-img-top" src="<?php echo $r['images'][0]['url']?>" alt="Card image cap">
                                <div class="card-title ">
                                    <label style="margin-top: -20px;" class="float-right badge badge-info"><?php echo $currency." ". number_format($r['productVariations'][0]['price'])?></label>
                                </div>
                                <div class="card-body">
                                    <p class="card-text"><?php echo $r['name']?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php $i++; endif; endforeach; ?>
        </div>
    </div>
    <div class="mt-30 mb-20">
<!--        <a href="--><?php //echo base_url('home/inframefrom') ?><!--?ulinks=https://toko.ly/mumustore/home" class="float-right" style="text-decoration: none; color: #09A59D; padding-right: 10px;">Semua</a>-->
        <div class="h5 mt-15 title-font bold no-margin">
            Produk Dampingan UMKM
        </div>
        <div></div>
        <div class="row"><div style="font-size: 14px;" class="col-10" >Dukung Pemberdayaan Ekonomi Dhuafa</div>
            <div class="col-2">
                <a href="<?php echo base_url('home/inframefrom') ?>?ulinks=https://toko.ly/mumustore/home" class="float-right" style="text-decoration: none; color: #09A59D; padding-right: 10px; font-size: 14px;">Semua</a>
            </div>
        </div>
    </div>
    <div href="#" class="live__scroll">
    <div class="row">
        <?php $i=1; foreach ($_data_shoptokotalk as $r):
            if($i <=7):
                $currency = !empty($r['currency'])?$r['currency']:'';
                ?>
        <div class="col-6 live__scroll--box">
            <a href="<?php echo base_url('home/inframefrom') ?>?ulinks=https://toko.ly/mumustore/products/<?php echo $r['id']; ?>"  class="link-block">
            <div class="card">
                <img class="card-img-top" src="<?php echo $r['images'][0]['url']?>" alt="Card image cap">
                <div class="card-title ">
                    <label style="margin-top: -20px;" class="float-right badge badge-info"><?php echo $currency." ". number_format($r['productVariations'][0]['price'])?></label>
                </div>
                <div class="card-body">
                    <p class="card-text"><?php echo $r['name']?></p>
                </div>
            </div>
            </a>
        </div>
        <?php $i++; endif; endforeach; ?>
    </div>
    </div>
    <div class="mt-30 mb-20">
        <div class="h5 title-font bold no-margin">
<!--            <img class="rounded" height="35"-->
<!--                                                       src="--><?php //URI::baseURL(); ?><!--assets/images/icons/july update - logo service-07.png"></div>-->
        Gemarikan</div>
        <div class="row"><div style="font-size: 14px;" class="col-10" >Terbaik dari Lautan Nusantara</div>
            <div class="col-2">
                <a href="<?php echo base_url('home/inframefrom') ?>?ulinks=https://toko.ly/gemarikan/home" class="float-right" style="text-decoration: none; color: #09A59D; padding-right: 10px; font-size: 14px;">Semua</a>
            </div>
            </div>
    </div>
    <div href="#" class="live__scroll">
    <div class="row">
        <?php $i=1; foreach ($_data_shoptokotalkikan as $r):
            if($i <=7):
                $currency = !empty($r['currency'])?$r['currency']:'';
            ?>
        <div class="col-6 live__scroll--box">
            <a href="<?php echo base_url('home/inframefrom') ?>?ulinks=https://toko.ly/gemarikan/products/<?php echo $r['id']; ?>"  class="link-block">
            <div class="card">
                <img class="card-img-top" src="<?php echo $r['images'][0]['url']?>" alt="Card image cap">
                <div class="card-title ">
                    <label style="margin-top: -20px;" class="float-right badge badge-info"><?php echo $currency." ". number_format($r['productVariations'][0]['price'])?></label>
                </div>
                <div class="card-body">
                    <p class="card-text"><?php echo $r['name']?></p>
                </div>
            </div>
            </a>
        </div>
        <?php $i++; endif; endforeach; ?>
    </div>
    </div>
    <div class="mt-30 mb-20">
        <a href="<?php URI::baseURL(); ?>news/list"class="float-right" style="text-decoration: none; color: #09A59D; padding-right: 10px;"> Semua</a>
        <div class="h5 title-font bold no-margin">Berita</div>
    </div>
    <?php
    if (!empty($_data_post_list)) {
        ?>

        <div class="owl-wrapper">
            <div class="owl-carousel owl-theme dana-populer">
                <?php foreach ($_data_post_list as $r) {
                    ?>
                    <?php
                    $img = '';
                    if (empty($r->img)):
                        $img = $r->img_urls;
                    else:
                        $img = !empty($r->img) ? $r->img : '';

                        $img = URI::existsImage('articles/',
                            $img,
                            'noimg_front/',
                            'no-amini.jpg');
                    endif;
                    ?>
                    <div class="item">
                        <div class="card overflow-hidden">
                            <div class="bg-donasi"
                                 style="background-image:url(<?php echo $img ?>);"></div>
                            <div class="card-body">
                                <div class="mb-25 font-title bold judul-donasi">
                                    <?php echo !empty($r->title) ? $r->title : "-"; ?>
                                </div>
                                <div class="mb-15 text-smaller">
                                    <div>
                                        <i class="text-muted icofont-user-alt-3 mr-5"></i> <?php echo $r->author ?>
                                    </div>
                                    <div>
                                        <i class="text-muted icofont-ui-calendar mr-5"></i> <?php echo date("d M Y", strtotime($r->post_date)) ?>
                                    </div>
                                </div>
                                <div>
                                    <?php
                                    echo !empty($r->desc_short) ? $r->desc_short : "-";
                                    ?>
                                </div>
                                <div class="mt-20">
                                    <a href="<?php URI::baseURL(); ?>news/<?php echo $r->slug ?>"
                                       class="btn btn-block btn-main"><span>Selengkapnya</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    {_layFooter}
