<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="title" content="<?php echo $_appTitle; ?>">

    <title><?php echo $_appTitle; ?> </title>
    <meta name="google-site-verification" content="z25T2qzRkrLtQViGARxao473Yf7WT5Qi63d7QfcIf58" />

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>/assets/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>/assets/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>/assets/icons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>/assets/icons/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url() ?>/assets/icons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap core CSS -->
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/css/heroic-features.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/css/rjcustoms.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/owl-carousel/owl.theme.default.min.css" rel="stylesheet">


    <!-- Bootstrap core JavaScript -->
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/jquery/jquery.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/js/rjcustoms.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '542214526701191');
        fbq('track', 'PageView');

        // $.ajaxSetup({
        //     beforeSend: function() {
        //         console.log('haloo');
        //         // show progress spinner
        //     },
        //     complete: function() {
        //         console.log('haloo complete');
        //         // hide progress spinner
        //     }
        // });

        function detectajax() {
            var proxied = window.XMLHttpRequest.prototype.send;
            var getel = document.getElementsByClassName('modalscren');
            window.XMLHttpRequest.prototype.send = function() {
                // console.log(getel);
                getel[0].classList.add("loadings");
                // getel[0].classList.add("loadingss");
                // console.log( arguments );
                //Here is where you can add any code to process the request.
                //If you want to pass the Ajax request object, pass the 'pointer' below
                var pointer = this
                var intervalId = window.setInterval(function(){
                    if(pointer.readyState != 4){
                        return;
                    }

                    // getel[0].classList.remove("loading");

                    getel[0].classList.remove("loadings");
                    // console.log( pointer.responseText );
                    //Here is where you can add any code to process the response.
                    //If you want to pass the Ajax request object, pass the 'pointer' below
                    clearInterval(intervalId);

                }, 1);//I found a delay of 1 to be sufficient, modify it as you need.
                return proxied.apply(this, [].slice.call(arguments));
            };

        }
        detectajax();
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=542214526701191&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
    <style>
        /* Start by setting display:none to make this hidden.
       Then we position it in relation to the viewport window
       with position:fixed. Width, height, top and left speak
       for themselves. Background we set to 80% white with
       our animation centered, and no-repeating */
        .modalscren {
            display:    none;
            /*    position:   fixed;*/
            /*    z-index:    1000;*/
            /*    top:        0;*/
            /*    left:       0;*/
            /*    height:     100%;*/
            /*    width:      100%;*/
            /*    background: rgba( 255, 255, 255, .8 )*/
            /*    !*50% 50%*!*/
            /*    no-repeat;*/
        }

        /* When the body has the loading class, we turn
           the scrollbar off with overflow:hidden */
        /*body.loading .modalscren {*/
        /*    overflow: hidden;*/
        /*}*/

        /* Anytime the body has the loading class, our
           modal element will be visible */
        .loadings {
            /*display: block;*/
            position:   fixed;
            width: 100%;
            height:100%;
            display: flex;
            align-items: center;
            z-index:    9999;
            top:        0;
            left:       0;
            bottom:       0;
            right:       0;
            height:     100%;
            width:      100%;
            background: rgba(0,0,0,.65)
                /*50% 50%*/
            no-repeat;
        }
    </style>

</head>

<body style="padding-top: 0px !important;">


<div class="container">

    <div class="modalscren">
        <div class="container">
            <div class="d-flex justify-content-center">
                <div class="spinner-border text-success" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
<?php

$this->load->model(array('home/Home_model' => "home"));
?>
<!--</div>-->
<div class="content ">
    <iframe id="myIframe" src="<?php echo $urllink ?>"style="width: 100%; height: 89vh; border: none;" ></iframe>
</div>

</div>
<div class="foot-menu">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <a href="<?php URI::baseURL(); ?>" class="active"><i class="icofont-home"></i></a>
            </div>
            <div class="col text-center">
                <a href="#" id="footer-mid-menu" class="footer-mid-menu btn btn-lg"><span><img style="margin-top: 8px;" src="<?php URI::baseURL(); ?>assets/images/logo/logomumuh2es.png" width="55"></i></span></a>
                <a href="#" id="close-footer-mid-menu" class="footer-mid-menu btn"><span><i class="icofont-close"></i></span></a>
            </div>
            <div class="col text-center">
                <a href="#" id="nav-menu" style="    color: #09A59D;"><i class="icofont-navigation-menu"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="search-top">
    <div class="container">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart(base_url() . 'donasi', array('id' => 'fmsearch','class'=>"form-search-top"), $hidden_form);
        ?>
        <div class="form-group relative-box">
            <i class="icon-search icofont-search-1"></i>
            <a href="#" id="close-search-top"><i class="icofont-close"></i></a>

            <input type="text" name="searchdonasi"  class="form-control" placeholder="Cari Pendanaan...">
        </div>
        <?php
        echo form_close();
        ?>
    </div>
</div>

<div class="nav-menu nav-menu2" style="overflow-y:scroll;
    overflow-x:hidden;">
    <div class="container">
        <div>
            <span class="float-left" id="hjudul"><b>Metode Pembayaran</b></span>
            <div class="text-right mb-20">
                <a href="#" id="close-nav-menu2"><i class="icofont-close"></i></a>
                <hr/>
            </div>
        </div>
        <div id="settmpl"></div>
    </div>
</div>
<div class="nav-menu">
    <div class="container">
        <div class="text-right mb-20">
            <a href="#" id="close-nav-menu"><i class="icofont-close"></i></a>
        </div>

        <?php
        if(!$_loginstatus)
        {
            ?>
            <div class="mb-25 medium">Masuk atau daftar untuk menggunakan fitur lainnya.</div>
            <div class="row mb-30">
                <div class="col">
                    <a href="<?php URI::baseURL(); ?>register" class="btn btn-link btn-block"><span>Daftar</span></a>
                </div>
                <div class="col">
                    <a href="<?php URI::baseURL(); ?>login" class="btn btn-main btn-block"><span>Masuk</span></a>
                </div>
            </div>
        <?php }?>
        <ul class="nav-menu-list medium">
            <?php
            if($_loginstatus)
            {
                ?>
                <li>
                    <div class="row">
                        <div class="col">
                            <a href="<?php URI::baseURL(); ?>donatur"><i class="icofont-gear mr-5"></i> Dashboard Donatur</a>
                        </div>
                        <div class="col">
                            <a href="<?php URI::baseURL(); ?>logout"><i class="icofont-logout mr-5"></i> Logout</a></div>
                    </div>

                </li>
            <?php }?>
            <li>
                <a href="#"><i class="icofont-file-alt mr-5"></i> Syarat & Ketentuan</a>
            </li>
            <li>
                <a href="#"><i class="icofont-question mr-5"></i> Bantuan</a>
            </li>
            <li>
                <a href="#"><i class="icofont-globe mr-5"></i> Pilih Bahasa</a>
            </li>
        </ul>
    </div>
</div>

<div class="footer-mid-menu-content">
    <div class="full-height container relative-box">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">

                        <ul class="nav-menu-list medium">
                            <li>
                                <a href="<?php URI::baseURL(); ?>zakat/zakat">
                                    <img src="<?php echo base_url().'assets/images/icons/001-Zakat.png' ?>" width="25">
                                    Zakat</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>donasi">
                                    <img src="<?php echo base_url().'assets/images/icons/002-Donation.png' ?>" width="25"> Donasi</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>wakaf/wakaf">
                                    <img src="<?php echo base_url().'assets/images/icons/003-Wakaf.png' ?>" width="25">
                                    Wakaf</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>kurban/kurban">
                                    <img src="<?php echo base_url().'assets/images/icons/004-Kurban.png' ?>" width="25"> Kurban</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>kurbanpartner/homepage">
                                    <img src="<?php echo base_url().'assets/images/icons/004-Kurban.png' ?>" width="25"> Kurban LA</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>beasiswa"><img src="<?php echo base_url().'assets/images/icons/005-Yatim-Dhuafa.png' ?>" width="25"> Yatim</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6">

                        <ul class="nav-menu-list medium">
                            <li>
                                <a href="<?php URI::baseURL(); ?>investasi"><img src="<?php echo base_url().'assets/images/icons/007-Investasi.png' ?>" width="25"> Investasi</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>asuransi"><img src="<?php echo base_url().'assets/images/icons/006-Proteksi.png' ?>" width="25"> Proteksi</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>aqiqah/aqiqah">
                                    <img src="<?php echo base_url().'assets/images/icons/Akikah.png' ?>" width="25"> Akikah</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>pulsa">
                                    <img src="<?php echo base_url().'assets/images/icons/008-Pulsa.png' ?>" width="25"> Pulsa</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<!--<script src="--><?php //URI::baseURL(); ?><!--assets/mganeshabisa/vendor/jquery/jquery.min.js"></script>-->
<!--<script src="--><?php //URI::baseURL(); ?><!--assets/mganeshabisa/vendor/owl-carousel/owl.carousel.min.js"></script>-->
<!--<script src="--><?php //URI::baseURL(); ?><!--assets/mganeshabisa/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>-->
<script src="<?php URI::baseURL(); ?>assets/mganeshabisa/js/rjcustoms.js"></script>

<script type="text/javascript">

    // Selecting the iframe element
    var iframe = document.getElementById("myIframe");

    // Adjusting the iframe height onload event
    iframe.onload = function(){
        var tinggi = iframe.contentWindow.document.body.scrollHeight;
        console.log('tinggi = ',tinggi);
        iframe.style.height =  tinggi+ 'px';
    }
    $( "#close-nav-menu2" ).click(function() {
        $('.nav-menu2').removeClass('open');
    });
    $( "#pilbayar" ).click(function() {
        $('.nav-menu2').addClass('open');
    });
    $('.dana-populer').owlCarousel({
        stagePadding: 40,
        loop: true,
        margin: 12,
        nav: false,
        responsive: {
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
</script>

</body>

</html>