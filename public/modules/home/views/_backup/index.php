{_layHeader}

	

	<!-- Slider -->
<?php print_r($_data_carousel);exit;?>
	<div id="slider">

		<div class="container-fluid">

			<div class="row">

			<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="10000">

			<!-- Indicators -->
				<ol class="carousel-indicators">

				<?php 
					$jum_dot = count($_data_carousel);
					
					$i = 0;
					foreach($_data_carousel as $row){
						$active = ($i == 0) ? "class='active'" : "";
				?>

					<li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" <?php echo $active; ?>></li>

				<?php
					$i++;
					}
				?>
				</ol>


				<div class="carousel-inner" role="listbox" style="overflow: hidden !important;">

				<!-- Wrapper for slides -->

				<?php 
					if(!empty($_data_carousel))
					{
						$i = 0;
						foreach($_data_carousel as $row){
							$active = ($i == 0) ? "active" : "";
				?>
						<div class="item <?php echo $active; ?>" style="border:0px solid black;">
						<?php
							//$img = !empty($row->img) ? $row->img : "noimage.png";
						?>
							<?php
							// $img = !empty($row->img) ? $row->img : "noimage.png";
							$img = !empty($row->img) ? $row->img : '';

							$img = URI::existsImage('slider/', 
													  $img, 
													  'noimg_front/',
													  'no-slider.jpg');
							?>
							<div class="bg-slout">&nbsp;</div>
							<img src="<?php echo $img; ?>" style="width: 1366px !important;">

							<?php
							if($row->link_d){
								if(!empty($row->title)){
							?>
							<div class="carousel-caption custom-caption">
								<div class="custom-caption-inner">
									<center>
									<?php 
										$title = !empty($row->title) ? $row->title : "-";
									?>
									<h1 class="bold"><?php echo strtoupper($title); ?></h1>

									<p>
									
									<?php 
										$desc = !empty($row->desc) ? $row->desc : "-";

										echo $desc;
									?>

									</p>

									<div>

										<a href="<?php echo $row->link ?>" class="btn btn-default sm-bold fo">FIND OUT MORE</a>

									</div>
									</center>
								</div>
							</div>
							<?php 
								}
							}
							?>
						</div>
				<?php
							$i++;
						}
					}else{
				?>

						<div class="item active">

							<img class="image" src="<?php URI::baseURL(); ?>assets/images/slider/bglegalitas.jpg">

							<div class="carousel-caption custom-caption">

								<h1 class="bold">SAVE <br/> ROHINGYA</h1>

								<p>

								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

								</p>

								<div class="pull-left">

									<a href="javascript:void(0);" class="btn btn-default sm-bold fo">FIND OUT MORE</a>

								</div>

							</div>

						</div>



						<div class="item">

							<img class="image" src="<?php URI::baseURL(); ?>assets/images/slider/bgprofil.jpg">

							<div class="carousel-caption">

								<h3>Title</h3>

								<p>The atmosphere in Chania has a touch of Florence and Venice.</p>

							</div>

						</div>



						<div class="item">

							<img class="image" src="<?php URI::baseURL(); ?>assets/images/slider/bgsekilas.jpg">

							<div class="carousel-caption">

								<h3>Title</h3>

								<p>Beatiful flowers in Kolymbari, Crete.</p>

							</div>

						</div>



						<div class="item">

							<img class="image" src="<?php URI::baseURL(); ?>assets/images/slider/bgsusunan.jpg">

							<div class="carousel-caption">

								<h3>Title</h3>

								<p>Beatiful flowers in Kolymbari, Crete.</p>

							</div>

						</div>

					<?php
					}
					?>

				</div>
			</div>

			</div>

		</div>

	</div>



	<!-- MAIN CAMPAIGN -->

	<div id="main-campaign">

		<div class="container">

		<?php 
			if(!empty($_data_main_campaign))
			{
		?>

		<!-- start create campaign main -->
		<?php foreach($_data_main_campaign as $r){ ?>
			<div class="main-campaign-wrap">
			 	<div class="col-md-5" style="overflow:hidden; padding:0px">
	                <?php
        		        $img = !empty($r->img) ? $r->img : '';
		  				
	                	$img = URI::existsImage('campaign/new_thumb/', 
	                								  $img, 
	                								  'noimg_front/',
	                								  'no-urgent.jpg');
                    ?>
					<img src="<?php echo $img; ?>"
						 style="position:relative; height: 442px; width:100%">
			 	</div>
  				<div class="col-md-7 main-campaign-konten">
  					<h3 class="sm-bold">URGENT CAMPAIGN</h3>

  					<h1 class="bold">
  						<a href="<?php echo base_url(); ?>donasi/<?php echo $r->slug; ?>" class="title-urgent">
				  		<?php 
							$title = !empty($r->title) ? strtoupper($r->title) : "-";
							echo $title; 
						?>				
						</a>
  					</h1>

					<p class="jtf">  		
						<?php 
							$desc_short = !empty($r->desc_short) ? $r->desc_short : "-";
							echo ucwords($desc_short); 
						?>	
					</p>

					<div style="padding-top:70px;">
						<div class="barWrapper">
							<div class="progress">
								<?php 
									$now    	= !empty($r->now) ? $r->now : 0;
									$target    	= !empty($r->target) ? $r->target : 0;

									if($target == 0)
									{
										$hitung 	= 0;
									}
									elseif(($now/$target) >=1)
									{
										$hitung = 100;
									}
									else
									{
										$hitung 	= ($now/$target)*100;
									}
								?>
								<div class="progress">
									<div 
									class="progress-bar" 
									role="progressbar" 
									aria-valuenow="<?php echo $hitung; ?>" 
									aria-valuemin="0" 
									aria-valuemax="100"
									>
										<b><?php echo  round($hitung,2); ?>%</b>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div style="margin-bottom:30px">
							<div class="col-md-4 col-sm-4 col-xs-12">
									<p class="hidden-xs hidden-sm">Terkumpul</p>
									<span class="sm-bold size14">
										Rp. <?php echo number_format($now,0,",",".") ?>
									</span>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-12">
									<p class="hidden-xs hidden-sm">&nbsp;</p>
									<span class="fa fa-clock-o"></span> : 
									<span class="sm-bold size14">
										<?php echo ($r->selisih_validate <= 0) ? "0" : $r->selisih_validate ?> 
										hari Lagi
									</span>
							</div>
							<div class="col-md-5 col-sm-5 col-xs-12" style="margin-bottom: 10px;">
								<?php if($r->selisih_validate > 0): ?>
									<a href="<?php echo base_url('donasi/'.$r->slug);?>" 
										class="btn btn-success padd-rl-50 pull-right">
										<b>DONASI</b>
									</a>
								<?php else: ?>
									<a href="<?php echo base_url('donasi/'.$r->slug);?>" 
										class="btn btn-warning padd-rl-50 pull-right">
										<b>DONASI TELAH SELESAI</b>
									</a>
								<?php endif; ?>
							</div>
					</div>
  				</div>
			</div>
		<?php } ?>
			<!-- end create campaign -->
		<?php
			}else{
		?>
			<div class="main-campaign-wrap">

			 	<div class="col-sm-5" style="overflow:hidden">

					<img src="<?php //URI::baseURL(); ?>assets/images/maincampaign/sample.jpg" 

						 style="position:relative; left:-15px; height: 442px; width:100%">

			 	</div>

  				<div class="col-sm-6 main-campaign-konten">

  					<h3 class="sm-bold">URGENT CAMPAIGN</h3>

  					<h1 class="bold">SAVE ROHINGYA</h1>

  					<p>oleh: Bambang</p>

					<p>

					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

					</p>



					<div style="padding-top:70px;">

						<div class="barWrapper">

								<div class="progress">
									<div 
									class="progress-bar" 
									role="progressbar" 
									aria-valuenow="60%" 
									aria-valuemin="0" 
									aria-valuemax="100"
									>
										<b>60%</b>
									</div>
								</div>

						</div>

					</div>

					<div style="margin-bottom:20px">
							<div class="col-md-4">

									Terkumpul <br/>

									<span class="sm-bold size14">Rp. 1.723.000</span>
							</div>

							<div class="col-md-4">
									<span class="fa fa-clock-o"></span> : 

									<span class="sm-bold size14">7 hari Lagi</span>

							</div>

							<div class="col-md-4">
									<a href="javascript:void(0)" 

										class="btn btn-success padd-rl-50" 

										style="padding-left:50px; padding-right:50px;">

										DONASI

									</a>
							</div>

					</div>

  				</div>

			</div>
		<?php
			}
		?>
		</div>

	</div>



	<!-- PROGRAM CAMPAIGN -->

	<div id="program-campaign">

		<div class="container">

			<div clas="row">

				<div class="col-md-12">

					<div class="program-campaign-title">

						<center>

							<h2 class="bold">PROGRAM DONASI POPULAR</h2>

							<hr class="line">

							<h5>Program akan kami salurkan kepada yang membutuhkan</h5>

						</center>

					</div>

				</div>

			</div>

			<div class="row">

			<?php
			if(!empty($_data_campaign_list))
			{
				foreach($_data_campaign_list as $r){

			?>

				<div class="col-md-4" style="margin-bottom:20px">

					<div class="program-campaign-list">

						<div class="list-image">
                        	<?php

		        		        $img = !empty($r->img) ? $r->img : '';
				  				
			                	$img = URI::existsImage('campaign/thumbnails/', 
			                								  $img, 
			                								  'noimg_front/',
			                								  'no-amini.jpg');
                        	?>
							<img src="<?php echo $img; ?>" style="width:100%">

						</div>

						<div class="list-konten">

							<h3 class="bold">
                                <a href="<?php echo base_url('donasi/'.$r->slug);?>">
                                <?php echo !empty($r->title) ? strtoupper($r->title) : "-"; ?></a>
	  						</h3>

							<p class="jtf">
  								<?php echo !empty($r->desc_short) ? ucwords($r->desc_short) : "-"; ?>	
							</p>

						</div>

						<div class="list-progress">
							<div class="barWrapper">
								<div class="progress">
									<?php 
										$now    	= !empty($r->now) ? $r->now : 0;
										$target    	= !empty($r->target) ? $r->target : 0;

										if($target == 0)
										{
											$hitung 	= 0;
										}
										elseif(($now/$target) >=1)
										{
											$hitung = 100;
										}
										else
										{
											$hitung 	= ($now/$target)*100;
										}
									?>
									<div class="progress">
										<div 
										class="progress-bar" 
										role="progressbar" 
										aria-valuenow="<?php echo $hitung; ?>" 
										aria-valuemin="0" 
										aria-valuemax="100"
										>
											<b><?php echo round($hitung,2); ?>%</b>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="list-desc">

							<table width="100%">

								<tr>

									<td>

										Terkumpul <br/>

										<span class="sm-bold size14 blue">
											Rp. <?php echo number_format($now,0,",",".") ?>
										</span>

									</td>

									<td align="right">

										<br/>

										<span class="fa fa-clock-o"></span> : 

										<span class="sm-bold size14">
											<span class="blue">
												<?php echo ($r->selisih_validate <= 0) ? "0" : $r->selisih_validate; ?>
											</span> 
										hari Lagi</span>

									</td>

								</tr>

								<tr>

									<td colspan="2">&nbsp;</td>

								</tr>

								<tr>

									<td align="center" colspan="2">
										<?php if($r->selisih_validate > 0): ?>
										<a href="<?php echo base_url('donasi/'.$r->slug);?>" 

											class="btn btn-success padd-rl-50" 

											style="padding-left:50px; padding-right:50px;">
											
											<b>DONASI</b>

										</a>
										<?php else: ?>
										<a href="<?php echo base_url('donasi/'.$r->slug);?>"

										   class="btn btn-warning padd-rl-50"

										   style="padding-left:50px; padding-right:50px;">

											<b>DONASI TELAH SELESAI</b>

										</a>
										<?php endif; ?>
									</td>

								</tr>

							</table>

						</div>

					</div>

				</div>

			<?php
				}
			}
			else
			{
			?>
				<div class="col-md-4" style="margin-bottom:20px">

					<div class="program-campaign-list">

						<div class="list-image">

							<img src="<?php URI::baseURL(); ?>assets/images/campaign/donasi1.jpg" style="width:100%">

						</div>

						<div class="list-konten">

							<h3 class="bold">SAVE ROHINGYA</h3>

							<p>oleh: Bambang</p>

							<p>

								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

							</p>

						</div>

						<div class="list-progress">

							<div class="barWrapper">

								<div class="progress">

									<div 

										class="progress-bar" 

										role="progressbar" 

										aria-valuenow="100" 

										aria-valuemin="10" 

										aria-valuemax="100" 

										style="">

									<span  class="popOver" data-toggle="tooltip" data-placement="top" title="230%"></span>  

									</div>

								</div>

							</div>

						</div>

						<div class="list-desc">

							<table width="100%">

								<tr>

									<td>

										Terkumpul <br/>

										<span class="sm-bold size14 blue">Rp. 1.723.000</span>

									</td>

									<td align="right">

										<br/>

										<span class="fa fa-clock-o"></span> : 

										<span class="sm-bold size14"><span class="blue">7</span> hari Lagi</span>

									</td>

								</tr>

								<tr>

									<td colspan="2">&nbsp;</td>

								</tr>

								<tr>

									<td align="right" colspan="2">

										<a href="javascript:void(0)" 

											class="btn btn-success padd-rl-50" 

											style="padding-left:50px; padding-right:50px;">

											<b>DONASI</b>

										</a>

									</td>

								</tr>

							</table>

						</div>

					</div>

				</div>
			<?php
			}
			?>

				<div clas="row">

					<div class="col-md-12">

						<div class="list-all">

							<center>

								<a href="<?php URI::baseURL(); ?>donasi" class="btn btn-blue"><b>LIHAT SEMUA</b></a>

							</center>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>



	<!-- PROGRAM CAMPAIGN -->

	<div id="program-lembaga">
		
			<div id="program-zakat">
					<div id="program-rel-position">
						<?php 
							$img = !empty($spage_infaq["m_img"]) ? "spage_program/" . $spage_infaq["m_img"] : "bg1.jpg";
						?>

						<center><img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img; ?>" style="min-width: 1366px;"></center>
					</div>

					<div id="program-mb-position" class="hidden-lg">
						<div>
							<div class="program-zakat-title hidden-lg">
								<center>
									<h2 class="bold">
										<?php 
											echo !empty($spage_infaq["m_title"]) ? strtoupper($spage_infaq["m_title"]) : "PROGRAM DONASI GaneshaBisa";
										?>
									</h2>
									<hr class="line line-white">
									<h5 style="width: 500px; height: 80px; overflow: hidden;">
									<?php
										if(!empty($spage_infaq["m_desc"])){
											echo $spage_infaq["m_desc"];
										}
										else
										{
									?>
										Bantuan Anda, berapa pun jumlahnya, sangat bermanfaat buat mereka yang membutuhkan. <br/>

										Kami menyalurkan seluruh bantuan amanat dari maysarakat untuk sebesar-besar kemanfaatan yang layak menerimanya.
									<?php
										}
									?>
									</h5>
								</center>
							</div>
							<div class="program-con-putih">
								<div class="hidden-sm hidden-md hidden-lg">
									<div style="padding: 20px;">
											<center>
												<h3 style="margin-top:20px; color:#0255a5" class="bold">
												PILIH PROGRAM</h3>
												<hr class="line">
											</center><br/>
			                				<?php 

												$name         = "program_menu";
												$value        = 'infaq';
												$extra        = 'id="program_menu" style="width:100%;"';
												$categories   = array(	
																		'shadaqah' => 'Shadaqah',
																		'zakat' => 'Zakat',
																		'infaq' => 'Infaq'
																		);

			                					echo form_dropdown($name, $categories, $value, $extra); 

			                				?>
									</div>
								</div>
								<div class="hidden-xs" style="padding-top: 15px;">
									<div class="col-sm-4" style="border-right: 1px solid #ccc">
										<a class="program-mb-href program-mb-href-a" href="<?php echo URI::baseURL(); ?>program/infaq">
											<center>
											<?php
												$img_infaq = !empty($spage_infaq["icon_img"]) ? 
																"spage_program/" . $spage_infaq["icon_img"] : 
																"icon/infaq.png"; 
											?>

											<?php
				            					$img = !empty($spage_infaq["icon_img"]) ? $spage_infaq["icon_img"] : '';
								  				
							                	$img = URI::existsImage('spage_program/', 
							                								  $img, 
							                								  'noimg_front/',
							                								  'no-aminilist.jpg');
			                            	?>
											<img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img_infaq; ?>" height="140px">
											<p class="bold">Infaq</p>
											</center>
										</a>
									</div>
									<div class="col-sm-4" style="border-right: 1px solid #ccc">
										<a class="program-mb-href" href="<?php echo URI::baseURL(); ?>program/shadaqah">
											<center>
											<?php 
												$img_shadaqah = !empty($spage_shadaq["icon_img"]) ? 
																"spage_program/" . $spage_shadaq["icon_img"] : 
																"icon/shadaqah.png"; 
											?>

											<img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img_shadaqah; ?>" height="140px">

											<p class="bold">Shadaqah</p>
											</center>
										</a>
									</div>
									<div class="col-sm-4">
										<a class="program-mb-href" href="<?php echo URI::baseURL(); ?>program/zakat" >
											<center>
											<?php

												$img_zakat = !empty($spage_zakat["icon_img"]) ? 
																"spage_program/" . $spage_zakat["icon_img"] : 
																"icon/zakat.png"; 
											/*
											    if($img_zakat != "icon/zakat.png")
							                    {
							                    	$name 		= explode(".", $img_zakat);
							                    	$img_zakat  = $name[0] . "_g." . $name[1];
							                    }
							                */

											?>
											<img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img_zakat; ?>" height="140px">
											<p class="bold">Zakat</p>
											</center>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="program-abs-position" class="hidden-xs hidden-sm hidden-md">
						<div class="container">
							<div clas="row">
								<div class="col-md-12">
									<div class="program-zakat-title">
										<center>
											<h2 class="bold">
												<?php 
													echo !empty($spage_infaq["m_title"]) ? strtoupper($spage_infaq["m_title"]) : "PROGRAM DONASI GaneshaBisa";
												?>
											</h2>
											<hr class="line line-white">
											<h5 style="width: 500px; height: 80px; overflow: hidden;">
											<?php
												if(!empty($spage_infaq["m_desc"])){
													echo $spage_infaq["m_desc"];
												}
												else
												{
											?>
												Bantuan Anda, berapa pun jumlahnya, sangat bermanfaat buat mereka yang membutuhkan. <br/>

												Kami menyalurkan seluruh bantuan amanat dari maysarakat untuk sebesar-besar kemanfaatan yang layak menerimanya.
											<?php
												}
											?>
											</h5>
										</center>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-4" style="margin-bottom:20px">
									<a href="<?php echo URI::baseURL(); ?>program/infaq" class="program-zakat-list infaq-h">
										<div class="list-program">
											<center>
												<div>
													<img src="<?php URI::baseURL(); ?>assets/images/spage_program/<?php echo $spage_infaq["icon_img"]; ?>">
												</div>
												<h2 class="bold">
													<?php 
														echo !empty($spage_infaq["icon_title"]) ? strtoupper($spage_infaq["icon_title"]) : "INFAQ";
													?>
												</h2>
												<hr class="line">
											</center>
										</div>

										<div class="list-konten">
											<p class="p-program">
												<?php
													if(!empty($spage_infaq["icon_desc"]))
													{
														echo $spage_infaq["icon_desc"];
													}
													else
													{
												?>
													Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
												<?php
													}
												?>
											</p>
										</div>

										<div class="list-btn padd-rl-50" style="padding-bottom:20px">

										</div>

										<div class="program-zakat-add-block">

										</div>
									</a>
								</div>

								<div class="col-md-4" style="margin-bottom:20px">
									<a href="<?php echo URI::baseURL(); ?>program/shadaqah" class="program-zakat-list shadaqah-h">
										<div class="list-program">
											<center>

												<div>
												<img src="<?php URI::baseURL(); ?>assets/images/spage_program/<?php echo $spage_shadaq["icon_img"]; ?>">
												</div>
												<h2 class="bold">
													<?php
														echo !empty($spage_shadaq["icon_title"]) ? strtoupper($spage_shadaq["icon_title"]) : "SHADAQAH"
													?>
												</h2>
												<hr class="line">
											</center>
										</div>

										<div class="list-konten">
											<p class="p-program">
												<?php
													if(!empty($spage_shadaq["icon_desc"]))
													{
														echo $spage_shadaq["icon_desc"];
													}
													else
													{
												?>
													Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
												<?php
													}
												?>
											</p>
										</div>

										<div class="list-btn padd-rl-50" style="padding-bottom:20px">

										</div>

										<div class="program-zakat-add-block">

										</div>
									</a>
								</div>

								<div class="col-md-4" style="margin-bottom:20px">
									<a href="<?php echo URI::baseURL(); ?>program/zakat" class="program-zakat-list zakatmaal-h">
										<div class="list-program">
											<center>
												<div>
													<img src="<?php URI::baseURL(); ?>assets/images/spage_program/<?php echo $spage_zakat["icon_img"]; ?>">
												</div>

												<h2 class="bold">
													<?php
														echo !empty($spage_zakat["icon_title"]) ? strtoupper($spage_zakat["icon_title"]) : "ZAKAT";
													?>
												</h2>

												<hr class="line">
											</center>
										</div>

										<div class="list-konten">
											<p class="p-program">
												<?php
													if(!empty($spage_zakat["icon_desc"]))
													{
														echo $spage_zakat["icon_desc"];
													}
													else
													{
												?>
													Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
												<?php
													}
												?>
											</p>
										</div>

										<div class="list-btn padd-rl-50" style="padding-bottom:20px">
										
										</div>

										<div class="program-zakat-add-block">


										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

	</div>

	

	<!-- NEWS UPDATE -->

	<div id="program-campaign" class="news">

		<div class="container">

			<div clas="row">

				<div class="col-md-12">

					<div class="program-campaign-title">

						<center>

							<h2 class="bold">NEWS UPDATE</h2>

							<hr class="line">

							<h5>Berita terkini mengenai kegiata GaneshaBisa</h5>

						</center>

					</div>

				</div>

			</div>

			<div class="row">


			<?php
			if(!empty($_data_post_list))
			{
				foreach($_data_post_list as $r){

			?>

				<div class="col-md-4" style="margin-bottom:20px;">

					<div class="program-campaign-list">

						<div class="list-image">
                        	<?php

		        		        $img = !empty($r->img) ? $r->img : '';
				  				
			                	$img = URI::existsImage('articles/thumbnails/', 
			                								  $img, 
			                								  'noimg_front/',
			                								  'no-amini.jpg');
                        	?>
							<img src="<?php echo $img; ?>" style="width:100%">

						</div>

						<div class="list-konten">

							<h3 class="bold">
								<?php echo !empty($r->title) ? $r->title : "-"; ?>
							</h3>

							<p class="jtf">

								<?php
									echo !empty($r->desc_short) ? $r->desc_short : "-";
								?>

							</p>
							<br/>
							<br/>
						</div>

						<div class="list-desc">
							<div class="col-md-12">
								
								<a href="<?php URI::baseURL(); ?>news/<?php echo $r->slug?>" 

									class="btn btn-success padd-rl-50 pull-right" 

									style="padding-left:50px; padding-right:50px; margin-bottom: 30px;">

									<b>LIHAT SEMUA</b>

								</a>
							</div>

						</div>
					</div>

				</div>

			<?php

				}

			}
			else
			{

			?>
				<div class="col-md-4" style="margin-bottom:20px">

					<div class="program-campaign-list">

						<div class="list-image">

							<img src="<?php URI::baseURL(); ?>assets/images/campaign/donasi1.jpg" style="width:100%">

						</div>

						<div class="list-konten">

							<h3 class="bold">SAVE ROHINGYA</h3>

							<p>oleh: Bambang</p>

							<p>

								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

							</p>

						</div>

						<div class="list-progress">

							<div class="barWrapper">

								<div class="progress">

									<div 

										class="progress-bar" 

										role="progressbar" 

										aria-valuenow="100" 

										aria-valuemin="10" 

										aria-valuemax="100" 

										style="">

									<span  class="popOver" data-toggle="tooltip" data-placement="top" title="230%"></span>  

									</div>

								</div>

							</div>

						</div>

						<div class="list-desc">

							<table width="100%">

								<tr>

									<td>

										Terkumpul <br/>

										<span class="sm-bold size14 blue">Rp. 1.723.000</span>

									</td>

									<td align="right">

										<br/>

										<span class="fa fa-clock-o"></span> : 

										<span class="sm-bold size14"><span class="blue">7</span> hari Lagi</span>

									</td>

								</tr>

								<tr>

									<td colspan="2">&nbsp;</td>

								</tr>

								<tr>

									<td align="right" colspan="2">

										<a href="javascript:void(0)" 

											class="btn btn-success padd-rl-50" 

											style="padding-left:50px; padding-right:50px;">

											DONASI

										</a>

									</td>

								</tr>

							</table>

						</div>

					</div>

				</div>
			<?php
			}
			?>

				<div clas="row">

					<div class="col-md-12">

						<div class="list-all">

							<center>

								<a href="<?php URI::baseURL(); ?>news/list" class="btn btn-blue"><b>LIHAT SEMUA</b></a>

							</center>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>
{_layFooter}

<script type="text/javascript">
	select2_icon("program_menu");
</script>