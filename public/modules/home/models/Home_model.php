<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

	private $_table1 = "campaign_slide";

  	private function _kunci($data = array()){
        $result = $data;

        if(!is_array($data))
            $result = array("id" => $data);

        return $result;
    }

    function changeTable($table = "")
    {
        $this->_table1 = $table;
    }

	function data($key = "")
	{
        $this->db->select("*");
        $this->db->from($this->_table1);	

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
	}

	function getDataSlide()
	{
		$result = $this->data()->get();

		return !empty($result->result()) ? $result->result() : array();
	}

    function getDataCMain($where=[])
    {
        $this->changeTable("v_campaign_main");
        $result = $this->data($where)->get();

        return !empty($result->result()) ? $result->result_array() : array();
    }

    function getDataCList()
    {		
		$sql="select id, title, slug, desc_short, author, now, target, img, to_days(valid_date) - to_days(curdate()) as selisih_validate from nb_campaign order by sum_donatur desc limit 3";
		
		return $this->db->query($sql);
    }

    function getDataPList()
    {
        $this->changeTable("nb_v_post_list");
        $this->db->limit(7);
        $result = $this->data()->get();

        return !empty($result->result()) ? $result->result() : array();
    }
	
	function get_email($where){
		return $this->db->get_where('nb_subscribe',array('email' => $where));
	}
	
	function subscribe($data){
		$this->db->insert('nb_subscribe',$data);
	}
	function getTotalDonasi(){
  	    $this->db->select('sum(nominal) total , count(*) donatur');
  	    $this->db->from('nb_trans_donation');
  	    $this->db->where('status','verified');
  	    return $this->db->get();
    }
    function getCampaignKategori($idkat='',$limit=''){
        $this->db->select('c.id AS id,
                        c.title AS title,
                        c.slug AS slug,
                        c.desc_short AS desc_short,
                        c.target AS target,
                        c.now AS now,
                        c.img AS img,
                        c.user_type AS user_type,
                        c.valid_date AS valid_date,
                        concat( c.author ) AS name_userpost,
                        to_days( c.valid_date ) - to_days(
                        curdate()) AS selisih_validate ,
                        cc.name kategori, 
                        efd.fname, 
                        efd.img as imgadm');
        $this->db->from('nb_campaign c');
        $this->db->join('nb_campaign_categories cc','cc.id = c.categories');
        $this->db->join("nb_sys_users as efd","c.user_post = efd.id");
        $this->db->where('c.is_active','1');
        $this->db->where('cc.is_active','1');
        if(!empty($idkat)){
            $this->db->where('cc.id',$idkat);
        }
        if(!empty($limit)){
            $this->db->limit($limit);
        }
        $this->db->order_by('c.now','desc');
        $this->db->order_by('c.valid_date','desc');
        return $this->db->get();
    }
    function getCampaignPilih($idcamp='',$limit=''){
        $this->db->select('c.id AS id,
                        c.title AS title,
                        c.slug AS slug,
                        c.desc_short AS desc_short,
                        c.target AS target,
                        c.now AS now,
                        c.img AS img,
                        c.user_type AS user_type,
                        c.valid_date AS valid_date,
                        concat( c.author ) AS name_userpost,
                        to_days( c.valid_date ) - to_days(
                        curdate()) AS selisih_validate ,
                        cc.name kategori, 
                        efd.fname, 
                        efd.img as imgadm');
        $this->db->from('nb_campaign c');
        $this->db->join('nb_campaign_categories cc','cc.id = c.categories');
        $this->db->join("nb_sys_users as efd","c.user_post = efd.id");
        $this->db->where('c.is_active','1');
        $this->db->where('cc.is_active','1');
        if(!empty($idcamp)){
            $this->db->where('c.id',$idcamp);
        }
        if(!empty($limit)){
            $this->db->limit($limit);
        }
        $this->db->order_by('c.now','desc');
        $this->db->order_by('c.valid_date','desc');
        return $this->db->get();
    }
    function getKategoriC(){
        $this->db->select('*');
        $this->db->from('nb_campaign_categories');
        $this->db->where('is_active','1');
        return $this->db->get();
    }
}

?>