<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Aktivasiwallet extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : No Reply
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */

    private $_modList = array(
        "aktivasiwallet/Kurban_model" => "mdl_donatur",
        "ppob/kurban_model" => 'donasimdl'
    );

    protected $_data = array();


    private $def;

    private $iddonaturs;

    private $_SESSION;

    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();
        $this->nbauth_front->lookingForAuten();

        $this->_SESSION = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
            $this->session->userdata(LGI_KEY . "login_info") : '';

        $this->iddonaturs = empty($this->_SESSION['userid']) ? '' : $this->_SESSION['userid'];

        $kondisi['where'] = array(

            'nb_donaturs_d.donatur' => $this->iddonaturs,

        );
        $kondisi['join'] = array('table' => 'nb_donaturs', 'condition' => 'nb_donaturs_d.donatur = nb_donaturs.id', 'type' => 'left');

        $this->def = $this->mdl_donatur->custom_query('nb_donaturs_d', $kondisi)->result();

        $this->load->library('myphpmailer');

    }


    private function modelsApp()

    {
        $this->load->model($this->_modList);
        $this->load->library('Fhhlib');

    }


    public function index()
    {
        $data = $this->_data;
        $HEADER = array('TOKEN-KEY: DDTEKNOPAYMENT', 'APP-KEY: APPKEYDDTEKNO');
        $urlmupays = $this->_data['mupays_url'];
//        $token = $this->_curl_exexc("http://localhost/apiwebpg/public/mupays/partner/gettokenweb", null, 'GET', $HEADER);
        $token = Fhhlib::_curl_exexc_with_header($urlmupays . "/mupays/partner/gettokenweb", null, 'GET', $HEADER);
        $token = json_decode($token, true);
        redirect($token['data']['landing_page']);
    }


    private function _curl_exexc($url = '', $postdata = '', $METHOD = 'POST', $HEADER = [])
    {
//        $postdata = http_build_query($data);
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADER);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }

    function activate()
    {
        if (!empty($_POST)) {
            $login_user = $_POST['tlogin_user'];
            $dataupdt = array('oauth_mupays' => $login_user);
            $datawhere = array('id' => $this->iddonaturs);
            $this->mdl_donatur->custom_update('nb_donaturs', $dataupdt, $datawhere);
            $_SESSION[LGI_KEY . "login_info"]['mupays_token'] = $login_user;
        }
        redirect('/');
        echo "ok bro";
    }
}

