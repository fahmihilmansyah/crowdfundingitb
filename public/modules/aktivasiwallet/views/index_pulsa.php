<?php
/**
 * Created by PhpStorm.
 * Project : berzakat
 * User: fahmihilmansyah
 * Date: 06/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.34
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$listoperator = array(
        '0811'=>'TSELPREPAID',
        '0812'=>'TSELPREPAID',
        '0813'=>'TSELPREPAID',
        '0821'=>'TSELPREPAID',
        '0822'=>'TSELPREPAID',
        '0823'=>'TSELPREPAID',
        '0851'=>'TSELPREPAID',
        '0852'=>'TSELPREPAID',
        '0853'=>'TSELPREPAID',
        '0898'=>'TRIPREPAID',
        '0899'=>'TRIPREPAID',
        '0896'=>'TRIPREPAID',
        '0897'=>'TRIPREPAID',
        '0814'=>'ISATPREPAID',
        '0816'=>'ISATPREPAID',
        '0856'=>'ISATPREPAID',
        '0857'=>'ISATPREPAID',
        '0858'=>'ISATPREPAID',
        '0889'=>'SMATFREN',
        '0881'=>'SMATFREN',
        '0882'=>'SMATFREN',
        '0883'=>'SMATFREN',
        '0884'=>'SMATFREN',
        '0885'=>'SMATFREN',
        '0886'=>'SMATFREN',
        '0887'=>'SMATFREN',
        '0888'=>'SMATFREN',
        '0828'=>'CERIA',
        '0868'=>'BYRU',
        '08315'=>'LIPOTEL',
        '0832'=>'AXIS',
        '0833'=>'AXIS',
        '0838'=>'AXIS',
);

$sessionsl = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
    $this->session->userdata(LGI_KEY . "login_info")['email'] : '';
 ?>
{_layHeader}
<style>
    .custom-radio-button div {
        display: inline-block;
    }
    .custom-radio-button input[type="radio"] {
        display: none;
    }
    .custom-radio-button input[type="radio"] + label {
        color: #333;
        font-family: Arial, sans-serif;
        font-size: 14px;
    }
    .custom-radio-button input[type="radio"] + label span {
        display: inline-block;
        width: 40px;
        height: 40px;
        margin: -1px 4px 0 0;
        vertical-align: middle;
        cursor: pointer;
        border-radius: 50%;
        border: 2px solid #ffffff;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
        background-repeat: no-repeat;
        background-position: center;
        text-align: center;
        line-height: 44px;
    }
    .custom-radio-button input[type="radio"] + label span img {
        opacity: 0;
        transition: all 0.3s ease;
    }
    .custom-radio-button input[type="radio"]#color-red + label span {
        background-color: red;
    }
    .custom-radio-button input[type="radio"]#color-blue + label span {
        background-color: blue;
    }
    .custom-radio-button input[type="radio"]#color-orange + label span {
        background-color: orange;
    }
    .custom-radio-button input[type="radio"]#color-pink + label span {
        background-color: pink;
    }
    .custom-radio-button input[type="radio"]:checked + label span {
        opacity: 1;
        background: url("https://www.positronx.io/wp-content/uploads/2019/06/tick-icon-4657-01.png")
        center center no-repeat;
        width: 40px;
        height: 40px;
        display: inline-block;
    }
</style>
<div class="content">
    <?php if (!empty($this->session->flashdata('error'))): ?>
        <div class="alert alert-danger alert-dismissable " role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <script type="text/javascript">
            setTimeout(function () {
                $('.alert').fadeOut();
            }, 10000); // <-- time in milliseconds
        </script>
    <?php endif; ?>
    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'pulsa' . $this->uri->segment(3), array('id' => 'fmdonasi', 'class' => 'fmdonasi mt-20'), $hidden_form);

    ?>
    <div class="card">
        <div class="card-header">Pembelian Pulsa</div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <label>No. Handphone</label>
                    <input type="text" id="nooper" name="donationPhone" class="form-control" placeholder="Ex: 081234566789">
                </div>
                <div class="col-12">
                    <label>Nominal</label>
                    <select class="form-control" id="resoper" name="donationNominal">
                        <option value=""></option>
                    </select>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Email</label>
                        <input name="donationEmail" <?php echo !empty($sessionsl)?'readonly':''; ?> value="<?php echo !empty($sessionsl)?$sessionsl:''; ?>" type="text" class="form-control" placeholder="Masukkan Email Anda">
                    </div>
                </div>
                <div class="mb-20">&nbsp;</div>
                <div class="col-12">
                    <div class="mb-20">Methode Pembayaran</div>
                    <div id="accordion">
                        <?php if(!empty($sessionsl)): ?>
                            <div class="card">
                                <div class="card-header" id="headingFor" data-toggle="collapse" data-target="#collapseFor"
                                     aria-expanded="true" aria-controls="collapseFor">
                                    <h5 class="mb-0">
                                        Dompet
                                    </h5>
                                </div>

                                <div id="collapseFor" class="collapse" aria-labelledby="headingFor" data-parent="#accordion">
                                    <div class="card-body">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="donationPay" id="dompetm"
                                                           value="SALDO_DOMPET">
                                                    <label class="form-check-label" for="dompetm">
                                                        <span> Saldo Dompet</span>
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="card">
                            <div class="card-header" id="headingFiv" data-toggle="collapse" data-target="#collapseFiv"
                                 aria-expanded="true" aria-controls="collapseTre">
                                <h5 class="mb-0">
                                    QRIS
                                </h5>
                            </div>

                            <div id="collapseFiv" class="collapse" aria-labelledby="headingFiv" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-group">
                                        <?php
                                        $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'qris'])->get()->result();
                                        foreach ($carilist as $r):
                                            ?>
                                            <li class="list-group-item">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="donationPay" id="<?php echo $r->kode_va ?>"
                                                           value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>">
                                                    <label class="form-check-label" for="<?php echo $r->kode_va ?>">
                                                        <?php if (!empty($r->img)): ?>
                                                            <img width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                                                        <?php else:?>
                                                            <?php echo $r->name ?>
                                                        <?php endif; ?>
                                                    </label>
                                                </div>
                                            </li>
                                        <?php endforeach;?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne"
                                 aria-expanded="true" aria-controls="collapseOne">
                                <h5 class="mb-0">
                                    Transfer Bank (Virtual Account)
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-group">
                                        <?php
                                        $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'va'])->get()->result();
                                        foreach ($carilist as $r):
                                            ?>
                                            <li class="list-group-item">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="donationPay" id="<?php echo $r->kode_va ?>"
                                                           value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>">

                                                    <label class="form-check-label" for="<?php echo $r->kode_va ?>">
                                                        <?php if (!empty($r->img)): ?>
                                                            <img width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                                                        <?php else:?>
                                                            <?php echo $r->name ?>
                                                        <?php endif; ?>
                                                    </label>
                                                </div>
                                            </li>
                                        <?php endforeach;?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo"
                                 aria-expanded="true" aria-controls="collapseTwo">
                                <h5 class="mb-0">
                                    E-Money
                                </h5>
                            </div>

                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-group">
                                        <?php
                                        $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'emoney'])->get()->result();
                                        foreach ($carilist as $r):
                                            ?>
                                            <li class="list-group-item">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="donationPay" id="<?php echo $r->kode_va ?>"
                                                           value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>">
                                                    <label class="form-check-label" for="<?php echo $r->kode_va ?>">
                                                        <?php if (!empty($r->img)): ?>
                                                            <img width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                                                        <?php else:?>
                                                            <?php echo $r->name ?>
                                                        <?php endif; ?>
                                                    </label>
                                                </div>
                                            </li>
                                        <?php endforeach;?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTre" data-toggle="collapse" data-target="#collapseTre"
                                 aria-expanded="true" aria-controls="collapseTre">
                                <h5 class="mb-0">
                                    Setor Tunai
                                </h5>
                            </div>

                            <div id="collapseTre" class="collapse" aria-labelledby="headingTre" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-group">
                                        <?php
                                        $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'retail'])->get()->result();
                                        foreach ($carilist as $r):
                                            ?>
                                            <li class="list-group-item">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="donationPay" id="<?php echo $r->kode_va ?>"
                                                           value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>">
                                                    <label class="form-check-label" for="<?php echo $r->kode_va ?>">
                                                        <?php if (!empty($r->img)): ?>
                                                            <img width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                                                        <?php else:?>
                                                            <?php echo $r->name ?>
                                                        <?php endif; ?>
                                                    </label>
                                                </div>
                                            </li>
                                        <?php endforeach;?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-30">
                        <button class="btn btn-block btn-main"><span>Bayar</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php

    echo form_close();

    ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
<script>
        $('#denomnms').number( true, 0, '', '.' );
    var jsonoper = <?php echo json_encode($listoperator) ?>;
    jQuery(document).on('keypress','#nooper',function () {
        var nom = $(this).val().substring(0, 4);
        if(jsonoper[nom] !== undefined){
            $.ajax({
                url:"<?php echo base_url('pulsa/operator') ?>",
                method:"GET",
                data:{kodeoperator:jsonoper[nom]},
                beforeSend:function(){
                  $("#resoper").empty();
                },
                dataType:'JSON',
                success:function (msg) {
                    $("#resoper").empty();
                    $.each(msg,function (i,v) {
                        var total = parseFloat(this.price) + parseFloat(this.admin);
                        $("#resoper").append('<option value="'+this.type+'-'+this.prodId+'|'+total+'">'+this.name+' - Rp '+total+'</option>'
                        );
                        console.log(this.name, this);
                    });
                    // console.log(msg);
                }
            })
        }
    })
</script>

    {_layFooter}