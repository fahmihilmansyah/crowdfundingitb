<div class="mydonation">
	<div class="mydonation-head">
		<div class="row col-md-12">
			<h2 class="bold">DONASI SAYA</h2>
		</div>
	</div>
	<div class="mydonation-body">
		<div>
			<table class="mydonation-table">
				<tr>
					<td><img src="<?php URI::baseURL(); ?>assets/images/donatur/idonation.png"></td>
					<td>
						<h2 class="bold">Rp. 350.000</h2>
						<span class="sm-bold">SALDO</span>
					</td>
				</tr>
			</table>
		</div>
		<hr/>
		<br clear="all"/>
		<div>
			<table 
					id="mutasi_donasi" 
					class="table table-striped table-bordered dt-responsive nowrap" 
					width="100%" 
			>
			<thead>
				<tr>
					<th>Jumlah </th>
					<th>Deskripsi</th>
					<th>Waktu</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<tr class="text-center">
					<td colspan="7"><i class="entypo-arrows-ccw"></i> Mengambil data...</td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
<br clear="all"/>
<br clear="all"/>
<br clear="all"/>