<div class="mydonation">
	<div class="mydonation-head">
		<div class="row col-md-12">
			<h2 class="bold">POIN AKSI BAIK</h2>
		</div>
	</div>
	<div class="mydonation-body">
		<div>
			<table class="mydonation-table">
				<tr>
					<td><img src="<?php URI::baseURL(); ?>assets/images/donatur/ipoin.png"></td>
					<td>
						<h2 class="bold">200 pt</h2>
						<span class="sm-bold">TOTAL POIN</span>
					</td>
				</tr>
			</table>
		</div>
		<hr/>
		<br clear="all"/>
		<div>
			<p>
				Anda bisa menukarkan poin aksi baik ini dengan berbagai macam merchandise menarik di Niatbaik Store
			</p>
			<p>
				Riwayat aksi baik anda:
			</p>
			<table 
					id="mutasi_donasi" 
					class="table table-striped table-bordered dt-responsive nowrap" 
					width="100%" 
			>
			<thead>
				<tr>
					<th>Jumlah </th>
					<th>Deskripsi</th>
					<th>Waktu</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<tr class="text-center">
					<td colspan="7"><i class="entypo-arrows-ccw"></i> Mengambil data...</td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>
	<div class="dompet-ab-topup-btn">
		<center><a class="bold">TUKARKAN</a></center>
	</div>
</div>
<br clear="all"/>
<br clear="all"/>
<br clear="all"/>