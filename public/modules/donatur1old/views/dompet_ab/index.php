<div class="dompetab">
	<div class="dompetab-head">
		<div class="row col-md-12">
			<h2 class="bold">DOMPET AKSI BAIK</h2>
		</div>
	</div>
	<div class="dompetab-body">
		<div>
			<table class="dompetab-table">
				<tr>
					<td><img src="<?php URI::baseURL(); ?>assets/images/donatur/idompet.png"></td>
					<td>
						<h2 class="bold">Rp. 350.000</h2>
						<span class="sm-bold">SALDO</span>
					</td>
				</tr>
			</table>
		</div>
		<br clear="all"/>
		<div class="tab-info">
			<!-- Nav tabs -->
			<div class="tab-nb no-shadow">
				<ul id="tab-personal" class="nav nav-tabs border-grey" role="tablist">
					<li role="presentation" class="active w33">
						<a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="no-bt">
							<center><h5 class="bold">Mutasi</h5></center>
						</a>
					</li>
					<li role="presentation" class="w33"">
						<a href="#dompetab" aria-controls="dompetab" role="tab" data-toggle="tab" class="no-bt">
							<center><h5 class="bold">Tambah Saldo</h5></center>
						</a>
					</li>
					<li role="presentation" class="w33">
						<a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" class="no-bt">
							<center><h5 class="bold">Pencairan Dana</h5></center>
						</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">
						{_fmutasi}
					</div>
					<div role="tabpanel" class="tab-pane" id="dompetab">
						{_ftsaldo}
					</div>
					<div role="tabpanel" class="tab-pane" id="messages" style="padding:0px !important">
						{_fpencairan}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="dompet-ab-topup-btn">
		<center><a class="bold">TAMBAH SALDO</a></center>
	</div>
</div>
<br clear="all"/>
<br clear="all"/>
<br clear="all"/>