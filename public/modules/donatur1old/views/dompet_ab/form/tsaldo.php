<div>
<form class="form-inline">
     <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-5">
			Jumlah penambahan saldo
		</label>
		<div class="col-xs-12 col-sm-12 col-md-7">
			<?php 
				$name  = 'data[name]';
				$value = !empty($default["name"]) ? $default["name"] : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_input($name, $value, $extra); 
			?>
			<br clear="all"/>
			<p class="pull-right">Minimal Penambahan Saldo Rp. 50.000</p>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-5">
			Metode pembayaran
		</label>
		<div class="col-xs-12 col-sm-12 col-md-7">
			<?php 
				$name  = 'data[name]';
				$value = !empty($default["name"]) ? $default["name"] : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_input($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">  
		<div class="col-xs-12 col-sm-12 col-md-12">
			<center>
			<input type="sumit" name="submit" class="btn btn-success" value="PROSES">
			</center>
		</div>
	</div>
</form>
</div>