<div>
	<form class="form-inline">
	     <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
			<label for="email" class="control-label col-xs-12 col-sm-12 col-md-5">
				Jumlah Pencairan Dana
			</label>
			<div class="col-xs-12 col-sm-12 col-md-7">
				<?php 
					$name  = 'data[name]';
					$value = !empty($default["name"]) ? $default["name"] : '';
					$extra = 'id="name" 
							  class="form-control w100" 
							  placeholder="isi disini"';

					echo form_input($name, $value, $extra); 
				?>
				<br clear="all"/>
				<p class="pull-right">Minimal Pencairan Dana Rp. 100.000</p>
			</div>
		</div>

	    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
			<label for="email" class="control-label col-xs-12 col-sm-12 col-md-5">
				Nama Bank Tujuan
			</label>
			<div class="col-xs-12 col-sm-12 col-md-7">
				<?php 
					$name  = 'data[name]';
					$value = !empty($default["name"]) ? $default["name"] : '';
					$extra = 'id="name" 
							  class="form-control w100" 
							  placeholder="isi disini"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>
	  
	    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
			<label for="email" class="control-label col-xs-12 col-sm-12 col-md-5">
				Kantor Cabang
			</label>
			<div class="col-xs-12 col-sm-12 col-md-7">
				<?php 
					$name  = 'data[name]';
					$value = !empty($default["name"]) ? $default["name"] : '';
					$extra = 'id="name" 
							  class="form-control w100" 
							  placeholder="isi disini"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

	    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
			<label for="email" class="control-label col-xs-12 col-sm-12 col-md-5">
				Nama Pemilik Rekening
			</label>
			<div class="col-xs-12 col-sm-12 col-md-7">
				<?php 
					$name  = 'data[name]';
					$value = !empty($default["name"]) ? $default["name"] : '';
					$extra = 'id="name" 
							  class="form-control w100" 
							  placeholder="isi disini"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

	    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
			<label for="email" class="control-label col-xs-12 col-sm-12 col-md-5">
				No Rekening
			</label>
			<div class="col-xs-12 col-sm-12 col-md-7">
				<?php 
					$name  = 'data[name]';
					$value = !empty($default["name"]) ? $default["name"] : '';
					$extra = 'id="name" 
							  class="form-control w100" 
							  placeholder="isi disini"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

	    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
			<label for="email" class="control-label col-xs-12 col-sm-12 col-md-5">
				&nbsp;
			</label>
			<div class="col-xs-12 col-sm-12 col-md-7">
				<input type="sumit" name="submit" class="btn btn-success pull-right" value="CAIRKAN">
			</div>
		</div>
	</form>
</div>