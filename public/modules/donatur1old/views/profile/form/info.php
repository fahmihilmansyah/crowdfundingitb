<form class="form-inline">
     <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-3">
			Nama Lengkap
		</label>
		<div class="col-xs-12 col-sm-12 col-md-9">
			<?php 
				$name  = 'data[name]';
				$value = !empty($default["name"]) ? $default["name"] : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_input($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-3">
			Email
		</label>
		<div class="col-xs-12 col-sm-12 col-md-9">
			<?php 
				$name  = 'data[name]';
				$value = !empty($default["name"]) ? $default["name"] : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_input($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-3">
			No Telpon
		</label>
		<div class="col-xs-12 col-sm-12 col-md-9">
			<?php 
				$name  = 'data[name]';
				$value = !empty($default["name"]) ? $default["name"] : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_input($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-3">
			Biografi
		</label>
		<div class="col-xs-12 col-sm-12 col-md-9">
			<?php 
				$name  = 'data[name]';
				$value = !empty($default["name"]) ? $default["name"] : '';
				$extra = 'id="name" 
						  class="form-control w100" 
						  placeholder="isi disini"';

				echo form_textarea($name, $value, $extra); 
			?>
		</div>
	</div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 mb10">                         
		<label for="email" class="control-label col-xs-12 col-sm-12 col-md-3">
			&nbsp;
		</label>
		<div class="col-xs-12 col-sm-12 col-md-9">
			<input type="sumit" name="submit" class="btn btn-success pull-right" value="SAVE">
		</div>
	</div>
</form>