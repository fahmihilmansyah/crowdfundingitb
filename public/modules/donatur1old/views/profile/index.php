<div class="profiledon">
	<div class="profiledon-head">
		<div class="row col-md-12">
			<h2 class="bold">PROFILE</h2>
		</div>
	</div>
	<div class="profiledon-body">
		<div class="tab-info">
			<!-- Nav tabs -->
			<div class="tab-nb">
				<ul id="tab-personal" class="nav nav-tabs border-grey" role="tablist">
					<li role="presentation" class="active w33">
						<a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="no-bt">
							<center><h4 class="bold">Informasi Personal</h4></center>
						</a>
					</li>
					<li role="presentation" class="w33"">
						<a href="#profiledon" aria-controls="profiledon" role="tab" data-toggle="tab" class="no-bt">
							<center><h4 class="bold">Password</h4></center>
						</a>
					</li>
					<li role="presentation" class="w33">
						<a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" class="no-bt">
							<center><h4 class="bold">Foto Profil</h4></center>
						</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">
						{_finfo}
					</div>
					<div role="tabpanel" class="tab-pane" id="profiledon">
						{_fpwd}
					</div>
					<div role="tabpanel" class="tab-pane" id="messages" style="padding:0px !important">
						{_ffoto}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br clear="all"/>
<br clear="all"/>
<br clear="all"/>