{_layHeader}
<div class="container-fluid grey">
	<div class="container">
		<div class="row">
			<br clear="all"/>
			<div class="col-md-4 right-sidebar">
				{_widget}
			</div>
			<div class="col-md-8">
				{_content}
			</div>
		</div>
	</div>
</div>
{_layFooter}

<script type="text/javascript">
	$(document).ready(function(){
		$("ul.profile-menu-list > li").hover(
			function()
			{
		    	$(this).find('a').addClass("active");
		    	var classname  = $(this).find('a').children('div').children("i").attr("menu");
		    	var rcondition = $(this).find('a').children('div').children("i").attr("remove");
		    	
		    	$(this).find('a').children('div').children("i").addClass(classname);
			},
			function()
			{
		        $(this).find('a').removeClass("active");
		    	var classname = $(this).find('a').children('div').children("i").attr("menu");
		    	var rcondition = $(this).find('a').children('div').children("i").attr("remove");

		    	$(this).find('a').children('div').children("i").removeClass(classname);			
		    }
		);
	});
	
	function PreviewImage(no) {
	    var oFReader = new FileReader();
	    oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);

	    oFReader.onload = function (oFREvent) {
	        document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
	    };
	}

	$('#mutasi_donasi').DataTable( {
    	responsive: true
	});
</script>