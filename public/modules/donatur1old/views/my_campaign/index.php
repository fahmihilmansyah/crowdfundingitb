<div class="mycampaign">
	<div class="mycampaign-head">
		<div class="row col-md-12">
			<h2 class="bold">CAMPAIGN SAYA</h2>
		</div>
	</div>
	<div class="mycampaign-body">
		<div class="row col-md-12">
			<?php
				for($i  = 0; $i <= 2; $i++)
				{
			?>
				<div class="row mycampaign-item">
					<div class="col-md-4 mycampaign-item-img hidden-xs hidden-sm">
						<img src="<?php URI::baseURL(); ?>assets/images/donatur/mycampaign/mycampaign.jpg" height="100%">
					</div>
					<div class="col-md-8 col-sm-12 mycampaign-item-konten">
						<h4 class="bold">SAVE ROHINGYA</h4>
						<span>2 Jan 2017</span>
						<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
						<div class="mycampaign-progress">
							<div class="progress">
								<div 
									class="progress-bar" 
									role="progressbar" 
									aria-valuenow="100" 
									aria-valuemin="10" 
									aria-valuemax="100" 
									style="">
								<span  class="popOver" data-toggle="tooltip" data-placement="top" title="230%"></span>  
								</div>
							</div>
							<table width="100%">
								<tr>
									<td width="30%">
										<span class="hidden-xs" style="display:block">Terkumpul</span>
										<span class="sm-bold size14">Rp. 1.723.000</span>
									</td>
									<td align="right">
										<span class="fa fa-clock-o"></span> : 
										<span class="sm-bold size14">7 hari Lagi</span>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			<?php
				}
			?>
			<div class="row mycampaign-button">
			<a href="#">
				<div class="campaign-button">
					
				</div>
				<h3 class="bold">BUAT CAMPAIGN BARU</h3>
			</a>
			</div>
		</div>
	</div>
</div>
<br clear="all"/>
<br clear="all"/>
<br clear="all"/>