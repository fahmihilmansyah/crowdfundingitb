<div class="overview">
	<div class="overview-head">
		<div class="row col-md-12">
			<h2 class="bold">OVERVIEW</h2>
		</div>
	</div>
	<div class="overview-body">
		<div class="row col-md-12 mb10">
			<div class="overview-item">
				<table width="100%" height="100%">
					<tr>
						<td>
							<div class="overview-image hidden-xs">
								<img src="<?php URI::baseURL(); ?>assets/images/donatur/overviewdonasi.png">
							</div>
							<div class="overview-text-h">
								<h3 class="bold">Rp. 150.000</h3>
								<h4 class="sm-bold">TOTAL DONASI</h4>
							</div>
						</td>
						<td class="overview-table-border hidden-xs">
							<p class="overview-text-r">
								Total donasi yang anda lakukan. <br/> Terima kasih telah membantu untuk berdonasi.
							</p>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="row col-md-12 mb10">
			<div class="overview-item">
				<table width="100%" height="100%">
					<tr>
						<td class="overview-table-border">
							<div class="overview-image hidden-xs">
								<img src="<?php URI::baseURL(); ?>assets/images/donatur/overviewcampaign.png">
							</div>
							<div class="overview-text-h">
								<h3 class="bold">3</h3>
								<h4 class="sm-bold">CAMPAIGN</h4>
							</div>
						</td>
						<td class="overview-table-border hidden-xs">
							<p class="overview-text-r">
								Total donasi yang anda lakukan. <br/> Total penggalangan dana yang telah anda lakukan
							</p>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="row col-md-12 mb10">
			<div class="overview-item">
				<table width="100%" height="100%">
					<tr>
						<td>
							<div class="overview-image hidden-xs">
								<img src="<?php URI::baseURL(); ?>assets/images/donatur/overviewdompet.png">
							</div>
							<div class="overview-text-h">
								<h3 class="bold">Rp. 200.000</h3>
								<h4 class="sm-bold">TOTAL SALDO</h4>
							</div>
						</td>
						<td class="overview-table-border hidden-xs">
							<p class="overview-text-r">
								Total saldo yang anda miliki di dompet aksi baik.
							</p>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="row col-md-12 mb10">
			<div class="overview-item">
				<table width="100%" height="100%">
					<tr>
						<td>
							<div class="overview-image hidden-xs">
								<img src="<?php URI::baseURL(); ?>assets/images/donatur/overviewpoin.png">
							</div>
							<div class="overview-text-h">
								<h3 class="bold">200 pt</h3>
								<h4 class="sm-bold">TOTAL POIN</h4>
							</div>
						</td>
						<td class="overview-table-border hidden-xs">
							<p class="overview-text-r">
								Anda bisa menukarkan poin aksi baik ini dengan berbagai macam merchandise  menarik di Niatbaik Store
							</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<br clear="all"/>
<br clear="all"/>
<br clear="all"/>