<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Donatur extends NBFront_Controller {

	/**
	 * ----------------------------------------
	 * #NB Controller
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_modList   = array();

	protected $_data    = array();

	function __construct()
	{
		parent::__construct();

		// ----------------------------------
		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN
		// -----------------------------------
		// $this->modelsApp();

		$this->setActiveMenu();
	}

	private function modelsApp()
	{		
		$this->load->model($this->_modList);
	}

	private function setActiveMenu()
	{
		$uri                		= $this->uri->segment(2);
	
		$this->_data["menu"]		= '';
		$this->_data["profile"] 	= (!empty($uri) && $uri == 'profile')? 'profile-w' : '';
		$this->_data["campaign"] 	= (!empty($uri) && $uri == 'campaign')? 'campaign-w' : '';
		$this->_data["donasiku"] 	= (!empty($uri) && $uri == 'donasiku')? 'donasiku-w' : '';
		$this->_data["dompet"] 		= (!empty($uri) && $uri == 'dompet')? 'dompet-w' : '';
		$this->_data["poin"] 		= (!empty($uri) && $uri == 'poin')? 'poin-w' : '';
	}



	public function index($data = array())

	{

		$data 			 	= $this->_data;

		$this->session->userdata("menu","profile");

		$data["_content"] 	= $this->parser->parse('donatur/overview/index', $data, TRUE);
		$data["_widget"] 	= $this->parser->parse('donatur/widget', $data, TRUE);

		$this->parser->parse("index",$data);
	}



	public function profile($data = array())
	{

		$data 			 	= $this->_data;
		$data["_finfo"]     = $this->parser->parse('donatur/profile/form/info', $data, TRUE);
		$data["_fpwd"]      = $this->parser->parse('donatur/profile/form/password', $data, TRUE);
		$data["_ffoto"]     = $this->parser->parse('donatur/profile/form/foto', $data, TRUE);
 		$data["_content"] 	= $this->parser->parse('donatur/profile/index', $data, TRUE);
		$data["_widget"] 	= $this->parser->parse('donatur/widget', $data, TRUE);

		$this->parser->parse("index",$data);

	}

	public function mycampaign()
	{
		$data 			 	= $this->_data;
 		$data["_content"] 	= $this->parser->parse('donatur/my_campaign/index', $data, TRUE);
		$data["_widget"] 	= $this->parser->parse('donatur/widget', $data, TRUE);

		$this->parser->parse("index",$data);
	}

	public function mydonation()
	{
		$data 			 	    = $this->_data;

 		$data["_content"] 	= $this->parser->parse('donatur/my_donation/index', $data, TRUE);
		$data["_widget"] 	= $this->parser->parse('donatur/widget', $data, TRUE);

		$this->parser->parse("index",$data);
	}

	public function dompetku()
	{
		$data 			 	    = $this->_data;
		$data["_fmutasi"]     	= $this->parser->parse('donatur/dompet_ab/form/mutasi', $data, TRUE);
		$data["_ftsaldo"]      	= $this->parser->parse('donatur/dompet_ab/form/tsaldo', $data, TRUE);
		$data["_fpencairan"]    = $this->parser->parse('donatur/dompet_ab/form/pencairan', $data, TRUE);
 		$data["_content"] 		= $this->parser->parse('donatur/dompet_ab/index', $data, TRUE);
		$data["_widget"] 		= $this->parser->parse('donatur/widget', $data, TRUE);

		$this->parser->parse("index",$data);
	}

	public function mypoin()
	{
		$data 			 	    = $this->_data;

 		$data["_content"] 	= $this->parser->parse('donatur/poin_ab/index', $data, TRUE);
		$data["_widget"] 	= $this->parser->parse('donatur/widget', $data, TRUE);
		$this->parser->parse("index",$data);
	}
}

