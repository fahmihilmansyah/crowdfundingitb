<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statis_page_model extends CI_Model {

	public function get_konten($key = array())
	{
		$this->db->select('*');
		$this->db->from('nb_v_spage_other');
		$this->db->where($key);
		
		$query = $this->db->get();
		
		return $query;
	}

	public function count_all($key = array())
	{
		$this->db->where($key);
		$this->db->from('nb_v_spage_other');
		return $this->db->count_all_results(); 
	}

	
}