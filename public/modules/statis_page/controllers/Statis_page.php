<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Statis_page extends NBFront_Controller {

	/**
	 * ----------------------------------------
	 * #NB Controller
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_modList   = array("Statis_page_model" => "spm");

	protected $_data    = array();

	function __construct()
	{
		parent::__construct();
		// -----------------------------------
		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN
		// -----------------------------------
		$this->modelsApp();
	}

	private function modelsApp()
	{		
		$this->load->model($this->_modList);
	}

	public function index($param = '')
	{
		$data 					= $this->_data;

		$cekHalamanJenis = $this->spm->count_all(array("name_key" => $param, "is_active" => "1"));
		
		if(empty($param) || $cekHalamanJenis <= 0)
		{
			show_404();
			return false;
		}

		$show_konten = $this->spm->get_konten(array("name_key" => $param, "is_active" => "1"));
		$data["sum_konten"]  = !empty($show_konten->num_rows()) ? $show_konten->num_rows() : 0;
		$data["show_konten"] = !empty($show_konten->num_rows()) ? $show_konten->row_array() : array();
 
		$this->parser->parse("index",$data);
	}

}

