{_layHeader}
	<div id="statispage">
		<div class="container-fluid">
			<div class="row">
				<div style="overflow: hidden; max-height: 540px;">
					<div class="spage-img">
						<?php 
							$img = !empty($show_konten["img"]) ? "spage_other/" . $show_konten["img"] : 'bgprofil.jpg';
						?>
						<img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img; ?>" style="min-width: 1366px;">
					</div>
					<div class="spage-title">
						<div class="spage-title-container">
							<center>
							<?php if(!empty($show_konten["title"]))
							{ ?>
								<h1 class="bold"><?php echo $show_konten["title"];?></h1>
							<? }
							else
							{?>
							<h1 class="bold">PROFIL YDSF</h1>
							<?php
								}
							?>
							
							<?php if(!empty($show_konten["title"]))
							{ ?>
								<p style="width:60%"><?php echo $show_konten["stitle"];?></p>
							<? }
							else
							{?>
							<p style="width:60%">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
							</p>
							<?php
								}
							?>
							
							</center>
						</div>
					</div>
				</div>

				<div class="spage-konten">
					<?php 
						if(!empty($show_konten["desc"]))
						{
							echo $show_konten["desc"];
						}
						else
						{
					?>
						<div class="alert alert-warning" style="padding: 20px;">
							Konten belum diisi
						</div>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>
{_layFooter}