<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Shadaqah extends NBFront_Controller {



    /**

     * ----------------------------------------

     * #NB Controller

     * ----------------------------------------

     * #author 		    : Fahmi Hilmansyah

     * #time created    : 4 January 2017

     * #last developed  : Fahmi Hilmansyah

     * #last updated    : 4 January 2017

     * ----------------------------------------

     */



    private $_modList   = array("shadaqah/Donasi_model" => "donasimdl");



    protected $_data    = array();



    function __construct()

    {

        parent::__construct();



        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

    }



    private function modelsApp()

    {

        $this->load->model($this->_modList);

    }



    public function index($data = array())

    {
        if($_POST){
            if(!empty($this->input->post('subnominal'))){
                $datadonat = array(
                    'prevnomDonation' => $this->input->post('donationNominal'),
                    'prevjenisDonation' => $this->input->post('otherJenis'),
                    'prevcomDonation' => $this->input->post('donationCommment'),
                    'statusActive' => 'active',
                );
                $this->session->set_userdata($datadonat);
            }
            if(!empty($this->input->post('subbayar'))){
                $jenis = $this->session->userdata('prevjenisDonation');
                $nominal = $this->session->userdata('prevnomDonation');
                $comment = $this->session->userdata('prevcomDonation');
                $donationName = $this->input->post('donationName');
                $donationEmail = $this->input->post('donationEmail');
                $donationPhone = $this->input->post('donationPhone');
                $methodpay = $this->input->post('donationPay');
                $notrx = date('Ymd') . sprintf('%05d', $this->geninc());
                $uniqcode = $this->genUniq($nominal);
                $insertData = array(
                    'no_transaksi' => $notrx,
                    'nominal' => $nominal,
                    'trx_date' => date('Y-m-d'),
                    'jenistrx' => $jenis,
                    'donatur' => 0,/*ubah donaturnya jika login maka akan mengambil session loginnya*/
                    'bank' => $methodpay,
                    'comment' => $comment,
                    'uniqcode' => $uniqcode,
                    'namaDonatur' => $donationName,
                    'emailDonatur' => $donationEmail,
                    'telpDonatur' => $donationPhone,
                    'nomuniq' => $nominal + $uniqcode,
                    'validDate' => date('Y-m-d', strtotime('+2 days')),
                    'crtd_at' => date("Y-m-d H:i:s"),
                    'edtd_at' => date("Y-m-d H:i:s"),
                );
                $this->donasimdl->custom_insert("nb_trans_program", $insertData);
		
                $array_items = array(
                    'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive');
                $this->session->unset_userdata($array_items);
                $datadonat = array(
                    'doneDonation' => $notrx,
                );
                $this->session->set_userdata($datadonat);
		
            }
            redirect('shadaqah');
        }else {

            $data 					= $this->_data;

            $data["_fdonasi"] 		= $this->parser->parse('form/donasi', $data, TRUE);
            $data["_fpembayaran"] 	= $this->parser->parse('form/pembayaran', $data, TRUE);

            $this->parser->parse("index",$data);
        }

    }
    private function geninc()
    {
        $kondisi['where'] = array('id' => $this->db->escape(1));
        $generate = $this->donasimdl->custom_query("nb_inc", $kondisi)->result();
        $tgl = $generate[0]->tgl;
        $uniqinc = 0;
        if ($tgl != date("Y-m-d")) {
            $uniqinc = $uniqinc + 1;
            //echo $uniqinc."||".$tgl;
        } else {
            $uniqinc = $generate[0]->inc + 1;
            //echo "else : ".$uniqinc;
        }
        $this->donasimdl->custom_update("nb_inc", array('inc' => $uniqinc, 'tgl' => date("Y-m-d")), array('id' => $this->db->escape(1)));
        //echo $this->db->last_query();
        return $uniqinc;
    }
    private function genUniq($nominal = 0)
    {
        $condition['where'] = array(
            $this->db->escape($nominal) . ' BETWEEN' => ' minlimit and maxlimit'
        );
        $generate = $this->donasimdl->custom_query("nb_incuniq", $condition)->result();
        $uniqid = $generate[0]->id;
        $uniqinc = $generate[0]->uniqinc + 1;
        if ($uniqinc > $generate[0]->maxuniq) {
            $uniqinc = $generate[0]->minuniq;
        }
        $this->donasimdl->custom_update("nb_incuniq", array('uniqinc' => $uniqinc), array('id' => $uniqid));
        return $uniqinc;
    }

}

