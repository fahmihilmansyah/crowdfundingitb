{_layHeader}
	<div id="legalitas">
		<div class="container-fluid">
			<div class="row">
				<div id="imglegalitas" class="legalitas">
					<center>
						<h1 class="bold">LEGALITAS</h1>
						<br/>
						<p style="width:50%">
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
						</p>
					</center>
				</div>
							
				<div class="konten">
					<div class="row col-sm-6">
						<h3 class="sm-bold">LEGALITAS</h3>
						<p>AKTA NOTARIS</p>
						<p>Abdurrazaq Ashiblie, SH</p>
						<p>Nomor 31 tanggal 14 April 1987</p>
						<p>Diperbaharui:</p>
						<p>Wachid Hasyim, SH</p>
						<p>Nomor 61 tanggal 19 Juli 1995</p>
					</div>
					<div class="row col-sm-6">
						<h3 class="sm-bold">REKOMENDASI</h3>
						<p>Menteri Agama Republik Indonesia</p>
						<p>Nomor B. IV/02/HK.03/6276/1989</p>
						<p>SK Menteri Agama Republik Indonesia</p>
						<p>Nomor 523 Tanggal 10 Desember tahun 2001</p>
					</div>
				</div>
			</div>
		</div>
	</div>
{_layFooter}