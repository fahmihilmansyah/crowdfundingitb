{_layHeader}

<?php
$hidden_form = array('id' => !empty($id) ? $id : '');

echo form_open_multipart($_lgn_default, array('id' => 'fdefault'), $hidden_form);

?>
<div class="valign-box">
    <div class="container">
        <div class="card no-margin">
            <div class="card-body pt-30 pr-30 pb-30 pl-30">
                <div class="row">
                    <div class="col-md-8 valign-wrapper">
                        <div class="valign-box text-center">
                            <img src="<?php URI::baseURL(); ?>assets/img/img-login.svg" class="img-reg img-login" alt="Image">
                        </div>
                    </div>
                    <div class="col-md-4 valign-wrapper">
                        <div class="valign-box">
                            <div class="text-center mb-15">
                                <div class="h3 colo-second title-font bold lh-1">Ganesha Bisa</div>
                                <div class="color-second">
                                    <span class="text-muted">Silakan Login.</span>
                                </div>
                            </div>
                            <form class="reg-box">
                                <div class="form-group relative-box">
                                    <i class="icofont icofont-envelope"></i>
                                    <input type="text" name="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group relative-box">
                                    <i class="icofont icofont-ui-lock"></i>
                                    <input type="password" name="pwd" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                <label class="custom-control-label text-small" style="padding-top:2px;" for="customCheck1">Ingat saya</label>
                                            </div>
                                        </div>
<!--                                        <div class="col-6 text-right">-->
<!--                                            <a href="lupa-password.html" class="text-small">Lupa Password?</a>-->
<!--                                        </div>-->
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-main btn-reg"><i class="icofont-login"></i> Login</button>
                                </div>
                            </form>
                            <div class="text-center mt-30">
                                Belum punya akun? <a href="<?php echo URI::baseURL(); ?>register" class="colcor-main">Daftar</a>
                            </div>
<!--                            <div class="mt-20">-->
<!--                                <div class="text-atau in-bglight mb-25">-->
<!--                                    <span>Atau</span>-->
<!--                                </div>-->
<!--                                <a href="#" class="btn btn-facebook btn-block"><i class="icofont-facebook mr-20"></i> Login dengan Facebook</a>-->
<!--                                <a href="#" class="btn btn-google btn-block mt-15"><i class="icofont-google-plus mr-20"></i> Login dengan Google</a>-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo form_close();?>


<script type="text/javascript">

</script>

{_layFooter}