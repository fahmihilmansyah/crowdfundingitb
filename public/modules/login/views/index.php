{_layHeader}

<div class="content">
    <div class="mb-10">&nbsp;</div>
<div class="container full-height valign-wrapper">
    <div class="valign-box">
        <div class="content">
             <div class="text-center">
                <img src="<?php URI::baseURL(); ?>assets/mganeshabisa/img/img-login.svg" class="img-reg img-login" alt="Image">
            </div>
            <div class="text-center mb-15">
                <div class="h3 colo-second title-font bold lh-1">MumuApps</div>
                <div class="color-second">
                    <span class="text-muted">Silakan Login.</span>
                </div>
            </div>

            <?php
            $hidden_form = array('id' => !empty($id) ? $id : '');

            echo form_open_multipart($_lgn_default, array('id' => 'fdefault','class'=>'reg-box'), $hidden_form);

            ?>
            <form class="reg-box">
                <div class="form-group relative-box">
                    <i class="icofont icofont-envelope"></i>
                    <input type="text" class="form-control" name="email" placeholder="Email">
                </div>
                <div class="form-group relative-box">
                    <i class="icofont icofont-ui-lock"></i>
                    <input type="password" name="pwd" class="form-control" placeholder="Password">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label text-small" style="padding-top:2px;" for="customCheck1">Ingat saya</label>
                            </div>
                        </div>
<!--                        <div class="col-6 text-right">-->
<!--                            <a href="lupa-password.html" class="text-small">Lupa Password?</a>-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-main btn-reg"><i class="icofont-login"></i> Login</button>
                </div>
                <div class="text-center mt-30">
                    Belum punya akun? <a href="<?php echo URI::baseURL(); ?>register" class="colcor-main">Daftar</a> atau <a href="<?php URI::baseURL(); ?>register/lupapass" class="color-main">Lupa Password</a>
                </div>
<!--                <div class="mt-20">-->
<!--                    <div class="text-atau in-bglight mb-25">-->
<!--                        <span>Atau</span>-->
<!--                    </div>-->
<!--                    <a href="#" class="btn btn-facebook btn-block"><i class="icofont-facebook mr-20"></i> Login dengan Facebook</a>-->
<!--                    <a href="#" class="btn btn-google btn-block mt-15"><i class="icofont-google-plus mr-20"></i> Login dengan Google</a>-->
<!--                </div>-->
                <?php echo form_close();?>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>

{_layFooter}