{_layHeader}

<div class="container-fluid grey">

<div class="row">

	<div class="col-md-4">
		&nbsp;
	</div>
	<div class="panel panel-default panel-login-nb col-md-4" style="padding:0px;">

		<div class="panel-heading panel-login-head bg-blue">Halaman Login!</div>

		<div class="panel-body">
				<?php
				    $hidden_form = array('id' => !empty($id) ? $id : '');

				    echo form_open_multipart($_lgn_default, array('id' => 'fdefault'), $hidden_form);

				?>

				<div class="form-group col-xs-12">

                    <p class="f19">Selamat bergabung di komunitas berbagi GaneshaBisa</p>

					<?php
						if($this->session->userdata("error_msg")){
					?>

						<p class="alert alert-warning" style="margin:0px; margin-top:10px; text-align: center">
							<?php echo $this->session->userdata("error_msg"); ?>
						</p>
						
					<?php
						}
					?>

				</div>	

				<div class="form-group col-xs-12"><label>Email</label>

					<input type="text" class="form-control" name="email" placeholder="input disini" required>

				</div>

				<div class="form-group col-xs-12"><label>Password</label>

					<input type="password" class="form-control" name="pwd" placeholder="input disini" required>

				</div>	

				<div class="form-group col-xs-12">

				<input type="submit" class="form-control btn btn-success sm-bold" name="" value="LOGIN">

				</div>	

				<?php echo form_close();?>


				<div class="form-group col-xs-12">

					<div class="panel-login-hr">
						<p class="panel-login-or">OR</p>
					</div>

				</div>	

				<?php
				    $hidden_form = array('id' => !empty($id) ? $id : '');

				    echo form_open_multipart($_lgn_facebook, array('id' => 'fmartikel'), $hidden_form);

				?>
				<div class="form-group col-xs-12">

                    <button class="btn btn-block btn-social btn-facebook">
                      <span class="fa fa-facebook"></span> <center>Login dengan Facebook</center>
                    </button>

				</div>	
				<?php echo form_close();?>
				
				<?php
				    $hidden_form = array('id' => !empty($id) ? $id : '');

				    echo form_open_multipart($_lgn_google, array('id' => 'ffacebook'), $hidden_form);

				?>
				<div class="form-group col-xs-12">

                    <button class="btn btn-block btn-social btn-google">
                      <span class="fa fa-google"></span> <center>Login dengan Google+</center>
                    </button>

				</div>	
				<?php echo form_close();?>

				<div class="form-group col-xs-12" style="border-top:1px solid #ccc">

                    <p class="f19">Belum bergabung? <a href="<?php URI::baseURL(); ?>register">Daftar</a></p>

				</div>	

		</div>

	</div>

	<div class="col-md-4">
		&nbsp;
	</div>
</div>

</div>

<script type="text/javascript">

</script>

{_layFooter}