<html>
<head></head>
<body>
<table >
<tbody>
<tr>
<td>&nbsp;</td>
<td bgcolor="#FFFFFF">
<div>
<table>
<tbody>
<tr>
<td>
<div>Adi Supriadi, <br />Mohon segera transfer sebelum:<br /><strong>Rabu, 22 Februari 2017, 13:56 WIB&nbsp;</strong><br /><br /></div>
<h2><strong>Lakukan Transfer sebesar:</strong><br /><strong>Rp 20.115</strong></h2>
<div>
<div><strong>Tepat hingga 3 digit terakhir</strong></div>
</div>
<div><strong><em>(perbedaan nilai transfer akan menghambat proses verifikasi donasi)</em></strong></div>
<div><strong><br /></strong>Ke:<br /><strong>Bank Central Asia (BCA) </strong><br /><strong>Cabang KCU Cibubur </strong><br /><strong>No Rek. 7401 18 1800</strong><br /><strong>a.n Yayasan Kita Bisa <br /></strong><br />Donasi / Dana Anda akan terverifikasi oleh sistem dalam 1 hari kerja*.<br /><br />Bila hingga Rabu, 22 Februari 2017, 13:56 WIB donasi / dana belum kami terima, maka donasi akan dibatalkan oleh sistem.<br /><br /></div>
<div>*Apabila transfer di luar jam kerja bank atau pada hari libur, maka verifikasi data akan mengalami keterlambatan.<br /><br /><em>P.S. Ingin lebih mudah berdonasi? Isi saldo <strong>Dompet Kebaikan</strong> Anda; transfer sekali, donasi berkali-kali!</em> <br /><br />Salam Hormat,</div>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
</body>
</html>