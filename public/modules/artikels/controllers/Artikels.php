<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Artikels extends NBFront_Controller {



	/**
	 * ----------------------------------------
	 * #NB Controller
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_modList   = array('Artikels_model' => 'artikel');
	protected $_data    = array();

	function __construct()
	{
		parent::__construct();

		// -----------------------------------
		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN
		// -----------------------------------
		$this->modelsApp();
		$this->load->library("pagination");
	}

	private function modelsApp()
	{
		$this->load->model($this->_modList);
	}

	public function index($data = array())
	{
		$data = $this->_data;

		$data["_data_artikel_popular"] = !empty($this->artikel->popular()->result()) ? $this->artikel->popular()->result() : array() ;
		$data["_data_artikel_recent"] = !empty($this->artikel->recent()->result()) ? $this->artikel->recent()->result() : array() ;
		
		$data["_widget"] = $this->parser->parse('artikels/widget', $data, TRUE);

		// Pagination       
        $config = array();
        $config["base_url"] = base_url() . "news/list";

        $num_rows= $this->artikel->record_count()->num_rows();
        $total_row = !empty($num_rows) ? $num_rows : 0;

        $config['total_rows'] = $total_row; 
        $config['per_page'] = 6;
        $config['uri_segment'] = 3;
    	$choice = $config['total_rows']/$config['per_page'];
   	    $config['num_links'] = round($choice);

        $this->pagination->initialize($config);

    	$page =($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["_data_artikel_list"]    = $this->artikel->fetch_data($config["per_page"], $page);
        
        $data["links"]      			= $this->pagination->create_links();
        $data['config'] = $config;

		$this->parser->parse("index",$data);
	}

	public function detail($slug = "")
	{
		$data = $this->_data;
//		$id =($this->uri->segment(3)) ? $this->uri->segment(3) : 0;		
		/*$konten = explode("-",$konten);
        $id = $konten[count($konten) - 1];
        if(!is_numeric($id)){
            redirect("news/list");
        }*/

        $detailcampaign=  !empty($this->artikel->detail($slug)->result()) ? $this->artikel->detail($slug)->result_array() : array() ;
        $this->artikel->custom_update('nb_post',['sum_read'=>$detailcampaign[0]['sum_read'] + 1],['id'=>$detailcampaign[0]['id']]);
        $data['meta_title'] = ''.ucfirst($detailcampaign[0]['title']);
        $data['meta_tipe'] = 'MumuApps.id';
        $data['meta_description'] = $detailcampaign[0]['desc_short']." -  Infonya bisa dilihat di: ". base_url();
        $data['meta_keyword'] = $detailcampaign[0]['meta_keyword'];
        $data['meta_author'] = "MumuApps.id - ".$detailcampaign[0]['fname'];


		$data["_data_artikel_detail"] = !empty($this->artikel->detail($slug)->result()) ? $this->artikel->detail($slug)->result_array() : array() ;
		$data["_data_artikel_popular"] = !empty($this->artikel->popular()->result()) ? $this->artikel->popular()->result_array() : array() ;
		$data["_data_artikel_recent"] = !empty($this->artikel->recent()->result()) ? $this->artikel->recent()->result() : array() ;
		$data["_widget"] = $this->parser->parse('artikels/widget', $data, TRUE);
		$this->parser->parse("detail",$data);
	}
}
