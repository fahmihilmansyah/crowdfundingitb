<div class="most-popular">

	<div class="list-header">

		<div class="program-campaign-title">

			<center>

				<h2 class="bold">MOST POPULAR</h2>

				<hr class="line">

			</center>

		</div>

	</div>

	<div>

		<ul class="list-popular">

		<?php

			foreach($_data_artikel_popular as $r){

		?>

			<li>

				<div class="list-item">

					<div class="pull-left" style="margin-right:5px;">
						<?php
                          
                            $img = !empty($r->img) ? $r->img : '';
                            
                            $img = URI::existsImage('articles/mostpopular/', 
                                                          $img, 
                                                          'noimg_front/',
                                                          'no-cminilist.jpg');
                        ?>
						<img class="img-thumbnail" src="<?php echo $img; ?>" style="height:98px; width:98px; overflow: hidden;">

					</div>

					<div class="pull-left" style="width:70%;">

						<a href="<?php URI::baseURL(); ?>news/<?php echo $r->slug?>" style="color:#000"><h4 class="sm-bold"><?php echo $r->title ?></h4></a>

						<span style="font-size: 11px;">Tanggal Post: <?php echo arr::monthI(date("d-m-Y", strtotime($r->post_date))); ?></span>

					</div>

				</div>

			</li>

		<?php

			}

		?>

		</ul>

	</div>

</div>

<div class="recent-articles">

	<div class="list-header">

		<div class="program-campaign-title">

			<center>

				<h2 class="bold">RECENT ARTICLES</h2>

				<hr class="line">

			</center>

		</div>

	</div>

	<div>

		<ul class="list-articles">

		<?php

			foreach($_data_artikel_recent as $r){ 

		?>

			<li>

				<div class="list-item">

					<div>

						<a href="<?php URI::baseURL(); ?>news/<?php echo $r->slug ?>" style="color:#000"><h4 class="sm-bold"><?php echo $r->title; ?></h4></a>

						<span style="font-size:11px">Tanggal Post: <?php echo arr::monthI(date("d-m-Y", strtotime($r->post_date))); ?></span>

					</div>

				</div>

			</li>

		<?php

			}

		?>

		</ul>

	</div>

</div>