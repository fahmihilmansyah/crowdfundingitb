{_layHeader}

	<div class="container-fluid grey">


			<div class="col-md-12">

				<div class="program-campaign-title" style="margin-top:20px">

					<center>

						<h2 class="bold">ARTICLES</h2>

						<hr class="line">

					</center>

				</div>

			</div>
            <div class="row">
			<div class="col-md-8 col-sm-12 col-xs-12">

				<div class="col-md-12 col-sm-12 col-xs-12" style="border:0px solid black">

				<?php 
				if(!empty($_data_artikel_list))
				{

                     $i = 0;
					foreach($_data_artikel_list as $r)

					{
                    $i++;
				?>

					<div class="art-data col-md-12 col-sm-4 col-xs-12" style="border:0px solid black; padding:0px;">

						<div class="art-image col-md-5" style="padding:0px;">
							<?php 
								// $img = !empty($r->img) ? $r->img : 'noimage.png';
								// $fp  = FCPATH . "assets/images/articles/thumbnails/" . $img; 
								
								// if(!file_exists($fp))
								// {
								// 	$fp = base_url() . "assets/images/articles/no-image.jpg"; 
								// }else
								// {
								// 	$fp = base_url() . "assets/images/articles/thumbnails/" . $img; 
								// }
							?>

							<?php
                          
                            $img = !empty($r->img) ? $r->img : '';
                            
                            $img = URI::existsImage('articles/new_thumb/', 
                                                          $img, 
                                                          'noimg_front/',
                                                          'no-alist.jpg');
                            ?>

							<img src="<?php echo $img; ?>" height="300px" width="100%">

						</div>

						<div class="art-konten col-md-7 pull-left">

							<div>

								<h4 class="sm-bold"><?php echo $r->title; ?></h4>

								<span style="font-size: 11px">
									Tanggal Post: <?php echo arr::monthI(date("d-m-Y", strtotime($r->post_date))); ?>
								</span>
								<br/>
								<br/>
								<p>

									<?php 
										echo $r->desc_short;
									?>

								</p>
                                <?php $titls = str_replace(" ","-", $r->title);
                                /**
                                 * edited by fahmi 14 april 2017
                                 */
                                ?>
								<div class="art-btn pull-right">
                                        <div class="btnshareart pull-left" id="btnshareart-<?php echo $i;?>">
                                        <a target="_blank"
                                           href="http://www.facebook.com/sharer.php?u=<?php echo URI::encode_url(base_url()).'news/'. $r->slug ?>"
                                           class="btn btn-share pull-left share-icon fb"  title="shared facebook"></a>
										<a target="_blank"
										   href="http://twitter.com/share?text=<?php echo $r->title ?>&url=<?php echo base_url()?>news/<?php echo $r->slug ?>"
										   class="btn btn-share pull-left share-icon twitter" title="shared twitter"></a>
										<a data-text="<?php echo $r->title; ?>" data-link="<?php echo base_url('news/'.$r->slug) ?>" class="btn btn-share pull-left share-icon whatsapp" title="shared whatsapp"></a>
										<a target="_blank" href="http://plus.google.com/share?url=<?php echo URI::encode_url(base_url()).'news/'. $r->slug ?>" class="btn btn-share pull-left share-icon google" title="shared google"></a>
										</div>
										<label  class="btn btn-share pull-left share-icon share btnshare" data-shareid="<?php echo $i;?>"></label>

									<a href="<?php URI::baseURL(); ?>news/<?php echo $r->slug?>" class="btn btn-success pull-left">LIHAT SEMUA</a>

								</div>

							</div>

						</div>

					</div>

				<?php

					}
				}
				else
				{

				?>
					<div class="art-data col-md-12 col-sm-4 col-xs-12" style="border:0px solid black; padding:0px;">

						<div class="art-image col-md-5" style="padding:0px;">

							<img src="<?php URI::baseURL(); ?>assets/images/articles/thumbspec/artikelthumbbig1.jpg" height="300px" width="100%">

						</div>

						<div class="art-konten col-md-7 pull-left">

							<div>

								<h4 class="sm-bold">MENGAPA KOMETIK DISERTIFIKASI HALAL?</h4>

								<span><i class="fa fa-clock"></i> 05 Jan 2017</span>

								<p>

									Shalat wajib seharusnya dikerjakan oleh setiap hamba dalam kondisi apa pun selagi syarat taklif dipenuhi, yakni berakal dan baligh. Jika mampu dikerjakan dengan berdiri, jika tidak mampu dikerjakan dengan duduk, jika masih tidak mampu dikerjakan dengan berbaring, dan jika masih tidak mampu dikerjakan dengan isyarat.

								</p>

								<div class="art-btn pull-right">

									<a href="#s" class="btn btn-share pull-left share-icon share"></a>

									<a href="#" class="btn btn-success pull-left">LIHAT SEMUA</a>

								</div>

							</div>

						</div>

					</div>
				<?php
					}
				?>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">
					<?php
						echo $links;
					?>
				</div>

			</div>

			<div class="col-md-4 col-sm-4 col-xs-12 right-sidebar">

				{_widget}

			</div>

            </div>
	</div>

{_layFooter}
<script>
    $(document).ready(function () {
        $('.btnshareart').hide();
        jQuery(document).on('click','.btnshare',function () {
            var shareid = $(this).data('shareid');
            if($('#btnshareart-'+shareid).is(":visible")){
                $('#btnshareart-'+shareid).removeClass('animated fadeInRight');
                $('#btnshareart-'+shareid).addClass('animated fadeOutRight').hide();
//                $('#btnshareart-'+shareid).hide();
            }else{
                $('#btnshareart-'+shareid).removeClass('animated fadeOutRight');
                $('#btnshareart-'+shareid).addClass('animated fadeInRight');
                $('#btnshareart-'+shareid).show();
            }
        });
        $('.whatsapp').on("click", function(e) {
            var article = $(this).attr("data-text");
            var weburl = $(this).attr("data-link");
            var whats_app_message = encodeURIComponent(article)+" - "+encodeURIComponent(weburl);
            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

                var whatsapp_url = "whatsapp://send?text="+whats_app_message;
                window.location.href= whatsapp_url;
            }
        });
    })
</script>