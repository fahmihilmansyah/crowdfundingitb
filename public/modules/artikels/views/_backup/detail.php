{_layHeader}

	<div class="container-fluid grey">

			<div class="col-md-8">

				<div class="page-konten">
				
					<?php //foreach($_data_artikel_detail as $r){ ?>
					
					<div class="page-header">

						<h1><?php echo !empty($_data_artikel_detail[0]['title']) ? $_data_artikel_detail[0]['title'] : "-"; ?></h1>

						<span>

							<i class="fa fa-clock"></i>
<?php

?>
							Tanggal Post: <?php echo arr::monthI(date("d-m-Y", strtotime($_data_artikel_detail[0]['post_date']))); ?>

						</span>

					</div>

					<div class="page-body">

						<?php echo !empty($_data_artikel_detail[0]['desc']) ? $_data_artikel_detail[0]['desc'] : "-";?>

					</div>

					<div class="page-footer">

						<div class="page-comment">
							<div id="disqus_thread"></div>
						</div>

					</div>
					
					<?php //} ?>
					
				</div>
			</div>

			<div class="col-md-4">

				{_widget}

			</div>

	</div>
	
<script id="dsq-count-scr" src="//berzakat.disqus.com/count.js" async></script>
{_layFooter}
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

	var PAGE_URL = '<?php echo base_url()?>news/<?php echo $this->uri->segment(2)?>';
	var PAGE_IDENTIFIER = 'news/<?php echo $this->uri->segment(2)?>';

	var disqus_config = function () {
	this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
	this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
	};

	(function() { // DON'T EDIT BELOW THIS LINE
	var d = document, s = d.createElement('script');
	s.src = 'https://berzakat.disqus.com/embed.js';
	s.setAttribute('data-timestamp', +new Date());
	(d.head || d.body).appendChild(s);
	})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>