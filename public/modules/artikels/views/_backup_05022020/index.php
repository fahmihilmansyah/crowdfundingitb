{_layHeader}

<section>
    <div class="container">
        <div class="mb-25">
            <div class="h3 bold title-font lh-1 color-second">Artikel</div>
            <div class="text-muted mb-10">Berita terkini seputar ITB</div>
        </div>
        <div class="row">
            <?php
            if(!empty($_data_artikel_list)){
            foreach ($_data_artikel_list as $r){ ?>
                <?php

                $img = !empty($r->img) ? $r->img : '';

                $img = URI::existsImage('articles/',
                    $img,
                    'noimg_front/',
                    'no-alist.jpg');
                ?>
            <div class="col-md-4">
                <div class="card overflow-hidden">
                    <div class="bg-donasi"
                         style="background-image:url(<?php echo $img ?>);"></div>
                    <div class="card-body">
                        <div class="mb-25 font-title bold judul-donasi">
                            <?php echo $r->title; ?>
                        </div>
                        <div class="mb-15 text-smaller">
                            <div>
                                <i class="text-muted icofont-user-alt-3 mr-5"></i> <?php echo $r->name_userpost; ?>
                            </div>
                            <div>
                                <i class="text-muted icofont-ui-calendar mr-5"></i> <?php echo $r->post_date; ?>
                            </div>
                        </div>
                        <div>
                            <?php echo $r->desc_short; ?>
                        </div>
                        <div class="mt-20">
                            <a href="<?php URI::baseURL(); ?>news/<?php echo $r->slug?>" class="btn btn-block btn-main"><span>Selengkapnya</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php  }}else{ echo "No data";} ?>
        </div>

        <?php
        $segment = $this->uri->segment(3);
        ?>
        <div class="mt-20">
            <nav aria-label="...">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i
                                    class="icofont-rounded-left"></i></a>
                    </li>
                    <?php
                    for ($i = 0; $i <= $config['num_links']; $i++) {
                        $nseg = $i * $config['per_page'];

                        if($nseg == 0){
                            $nseg = '';
                        }
                        $actf = '';
                        $actfx = '';
                        if ($segment == $nseg) {
                            $actfx = 'active';
                            $actf = '<span class="sr-only">(current)</span>';
                            ?>
                            <li class="page-item <?php echo $actfx ?>"><a class="page-link"
                                                                          href="<?php echo $config['base_url'].'/'.$nseg ?>"><?php echo $i + 1 . " " . $actf; ?></a>
                            </li>

                            <?php
                        } else {
                            ?>
                            <li class="page-item "><a class="page-link"
                                                      href="<?php echo $config['base_url'].'/'.$nseg ?>"><?php echo $i + 1; ?></a></li>

                            <?php $actf = '';
                            $actfx = '';
                        }
                    } ?>
                    <li class="page-item">
                        <a class="page-link" href="#"><i class="icofont-rounded-right"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</section>

{_layFooter}