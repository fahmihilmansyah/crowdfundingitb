
<?php
$urlsd = '';
if (!empty($_data_artikel_detail)) {
    $img='';
    if(empty($_data_artikel_detail[0]['img'])):
        $img =  '';
        else:
    $img = !empty($_data_artikel_detail[0]['img']) ? $_data_artikel_detail[0]['img'] : '';

    $img = URI::existsImage('articles/',
        $img,
        'noimg_front/',
        'no-amini.jpg');
    endif;
    $urlsd = base_url().'news/'.$_data_artikel_detail[0]['slug'];
}
$ndata['META_TITLE'] = !empty($meta_title) ?ucfirst( $meta_title) :$_appTitle;
$ndata['META_KEYWORD'] = !empty($meta_keyword) ?$meta_keyword :'';
$ndata['META_AUTHOR'] = !empty($meta_author) ?$meta_author :'';
$ndata['META_DESCRIPTION'] = !empty($meta_description) ?$meta_description :'';
$ndata['META_URL'] = !empty($urlsd) ?$urlsd :'';
$ndata['META_IMG'] = !empty($img) ?$img :'';
$ndata['META_TIPE'] = !empty($meta_tipe) ?$meta_tipe :'';
echo $this->parser->parse('template/header_campaign', $ndata, TRUE);

?>
<?php

//$img = !empty($_data_artikel_detail[0]['img']) ? $_data_artikel_detail[0]['img'] : '';
//
//$img = URI::existsImage('articles/',
//    $img,
//    'noimg_front/',
//    'no-alist.jpg');
if(!empty($img)):
?>

<img src="<?php echo $img ?>" alt="Image" style="width: 100%;">
<?php endif; ?>
<div class="content">
    <div class="title-font judul-donasi-big bold mt-15 mb-20"><?php echo $_data_artikel_detail[0]['title'] ?></div>
    <div class="mt-15 mr-15 text-smaller">
                <span>
                    <i class="text-muted icofont-user-alt-3 mr-5"></i> <?php echo $_data_artikel_detail[0]['author'] ?>
                </span>
        <span class="ml-15">
                    <i class="text-muted icofont-ui-calendar mr-5"></i> <?php echo date("d M Y",strtotime($_data_artikel_detail[0]['edtd_at'])) ?>
                </span>
    </div>
    <div class="mt-25 mb-40">
        <?php echo $_data_artikel_detail[0]['desc'] ?>
    </div>
    <div class="mb-20">
        <div class="title-font bold color-second mb-10">Artikel Populer</div>
        <div class="pendana-wrapper">
            <?php foreach ($_data_artikel_popular as $r){?>

            <?php
                $img='';
                if(empty($r['img'])):
                    $img = $r['img_urls'];
                else:
            $img = !empty($r['img']) ? $r['img'] : '';

            $img = URI::existsImage('articles/',
                $img,
                'noimg_front/',
                'no-alist.jpg');
            endif;
            ?>
            <a href="<?php URI::baseURL(); ?>news/<?php echo $r['slug']?>" class="pendana-box blog-link hoverable">
                <div class="blog-img" style="background-image:url(<?php echo $img ?>);"></div>
                <div class="blog-text">
                    <div class="title-font semibold">
                        <?php echo $r['title'] ?>
                    </div>
                    <div class="text-muted text-small mt-5"><?php echo date("d M Y",strtotime($r['post_date'])) ?></div>
                </div>
            </a>
            <?php } ?>
        </div>
        <a href="<?php URI::baseURL(); ?>news/list" class="btn btn-main btn-block mt-15"><span>Lihat Artikel Lainnnya</span></a>
    </div>
<!--</div>-->

<!--End Off New Design-->
{_layFooter}