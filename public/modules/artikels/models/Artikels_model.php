<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikels_model extends CI_Model {

	private $_table1 = "v_artikel_list";

  	private function _kunci($data = array()){
        $result = $data;

        if(!is_array($data))
            $result = array("id" => $data);

        return $result;
    }

    function changeTable($table = "")
    {
        $this->_table1 = $table;
    }

	function data($key = "")
	{
        $this->db->select("*");
        $this->db->from($this->_table1);	

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
	}

    function record_count($condition = array()){
        $this->changeTable("v_artikel_list");
        $result = $this->data($condition)->get();
        return $result;
    }

    function fetch_data($limit, $offset, $condition = array())
    {
        $this->changeTable("v_artikel_list");
        $this->db->limit($limit, $offset);
        $result = $this->data($condition)->get();

        return !empty($result->result()) ? $result->result() : new stdClass();
    }

    function fetch_data_mp()
    {
        $this->changeTable("v_artikel_list");
        $this->db->limit('4');
        $this->db->order_by('sum_read','DESC');
        $result = $this->data()->get();

        return !empty($result->result()) ? $result->result() : new stdClass();
    }
	
	function detail($slug){
		$this->db->select("nb_post.*, nb_sys_users.fname");
		$this->db->from("nb_post");
		$this->db->join("nb_sys_users","nb_post.usr_post = nb_sys_users.id",'left');
		$this->db->where('nb_post.slug', $slug);
		$query = $this->db->get();
		
		return $query;
	}
	
	function popular(){
		$sql="select * from nb_post order by sum_read desc limit 3";
		return $this->db->query($sql);
	}
	
	function recent(){
		$sql="select * from nb_post order by id desc limit 3";
		
		return $this->db->query($sql);
	}

    function custom_update($table=null, $data=array(),$where=array()){
        $this->db->trans_begin();
        $this->db->where($where);
        $this->db->update($table, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            die("ERROR: UPDATING DATA, PLEASE CONTACT ADMINISTRATOR WEB");
        }
        else
        {
            $this->db->trans_commit();
        }
    }
	
}

?>