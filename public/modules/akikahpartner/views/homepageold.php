{_layHeader}

<?php
$url = implode("/", $this->uri->segment_array());
$dats = array('urllink' => $url);
$this->session->set_userdata($dats);
$sessionsl = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
    $this->session->userdata(LGI_KEY . "login_info")['email'] : '';
?>
<div class="content pr-15 pl-15 bg-white" style="padding-top: 10px;">

    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'kurban/kurban/' . $this->uri->segment(3), array('id' => 'fmdonasi', 'class' => 'fmdonasi mt-20'), $hidden_form);

    ?>
    <img src="http://news.unair.ac.id/wp-content/uploads/2019/07/Stres-pada-Hewan-Kurban-Pengaruhi-Kualitas-Daging.jpg">
    <div class="mb-20">
       &nbsp;
    </div>
    <div class="row">
        <div class="col-12">

            <h5 class="">Kenapa kurban di MuMuApps:</h5>
            <div class="row">
                <div class="col-6">
                    <div  class="rounded text-white p-2 m-1" style="background-color: #09A59D; font-size: 14px;"><p><h5>Sesuai Syariah</h5></p>
                        <p>Kambing dan sapi yang disalurkan memenuhi kriteria untuk jadi hewan qurban.</p>
                    </div>
                </div>
                <div class="col-6">
                    <div  class="rounded text-white p-2 m-1" style="background-color: #09A59D; font-size: 14px;">
                        <p><h5>Harga Terjangkau</h5></p>
                        <p>Harga hewan qurban murah dengan kualitas daging yang baik.</p>
                    </div>
                </div>
                <div class="col-6">
                    <div  class="rounded text-white p-2 m-1" style="background-color: #09A59D; font-size: 14px;">
                        <p><h5>Tepat Sasaran</h5></p>
                        <p>Qurban akan disalurkan ke saudara kita yang benar-benar membutuhkan.</p>
                    </div>
                </div>
                <div class="col-6">
                    <div class="rounded text-white p-2 m-1" style="background-color: #09A59D; font-size: 14px;">
                        <p><h5>Transparan</h5></p>
                        <p>Dapat update dan laporan lengkap penyaluran setelah pemotongan hewan qurban.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="mb-5">
                &nbsp;<?php function isMobileDevice() {
                    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
                }
                $lalink = 'WCLINKAJA';
                if(isMobileDevice()){
                    $lalink = 'APPLINKAJA';
                    //Your content or code for mobile devices goes here
                }
                ?>
            </div>
            <h5>Silakan Pilih Partner : </h5>
            <div class="row">
                <?php foreach ($list_mitra as $r): ?>
                <div class="col-6" >
                    <a href="<?php echo base_url('kurban/kurban?jtrx=emoneypil&pgtrx='.$lalink.'&cpartner='.$r->id)?>" class="rounded text-white p-2 m-1 d-flex align-items-center" style="background-color: <?php echo empty($r->bg_color)?'#d6d6d6':$r->bg_color ?>;height: 150px;">
                        <?php $imgfile = !empty($r->path_img)?base_url() . "assets/images/logo/" .$r->path_img:''; ?>
                        <img width="200" src="<?php echo !empty($imgfile)?$imgfile:$r->url_img ?>">
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>



    {_layFooter}

