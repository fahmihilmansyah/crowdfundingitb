<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nbauth_model extends CI_Model {

	private $_table1 = "v_donaturs_data";

    function __construct()
    {
        parent::__construct();
        $this->load->library("nbauth");
    }

  	private function _kunci($data = array()){
        $result = $data;

        if(!is_array($data))
            $result = array("id" => $data);

        return $result;
    }

    function changeTable($table = "")
    {
        $this->_table1 = $table;
    }

	function data($key = "")
	{
        $this->db->select("*");
        $this->db->from($this->_table1);	

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
	}

    function data_dynamic($table = "" , $key = "")
    {
        $this->db->select("*");
        $this->db->from($table);    

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    function existsUser($param = array())
    {
        $email  = $param["email"];
        $pwd    = hash::getCustomHash($param["pwd"]);

        $key    = array("email" => $email, "pwd" => $pwd);
        $result = $this->data($key)->get();

        return $result;
    }

    function insert_token($token, $validate, $donatur)
    {
        $this->db->trans_begin();
        
        $data = array(
                "donatur"       => $donatur,
                "code"          => $token,
                "valid_until"   => $validate,
                "donatur_ip"    => $_SERVER['REMOTE_ADDR']
            );

        $this->db->insert("donatur_tokens", $data);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    function cekToken($param = "")
    {
        $key    = array("code" => $param["token"], "donatur" => $param["uid"], "donatur_ip" => $param["uip"]);
        
        $result = $this->data_dynamic("donatur_tokens", $key)->get();

        return $result; 
    }

    function existsOauthUser($key = array())
    {
        $result = $this->data_dynamic("v_donatur_oauth", $key)->get();

        return !empty($result->num_rows()) ? $result->num_rows() : 0;
    }

    function getUserOauthID($key = array())
    {
        $result = $this->data_dynamic("donaturs",$key)->get();

        return $result;
    }

    function getUserOauth($key = array())
    {
        $this->db->from("donaturs a");
        $this->db->join("donaturs_d b","b.donatur = a.id");
        $this->db->where($key);
        
        $result = $this->db->get()->row();

        return $result;
    }

    function insert_oauthUser($data = array())
    {
        $this->db->insert("donaturs", $data);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array(false, 0);
        } else {
            $this->db->trans_commit();
            return array(true, $this->db->insert_id());
        }
    }

    function insert_oauthData($data)
    {
        $this->db->insert("donatur_oauth", $data);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }


}

?>