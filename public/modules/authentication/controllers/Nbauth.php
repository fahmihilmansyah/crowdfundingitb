<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Nbauth extends NBFront_Controller {



	/**

	 * ----------------------------------------

	 * #NB Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_modList   = array("authentication/Nbauth_model" => "auth");



	protected $_data    = array();



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------

		// $this->modelsApp();

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	public function auten($param = '')

	{

		$email  = !empty($this->input->post("email")) ? $this->input->post("email") : "";

		$pwd    = !empty($this->input->post("pwd")) ? $this->input->post("pwd") : "";





		if($param == "login" || !empty($param))

		{

			$type 	= !empty($this->input->get('t')) ? $this->input->get('t') : 'default';

			

			if($type == "default")

			{

				$result = $this->nbauth_front->getAuthDefault($email, $pwd);

				if($result)

				{

                    $preflink = $this->session->userdata('urllink');
            redirect($preflink);

				}

				else

				{
                    $message = 'Anda belum terdaftar di komunitas berbagi GaneshaBisa! <br/> ';
                    $message.= '<a href="' . base_url() . 'register">Daftar sekarang!</a>';
                    $this->session->set_flashdata('error_msg', $message);

                    redirect('auten/page/login');

				}

			}



			if($type == "facebook")

			{

				header('Location: ' . facebook::getFacebookURL());

			}



			if($type == "google")

			{

				header('Location: ' . base_url() . 'register/google');

			}

		}

		else

		{

			// redirect to error page

		}

	}



	public function facebook()

	{

		

        $facebook = facebook::getFacebookAuth();



        $helper = $facebook->getRedirectLoginHelper();



        $permissions = ['email']; // optional



        try 

        {

            if (isset($_SESSION['facebook_access_token'])) 

            {

                $accessToken = $_SESSION['facebook_access_token'];

            } 

            else 

            {

                $accessToken = $helper->getAccessToken();

            }

        } 

        catch(Facebook\Exceptions\FacebookResponseException $e) 

        {

            // When Graph returns an error

            echo 'Graph returned an error: ' . $e->getMessage();

            exit;

        } 

        catch(Facebook\Exceptions\FacebookSDKException $e) 

        {

            // When validation fails or other local issues

            echo 'Facebook SDK returned an error: ' . $e->getMessage();

            exit;

        }



        if(isset($accessToken)) 

        {



            if (isset($_SESSION['facebook_access_token'])) 

            {

                $facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);

            } 

            else 

            {

                // getting short-lived access token

                $_SESSION['facebook_access_token'] = (string) $accessToken;



                // OAuth 2.0 client handler

                $oAuth2Client = $facebook->getOAuth2Client();



                // Exchanges a short-lived access token for a long-lived one

                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);



                $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;



                // setting default access token to be used in script

                $facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);

            }



            // redirect the user back to the same page if it has "code" GET variable

            if (isset($_GET['code'])) {

                redirect('auten/redirect/facebook');

            }



            // getting basic info about user

            try 

            {

                $profile_request = $facebook->get('/me?fields=name,email,first_name,last_name,gender,picture');



                $profile = $profile_request->getGraphNode()->asArray();

            }

            catch(Facebook\Exceptions\FacebookResponseException $e) 

            {

                // When Graph returns an error

                echo 'Graph returned an error: ' . $e->getMessage();



                session_destroy();



                // redirecting user back to app login page

            	redirect('auten/page/login');

                exit;

            } 

            catch(Facebook\Exceptions\FacebookSDKException $e) 

            {

                // When validation fails or other local issues

                echo 'Facebook SDK returned an error: ' . $e->getMessage();

            	redirect('auten/page/login');

                exit;

            }



            // SET TOKEN

			$token 		= md5($profile["email"] . $profile["id"] . md5(strtotime(date("ymdhis"))) . md5(time()));

			$validdate 	= date("Y-m-d H:i:s", strtotime("+1 hour"));



            $data["email"]          = $profile["email"];

            $data["oauth_uid"]      = $profile["id"];

            $data["oauth_provider"] = "facebook";

            $data["login_type"]		= "oauth";



            $existsOauthUser = $this->auth->existsOauthUser($data);




            if($existsOauthUser > 0)

            {	

        		$donatur["oauth_provider"]  = "facebook";

        		$donatur["oauth_uid"]       = $profile["id"];

            	$donatur["email"] 			= $profile["email"];
            	$donatur["picture_url"] 			= "http://graph.facebook.com/".$profile['id']."/picture?type=large";

            	$donatur["last_login_ip"] 	= $_SERVER['REMOTE_ADDR'];

            	$donatur["is_activate"]		= "true";

            	$donatur["activated"]		= "active";

            	$donatur["login_type"]      = "oauth";



            	$key = array(

            					"email" 		 => $profile["email"], 

            					"oauth_uid" 	 => $profile["id"], 

            					"oauth_provider" => "facebook",

            					"login_type" 	 => "oauth", 

            					"is_activate"	 => "true", 

            					"activated" 	 => "active"

            				);



            	$result = $this->db->update("donaturs", $donatur, $key);



            	$id = $this->auth->getUserOauthID($key)->row();

            	$id = !empty($id->id) ? $id->id : "";



        		// DETAIL DATA

        		$detail["fname"] 	= $profile["first_name"];

        		$detail["lname"] 	= $profile["last_name"];

        		$detail["sex"]	 	= $profile["gender"];

        		$detail["donatur"]	= $id;



        		$key = array("donatur" => $id);



        		$this->db->update("donaturs_d", $detail, $key);



        		// TOKEN

        		$dtoken["code"] 		= $token;

        		$dtoken["valid_until"] 	= $validdate;

        		$dtoken["donatur"]		= $id;

        		$dtoken["donatur_ip"]	= $_SERVER['REMOTE_ADDR'];



        		$this->db->insert("donatur_tokens", $dtoken);

            }

            else

            {

            	// OAUTH USER

            	$donatur["email"] 			= $profile["email"];

            	$donatur["last_login_ip"] 	= $_SERVER['REMOTE_ADDR'];

            	$donatur["is_activate"]		= "true";

            	$donatur["activated"]		= "active";

            	$donatur["login_type"]      = "oauth";

        		$donatur["oauth_provider"]  = "facebook";

        		$donatur["oauth_uid"]       = $profile["id"];

                $donatur["picture_url"] 			= "http://graph.facebook.com/".$profile['id']."/picture?type=large";



            	$result = $this->db->insert("donaturs", $donatur);



            	if($result)

            	{

            		$id = $this->db->insert_id();



            		// DETAIL DATA

            		$detail["fname"] 	= $profile["first_name"];

            		$detail["lname"] 	= $profile["last_name"];

            		$detail["sex"]	 	= $profile["gender"];

            		$detail["donatur"]	= $id;



            		$this->db->insert("donaturs_d", $detail);



            		// TOKEN

            		$dtoken["code"] 		= $token;

            		$dtoken["valid_until"] 	= $validdate;

            		$dtoken["donatur"]		= $id;

            		$dtoken["donatur_ip"]	= $_SERVER['REMOTE_ADDR'];



            		$this->db->insert("donatur_tokens", $dtoken);



					$datasaldo = array(

						"donatur"=>$id, 

						"prevBalance"=>0,

						"balanceAmount"=>0, 

						'crtd_at' => date('Y-m-d H:i:s'), 

						'edtd_at' => date('Y-m-d H:i:s')

					);



					$this->db->insert('donaturs_saldo', $datasaldo);

            	}

            }



	        if ($this->db->trans_status() === FALSE) {

	            $this->db->trans_rollback();

	        } else {

	            $this->db->trans_commit();

	        }



	        // SET SESSION USER FACEBOOK

   			$key = array(

            					"email" 		 => $profile["email"], 

            					"oauth_uid" 	 => $profile["id"], 

            					"oauth_provider" => "facebook",

            					"login_type" 	 => "oauth", 

            					"is_activate"	 => "true", 

            					"activated" 	 => "active"

            				);



	        $user_data = $this->auth->getUserOauth($key);

	        $user_data = !empty($user_data) ? $user_data : (object) array();

	        

   			$sess_data[LGI_KEY . "login_info"] = array(

											"userid" 			=> $user_data->id,

											"fname" 			=> $user_data->fname,

											"email" 			=> $user_data->email,

											"statLogin" 		=> true,

											"login_uid" 		=> $user_data->id,

											"login_ouid" 		=> $user_data->oauth_uid,

											"login_fname" 		=> $user_data->fname,

											"login_lname" 		=> $user_data->lname,

											"login_email" 		=> $user_data->email,

											"login_laston" 		=> $user_data->last_online,

											"login_ip"	 		=> $user_data->last_login_ip,

											"login_date" 		=> $user_data->last_login_date,

											"login_type"    	=> "oauth",

											"login_oprovider"   => "facebook",

											"login_sex"     	=> $user_data->sex,

											"login_status"  	=> true,

											"login_token"   	=> $token

										 );



			$this->session->set_userdata($sess_data);



            // Now you can redirect to another page and use the access token from $_SESSION['facebook_access_token']
            $preflink = $this->session->userdata('urllink');
            redirect($preflink);

        }

        else

        {

            redirect('auten/page/login');

        }

	}

    public function logout()
    {
        session_destroy();

        redirect('/');
    }

}

