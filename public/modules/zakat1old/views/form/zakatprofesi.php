<div class="col-md-3">
	&nbsp;
</div>
<div class="col-md-6">
	<center>
	    <?php
		    $hidden_form = array('id' => !empty($id) ? $id : '');
		    echo form_open_multipart('', array('id' => 'fmdonasi'), $hidden_form);
	    ?>

		<div>
			<div class="col-md-5">
				Pendapatan ( Gaji per bulan )
			</div>
			<div class="col-md-7">
				<?php 
					$name  = 'donationNominal';
					$value = '0';

					$extra = 'id="donationNominal" 
							  class="form-control" placeholder="Nominal Donasi"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<div class="col-md-5">
				Pendapatan lainnya ( Opsional )
			</div>
			<div class="col-md-7">
				<?php 
					$name  = 'donationNominal';
					$value = '0';

					$extra = 'id="donationNominal" 
							  class="form-control" placeholder="Nominal Donasi"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<div class="col-md-5">
				Hutang/Cicilan ( Opsional )
			</div>
			<div class="col-md-7">
				<?php 
					$name  = 'donationNominal';
					$value = '0';

					$extra = 'id="donationNominal" 
							  class="form-control" placeholder="Nominal Donasi"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<?php 
				$name  = 'donationNominal';
				$value =  empty($this->session->userdata('prevnomDonation'))? "" : 
								$this->session->userdata('prevnomDonation');

				$extra = 'id="donationNominal" 
						  class="form-control don-input" placeholder="Nominal Donasi"';

				echo form_input($name, $value, $extra); 
			?>
		</div>

		<div>
			<button type="submit" class="btn btn-success" style="width:100%; margin-top:10px"> 
				Hitung Zakat Saya
			</button>
		</div>	
	</center>
    <?php
    	echo form_close();
    ?>
</div>
<div class="col-md-3">
	&nbsp;
</div>