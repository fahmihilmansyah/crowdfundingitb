<div class="col-md-3">
	&nbsp;
</div>
<div class="col-md-6">
	<center>
		<p>
			Khusus untuk harta yang telah tersimpan selama lebih dari 1 tahun ( haul )
			dan mencapai batas tertentu ( nisab )
		</p>

		<br clear="all">

	    <?php
		    $hidden_form = array('id' => !empty($id) ? $id : '');
		    echo form_open_multipart('', array('id' => 'fmdonasi'), $hidden_form);
	    ?>

		<div>
			<div class="col-md-5">
				Nilai Deposito/Tabungan/Giro
			</div>
			<div class="col-md-7">
				<?php 
					$name  = 'donationNominal';
					$value = '0';

					$extra = 'id="donationNominal" 
							  class="form-control" placeholder="Nominal Donasi"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<div class="col-md-5">
				Nilai Properti & Kendaraan <br/> 
				<span style="font-size:11px">( bukan yang digunakan sehari-hari )</span>
			</div>
			<div class="col-md-7">
				<?php 
					$name  = 'donationNominal';
					$value = '0';

					$extra = 'id="donationNominal" 
							  class="form-control" placeholder="Nominal Donasi"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<div class="col-md-5">
				Emas, perak, permata, atau sejenisnya
			</div>
			<div class="col-md-7">
				<?php 
					$name  = 'donationNominal';
					$value = '0';

					$extra = 'id="donationNominal" 
							  class="form-control" placeholder="Nominal Donasi"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<div class="col-md-5">
				Lainnya
				<span style="font-size:11px">( Saham, piutang, dan surat-surat berharga lainnya )</span>
			</div>
			<div class="col-md-7">
				<?php 
					$name  = 'donationNominal';
					$value = '0';

					$extra = 'id="donationNominal" 
							  class="form-control" placeholder="Nominal Donasi"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<div class="col-md-5">
				Hutang pribadi yang jatuh tempo tahun ini
			</div>
			<div class="col-md-7">
				<?php 
					$name  = 'donationNominal';
					$value = '0';

					$extra = 'id="donationNominal" 
							  class="form-control" placeholder="Nominal Donasi"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<?php 
				$name  = 'donationNominal';
				$value =  empty($this->session->userdata('prevnomDonation'))? "" : 
								$this->session->userdata('prevnomDonation');

				$extra = 'id="donationNominal" 
						  class="form-control don-input" placeholder="Nominal Donasi"';

				echo form_input($name, $value, $extra); 
			?>
		</div>

		<div>
			<button type="submit" class="btn btn-success" style="width:100%; margin-top:10px"> 
				Hitung Zakat Saya
			</button>
		</div>	
	</center>
    <?php
    	echo form_close();
    ?>
</div>
<div class="col-md-3">
	&nbsp;
</div>