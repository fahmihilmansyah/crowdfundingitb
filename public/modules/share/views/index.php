{_layHeader}


<!-- Slider -->
<div id="slider">
    <div class="container-fluid">
        <div class="row">
            <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="10000">

                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php
                    $jum_dot = count($_data_carousel);

                    $i = 0;
                    foreach ($_data_carousel as $row) {
                        $active = ($i == 0) ? "class='active'" : "";
                        ?>

                        <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" <?php echo $active; ?>></li>

                        <?php
                        $i++;
                    }
                    ?>
                </ol>

                <div class="carousel-inner" role="listbox" style="overflow: hidden !important;">

                    <!-- Wrapper for slides -->
                    <?php
                    if (!empty($_data_carousel)) {
                        $i = 0;
                        foreach ($_data_carousel as $row) {
                            $active = ($i == 0) ? "active" : "";
                            ?>
                            <div class="item <?php echo $active; ?>" style="border:0px solid black;">
                                <?php
                                // $img = !empty($row->img) ? $row->img : "noimage.png";
            					$img = !empty($row->img) ? $row->img : '';
				  				
			                	$img = URI::existsImage('slider/', 
			                								  $img, 
			                								  'noimg_front/',
			                								  'no-slider.jpg');
                                ?>
                                <img src="<?php echo $img; ?>" style="width: 1366px !important;">

                                <?php
                                if($row->link_d){
                                    if (!empty($row->title)) {
                                        ?>
                                        <div class="carousel-caption custom-caption" style="width: 70%; text-align: center;">
                                            <div style="position: relative; margin:0 auto; max-width:1366px; border:0px solid black;">
                                            <center>
                                            <?php
                                            $title = !empty($row->title) ? ucwords($row->title) : "-";
                                            ?>
                                            <h1 class="bold"><?php echo strtoupper($title); ?></h1>

                                            <p>
                                                <?php
                                                $desc = !empty($row->desc) ? ucwords($row->desc) : "-";

                                                echo $desc;
                                                ?>
                                            </p>
                                                <a href="<?php echo $row->link; ?>" class="btn btn-default btn-daqu">
                                                    <span class="bold">FIND
                                                    OUT MORE</span>
                                                </a>
                                            </center>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php
                            $i++;
                        }
                    } else {
                        ?>

                        <div class="item active">
                            <img class="image" src="<?php URI::baseURL(); ?>assets/images/slider/Splash Screen.png">
                            <div class="carousel-caption red">

                                <h1 class="bold">DAARUL QUR'AN</h1>
                                <p>

                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                    unknown printer took a galley of type and scrambled it to make a type specimen book.
                                    It has survived not only five centuries, but also the leap into electronic
                                    typesetting, remaining essentially unchanged. It was popularised in the 1960s with
                                    the release of Letraset sheets containing Lorem Ipsum passages, and more recently
                                    with desktop publishing software like Aldus PageMaker including versions of Lorem
                                    Ipsum.
                                </p>
                                <br/>

                                <center>
                                    <a href="javascript:void(0);" class="btn btn-warning">FIND OUT MORE</a>
                                </center>
                            </div>
                        </div>

                        <div class="item">
                            <img class="image" src="<?php URI::baseURL(); ?>assets/images/slider/Slide-Screen.png">
                            <div class="carousel-caption red">
                                <h3>Title</h3>
                                <center>
                                    <p>The atmosphere in Chania has a touch of Florence and Venice.</p></center>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- PROGRAM CAMPAIGN -->

<div id="program-lembaga">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="program-lembaga-title">
                    <center>
                        <h2 class="bold">
							<?php 
								echo !empty($spage_shadaq["m_title"]) ? strtoupper($spage_shadaq["m_title"]) : "PROGRAM DAARUL QUR'AN";
							?>
						</h2>

                        <br/>

                        <h5 style="width: 80%"><?php
							if(!empty($spage_shadaq["m_desc"])){
								echo $spage_shadaq["m_desc"];
							}
							else
							{
							?>

								Bantuan Anda, berapa pun jumlahnya, sangat bermanfaat buat mereka yang membutuhkan. <br/>

								Kami menyalurkan seluruh bantuan amanat dari maysarakat untuk sebesar-besar kemanfaatan yang layak menerimanya.
							<?php
							}
							?>
						</h5>
                    </center>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6" style="margin-bottom:20px;">
                <a href="<?php URI::baseURL(); ?>program/shadaqah" style="text-decoration:none;color:#333">
                    <div class="program-lembaga-list">
                        <div class="list-program">
                            <center>
                            	<?php
	            					$img = !empty($spage_shadaq["icon_img"]) ? $spage_shadaq["icon_img"] : '';
					  				
				                	$img = URI::existsImage('spage_program/', 
				                								  $img, 
				                								  'noimg_front/',
				                								  'no-aminilist.jpg');
                            	?>
                                <img src="<?php echo $img; ?>">
                                <h2 class="bold">
									<?php
										echo !empty($spage_shadaq["icon_title"]) ? strtoupper($spage_shadaq["icon_title"]) : "SEDEKAH"
									?>
								</h2>
                            </center>
                        </div>

                        <div class="list-konten">
                            <p class="p-program">
                                <?php
									if(!empty($spage_shadaq["icon_desc"]))
									{
										echo $spage_shadaq["icon_desc"];
									}
									else
									{
								?>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
								<?php
									}
								?>
                            </p>
                        </div>
                    </div>
					<div class="bt5">

					</div>
                </a>
            </div>

            <div class="col-md-6" style="margin-bottom:20px">
                <a href="<?php URI::baseURL(); ?>program/lainnya" style="text-decoration:none;color:#333">
                    <div class="program-lembaga-list">

                        <div class="list-program">
                            <center>
                            	<?php
	            					$img = !empty($spage_infaq["icon_img"]) ? $spage_infaq["icon_img"] : '';
					  				
				                	$img = URI::existsImage('spage_program/', 
				                								  $img, 
				                								  'noimg_front/',
				                								  'no-aminilist.jpg');
                            	?>
                                <img src="<?php echo $img; ?>">
                                <h2 class="bold">
									<?php
										echo !empty($spage_infaq["icon_title"]) ? strtoupper($spage_infaq["icon_title"]) : "PROGRAM LAINNYA"
									?>
								</h2>
                            </center>
                        </div>

                        <div class="list-konten">
                            <p class="p-program">
                                <?php
									if(!empty($spage_infaq["icon_desc"]))
									{
										echo $spage_infaq["icon_desc"];
									}
									else
									{
								?>
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
								<?php
									}
								?>
                            </p>
                        </div>
                    </div>
					<div class="bt5">

					</div>
                </a>
            </div>
        </div>
    </div>
</div>


<!-- MAIN CAMPAIGN -->

<div id="main-campaign">
    <div class="container">
        <?php
        if (!empty($_data_main_campaign)) {
            ?>
            <!-- start create campaign main -->
            <?php foreach ($_data_main_campaign as $r) { ?>
                <div class="main-campaign-wrap bt5">
                    <div class="col-md-5" style="overflow:hidden; padding:0px">
                    	<?php
        		        $img = !empty($r->img) ? $r->img : '';
		  				
	                	$img = URI::existsImage('campaign/new_thumb/', 
	                								  $img, 
	                								  'noimg_front/',
	                								  'no-urgent.jpg');
                    	?>
                        <img src="<?php echo $img; ?>"
                             style="position:relative; height: 442px; width:100%">
                    </div>
                    <div class="col-md-7 main-campaign-konten">
                        <h3 class="sm-bold">POPULAR CAMPAIGN</h3>

                        <h1 class="bold">
                            <?php
                            $title = !empty($r->title) ? ucwords($r->title) : "-";
                            echo $title;
                            ?>
                        </h1>

                        <p>
                            Oleh:
                            <?php
                            $name_userpost = !empty($r->name_userpost) ? $r->name_userpost : "-";
                            echo $r->name_userpost;
                            ?> ( Admin Daarul Qur'an)
                        </p>

                        <p>
                            <?php
                            $desc_short = !empty($r->desc_short) ? $r->desc_short : "-";
                            echo $r->desc_short;
                            ?>
                        </p>

                        <div style="padding-top:70px;">
                            <div class="barWrapper">
                                <div class="progress">
                                    <?php
                                    $now 	= !empty($r->now) ? $r->now : 0;
                                    $target = !empty($r->target) ? $r->target : 0;

                                    if ($target == 0) {
                                        $hitung = 0;
                                    } elseif (($now / $target) >= 1) {
                                        $hitung = 100;
                                    } else {
                                        $hitung = ($now / $target) * 100;
                                    }
                                    ?>
                                    <div class="progress">
                                        <div
                                                class="progress-bar"
                                                role="progressbar"
                                                aria-valuenow="<?php echo $hitung; ?>"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="background:#f0ad4e;"
                                        >
                                            <b><?php echo  round($hitung,2); ?>%</b>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="margin-bottom:30px">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <p class="hidden-xs hidden-sm">Terkumpul</p>
                                <span class="sm-bold size14">
										Rp. <?php echo number_format($now, 0, ",", ".") ?>
									</span>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <p class="hidden-xs hidden-sm">Waktu</p>
                                <span class="sm-bold size14">
									<?php
                                    $selisih_validate = !empty($r->selisih_validate) ? $r->selisih_validate : 0;
                                    $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;
                                    echo $selisih_validate;
                                    ?>
                                    Hari lagi<span>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12" style="margin-bottom: 10px;">
                                <?php if ($r->selisih_validate > 0) { ?>
                                    <a href="<?php echo base_url('donasi/' . $r->slug); ?>"
                                       class="btn btn-warning padd-rl-50 pull-right">
                                        <span class="bold">SEDEKAH</span>
                                    </a>
                                <?php } else { ?>
                                    <a href="<?php echo base_url('donasi/' . $r->slug); ?>"
                                       class="btn btn-danger padd-rl-50 pull-right">
                                        <span class="bold">SEDEKAH TELAH SELESAI</span>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!-- end create campaign -->
            <?php
        } else {
            ?>
            <div class="main-campaign-wrap bt5">

                <div class="col-sm-5" style="overflow:hidden">

                    <img src="<?php //URI::baseURL(); 
                    ?>assets/images/maincampaign/sample.png"

                         style="position:relative; left:-15px; height: 442px; width:100%">

                </div>

                <div class="col-sm-6 main-campaign-konten">

                    <h3 class="sm-bold">POPULAR CAMPAIGN</h3>

                    <h1 class="bold">SAVE ROHINGYA</h1>

                    <p>oleh: Bambang</p>

                    <p>

                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged.

                    </p>

                    <div class="list-progress">
                        <div class="barWrapper">
                            <div class="progress">
                                <div
                                        class="progress-bar"
                                        aria-valuenow="100"
                                        aria-valuemin="10"
                                        aria-valuemax="100"
                                        style="background:#f0ad4e;">
                                        <b>100%</b>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="margin-bottom:20px">
                        <div class="col-md-4">
                            Terkumpul <br/>

                            <span class="sm-bold size14">Rp. 1.723.000</span>
                        </div>

                        <div class="col-md-4">
                            Waktu<br/>

                            <span class="sm-bold size14">7 hari Lagi</span>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>

</div>


<!-- PROGRAM CAMPAIGN -->

<div id="program-campaign" class="pilihan">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="program-campaign-title">
                    <center>
                        <h2 class="bold">PROGRAM PILIHAN</h2>
                        <h5>Program akan kami salurkan kepada yang membutuhkan</h5>
                    </center>
                </div>
            </div>
        </div>

        <div class="row">
            <?php
            if (!empty($_data_campaign_list)) {
                foreach ($_data_campaign_list as $r) {

                    ?>

                    <div class="col-md-4" style="margin-bottom:20px">
                        <div class="program-campaign-list bt5">
                            <div class="list-image">
                            	<?php

			        		        $img = !empty($r->img) ? $r->img : '';
					  				
				                	$img = URI::existsImage('campaign/thumbnails/', 
				                								  $img, 
				                								  'noimg_front/',
				                								  'no-amini.jpg');
                            	?>
                                <img src="<?php echo $img; ?>"
                                     style="width:100%">
                            </div>

                            <div class="list-konten">
                                <h3 class="bold">
                                    <?php echo !empty($r->title) ? $r->title : "-"; ?>
                                </h3>
                                <span>oleh:
                                    <?php echo !empty($r->name_userpost) ? $r->name_userpost : "-"; ?> ( Admin Daarul Qur'an)		
								</span>
	                            <p>
	                                <?php echo !empty($r->desc_short) ? $r->desc_short : "-"; ?>
	                            </p>
                            </div>

                            <div class="list-progress">
                                <div class="barWrapper">
                                    <div class="progress">
                                        <?php
                                        $now = !empty($r->now) ? $r->now : 0;
                                        $target = !empty($r->target) ? $r->target : 0;

                                        if ($target == 0) {
                                            $hitung = 0;
                                        } elseif (($now / $target) >= 1) {
                                            $hitung = 100;
                                        } else {
                                            $hitung = ($now / $target) * 100;
                                        }
                                        ?>
                                        <div class="progress">
                                            <div
                                                    class="progress-bar"
                                                    role="progressbar"
                                                    aria-valuenow="<?php echo $hitung; ?>"
                                                    aria-valuemin="0"
                                                    aria-valuemax="100"
                                            >
                                                <b><?php echo  round($hitung,2); ?>%</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="list-desc">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            Terkumpul <br/>
                                            <span class="sm-bold size14 red">
											Rp. <?php echo number_format($now, 0, ",", ".") ?>
										</span>

                                        </td>
                                        <td align="right">
                                            <br/>
                                            <span class="fa fa-clock-o"></span> :
                                            <span class="sm-bold size14 red">
												<?php
                                                $selisih_validate = !empty($r->selisih_validate) ? $r->selisih_validate : 0;
                                                $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;
                                                echo $selisih_validate;
                                                ?>
                                                hari Lagi
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <label class="alert alert-success">Campaign Dilanjutkan</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <center>
                                            <td align="center" colspan="2">
                                                <?php if ($r->selisih_validate > 0) { ?>
                                                    <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                                                       class="btn btn-warning padd-rl-50"
                                                       style="padding-left:50px; padding-right:50px;">
                                                        <span class="bold">SEDEKAH</span>
                                                    </a>
                                                <?php } else { ?>

                                                    <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                                                       class="btn btn-danger padd-rl-50">
                                                        <span class="bold">SEDEKAH TELAH SELESAI</span>
                                                    </a>
                                                <?php } ?>

                                            </td>
                                        </center>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="col-md-4" style="margin-bottom:20px">
                    <div class="program-campaign-list bt5">
                        <div class="list-image">
                            <img src="<?php URI::baseURL(); ?>assets/images/campaign/donasi1.jpg" style="width:100%">
                        </div>

                        <div class="list-konten">
                            <h3 class="bold">SAVE ROHINGYA</h3>
                            <p>oleh: Bambang</p>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has
                                survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged.
                            </p>
                        </div>

                        <div class="list-progress">
                            <div class="barWrapper">
                                <div class="progress">
                                    <div
                                            class="progress-bar"
                                            aria-valuenow="60"
                                            aria-valuemin="10"
                                            aria-valuemax="100"
                                            style="background:#7a0a0e;">
                                        		<b>100%</b>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="list-desc">
                            <table width="100%">
                                <tr>
                                    <td>
                                        Terkumpul <br/>
                                        <span class="sm-bold size14 red">Rp. 1.723.000</span>
                                    </td>

                                    <td align="right">
                                        <br/>
                                        <span class="fa fa-clock-o"></span> :
                                        <span class="sm-bold size14 red">7 hari Lagi</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td align="right" colspan="2">
                                        <center>
                                            <a href="javascript:void(0)"
                                               class="btn btn-warning padd-rl-50"
                                               style="padding-left:50px; padding-right:50px;">
                                                <span class="bold">SEDEKAH</span>
                                            </a>
                                        </center>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <div clas="row">
                <div class="col-md-12">
                    <div class="list-all">
                        <center>
                            <a href="<?php URI::baseURL(); ?>donasi" class="btn btn-warning btn-lihat"><span class="bold">LIHAT SEMUA</span></a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Popular Campaign -->

<div id="program-campaign" class="pilihan">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="program-campaign-title">
                    <center>
                        <h2 class="bold">POPULAR CAMPAIGN CABANG</h2>
                        <br/>
                        <h5>Berita terkini mengenai kegiatan Daarul Qur'an</h5>
                    </center>
                </div>
            </div>
        </div>

        <div class="row">
            <?php
            if (!empty($_data_campaign_list_cab)) {
                foreach ($_data_campaign_list_cab as $r) {

                    ?>

                    <div class="col-md-4" style="margin-bottom:20px;">

                        <div class="program-campaign-list bt5">

                            <div class="list-image">
                            	<?php

			        		        $img = !empty($r->img) ? $r->img : '';
					  				
				                	$img = URI::existsImage('campaign/thumbnails/', 
				                								  $img, 
				                								  'noimg_front/',
				                								  'no-amini.jpg');
                            	?>
                                <img src="<?php echo $img; ?>"
                                     style="width:100%">

                            </div>

                            <div class="list-konten">

                                <h3 class="bold">
                                    <?php echo !empty($r->title) ? $r->title : "-"; ?>
                                </h3>

                                <span>
								oleh:
                                    <?php
                                    echo !empty($r->name_userpost) ? $r->name_userpost : "-";
                                    ?> ( Admin Daarul Qur'an)	
							</span>

                                <p>

                                    <?php
                                    echo !empty($r->desc_short) ? $r->desc_short : "-";
                                    ?>

                                </p>

                            </div>

                            <div class="list-progress">
                                <div class="barWrapper">
                                    <div class="progress">
                                        <?php
                                        $now = !empty($r->now) ? $r->now : 0;
                                        $target = !empty($r->target) ? $r->target : 0;

                                        if ($target == 0) {
                                            $hitung = 0;
                                        } elseif (($now / $target) >= 1) {
                                            $hitung = 100;
                                        } else {
                                            $hitung = ($now / $target) * 100;
                                        }
                                        ?>
                                        <div class="progress">
                                            <div
                                                    class="progress-bar"
                                                    role="progressbar"
                                                    aria-valuenow="<?php echo $hitung; ?>"
                                                    aria-valuemin="0"
                                                    aria-valuemax="100"
                                            >
                                                <b><?php echo  round($hitung,2); ?>%</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="list-desc">

                                <table width="100%">

                                    <tr>

                                        <td>

                                            Terkumpul <br/>

                                            <span class="sm-bold size14 red">
											Rp. <?php echo number_format($now, 0, ",", ".") ?>
										</span>

                                        </td>

                                        <td align="right">

                                            <br/>

                                            <span class="fa fa-clock-o"></span> :

                                            <span class="sm-bold size14 red">
												<?php
                                                $selisih_validate = !empty($r->selisih_validate) ? $r->selisih_validate : 0;
                                                $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;
                                                echo $selisih_validate;
                                                ?>
                                                hari Lagi</span>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td colspan="2">&nbsp;</td>

                                    </tr>

                                    <tr>

                                        <center>
                                            <td align="center" colspan="2">

                                                <?php if ($r->selisih_validate > 0) { ?>
                                                    <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"


                                                       class="btn btn-warning padd-rl-50"


                                                       style="padding-left:50px; padding-right:50px;">


                                                        <span class="bold">SEDEKAH</span>


                                                    </a>
                                                <?php } else { ?>

                                                    <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"


                                                       class="btn btn-danger padd-rl-50">


                                                        <span class="bold">SEDEKAH TELAH SELESAI</span>


                                                    </a>
                                                <?php } ?>

                                            </td>

                                        </center>

                                    </tr>

                                </table>

                            </div>
                        </div>

                    </div>

            <?php

                }

            } else {

            ?>
                <div class="col-md-4" style="margin-bottom:20px">
                    <div class="program-campaign-list bt5">
                        <div class="list-image">
                            <img src="<?php URI::baseURL(); ?>assets/images/campaign/donasi1.jpg" style="width:100%">
                        </div>

                        <div class="list-konten">
                            <h3 class="bold">SAVE ROHINGYA</h3>
                            <p>oleh: Bambang</p>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has
                                survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged.
                            </p>
                        </div>

                        <div class="list-desc">
                            <div class="col-md-12" style="margin-bottom: 20px">
                                <div class="nav navbar-left">05 Jan 2017
                                </div>
                                <div class="nav navbar-right">
                                    <a href="javascript:void(0)"
                                       class="btn btn-warning padd-rl-50 pull-right"
                                       style="padding-left:50px; padding-right:50px;">
                                        <span class="bold">LIHAT SEMUA</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <div clas="row">
                <div class="col-md-12">
                    <div class="list-all">
                        <center>
                            <a href="<?php URI::baseURL(); ?>donasi" class="btn btn-warning btn-lihat"><span class="bold">LIHAT SEMUA</span></a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Latest Articles -->
<div id="program-campaign" class="news">
    <div class="container">
        <div clas="row">
            <div class="col-md-12">
                <div class="program-campaign-title">
                    <center>
                        <h2 class="bold">LATEST ARTICLES</h2>
                        <br/>
                        <h5>Berita terkini mengenai kegiatan Daarul Qur'an</h5>
                    </center>
                </div>
            </div>
        </div>
        <div class="row">
            <?php
            if (!empty($_data_post_list)) 
            {
                foreach ($_data_post_list as $r) 
                {
            ?>
                    <div class="col-md-4" style="margin-bottom:20px;">
                        <div class="program-campaign-list bt5">
                            <div class="list-image">
                            	<?php

			        		        $img = !empty($r->img) ? $r->img : '';
					  				
				                	$img = URI::existsImage('articles/thumbnails/', 
				                								  $img, 
				                								  'noimg_front/',
				                								  'no-amini.jpg');
                            	?>
                                <img src="<?php echo $img; ?>"
                                     style="width:100%">
                            </div>

                            <div class="list-konten">
                                <h3 class="bold">
                                    <?php echo !empty($r->title) ? ucwords($r->title) : "-"; ?>
                                </h3>
                                <span>
								oleh:
                                    <?php
                                    echo !empty($r->author) ? ucwords($r->author) : "-";
                                    ?> ( Admin Daarul Qur'an)	
							    </span>

                                <p>
                                    <?php
                                    echo !empty($r->desc_short) ? $r->desc_short : "-";
                                    ?>
                                </p>
                                <br/>
                            </div>

                            <div class="list-desc">
                                <div class="col-md-12" style="margin-bottom: 20px">
                                    <div class="nav navbar-left">
                                        <?php echo arr::monthI(date("d-m-Y", strtotime($r->post_date))); ?>
                                    </div>
                                    <div class="nav navbar-right">
                                        <a href="<?php URI::baseURL(); ?>news/<?php echo $r->slug ?>"
                                           class="btn btn-warning padd-rl-50 pull-right"
                                           style="padding-left:50px; padding-right:50px;">
                                            <span class="bold">LIHAT SEMUA</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            <?php

                }
            } else {
            
            ?>
                <div class="col-md-4" style="margin-bottom:20px">
                    <div class="program-campaign-list bt5">
                        <div class="list-image">
                            <img src="<?php URI::baseURL(); ?>assets/images/campaign/donasi1.jpg" style="width:100%">
                        </div>

                        <div class="list-konten">
                            <h3 class="bold">SAVE ROHINGYA</h3>
                            <p>oleh: Bambang</p>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has
                                survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged.
                            </p>
                        </div>

                        <div class="list-desc">
                            <div class="col-md-12" style="margin-bottom: 20px">
                                <div class="nav navbar-left">05 Jan 2017
                                </div>
                                <div class="nav navbar-right">
                                    <a href="javascript:void(0)"
                                       class="btn btn-warning padd-rl-50 pull-right"
                                       style="padding-left:50px; padding-right:50px;">
                                        <span class="bold">LIHAT SEMUA</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
            <div clas="row">
                <div class="col-md-12">
                    <div class="list-all">
                        <center>
                            <a href="<?php URI::baseURL(); ?>news/list" class="btn btn-warning  btn-lihat"><span class="bold">LIHAT SEMUA</span></a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br/>
<!-- NEWS UPDATE -->
<div id="program-create">
    <?php
    // $img = !empty($row->img) ? $row->img : "noimage.png";
	$img = !empty($power_month[0]->img) ? $power_month[0]->img : '';
		
	$img = URI::existsImage('campaign/', 
								  $img, 
								  'noimg_front/',
								  'no-slider.jpg');
    ?>
    <img class="sedekah_of_the_mnth" src="<?php echo $img; ?>">
    <div class="sedekah_of_the_mnth_txt">
    <div class="container">
	    <center>
	        <h1 class="bold">SEDEKAH OF THE MONTH</h1>
	        <?php if (!empty($power_month)): ?>
	            <h2 class="bold"><?php echo $power_month[0]->title; ?></h2>
	            <p>
	                <?php echo $power_month[0]->desc_short; ?>
	            </p>
                <br/>
	            <a href="<?php echo base_url('donasi/' . $power_month[0]->slug); ?>" class="btn btn-warning btn-lihat"><span class="bold">SEDEKAH SEKARANG</span></a>
	        <?php endif; ?>
	    </center>
    </div>
	</div>
</div>
{_layFooter}