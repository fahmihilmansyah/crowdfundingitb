<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Share extends NBFront_Controller {
	 private $_modList = array(
                                'share/Share_model' => "sharemdl"
                            );
	private $_SESSION;
	private $iddonaturs;
	function __construct()

	{

		parent::__construct();
		$this->modelsApp();
		$this->_SESSION   = !empty($this->session->userdata(LGI_KEY . "login_info")) ? 
                                   $this->session->userdata(LGI_KEY . "login_info") : '';

        $this->iddonaturs = empty($this->_SESSION['userid'])? '' :$this->_SESSION['userid'];
	}

	private function modelsApp()
    {
        $this->load->model($this->_modList);
    }

	public function index($data = array())
	{
		$arr = array('fb','twitter','gplus','whatsapp');
		if(!empty($this->iddonaturs)){
			$media = $this->input->get('m');
			$article = $this->input->get('a');
			if(in_array($media, $arr)){
			   $val = $this->_cekshare($media);
			   if($val){
			    $this->db->query("UPDATE nb_campaign
			    SET nb_campaign.sum_share = nb_campaign.sum_share + 1
			    WHERE nb_campaign.title = ".$this->db->escape($article) );
				}
			}
		}else{
			echo "belum login ya!".$this->iddonaturs;
		}
	}

	private function _cekshare($media=null){
		$kondisi['where'] = array(
				'userid'=>$this->db->escape($this->iddonaturs),
				'media'=>$this->db->escape($media),
			);
		$cek = $this->sharemdl->custom_query('nb_share',$kondisi)->result();
		if(count($cek) == 0){
			$arrdata = array('jumshare'=> 1,'userid'=>$this->iddonaturs,'media'=>$media,'edtd_at'=>date("Y-m-d H:i:s"), 'crtd_at'=>date("Y-m-d H:i:s"));
			$this->sharemdl->custom_insert('nb_share',$arrdata);
			return true;
		}elseif (count($cek) == 1) {
				$tglskr = strtotime(date("Y-m-d"));
			$tgldb = strtotime(date("Y-m-d",strtotime($cek[0]->edtd_at)));
			if($tgldb != $tglskr){

				$dataup=array(
						'edtd_at'=>date("Y-m-d H:i:s"),
						'jumshare'=> $cek[0]->jumshare + 1
					);
				$this->sharemdl->custom_update('nb_share',$dataup,array('userid'=>$this->db->escape($iddonaturs),'media'=>$this->db->escape($media)));
				return true;
			}
		}
		return false;
	}

}

