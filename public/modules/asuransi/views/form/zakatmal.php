<div class="col-md-3">
	&nbsp;
</div>
<div class="col-md-6">
	<center>
		<p>
			Khusus untuk harta yang telah tersimpan selama lebih dari 1 tahun ( haul )
			dan mencapai batas tertentu ( nisab )
		</p>

		<br clear="all">


		<div>
			<div class="col-md-5">
				Nilai Deposito/Tabungan/Giro
			</div>
			<div class="col-md-7">
				<?php 
					$name  = '';
					$value = '0';

					$extra = 'id="" 
							  class="form-control angka" placeholder="Nominal Nilai Deposito/Tabungan/Giro"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<div class="col-md-5">
				Nilai Properti & Kendaraan <br/> 
				<span style="font-size:11px">( bukan yang digunakan sehari-hari )</span>
			</div>
			<div class="col-md-7">
				<?php 
					$name  = '';
					$value = '0';

					$extra = 'id="" 
							  class="form-control angka" placeholder="Nominal Nilai Properti & Kendaraan"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<div class="col-md-5">
				Emas, perak, permata, atau sejenisnya
			</div>
			<div class="col-md-7">
				<?php 
					$name  = '';
					$value = '0';

					$extra = 'id="" 
							  class="form-control angka" placeholder="Nominal Emas, perak, permata, atau sejenisnya"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<div class="col-md-5">
				Lainnya
				<span style="font-size:11px">( Saham, piutang, dan surat-surat berharga lainnya )</span>
			</div>
			<div class="col-md-7">
				<?php 
					$name  = '';
					$value = '0';

					$extra = 'id="" 
							  class="form-control angka" placeholder="Nominal Saham, piutang, dan surat-surat berharga lainnya"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">

		<div>
			<div class="col-md-5">
				Hutang pribadi yang jatuh tempo tahun ini
			</div>
			<div class="col-md-7">
				<?php 
					$name  = '';
					$value = '0';

					$extra = 'id="" 
							  class="form-control angkahutang" placeholder="Nominal Hutang pribadi yang jatuh tempo tahun ini"';

					echo form_input($name, $value, $extra); 
				?>
			</div>
		</div>

		<br clear="all">
		<br clear="all">


	</center>
</div>
<div class="col-md-3">

</div>