{_layHeader}
<div class="container-fluid grey">

    <div class="don-head">

        <div class="container">

            <!-- start create campaign main -->
            <center>
                <div class="invoice">

                    <center>
                        <h1 class="bold">TERIMA KASIH</h1>
                        <img src="<?php URI::baseURL(); ?>assets/images/icon/invoice.png"></br></br>
                        <h4>Terima kasih atas bantuan yang anda berikan</h4><br/>
                        <div class="inv">
                            <div class="invoice-">
                                <div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
                                    Nomor Transaksi</br>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
                                    <?php echo $summary[0]->no_transaksi;?></br>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
                                    </br>Nominal</br>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
                                    </br>Rp <?php echo number_format($summary[0]->nominal,0,",",".");?></br>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
                                    </br>Kode unik
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
                                    </br><?php echo number_format($summary[0]->uniqcode,0,",",".");?>
                                </div>
                            </div>
                            <hr>
                            <div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
                                <h3>Total</h3>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
                                <h3 style="color:#4cb050">Rp <?php echo number_format($summary[0]->nomuniq,0,',','.');?></h3>
                            </div>

                            <br/>
                            <!-- <div class="page-share-icon">
                                <div class="col-md-3 col-sm-4 col-xs-3"><h3 class="bold" style="valign:top">SHARE:</h3></div>
                                <p class="col-md-1 col-sm-2 col-xs-1">

                                    <a href="#" class="share-icon fb"></a>

                                </p>

                                <p class="col-md-1 col-sm-2 col-xs-1">

                                    <a href="#" class="share-icon twitter"></a>

                                </p>

                                <p class="col-md-1 col-sm-2 col-xs-1">

                                    <a href="#" class="share-icon google"></a>

                                </p>

                                <p class="col-md-1 col-sm-2 col-xs-1">

                                    <a href="#" class="share-icon linkedin"></a>

                                </p>

                                <p class="col-md-1 col-sm-2 col-xs-1">

                                    <a href="#" class="share-icon pinterest"></a>

                                </p>

                                <p class="col-md-1 col-sm-2 col-xs-1">

                                    <a href="#" class="share-icon share"></a>

                                </p>

                            </div> -->
                        </div>
                    </center>

                </div>
            </center>
        </div>

    </div>
</div>
{_layFooter}