{_layHeader}
<div class="container-fluid grey">

			<div class="don-head">

				<div class="container">

		<!-- start create campaign main -->
			<center>
			<div class="invoice">
			
				<center>
					<h1 class="bold">TERIMA KASIH</h1>
					<img src="<?php URI::baseURL(); ?>assets/images/icon/invoice.png"></br></br>
					<h4>Terima kasih atas bantuan yang anda berikan</h4><br/>
				<div class="inv">
					<div class="invoice-">
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
							Nomor Transaksi</br>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
							<?php echo $summary[0]->no_transaksi;?></br>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
							</br>Nominal</br>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
							</br>Rp <?php echo number_format($summary[0]->nominal,0,",",".");?></br>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
							</br>Kode unik
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
							</br><?php echo number_format($summary[0]->uniqcode,0,",",".");?>
						</div>
						</div>
					<hr>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
								<h3>Total</h3>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
							<h3 style="color:#4cb050">Rp <?php echo number_format($summary[0]->nomuniq,0,',','.');?></h3>
						</div>
					<br/><br/><br/>
					<h4 align="left">Silahkan Transfer <b>Rp <?php echo number_format($summary[0]->nomuniq,0,',','.');?></b> (Donasi + Kode Unik).<br><br>
                        Kode unik digunakan untuk identifikasi otomatis transfer Anda sehingga Anda tidak perlu melakukan konfirmasi.<br><br>
                        agar transaksi anda terverifikasi secara otomatis tanpa perlu konfirmasi ke rekening berikut:</h4>
					<br/>
					<table width="100%">
						<tr class="bold">
							<td align="left" width="60%"><img src="<?php echo base_url("assets/images/bank/".$summary[0]->imglink)?>" style="width:90%"></td>
							<td align="left" valign="top"><p style="color:#4cb050;font-size:30px;"><?php echo $summary[0]->namaBank?></p>No.Rekening: <?php echo $summary[0]->no_rekening?></br>Atas Nama:	<?php echo $summary[0]->atasnama?></td>
						</tr>
					</table>
					<h4 align="left">Pastikan anda transfer sebelum <b><i><?php echo date("d-m-Y H:i:s",strtotime($summary[0]->validDate));?></i></b> atau transaksi anda akan batal secara otomatis oleh sistem.</h4>

					<br/>
					
				</div>
				</center>
				
			</div>
			</center>
		</div>

			</div>
	</div>
{_layFooter}