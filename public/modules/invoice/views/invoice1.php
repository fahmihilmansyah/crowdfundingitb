<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<title>Invoice</title>

<style type="text/css">
html,body{
	font-family: 'Source Sans Pro', sans-serif;
	background:#efefef;
	margin:0px;
	padding:0px;
}

.container {
    padding-top:50px;
	padding-bottom:50px;
	padding-left:50px;
	padding-right:50px;
    margin-right: auto;
    margin-left: auto;
	background:white;
	
}

.right{
	width:50%;
	float:right;
}

.left{
	width:49.5%;
	float:left;
}

.full{
	width:100%;
	padding-top:20px;
	clear: both;
}

.grey{
	background:#efefef;
	padding:10px;
}

.table{
	border:1px solid #ccc;
	border-collapse:collapse;
}
</style>
</head>
<body>
	<div class="container">
		<table style="width:100%;">
		<tr>
		<td><div class="left">
			<img src="<?php echo base_url("assets/images/logo/logo.png")?>" width="100">
		</div></td><td><div align="right" class="right">
                    Ganesha Bisa<br/>
                    Philanthropy Building Lt. 2
                    Jl. Warung Jati Barat No. 14, Jatipadang Jakarta Selatan - 12450<br/>
                    Telpon: (021) 741 6050<br/>
		</div></td>
		
		
		</tr>
		</table>
		<div class="full">
			<div class="grey">
				Invoice #<?php echo $summary[0]->no_transaksi?><br/>
				Tanggal Transaksi: <?php echo $summary[0]->crtd_at ." ". date("d M Y",strtotime($summary[0]->crtd_at))?><br/>
			</div>
			<br/>
			<b>Kepada : <?php echo ucwords($summary[0]->namaDonatur)?></b><br/><br/>
			<table class="table" width="100%">
				<tr style="background:#efefef">
					<th width="5%">No</th>
					<th width="35%">Jenis Transaksi / Campaign</th>
					<th width="25%">Nominal</th>
				</tr>
				<tr>
					<td align="center">1</td>
					<td><?php echo strtoupper(empty($summary[0]->jenistrx)?$summary[0]->title:$summary[0]->jenistrx)?></td>
					<td align="center">Rp <?php echo number_format($summary[0]->nomuniq,0,",",".") ?></td>
				</tr>
			</table>
			<br/><br/>
			Terima kasih telah melakukan transaksi menggunakan <b>Dompet Aksi Baik</b>. Tingkatkan terus donasi anda dan dapatkan manfaatnya.
			<br/><br/>
			<div align="right" class="right">
                Tim Yayasan Solidarity Forever (YSF) dan Dompet Dhuafa (DD)<br/>
                Powered by DDTekno <?php echo date("Y"); ?>><br/>
		</div>
		</div>
	</div>
</body>
</html>