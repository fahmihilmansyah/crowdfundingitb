<html>
<head></head>
<body>
<table >
<tbody>
<tr>
<td>&nbsp;</td>
<td bgcolor="#FFFFFF">
<div>
<table>
<tbody>
<tr>
<td>
<div><strong><h2><?php echo ucwords($summary[0]->namaDonatur)?></h2></strong> <br />Mohon segera transfer sebelum:<br /><strong><?php echo date("d M Y H:i:s",strtotime($summary[0]->validDate))?>&nbsp;</strong><br /><br /></div>
<h2><strong>Lakukan Transfer sebesar:</strong><br /><strong>Rp <?php echo number_format($summary[0]->nomuniq,0,",",".")?></strong></h2>
<div>
<div><strong>Tepat hingga 3 digit terakhir</strong></div>
</div>
<div><strong><em>(perbedaan nilai transfer akan menghambat proses verifikasi donasi)</em></strong></div>
<div><strong><br /></strong>Ke:<br /><strong><?php echo $summary[0]->namaBank?> </strong><br /><strong>No Rek. <?php echo $summary[0]->no_rekening?></strong><br /><strong>a.n <?php echo $summary[0]->atasnama?><br /></strong><br />Donasi / Dana Anda akan terverifikasi oleh sistem dalam 1 hari kerja*.<br /><br />Bila hingga <?php echo date("d M Y H:i:s",strtotime($summary[0]->validDate))?> donasi / dana belum kami terima, maka donasi akan dibatalkan oleh sistem.<br /><br /></div>
<div>*Apabila transfer di luar jam kerja bank atau pada hari libur, maka verifikasi data akan mengalami keterlambatan.<br /><br /><em>P.S. - Ingin lebih mudah berdonasi? Isi saldo <strong>Dompet Kebaikan</strong> Anda; transfer sekali, donasi berkali-kali!</em><br /> <em> - Perbanyak Terus Point aksi baik anda!</em> <br /><br />Salam Hormat,</div>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
</body>
</html>