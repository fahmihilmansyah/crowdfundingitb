<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Invoice extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_modList = array("invoice/Invoice_model" => "invoicemdl");


    protected $_data = array();


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);

    }


    public function donation($id = null)

    {
         if (empty($id)) {
             redirect('home');
         }
        $iddonasi = $id;
//echo $iddonasi;exit;

        $kondisi['where'] = array('nb_trans_donation.no_transaksi' => $this->db->escape($iddonasi),
            'nb_trans_donation.status'=>$this->db->escape('unverified'));
        $kondisi['join'] = array(
            array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_donation.bank", "type" => "left"),
            array('table' => "nb_campaign", 'condition' => "nb_campaign.id = nb_trans_donation.campaign", "type" => "left")
            );
        $summary = $this->invoicemdl->custom_query("nb_trans_donation", $kondisi)->result();

        if(count($summary) != 1){
            die("Error");
        }
        if($summary[0]->bank != 919) {
            $data = $this->_data;
            $data['summary'] = $summary;
//        $this->sendMail($summary);
            $this->parser->parse("invoice/invoice", $data);
		$this->sendMail($summary);
        }else{
            die('Error 1234');
        }
    }
    public function program($id = null)

    {
         if (empty($id)) {
             redirect('home');
         }
        $iddonasi = $id;


        $kondisi['where'] = array('nb_trans_program.no_transaksi' => $this->db->escape($iddonasi),
            'nb_trans_program.status'=>$this->db->escape('unverified'));
        $kondisi['join'] = array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_program.bank", "type" => "left");
        $summary = $this->invoicemdl->custom_query("nb_trans_program", $kondisi)->result();

        if(count($summary) != 1){
            die("Error");
        }
        if($summary[0]->bank != 919) {
            $data = $this->_data;
            $data['summary'] = $summary;
            $this->parser->parse("invoice/invoice", $data);
	    $this->sendMail($summary);
        }else{
            die('Error 1234');
        }
    }
    public function depoprogram($id = null)

    {
         if (empty($id)) {
             redirect('home');
         }
        $iddonasi = $id;


        $kondisi['where'] = array('nb_trans_program.no_transaksi' => $this->db->escape($iddonasi),
            'nb_trans_program.status'=>$this->db->escape('verified'));
        $kondisi['join'] = array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_program.bank", "type" => "left");
        $summary = $this->invoicemdl->custom_query("nb_trans_program", $kondisi)->result();

        if(count($summary) != 1){
            die("Error");
        }
        if($summary[0]->bank == 919) {
            $data = $this->_data;
            $data['summary'] = $summary;
            $this->parser->parse("invoice/invoicedompet", $data);
       $this->sendMailDepo($summary);
        }else{
            die('Error 1234');
        }
    }

    public function depodonation($id = null)
    {
         if (empty($id)) {
             redirect('home');
         }
        $iddonasi = $id;


        $kondisi['where'] = array('nb_trans_donation.no_transaksi' => $this->db->escape($iddonasi),
            'nb_trans_donation.status'=>$this->db->escape('verified'));
        $kondisi['join'] = array(
            array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_donation.bank", "type" => "left"),
            array('table' => "nb_campaign", 'condition' => "nb_campaign.id = nb_trans_donation.campaign", "type" => "left")
            );
        $summary = $this->invoicemdl->custom_query("nb_trans_donation", $kondisi)->result();

        if(count($summary) != 1){
            die("Error");
        }
        if($summary[0]->bank == 919) {
            $data = $this->_data;
            $data['summary'] = $summary;
            $this->parser->parse("invoice/invoicedompet", $data);
          $this->sendMailDepo($summary);
        }else{
            die('Error 1234');
        }
    }

private function sendMail($summary = null)
    {

        
        $config["username"]   = MAIL_UNAME;
        $config["password"]   = MAIL_PASS;
        $config["host"]       = MAIL_HOST;
        $config["smtpAuth"]   = MAIL_SMTP;
        $config["smtpType"]   = MAIL_SMTP_TYPE;
        $config["port"]       = MAIL_PORT;



        sendmail::ConfigUser($config);



        $data["emailTo"]    = $summary[0]->emailDonatur;

        $data["senderName"] = "GaneshaBisa";

        $data["subject"]    = "Verifikasi Transfer"; 



        $data["type"]       = "view";



        $data["summary"]  = $summary;

        $data["konten"]     = "invoice/invoice2";  



        sendmail::SendMail($data);
    }
    private function sendMailDepo($summary = null)
    {

        
        $config["username"]   = MAIL_UNAME;
        $config["password"]   = MAIL_PASS;
        $config["host"]       = MAIL_HOST;
        $config["smtpAuth"]   = MAIL_SMTP;
        $config["smtpType"]   = MAIL_SMTP_TYPE;
        $config["port"]       = MAIL_PORT;



        sendmail::ConfigUser($config);



        $data["emailTo"]    = $summary[0]->emailDonatur;

        $data["senderName"] = "Yayasan Dana Sosial Al-Falah (GaneshaBisa)";

        $data["subject"]    = "Konfirmasi"; 



        $data["type"]       = "view";



        $data["summary"]  = $summary;

        $data["konten"]     = "invoice/invoice1";  



        sendmail::SendMail($data);
    }
}

