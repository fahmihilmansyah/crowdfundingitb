{_layHeader}

<?php
$url = implode("/", $this->uri->segment_array());
$dats = array('urllink' => $url);
$this->session->set_userdata($dats);
$sessionsl = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
    $this->session->userdata(LGI_KEY . "login_info")['email'] : '';
?>
<div class="featured-image"
     style="background-image:url(<?php URI::baseURL(); ?>assets/mumuapps/img/festivalkurban.jpg);"></div>
<div class="content">
    <div class="h5 title-font text-center bold mt-30 mb-30">Kenapa Kurban di MumuApps?</div>
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-body text-center">
                    <img src="<?php URI::baseURL(); ?>assets/mumuapps/img/syariah.svg" class="icon-home">
                    <div class="text-center title-font bold mt-15 mb-15">Sesuai Syariah</div>
                    <div class="text-muted">Kambing dan sapi yang disalurkan memenuhi kriteria hewan kurban.</div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-body text-center">
                    <img src="<?php URI::baseURL(); ?>assets/mumuapps/img/terjangkau.svg" class="icon-home">
                    <div class="text-center title-font bold mt-15 mb-15">Harga Terjangkau</div>
                    <div class="text-muted">Harga hewan kurban yang terjangkau dengan kualitas daging yang baik.</div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-body text-center">
                    <img src="<?php URI::baseURL(); ?>assets/mumuapps/img/tepatsasaran.svg" class="icon-home">
                    <div class="text-center title-font bold mt-15 mb-15">Tepat Sasaran</div>
                    <div class="text-muted">Kurban akan disalurkan kepada saudara kita yang benar-benar membutuhkan.
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-body text-center">
                    <img src="<?php URI::baseURL(); ?>assets/mumuapps/img/transparan.svg" class="icon-home">
                    <div class="text-center title-font bold mt-15 mb-15">Transparan</div>
                    <div class="text-muted">Update dan laporan lengkap penyaluran setelah pemotongan hewan kurban.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="h5 title-font text-center bold mt-30 mb-30">Pilih Mitra</div>
    <div class="card">
        <div class="card-body">
            <div class="row mb-10 justify-content-md-center">
                <?php foreach ($list_mitra as $r): ?>
                    <?php $imgfile = !empty($r->path_img) ? base_url() . "assets/" . $r->path_img : ''; ?>
                    <div class="col-4">
                        <a href="<?php echo base_url('kurban/kurban?cpartner=' . $r->id) ?>" class="partner-container"
                           style="background: url(<?php echo !empty($imgfile) ? $imgfile : $r->url_img ?>)no-repeat center / 65% auto;"></a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    {_layFooter}

