{_layHeader}

<?php
$url = implode("/", $this->uri->segment_array());
$dats = array('urllink' => $url);
$this->session->set_userdata($dats);
$sessionsl = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
    $this->session->userdata(LGI_KEY . "login_info")['email'] : '';
?>
<?php $cek = empty($this->session->userdata('prevnomDonation')) ? "" : "active"; ?>
<div class="content pr-15 pl-15 bg-white" style="padding-top: 10px;">
    <?php if (!empty($this->session->flashdata('error'))): ?>
        <div class="alert alert-danger alert-dismissable " role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <script type="text/javascript">
            setTimeout(function () {
                $('.alert').fadeOut();
            }, 10000); // <-- time in milliseconds
        </script>
    <?php endif; ?>
    <div class="mb-40 text-center">
        <div class="h3 bold title-font lh-1 color-second"><?php echo empty($judul[0]->title) ? "" : $judul[0]->title; ?></div>
    </div>
    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'kurban/kurban/' . $this->uri->segment(3), array('id' => 'fmdonasi', 'class' => 'fmdonasi mt-20'), $hidden_form);

    ?>
    <img src="http://news.unair.ac.id/wp-content/uploads/2019/07/Stres-pada-Hewan-Kurban-Pengaruhi-Kualitas-Daging.jpg">
    <div class="mb-20">
        <h3>Kurban</h3>
    </div>
    <div class="row">
        <div class="col-12">

            <h5 class="">Kenapa kurban di MuMuApps:</h5>
            <div class="row">
                <div class="col-6">
                    <div class="bg-grey p-2 m-1"><p><h5>Sesuai Syariah</h5></p>
                        <p>Kambing dan sapi yang disalurkan memenuhi kriteria untuk jadi hewan qurban.</p>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-grey p-2 m-1">
                        <p><h5>Harga Terjangkau</h5></p>
                        <p>Harga hewan qurban murah dengan kualitas daging yang baik.</p>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-grey p-2 m-1">
                        <p><h5>Tepat Sasaran</h5></p>
                        <p>Qurban akan disalurkan ke saudara kita yang benar-benar membutuhkan.</p>
                    </div>
                </div>
                <div class="col-6">
                    <div class="bg-grey p-2 m-1">
                        <p><h5>Transparan</h5></p>
                        <p>Dapat update dan laporan lengkap penyaluran setelah pemotongan hewan qurban.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12"></div>
    </div>
    <div class="form-group">
        <label>Jenis Hewan Kurban</label>
        <select class="form-control" name="donationNominal">
            <?php foreach ($trxkurban as $r): ?>
                <option value="<?php echo $r->kategoricode ?>|<?php echo $r->amount ?>"><?php echo $r->kategoriname ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="btn-group btn-group-toggle" data-toggle="buttons">
        <div class="row">
            <div class="col-6">
                <label class="btn btn-outline-primary">
                    <input type="radio" name="testbtn" value="1"> test 1
                </label>
            </div>
            <div class="col-6">
                <label class="btn btn-outline-primary">
                    <input type="radio" name="testbtn" value="2"> test 2
                </label>
            </div>
        </div>

        <label class="btn btn-outline-primary">
            <input type="radio" name="testbtn" value="2">test 2
        </label>
        <label class="btn btn-outline-primary">
            <input type="radio" name="testbtn" value="3">test 3
        </label>
    </div>
    <div class="form-group">
        <label>No. Handphone</label>
        <input name="donationPhone" type="text" class="form-control" placeholder="Masukkan Nomor Handphone Anda">
    </div>
    <div class="form-group">
        <label>Nama</label>
        <input name="donationName" type="text" class="form-control" placeholder="Masukkan Nama Anda">
        <!--        <div class="custom-control custom-checkbox mt-5">-->
        <!--            <input type="checkbox" class="custom-control-input" id="customCheck1">-->
        <!--            <label class="custom-control-label" for="customCheck1">Danai secara Anonim</label>-->
        <!--        </div>-->
    </div>
    <div class="form-group">
        <label>Email</label>
        <input name="donationEmail" <?php echo !empty($sessionsl) ? 'readonly' : ''; ?>
               value="<?php echo !empty($sessionsl) ? $sessionsl : ''; ?>" type="text" class="form-control"
               placeholder="Masukkan Email Anda">
    </div>
    <div class="form-group">
        <label>Komentar Dukungan Anda</label>
        <textarea name="donationCommment" class="form-control" placeholder="Berikan Komentar, Doa, atau Dukungan Anda"
                  rows="3"></textarea>
    </div>

    <hr/>
    <div class="form-group">

        <input type="hidden" name="donationPay" id="paymetod">
        <div class="row">
            <div class="col-12">
                <span><b><h5>Metode Pembayaran : </h5></b></span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div id="respemb"></div>
    </div>
    <hr/>
    <div class="mt-30">
        <button class="btn btn-block btn-main"><span>Bayar</span></button>
    </div>

    <?php

    echo form_close();

    ?>
    <div class="mb-10"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
    <script>

        $("#respemb").load("<?php echo base_url('/pembayaran/index') ?>");
        jQuery(document).on('click', '#pilbayar', function () {
            $("#settmpl").load("<?php echo base_url('/pembayaran/index') ?>");
        });
        jQuery(document).on('click', '.nextbayar', function () {
            $('#pills-pembayaran-tab').tab('show');
        });
        // $('#denomnms').number( true, 0, '', '.' );
        var fnf = document.getElementById("denomnms");
        fnf.addEventListener('keyup', function (evt) {
            var n = parseInt(this.value.replace(/\D/g, ''), 10);
            fnf.value = n.toLocaleString("id-ID");
        }, false);
    </script>


    {_layFooter}

