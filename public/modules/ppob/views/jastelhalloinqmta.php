{_layHeader}

<?php
/**
 * Created by PhpStorm.
 * Project : berzakat
 * User: fahmihilmansyah
 * Date: 28/07/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 20.37
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 * 56602548903
 */

$url = implode("/", $this->uri->segment_array());
$dats = array('urllink' => $url);
$this->session->set_userdata($dats);
$sessionsl = !empty($this->session->userdata(LGI_KEY . "login_info")) ? $this->session->userdata(LGI_KEY . "login_info")['email'] : '';
$sessionsn = !empty($this->session->userdata(LGI_KEY . "login_info")) ? $this->session->userdata(LGI_KEY . "login_info")['fname'] . " " . $this->session->userdata(LGI_KEY . "login_info")['login_lname'] : '';
$sessionsp = !empty($this->session->userdata(LGI_KEY . "login_info")) ? $this->session->userdata(LGI_KEY . "login_info")['phone'] : '';
if($result['rc'] != '00') {
?>

<div class="content">
    <div class="row">
        <div class="col-12">
            <h3 style="color: #09A59D;">Bayar Telkomsel Halo</h3>
            <span style="color: rgba(104,126,123,0.65);">Bayar Telkomsel Halo kapan saja dan dimana saja</span>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td>IDPEL / NO. METER</td>
                            <td>:</td>
                            <td><?php echo $billnumb ?></td>
                        </tr>
                        <tr class="bg-danger text-white">
                            <td>Code</td>
                            <td>:</td>
                            <td><?php echo $result['rc'] ?></td>
                        </tr>
                        <tr class="bg-danger text-white">
                            <td>Response</td>
                            <td>:</td>
                            <td><?php echo $result['data']['text_data'] ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php }else{
    ?>
<div class="content">
    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'ppob/multipayment/pembayaran', array('id' => 'fmdonasi', 'class' => 'fmdonasi'), $hidden_form);

    ?>

    <?php $totalbayar = $result['data']['amount'] + $result['data']['feeAmount'];?>
    <input type="hidden" name="donationNominal" value="<?php echo $totalbayar ?>">
    <input type="hidden" name="amount" value="<?php echo $totalbayar ?>">
    <input type="hidden" name="billnumb" value="<?php echo $billnumb ?>">
    <input type="hidden" name="jenisDonation" value="<?php echo $produk_amount ?>">
    <input type="hidden" name="kode_produk" value="<?php echo $kode_produk ?>">
    <input type="hidden" name="billreff" value="<?php echo $result['data']['billReff'] ?>">
    <input type="hidden" name="titletrx" value="Token Listrik <?php echo number_format($nominal); ?>">
    <div class="row">
        <div class="col-12">
            <h3 style="color: #09A59D;">Bayar Telkomsel Halo</h3>
            <span style="color: rgba(104,126,123,0.65);">Bayar Telkomsel Halo kapan saja dan dimana saja</span>
        </div>
        <div class="col-12 mb-10">
            <div class="card">
                <div class="card-body">
                    <div class="row" style="color: rgba(46,47,47,0.57)">
                        <?php
                        $resrd= explode('|',$result['data']['text_data']);
                        for ($x=1;$x<count($resrd);$x++):
                        $expad = explode(':',$resrd[$x]);
                            ?>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-5"><?php echo trim($expad[0])?></div>
                                <div class="col-1">:</div>
                                <div class="col-5"><?php echo trim($expad[1])?></div>
                                <div class="col-1">&nbsp;</div>
                            </div>
                        </div>
                        <?php endfor;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">

                            <div class="form-group">
                                <input type="hidden" name="donationPay" id="paymetod">
                                <div class="row">
                                    <div class="col-12">
                                        <span><b><h5>Metode Pembayaran : </h5></b></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="respemb"></div>
                            </div>
                            <?php if (empty($sessionsl)): ?>
                                <div class="form-group text-center">
            <span><a id="pagelogin"
                     href="<?php echo base_url('/register')?>">Daftar</a> atau lengkapi data dibawah ini.</span>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label>Nama</label>
                                <input name="donationName" type="text" <?php echo !empty($sessionsn) ? 'readonly' : ''; ?>
                                       value="<?php echo !empty($sessionsn) ? $sessionsn : ''; ?>" class="form-control"
                                       placeholder="Masukkan Nama Anda">
                            </div>
                            <div class="form-group">
                                <label>No. Handphone</label>
                                <input name="donationPhone" <?php echo !empty($sessionsp) ? 'readonly' : ''; ?>
                                       value="<?php echo !empty($sessionsp) ? $sessionsp : ''; ?>" type="text" class="form-control"
                                       placeholder="Masukkan Nomor Handphone Anda">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input name="donationEmail" <?php echo !empty($sessionsl) ? 'readonly' : ''; ?>
                                       value="<?php echo !empty($sessionsl) ? $sessionsl : ''; ?>" type="text" class="form-control"
                                       placeholder="Masukkan Email Anda">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row justify-content-md-center">
                                <div class="col-10  mb-10">
                                    <button type="submit" class="form-control btn btn-success">Bayar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php

    echo form_close();

    ?>

    <script>

        <?php $paramsd = [];
        foreach ($_GET as $k=>$v):
            $paramsd[] = $k."=".$v;
        endforeach;
        $setparams = implode('&',$paramsd);
        ?>
        $("#respemb").load("<?php echo base_url('/pembayaran/index?'.$setparams); ?>");
    </script>
{_layFooter}
    <?php }?>