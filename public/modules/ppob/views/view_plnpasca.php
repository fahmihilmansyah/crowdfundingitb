{_layHeader}

<?php
/**
 * Created by PhpStorm.
 * Project : berzakat
 * User: fahmihilmansyah
 * Date: 28/07/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 20.37
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 * 56602548903
 */
 ?>

<div class="content">
    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'ppob/multipayment/inqplnpascabayar', array('id' => 'fmdonasi', 'class' => 'fmdonasi'), $hidden_form);

    ?>
    <div class="row">
        <div class="col-12">
            <h3 style="color: #09A59D;">Bayar Listrik</h3>
            <span style="color: rgba(104,126,123,0.65);">Bayar listrik kapan saja dan dimana saja</span>
        </div>
        <div class="col-12 mb-10">
            <input type="text" name="billnumb" class="form-control" value="9999512345002" placeholder="Nomor Meter / ID Pelanggan">
            <input type="hidden" name="kode_produk" value="M1003" class="form-control">
        </div>
        <div class="col-12 mb-10">
            <div class="row justify-content-md-center">
                <div class="col-10  mb-10">
                    <button type="submit" class="form-control btn btn-main">Lanjut</button>
                </div>
            </div>
        </div>

    </div>

    <?php

    echo form_close();

    ?>

{_layFooter}