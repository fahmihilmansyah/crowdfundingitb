<?php
/**
 * Created by PhpStorm.
 * Project : berzakat
 * User: fahmihilmansyah
 * Date: 06/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.34
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$listoperator = array(
        '0811'=>'TSELPREPAID',
        '0812'=>'TSELPREPAID',
        '0813'=>'TSELPREPAID',
        '0821'=>'TSELPREPAID',
        '0822'=>'TSELPREPAID',
        '0823'=>'TSELPREPAID',
        '0851'=>'TSELPREPAID',
        '0852'=>'TSELPREPAID',
        '0853'=>'TSELPREPAID',
        '0898'=>'TRIPREPAID',
        '0899'=>'TRIPREPAID',
        '0896'=>'TRIPREPAID',
        '0897'=>'TRIPREPAID',
        '0814'=>'ISATPREPAID',
        '0816'=>'ISATPREPAID',
        '0856'=>'ISATPREPAID',
        '0857'=>'ISATPREPAID',
        '0858'=>'ISATPREPAID',
        '0889'=>'SMATFREN',
        '0881'=>'SMATFREN',
        '0882'=>'SMATFREN',
        '0883'=>'SMATFREN',
        '0884'=>'SMATFREN',
        '0885'=>'SMATFREN',
        '0886'=>'SMATFREN',
        '0887'=>'SMATFREN',
        '0888'=>'SMATFREN',
        '0828'=>'CERIA',
        '0868'=>'BYRU',
        '08315'=>'LIPOTEL',
        '0832'=>'AXIS',
        '0833'=>'AXIS',
        '0838'=>'AXIS',
);

$sessionsl = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
    $this->session->userdata(LGI_KEY . "login_info")['email'] : '';
 ?>
{_layHeader}
<style>
    .custom-radio-button div {
        display: inline-block;
    }
    .custom-radio-button input[type="radio"] {
        display: none;
    }
    .custom-radio-button input[type="radio"] + label {
        color: #333;
        font-family: Arial, sans-serif;
        font-size: 14px;
    }
    .custom-radio-button input[type="radio"] + label span {
        display: inline-block;
        width: 40px;
        height: 40px;
        margin: -1px 4px 0 0;
        vertical-align: middle;
        cursor: pointer;
        border-radius: 50%;
        border: 2px solid #ffffff;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
        background-repeat: no-repeat;
        background-position: center;
        text-align: center;
        line-height: 44px;
    }
    .custom-radio-button input[type="radio"] + label span img {
        opacity: 0;
        transition: all 0.3s ease;
    }
    .custom-radio-button input[type="radio"]#color-red + label span {
        background-color: red;
    }
    .custom-radio-button input[type="radio"]#color-blue + label span {
        background-color: blue;
    }
    .custom-radio-button input[type="radio"]#color-orange + label span {
        background-color: orange;
    }
    .custom-radio-button input[type="radio"]#color-pink + label span {
        background-color: pink;
    }
    .custom-radio-button input[type="radio"]:checked + label span {
        opacity: 1;
        background: url("https://www.positronx.io/wp-content/uploads/2019/06/tick-icon-4657-01.png")
        center center no-repeat;
        width: 40px;
        height: 40px;
        display: inline-block;
    }
</style>
<div class="content pr-15 pl-15 bg-white" style="padding-top: 10px;">
    <?php if (!empty($this->session->flashdata('error'))): ?>
        <div class="alert alert-danger alert-dismissable " role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <script type="text/javascript">
            setTimeout(function () {
                $('.alert').fadeOut();
            }, 10000); // <-- time in milliseconds
        </script>
    <?php endif; ?>
    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'pulsa' . $this->uri->segment(3), array('id' => 'fmdonasi', 'class' => 'fmdonasi mt-20'), $hidden_form);

    ?>
    <div class="card">
        <div class="card-header">Pembelian Pulsa</div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <label>No. Handphone</label>
                    <input type="text" id="nooper" name="donationPhone" class="form-control" placeholder="Ex: 081234566789">
                </div>
                <div class="col-12">
                    <label>Nominal</label>
                    <select class="form-control" id="resoper" name="donationNominal">
                        <option value=""></option>
                    </select>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Email</label>
                        <input name="donationEmail" <?php echo !empty($sessionsl)?'readonly':''; ?> value="<?php echo !empty($sessionsl)?$sessionsl:''; ?>" type="text" class="form-control" placeholder="Masukkan Email Anda">
                    </div>
                </div>
                <div class="mb-20">&nbsp;</div>
                <div class="col-12">

                    <hr/>
                    <div class="form-group">

                        <input type="hidden" name="donationPay" id="paymetod">
                        <div class="row" >
                            <div class="col-12">
                                <span><b><h5>Metode Pembayaran : </h5></b></span>
                            </div>
                            <div class="col-3" style="display: none;">
                                <img id="logobnk" src="#" style="width: 80px; display: none;">
                            </div>
                            <div class="col-6" style="display: none;">
                                <span id="namapembayran"></span>
                            </div>
                            <div class="col-3 " style="display: none;"> &nbsp;
                                <!--                <label id="pilbayar" data-paymetod="" class="btn float-right btn-sm btn-outline-primary">Ubah <i-->
                                <!--                            class="icofont icofont-caret-down"></i></label>-->
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div id="respemb"></div>
                    </div>
                    <hr/>
                    <div class="mt-30">
                        <button class="btn btn-block btn-main"><span>Bayar</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php

    echo form_close();

    ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
<script>
    jQuery(document).on('click', '#pilbayar', function () {
        $("#settmpl").load("<?php echo base_url('/pembayaran/index') ?>");
    });
        $('#denomnms').number( true, 0, '', '.' );
    var jsonoper = <?php echo json_encode($listoperator) ?>;
    jQuery(document).on('keypress','#nooper',function () {
        var nom = $(this).val().substring(0, 4);
        if(jsonoper[nom] !== undefined){
            $.ajax({
                url:"<?php echo base_url('pulsa/operator') ?>",
                method:"GET",
                data:{kodeoperator:jsonoper[nom]},
                beforeSend:function(){
                  $("#resoper").empty();
                },
                dataType:'JSON',
                success:function (msg) {
                    $("#resoper").empty();
                    $.each(msg,function (i,v) {
                        var total = parseFloat(this.price) + parseFloat(this.admin);
                        $("#resoper").append('<option value="'+this.type+'-'+this.prodId+'|'+total+'">'+this.name+' - Rp '+total+'</option>'
                        );
                        console.log(this.name, this);
                    });
                    // console.log(msg);
                }
            })
        }
    })
</script>

    {_layFooter}