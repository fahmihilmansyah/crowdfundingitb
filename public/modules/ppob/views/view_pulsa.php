{_layHeader}

<?php
/**
 * Created by PhpStorm.
 * Project : berzakat
 * User: fahmihilmansyah
 * Date: 28/07/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 20.37
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 * 56602548903
 */
 ?>

<div class="content">
    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'ppob/multipayment/pembayaran', array('id' => 'fmdonasi', 'class' => 'fmdonasi'), $hidden_form);

    ?>
    <div class="row">
        <div class="col-12">
            <h3 style="color: #09A59D;">Beli Pulsa</h3>
            <span style="color: rgba(104,126,123,0.65);">Beli Pulsa kapan saja dan dimana saja</span>
        </div>
        <div class="col-12 mb-10">
            <input type="text" name="billnumb" class="form-control" value="081310109000" placeholder="ID Pelanggan">
            <input type="hidden" name="jenisDonation" value="M11725" class="form-control">
<!--            <input type="hidden" name="kode_produk" value="M1150" class="form-control">-->
            <input type="hidden" name="amount" value="0" class="form-control">
            <input type="hidden" name="donationNominal" value="250000" class="form-control">
            <select name="kode_produk" class="form-control">
                <option value="M1150">Telkomsel 50.000</option>
                <option value="M11625">XL 25.000</option>
                <option>Smartfren 60.000</option>
                <option value="M11520">Smartfren 100.000</option>
                <option value="M11725">Indosat 25.000</option>
            </select>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">

                            <div class="form-group">
                                <input type="hidden" name="donationPay" id="paymetod">
                                <div class="row">
                                    <div class="col-12">
                                        <span><b><h5>Metode Pembayaran : </h5></b></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="respemb"></div>
                            </div>
                            <?php if (empty($sessionsl)): ?>
                                <div class="form-group text-center">
            <span><a id="pagelogin"
                     href="<?php echo base_url('/register')?>">Daftar</a> atau lengkapi data dibawah ini.</span>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label>Nama</label>
                                <input name="donationName" type="text" <?php echo !empty($sessionsn) ? 'readonly' : ''; ?>
                                       value="<?php echo !empty($sessionsn) ? $sessionsn : ''; ?>" class="form-control"
                                       placeholder="Masukkan Nama Anda">
                            </div>
                            <div class="form-group">
                                <label>No. Handphone</label>
                                <input name="donationPhone" <?php echo !empty($sessionsp) ? 'readonly' : ''; ?>
                                       value="<?php echo !empty($sessionsp) ? $sessionsp : ''; ?>" type="text" class="form-control"
                                       placeholder="Masukkan Nomor Handphone Anda">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input name="donationEmail" <?php echo !empty($sessionsl) ? 'readonly' : ''; ?>
                                       value="<?php echo !empty($sessionsl) ? $sessionsl : ''; ?>" type="text" class="form-control"
                                       placeholder="Masukkan Email Anda">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row justify-content-md-center">
                                <div class="col-10  mb-10">
                                    <button type="submit" class="form-control btn btn-success">Bayar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php

    echo form_close();

    ?>
    <script>

        <?php $paramsd = [];
        foreach ($_GET as $k=>$v):
            $paramsd[] = $k."=".$v;
        endforeach;
        $setparams = implode('&',$paramsd);
        ?>
        $("#respemb").load("<?php echo base_url('/pembayaran/index?'.$setparams); ?>");
    </script>
{_layFooter}