<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Pulsa extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : No Reply
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */

    private $_modList = array(
        "kurban/kurban_model" => "infaqmdl",
        "ppob/kurban_model"=>'donasimdl'
    );

    protected $_data = array();

    private $_SESSION;

    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

        $this->_SESSION = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
            $this->session->userdata(LGI_KEY . "login_info") : '';
        $this->load->library('myphpmailer');

    }


    private function modelsApp()

    {
        $this->load->model($this->_modList);
        $this->load->library('Fhhlib');

    }


    public function pulsa($data = array())

    {
        if ($_POST) {
            $jndon = explode("|", $this->input->post('donationNominal'));
            $datadonat = array(
                'prevnomDonation' => $jndon[1],
                'prevjenisDonation' => $jndon[0],
                'prevcomDonation' => $this->input->post('donationCommment'),
                'statusActive' => 'active',
            );
            $this->session->set_userdata($datadonat);

            $jenis = $this->session->userdata('prevjenisDonation');
            $nominal = $this->session->userdata('prevnomDonation');
            $comment = $this->session->userdata('prevcomDonation');
            $donationName = $this->input->post('donationName');
            $donationEmail = $this->input->post('donationEmail');
            $donationPhone = empty($this->input->post('donationPhone'))?'081234567899':$this->input->post('donationPhone');
            $methodpay = $this->input->post('donationPay');
            $error = "";
            if (empty($nominal) || !is_numeric($nominal)) {
                $error .= '- Nominal harus diisi <br>';
            }
            if (empty($donationName)) {
                $donationName = 'Anonim';
            }
            if (empty($methodpay)) {
                $error .= '- Metode pembayaran harus dipilih. <br>';
            }
            if (empty($donationEmail) || empty($donationPhone) || strlen($donationPhone) < 9) {
                $error .= '- Email / Telp harus diisi (minimal 10 karakter) <br>';
            }
            if (!empty($error)) {
                $dataerror = array('error' => $error);
                $this->session->set_flashdata($dataerror);
                redirect('pulsa');
                exit;
            }

            $notrx = date('Ymd') . sprintf('%05d', $this->geninc());

            $uniqcode = $this->genUniq($nominal);

            $iddonatur = empty($this->_SESSION['userid']) ? '0' : $this->_SESSION['userid'];

            $insertData = array(
                'no_transaksi' => $notrx,
                'nominal' => $nominal,
                'trx_date' => date('Y-m-d'),
                'jenistrx' => $jenis,
                'donatur' => $iddonatur,/*ubah donaturnya jika login maka akan mengambil session loginnya*/
                'bank' => $methodpay,
                'comment' => $comment,
                'uniqcode' => $uniqcode,//($methodpay == 919 ? 0 : $uniqcode),
                'namaDonatur' => $donationName,
                'emailDonatur' => $donationEmail,
                'telpDonatur' => $donationPhone,
                'nomuniq' => $nominal + $uniqcode,//($methodpay == 919 ? 0 : $uniqcode),
                'validDate' => date('Y-m-d', strtotime('+2 days')),
                'crtd_at' => date("Y-m-d H:i:s"),
                'edtd_at' => date("Y-m-d H:i:s"),
                'month' => date("m"),
                'year' => date("Y"),
            );

            $this->infaqmdl->custom_insert("nb_trans_program", $insertData);
            $array_items = array(

                'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive', 'prevanomDonation', 'prevcampDonation');

            $this->session->unset_userdata($array_items);

            $datadonat = array(

                'doneDonation' => $notrx,

            );

            $this->session->set_userdata($datadonat);

            $idipg = Fhhlib::uuidv4Nopad();

            $arrParam['idipg'] = $idipg;
            $expmethod = explode('|', $methodpay);
            if ($expmethod[0] == 'FINPAY') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = ($donationPhone);
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                $arrParam['items'] = "Pembelian Pulsa ";
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                    }
                }
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = "Pembelian Pulsa";//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_donation',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

                $this->donasimdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $res = Fhhlib::finpayIPG($arrParam);
//            $res = $this->finpayIPG($nominal,$donationEmail,$iddonatur,$donationPhone,$donationName,$notrx);
                if (!empty($res)) {
                    if ($res['status_code'] == '00') {
                        if (!empty($res['landing_page'])) {
                            redirect($res['landing_page'], 'refresh');
                        } elseif (!empty($res['redirect_url'])) {
                            redirect($res['redirect_url'], 'refresh');
                        } else {
                            $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                            $this->session->set_flashdata($dataerror);
                            redirect('donasi/index/' . $id);
                        }
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('donasi/index/' . $id);
                        exit;
                    }
                }
//                header("Location: ".$res);
                print_r($res);
                exit;
//            print_r($res);exit;
            }elseif ($expmethod[0] == 'MUPAYS') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = ($donationPhone);
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                $arrParam['token_login'] = !empty($this->_SESSION['mupays_token'])?$this->_SESSION['mupays_token']:"";
                $arrParam['items'] = "Pembayaran Donasi Untuk " . $title_campaign;
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                    }
                }
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = "Pembayaran Donasi Untuk " . $title_campaign;//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_donation',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

                $this->donasimdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $res = Fhhlib::devfinpayIPG($arrParam);
//            $res = $this->finpayIPG($nominal,$donationEmail,$iddonatur,$donationPhone,$donationName,$notrx);
                if (!empty($res)) {
                    if ($res['rc'] == '0000') {
                        if (!empty($res['data']['no_va'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['hasil'] = $res['data'];
                            return $this->parser->parse("view_vatrx", $datas);
                        }
                        if (!empty($res['data']['landing_page'])) {
                            return redirect($res['data']['landing_page'], 'refresh');
                        }
                        if (!empty($res['data']['qris'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['url_img'] = $res['data']['qris']['url_img'];
                            $datas['nominal'] = $nominal;
                            return $this->parser->parse("view_qris", $datas);
                        }
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('donasi/index/' . $id);
                        exit;
                    }
                }
//                header("Location: ".$res);
                print_r($res);
                exit;
//            print_r($res);exit;
            } elseif($expmethod[0] == 'E2PAY') {
                $cariipg = $this->donasimdl->custom_query('nb_partner_ipg', ['where' => ['id' => $this->db->escape('E2PAY')]])->result();
                $key = !empty($cariipg[0]->key) ? $cariipg[0]->key : "v1XdZSLMs9";
                $merchant_id = !empty($cariipg[0]->merchant_key) ? $cariipg[0]->merchant_key : "IF00294";
                $merchantkey = $key;//'v1XdZSLMs9';
                $merchantcode = $merchant_id;//'IF00294';
                $arrParam['url_host'] = $cariipg[0]->url_host;
                $arrParam['MerchantCode'] = $merchantcode;
                $arrParam['PaymentId'] = $expmethod[1];
                $arrParam['RefNo'] = $notrx;
                $arrParam['Amount'] = $nominal . "00";
                $arrParam['Currency'] = 'IDR';
                $arrParam['ProdDesc'] = "Pembelian Pulsa";
                $arrParam['UserName'] = $donationName;
                $arrParam['UserEmail'] = $donationEmail;
                $arrParam['UserContact'] = $donationPhone;
                $signatures = Fhhlib::E2Pay_signature($merchantkey . $merchantcode . $arrParam['RefNo'] . $arrParam['Amount'] . $arrParam['Currency']);
                $arrParam['Signature'] = $signatures;
                $arrParam['Remark'] = '';
                $arrParam['ResponseURL'] = base_url('trxipg/responsee2pay');
                $arrParam['BackendURL'] = base_url('trxipg/backende2pay');

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_donation',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

//                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $this->donasimdl->custom_insert("nb_trans_e2pay", $insipgtrx);
                $datas = $this->_data;
                $datas['parame2pay'] = $arrParam;
////            print_r($datas);
                return $this->parser->parse("view_e2pay", $datas);

                $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . $res);
                $this->session->set_flashdata($dataerror);
                redirect('donasi/index/' . $id);

            }
            exit;
        }
        $data = $this->_data;
        $postdata = ['grant_type' => 'password',
            'username' => 'mumuweb1', 'password' => 'mumuweb123'];
        $HEADER = ['authorization: Basic bG9jYWxkZXY6dDYxRklMNUJuV2U3Wmp5cDlncTNFWE1KaERZ',
            'content-type: application/x-www-form-urlencoded'];
        $postdata = http_build_query($postdata);
        $token = $this->_curl_exexc("http://35.240.184.113/am/mydomain/oauth/token", $postdata, 'POST', $HEADER);
        $_SESSION['tokenppob'] = json_decode($token, true);
        $this->parser->parse("index_pulsa", $data);

    }

    function getlistoperator()
    {
        $data = $this->_data;
        if (empty($_SESSION['tokenppob']['access_token'])) {
            echo 'Error get token';
            exit;
        }
        $kodeoperator = $_GET['kodeoperator'];
        $tokenopertaor = $_SESSION['tokenppob']['access_token'];
        $HEADER = ['authorization: Bearer ' . $tokenopertaor,
            'content-type: application/json'];
        $postdata = ['id' => 'mumuweb1',
            'props' => array('prodType' => $kodeoperator),
        ];
        $postdata = json_encode($postdata);
        $datapulsa = $this->_curl_exexc("http://34.87.70.137/gateway/products", $postdata, 'POST', $HEADER);
        echo($datapulsa);
        exit;
    }

    function listrik()
    {
        $data = $this->_data;
        $this->parser->parse("index_listrik", $data);
    }

    private function _curl_exexc($url = '', $postdata = '', $METHOD = 'POST', $HEADER = [])
    {
//        $postdata = http_build_query($data);
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADER);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }

    private function geninc()
    {
        $kondisi['where'] = array('id' => $this->db->escape(1));
        $generate = $this->infaqmdl->custom_query("nb_inc", $kondisi)->result();
        $tgl = $generate[0]->tgl;
        $uniqinc = 0;
        if ($tgl != date("Y-m-d")) {
            $uniqinc = $uniqinc + 1;
            //echo $uniqinc."||".$tgl;
        } else {
            $uniqinc = $generate[0]->inc + 1;
            //echo "else : ".$uniqinc;
        }
        $this->infaqmdl->custom_update("nb_inc", array('inc' => $uniqinc, 'tgl' => date("Y-m-d")), array('id' => $this->db->escape(1)));
        //echo $this->db->last_query();
        return $uniqinc;
    }

    private function genUniq($nominal = 0)
    {
        $condition['where'] = array(
            $this->db->escape($nominal) . ' BETWEEN' => ' minlimit and maxlimit'
        );
        $generate = $this->infaqmdl->custom_query("nb_incuniq", $condition)->result();
        $uniqid = $generate[0]->id;
        $uniqinc = $generate[0]->uniqinc + 1;
        if ($uniqinc > $generate[0]->maxuniq) {
            $uniqinc = $generate[0]->minuniq;
        }
        $this->infaqmdl->custom_update("nb_incuniq", array('uniqinc' => $uniqinc), array('id' => $uniqid));
        return $uniqinc;
    }
}

