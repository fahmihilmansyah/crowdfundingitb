<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Multipayment extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */
    private $_modList = array(
        "ppob/kurban_model" => "infaqmdl"
    );


    protected $_data = array();

    private $_SESSION;


    function __construct()
    {

        parent::__construct();
        // -----------------------------------
        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN
        // -----------------------------------
        $this->modelsApp();
        $this->_SESSION = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
            $this->session->userdata(LGI_KEY . "login_info") : '';
    }


    private function modelsApp()
    {

        $this->load->model($this->_modList);
        $this->load->library('Fhhlib');
        // REDIRECT IF SESSION EXIST
        // $this->nbauth_front->lookingForAuten();
    }


    public function index($data = array())
    {
        if ($_POST) {
            $jndon = explode("|", $this->input->post('donationNominal'));
            $cariharga = $this->db->from('nb_trans_program_kategori')->where(['kategoricode' => $jndon[0]])->get()->result();
            $datadonat = array(
                'prevnomDonation' => $cariharga[0]->amount,
                'prevjenisDonation' => $jndon[0],
                'prevcomDonation' => $this->input->post('donationCommment'),
                'statusActive' => 'active',
            );
            $this->session->set_userdata($datadonat);
            $jenis = $this->session->userdata('prevjenisDonation');
            $nominal = $this->session->userdata('prevnomDonation');
            $comment = $this->session->userdata('prevcomDonation');
            $donationName = $this->input->post('donationName');
            $donationEmail = $this->input->post('donationEmail');
            $donationPhone = empty($this->input->post('donationPhone')) ? '081234567899' : $this->input->post('donationPhone');
            $methodpay = $this->input->post('donationPay');
            $error = "";
            if (empty($jenis)) {
                $error .= '- Kesalahan sistem, hubungin administrator. <br>';
            }
            if ($nominal < 29999 || !is_numeric($nominal)) {
                $error .= '- Nominal dana harus minimal Rp.30.000 <br>';
            }
            if (empty($methodpay)) {
                $error .= '- Metode pembayaran harus dipilih. <br>';
            }
            if (empty($donationEmail) || empty($donationPhone) || strlen($donationPhone) < 9) {
                $error .= '- Email / Telp harus diisi (minimal 10 karakter) <br>';
            }
            if (!empty($error)) {
                $dataerror = array('error' => $error);
                $this->session->set_flashdata($dataerror);
                redirect('kurban#gagalnotif');
                exit;
            }
            $notrx = date('Ymd') . sprintf('%05d', $this->geninc());
            $uniqcode = 0;//$this->genUniq($nominal);
            $iddonatur = empty($this->_SESSION['userid']) ? 0 : $this->_SESSION['userid'];
            $insertData = array(
                'no_transaksi' => $notrx,
                'nominal' => $nominal,
                'trx_date' => date('Y-m-d'),
                'jenistrx' => $jenis,
                'donatur' => $iddonatur,/*ubah donaturnya jika login maka akan mengambil session loginnya*/
                'bank' => $methodpay,
                'comment' => $comment,
                'uniqcode' => $uniqcode,//($methodpay == 919 ? 0 : $uniqcode),
                'namaDonatur' => $donationName,
                'emailDonatur' => $donationEmail,
                'telpDonatur' => $donationPhone,
                'nomuniq' => $nominal + $uniqcode,//($methodpay == 919 ? 0 : $uniqcode),
                'validDate' => date('Y-m-d', strtotime('+2 days')),
                'crtd_at' => date("Y-m-d H:i:s"),
                'edtd_at' => date("Y-m-d H:i:s"),
                'month' => date("m"),
                'year' => date("Y"),
            );
            $this->infaqmdl->custom_insert("nb_trans_program", $insertData);
            $array_items = array(
                'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive');
            $this->session->unset_userdata($array_items);
            $datadonat = array(
                'doneDonation' => $notrx,
            );
            $this->session->set_userdata($datadonat);
            /*cek jika pemilihan melalui dompetku*/
//                if ($methodpay == 919) {
//                    $this->trxDeposit($iddonatur, $nominal, $notrx);
//                    redirect('invoice/depoprogram/'.$notrx);
//                }else{
//                    redirect('invoice/program/'.$notrx);
//                }
            if ($methodpay == 'SALDO_DOMPET' && !empty($this->_SESSION['userid'])) {
                $condition['where'] = array(
                    'donatur' => $this->db->escape($this->_SESSION['userid']),
                );
                $ceksaldo = $this->infaqmdl->custom_query("nb_donaturs_saldo", $condition)->result();
                if ($ceksaldo[0]->balanceAmount < $nominal):
                    $dataerror = array('error' => 'Saldo anda tidak mencukupi. Saldo : ' . $ceksaldo[0]->balanceAmount);
                    $this->session->set_flashdata($dataerror);
                    redirect('kurban');
                    exit;
                endif;
                Fhhlib::updateSaldo($this->_SESSION['userid'], $nominal, $notrx);
                Fhhlib::updateTrans('nb_trans_program', $notrx, 'verified');
                $data = $this->_data;
                $data['title'] = 'Transaksi Sukses';
                $data['trxid'] = $notrx;
                return $this->parser->parse("v_sukses", $data);
            }
            $idipg = Fhhlib::uuidv4Nopad();
            $arrParam['idipg'] = $idipg;
            $expmethod = explode('|', $methodpay);
            if ($expmethod[0] == 'FINPAY') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = $donationPhone;
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
//            $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["' . $cariharga[0]->kategoriname . '",' . $nominal . ',1]]';
                    }
                }
                $arrParam['items'] = $cariharga[0]->kategoriname;
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = $cariharga[0]->kategoriname;//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes
//            $merchantkey ='v1XdZSLMs9';
//            $merchantcode ='IF00294';
//            $arrParam['MerchantCode'] = $merchantcode;
//            $arrParam['PaymentId'] = 10;
//            $arrParam['RefNo'] = $notrx;
//            $arrParam['Amount'] =$nominal;
//            $arrParam['Currency'] = 'IDR';
//            $arrParam['ProdDesc'] = "Pembayaran Hewan Kurban";
//            $arrParam['UserName'] = $donationName;
//            $arrParam['UserEmail'] = $donationEmail;
//            $arrParam['UserContact'] = $donationPhone;
//            $signatures = Fhhlib::E2Pay_signature($merchantkey.$merchantcode.$arrParam['RefNo'].$arrParam['Amount'].$arrParam['Currency']);
//            $arrParam['Signature'] = $signatures;
//            $arrParam['Remark'] = '';
//            $arrParam['ResponseURL'] = base_url('trxipg/responsee2pay');
//            $arrParam['BackendURL'] = base_url('trxipg/backende2pay');
                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_program',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );
                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
//            $this->infaqmdl->custom_insert("nb_trans_e2pay", $insipgtrx);
//            $datas = $this->_data;
//            $datas['parame2pay']=$arrParam;
////            print_r($datas);
//           return $this->parser->parse("view_e2pay", $datas);
//            $res = Fhhlib::e2payIPG($arrParam);
//            echo ($res);
//            exit;
                $res = Fhhlib::finpayIPG($arrParam);//$this->finpayIPG($arrParam);
                if (!empty($res)) {
                    if (!empty($res['landing_page'])) {
                        redirect($res['landing_page']);
                    } elseif (!empty($res['redirect_url'])) {
                        redirect($res['redirect_url']);
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('kurban');
                    }
//                header("Location: ".$res);
                    print_r($res);
                    exit;
//                return $res;
//                header("Location: ".$res);
//                redirect($res, 'refresh');
//            print_r($res);exit;
                } else {

                    $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . $res);
                    $this->session->set_flashdata($dataerror);
                    redirect('kurban');

                }
                exit;
            } elseif ($expmethod[0] == 'MUPAYS') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = ($donationPhone);
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                $arrParam['token_login'] = !empty($this->_SESSION['mupays_token']) ? $this->_SESSION['mupays_token'] : "";
                $arrParam['items'] = $cariharga[0]->kategoriname;
//                if (!empty($carilist)) {
//                    if ($carilist[0]->typeipg == 'emoney') {
//                        $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
//                    }
//                }
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = $cariharga[0]->kategoriname;//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes
                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_program',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );
                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $res = Fhhlib::devfinpayIPG($arrParam);
//            $res = $this->finpayIPG($nominal,$donationEmail,$iddonatur,$donationPhone,$donationName,$notrx);
                if (!empty($res)) {
                    if ($res['rc'] == '0000') {
                        if (!empty($res['data']['no_va'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['hasil'] = $res['data'];
                            return $this->parser->parse("view_vatrx", $datas);
                        }
                        if (!empty($res['data']['landing_page'])) {
                            return redirect($res['data']['landing_page']);
                        }
                        if (!empty($res['data']['qris'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['url_img'] = $res['data']['qris']['url_img'];
                            $datas['nominal'] = $nominal;
                            return $this->parser->parse("view_qris", $datas);
                        }
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('ppob/multipayment');
                        exit;
                    }
                }
//                header("Location: ".$res);
                print_r($res);
                exit;
//            print_r($res);exit;
            } elseif ($expmethod[0] == 'E2PAY') {
                $cariipg = $this->infaqmdl->custom_query('nb_partner_ipg', ['where' => ['id' => $this->db->escape('E2PAY')]])->result();
                $key = !empty($cariipg[0]->key) ? $cariipg[0]->key : "v1XdZSLMs9";
                $merchant_id = !empty($cariipg[0]->merchant_key) ? $cariipg[0]->merchant_key : "IF00294";
                $merchantkey = $key;//'v1XdZSLMs9';
                $merchantcode = $merchant_id;//'IF00294';
                $arrParam['url_host'] = $cariipg[0]->url_host;
                $arrParam['MerchantCode'] = $merchantcode;
                $arrParam['PaymentId'] = $expmethod[1];
                $arrParam['RefNo'] = $notrx;
                $arrParam['Amount'] = $nominal . "00";
                $arrParam['Currency'] = 'IDR';
                $arrParam['ProdDesc'] = "Pembayaran Hewan Kurban ";
                $arrParam['UserName'] = $donationName;
                $arrParam['UserEmail'] = $donationEmail;
                $arrParam['UserContact'] = $donationPhone;
                $signatures = Fhhlib::E2Pay_signature($merchantkey . $merchantcode . $arrParam['RefNo'] . $arrParam['Amount'] . $arrParam['Currency']);
                $arrParam['Signature'] = $signatures;
                $arrParam['Remark'] = '';
                $arrParam['ResponseURL'] = base_url('trxipg/responsee2pay');
                $arrParam['BackendURL'] = base_url('trxipg/backende2pay');
                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_program',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );
//                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $this->infaqmdl->custom_insert("nb_trans_e2pay", $insipgtrx);
                $datas = $this->_data;
                $datas['parame2pay'] = $arrParam;
////            print_r($datas);
                return $this->parser->parse("view_e2pay", $datas);
                $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . $res);
                $this->session->set_flashdata($dataerror);
                redirect('ppob/multipayment');

            }
            redirect('kurban');
        } else {
            if ($this->session->userdata('prevjenisDonation') != "kurban") {
                $array_items = array(
                    'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive');
                $this->session->unset_userdata($array_items);
            }
            $data = $this->_data;
            $data['jtrx'] = !empty($_GET['jtrx']) ? $_GET['jtrx'] : '';
            $data['cpartner'] = !empty($_GET['cpartner']) ? $_GET['cpartner'] : '';
            $data['pgtrx'] = !empty($_GET['pgtrx']) ? $_GET['pgtrx'] : '';
            $data['kdtrx'] = !empty($_GET['kdtrx']) ? $_GET['kdtrx'] : '';
            $kondisi['where'] = array('nb_trans_program_kategori.type' => $this->db->escape("KURBAN"));
            if (!empty($_GET['cpartner'])) {
                $kondisi['where']['nb_trans_program_kategori.mitra_id'] = $this->db->escape($_GET['cpartner']);
            }
            if (!empty($_GET['kdtrx'])) {
                $kondisi['where']['nb_trans_program_kategori.kategoricode'] = $this->db->escape($_GET['kdtrx']);
            }
            if (!empty($_GET['kdtrx']) && !empty($_GET['jtrx'])) {
                $kondisi['where']['nb_trans_program_kategori.type'] = $this->db->escape('KURBANPIL');
            }
            $data['trxkurban'] = $this->infaqmdl->custom_query("nb_trans_program_kategori", $kondisi)->result();
            $this->parser->parse("index", $data);
        }
    }

    function plnprabayar()
    {
        $data = $this->_data;
        return $this->parser->parse('view_pln', $data);
    }
    function plnppascabayar()
    {
        $data = $this->_data;
        return $this->parser->parse('view_plnpasca', $data);
    }
    function jastelkom()
    {
        $data = $this->_data;
        return $this->parser->parse('view_jastel', $data);
    }
    function telhallo()
    {
        $data = $this->_data;
        return $this->parser->parse('view_jastelhllo', $data);
    }
    function telhallomta()
    {
        $data = $this->_data;
        return $this->parser->parse('view_jastelhllomta', $data);
    }

    function inqplnprabayar()
    {
        if ($_POST) {
            $amount = empty($_POST['amount']) ? 0 : $_POST['amount'];
            $billNumber = empty($_POST['billnumb']) ? 0 : $_POST['billnumb'];
            $kode_produk = empty($_POST['kode_produk']) ? 0 : $_POST['kode_produk'];
            $isi = array(
                'kode_produk' => $kode_produk,
                'trxid' => uniqid('INQPLNPRE'),
                'trx_type' => 'INQ',
                'amount' => $amount,
                'billNumber' => $billNumber//'699992001'
            );
            $res = $this->_koneksicore($isi);
            $data = $this->_data;
            $data['billnumb'] = $billNumber;
            $data['kode_produk'] = $kode_produk;
            $data['produk_amount'] = $kode_produk.$amount;
            $data['nominal'] = $amount;
            $data['result'] = json_decode($res, true);
            return $this->parser->parse('plnprabayarinq', $data);
            print_r($isi);
            exit;
        }
    }
    function inqplnpascabayar()
    {
        if ($_POST) {
            $amount = empty($_POST['amount']) ? 0 : $_POST['amount'];
            $billNumber = empty($_POST['billnumb']) ? 0 : $_POST['billnumb'];
            $kode_produk = empty($_POST['kode_produk']) ? 'M1003' : $_POST['kode_produk'];
            $isi = array(
                'kode_produk' => $kode_produk,
                'trxid' => uniqid('INQPLNPASCA'),
                'trx_type' => 'INQ',
                'amount' => $amount,
                'billNumber' => $billNumber//'699992001'
            );
            $res = $this->_koneksicore($isi);
            $data = $this->_data;
            $data['billnumb'] = $billNumber;
            $data['kode_produk'] = $kode_produk;
            $data['produk_amount'] = $kode_produk;
            $data['nominal'] = $amount;
            $data['result'] = json_decode($res, true);
            return $this->parser->parse('plnpostpaidinq', $data);
            print_r($isi);
            exit;
        }
    }
    function inqtelkomjastel()
    {
        if ($_POST) {
            $arrtelp = array('0627'=>'0627',
                '0629'=>'0629',
                '0641'=>'0641',
                '0642'=>'0642',
                '0643'=>'0643',
                '0644'=>'0644',
                '0645'=>'0645',
                '0646'=>'0646',
                '0650'=>'0650',
                '0651'=>'0651',
                '0652'=>'0652',
                '0653'=>'0653',
                '0654'=>'0654',
                '0655'=>'0655',
                '0656'=>'0656',
                '0657'=>'0657',
                '0658'=>'0658',
                '0659'=>'0659',
                '061'=>'061',
                '0620'=>'0620',
                '0621'=>'0621',
                '0622'=>'0622',
                '0623'=>'0623',
                '0624'=>'0624',
                '0625'=>'0625',
                '0626'=>'0626',
                '0627'=>'0627',
                '0628'=>'0628',
                '0630'=>'0630',
                '0631'=>'0631',
                '0632'=>'0632',
                '0633'=>'0633',
                '0634'=>'0634',
                '0635'=>'0635',
                '0636'=>'0636',
                '0639'=>'0639',
                '0751'=>'0751',
                '0752'=>'0752',
                '0753'=>'0753',
                '0754'=>'0754',
                '0755'=>'0755',
                '0756'=>'0756',
                '0757'=>'0757',
                '0760'=>'0760',
                '0761'=>'0761',
                '0762'=>'0762',
                '0763'=>'0763',
                '0764'=>'0764',
                '0765'=>'0765',
                '0766'=>'0766',
                '0767'=>'0767',
                '0768'=>'0768',
                '0769'=>'0769',
                '0624'=>'0624',
                '0770'=>'0770',
                '0771'=>'0771',
                '0772'=>'0772',
                '0773'=>'0773',
                '0776'=>'0776',
                '0777'=>'0777',
                '0778'=>'0778',
                '0779'=>'0779',
                '0740'=>'0740',
                '0741'=>'0741',
                '0742'=>'0742',
                '0743'=>'0743',
                '0744'=>'0744',
                '0745'=>'0745',
                '0746'=>'0746',
                '0747'=>'0747',
                '0748'=>'0748',
                '0702'=>'0702',
                '0711'=>'0711',
                '0712'=>'0712',
                '0713'=>'0713',
                '0714'=>'0714',
                '0730'=>'0730',
                '0731'=>'0731',
                '0733'=>'0733',
                '0734'=>'0734',
                '0735'=>'0735',
                '0715'=>'0715',
                '0716'=>'0716',
                '0717'=>'0717',
                '0718'=>'0718',
                '0719'=>'0719',
                '0732'=>'0732',
                '0736'=>'0736',
                '0737'=>'0737',
                '0738'=>'0738',
                '0739'=>'0739',
                '0721'=>'0721',
                '0722'=>'0722',
                '0723'=>'0723',
                '0724'=>'0724',
                '0725'=>'0725',
                '0726'=>'0726',
                '0727'=>'0727',
                '0728'=>'0728',
                '0729'=>'0729',
                '021'=>'021',
                '021'=>'021',
                '0252'=>'0252',
                '0253'=>'0253',
                '0254'=>'0254',
                '0257'=>'0257',
                '021'=>'021',
                '022'=>'022',
                '0231'=>'0231',
                '0232'=>'0232',
                '0233'=>'0233',
                '0234'=>'0234',
                '0251'=>'0251',
                '0260'=>'0260',
                '0261'=>'0261',
                '0262'=>'0262',
                '0263'=>'0263',
                '0264'=>'0264',
                '0265'=>'0265',
                '0266'=>'0266',
                '0267'=>'0267',
                '024'=>'024',
                '0271'=>'0271',
                '0272'=>'0272',
                '0273'=>'0273',
                '0274'=>'0274',
                '0275'=>'0275',
                '0276'=>'0276',
                '0280'=>'0280',
                '0281'=>'0281',
                '0282'=>'0282',
                '0283'=>'0283',
                '0284'=>'0284',
                '0285'=>'0285',
                '0286'=>'0286',
                '0287'=>'0287',
                '0289'=>'0289',
                '0291'=>'0291',
                '0292'=>'0292',
                '0293'=>'0293',
                '0294'=>'0294',
                '0295'=>'0295',
                '0296'=>'0296',
                '0297'=>'0297',
                '0298'=>'0298',
                '0299'=>'0299',
                '0356'=>'0356',
                '0274'=>'0274',
                '031'=>'031',
                '0321'=>'0321',
                '0322'=>'0322',
                '0323'=>'0323',
                '0324'=>'0324',
                '0325'=>'0325',
                '0327'=>'0327',
                '0328'=>'0328',
                '0331'=>'0331',
                '0332'=>'0332',
                '0333'=>'0333',
                '0334'=>'0334',
                '0335'=>'0335',
                '0336'=>'0336',
                '0338'=>'0338',
                '0341'=>'0341',
                '0342'=>'0342',
                '0343'=>'0343',
                '0351'=>'0351',
                '0352'=>'0352',
                '0353'=>'0353',
                '0354'=>'0354',
                '0355'=>'0355',
                '0356'=>'0356',
                '0357'=>'0357',
                '0358'=>'0358',
                '0361'=>'0361',
                '0362'=>'0362',
                '0363'=>'0363',
                '0365'=>'0365',
                '0366'=>'0366',
                '0368'=>'0368',
                '0364'=>'0364',
                '0370'=>'0370',
                '0371'=>'0371',
                '0372'=>'0372',
                '0373'=>'0373',
                '0374'=>'0374',
                '0376'=>'0376',
                '0380'=>'0380',
                '0381'=>'0381',
                '0382'=>'0382',
                '0383'=>'0383',
                '0384'=>'0384',
                '0385'=>'0385',
                '0386'=>'0386',
                '0387'=>'0387',
                '0388'=>'0388',
                '0389'=>'0389',
                '0561'=>'0561',
                '0562'=>'0562',
                '0563'=>'0563',
                '0564'=>'0564',
                '0565'=>'0565',
                '0567'=>'0567',
                '0568'=>'0568',
                '0534'=>'0534',
                '0513'=>'0513',
                '0522'=>'0522',
                '0525'=>'0525',
                '0526'=>'0526',
                '0528'=>'0528',
                '0531'=>'0531',
                '0532'=>'0532',
                '0536'=>'0536',
                '0537'=>'0537',
                '0538'=>'0538',
                '0539'=>'0539',
                '0511'=>'0511',
                '0512'=>'0512',
                '0517'=>'0517',
                '0518'=>'0518',
                '0526'=>'0526',
                '0527'=>'0527',
                '0541'=>'0541',
                '0542'=>'0542',
                '0543'=>'0543',
                '0545'=>'0545',
                '0548'=>'0548',
                '0549'=>'0549',
                '0554'=>'0554',
                '0551'=>'0551',
                '0552'=>'0552',
                '0553'=>'0553',
                '0556'=>'0556',
                '0430'=>'0430',
                '0431'=>'0431',
                '0432'=>'0432',
                '0434'=>'0434',
                '0438'=>'0438',
                '0435'=>'0435',
                '0443'=>'0443',
                '0445'=>'0445',
                '0450'=>'0450',
                '0451'=>'0451',
                '0452'=>'0452',
                '0453'=>'0453',
                '0454'=>'0454',
                '0457'=>'0457',
                '0458'=>'0458',
                '0461'=>'0461',
                '0462'=>'0462',
                '0463'=>'0463',
                '0464'=>'0464',
                '0465'=>'0465',
                '0455'=>'0455',
                '0422'=>'0422',
                '0426'=>'0426',
                '0428'=>'0428',
                '0410'=>'0410',
                '0411'=>'0411',
                '0413'=>'0413',
                '0414'=>'0414',
                '0417'=>'0417',
                '0418'=>'0418',
                '0419'=>'0419',
                '0420'=>'0420',
                '0421'=>'0421',
                '0423'=>'0423',
                '0427'=>'0427',
                '0471'=>'0471',
                '0472'=>'0472',
                '0473'=>'0473',
                '0474'=>'0474',
                '0475'=>'0475',
                '0481'=>'0481',
                '0482'=>'0482',
                '0484'=>'0484',
                '0485'=>'0485',
                '0401'=>'0401',
                '0402'=>'0402',
                '0403'=>'0403',
                '0404'=>'0404',
                '0405'=>'0405',
                '0408'=>'0408',
                '0910'=>'0910',
                '0911'=>'0911',
                '0913'=>'0913',
                '0914'=>'0914',
                '0915'=>'0915',
                '0916'=>'0916',
                '0917'=>'0917',
                '0918'=>'0918',
                '0921'=>'0921',
                '0922'=>'0922',
                '0923'=>'0923',
                '0924'=>'0924',
                '0927'=>'0927',
                '0929'=>'0929',
                '0931'=>'0931',
                '0901'=>'0901',
                '0902'=>'0902',
                '0951'=>'0951',
                '0952'=>'0952',
                '0955'=>'0955',
                '0956'=>'0956',
                '0957'=>'0957',
                '0966'=>'0966',
                '0967'=>'0967',
                '0969'=>'0969',
                '0971'=>'0971',
                '0975'=>'0975',
                '0980'=>'0980',
                '0981'=>'0981',
                '0983'=>'0983',
                '0984'=>'0984',
                '0985'=>'0985',
                '0986'=>'0986');
            $amount = empty($_POST['amount']) ? 0 : $_POST['amount'];
            $billNumber = empty($_POST['billnumb']) ? 0 : $_POST['billnumb'];
            $subskode3 = substr($billNumber,0,3);
            $subskode4 = substr($billNumber,0,4);
            if(strlen($billNumber) <13){
                $substlp='';
                $subkode = '';
                if(!empty($arrtelp[$subskode3])){
                    $subkode = $subskode3;
                    $substlp = substr($billNumber,3);
                    if(strlen($substlp)<=8){
                        $substlp=str_pad($substlp, 9, "0", STR_PAD_LEFT);//'0021003400003';
                    }
//                    $billNumber=$subkode.$substlp;
                }elseif(!empty($arrtelp[$subskode4])){
                    $subkode = $subskode4;
                    $substlp = substr($billNumber,4);
                    if(strlen($substlp)<=8){
                        $substlp=str_pad($substlp, 9, "0", STR_PAD_LEFT);//'0021003400003';

                    }
//                    $billNumber=$subkode.$substlp;
                }
            }
//            echo $billNumber;exit;
            $kode_produk = empty($_POST['kode_produk']) ? 'M1003' : $_POST['kode_produk'];
            $isi = array(
                'kode_produk' => $kode_produk,
                'trxid' => uniqid('INQTELKOM'),
                'trx_type' => 'INQ',
//                'amount' => $amount,
                'billNumber' => $billNumber//'699992001'
            );
            $res = $this->_koneksicore($isi);
            $data = $this->_data;
            $data['billnumb'] = $billNumber;
            $data['kode_produk'] = $kode_produk;
            $data['produk_amount'] = $kode_produk;
            $data['nominal'] = $amount;
            $data['result'] = json_decode($res, true);
            return $this->parser->parse('jastelinq', $data);
            print_r($isi);
            exit;
        }
    }
    function inqtelkomjastelhallo()
    {
        if ($_POST) {
            $amount = empty($_POST['amount']) ? 0 : $_POST['amount'];
            $billNumber = empty($_POST['billnumb']) ? 0 : $_POST['billnumb'];
            $kode_produk = empty($_POST['kode_produk']) ? 'M1003' : $_POST['kode_produk'];
            $isi = array(
                'kode_produk' => $kode_produk,
                'trxid' => uniqid('INQHALLO'),
                'trx_type' => 'INQ',
//                'amount' => $amount,
                'billNumber' => $billNumber//'699992001'
            );
            $res = $this->_koneksicore($isi);
            $data = $this->_data;
            $data['billnumb'] = $billNumber;
            $data['kode_produk'] = $kode_produk;
            $data['produk_amount'] = $kode_produk;
            $data['nominal'] = $amount;
            $data['result'] = json_decode($res, true);
            return $this->parser->parse('jastelhalloinq', $data);
            print_r($isi);
            exit;
        }
    }
    function inqtelkomjastelhallomta()
    {
        if ($_POST) {
            $amount = empty($_POST['amount']) ? 0 : $_POST['amount'];
            $billNumber = empty($_POST['billnumb']) ? 0 : $_POST['billnumb'];
            $kode_produk = empty($_POST['kode_produk']) ? 'M1003' : $_POST['kode_produk'];
            $isi = array(
                'kode_produk' => $kode_produk,
                'trxid' => uniqid('INQHALLO'),
                'trx_type' => 'INQ',
//                'amount' => $amount,
                'billNumber' => $billNumber//'699992001'
            );
            $res = $this->_koneksicore($isi);
            $data = $this->_data;
            $data['billnumb'] = $billNumber;
            $data['kode_produk'] = $kode_produk;
            $data['produk_amount'] = $kode_produk;
            $data['nominal'] = $amount;
            $data['result'] = json_decode($res, true);
//            echo "<pre>";
//            print_r($data['result']);exit;
            return $this->parser->parse('jastelhalloinqmta', $data);
            print_r($isi);
            exit;
        }
    }

    function pembayaran()
    {
        if ($_POST) {
            $datadonat = array(
                'prevnomDonation' => str_replace(".", '', $this->input->post('donationNominal')),
                'prevjenisDonation' => $this->input->post('jenisDonation'),
                'prevcomDonation' => empty($this->input->post('donationCommment')) ? '' : $this->input->post('donationCommment'),
            );
            $titletrx = empty($this->input->post('titletrx')) ? 'Pembayaran' : $this->input->post('titletrx');
            $this->session->set_userdata($datadonat);
            $jenis = $this->session->userdata('prevjenisDonation');
            $nominal = $this->session->userdata('prevnomDonation');
            $comment = $this->session->userdata('prevcomDonation');
            $donationName = $this->input->post('donationName');
            $donationEmail = $this->input->post('donationEmail');
            $donationPhone = empty($this->input->post('donationPhone')) ? '081234567899' : $this->input->post('donationPhone');
            $methodpay = $this->input->post('donationPay');
            $billnumb = $this->input->post('billnumb');
            $amountbill = empty($this->input->post('amount'))?'':$this->input->post('amount');
            $kode_produk = $this->input->post('kode_produk');
            $billreff = empty($this->input->post('billreff'))?$billnumb:$this->input->post('billreff');
            $error = "";
            if (empty($jenis)) {
                $error .= '- Kesalahan sistem, hubungin administrator. <br>';
            }
            if ($nominal < 9999 || !is_numeric($nominal)) {
                $error .= '- Nominal dana harus minimal Rp.10.000 <br>';
            }
            if (empty($methodpay)) {
                $error .= '- Metode pembayaran harus dipilih. <br>';
            }
            if (empty($donationEmail) || empty($donationPhone) || strlen($donationPhone) < 9) {
                $error .= '- Email / Telp harus diisi (minimal 10 karakter) <br>';
            }
            if (!empty($error)) {
                $dataerror = array('error' => $error);
                $this->session->set_flashdata($dataerror);
                print_r($dataerror);exit;
                redirect('ppob/multipayment');
                exit;
            }
            $notrx = date('Ymd') . sprintf('%05d', $this->geninc());
            $uniqcode = 0;
            $iddonatur = empty($this->_SESSION['userid']) ? '0' : $this->_SESSION['userid'];
            $insertData = array(
                'no_transaksi' => $notrx,
                'nominal' => $nominal,
                'trx_date' => date('Y-m-d'),
                'jenistrx' => $jenis,
                'donatur' => $iddonatur,/*ubah donaturnya jika login maka akan mengambil session loginnya*/
                'bank' => $methodpay,
                'comment' => $comment,
                'uniqcode' => $uniqcode,//($methodpay == 919 ? 0 : $uniqcode),
                'namaDonatur' => $donationName,
                'emailDonatur' => $donationEmail,
                'telpDonatur' => $donationPhone,
                'nomuniq' => $nominal + $uniqcode,//($methodpay == 919 ? 0 : $uniqcode),
                'validDate' => date('Y-m-d', strtotime('+2 days')),
                'crtd_at' => date("Y-m-d H:i:s"),
                'edtd_at' => date("Y-m-d H:i:s"),
                'month' => date("m"),
                'year' => date("Y"),
            );
            $this->infaqmdl->custom_insert("nb_trans_program", $insertData);
            $insertPpob = array(
                'id'=>Fhhlib::uuidv4(),
                'no_transaksi'=>$notrx,
                'billNumber'=>$billnumb,
                'amount'=>$amountbill,
                'produk'=>$kode_produk,
                'type_trx'=>'INQ',
                'billreff'=>$billreff,
                'created_ad'=>date('Y-m-d H:i:s'),
            );
            $this->infaqmdl->custom_insert("nb_ppob_pay", $insertPpob);
            $array_items = array(
                'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive');
            $this->session->unset_userdata($array_items);
            $datadonat = array(
                'doneDonation' => $notrx,
            );
            $this->session->set_userdata($datadonat);
            $idipg = Fhhlib::uuidv4Nopad();
            $arrParam['idipg'] = $idipg;
            $expmethod = explode('|', $methodpay);

            if ($expmethod[0] == 'FINPAY') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = ($donationPhone);
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                $arrParam['items'] = $titletrx;
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                    }
                }
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = $titletrx;//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes
                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_program',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );
                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $res = Fhhlib::finpayIPG($arrParam);
//            $res = $this->finpayIPG($nominal,$donationEmail,$iddonatur,$donationPhone,$donationName,$notrx);
                if (!empty($res)) {
                    if ($res['status_code'] == '00') {
                        if (!empty($res['landing_page'])) {
                            redirect($res['landing_page'], 'refresh');
                        } elseif (!empty($res['redirect_url'])) {
                            redirect($res['redirect_url'], 'refresh');
                        } else {
                            $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                            $this->session->set_flashdata($dataerror);
                            redirect('ppob/multipayment');
                        }
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('ppob/multipayment');
                        exit;
                    }
                }
//                header("Location: ".$res);
                print_r($res);
                exit;
//            print_r($res);exit;
            }
            elseif ($expmethod[0] == 'MUPAYS') {

                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = ($donationPhone);
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                $arrParam['token_login'] = !empty($this->_SESSION['mupays_token']) ? $this->_SESSION['mupays_token'] : "";
                $arrParam['items'] = $titletrx;
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                    }
                }
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = $titletrx;//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes
                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_program',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );
                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $res = Fhhlib::devfinpayIPG($arrParam);
//            $res = $this->finpayIPG($nominal,$donationEmail,$iddonatur,$donationPhone,$donationName,$notrx);
                if (!empty($res)) {
                    if ($res['rc'] == '0000') {
                        if (!empty($res['data']['no_va'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['hasil'] = $res['data'];
                            return $this->parser->parse("view_vatrx", $datas);
                        }
                        if (!empty($res['data']['landing_page'])) {
                            return redirect($res['data']['landing_page'], 'refresh');
//                            $datas = $this->_data;
//                            $datas['hasil'] = $res['data'];
//                            return $this->parser->parse("view_vatrx", $datas);
                        }
                        if (!empty($res['data']['qris'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['url_img'] = $res['data']['qris']['url_img'];
                            $datas['nominal'] = $nominal;
                            return $this->parser->parse("view_qris", $datas);
                        }
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('ppob/multipayment');
                        exit;
                    }
                }
//                header("Location: ".$res);
                print_r($res);
                exit;
//            print_r($res);exit;
            } elseif ($expmethod[0] == 'E2PAY') {
                $cariipg = $this->infaqmdl->custom_query('nb_partner_ipg', ['where' => ['id' => $this->db->escape('E2PAY')]])->result();
                $key = !empty($cariipg[0]->key) ? $cariipg[0]->key : "v1XdZSLMs9";
                $merchant_id = !empty($cariipg[0]->merchant_key) ? $cariipg[0]->merchant_key : "IF00294";
                $merchantkey = $key;//'v1XdZSLMs9';
                $merchantcode = $merchant_id;//'IF00294';
                $arrParam['url_host'] = $cariipg[0]->url_host;
                $arrParam['MerchantCode'] = $merchantcode;
                $arrParam['PaymentId'] = $expmethod[1];
                $arrParam['RefNo'] = $notrx;
                $arrParam['Amount'] = $nominal . "00";
                $arrParam['Currency'] = 'IDR';
                $arrParam['ProdDesc'] = $titletrx;
                $arrParam['UserName'] = $donationName;
                $arrParam['UserEmail'] = $donationEmail;
                $arrParam['UserContact'] = $donationPhone;
                $signatures = Fhhlib::E2Pay_signature($merchantkey . $merchantcode . $arrParam['RefNo'] . $arrParam['Amount'] . $arrParam['Currency']);
                $arrParam['Signature'] = $signatures;
                $arrParam['Remark'] = '';
                $arrParam['ResponseURL'] = base_url('trxipg/responsee2pay');
                $arrParam['BackendURL'] = base_url('trxipg/backende2pay');
                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_program',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );
//                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $this->infaqmdl->custom_insert("nb_trans_e2pay", $insipgtrx);
                $datas = $this->_data;
                $datas['parame2pay'] = $arrParam;
////            print_r($datas);
                return $this->parser->parse("view_e2pay", $datas);
                $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . $res);
                $this->session->set_flashdata($dataerror);
                redirect('ppob/multipayment');

            } else {
                $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . $res);
                $this->session->set_flashdata($dataerror);
                redirect('ppob/multipayment');
            }
            exit;
        }
    }

    function _koneksicore($data = [])
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://core.mumuapps.id/ppob/b2b/transaksi",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    private function geninc()
    {

        $kondisi['where'] = array('id' => $this->db->escape(1));
        $generate = $this->infaqmdl->custom_query("nb_inc", $kondisi)->result();
        $tgl = $generate[0]->tgl;
        $uniqinc = 0;
        if ($tgl != date("Y-m-d")) {
            $uniqinc = $uniqinc + 1;
        } else {
            $uniqinc = $generate[0]->inc + 1;
        }
        $this->infaqmdl->custom_update("nb_inc", array('inc' => $uniqinc, 'tgl' => date("Y-m-d")), array('id' => $this->db->escape(1)));
        return $uniqinc;

    }
    function pulsa(){
        $data = $this->_data;
        return $this->parser->parse('view_pulsa', $data);
    }
}

