{_layHeader}

<div class="container-fluid grey" style="padding-top: 100px;">

<div class="row">
	<div class="col-md-3">
		&nbsp;
	</div>
	<div class="panel panel-default panel-login-nb col-md-12" style="padding:0px;">

        <div class="panel-heading panel-login-head bg-blue"><b>Terima kasih!</b></div>

		<div class="panel-body">
				
			<?php
				if($type == "registrasi")
				{
			?>

			<div>
				<p class="alert alert-info" style="text-align:center">
					Terima kasih telah mendaftar di komunitas berbagi MumuApps, semoga menjadi manfaat yang luar biasa bagi semua orang, silahkan aktifasi melalui email yang telah terdaftar.
				</p>
				<p>
					kami telah mengirimkan link aktifasi ke email anda.  
				</p>
			</div>

			<?php
				}
			?>

			<?php
				if($type == "aktivasi")
				{
			?>	

			<div>
				<p class="alert alert-info" style="text-align:center">
					Akun anda telah aktif, anda dapat menggunakan seluruh fitur yang tersedia di website ini.
				</p>
				<p>
					Silakan <a href="<?php URI::baseURL(); ?>auten/page/login">Login</a> untuk mengakses seluruh fitur yang tersedia.
				</p>
			</div>

			<?php
				}
			?>
		</div>

	</div>
	<div class="col-md-3">
		&nbsp;
	</div>
</div>

</div>

<script type="text/javascript">

</script>

{_layFooter}