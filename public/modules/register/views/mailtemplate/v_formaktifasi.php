{_layHeader}

<div class="container" style="padding-top: 100px;">

<div class="row">

	<div class="col-md-4">
		&nbsp;
	</div>

	<div class="panel panel-default panel-login-nb col-md-12" style="padding-right:0px; padding-left:0px">

        <div class="panel-heading panel-login-head bg-blue text-center"><h3>Aktivasi Akun Anda</h3></div>

		<div class="panel-body">

			<?php

			    $hidden_form = array('id' => !empty($donatur) ? $donatur : '');

			    echo form_open_multipart(base_url('register/doaktifasi?token='.$token), array('id' => 'fmartikel'), $hidden_form);

		    ?>

			<div class="form-group col-xs-12"><label>Password</label>
					<input type="password" class="form-control" name="pass" placeholder="ketik disini" required>
			</div>

			<div class="form-group col-xs-12"><label>Re-Type Password</label>
					<input type="password" class="form-control" name="repass" placeholder="ketik disini" required>
			</div>	

			<div class="form-group col-xs-12">

			<input type="submit" class="form-control btn btn-success" name="" value="Simpan">

			</div>	

			<?php echo form_close();?>

		</div>

	</div>

	<div class="col-md-4">
		&nbsp;
	</div>

</div>

</div>

<script type="text/javascript">

</script>

{_layFooter}