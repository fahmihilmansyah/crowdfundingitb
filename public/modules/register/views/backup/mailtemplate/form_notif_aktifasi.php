{_layHeader}

<div class="container-fluid grey">

<div class="row">
	<div class="col-md-3">
		&nbsp;
	</div>
	<div class="panel panel-default panel-login-nb col-md-6" style="padding:0px;">

		<div class="panel-heading panel-login-head bg-blue">Terima kasih!</div>

		<div class="panel-body">
				
			<?php
				if($type == "registrasi")
				{
			?>

			<div>
				<p class="alert alert-info" style="text-align:center">
					Terima kasih telah mendaftar di komunitas berbagi GaneshaBisa, semoga menjadi manfaat yang luar biasa bagi semua orang, silahkan aktifasi melalui email yang telah terdaftar.
				</p>
				<p>
					kami telah mengirimkan link aktifasi ke email anda.  
				</p>
			</div>

			<?php
				}
			?>

			<?php
				if($type == "aktivasi")
				{
			?>	

			<div>
				<p class="alert alert-info" style="text-align:center">
					Akun anda telah aktif, anda dapat menggunakan seluruh fitur yang tersedia di website ini.
				</p>
				<p>
					Silahkan <a href="<?php URI::baseURL(); ?>auten/page/login">Login</a> untuk mengakses seluruh fitur yang tersedia. 
				</p>
			</div>

			<?php
				}
			?>
		</div>

	</div>
	<div class="col-md-3">
		&nbsp;
	</div>
</div>

</div>

<script type="text/javascript">

</script>

{_layFooter}