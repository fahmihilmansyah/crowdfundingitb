{_layHeader}
<div class="content">
    <div class="mb-10">&nbsp;</div>
<div class="container full-height valign-wrapper">
    <div class="valign-box">
        <div class="content">
            <div class="text-center">
                <img src="<?php URI::baseURL(); ?>assets/mganeshabisa/img/img-reg.svg" class="img-reg mb-20" alt="Image">
            </div>
            <div class="text-center mb-30">
                <div class="h3 colo-second title-font bold lh-1">MumuApps</div>
                <div class="color-second">
                    <span class="text-muted">Silakan masukkan email anda ketika mendaftar.</span>
                </div>
            </div>
            <?php
            $hidden_form = array('id' => !empty($id) ? $id : '');

            echo form_open_multipart(base_url('register/dolupapass'), array('class'=>'reg-box'), $hidden_form);

            ?>
            <?php
            if($this->session->userdata("error_msg")){
                ?>

                <p class="alert alert-warning" style="margin:0px; margin-top:10px; text-align: center">
                    <?php echo $this->session->userdata("error_msg"); ?>
                </p>

                <?php
            }
            ?>
                <div class="form-group relative-box">
                    <i class="icofont icofont-email"></i>
                    <input name="email" type="email" required class="form-control title-font medium" placeholder="Email">
                </div>
                <div class="text-center">
                    <button type="submit"  class="btn btn-main btn-reg">Lupa Password</button>
                </div>

            <?php echo form_close();?>
            <div class="mt-30 text-center colo-second">
                Sudah punya akun? <a href="<?php URI::baseURL(); ?>login" class="color-main">Login</a>.
            </div>
        </div>
    </div>
</div>

{_layFooter}