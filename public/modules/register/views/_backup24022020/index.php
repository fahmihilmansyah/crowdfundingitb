{_layHeader}

<div class="valign-box">
    <div class="container">
        <div class="card no-margin">
            <div class="card-body pt-30 pr-30 pb-30 pl-30">
                <div class="row">
                    <div class="col-md-8 valign-wrapper">
                        <div class="valign-box text-center">
                            <img src="<?php URI::baseURL(); ?>assets/img/img-reg.svg" class="img-reg" alt="Image">
                        </div>
                    </div>
                    <div class="col-md-4 valign-wrapper">
                        <?php
                        $hidden_form = array('id' => !empty($id) ? $id : '');

                        echo form_open_multipart(base_url('register/doregister'), array(), $hidden_form);

                        ?>
                        <?php
                        if($this->session->userdata("error_msg")){
                            ?>

                            <p class="alert alert-warning" style="margin:0px; margin-top:10px; text-align: center">
                                <?php echo $this->session->userdata("error_msg"); ?>
                            </p>

                            <?php
                        }
                        ?>
                        <div class="valign-box">
                            <div class="text-center mb-30">
                                <div class="h3 colo-second title-font bold lh-1">Ganesha Bisa</div>
                                <div class="color-second">
                                    <span class="text-muted">Silakan isi data Anda untuk mendaftar.</span>
                                </div>
                            </div>
                            <form class="reg-box">
                                <div class="form-group relative-box">
                                    <i class="icofont icofont-user-alt-4"></i> Nama Lengkap
                                    <input name="fname" type="text" class="form-control title-font medium" placeholder="Nama Lengkap">
                                </div>
                                <div class="form-group relative-box">
                                    <i class="icofont icofont-email"></i> Email
                                    <input name="email" type="email" class="form-control title-font medium" placeholder="Email">
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-main btn-reg">Daftar</button>
                                </div>
                            </form>
                            <div class="mt-30 text-center colo-second">
                                Sudah punya akun? <a href="<?php URI::baseURL(); ?>login" class="color-main">Login</a>.
                            </div>
                        </div>

                        <?php echo form_close();?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>

{_layFooter}