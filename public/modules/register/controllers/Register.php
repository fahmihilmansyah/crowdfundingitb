<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Register extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : No Reply
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_modList = array('Register_model' => 'regmod');


    protected $_data = array();


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();


        $this->load->library('myphpmailer');

    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);

    }


    public function index($data = array())

    {

        $data = $this->_data;

        $this->parser->parse("index", $data);

    }

    function lupapass(){
        $data = $this->_data;
        $this->parser->parse("lupapass",$data);
    }
    function dolupapass()
    {
        if ($_POST) {
            $email = trim($this->input->post('email'));
            $where = array("email" => $email);
            $caridonat = $cek = $this->regmod->selectData('nb_donaturs', $where)->result();
           if (!empty($caridonat)):
               $caridonatname = $cek = $this->regmod->selectData('nb_donaturs_d', ['donatur'=>$caridonat[0]->id])->result();
               $fname = $caridonatname[0]->fname;
               $token = md5(uniqid());

               $data = array(

                   'email' => $email,

                   'token_aktifasi' => $token,

                   'crtd_at' => date("Y-m-d H:i:s"),

                   'edtd_at' => date("Y-m-d H:i:s"),

               );

               $insertid = $this->regmod->update_data("nb_donaturs", $data, $where);
               $data["emailTo"] = $email;
               $data["senderName"] = "MumuApps";
               $data["subject"] = "Aktivasi Email";
               $data["type"] = "view";
               $data["fname"] = $fname;
               $data["email"] = $email;
               $data["token"] = $token;
               $data["konten"] = "register/mailtemplate/aktifasi";
               sendmail::SendMail($data);
               redirect('register/notif?type=registrasi');
           endif;
        }
        die('error');
        exit;
    }

    function doregister()
    {

        if ($_POST) {

            $this->load->library('nbauth');

            $this->nbauth = new nbauth();

            $email = trim($this->input->post('email'));

            $fname = $this->input->post('fname');

            $where = array("email" => $email);

            $jum_stat = $this->regmod->check_if_exists("nb_donaturs", $where);

            if (!$jum_stat) {
                $token = md5(uniqid());

                $data = array(

                    'email' => $email,

                    'token_aktifasi' => $token,

                    'activated' => 'not-confirm',

                    'crtd_at' => date("Y-m-d H:i:s"),

                    'edtd_at' => date("Y-m-d H:i:s"),

                );

                $insertid = $this->regmod->insert_data("nb_donaturs", $data);

                $data_d = array(

                    'fname' => $fname,

                    'donatur' => $insertid,

                );

                $this->regmod->insert_data("nb_donaturs_d", $data_d);

                $datamail = array(

                    "fname" => $fname,

                    "email" => $email,

                    "token" => $token

                );

                $datasaldo = array(

                    "donatur" => $insertid,

                    "prevBalance" => 0,

                    "balanceAmount" => 0,

                    'crtd_at' => date('Y-m-d H:i:s'),

                    'edtd_at' => date('Y-m-d H:i:s')

                );

                $this->db->insert('donaturs_saldo', $datasaldo);

                $config["username"] = MAIL_UNAME;
                $config["password"] = MAIL_PASS;
                $config["host"] = MAIL_HOST;
                $config["smtpAuth"] = MAIL_SMTP;
                $config["smtpType"] = MAIL_SMTP_TYPE;
                $config["port"] = MAIL_PORT;

                sendmail::ConfigUser($config);


                $data["emailTo"] = $email;
                $data["senderName"] = "MumuApps";
                $data["subject"] = "Aktivasi Email";
                $data["type"] = "view";
                $data["fname"] = $fname;
                $data["email"] = $email;
                $data["token"] = $token;
                $data["konten"] = "register/mailtemplate/aktifasi";

                // Fungsi kirim Email Mail PHP
                // To send HTML mail, the Content-type header must be set
                //$headers = "MIME-Version: 1.0" . "\r\n";
                //$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                // More headers
                //$headers .= 'From: <noreply@berzakat.id>' . "\r\n";
                // Mail it
                //mail($email, "Aktifasi Email", "Dear ".$fname." berikut detil anda : Token : ".$token." <br> Alamat URL Aktifasi : ".base_url('register/mailtemplate/aktifasi'), $headers);

                sendmail::SendMail($data);
                // $this->sendMail($datamail, "aktifasi");

                redirect('register/notif?type=registrasi');
            } else {
                $this->session->set_flashdata('error_msg', 'Email yang anda gunakan sudah terdaftar');

                redirect('register');
            }
        }
    }

    function notif_aktifasi()
    {
        $type = $this->input->get("type");

        if ($type == "registrasi" || $type == "aktivasi") {
            $data = $this->_data;
            $data["type"] = $type;

            $this->parser->parse("mailtemplate/form_notif_aktifasi", $data);
        } else {
            die("error");
        }
    }

    function aktifasi()
    {

        $token = $this->input->get('token');
        $donatur = $this->input->get('donatur');

        if ($token == null || empty($token)) {
            die("error");
        }

        $cek = $this->regmod->selectData('nb_donaturs', array('token_aktifasi' => $token))->result();

        if (count($cek) > 0) {

            $data = $this->_data;

            $data['token'] = $token;

            $data['donatur'] = $donatur;


            $this->parser->parse("mailtemplate/v_formaktifasi", $data);

        } else {

            die("error");

        }

    }


    function doaktifasi()
    {

        if ($_POST) {

            $token = $this->input->get('token');

            $pass = $this->input->post('pass');

            $repass = $this->input->post('repass');

            $this->nbauth = new nbauth();

            if (empty($pass) || ($pass) == null || empty($repass) || ($repass) == null) {

                die("cannot be null");

            }

            if ($pass != $repass) {
                die("Not Match");
            } else {

                $data = array(

                    'pwd' => $this->nbauth->getCustomHash($pass),

                    'activated' => "active",

                    'is_activate' => "true",

                    'edtd_at' => date("Y-m-d H:i:s"),

                );

                $where = array('token_aktifasi' => $token);

                $result = $this->regmod->update_data("nb_donaturs", $data, $where);

                if ($result) {

                    $data = array(
                        'token_aktifasi' => ''
                    );

                    $this->regmod->update_data("nb_donaturs", $data, $where);
                } else {
                    die('error');
                }


                redirect('register/notif?type=aktivasi');
            }
        } else {
            die('error');
        }

    }

    public function logout()
    {
        $this->session->unset_userdata('token');
        $this->session->unset_userdata('userData');
        $this->session->sess_destroy();
        redirect('/');
    }

}

