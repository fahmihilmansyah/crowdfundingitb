<?php

/**

 * Created by PhpStorm.

 * User: Fahmi hilmansyah

 * Date: 3/23/2017

 * Time: 10:41 AM

 */

class Google extends NBFront_Controller{

    private $_modList   = array("register/Googles_model"=>"googlemdl");



    protected $_data    = array();



    function __construct()

    {

        parent::__construct();

        $this->modelsApp();

    }

    private function modelsApp()

    {

        $this->load->model($this->_modList);

    }



    function index(){

        // Include the google api php libraries
        include_once APPPATH."libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH."libraries/google-api-php-client/contrib/Google_Oauth2Service.php";

        // Google Project API Credentials
        $clientId     = GOOGLE_API_ID;
        $clientSecret = GOOGLE_API_KEY;
        $redirectUrl  = base_url() . 'register/google/';


        // Google Client Configuration
        $gClient = new Google_Client();

        $gClient->setApplicationName('Login to Niatbaik');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);

        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if (isset($_REQUEST['code'])) {
            $gClient->authenticate();
            $this->session->set_userdata('token', $gClient->getAccessToken());
            redirect($redirectUrl);
        }

        $token = $this->session->userdata('token');

        if (!empty($token)) {
            $gClient->setAccessToken($token);
        }


        if ($gClient->getAccessToken()) {

            $profile = $google_oauthV2->userinfo->get();
            
            // Preparing data for database insertion
            // $userData['oauth_provider'] = 'google';
            // $userData['oauth_uid']      = $userProfile['id'];
            // $userData['first_name']     = $userProfile['given_name'];
            // $userData['last_name']      = $userProfile['family_name'];
            // $userData['email']          = $userProfile['email'];

            // $data['userData']           = $userData;

            // $this->session->set_userdata('userData',$userData);

            // $kondisi['where']           = array('g_uid'=>$this->db->escape($userProfile['id']));

            // $ceks = $this->googlemdl->custom_query('nb_donaturs_d',$kondisi)->result();

            // if(count($ceks) < 1) {

                // Insert or update user data
          //       $datau      = array(
          //                           'email'         => $userProfile['email'], 
          //                           'is_activate'   => 'true', 
          //                           'activated'     => 'active', 
          //                           'crtd_at'       => date('Y-m-d H:i:s'), 
          //                           'edtd_at'       => date('Y-m-d H:i:s')
          //                           );

          //       $this->googlemdl->custom_insert('nb_donaturs', $datau);

          //       $iddonatur = $this->googlemdl->getInsertId();

          //       $datad     = array(
          //                           'fname'     => $userProfile['name'], 
          //                           'email'     => $userProfile['email'], 
          //                           'donatur'   => $iddonatur, 
          //                           'g_uid'     => $userProfile['id']);

          //       $this->googlemdl->custom_insert('nb_donaturs_d', $datad);

		        // $datasaldo = array(
          //                           "donatur"       => $iddonatur, 
          //                           "prevBalance"   => 0,
          //                           "balanceAmount" => 0, 
          //                           'crtd_at'       => date('Y-m-d H:i:s'), 
          //                           'edtd_at'       => date('Y-m-d H:i:s')
          //                           );

		        // $this->googlemdl->custom_insert('nb_donaturs_saldo', $datasaldo );
                
          //       $datalogin['userid']        = $iddonatur;
          //       $datalogin['fname']         = $userProfile['name'];
          //       $datalogin['email']         = $userProfile['email'];
          //       $datalogin['statLogin']     = true;

          //       $this->session->set_userdata($datalogin);

          //   }else{
          //       $datalogin['userid']        = $ceks[0]->donatur;
          //       $datalogin['fname']         = $ceks[0]->fname;
          //       $datalogin['email']         = $ceks[0]->email;
          //       $datalogin['statLogin']     = true;

          //       $this->session->set_userdata($datalogin);

          //   }
        
          //   redirect('/');

            // SET TOKEN
            $token      = md5($profile["email"] . $profile["id"] . md5(strtotime(date("ymdhis"))) . md5(time()));
            $validdate  = date("Y-m-d H:i:s", strtotime("+1 hour"));

            $data["email"]          = $profile["email"];
            $data["oauth_uid"]      = $profile["id"];
            $data["oauth_provider"] = "google";
            $data["login_type"]     = "oauth";

            $existsOauthUser = $this->auth->existsOauthUser($data);

            if($existsOauthUser > 0)
            {   
                $donatur["oauth_provider"]  = "google";
                $donatur["oauth_uid"]       = $profile["id"];
                $donatur["email"]           = $profile["email"];
                $donatur["picture_url"]           = $profile["picture"];
                $donatur["last_login_ip"]   = $_SERVER['REMOTE_ADDR'];
                $donatur["is_activate"]     = "true";
                $donatur["activated"]       = "active";
                $donatur["login_type"]      = "oauth";

                $key = array(
                                "email"          => $profile["email"],
                                "oauth_uid"      => $profile["id"],
                                "oauth_provider" => "google",
                                "login_type"     => "oauth", 
                                "is_activate"    => "true", 
                                "activated"      => "active"
                            );

                $result = $this->db->update("donaturs", $donatur, $key);

                $id = $this->auth->getUserOauthID($key)->row();
                $id = !empty($id->id) ? $id->id : "";

                // DETAIL DATA
                $detail["fname"]    = $profile["given_name"];
                $detail["lname"]    = $profile["family_name"];
                // $detail["sex"]      = $profile["gender"];
                $detail["donatur"]  = $id;

                $key = array("donatur" => $id);

                $this->db->update("donaturs_d", $detail, $key);

                // TOKEN
                $dtoken["code"]         = $token;
                $dtoken["valid_until"]  = $validdate;
                $dtoken["donatur"]      = $id;
                $dtoken["donatur_ip"]   = $_SERVER['REMOTE_ADDR'];

                $this->db->insert("donatur_tokens", $dtoken);
            }
            else
            {
                // OAUTH USER
                $donatur["email"]           = $profile["email"];
                $donatur["last_login_ip"]   = $_SERVER['REMOTE_ADDR'];
                $donatur["is_activate"]     = "true";
                $donatur["activated"]       = "active";
                $donatur["login_type"]      = "oauth";
                $donatur["picture_url"]      = $profile['picture'];
                $donatur["oauth_provider"]  = "google";
                $donatur["oauth_uid"]       = $profile["id"];

                $result = $this->db->insert("donaturs", $donatur);
                
                if($result)
                {
                    $id = $this->db->insert_id();

                    // DETAIL DATA
                    $detail["fname"]    = $profile["given_name"];
                    $detail["lname"]    = $profile["family_name"];
                    $detail["sex"]      = $profile["gender"];
                    $detail["donatur"]  = $id;

                    $this->db->insert("donaturs_d", $detail);

                    // TOKEN
                    $dtoken["code"]         = $token;
                    $dtoken["valid_until"]  = $validdate;
                    $dtoken["donatur"]      = $id;
                    $dtoken["donatur_ip"]   = $_SERVER['REMOTE_ADDR'];

                    $this->db->insert("donatur_tokens", $dtoken);

                    $datasaldo = array(
                        "donatur"=>$id, 
                        "prevBalance"=>0,
                        "balanceAmount"=>0, 
                        'crtd_at' => date('Y-m-d H:i:s'), 
                        'edtd_at' => date('Y-m-d H:i:s')
                    );

                    $this->db->insert('donaturs_saldo', $datasaldo);
                }
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }

            // SET SESSION USER google
            $key = array(
                                "email"          => $profile["email"], 
                                "oauth_uid"      => $profile["id"], 
                                "oauth_provider" => "google",
                                "login_type"     => "oauth", 
                                "is_activate"    => "true", 
                                "activated"      => "active"
                            );

            $user_data = $this->auth->getUserOauth($key);
            $user_data = !empty($user_data) ? $user_data : (object) array();
            
            $sess_data[LGI_KEY . "login_info"] = array(
                                            "userid"            => $user_data->id,
                                            "fname"             => $profile['name'],
                                            "email"             => $user_data->email,
                                            "statLogin"         => true,
                                            "login_uid"         => $user_data->id,
                                            "login_ouid"        => $user_data->oauth_uid,
                                            "login_name"       => $profile['name'],
                                            "login_fname"       => $user_data->fname,
                                            "login_lname"       => $user_data->lname,
                                            "login_email"       => $user_data->email,
                                            "login_laston"      => $user_data->last_online,
                                            "login_ip"          => $user_data->last_login_ip,
                                            "login_date"        => $user_data->last_login_date,
                                            "login_type"        => "oauth",
                                            "login_oprovider"   => "google",
                                            "login_sex"         => $user_data->sex,
                                            "login_status"      => true,
                                            "login_token"       => $token
                                         );

            $this->session->set_userdata($sess_data);

            // Now you can redirect to another page and use the access token from $_SESSION['google_access_token']
            $preflink = $this->session->userdata('urllink');
            redirect($preflink);
        } else {
            $urllogin = $gClient->createAuthUrl();
            header('Location: ' . filter_var($urllogin, FILTER_SANITIZE_URL));
        }
    }



}