<?php

/**

 * Created by PhpStorm.

 * User: Fahmi hilmansyah

 * Date: 3/23/2017

 * Time: 10:41 AM

 */

class Google extends NBFront_Controller{

    private $_modList   = array("register/Googles_model"=>"googlemdl");



    protected $_data    = array();



    function __construct()

    {

        parent::__construct();

        $this->modelsApp();

    }

    private function modelsApp()

    {

        $this->load->model($this->_modList);

    }



    function index(){

        // Include the google api php libraries

        include_once APPPATH."libraries/google-api-php-client/Google_Client.php";

        include_once APPPATH."libraries/google-api-php-client/contrib/Google_Oauth2Service.php";

        // Google Project API Credentials

        $clientId = '634548202035-11pg8nnrvinark4c0nng76htd5a15tgt.apps.googleusercontent.com';

        $clientSecret = 'DBalJCd_hIuvA8_nD0Wd-aEY';

        $redirectUrl = base_url() . 'register/google/';



        // Google Client Configuration

        $gClient = new Google_Client();

        $gClient->setApplicationName('Login to Niatbaik');

        $gClient->setClientId($clientId);

        $gClient->setClientSecret($clientSecret);

        $gClient->setRedirectUri($redirectUrl);

        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if (isset($_REQUEST['code'])) {

            $gClient->authenticate();

            $this->session->set_userdata('token', $gClient->getAccessToken());

            redirect($redirectUrl);

        }



        $token = $this->session->userdata('token');

        if (!empty($token)) {

            $gClient->setAccessToken($token);

        }



        if ($gClient->getAccessToken()) {

            $userProfile = $google_oauthV2->userinfo->get();

            // Preparing data for database insertion

            $userData['oauth_provider'] = 'google';

            $userData['oauth_uid'] = $userProfile['id'];

            $userData['first_name'] = $userProfile['given_name'];

            $userData['last_name'] = $userProfile['family_name'];

            $userData['email'] = $userProfile['email'];



            $data['userData'] = $userData;

            $this->session->set_userdata('userData',$userData);

            $kondisi['where']=array('g_uid'=>$this->db->escape($userProfile['id']));

            $ceks = $this->googlemdl->custom_query('nb_donaturs_d',$kondisi)->result();

            if(count($ceks) < 1) {

                // Insert or update user data

                $datau = array('email' => $userProfile['email'], 'is_activate' => 'true', 'activated' => 'active', 'crtd_at' => date('Y-m-d H:i:s'), 'edtd_at' => date('Y-m-d H:i:s'));

                $this->googlemdl->custom_insert('nb_donaturs', $datau);

                $iddonatur = $this->googlemdl->getInsertId();

                $datad = array('fname' => $userProfile['name'], 'email' => $userProfile['email'], 'donatur' => $iddonatur, 'g_uid' => $userProfile['id']);

                $this->googlemdl->custom_insert('nb_donaturs_d', $datad);
		$datasaldo = array("donatur"=>$iddonatur, "prevBalance"=>0,"balanceAmount"=>0, 'crtd_at' => date('Y-m-d H:i:s'), 'edtd_at' => date('Y-m-d H:i:s'));
		$this->googlemdl->custom_insert('nb_donaturs_saldo', $datasaldo );
                $datalogin['userid']=$iddonatur;

                $datalogin['fname']=$userProfile['name'];

                $datalogin['email']=$userProfile['email'];

                $datalogin['statLogin']=true;

                $this->session->set_userdata($datalogin);

            }else{

                $datalogin['userid']=$ceks[0]->donatur;

                $datalogin['fname']=$ceks[0]->fname;

                $datalogin['email']=$ceks[0]->email;

                $datalogin['statLogin']=true;

                $this->session->set_userdata($datalogin);

            }

          

            redirect('/');

        } else {

            $urllogin = $gClient->createAuthUrl();

            header('Location: ' . filter_var($urllogin, FILTER_SANITIZE_URL));

        }

//        redirect('/');

    }



}