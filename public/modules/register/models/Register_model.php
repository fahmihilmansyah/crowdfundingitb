<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Register_model extends CI_Model {



	/**

	 * ----------------------------------------

	 * #Manajemen Akses Model

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 9 Februari 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 9 Februari 2017

	 * ----------------------------------------

	 */





	function __construct()

	{

		parent::__construct();

	}

	

    function insert_data($table, $data){

    	$this->db->trans_begin();

		$this->db->insert($table, $data); 

		$insertid = $this->db->insert_id();

    	if ($this->db->trans_status() === FALSE)

		{

		    $this->db->trans_rollback();

		    die("ERROR: INSERTING DATA, PLEASE CONTACT ADMINISTRATOR WEB");

		}

		else

		{

		    $this->db->trans_commit();

		    return $insertid;

		}

    }

    function update_data($table, $data,$where=array()){

    	$this->db->trans_begin();

    		$this->db->where($where);

			$this->db->update($table, $data); 

    	if ($this->db->trans_status() === FALSE)

		{

		    $this->db->trans_rollback();

		   	return false;

		}

		else

		{

		    $this->db->trans_commit();
		    return true;
		}

    }

    function selectData($table =null,$where=array(),$select="*"){

    	$this->db->select($select);

    	$this->db->from($table);

    	$this->db->where($where);

    	return $this->db->get();

    }

    function check_if_exists($table = "", $condition = array())
    {
    	$this->db->from($table);
    	$this->db->where($condition);

    	$jumlah = $this->db->count_all_results(); 

    	if($jumlah > 0)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
}