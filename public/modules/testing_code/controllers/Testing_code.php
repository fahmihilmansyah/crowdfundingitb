<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Testing_code extends NBFront_Controller {



	/**

	 * ----------------------------------------

	 * #NB Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_modList   = array('testing_code/Testing_code_model' => 'test_model');

	protected $_data    = array();

	private $fb;

	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------

		$this->modelsApp();



		$this->load->config("socialconfig");

		$this->fb = $this->config->item('facebook');

	}

	public function showData()
	{
		echo "showData";
	}

	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	public function index(){

		// Facebook API Configuration
		$appId 			= '1901519173464657';
		$appSecret 		= '530664b374da654562ded56a73517d33';
		$redirectUrl 	= base_url() . 'user_authentication/';
		$fbPermissions  = 'email';

		//Call Facebook API
		$facebook = new \Facebook\Facebook([
		  'app_id' => $appId,
		  'app_secret' => $appSecret,
		  'default_graph_version' => 'v2.8',
		  //'default_access_token' => '{access-token}', // optional
		]);

		$helper 		= $facebook->getRedirectLoginHelper();

		$permissions 	= ['email']; // optional

		try 
		{
			if (isset($_SESSION['facebook_access_token'])) 
			{
				$accessToken = $_SESSION['facebook_access_token'];
			} 
			else 
			{
				$accessToken = $helper->getAccessToken();
			}
		} 
		catch(Facebook\Exceptions\FacebookResponseException $e) 
		{
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} 
		catch(Facebook\Exceptions\FacebookSDKException $e) 
		{
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}

		if(isset($accessToken)) 
		{

			if (isset($_SESSION['facebook_access_token'])) 
			{
				$facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);
			} 
			else 
			{
				// getting short-lived access token
				$_SESSION['facebook_access_token'] = (string) $accessToken;

				// OAuth 2.0 client handler
				$oAuth2Client = $facebook->getOAuth2Client();

				// Exchanges a short-lived access token for a long-lived one
				$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);

				$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;

				// setting default access token to be used in script
				$facebook->setDefaultAccessToken($_SESSION['facebook_access_token']);
			}

			// redirect the user back to the same page if it has "code" GET variable
			if (isset($_GET['code'])) {
				header('Location: ./');
			}

			// getting basic info about user
			try 
			{
				$profile_request = $facebook->get('/me?fields=name,email,first_name,last_name,gender,picture');

				$profile = $profile_request->getGraphNode()->asArray();
			}
			catch(Facebook\Exceptions\FacebookResponseException $e) 
			{
				// When Graph returns an error
				echo 'Graph returned an error: ' . $e->getMessage();

				session_destroy();

				// redirecting user back to app login page
				header("Location: ./");
				exit;
			} 
			catch(Facebook\Exceptions\FacebookSDKException $e) 
			{
				// When validation fails or other local issues
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}

			echo "<PRE>";
			// printing $profile array on the screen which holds the basic info about user
			print_r($profile);

			// Now you can redirect to another page and use the access token from $_SESSION['facebook_access_token']
		} 
		else 
		{
			// replace your website URL same as added in the developers.facebook.com/apps e.g. if you used http instead of https and you used non-www version or www version of your website then you must add the same here
			$loginUrl = $helper->getLoginUrl('http://niatbaik.co.id/ydsf/testing_code', $permissions);

			echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
		}
	}

    

    public function logout() {

        $this->session->unset_userdata('userData');

        $this->session->sess_destroy();

        redirect('/user_authentication');

    }

}

