<div class="col-md-3">
	&nbsp;
</div>
<div class="col-md-6">
	<center>
	    <?php
		    $hidden_form = array('id' => !empty($id) ? $id : '');
		    echo form_open_multipart('', array('id' => 'fmdonasi'), $hidden_form);
	    ?>

		<div>
            <?php print_r($this->session->all_userdata());?>
            <label class="hasilhitung"></label>
            <input type="text" value="Rp. 0" id="donationNominal" readonly class="form-control hasilhitungtxt don-input" placeholder="Nominal Donasi">
            <input type="hidden" name="donationNominal" id="donationNominal" class="form-control hasilhitung" placeholder="Nominal Donasi">
		</div>
		<div>
			<label class="agreement">
				<div>
				<?php
					$data = array(
					        'name'          => 'agreement',
					        'id'            => 'agreement',
					        'value'         => 'accept',
					        'checked'       => FALSE,
					);

					echo form_checkbox($data);
				?>
				Donasi Sebagai Anonim
				</div>
			</label>
		</div>      
		<br clear="all"/>
		<div>    
			<?php 
				$name  = 'donationCommment';
				$value = empty($this->session->userdata('prevcomDonation'))? "" :
							   $this->session->userdata('prevcomDonation');
				$extra = 'id="donationCommment" 
						  class="form-control mini-height" 
						  placeholder="Tuliskan Komentar"';

				echo form_textarea($name, $value, $extra); 
			?>		
		</div>

		<div>
			<input type="hidden" name="otherJenis" id="jenis" value="zakatmall">
            <input type="submit" name="subnominal" class="btn btn-success" style="width:100%; margin-top:10px" value="Lanjut">
		</div>	
	</center>
    <?php
    	echo form_close();
    ?>
</div>
<div class="col-md-3">
	&nbsp;
</div>