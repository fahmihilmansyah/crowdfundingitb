<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_model extends CI_Model {
	public function data_parent()
	{
		$this->db->select('*');
		$this->db->from('faq_parent');
		$this->db->order_by("id","asc");
		
		$query = $this->db->get();
		
		return $query;
	}
	public function data_child(){
		$this->db->order_by("faq_parent.title","asc");
		$this->db->select('faq_parent.*', FALSE);
		$this->db->select('faq_child.description,faq_child.parent,faq_child.key_search,faq_child.title AS title_child', FALSE);
		$this->db->from('faq_parent');
		$this->db->join('faq_child', 'faq_parent.id = faq_child.parent');
		
		
		$query = $this->db->get();
		
		return $query;
	}

	function getkey()
    {
        $query = $this->db->get('faq_child');
        return $query->result();
    }


}