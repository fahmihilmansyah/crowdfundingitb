<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends NBFront_Controller {

	/**
	 * ----------------------------------------
	 * #NB Controller
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_modList   = array('Faq_model' => 'faq',);

	protected $_data    = array();

	function __construct()
	{
		parent::__construct();

		// -----------------------------------
		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN
		// -----------------------------------
		 $this->modelsApp();
	}

	private function modelsApp()
	{		
		$this->load->model($this->_modList);
	}

	public function index($data = array())
	{
		$data 			 	= $this->_data;

		$data["_content"] 	= $this->parser->parse('donatur/overview/index', $data, TRUE);
		
		
		$data["data_parent"] = !empty($this->faq->data_parent()->result()) ? $this->faq->data_parent()->result() : array() ;
		$data["data_child"]  = !empty($this->faq->data_child()->result()) ? $this->faq->data_child()->result() : array() ;
		
		$this->parser->parse("index",$data);
	}
	
	public function search(){
		$query  = $this->faq->getkey();
        $data = array();
        foreach ($query as $key => $value) 
        {
            $data[] = array('id_child' => $value->id, 'key_search' => $value->key_search);
        }
        echo json_encode($data);
	}
}
