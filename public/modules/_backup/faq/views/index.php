{_layHeader}
	<div id="legalitas">
		<div class="container-fluid">
			<div class="row">
				<div id="imglegalitas" class="profil">
					<center>
						<h1 class="bold">APA YANG KAMI BISA BANTU?</h1>
						<br/>
						<p style="width:50%">
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
						</p>
					</center>
				</div>
							
				<div class="faq-search">
					<div class="col-md-12">
						<table width="60%" style="position: relative; margin:0 auto;">
							<tr>
								<td>
									<?php 
										$data = array(
												'type'  => 'hidden',
												'name'  => 'id_child',
												'id'    => 'id_child'
										); 
										echo form_input($data);
									?>
								</td>
								<td width="80%">
									<?php 
										$name  = 'key_search';
										$value = !empty($default["key_search"]) ? $default["key_search"] : '';
										$extra = 'id="key_search" 
												  class="form-control w100" 
												  placeholder="isi disini"
												  autocomplete="off"
												  data-provide="typeahead"';

										echo form_input($name, $value, $extra); 
									?>
								</td>
								<td style="padding-left:10px;">
									<input type="submit" name="submit" class="btn btn-success pull-left" value="CARI">
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div class="faq-breadcrumbs">
					<div class="faq-home nav navbar-left">
						<img src="<?php URI::baseURL(); ?>assets/images/icon/FAQ.png">
						&nbsp;<i class="glyphicon glyphicon-chevron-right"></i>
					</div>
				</div>

				
				
				<div class="konten">
					<div class="col-md-4">
						<ul class="list-group">
						<?php foreach($data_parent as $key => $dataFaq){ 
							if($dataFaq->is_active=="1"){ ?>
							<div class="kategori" id="<?php echo $key ?>">
								<li class="list-group-item">
								<a href="javascript:void(0)">
									<?php echo $dataFaq->title ?>
									<i class="glyphicon glyphicon-chevron-right pull-right"></i>
								</a>
								</li>
							</div>
							<?php } } ?>
						</ul>
					</div>
					<div class="col-md-8">
					<?php foreach($data_parent as $key => $dataFaq){ 
							if($dataFaq->is_active=="1"){ ?>
						<div class="konten-faq" id="kontenshow<?php echo $key?>" style="display:none;"><h3 class="panduan-umum">
							<?php echo $dataFaq->title; ?>
						</h3>
						<div class="panel-group" id="accordion">
							<?php foreach($data_child as $key => $dataFaqc){
								if($dataFaqc->title == $dataFaq->title){
							?>
							<div class="panel panel-default no-border">
								<div class="panel-heading no-border">
									<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key ?>">
									  <?php echo $dataFaqc->title_child ?>
									</a>
									</h4>
								</div>
								<div id="collapse<?php echo $key ?>" class="panel-collapse collapse in">
									<div class="panel-body no-border bb">
									<?php echo $dataFaqc->description ?>
									</div>
								</div>
							</div>
							<?php } } ?>
						</div></div>
					<?php } } ?>
					</div>
				</div>
				<br clear="all">
				<br clear="all">
				<br clear="all">
			</div>
		</div>
	</div>
{_layFooter}
<script>
$(document).ready(function(){
    $(".kategori").click(function(){
		var id = $(this).attr('id');
        $(".konten-faq").hide();
        $("#kontenshow"+id).show();
    });
});


$(document).ready(function(e){
		var base_url = "<?php echo base_url(); ?>";
		var input = $("input[name=key_search]");

			$.get(base_url+'faq/search', function(data){
						input.typeahead({
						    source: data,
						    minLength: 1,
						});
			}, 'json');

			input.change(function(){
				var current = input.typeahead("getActive");
				$('#id_child').val(current.id);
			});

	});	


</script>