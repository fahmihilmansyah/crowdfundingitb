<?php 
/**
* 
*/
class kurban_model extends CI_Model
{
	private $insert_id = 0;
	/**
	* #Created By : Fahmi Hilmansyah
	* #Created Date : 14-02-2017
	* #keterangan:
	* # $table=isikan table yang dituju
	* # $select=field yg ingin di isikan atau di tampilkan sesuai kondisi
	* # $condition=array(
		'where' = array(data),
		'or_where' = array(data),
		'join' = array(
		'table'=>'nama_table',
		'condition'=>'kondisi join', 
		'type' => 'LEFT', 'RIGHT', 'OUTER', 'INNER', 'LEFT OUTER', 'RIGHT OUTER' ),
		'join' = array(
				array(
					'table'=>'nama_table',
					'condition'=>'kondisi join', 
					'type' => 'LEFT', 'RIGHT', 'OUTER', 'INNER', 'LEFT OUTER', 'RIGHT OUTER' 
				),
				array(
					'table'=>'nama_table',
					'condition'=>'kondisi join', 
					'type' => 'LEFT', 'RIGHT', 'OUTER', 'INNER', 'LEFT OUTER', 'RIGHT OUTER' 
				)
			),//multipler join with array
		'order_by' = array(
			'field'=>{field table yg di orderby},
			'type'=>{'ASC','DESC'}
		),
		'order_by' = array(
			array(
				'field'=>{field table yg di orderby},
				'type'=>{'ASC','DESC'}
			),
			array(
				'field'=>{field table yg di orderby},
				'type'=>{'ASC','DESC'}
			)
		),//multiple order_by with array
		'group_by' = array(data),
		'limit' = array(
			'row' => 0,
			'offset' => 100
		),
	)
	*/	
	function custom_query($table=null,$condition=array(),$select="*"){
		$this->db->select($select);
		$this->db->from($table);
		if(is_array($condition)){
			if(count($condition) > 0){
				foreach ($condition as $k=>$v) {
					if($k == 'where'){
						$this->db->where($v,null,false);			
					}
					if($k == 'or_where'){
						$this->db->or_where($v,null,false);			
					}
					if($k == 'join'){
						if(isset($v['table'])){
							$this->db->join($v['table'],$v['condition'],$v['type']);
						}else{
							foreach ($v as $value) {
								$this->db->join($value['table'],$value['condition'],$value['type']);
							}	
						}		
					}
					if($k == 'order_by'){
						if(isset($v['field'])){
							$this->db->order_by($v['field'],$v['type']);
						}else{
							foreach ($v as $value) {
								$this->db->order_by($value['field'],$value['type']);
							}
						}		
					}
					if($k == 'group_by'){
							$this->db->group_by($v);
					}
					if($k == 'limit'){
							$this->db->limit($v['row'],$v['offset']);
					}
				}
			}
		}
		return $this->db->get();
	}
	function getInsertId(){
		return $this->insert_id;
	}
    function custom_insert($table=null, $data=array()){
    	$this->db->trans_begin();
    		$this->db->insert($table, $data); 
    		$this->insert_id = $this->db->insert_id();
    	if ($this->db->trans_status() === FALSE)
		{
		    $this->db->trans_rollback();
		    die("ERROR: INSERTING DATA, PLEASE CONTACT ADMINISTRATOR WEB");
		}
		else
		{
		    $this->db->trans_commit();
		}
    }
    function custom_update($table=null, $data=array(),$where=array()){
    	$this->db->trans_begin();
    		$this->db->where($where);
			$this->db->update($table, $data); 
    	if ($this->db->trans_status() === FALSE)
		{
		    $this->db->trans_rollback();
		    die("ERROR: UPDATING DATA, PLEASE CONTACT ADMINISTRATOR WEB");
		}
		else
		{
		    $this->db->trans_commit();
		}
    }
    function custom_delete($table=null,$where=array()){
    	$this->db->trans_begin();
    		$this->db->where($where);
			$this->db->delete($table); 
    	if ($this->db->trans_status() === FALSE)
		{
		    $this->db->trans_rollback();
		    die("ERROR: UPDATING DATA, PLEASE CONTACT ADMINISTRATOR WEB");
		}
		else
		{
		    $this->db->trans_commit();
		}
    }
}