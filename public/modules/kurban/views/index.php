{_layHeader}

<?php
$url = implode("/", $this->uri->segment_array());
$dats = array('urllink' => $url);
$this->session->set_userdata($dats);
$sessionsl = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
    $this->session->userdata(LGI_KEY . "login_info")['email'] : '';
?>
<?php $cek = empty($this->session->userdata('prevnomDonation')) ? "" : "active"; ?>

<img src="<?php URI::baseURL(); ?>assets/mumuapps/img/img-home.jpg">
<div class="content pr-15 pl-15 bg-white" style="padding-top: 0px;">
    <?php if (!empty($this->session->flashdata('error'))): ?>
        <div class="alert alert-danger alert-dismissable " role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <script type="text/javascript">
            setTimeout(function () {
                $('.alert').fadeOut();
            }, 10000); // <-- time in milliseconds
        </script>
    <?php endif; ?>
    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'kurban/kurban/' . $this->uri->segment(3), array('id' => 'fmdonasi', 'class' => 'fmdonasi'), $hidden_form);

    ?>
    <div class="mb-20">
        <h3>Kurban</h3></div>
    <?php
    if (empty($_GET['kdtrx']) && empty($_GET['jtrx'])) {
        $getmitra = $this->db->from('mitra')->order_by('sortorder asc')->get()->result();
        ?>
        <div class="form-group">
            <label>Mitra</label>
            <select class="form-control" id="listmitra">
                <?php foreach ($getmitra as $r): ?>
                    <?php if(!empty($_GET['cpartner'])):  if($r->id == $_GET['cpartner']): ?>
                    <option value="<?php echo $r->id ?>"><?php echo $r->nama_mitra ?></option>
                    <?php endif; else:?>
                    <option value="<?php echo $r->id ?>"><?php echo $r->nama_mitra ?></option>
                    <?php endif;?>
                <?php endforeach; ?>
            </select>
        </div>
    <?php } ?>
    <div class="form-group">
        <label>Jenis Hewan Kurban</label>
        <select class="form-control" id="jeniskurban" name="donationNominal">
            <?php foreach ($trxkurban as $r): ?>
                <option value="<?php echo $r->kategoricode ?>|<?php echo $r->amount ?>"><?php echo $r->kategoriname ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>No. Handphone</label>
        <input name="donationPhone" type="text" class="form-control" placeholder="Masukkan Nomor Handphone Anda">
    </div>
    <div class="form-group">
        <label>Nama</label>
        <input name="donationName" type="text" class="form-control" placeholder="Masukkan Nama Anda">
        <!--        <div class="custom-control custom-checkbox mt-5">-->
        <!--            <input type="checkbox" class="custom-control-input" id="customCheck1">-->
        <!--            <label class="custom-control-label" for="customCheck1">Danai secara Anonim</label>-->
        <!--        </div>-->
    </div>
    <div class="form-group">
        <label>Email</label>
        <input name="donationEmail" <?php echo !empty($sessionsl) ? 'readonly' : ''; ?>
               value="<?php echo !empty($sessionsl) ? $sessionsl : ''; ?>" type="text" class="form-control"
               placeholder="Masukkan Email Anda">
    </div>
    <div class="form-group">
        <label>Nama Pekurban:</label>
        <textarea name="donationCommment" class="form-control"
                  placeholder="Tuliskan nama Pekurban, untuk jumlah Pekurban lebih dari satu orang, pisahkan dengan tanda koma (,)]"
                  rows="3"></textarea>
    </div>

    <hr/>
    <div class="form-group">

        <input type="hidden" name="donationPay" id="paymetod">
        <div class="row">
            <div class="col-12">
                <span><b><h5>Metode Pembayaran : </h5></b></span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div id="respemb"></div>
    </div>
    <hr/>
    <div class="mt-30">
        <button class="btn btn-block btn-main"><span>Bayar</span></button>
    </div>

    <?php

    echo form_close();

    ?>
    <div class="mb-10"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
    <script>
        <?php $paramsd = [];
        foreach ($_GET as $k => $v):
            $paramsd[] = $k . "=" . $v;
        endforeach;
        $setparams = implode('&', $paramsd);
        ?>
        $("#respemb").load("<?php echo base_url('/pembayaran/index?' . $setparams); ?>");
        jQuery(document).on('click', '#pilbayar', function () {
            $("#settmpl").load("<?php echo base_url('/pembayaran/index') ?>");
        });
        jQuery(document).on('click', '.nextbayar', function () {
            $('#pills-pembayaran-tab').tab('show');
        });
        // $('#denomnms').number( true, 0, '', '.' );
        // var fnf = document.getElementById("denomnms");
        // fnf.addEventListener('keyup', function (evt) {
        //     var n = parseInt(this.value.replace(/\D/g, ''), 10);
        //     fnf.value = n.toLocaleString("id-ID");
        // }, false);
        <?php if(empty($_GET['kdtrx']) && empty($_GET['jtrx'])):?>
        function getjeniskurban() {
            var partner = $('#listmitra').val();
            $.ajax({
                method: "GET",
                url: "<?php echo base_url('/kurban/kurban/getharga') ?>?cpart=" + partner,
                dataType: "json",
                beforeSend: function () {
                    $("#jeniskurban").empty();
                },
                success: function (data) {
                    $.each(data.data, function () {
                        $("#jeniskurban").append('<option value="' + this.kategoricode + '|' + this.amount + '">' + this.kategoriname + '</option>');
                    })
                }
            });
        }
        getjeniskurban();
        $('#listmitra').on('change',function () {
            getjeniskurban();
        })

        <?php endif; ?>
    </script>


    {_layFooter}

