{_layHeader}

<div class="content">

    <div class="card mt-10">
        <div class="card-body">
            <div class="row  d-flex align-items-center">
                <div class="col-12 text-center">
                    <img src="<?php echo base_url().'assets/images/icons/checked.png' ?>" width="100"><br/>
                    <label class="btn btn-success text-white" style="margin-top: 20px;"> <?php echo $title ?></label>
                    <br/>
                    <br/>
                    <span>No Transaksi: <?php echo $trxid ?></span>
                </div>
                <div class="col-12 text-center">&nbsp;</div>
                <div class="col-12 text-center">
                    <a href="<?php echo base_url() ?>" class="btn btn-danger btn-sm"><i class="icofont-home"></i> Back Home</a>
                </div>
            </div>
           
        </div>
    </div>


    {_layFooter}

