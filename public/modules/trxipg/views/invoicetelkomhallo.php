<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->db->select('nb_sys_users.*');
$this->db->from('nb_trans_donation');
$this->db->join('nb_campaign', 'nb_trans_donation.campaign = nb_campaign.id');
$this->db->join('nb_sys_users', 'nb_campaign.user_post = nb_sys_users.id');
$this->db->where(['nb_trans_donation.no_transaksi' => $invoice]);
$query = $this->db->get()->result_array();
//print_r($query);
//exit;

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <title>Invoice</title>

    <style type="text/css">
        html, body {
            font-family: 'Source Sans Pro', sans-serif;
            background: #efefef;
            margin: 0px;
            padding: 0px;
        }

        .container {
            padding-top: 50px;
            padding-bottom: 50px;
            padding-left: 50px;
            padding-right: 50px;
            margin-right: auto;
            margin-left: auto;
            background: white;

        }

        .right {
            width: 50%;
            float: right;
        }

        .left {
            width: 49.5%;
            float: left;
        }

        .full {
            width: 100%;
            padding-top: 20px;
            clear: both;
        }

        .grey {
            background: #efefef;
            padding: 10px;
        }

        .table {
            border: 1px solid #ccc;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<div class="container">
    <table style="width:100%;">

        <?php if(!empty($query)): ?>
        <tr>
            <td>
                <div class="left">
                    <?php
                    $img = !empty($query[0]["img"]) ? $query[0]["img"] : 'noimage.png';
                    $img = URI::existsImage('admprofil/',
                    $img,
                    'articles/',
                    'noimage.jpg');
                    $url_image = $img;
                    ?>
                    <img src="<?php echo $url_image ?>" width="100">
                </div>
            </td>
            <td>
                <div align="right" class="right">
                    <?php echo $query[0]['fname'] ?><br/>
                    <?php echo $query[0]['address'] ?>
                        <br/>
                        Telpon: <?php echo $query[0]['nope'] ?><br/>
                </div>
            </td>
        </tr>
        <?php else: ?>
            <tr>
                <td>
                    <div class="left">
                        <img src="<?php echo base_url('assets/images/logo/logomumuh.png'); ?>" width="100">
                    </div>
                </td>
                <td>
                    <div align="right" class="right">
                        Mumu Apps<br/>
                        Philanthropy Building Lt. 2
                        Jl. Warung Jati Barat No. 14, Jatipadang Jakarta Selatan - 12450<br/>
                        Telpon: (021) 741 6050<br/>
                    </div>
                </td>
            </tr>
        <?php endif; ?>
    </table>
    <div class="full">
        <div class="grey">
            <b>BUKTI PEMBAYARAN</b><br/>
            No. Transaksi #<?php echo $invoice ?><br/>
        Tanggal Transaksi: <?php echo date("d M Y, H:i:s", strtotime($tgl_trx)) ?><br/>
        </div>
        <br/>
        <b>Kepada : <?php echo ucwords($fullname) ?></b><br/><br/>
        <?php
        if (!empty($mppob['data']['text_data'])):
            $datamta = $mppob['data']['text_data'];
            if($mppob['rc'] == '68'){ ?>
                <table class="table" width="100%">
                    <tr style="background:#efefef">
                        <th colspan="3"><?php echo $datamta ?></th>
                    </tr>
                </table>
            <?php }else{
            $exptmpdata = explode('|',$datamta);
            ?>
            <table class="table" width="100%">
                <tr style="background:#efefef">
                    <th colspan="3"><?php echo $exptmpdata[0] ?></th>
                </tr>
                <?php for($x=1;$x<=count($exptmpdata)-1;$x++): ?>
                    <tr>
                        <?php
                        $exp2 = explode(':',$exptmpdata[$x]);
                        ?>
                        <?php if(count($exp2) == 2): ?>
                        <td><?php echo $exp2[0]?></td>
                        <td>:</td>
                        <td><?php echo ($exp2[1]) ?></td>
                        <?php else: ?>
                            <?php if(trim($exp2[0]) == 'DETAIL TAGIHAN'): ?>
                            <td colspan="3"><?php echo $exp2[0]?></td>
                            <?php else: ?>
                            <td colspan="3" align="center"><?php echo $exp2[0]?></td>
                            <?php endif; ?>
                        <?php endif; ?>
                    </tr>
                <?php endfor; ?>
                <tr>
                    <td colspan="3">*Cetak Asli</td>
                </tr>
            </table>
        <?php } else: ?>
        <table class="table" width="100%">
            <tr style="background:#efefef">
                <th colspan="2">STRUK PEMBAYARAN TAGIHAN HALLO</th>
            </tr>
            <tr>
                <td align="">Billreff</td>
                <td><?php echo ($mppob['data']['billRefCustomer']) ?></td>
            </tr>
            <tr>
                <td align="">Reff</td>
                <td><?php echo ($mppob['data']['traxId']) ?></td>
            </tr>
            <tr>
                <td align="">IDPEL</td>
                <td><?php echo strtoupper($mppob['data']['customerId']) ?></td>
            </tr>
            <tr>
                <td align="">NAMA</td>
                <td><?php echo strtoupper($mppob['data']['customerName']) ?></td>
            </tr>
            <tr>
                <td align="">Harga</td>
                <td>RP <?php echo number_format((float)$mppob['data']['amount']) ?></td>
            </tr>
            <tr>
                <td align="">Admin</td>
                <td>RP <?php echo number_format((float)$mppob['data']['feeAmount']) ?></td>
            </tr>
            <tr>
                <td align="">Total Bayar</td>
                <td>RP <?php echo number_format((float)$mppob['data']['totalBayar']) ?></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
        <?php endif; ?>
        <?php if (!empty($no_polis)): ?>
    <br/>
        <table class="table" width="100%">
            <tr style="background:#efefef">
                <th width="5%">No</th>
                <th width="35%">No Polis</th>
            </tr>
            <tr>
                <td align="center">1</td>
                <td><?php echo ($no_polis) ?></td>
            </tr>
        </table>
            <p>Informasi lebih lanjut bisa menghubungi <a href="<?php echo base_url() ?>">mumuapps.id</a></p>
        <p>E-Polis Anda dapat diunduh pada laman berikut: <br/>
            <?php $epolis = 'http://dd.asyki.com/download/index/'.$no_polis.'/'.$tgl_lahit; ?>
            <a href="<?php echo $epolis?>">
                http://dd.asyki.com/download/index/<?php echo $no_polis ?>/<?php echo $tgl_lahit ?> </a>
            <br>
        </p>
        <?php endif; ?>
        <br/><br/>
        Terima kasih telah melakukan transaksi di MumuApps. Tingkatkan terus transkasi dan donasi anda, semoga berkah dan bermanfaat untuk Umat.
        <br/><br/>
        <div align="right" class="right">
            Powered by Duta Danadyaksa Teknologi &#169; <?php echo date("Y"); ?><br/>
        </div>
    </div>
</div>
</body>
</html>