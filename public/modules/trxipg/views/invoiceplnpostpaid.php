<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->db->select('nb_sys_users.*');
$this->db->from('nb_trans_donation');
$this->db->join('nb_campaign', 'nb_trans_donation.campaign = nb_campaign.id');
$this->db->join('nb_sys_users', 'nb_campaign.user_post = nb_sys_users.id');
$this->db->where(['nb_trans_donation.no_transaksi' => $invoice]);
$query = $this->db->get()->result_array();
//print_r($query);
//exit;
/**
 * Replaces all but the last for digits with x's in the given credit card number
 * @param int|string $cc The credit card number to mask
 * @return string The masked credit card number
 */
function MaskCreditCard($cc){
    // Get the cc Length
    $cc_length = strlen($cc);
    // Replace all characters of credit card except the last four and dashes
    for($i=0; $i<$cc_length-4; $i++){
        if($cc[$i] == '-'){continue;}
        $cc[$i] = 'X';
    }
    // Return the masked Credit Card #
    return $cc;
}
/**
 * Add dashes to a credit card number.
 * @param int|string $cc The credit card number to format with dashes.
 * @return string The credit card with dashes.
 */
function FormatCreditCard($cc)
{
    // Clean out extra data that might be in the cc
    $cc = str_replace(array('-',' '),'',$cc);
    // Get the CC Length
    $cc_length = strlen($cc);
    // Initialize the new credit card to contian the last four digits
    $newCreditCard = substr($cc,-4);
    // Walk backwards through the credit card number and add a dash after every fourth digit
    for($i=$cc_length-5;$i>=0;$i--){
        // If on the fourth character add a dash
        if((($i+1)-$cc_length)%4 == 0){
            $newCreditCard = '-'.$newCreditCard;
        }
        // Add the current character to the new credit card
        $newCreditCard = $cc[$i].$newCreditCard;
    }
    // Return the formatted credit card number
    return $newCreditCard;
}
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <title>Invoice</title>

    <style type="text/css">
        html, body {
            font-family: 'Source Sans Pro', sans-serif;
            background: #efefef;
            margin: 0px;
            padding: 0px;
        }

        .container {
            padding-top: 50px;
            padding-bottom: 50px;
            padding-left: 50px;
            padding-right: 50px;
            margin-right: auto;
            margin-left: auto;
            background: white;

        }

        .right {
            width: 50%;
            float: right;
        }

        .left {
            width: 49.5%;
            float: left;
        }

        .full {
            width: 100%;
            padding-top: 20px;
            clear: both;
        }

        .grey {
            background: #efefef;
            padding: 10px;
        }

        .table {
            border: 1px solid #ccc;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<div class="container">
    <table style="width:100%;">

        <?php if(!empty($query)): ?>
        <tr>
            <td>
                <div class="left">
                    <?php
                    $img = !empty($query[0]["img"]) ? $query[0]["img"] : 'noimage.png';
                    $img = URI::existsImage('admprofil/',
                    $img,
                    'articles/',
                    'noimage.jpg');
                    $url_image = $img;
                    ?>
                    <img src="<?php echo $url_image ?>" width="100">
                </div>
            </td>
            <td>
                <div align="right" class="right">
                    <?php echo $query[0]['fname'] ?><br/>
                    <?php echo $query[0]['address'] ?>
                        <br/>
                        Telpon: <?php echo $query[0]['nope'] ?><br/>
                </div>
            </td>
        </tr>
        <?php else: ?>
            <tr>
                <td>
                    <div class="left">
                        <img src="<?php echo base_url('assets/images/logo/logomumuh.png'); ?>" width="100">
                    </div>
                </td>
                <td>
                    <div align="right" class="right">
                        Mumu Apps<br/>
                        Philanthropy Building Lt. 2
                        Jl. Warung Jati Barat No. 14, Jatipadang Jakarta Selatan - 12450<br/>
                        Telpon: (021) 741 6050<br/>
                    </div>
                </td>
            </tr>
        <?php endif; ?>
    </table>
    <div class="full">
        <div class="grey">
            <b>BUKTI PEMBAYARAN</b><br/>
            No. Transaksi #<?php echo $invoice ?><br/>
        Tanggal Transaksi: <?php echo date("d M Y, H:i:s", strtotime($tgl_trx)) ?><br/>
        </div>
        <br/>
        <b>Kepada : <?php echo ucwords($fullname) ?></b><br/><br/>
        <?php
        $penalty=0;
        $meterawal = $mppob['data']['billInfo'][0]['pastMeter'];
        $meterakhir = 0;
        $period = [];
        $meterfull = [];
        foreach ($mppob['data']['billInfo'] as $r):
            $period[] = $r['period'];
            $penalty = $penalty + $r['penalty'];
        $meterakhir = $r['presentMeter'];
            $meterfull[] = $r['pastMeter'] .'-'. $r['presentMeter'];
        endforeach;
        ?>

        <table class="table" width="100%">
            <tr style="background:#efefef">
                <th colspan="2">STRUK PEMBAYARAN TAGIHAN LISTRIK</th>
            </tr>
            <tr>
                <td align="">NO REF</td>
                <td><?php echo ((string)$mppob['data']['plnReferenceNumber']) ?></td>
            </tr>
            <tr>
                <td align="">IDPEL</td>
                <td><?php echo strtoupper($mppob['data']['customerId']) ?></td>
            </tr>
            <tr>
                <td align="">NAMA</td>
                <td><?php echo strtoupper($mppob['data']['customerName']) ?></td>
            </tr>
            <tr>
                <td align="">TARIF/DAYA</td>
                <td><?php echo strtoupper($mppob['data']['subscriberSegmen'])."/".strtoupper((float)$mppob['data']['power']) ?></td>
            </tr>
            <tr>
                <td align="">BL/TH</td>
                <td><?php echo implode(', ',$period) ?></td>
            </tr>
            <tr>
                <td align="">STAND METER</td>
                <td><?php echo implode(' | ',$meterfull) ?></td>
            </tr>
            <tr>
                <td align="">RP TAG PLN</td>
                <td>Rp <?php echo number_format((float)$mppob['data']['amount']) ?></td>
            </tr>
            <tr>
                <td align="center" colspan="2"><b>PLN menyatakan struk ini sbg bukti pembayaran yg sah.</b></td>
            </tr>
            <tr>
                <td align="">ADMIN BANK</td>
                <td>RP <?php echo number_format((integer)$mppob['data']['feeAmount']) ?></td>
            </tr>
            <tr>
                <td align="">TOTAL BAYAR</td>
                <td>RP <?php echo number_format((integer)$mppob['data']['totalBayar']) ?></td>
            </tr>
            <tr>
                <td align="center" colspan="2">Terima Kasih</td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <?php echo ($mppob['data']['info']) ?><br/>
                    Rincian tagihan dapat diakses <a href="www.pln.co.id">www.pln.co.id</a>
                </td>
            </tr>
        </table>
        <?php if (!empty($no_polis)): ?>
    <br/>
        <table class="table" width="100%">
            <tr style="background:#efefef">
                <th width="5%">No</th>
                <th width="35%">No Polis</th>
            </tr>
            <tr>
                <td align="center">1</td>
                <td><?php echo ($no_polis) ?></td>
            </tr>
        </table>
        <p>E-Polis Anda dapat diunduh pada laman berikut: <br/>
            <?php $epolis = 'http://dd.asyki.com/download/index/'.$no_polis.'/'.$tgl_lahit; ?>
            <a href="<?php echo $epolis?>">
                http://dd.asyki.com/download/index/<?php echo $no_polis ?>/<?php echo $tgl_lahit ?> </a>
            <br>
        </p>
        <?php endif; ?>
        <br/><br/>
        Terima kasih telah melakukan transaksi di MumuApps. Tingkatkan terus transkasi dan donasi anda, semoga berkah dan bermanfaat untuk Umat.
        <br/><br/>
        <div align="right" class="right">
            Powered by Duta Danadyaksa Teknologi &#169; <?php echo date("Y"); ?><br/>
        </div>
    </div>
</div>
</body>
</html>