<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->db->select('nb_sys_users.*');
$this->db->from('nb_trans_donation');
$this->db->join('nb_campaign', 'nb_trans_donation.campaign = nb_campaign.id');
$this->db->join('nb_sys_users', 'nb_campaign.user_post = nb_sys_users.id');
$this->db->where(['nb_trans_donation.no_transaksi' => $invoice]);
$query = $this->db->get()->result_array();
//print_r($query);
//exit;
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <title>Invoice</title>

    <style type="text/css">
        html, body {
            font-family: 'Source Sans Pro', sans-serif;
            background: #efefef;
            margin: 0px;
            padding: 0px;
        }

        .container {
            padding-top: 50px;
            padding-bottom: 50px;
            padding-left: 50px;
            padding-right: 50px;
            margin-right: auto;
            margin-left: auto;
            background: white;

        }

        .right {
            width: 50%;
            float: right;
        }

        .left {
            width: 49.5%;
            float: left;
        }

        .full {
            width: 100%;
            padding-top: 20px;
            clear: both;
        }

        .grey {
            background: #efefef;
            padding: 10px;
        }

        .table {
            border: 1px solid #ccc;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<div class="container">
    <table style="width:100%;">

        <?php if(!empty($query)): ?>
        <tr>
            <td>
                <div class="left">
                    <?php
                    $img = !empty($query[0]["img"]) ? $query[0]["img"] : 'noimage.png';
                    $img = URI::existsImage('admprofil/',
                    $img,
                    'articles/',
                    'noimage.jpg');
                    $url_image = $img;
                    ?>
                    <img src="<?php echo $url_image ?>" width="100">
                </div>
            </td>
            <td>
                <div align="right" class="right">
                    <?php echo $query[0]['fname'] ?><br/>
                    <?php echo $query[0]['address'] ?>
                        <br/>
                        Telpon: <?php echo $query[0]['nope'] ?><br/>
                </div>
            </td>
        </tr>
        <?php else: ?>
            <tr>
                <td>
                    <div class="left">
                        <img src="<?php echo base_url('assets/images/logo/logomumuh.png'); ?>" width="100">
                    </div>
                </td>
                <td>
                    <div align="right" class="right">
                        Mumu Apps<br/>
                        Philanthropy Building Lt. 2
                        Jl. Warung Jati Barat No. 14, Jatipadang Jakarta Selatan - 12450<br/>
                        Telpon: (021) 741 6050<br/>
                    </div>
                </td>
            </tr>
        <?php endif; ?>
    </table>
    <div class="full">
        <div class="grey">
            BUKTI PEMBAYARAN<br/>
            No. Transaksi #<?php echo $invoice ?><br/>
        Tanggal Transaksi: <?php echo date("d M Y", strtotime($tgl_trx)) ?><br/>
        </div>
        <br/>
        <b>Kepada : <?php echo ucwords($fullname) ?></b><br/><br/>
        <table class="table" width="100%">
            <tr style="background:#efefef">
                <th width="5%">No</th>
                <th width="35%">Jenis Transaksi / Campaign</th>
                <th width="25%">Nominal</th>
            </tr>
            <tr>
                <td align="center">1</td>
                <td><?php echo strtoupper($jenistrx) ?></td>
                <td align="center">Rp <?php echo number_format($amount, 0, ",", ".") ?></td>
            </tr>
        </table>
        <?php if (!empty($no_polis)): ?>
    <br/>
        <table class="table" width="100%">
            <tr style="background:#efefef">
                <th width="5%">No</th>
                <th width="35%">No Polis</th>
            </tr>
            <tr>
                <td align="center">1</td>
                <td><?php echo ($no_polis) ?></td>
            </tr>
        </table>
        <p>E-Polis Anda dapat diunduh pada laman berikut: <br/>
            <?php $epolis = 'http://dd.asyki.com/download/index/'.$no_polis.'/'.$tgl_lahit; ?>
            <a href="<?php echo $epolis?>">
                http://dd.asyki.com/download/index/<?php echo $no_polis ?>/<?php echo $tgl_lahit ?> </a>
            <br>
        </p>
        <?php endif; ?>
        <br/><br/>
        Terima kasih telah melakukan transaksi di MumuApps. Tingkatkan terus transkasi dan donasi anda, semoga berkah dan bermanfaat untuk Umat.
        <br/><br/>
        <div align="right" class="right">
<!--            Dompet Dhuafa (DD)<br/>-->
            Powered by Duta Danadyaksa Teknologi &#169; <?php echo date("Y"); ?><br/>
        </div>
    </div>
</div>
</body>
</html>