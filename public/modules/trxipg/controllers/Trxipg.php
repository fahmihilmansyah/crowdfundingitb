<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Trxipg extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_modList = array(
        "Trxipg/Trxipg_model" => "infaqmdl"
    );


    protected $_data = array();

    private $_SESSION;


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

        $this->_SESSION = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
            $this->session->userdata(LGI_KEY . "login_info") : '';


    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);

        // REDIRECT IF SESSION EXIST
        // $this->nbauth_front->lookingForAuten();
    }


    public function index($data = array())
    {

    }

    function ipgsuccessold($id = '')
    {
        $idipg = Fhhlib::uuidv4Nopad();
        $insipgtrx = array('id' => $idipg,
            'request_json' => json_encode($_REQUEST),
            'id_transipg' => $id,
            'jenis' => 'success',
            'created_ad' => date('Y-m-d H:i:s'),
        );
        $this->infaqmdl->custom_insert("nb_trans_ipg_log", $insipgtrx);

        $dataupdate = array('rescallback_json' => json_encode($_REQUEST));
        $datawhere = array('id' => $id);
        $this->infaqmdl->custom_update('nb_trans_ipg', $dataupdate, $datawhere);
        $title = 'Transaksi Sukses';
        $no_transaksi = $id;
        $dataupd = array(
            'status' => 'SUCCESS',
            'response_get' => !empty($_GET) ? json_encode($_GET) : '',
            'response_post' => !empty($_POST) ? json_encode($_POST) : '',
            'updated_ad' => date('Y-m-d H:i:s'),
        );
        $invoice = !empty($_REQUEST['invoice']) ? $_REQUEST['invoice'] : '';
        $datawhere = array(
            'id' => $this->db->escape($id),
            'trxid' => $this->db->escape($invoice),
        );
        $datawhere2 = array(
            'id' => ($id),
            'trxid' => ($invoice),
            'status' => ('WAITING'),
        );
        $kondisi['where'] = $datawhere;
        $getipgdata = $this->infaqmdl->custom_query("nb_trans_ipg", $kondisi)->result();
        if (!empty($getipgdata)) {
            $target_table = $getipgdata[0]->target_table;
            $no_transaksi = $getipgdata[0]->trxid;
            $statustrx = $getipgdata[0]->status;
            if ($statustrx == 'WAITING') {
                $wheretrx = array('no_transaksi' => $no_transaksi);
                $wheretrx2['where'] = array('no_transaksi' => $no_transaksi);
                $updatetrx = array('status' => 'verified', 'edtd_at' => date('Y-m-d H:i:s'));
                $gettrxcampaign = $this->infaqmdl->custom_query($target_table, $wheretrx2)->result();
                if (!empty($gettrxcampaign)) {
                    if (!empty($gettrxcampaign[0]->campaign)) {
                        $wherecampaign = array('id' => $gettrxcampaign[0]->campaign);
                        $wherecampaign2['where'] = array('id' => $gettrxcampaign[0]->campaign);
                        $caricamp = $this->infaqmdl->custom_query('nb_campaign', $wherecampaign2)->result();
                        $totaldona = (float)$caricamp[0]->now + (float)$gettrxcampaign[0]->nomuniq;
                        $totalsumdona = (float)$caricamp[0]->sum_donatur + 1;
                        $updatetrxcamp = array('now' => $totaldona, 'sum_donatur' => $totalsumdona);
                        $this->infaqmdl->custom_update('nb_campaign', $updatetrxcamp, $wherecampaign);
                    }
                    $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                }
                $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                $this->infaqmdl->custom_update("nb_trans_ipg", $dataupd, $datawhere2);
            }
        }
        $data = $this->_data;
        $data['title'] = $title;
        $data['trxid'] = $no_transaksi;
        $this->parser->parse("index", $data);
    }

    function ipgsuccess($id = '')
    {
        $tgl_skr = date('Y-m-d H:i:s');
        $idipg = Fhhlib::uuidv4Nopad();
        $insipgtrx = array('id' => $idipg,
            'request_json' => json_encode($_REQUEST),
            'id_transipg' => $id,
            'jenis' => 'success',
            'created_ad' => $tgl_skr,
        );
        $this->infaqmdl->custom_insert("nb_trans_ipg_log", $insipgtrx);

        $dataupdate = array('rescallback_json' => json_encode($_REQUEST));
        $datawhere = array('id' => $id);
        $this->infaqmdl->custom_update('nb_trans_ipg', $dataupdate, $datawhere);
        $title = 'Transaksi Sukses';
        $no_transaksi = $id;
        $invoice = !empty($_REQUEST['invoice']) ? $_REQUEST['invoice'] : '';
        $result_code = !empty($_REQUEST['result_code']) ? $_REQUEST['result_code'] : '';
        $result_desc = !empty($_REQUEST['result_desc']) ? $_REQUEST['result_desc'] : '';
        $statusasli = 'gagal';
        $dataupd = array(
            'status' => 'FAILED',
            'response_get' => !empty($_GET) ? json_encode($_GET) : '',
            'response_post' => !empty($_POST) ? json_encode($_POST) : '',
            'updated_ad' => $tgl_skr,
        );
        $updatetrx = array('status' => 'cancel', 'edtd_at' => $tgl_skr);
        if ((strtoupper($result_code) == '00' || strtoupper($result_code) == '0000') && strtoupper($result_desc) == 'SUCCESS') {
            $dataupd = array(
                'status' => 'SUCCESS',
                'response_get' => !empty($_GET) ? json_encode($_GET) : '',
                'response_post' => !empty($_POST) ? json_encode($_POST) : '',
                'updated_ad' => $tgl_skr,
            );
            $statusasli = 'sukses';
            $updatetrx = array('status' => 'verified', 'edtd_at' => $tgl_skr);
        }
        $datawhere = array(
            'id' => $this->db->escape($id),
            'trxid' => $this->db->escape($invoice),
        );
        $datawhere2 = array(
            'id' => ($id),
            'trxid' => ($invoice),
            'status' => ('WAITING'),
        );
        $kondisi['where'] = $datawhere;
        $getipgdata = $this->infaqmdl->custom_query("nb_trans_ipg", $kondisi)->result();
        if (!empty($getipgdata)) {


            $target_table = $getipgdata[0]->target_table;
            $no_transaksi = $getipgdata[0]->trxid;
            $statustrx = $getipgdata[0]->status;
            if ($statustrx == 'WAITING') {
                $this->db->trans_begin();
                try {
                    $wheretrx = ['no_transaksi' => ($no_transaksi)];
                    $wheretrx2 = array('where' => ['no_transaksi' => $this->db->escape($no_transaksi)]);

                    $gettrxcampaign = $this->infaqmdl->custom_query($target_table, $wheretrx2)->result();
                    if (!empty($gettrxcampaign)) {

                        if ($statusasli == 'sukses') {
                            $kontenemail = 'trxipg/invoice1';
                            $jenistrx = '';
                            $nopolisasyki = '';
                            $mppob = [];
                            $tgl_lahir = '';
                            $jntrx = 'TRX';
                            $amounts = 0;
                            $balanceamount = 0;
                            $prevBalance = 0;
                            if (!empty($gettrxcampaign[0]->jenistrx)) {
                                $whereprogram['where'] = array('kategoricode' => $this->db->escape($gettrxcampaign[0]->jenistrx));
                                $cariprog = $this->infaqmdl->custom_query('nb_trans_program_kategori', $whereprogram)->result();

                                $jenistrx = '';
                                if (!empty($cariprog)):
                                    $jenistrx = $cariprog[0]->kategoriname;
                                    $expl = explode("-", $gettrxcampaign[0]->jenistrx);
                                    if (count($expl) == 3 && $cariprog[0]->type == 'ASURANSI_PERJALANAN') {
                                        $tgl_lahir = $gettrxcampaign[0]->tgl_lahir;
                                        $dataisipolis['paket'] = $expl[2];
                                        $dataisipolis['trx_id'] = $gettrxcampaign[0]->no_transaksi;
                                        $dataisipolis['nik'] = $gettrxcampaign[0]->no_ktp;
                                        $dataisipolis['nama'] = $gettrxcampaign[0]->namaDonatur;
                                        $dataisipolis['tgl_lahir'] = $gettrxcampaign[0]->tgl_lahir;
                                        $dataisipolis['jenis_kelamin'] = $gettrxcampaign[0]->jk;
                                        $dataisipolis['email'] = $gettrxcampaign[0]->emailDonatur;
                                        $dataisipolis['no_ponsel'] = $gettrxcampaign[0]->telpDonatur;
                                        $dataisipolis['nominal'] = $gettrxcampaign[0]->nomuniq;
                                        $dataisipolis['masa_asuransi'] = $expl[1];
                                        $polisasyki = $this->konekpolis($dataisipolis);
                                        $polisasyki = json_decode($polisasyki, true);
                                        $nopolisasyki = !empty($polisasyki['data']['certificate_no']) ? $polisasyki['data']['certificate_no'] : '';
                                        $updatetrx['no_polis'] = $nopolisasyki;
                                    }

                                    if ($cariprog[0]->type == 'MPPOB') {
                                        $carippob = $this->db->from('nb_ppob_pay')->where(['no_transaksi' => $gettrxcampaign[0]->no_transaksi])->get()->result();

                                        $trxidppob = uniqid('PPOB');
                                        $datappob = array(
                                            'kode_produk' => $carippob[0]->produk,
                                            'trxid' => $trxidppob,
                                            'trx_type' => 'PAY',
                                            'amount' => (int)$carippob[0]->amount,
                                            'billNumber' => $carippob[0]->billreff
                                        );
                                        $responseppob = $this->_koneksippob($datappob, $carippob[0]->no_transaksi);

                                        $insdt = array(
                                            'id' => Fhhlib::uuidv4(),
                                            'request_json' => json_encode($datappob),
                                            'response_json' => $responseppob,
                                            'created_ad' => $tgl_skr,
                                            'no_transaksi' => $gettrxcampaign[0]->no_transaksi
                                        );
                                        $this->infaqmdl->custom_insert("nb_trans_ppob_request", $insdt);
//                                    print_r($responseppob);
                                        $responseppob = json_decode($responseppob, true);
                                        $mppob = $responseppob;
                                        if ($carippob[0]->produk == 'M1002'):
                                            $kontenemail = 'trxipg/invoiceplntoken';
                                        endif;
                                        if ($carippob[0]->produk == 'M1001'):
                                            $kontenemail = 'trxipg/invoicetelkom';
                                        endif;
                                        $arrtelco = ['M1004', 'M1006', 'M1007', 'M1008', 'M1009', 'M1010',];
                                        if (in_array($carippob[0]->produk, $arrtelco)):
                                            $kontenemail = 'trxipg/invoicetelkomhallo';
                                        endif;
                                        if ($carippob[0]->produk == 'M1003'):
                                            $kontenemail = 'trxipg/invoiceplnpostpaid';
                                        endif;
                                        if ($carippob[0]->produk == 'M1150'):
                                            $kontenemail = 'trxipg/invoicepulsamta';
                                        endif;
                                        if ($carippob[0]->produk == 'M11520'):
                                            $kontenemail = 'trxipg/invoicepulsamta';
                                        endif;
                                        if ($carippob[0]->produk == 'M11625'):
                                            $kontenemail = 'trxipg/invoicepulsamta';
                                        endif;
                                        if ($carippob[0]->produk == 'M11725'):
                                            $kontenemail = 'trxipg/invoicepulsamta';
                                        endif;
                                    }
                                    if (count($expl) == 2 && $cariprog[0]->type == 'PPOB') {
                                        $postdata = ['grant_type' => 'password',
                                            'username' => 'mumuweb1', 'password' => 'mumuweb123'];
                                        $HEADER = ['authorization: Basic bG9jYWxkZXY6dDYxRklMNUJuV2U3Wmp5cDlncTNFWE1KaERZ',
                                            'content-type: application/x-www-form-urlencoded'];
                                        $postdata = http_build_query($postdata);
                                        $token = $this->_curl_exexc("http://35.240.184.113/am/mydomain/oauth/token", $postdata, 'POST', $HEADER);
                                        $token = json_decode($token, true);
                                        $access_token = $token['access_token'];
                                        $HEADER = ['authorization: Bearer ' . $access_token,
                                            'content-type: application/json'];
                                        $postdata = [
                                            'fsp' => 'multibiller-hike',
                                            'id' => 'mumuweb1',
                                            'props' => array(
                                                'callback' => base_url('trxipg/responseppob'),
                                                'prodId' => $expl[1],
                                                'destination' => $gettrxcampaign[0]->telpDonatur,
                                            ),
                                        ];
                                        $postdata = json_encode($postdata);
                                        $datapulsa = $this->_curl_exexc("http://34.87.70.137/gateway", $postdata, 'POST', $HEADER);
                                        $insdt = array(
                                            'id' => Fhhlib::uuidv4(),
                                            'request_json' => json_encode($postdata),
                                            'response_json' => $datapulsa,
                                            'created_ad' => $tgl_skr,
                                            'no_transaksi' => $gettrxcampaign[0]->no_transaksi
                                        );
                                        $this->infaqmdl->custom_insert("nb_trans_ppob_request", $insdt);
                                    }
                                endif;
                            }
                            if (!empty($gettrxcampaign[0]->campaign)) {
                                $wherecampaign2 = array('where' => array('id' => $gettrxcampaign[0]->campaign));
                                $wherecampaign = array('id' => $gettrxcampaign[0]->campaign);
                                $caricamp = $this->infaqmdl->custom_query('nb_campaign', $wherecampaign2)->result();
                                $totaldona = (float)$caricamp[0]->now + (float)$gettrxcampaign[0]->nomuniq;
                                $totalsumdona = (float)$caricamp[0]->sum_donatur + 1;
                                $updatetrxcamp = array('now' => $totaldona, 'sum_donatur' => $totalsumdona);

                                $this->infaqmdl->custom_update('nb_campaign', $updatetrxcamp, $wherecampaign);
                                $jenistrx = "Donasi : " . $caricamp[0]->title;
                            }

                            $dataisi['invoice'] = $no_transaksi;
                            $dataisi['tgl_trx'] = $tgl_skr;
                            $dataisi['jenistrx'] = $jenistrx;
                            $dataisi['fullname'] = $gettrxcampaign[0]->namaDonatur;
                            $dataisi['senderName'] = 'MumuApps - System';
                            $dataisi['emailTo'] = $gettrxcampaign[0]->emailDonatur;
                            $dataisi['subject'] = 'Invoice Transaksi';
                            $dataisi['amount'] = $gettrxcampaign[0]->nomuniq;
                            $dataisi['no_polis'] = $nopolisasyki;
                            $dataisi['tgl_lahit'] = $tgl_lahir;
                            $dataisi['mppob'] = $mppob;
                            $dataisi['konten'] = $kontenemail;
                            $resendmail = sendmail::SendMail($dataisi);
                        }
                        $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                        if ($gettrxcampaign[0]->donatur != '0') {
                            $wheredepo = array('donatur' => $gettrxcampaign[0]->donatur);
                            $wheresaldo['where'] = array('donatur' => $this->db->escape($gettrxcampaign[0]->donatur));
                            $carisaldo = $this->infaqmdl->custom_query('nb_donaturs_saldo', $wheresaldo)->result();
                            $amounts = $gettrxcampaign[0]->nomuniq;
                            $prevBalance = $carisaldo[0]->balanceAmount;
                            if (!empty($gettrxcampaign[0]->jenistrx) && ($gettrxcampaign[0]->jenistrx == 'deposit')) {
                                $balanceamount = $prevBalance + $amounts;
                                $updatedepo = array('prevBalance' => $prevBalance, 'balanceamount' => $balanceamount, 'edtd_at' => date('Y-m-d H:i:s'));
                                $this->infaqmdl->custom_update('nb_donaturs_saldo', $updatedepo, $wheredepo);
                                $jntrx = 'DEPO';
                            }
//                        else{
//                            $balanceamount = $carisaldo[0]->balanceAmount - $amounts;
//                            $updatedepo = array('prevBalance' => $prevBalance, 'balanceamount' => $balanceamount, 'edtd_at' => date('Y-m-d H:i:s'));
//                            $this->infaqmdl->custom_update('nb_donaturs_saldo', $updatedepo, $wheredepo);
//                            $jntrx = 'TRX';
//                        }
//                        $insdt = array(
//                            'donaturs' => $gettrxcampaign[0]->donatur,
//                            'jenistrx' => $jntrx,
//                            'id_transaksi' =>  $gettrxcampaign[0]->no_transaksi,
//                            'prevBalance' => $prevBalance,
//                            'amount' => $amounts,
//                            'balance' =>$balanceamount,
//                            'crtd_at' => date("Y-m-d H:i:s"),
//                            'edtd_at' => date("Y-m-d H:i:s"),
//                        );
//                        $this->infaqmdl->custom_insert("nb_donaturs_trx", $insdt);
                        }
                    }
                    $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                    $this->infaqmdl->custom_update("nb_trans_ipg", $dataupd, $datawhere2);

                    $this->db->trans_commit();
                }catch (\Exception $e) {
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        die("ERROR: INSERTING DATA, PLEASE CONTACT ADMINISTRATOR WEB Error: ".$e->getMessage());
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
        }
        $data = $this->_data;
        $data['title'] = $title;
        $data['trxid'] = $no_transaksi;
        $this->parser->parse("index", $data);
    }
    function ipgreturn($id = '')
    {
        $tgl_skr = date('Y-m-d H:i:s');
        $idipg = Fhhlib::uuidv4Nopad();
        $insipgtrx = array('id' => $idipg,
            'request_json' => json_encode($_REQUEST),
            'id_transipg' => $id,
            'jenis' => 'success',
            'created_ad' => $tgl_skr,
        );
        $this->infaqmdl->custom_insert("nb_trans_ipg_log", $insipgtrx);

        $dataupdate = array('rescallback_json' => json_encode($_REQUEST));
        $datawhere = array('id' => $id);
        $this->infaqmdl->custom_update('nb_trans_ipg', $dataupdate, $datawhere);
        $title = 'Transaksi Sukses';
        $no_transaksi = $id;
        $invoice = !empty($_REQUEST['invoice']) ? $_REQUEST['invoice'] : '';
        $result_code = !empty($_REQUEST['result_code']) ? $_REQUEST['result_code'] : '';
        $result_desc = !empty($_REQUEST['result_desc']) ? $_REQUEST['result_desc'] : '';
        $statusasli = 'gagal';
        $dataupd = array(
            'status' => 'FAILED',
            'response_get' => !empty($_GET) ? json_encode($_GET) : '',
            'response_post' => !empty($_POST) ? json_encode($_POST) : '',
            'updated_ad' => $tgl_skr,
        );
        $updatetrx = array('status' => 'cancel', 'edtd_at' => $tgl_skr);
        if ((strtoupper($result_code) == '00' || strtoupper($result_code) == '0000') && strtoupper($result_desc) == 'SUCCESS') {
            $dataupd = array(
                'status' => 'SUCCESS',
                'response_get' => !empty($_GET) ? json_encode($_GET) : '',
                'response_post' => !empty($_POST) ? json_encode($_POST) : '',
                'updated_ad' => $tgl_skr,
            );
            $statusasli = 'sukses';
            $updatetrx = array('status' => 'verified', 'edtd_at' => $tgl_skr);
        }
        $datawhere = array(
            'id' => $this->db->escape($id),
            'trxid' => $this->db->escape($invoice),
        );
        $datawhere2 = array(
            'id' => ($id),
            'trxid' => ($invoice),
            'status' => ('WAITING'),
        );
        $kondisi['where'] = $datawhere;
        $getipgdata = $this->infaqmdl->custom_query("nb_trans_ipg", $kondisi)->result();
        if (!empty($getipgdata)) {
            $target_table = $getipgdata[0]->target_table;
            $no_transaksi = $getipgdata[0]->trxid;
            $statustrx = $getipgdata[0]->status;
            if ($statustrx == 'WAITING') {
                $this->db->trans_begin();
                $wheretrx = ['no_transaksi' => ($no_transaksi)];
                $wheretrx2 = array('where' => ['no_transaksi' => $this->db->escape($no_transaksi)]);

                $gettrxcampaign = $this->infaqmdl->custom_query($target_table, $wheretrx2)->result();
                if (!empty($gettrxcampaign)) {
                    if ($statusasli == 'sukses') {

                        $kontenemail = 'trxipg/invoice1';
                        $mppob =[];
                        $jenistrx = '';
                        $nopolisasyki = '';
                        $tgl_lahir = '';
                        $jntrx = 'TRX';
                        $amounts = 0;
                        $balanceamount = 0;
                        $prevBalance = 0;
                        if (!empty($gettrxcampaign[0]->jenistrx)) {
                            $whereprogram['where'] = array('kategoricode' => $this->db->escape($gettrxcampaign[0]->jenistrx));
                            $cariprog = $this->infaqmdl->custom_query('nb_trans_program_kategori', $whereprogram)->result();
                            $jenistrx = '';
                            if (!empty($cariprog)):
                                $jenistrx = $cariprog[0]->kategoriname;
                                $expl = explode("-", $gettrxcampaign[0]->jenistrx);
                                if (count($expl) == 3 && $cariprog[0]->type == 'ASURANSI_PERJALANAN') {
                                    $tgl_lahir = $gettrxcampaign[0]->tgl_lahir;
                                    $dataisipolis['paket'] = $expl[2];
                                    $dataisipolis['trx_id'] = $gettrxcampaign[0]->no_transaksi;
                                    $dataisipolis['nik'] = $gettrxcampaign[0]->no_ktp;
                                    $dataisipolis['nama'] = $gettrxcampaign[0]->namaDonatur;
                                    $dataisipolis['tgl_lahir'] = $gettrxcampaign[0]->tgl_lahir;
                                    $dataisipolis['jenis_kelamin'] = $gettrxcampaign[0]->jk;
                                    $dataisipolis['email'] = $gettrxcampaign[0]->emailDonatur;
                                    $dataisipolis['no_ponsel'] = $gettrxcampaign[0]->telpDonatur;
                                    $dataisipolis['nominal'] = $gettrxcampaign[0]->nomuniq;
                                    $dataisipolis['masa_asuransi'] = $expl[1];
                                    $polisasyki = $this->konekpolis($dataisipolis);
                                    $polisasyki = json_decode($polisasyki, true);
                                    $nopolisasyki = !empty($polisasyki['data']['certificate_no']) ? $polisasyki['data']['certificate_no'] : '';
                                    $updatetrx['no_polis'] = $nopolisasyki;
                                }
                                if ($cariprog[0]->type == 'MPPOB') {
                                    $carippob = $this->db->from('nb_ppob_pay')->where(['no_transaksi'=>$gettrxcampaign[0]->no_transaksi])->get()->result();
                                    $trxidppob= uniqid('PPOB');
                                    $datappob = array(
                                        'kode_produk' => $carippob[0]->produk,
                                        'trxid' => $trxidppob,
                                        'trx_type' => 'PAY',
                                        'amount' => $carippob[0]->amount,
                                        'billNumber' => $carippob[0]->billreff
                                    );
                                    $responseppob = $this->_koneksippob($datappob,$carippob[0]->no_transaksi);
                                    $responseppob = json_decode($responseppob,true);
                                    $mppob=$responseppob;
                                    if($carippob[0]->produk == 'M1002'):
                                        $kontenemail = 'trxipg/invoiceplntoken';
                                    endif;
                                    if($carippob[0]->produk == 'M1001'):
                                        $kontenemail = 'trxipg/invoicetelkom';
                                    endif;
                                    if($carippob[0]->produk == 'M1004'):
                                        $kontenemail = 'trxipg/invoicetelkomhallo';
                                    endif;
                                    if($carippob[0]->produk == 'M1003'):
                                        $kontenemail = 'trxipg/invoiceplnpostpaid';
                                    endif;
                                }
                                if (count($expl) == 2 && $cariprog[0]->type == 'PPOB') {
                                    $postdata = ['grant_type' => 'password',
                                        'username' => 'mumuweb1', 'password' => 'mumuweb123'];
                                    $HEADER = ['authorization: Basic bG9jYWxkZXY6dDYxRklMNUJuV2U3Wmp5cDlncTNFWE1KaERZ',
                                        'content-type: application/x-www-form-urlencoded'];
                                    $postdata = http_build_query($postdata);
                                    $token = $this->_curl_exexc("http://35.240.184.113/am/mydomain/oauth/token", $postdata, 'POST', $HEADER);
                                    $token = json_decode($token, true);
                                    $access_token = $token['access_token'];
                                    $HEADER = ['authorization: Bearer ' . $access_token,
                                        'content-type: application/json'];
                                    $postdata = [
                                        'fsp' => 'multibiller-hike',
                                        'id' => 'mumuweb1',
                                        'props' => array(
                                            'callback' => base_url('trxipg/responseppob'),
                                            'prodId' => $expl[1],
                                            'destination' => $gettrxcampaign[0]->telpDonatur,
                                        ),
                                    ];
                                    $postdata = json_encode($postdata);
                                    $datapulsa = $this->_curl_exexc("http://34.87.70.137/gateway", $postdata, 'POST', $HEADER);
                                    $insdt = array(
                                        'id' => Fhhlib::uuidv4(),
                                        'request_json' => json_encode($postdata),
                                        'response_json' => $datapulsa,
                                        'created_ad' => $tgl_skr,
                                        'no_transaksi' => $gettrxcampaign[0]->no_transaksi
                                    );
                                    $this->infaqmdl->custom_insert("nb_trans_ppob_request", $insdt);
                                }
                            endif;
                        }
                        if (!empty($gettrxcampaign[0]->campaign)) {
                            $wherecampaign2 = array('where' => array('id' => $gettrxcampaign[0]->campaign));
                            $wherecampaign = array('id' => $gettrxcampaign[0]->campaign);
                            $caricamp = $this->infaqmdl->custom_query('nb_campaign', $wherecampaign2)->result();
                            $totaldona = (float)$caricamp[0]->now + (float)$gettrxcampaign[0]->nomuniq;
                            $totalsumdona = (float)$caricamp[0]->sum_donatur + 1;
                            $updatetrxcamp = array('now' => $totaldona, 'sum_donatur' => $totalsumdona);

                            $this->infaqmdl->custom_update('nb_campaign', $updatetrxcamp, $wherecampaign);
                            $jenistrx = "Donasi : " . $caricamp[0]->title;
                        }
                        $dataisi['invoice'] = $no_transaksi;
                        $dataisi['tgl_trx'] = $tgl_skr;
                        $dataisi['jenistrx'] = $jenistrx;
                        $dataisi['fullname'] = $gettrxcampaign[0]->namaDonatur;
                        $dataisi['senderName'] = 'MumuApps - System';
                        $dataisi['emailTo'] = $gettrxcampaign[0]->emailDonatur;
                        $dataisi['subject'] = 'Invoice Transaksi';
                        $dataisi['amount'] = $gettrxcampaign[0]->nomuniq;
                        $dataisi['no_polis'] = $nopolisasyki;
                        $dataisi['tgl_lahit'] = $tgl_lahir;
//                        $dataisi['konten'] = 'trxipg/invoice1';
                        $dataisi['mppob'] = $mppob;
                        $dataisi['konten'] = $kontenemail;
                        sendmail::SendMail($dataisi);
                    }
                    $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                    if($gettrxcampaign[0]->donatur != '0'){
                        $wheredepo = array('donatur' => $gettrxcampaign[0]->donatur);
                        $wheresaldo['where'] = array('donatur' => $this->db->escape($gettrxcampaign[0]->donatur));
                        $carisaldo = $this->infaqmdl->custom_query('nb_donaturs_saldo', $wheresaldo)->result();
                        $amounts = $gettrxcampaign[0]->nomuniq;
                        $prevBalance = $carisaldo[0]->balanceAmount;
                        if (!empty($gettrxcampaign[0]->jenistrx)&& ($gettrxcampaign[0]->jenistrx == 'deposit')) {
                            $balanceamount = $prevBalance + $amounts;
                            $updatedepo = array('prevBalance' => $prevBalance, 'balanceamount' => $balanceamount, 'edtd_at' => date('Y-m-d H:i:s'));
                            $this->infaqmdl->custom_update('nb_donaturs_saldo', $updatedepo, $wheredepo);
                            $jntrx = 'DEPO';
                        }
//                        else{
//                            $balanceamount = $carisaldo[0]->balanceAmount - $amounts;
//                            $updatedepo = array('prevBalance' => $prevBalance, 'balanceamount' => $balanceamount, 'edtd_at' => date('Y-m-d H:i:s'));
//                            $this->infaqmdl->custom_update('nb_donaturs_saldo', $updatedepo, $wheredepo);
//                            $jntrx = 'TRX';
//                        }
//                        $insdt = array(
//                            'donaturs' => $gettrxcampaign[0]->donatur,
//                            'jenistrx' => $jntrx,
//                            'id_transaksi' =>  $gettrxcampaign[0]->no_transaksi,
//                            'prevBalance' => $prevBalance,
//                            'amount' => $amounts,
//                            'balance' =>$balanceamount,
//                            'crtd_at' => date("Y-m-d H:i:s"),
//                            'edtd_at' => date("Y-m-d H:i:s"),
//                        );
//                        $this->infaqmdl->custom_insert("nb_donaturs_trx", $insdt);
                    }
                }
                $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                $this->infaqmdl->custom_update("nb_trans_ipg", $dataupd, $datawhere2);

                $this->db->trans_commit();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    die("ERROR: INSERTING DATA, PLEASE CONTACT ADMINISTRATOR WEB");
                } else {
                    $this->db->trans_commit();
                }
            }
        }
        $data = $this->_data;
        $data['title'] = $title;
        $data['trxid'] = $no_transaksi;
        $this->parser->parse("index", $data);
    }

    function ipgfailed($id = '')
    {

        $idipg = Fhhlib::uuidv4Nopad();
        $insipgtrx = array('id' => $idipg,
            'request_json' => json_encode($_REQUEST),
            'id_transipg' => $id,
            'jenis' => 'failed',
            'created_ad' => date('Y-m-d H:i:s'),
        );
        $this->infaqmdl->custom_insert("nb_trans_ipg_log", $insipgtrx);
        $dataupdate = array('rescallback_json' => json_encode($_REQUEST));
        $datawhere = array('id' => $id);
        $this->infaqmdl->custom_update('nb_trans_ipg', $dataupdate, $datawhere);
        $title = 'Transaksi Failed';
        $no_transaksi = $id;
        $dataupd = array(
            'status' => 'FAILED',
            'response_get' => !empty($_GET) ? json_encode($_GET) : '',
            'response_post' => !empty($_POST) ? json_encode($_POST) : '',
            'updated_ad' => date('Y-m-d H:i:s'),
        );
        $invoice = !empty($_REQUEST['invoice']) ? $_REQUEST['invoice'] : '';
        $datawhere = array(
            'id' => $this->db->escape($id),
            'trxid' => $this->db->escape($invoice),
        );
        $datawhere2 = array(
            'id' => ($id),
            'trxid' => ($invoice),
            'status' => ('WAITING'),
        );
        $kondisi['where'] = $datawhere;
        $getipgdata = $this->infaqmdl->custom_query("nb_trans_ipg", $kondisi)->result();
        if (!empty($getipgdata)) {
            $target_table = $getipgdata[0]->target_table;
            $no_transaksi = $getipgdata[0]->trxid;;
            $statustrx = $getipgdata[0]->status;
            if ($statustrx == 'WAITING') {
                $wheretrx = array('no_transaksi' => $no_transaksi);
                $updatetrx = array('status' => 'cancel', 'edtd_at' => date('Y-m-d H:i:s'));
                $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                $this->infaqmdl->custom_update("nb_trans_ipg", $dataupd, $datawhere2);
            }
        }
        $data = $this->_data;
        $data['title'] = $title;
        $data['trxid'] = $no_transaksi;
        $this->parser->parse("index", $data);
//        exit;
    }

    function ipgreturnold($id = '')
    {

        $idipg = Fhhlib::uuidv4Nopad();
        $insipgtrx = array('id' => $idipg,
            'request_json' => json_encode($_REQUEST),
            'id_transipg' => $id,
            'jenis' => 'return',
            'created_ad' => date('Y-m-d H:i:s'),
        );
        $this->infaqmdl->custom_insert("nb_trans_ipg_log", $insipgtrx);
        $dataupdate = array('rescallback_json' => json_encode($_REQUEST));
        $datawhere = array('id' => $id);
        $this->infaqmdl->custom_update('nb_trans_ipg', $dataupdate, $datawhere);
        $title = 'Transaksi Return';
        $no_transaksi = $id;
        $dataupd = array(
            'status' => 'RETURN',
            'response_get' => !empty($_GET) ? json_encode($_GET) : '',
            'response_post' => !empty($_POST) ? json_encode($_POST) : '',
            'updated_ad' => date('Y-m-d H:i:s'),
        );
        $invoice = !empty($_REQUEST['invoice']) ? $_REQUEST['invoice'] : '';
        $datawhere = array(
            'id' => $this->db->escape($id),
            'trxid' => $this->db->escape($invoice),
        );
        $datawhere2 = array(
            'id' => ($id),
            'trxid' => ($invoice),
            'status' => ('WAITING'),
        );
        $kondisi['where'] = $datawhere;
        $getipgdata = $this->infaqmdl->custom_query("nb_trans_ipg", $kondisi)->result();
        if (!empty($getipgdata)) {
            $target_table = $getipgdata[0]->target_table;
            $no_transaksi = $getipgdata[0]->trxid;;
            $statustrx = $getipgdata[0]->status;
            if ($statustrx == 'WAITING') {
                $wheretrx = array('no_transaksi' => $no_transaksi);
                $updatetrx = array('status' => 'cancel', 'edtd_at' => date('Y-m-d H:i:s'));
                $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                $this->infaqmdl->custom_update("nb_trans_ipg", $dataupd, $datawhere2);
            }
        }
        $data = $this->_data;
        $data['title'] = $title;
        $data['trxid'] = $no_transaksi;
        $this->parser->parse("index", $data);
        $data['title'] = $title;
    }

    public static function uuidv4()
    {
        return time() . "-" . sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),

                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),

                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,

                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,

                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
            );
    }

    function responsee2pay()
    {
        if ($_POST) {
            $tgl_skr = date('Y-m-d H:i:s');
            $msg_post = $_POST;
            $jsonar = $_POST;
            $wherecari = array('where' => ['id' => $this->db->escape('E2PAY')]);

            $getpartner = $this->infaqmdl->custom_query('nb_partner_ipg', $wherecari)->result();
            $signter = Fhhlib::E2Pay_signature($getpartner[0]->key.$jsonar['MerchantCode'].$jsonar['PaymentId'].$jsonar['RefNo'].$jsonar['Amount'].$jsonar['Currency'].$jsonar['Status']);
            $title = 'Transaksi Failed';
            if($signter != $jsonar['Signature']){
                $data = $this->_data;
                $data['title'] = $title;
                $data['trxid'] = $jsonar['RefNo'];
                return $this->parser->parse("index", $data);
                exit;
            }
            $datapost = json_encode($_POST);
            $insipgtrx = array('id' => Fhhlib::uuidv4(),
                'response_json' => $datapost,
                'typeres' => 'POST',
                'created_ad' => date('Y-m-d H:i:s'),
            );
            $this->infaqmdl->custom_insert("nb_trans_e2pay_response", $insipgtrx);
            $invoice = $msg_post['RefNo'];
            $statuscodetrx = $msg_post['Status'];

            $dataupd = array(
                'status' => 'FAILED',
                'response_post' => !empty($_POST) ? json_encode($_POST) : '',
                'updated_ad' => $tgl_skr,
            );
            $updatetrx = array('status' => 'cancel', 'edtd_at' => $tgl_skr);
            if ($statuscodetrx == '1') {
                $dataupd = array(
                    'status' => 'SUCCESS',
                    'response_get' => !empty($_GET) ? json_encode($_GET) : '',
                    'response_post' => !empty($_POST) ? json_encode($_POST) : '',
                    'updated_ad' => $tgl_skr,
                );
                $title = 'Transaksi Sukses';
                $updatetrx = array('status' => 'verified', 'edtd_at' => $tgl_skr);
            }
            $datawhere = array(
                'trxid' => $this->db->escape($invoice),
            );
            $datawhere2 = array(
                'trxid' => ($invoice),
                'status' => ('WAITING'),
            );
            $kondisi['where'] = $datawhere;
            $getipgdata = $this->infaqmdl->custom_query("nb_trans_e2pay", $kondisi)->result();
            if (!empty($getipgdata)) {
                $target_table = $getipgdata[0]->target_table;
                $no_transaksi = $getipgdata[0]->trxid;
                $statustrx = $getipgdata[0]->status;
                if ($statustrx == 'WAITING') {
                    $this->db->trans_begin();
                    $wheretrx = ['no_transaksi' => ($no_transaksi)];
                    $wheretrx2 = array('where' => ['no_transaksi' => $this->db->escape($no_transaksi)]);

                    $gettrxcampaign = $this->infaqmdl->custom_query($target_table, $wheretrx2)->result();

                    if (!empty($gettrxcampaign)) {
                        if ($statuscodetrx == '1') {
                            $jenistrx = '';
                            $nopolisasyki = '';
                            $tgl_lahir = '';
                            $jntrx = 'TRX';
                            $amounts = 0;
                            $balanceamount = 0;
                            $prevBalance = 0;
                            if (!empty($gettrxcampaign[0]->jenistrx)) {

                                $whereprogram['where'] = array('kategoricode' => $this->db->escape($gettrxcampaign[0]->jenistrx));
                                $cariprog = $this->infaqmdl->custom_query('nb_trans_program_kategori', $whereprogram)->result();
                                $jenistrx = '';
                                if (!empty($cariprog)):
                                    $jenistrx = $cariprog[0]->kategoriname;
                                    $expl = explode("-", $gettrxcampaign[0]->jenistrx);
                                    if (count($expl) == 3 && $cariprog[0]->type == 'ASURANSI_PERJALANAN') {
                                        $tgl_lahir = $gettrxcampaign[0]->tgl_lahir;
                                        $dataisipolis['paket'] = $expl[2];
                                        $dataisipolis['trx_id'] = $gettrxcampaign[0]->no_transaksi;
                                        $dataisipolis['nik'] = $gettrxcampaign[0]->no_ktp;
                                        $dataisipolis['nama'] = $gettrxcampaign[0]->namaDonatur;
                                        $dataisipolis['tgl_lahir'] = $gettrxcampaign[0]->tgl_lahir;
                                        $dataisipolis['jenis_kelamin'] = $gettrxcampaign[0]->jk;
                                        $dataisipolis['email'] = $gettrxcampaign[0]->emailDonatur;
                                        $dataisipolis['no_ponsel'] = $gettrxcampaign[0]->telpDonatur;
                                        $dataisipolis['nominal'] = $gettrxcampaign[0]->nomuniq;
                                        $dataisipolis['masa_asuransi'] = $expl[1];
                                        $polisasyki = $this->konekpolis($dataisipolis);
                                        $polisasyki = json_decode($polisasyki, true);
                                        $nopolisasyki = !empty($polisasyki['data']['certificate_no']) ? $polisasyki['data']['certificate_no'] : '';
                                        $updatetrx['no_polis'] = $nopolisasyki;
                                    }
                                    if (count($expl) == 2 && $cariprog[0]->type == 'PPOB') {
                                        $postdata = ['grant_type' => 'password',
                                            'username' => 'mumuweb1', 'password' => 'mumuweb123'];
                                        $HEADER = ['authorization: Basic bG9jYWxkZXY6dDYxRklMNUJuV2U3Wmp5cDlncTNFWE1KaERZ',
                                            'content-type: application/x-www-form-urlencoded'];
                                        $postdata = http_build_query($postdata);
                                        $token = $this->_curl_exexc("http://35.240.184.113/am/mydomain/oauth/token", $postdata, 'POST', $HEADER);
                                        $token = json_decode($token, true);
                                        $access_token = $token['access_token'];
                                        $HEADER = ['authorization: Bearer ' . $access_token,
                                            'content-type: application/json'];
                                        $postdata = [
                                            'fsp' => 'multibiller-hike',
                                            'id' => 'mumuweb1',
                                            'props' => array(
                                                'callback' => base_url('trxipg/responseppob'),
                                                'prodId' => $expl[1],
                                                'destination' => $gettrxcampaign[0]->telpDonatur,
                                            ),
                                        ];
                                        $postdata = json_encode($postdata);
                                        $datapulsa = $this->_curl_exexc("http://34.87.70.137/gateway", $postdata, 'POST', $HEADER);
                                        $insdt = array(
                                            'id' => Fhhlib::uuidv4(),
                                            'request_json' => json_encode($postdata),
                                            'response_json' => $datapulsa,
                                            'created_ad' => $tgl_skr,
                                            'no_transaksi' => $gettrxcampaign[0]->no_transaksi
                                        );
                                        $this->infaqmdl->custom_insert("nb_trans_ppob_request", $insdt);
                                    }
                                endif;
                            }
                            if (!empty($gettrxcampaign[0]->campaign)) {
                                $wherecampaign2 = array('where' => array('id' => $gettrxcampaign[0]->campaign));
                                $wherecampaign = array('id' => $gettrxcampaign[0]->campaign);
                                $caricamp = $this->infaqmdl->custom_query('nb_campaign', $wherecampaign2)->result();
                                $totaldona = (float)$caricamp[0]->now + (float)$gettrxcampaign[0]->nomuniq;
                                $totalsumdona = (float)$caricamp[0]->sum_donatur + 1;
                                $updatetrxcamp = array('now' => $totaldona, 'sum_donatur' => $totalsumdona);

                                $this->infaqmdl->custom_update('nb_campaign', $updatetrxcamp, $wherecampaign);
                                $jenistrx = "Donasi : " . $caricamp[0]->title;
                            }
                            $dataisi['invoice'] = $no_transaksi;
                            $dataisi['tgl_trx'] = $tgl_skr;
                            $dataisi['jenistrx'] = $jenistrx;
                            $dataisi['fullname'] = $gettrxcampaign[0]->namaDonatur;
                            $dataisi['senderName'] = 'MumuApps - System';
                            $dataisi['emailTo'] = $gettrxcampaign[0]->emailDonatur;
                            $dataisi['subject'] = 'Invoice Transaksi';
                            $dataisi['amount'] = $gettrxcampaign[0]->nomuniq;
                            $dataisi['no_polis'] = $nopolisasyki;
                            $dataisi['tgl_lahit'] = $tgl_lahir;
                            $dataisi['konten'] = 'trxipg/invoice1';
                            sendmail::SendMail($dataisi);
                        }
                        $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                        if($gettrxcampaign[0]->donatur != '0'){
                            $wheredepo = array('donatur' => $gettrxcampaign[0]->donatur);
                            $wheresaldo['where'] = array('donatur' => $this->db->escape($gettrxcampaign[0]->donatur));
                            $carisaldo = $this->infaqmdl->custom_query('nb_donaturs_saldo', $wheresaldo)->result();
                            $amounts = $gettrxcampaign[0]->nomuniq;
                            $prevBalance = $carisaldo[0]->balanceAmount;
                            if (!empty($gettrxcampaign[0]->jenistrx)&& ($gettrxcampaign[0]->jenistrx == 'deposit')) {
                                $balanceamount = $prevBalance + $amounts;
                                $updatedepo = array('prevBalance' => $prevBalance, 'balanceamount' => $balanceamount, 'edtd_at' => date('Y-m-d H:i:s'));
                                $this->infaqmdl->custom_update('nb_donaturs_saldo', $updatedepo, $wheredepo);
                                $jntrx = 'DEPO';
                            }
//                            else{
//                                $balanceamount = $prevBalance - $carisaldo[0]->balanceAmount;
//                                $updatedepo = array('prevBalance' => $prevBalance, 'balanceamount' => $balanceamount, 'edtd_at' => date('Y-m-d H:i:s'));
//                                $this->infaqmdl->custom_update('nb_donaturs_saldo', $updatedepo, $wheredepo);
//                                $jntrx = 'TRX';
//                            }
//                            $insdt = array(
//                                'donaturs' => $gettrxcampaign[0]->donatur,
//                                'jenistrx' => $jntrx,
//                                'id_transaksi' =>  $gettrxcampaign[0]->no_transaksi,
//                                'prevBalance' => $prevBalance,
//                                'amount' => $amounts,
//                                'balance' =>$balanceamount,
//                                'crtd_at' => date("Y-m-d H:i:s"),
//                                'edtd_at' => date("Y-m-d H:i:s"),
//                            );
//                            $this->infaqmdl->custom_insert("nb_donaturs_trx", $insdt);
                        }
                    }
                    $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                    $this->infaqmdl->custom_update("nb_trans_e2pay", $dataupd, $datawhere2);

                    $this->db->trans_commit();
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        die("ERROR: INSERTING DATA, PLEASE CONTACT ADMINISTRATOR WEB");
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            $data = $this->_data;
            $data['title'] = $title;
            $data['trxid'] = $no_transaksi;
            $this->parser->parse("index", $data);
        }
//        if ($_GET) {
//            $datapost = json_encode($_GET);
//            $insipgtrx = array('id' => Fhhlib::uuidv4(),
//                'response_json' => $datapost,
//                'typeres' => 'GET',
//                'created_ad' => date('Y-m-d H:i:s'),
//            );
//            $this->infaqmdl->custom_insert("nb_trans_e2pay_response", $insipgtrx);
//        }
//        print_r($_REQUEST);
//        exit;
    }

    function backende2pay()
    {

        if ($_POST) {
            $tgl_skr = date('Y-m-d H:i:s');
            $msg_post = $_POST;
            $jsonar = $_POST;
            $wherecari = array('where' => ['id' => $this->db->escape('E2PAY')]);

            $getpartner = $this->infaqmdl->custom_query('nb_partner_ipg', $wherecari)->result();
            $signter = Fhhlib::E2Pay_signature($getpartner[0]->key.$jsonar['MerchantCode'].$jsonar['PaymentId'].$jsonar['RefNo'].$jsonar['Amount'].$jsonar['Currency'].$jsonar['Status']);
            $title = 'Transaksi Failed';
            if($signter != $jsonar['Signature']){
                $data = $this->_data;
                $data['title'] = $title;
                $data['trxid'] = $jsonar['RefNo'];
                return $this->parser->parse("index", $data);
                exit;
            }
            $datapost = json_encode($_POST);
            $insipgtrx = array('id' => Fhhlib::uuidv4(),
                'response_json' => $datapost,
                'typeres' => 'POST',
                'created_ad' => date('Y-m-d H:i:s'),
            );
            $this->infaqmdl->custom_insert("nb_trans_e2pay_response", $insipgtrx);
            $invoice = $msg_post['RefNo'];
            $statuscodetrx = $msg_post['Status'];

            $dataupd = array(
                'status' => 'FAILED',
                'response_post' => !empty($_POST) ? json_encode($_POST) : '',
                'updated_ad' => $tgl_skr,
            );
            $updatetrx = array('status' => 'cancel', 'edtd_at' => $tgl_skr);
            if ($statuscodetrx == '1') {
                $dataupd = array(
                    'status' => 'SUCCESS',
                    'response_get' => !empty($_GET) ? json_encode($_GET) : '',
                    'response_post' => !empty($_POST) ? json_encode($_POST) : '',
                    'updated_ad' => $tgl_skr,
                );
                $title = 'Transaksi Sukses';
                $updatetrx = array('status' => 'verified', 'edtd_at' => $tgl_skr);
            }
            $datawhere = array(
                'trxid' => $this->db->escape($invoice),
            );
            $datawhere2 = array(
                'trxid' => ($invoice),
                'status' => ('WAITING'),
            );
            $kondisi['where'] = $datawhere;
            $getipgdata = $this->infaqmdl->custom_query("nb_trans_e2pay", $kondisi)->result();
            if (!empty($getipgdata)) {
                $target_table = $getipgdata[0]->target_table;
                $no_transaksi = $getipgdata[0]->trxid;
                $statustrx = $getipgdata[0]->status;
                if ($statustrx == 'WAITING') {
                    $this->db->trans_begin();
                    $wheretrx = ['no_transaksi' => ($no_transaksi)];
                    $wheretrx2 = array('where' => ['no_transaksi' => $this->db->escape($no_transaksi)]);

                    $gettrxcampaign = $this->infaqmdl->custom_query($target_table, $wheretrx2)->result();

                    if (!empty($gettrxcampaign)) {
                        if ($statuscodetrx == '1') {
                            $jenistrx = '';
                            $nopolisasyki = '';
                            $tgl_lahir = '';
                            $jntrx = 'TRX';
                            $amounts = 0;
                            $balanceamount = 0;
                            $prevBalance = 0;
                            if (!empty($gettrxcampaign[0]->jenistrx)) {

                                $whereprogram['where'] = array('kategoricode' => $this->db->escape($gettrxcampaign[0]->jenistrx));
                                $cariprog = $this->infaqmdl->custom_query('nb_trans_program_kategori', $whereprogram)->result();
                                $jenistrx = '';
                                if (!empty($cariprog)):
                                    $jenistrx = $cariprog[0]->kategoriname;
                                    $expl = explode("-", $gettrxcampaign[0]->jenistrx);
                                    if (count($expl) == 3 && $cariprog[0]->type == 'ASURANSI_PERJALANAN') {
                                        $tgl_lahir = $gettrxcampaign[0]->tgl_lahir;
                                        $dataisipolis['paket'] = $expl[2];
                                        $dataisipolis['trx_id'] = $gettrxcampaign[0]->no_transaksi;
                                        $dataisipolis['nik'] = $gettrxcampaign[0]->no_ktp;
                                        $dataisipolis['nama'] = $gettrxcampaign[0]->namaDonatur;
                                        $dataisipolis['tgl_lahir'] = $gettrxcampaign[0]->tgl_lahir;
                                        $dataisipolis['jenis_kelamin'] = $gettrxcampaign[0]->jk;
                                        $dataisipolis['email'] = $gettrxcampaign[0]->emailDonatur;
                                        $dataisipolis['no_ponsel'] = $gettrxcampaign[0]->telpDonatur;
                                        $dataisipolis['nominal'] = $gettrxcampaign[0]->nomuniq;
                                        $dataisipolis['masa_asuransi'] = $expl[1];
                                        $polisasyki = $this->konekpolis($dataisipolis);
                                        $polisasyki = json_decode($polisasyki, true);
                                        $nopolisasyki = !empty($polisasyki['data']['certificate_no']) ? $polisasyki['data']['certificate_no'] : '';
                                        $updatetrx['no_polis'] = $nopolisasyki;
                                    }
                                    if (count($expl) == 2 && $cariprog[0]->type == 'PPOB') {
                                        $postdata = ['grant_type' => 'password',
                                            'username' => 'mumuweb1', 'password' => 'mumuweb123'];
                                        $HEADER = ['authorization: Basic bG9jYWxkZXY6dDYxRklMNUJuV2U3Wmp5cDlncTNFWE1KaERZ',
                                            'content-type: application/x-www-form-urlencoded'];
                                        $postdata = http_build_query($postdata);
                                        $token = $this->_curl_exexc("http://35.240.184.113/am/mydomain/oauth/token", $postdata, 'POST', $HEADER);
                                        $token = json_decode($token, true);
                                        $access_token = $token['access_token'];
                                        $HEADER = ['authorization: Bearer ' . $access_token,
                                            'content-type: application/json'];
                                        $postdata = [
                                            'fsp' => 'multibiller-hike',
                                            'id' => 'mumuweb1',
                                            'props' => array(
                                                'callback' => base_url('trxipg/responseppob'),
                                                'prodId' => $expl[1],
                                                'destination' => $gettrxcampaign[0]->telpDonatur,
                                            ),
                                        ];
                                        $postdata = json_encode($postdata);
                                        $datapulsa = $this->_curl_exexc("http://34.87.70.137/gateway", $postdata, 'POST', $HEADER);
                                        $insdt = array(
                                            'id' => Fhhlib::uuidv4(),
                                            'request_json' => json_encode($postdata),
                                            'response_json' => $datapulsa,
                                            'created_ad' => $tgl_skr,
                                            'no_transaksi' => $gettrxcampaign[0]->no_transaksi
                                        );
                                        $this->infaqmdl->custom_insert("nb_trans_ppob_request", $insdt);
                                    }
                                endif;
                            }
                            if (!empty($gettrxcampaign[0]->campaign)) {
                                $wherecampaign2 = array('where' => array('id' => $gettrxcampaign[0]->campaign));
                                $wherecampaign = array('id' => $gettrxcampaign[0]->campaign);
                                $caricamp = $this->infaqmdl->custom_query('nb_campaign', $wherecampaign2)->result();
                                $totaldona = (float)$caricamp[0]->now + (float)$gettrxcampaign[0]->nomuniq;
                                $totalsumdona = (float)$caricamp[0]->sum_donatur + 1;
                                $updatetrxcamp = array('now' => $totaldona, 'sum_donatur' => $totalsumdona);

                                $this->infaqmdl->custom_update('nb_campaign', $updatetrxcamp, $wherecampaign);
                                $jenistrx = "Donasi : " . $caricamp[0]->title;
                            }
                            $dataisi['invoice'] = $no_transaksi;
                            $dataisi['tgl_trx'] = $tgl_skr;
                            $dataisi['jenistrx'] = $jenistrx;
                            $dataisi['fullname'] = $gettrxcampaign[0]->namaDonatur;
                            $dataisi['senderName'] = 'MumuApps - System';
                            $dataisi['emailTo'] = $gettrxcampaign[0]->emailDonatur;
                            $dataisi['subject'] = 'Invoice Transaksi';
                            $dataisi['amount'] = $gettrxcampaign[0]->nomuniq;
                            $dataisi['no_polis'] = $nopolisasyki;
                            $dataisi['tgl_lahit'] = $tgl_lahir;
                            $dataisi['konten'] = 'trxipg/invoice1';
                            sendmail::SendMail($dataisi);
                        }
                        $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                        if($gettrxcampaign[0]->donatur != '0'){
                            $wheredepo = array('donatur' => $gettrxcampaign[0]->donatur);
                            $wheresaldo['where'] = array('donatur' => $this->db->escape($gettrxcampaign[0]->donatur));
                            $carisaldo = $this->infaqmdl->custom_query('nb_donaturs_saldo', $wheresaldo)->result();
                            $amounts = $gettrxcampaign[0]->nomuniq;
                            $prevBalance = $carisaldo[0]->balanceAmount;
                            if (!empty($gettrxcampaign[0]->jenistrx)&& ($gettrxcampaign[0]->jenistrx == 'deposit')) {
                                $balanceamount = $prevBalance + $amounts;
                                $updatedepo = array('prevBalance' => $prevBalance, 'balanceamount' => $balanceamount, 'edtd_at' => date('Y-m-d H:i:s'));
                                $this->infaqmdl->custom_update('nb_donaturs_saldo', $updatedepo, $wheredepo);
                                $jntrx = 'DEPO';
                            }
//                            else{
//                                $balanceamount = $prevBalance - $carisaldo[0]->balanceAmount;
//                                $updatedepo = array('prevBalance' => $prevBalance, 'balanceamount' => $balanceamount, 'edtd_at' => date('Y-m-d H:i:s'));
//                                $this->infaqmdl->custom_update('nb_donaturs_saldo', $updatedepo, $wheredepo);
//                                $jntrx = 'TRX';
//                            }
//                            $insdt = array(
//                                'donaturs' => $gettrxcampaign[0]->donatur,
//                                'jenistrx' => $jntrx,
//                                'id_transaksi' =>  $gettrxcampaign[0]->no_transaksi,
//                                'prevBalance' => $prevBalance,
//                                'amount' => $amounts,
//                                'balance' =>$balanceamount,
//                                'crtd_at' => date("Y-m-d H:i:s"),
//                                'edtd_at' => date("Y-m-d H:i:s"),
//                            );
//                            $this->infaqmdl->custom_insert("nb_donaturs_trx", $insdt);
                        }
                    }
                    $this->infaqmdl->custom_update($target_table, $updatetrx, $wheretrx);
                    $this->infaqmdl->custom_update("nb_trans_e2pay", $dataupd, $datawhere2);

                    $this->db->trans_commit();
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        die("ERROR: INSERTING DATA, PLEASE CONTACT ADMINISTRATOR WEB");
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            $data = $this->_data;
            $data['title'] = $title;
            $data['trxid'] = $no_transaksi;
//            $this->parser->parse("index", $data);
        }
        echo 'OK';
        exit;
    }

    function konekpolis($data = null)
    {
        $url = 'http://194.31.53.26/ddipg/public/asuransi/asyki';//prod
        $ch = curl_init();
        $awal = $data;
        $datapack = http_build_query($awal);
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            $datapack);
        // return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $output contains the output string
        $output = curl_exec($ch);
        // tutup curl
        curl_close($ch);
//        insertlog json
//        Yii::$app->db->createCommand()->insert('log_json', ['trx_id' => $data['trx_id'], 'res_json' => $output, 'send_json' => json_encode($data), 'created_at' => date("Y-m-d H:i:s")])->execute();
        // menampilkan hasil curl
        return $output;
    }


    private function _curl_exexc($url = '', $postdata = '', $METHOD = 'POST', $HEADER = [])
    {
//        $postdata = http_build_query($data);
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADER);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }

    function responseppob()
    {
        if ($_POST) {
            $datapost = json_encode($_POST);
            $insipgtrx = array('id' => Fhhlib::uuidv4(),
                'response_json' => $datapost,
                'created_ad' => date('Y-m-d H:i:s'),
            );
            $this->infaqmdl->custom_insert("nb_trans_ppob_response", $insipgtrx);
        }
        if ($_GET) {
            $datapost = json_encode($_GET);
            $insipgtrx = array('id' => Fhhlib::uuidv4(),
                'response_json' => $datapost,
                'created_ad' => date('Y-m-d H:i:s'),
            );
            $this->infaqmdl->custom_insert("nb_trans_ppob_response", $insipgtrx);
        }
    }

    function _koneksippob($data = [],$no_transaksi='')
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://core.mumuapps.id/ppob/b2b/transaksi",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $this->db->where('no_transaksi', $no_transaksi);
        $dataup=array('response_pay'=>$response);
        $this->db->update('nb_ppob_pay', $dataup);
        return $response;
    }
}

