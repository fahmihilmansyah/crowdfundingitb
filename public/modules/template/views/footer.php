
</div>
<!--<div class="bg-main footer-info" style="background-color: #A9A9A9;">-->
<!--    <div class="h5 uppercase title-font bold mb-25">-->
<!---->
<!--        <img src="--><?php //URI::baseURL(); ?><!--assets/images/logo/logomumu.png" width="100">-->
<!--    </div>-->
<!--    <div class="mb-20">-->
<!--        Philanthropy Building Lt. 2 Jl. Warung Jati Barat No. 14, Jatipadang Jakarta Selatan - 12450-->
<!--        <br/>-->
<!--       <span class="btn btn-primary" style="background: #1f7c4d; border-color: #1f7c4d;"> Member of Dompet Dhuafa <br/><i> No. Reg: 029/SK/MPZ-DD/III/2019</i></span>-->
<!--    </div>-->
<!--    <div>-->
<!--        <i class="icofont-phone"></i> 0217416050 <br>-->
<!--        <i class="icofont-envelope"></i> mail@mumuapps.id-->
<!--    </div>-->
<!--</div>-->
</div>
<div class="foot-menu">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <a href="<?php URI::baseURL(); ?>" class="active"><i class="icofont-home"></i></a>
            </div>
            <div class="col text-center">
<!--                <a href="#" id="footer-mid-menu" class="footer-mid-menu btn btn-lg"><span><img src="--><?php //URI::baseURL(); ?><!--assets/images/logo/logomumuh2es.png" width="55"></i></span></a>-->
<!--                <a href="#" id="close-footer-mid-menu" class="footer-mid-menu btn"><span><i class="icofont-close"></i></span></a>-->
                <a href="#" id="footer-mid-menu" class="footer-mid-menu">
                    <img src="<?php URI::baseURL(); ?>assets/mumuapps/img/logo-symbol.svg">
                </a>
                <a href="#" id="close-footer-mid-menu" class="footer-mid-menu"><i class="icofont-close"></i></a>
            </div>
            <div class="col text-center">
                <a href="#" id="nav-menu" style="    color: #09A59D;"><i class="icofont-navigation-menu"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="search-top">
    <div class="container">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart(base_url() . 'donasi', array('id' => 'fmsearch','class'=>"form-search-top"), $hidden_form);
        ?>
            <div class="form-group relative-box">
                <i class="icon-search icofont-search-1"></i>
                <a href="#" id="close-search-top"><i class="icofont-close"></i></a>

                <input type="text" name="searchdonasi"  class="form-control" placeholder="Cari Pendanaan...">
            </div>
            <?php
            echo form_close();
            ?>
    </div>
</div>

<div class="nav-menu nav-menu2" style="overflow-y:scroll;
    overflow-x:hidden;">
    <div class="container">
        <div>
            <span class="float-left" id="hjudul"><b>Metode Pembayaran</b></span>
        <div class="text-right mb-20">
            <a href="#" id="close-nav-menu2"><i class="icofont-close"></i></a>
            <hr/>
        </div>
        </div>
        <div id="settmpl"></div>
    </div>
</div>
<div class="nav-menu">
    <div class="container">
        <div class="text-right mb-20">
            <a href="#" id="close-nav-menu"><i class="icofont-close"></i></a>
        </div>

        <?php
        if(!$_loginstatus)
        {
        ?>
        <div class="mb-25 medium">Masuk atau daftar untuk menggunakan fitur lainnya.</div>
        <div class="row mb-30">
            <div class="col">
                <a href="<?php URI::baseURL(); ?>register" class="btn btn-link btn-block"><span>Daftar</span></a>
            </div>
            <div class="col">
                <a href="<?php URI::baseURL(); ?>login" class="btn btn-main btn-block"><span>Masuk</span></a>
            </div>
        </div>
        <?php }?>
        <ul class="nav-menu-list medium">
            <?php
            if($_loginstatus)
            {
            ?>
            <li>
                <div class="row">
                    <div class="col">
                        <a href="<?php URI::baseURL(); ?>donatur"><i class="icofont-gear mr-5"></i> Dashboard Donatur</a>
                    </div>
                    <div class="col">
                        <a href="<?php URI::baseURL(); ?>logout"><i class="icofont-logout mr-5"></i> Logout</a></div>
                </div>

            </li>
            <?php }?>
            <li>
                <a href="#"><i class="icofont-file-alt mr-5"></i> Syarat & Ketentuan</a>
            </li>
            <li>
                <a href="#"><i class="icofont-question mr-5"></i> Bantuan</a>
            </li>
            <li>
                <a href="#"><i class="icofont-globe mr-5"></i> Pilih Bahasa</a>
            </li>
        </ul>
    </div>
</div>

<div class="footer-mid-menu-content">
    <div class="full-height container relative-box">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">

                        <ul class="nav-menu-list medium">
                            <li>
                                <a href="<?php URI::baseURL(); ?>zakat/zakat">
                                    <img src="<?php echo base_url().'assets/images/icons/july update - icon-11.png' ?>" width="25">
                                    Zakat</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>donasi">
                                    <img src="<?php echo base_url().'assets/images/icons/july update - icon-12.png' ?>" width="25"> Donasi</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>wakaf/wakaf">
                                    <img src="<?php echo base_url().'assets/images/icons/july update - icon-13.png' ?>" width="25">
                                    Wakaf</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>kurbanpartner/homepage">
                                    <img src="<?php echo base_url().'assets/images/icons/july update - icon-14.png' ?>" width="25"> Kurban</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>beasiswa"><img src="<?php echo base_url().'assets/images/icons/july update - icon-15.png' ?>" width="25"> Yatim</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>aqiqah/aqiqah">
                                    <img src="<?php echo base_url().'assets/images/icons/july update - icon-19.png' ?>" width="25"> Akikah</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>investasi"><img src="<?php echo base_url().'assets/images/icons/july update - icon-17.png' ?>" width="25"> Investasi</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6">

                        <ul class="nav-menu-list medium">
                            <li>
                                <a href="<?php URI::baseURL(); ?>asuransi"><img src="<?php echo base_url().'assets/images/icons/july update - icon-16.png' ?>" width="25"> Proteksi</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>/ppob/multipayment/pulsa">
                                    <img src="<?php echo base_url().'assets/images/icons/july update - icon-18.png' ?>" width="25"> Pulsa</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>ppob/multipayment/jastelkom">
                                    <i class="icofont icofont-phone"></i> Telkom</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>ppob/multipayment/telhallo">
                                    <i class="icofont icofont-phone"></i> Telco</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>ppob/multipayment/plnprabayar">
                                    <i class="icofont icofont-thunder-light"></i> PLN Prepaid</a>
                            </li>
                            <li>
                                <a href="<?php URI::baseURL(); ?>ppob/multipayment/plnppascabayar">
                                    <i class="icofont icofont-thunder-light"></i> PLN Postpaid</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<!--<script src="--><?php //URI::baseURL(); ?><!--assets/mganeshabisa/vendor/jquery/jquery.min.js"></script>-->
<!--<script src="--><?php //URI::baseURL(); ?><!--assets/mganeshabisa/vendor/owl-carousel/owl.carousel.min.js"></script>-->
<!--<script src="--><?php //URI::baseURL(); ?><!--assets/mganeshabisa/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>-->
<script src="<?php URI::baseURL(); ?>assets/mganeshabisa/js/rjcustoms.js"></script>

<script type="text/javascript">

    $( "#close-nav-menu2" ).click(function() {
        $('.nav-menu2').removeClass('open');
    });
    $( "#pilbayar" ).click(function() {
        $('.nav-menu2').addClass('open');
    });
    $('.dana-populer').owlCarousel({
        stagePadding: 40,
        loop: true,
        margin: 12,
        nav: false,
        responsive: {
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
</script>

</body>

</html>