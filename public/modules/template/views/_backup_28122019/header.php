<!DOCTYPE html>

<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <!-- Meta, title, CSS, favicons, etc. -->

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?php echo $_appTitle; ?> </title>
  <link rel="icon"  type="image/png" href="<?php URI::baseURL(); ?>assets/images/logo/favicon.png" />

  <?php

    echo $_cssHeader;

  ?>

</head>
<body>

  <!-- NAVIGASI -->

  <div class="navbar">
    <nav class="navbar navbar-default navbar-fixed-top navbar-margin">
      <div class="container-fluid">
      <div class="row">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php URI::baseURL() ?>">
            <img src="<?php URI::baseURL() ?>assets/images/logo/logo.png" class="navbar-logo">
          </a>

          <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <ul id="navbarCollapse" class="nav navbar-nav navbar-right navbar-ul collapse navbar-collapse">
          <li><a class="menu" href="<?php URI::baseURL(); ?>program/zakat#kalkulator_zakat"><b>Kalkulator Zakat</b></a></li>
          <li><a class="menu" href="<?php URI::baseURL(); ?>program"><b>Program</b></a></li>
          <li><a class="menu" href="<?php URI::baseURL(); ?>donasi"><b>Donasi</b></a></li>
          <li><a class="menu" href="<?php URI::baseURL(); ?>news/list"><b>Artikel</b></a></li>
          <li class="dropdown mega-dropdown hidden-xs">
            <a href="javascript:void(0)" class="dropdown-toggle menu navbar-pipe" data-toggle="dropdown">
              <span id="isearch" class="fa fa-search navbar-gi"></span>
            </a>        

            <ul id="landing-search" class="dropdown-menu mega-dropdown-menu mega-arrow">
              <div>
                  <center>

                  <div class="center">
                    <?php
                      $hidden_form = array('id' => !empty($id) ? $id : '');
                      echo form_open_multipart(base_url() . 'donasi', array('id' => 'fmsearch'), $hidden_form);
                    ?>
                    <table style="width:40%;">
                      <tr>
                        <td style="padding-right:5px">
                          <input 
                              type="text" 
                              name="searchdonasi" 
                              id="search" 
                              placeholder="Search..." 
                              class="form-control"
                              value="" >
                        </td>
                        <td width="1">
                          <input 
                            type="submit" 
                            name="search-submit" 
                            id="search-submit" 
                            class="btn btn-success" 
                            value="Search"
                            style="" 
                          >
                        </td>
                      </tr>
                    </table>
                    <?php 
                      echo form_close();
                    ?>
                  </div>
                </center>
              </div>
            </ul>       
          </li>

          <?php
            if(!$_loginstatus)
            {
          ?>

          <li class="hidden-lg hidden-sm hidden-md"><a class="menu red-txt bold" href="<?php URI::baseURL(); ?>login"><b>Login</b></a></li>
		  <li class="dropdown hidden-xs">
              <a href="javascript:void(0)" class="dropdown-toggle menu" data-toggle="dropdown"><b>Log In</b></a>
              <ul id="landing-login" class="dropdown-menu navbar-login-wrapper arrow-menu" role="menu">
                  <div class="navbar-login-box">
                      <div class="mb10">
                        <center>
                          <span style="font-size:15px; color:#fff">Login untuk dapat <br/> mengakses semua fitur YDSF</span>
                        </center>
                      </div>

                      <div>

                        <?php
                          $hidden_form = array('id' => !empty($id) ? $id : '');
                          echo form_open_multipart($_lgn_default, array('id' => 'lg_default'), $hidden_form);
                        ?>
                          <div class="form-group">
                              <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="email" value="" autocomplete="off">
                          </div>

                          <div class="form-group">
                              <input type="password" name="pwd" id="pwd" tabindex="2" class="form-control" placeholder="password" autocomplete="off">
                          </div>

                          <div class="form-group">
                              <div class="row">
                                  <div class="col-xs-5 navbar-login-fsmall">
                                    <label for="remember"> 
                                      <input type="checkbox" tabindex="3" name="remember" id="remember">
                                      <span style="position:relative; top:-2px">Ingat Saya</span>
                                    </label>
                                  </div>
                                  <div class="col-xs-6 pull-right navbar-login-fsmall right">
                                    <a href="http://phpoll.com/recover" tabindex="5" class="forgot-password">Lupa Password?</a>
                                  </div>
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="row">
                                  <div class="col-lg-12 navbar-login-fsmall">
                                      <div class="text-center">
                                      <input 
                                        type="submit" 
                                        name="login-submit" 
                                        id="login-submit" 
                                        tabindex="4" 
                                        class="form-control btn btn-success" 
                                        value="Log In"
                                      >
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="form-group">
                            <hr style="z-index:9000">
                            <span class="navbar-or"><center>OR</center></span>
                          </div>
                      <?php echo form_close(); ?>

                      <?php
                        $hidden_form = array('id' => !empty($id) ? $id : '');
                        echo form_open_multipart($_lgn_facebook, array('id' => 'lg_fb'), $hidden_form);
                      ?>    
                          <div class="form-group">
                              <div class="row">
                                  <div class="col-lg-12 navbar-login-fsmall">
                                      <div class="text-center">
                                        <button class="btn btn-block btn-social btn-facebook">
                                          <span class="fa fa-facebook"></span> <center>Login dengan Facebook</center>
                                        </button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      <?php echo form_close(); ?>

                      <?php
                        $hidden_form = array('id' => !empty($id) ? $id : '');
                        echo form_open_multipart($_lgn_google, array('id' => 'lg_google'), $hidden_form);
                      ?>    
                          <div class="form-group">
                              <div class="row">
                                  <div class="col-lg-12 navbar-login-fsmall">
                                      <div class="text-center">
                                        <button class="btn btn-block btn-social btn-google">
                                          <span class="fa fa-google"></span> <center>Login dengan Google+</center>
                                        </button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      <?php echo form_close(); ?>
                      <div>
                  </div>
              </ul>
          </li>
          <li><a class="menu" href="<?php URI::baseURL(); ?>register"><b>Daftar</b></a></li>
          <?php
            }else{
          ?>
          <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle menu" data-toggle="dropdown">
                <div class="alogin" style="position: relative; top: -20px !important;">

                    <?php
                    $ses = $this->session->userdata(LGI_KEY . "login_info");
                    $indexs = $this->db->query('select nb_donaturs.picture_url, nb_donaturs_d.img from nb_donaturs LEFT JOIN nb_donaturs_d on nb_donaturs.id = nb_donaturs_d.donatur where nb_donaturs.id = '.$ses['userid'])->result();
                    if(!empty($indexs[0]->img)):?>
                        &nbsp;<img src="<?php echo base_url('assets/images/donatur/profile/'.$indexs[0]->img);?>" width="40px" class="img-thumbnail" style="position: relative;">
                    <?php elseif(!empty($indexs[0]->picture_url)): ?>
                        <img src="<?php echo $indexs[0]->picture_url ;?>" class="img-thumbnail" width="40px">
                    <?php else: ?>
                        <span class="fa fa-user"></span>
                    <?php endif; ?>
                    <b> <?php echo $login_userdata["login_fname"] . " " . $login_userdata["login_lname"]; ?></b>
                </div>
              </a>
              <ul id="landing-alogin" class="dropdown-menu alogin-wrapper arrow-menu ar-alo" role="alogin">
                  <div class="alogin-konten">
                    <ul class="alogin-list">
                      <li><a href="<?php URI::baseURL(); ?>myprofile"><img src="<?php URI::baseURL(); ?>assets/images/icon/iconprofile.png" style="padding-right:10px">Edit Profile</a></li>
                      <li><a href="<?php URI::baseURL(); ?>mydonation"><img src="<?php URI::baseURL(); ?>assets/images/icon/icondonasi.png" style="padding-right:10px">Donasi Saya</a></li>
                      <li><a href="<?php URI::baseURL(); ?>mywallet"><img src="<?php URI::baseURL(); ?>assets/images/icon/icondompet.png" style="padding-right:10px">Dompet Aksi Baik</a></li>
                      <li><a href="<?php URI::baseURL(); ?>mypoin"><img src="<?php URI::baseURL(); ?>assets/images/icon/iconpoin.png" style="padding-right:10px">Poin Aksi Baik</a></li>
                      <li><hr/></li>
                      <li><a href="<?php URI::baseURL(); ?>logout"><img src="<?php URI::baseURL(); ?>assets/images/icon/iconkeluar.png" style="padding-right:10px">Keluar</a></li>
                    </ul>
                  </div>
              </ul>
          </li>
          <?php
            }
          ?>
        </ul>
      </div>
      </div>
    </nav>
  </div>



  <div id="main-konten">





