

  	</div>

	<div id="footer">
		<div class="container-fluid footer">
		<div class="row">
		<div id="notifications"><?php echo $this->session->flashdata('msg'); ?></div>
			<div class="footer-subscribe">
				<div class="row col-md-4" style="text-align:right; margin-left:20px; margin-right:5px;">
					<p style="font-size:15px; color:#fff; line-height:35px; margin-bottom:5px">BERLANGGANAN BERITA DENGAN YDSF</p>
				</div>

				
				<form action="<?php echo site_url()?>home/addsub" method="post">
					<div class="row col-md-4" style="text-align:center; margin-right:5px; margin-bottom:10px">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="email" 
                			name="email" 
                			id="email" 
                			class="form-control input-black" 
                			placeholder="Your Email Address Here" 
                			value="" 
                			autocomplete="off">
					</div>
					<div class="row col-md-4 mb20" style="text-align:left;">
					<button class="btn btn-success sm-bold" type="submit">BERLANGGANAN</button>
					</div>
				</form>
				
				<!--<div class="row col-md-4" style="text-align:center; margin-right:5px; margin-bottom:10px">
                	<input type="text" 
                			name="username" 
                			id="username" 
                			class="form-control input-black" 
                			placeholder="Your Email Address Here" 
                			value="" 
                			autocomplete="off">
				</div>

				<div class="row col-md-4 mb20" style="text-align:left;">
					<a href="javascript:void(0)" class="btn btn-success sm-bold">BERLANGGANAN</a>
				</div>-->
			</div>

			<div class="container footer-konten">
				<div class="row col-sm-6 col-md-4 mb20">
					<img 
						style="position:relative; left:-10px; margin-bottom:10px;" 
						src="<?php URI::baseURL(); ?>assets/images/logo/logo-white.png">

					<p style="color:#b5b5b5; font-size:15px; padding:5px">
						Follow us
					</p>

					<div class="icon" style="margin-top:10px">
						<a target="_blank" href="https://www.facebook.com/BerzakatID/"><img 
							src="<?php URI::baseURL(); ?>assets/images/icon/fb.png" class="pull-left" 
							style="position:relative; left:-15px"></a>

						<a target="_blank" href="https://www.instagram.com/berzakatid/">
						<img 
							src="<?php URI::baseURL(); ?>assets/images/icon/ig.png" class="pull-left"
							style="position:relative; left:-15px"></a>

						<a target="_blank" href="https://www.twitter.com/BerzakatID/">
						<img 
							src="<?php URI::baseURL(); ?>assets/images/icon/twitter.png" class="pull-left"						
							style="position:relative; left:-15px"></a>
					</div>
				</div>

				<div class="row col-sm-6 col-md-3 mb20">
						<span class="sm-bold footer-block-title">TENTANG KAMI</span>
						<ul class="footer-link">
						<?php
							if(!empty($_program_link)){
								foreach($_program_link as $row){
						?>
							<li>
								<a href="<?php echo base_url() . $row['link']; ?>"><?php echo ucwords($row['name']); ?></a>
							</li>
						<?php
								}
							}else{
						?>
							<li><a href="<?php URI::baseURL(); ?>aboutus">Profil</a></li>
							<li><a href="<?php URI::baseURL(); ?>visimisi">Visi & Misi</a></li>
							<li><a href="<?php URI::baseURL(); ?>sekilas">Sekilas YDSF</a></li>
							<li><a href="<?php URI::baseURL(); ?>legalitas">Legalitas</a></li>
							<li><a href="<?php URI::baseURL(); ?>pengurus">Susunan Pengurus</a></li>
						<?php
							}
						?>
						</ul>
				</div>

				<div class="row col-sm-6 col-md-3 mb20">
						<span class="sm-bold footer-block-title">PROGRAM</span>
						<ul class="footer-link">
						<?php
							if(!empty($_ttgkm_link)){
								foreach($_ttgkm_link as $row){
						?>
							<li>
								<a href="<?php echo base_url() . $row['link']; ?>"><?php echo ucwords($row['name']); ?></a>
							</li>
						<?php
								}
							}else{
						?>
							<li><a href="<?php URI::baseURL(); ?>program">Program</a></li>
							<li><a href="<?php URI::baseURL(); ?>donasi">Donasi</a></li>
							<li><a href="<?php URI::baseURL(); ?>faq">FAQ</a></li>
							<li><a href="<?php URI::baseURL(); ?>#">Syarat dan Ketentuan</a></li>
							<li><a href="<?php URI::baseURL(); ?>#">Kebijakan Privasi</a></li>
						<?php
							}
						?>
						</ul>
				</div>
				
				<div class="row col-sm-6 col-md-3 mb20">
						<span class="sm-bold footer-block-title">KONTAK KAMI</span>
						<p class="footer-address">
							Yayasan Dana Sosial Al-Falah Jakarta<br/>
							Jl. Siaga Raya No 40, Pejaten Barat, Pasar Minggu Jakarta Selatan 12510
						</p>

						<p class="footer-address mb20">
							Telpon: (021) 7945 971<br/>
							Faximile: (021) 7945 972
						</p>
				</div>
			</div>
			<div class="footer-copyright">
				<p class="sm-bold">Copyright &copy; 2017 Yayasan Dana Sosial Al-Falah. All Right Reserved.</p>
			</div>
		</div>
		</div>
	</div>

    <?php
      echo $_jsHeader;
    ?>

  </body>
</html>

<script type="text/javascript">
	$(function () { 
		$('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
	});  

	$(".progress-bar").each(function(){
		each_bar_width = $(this).attr('aria-valuenow');
		$(this).width(each_bar_width + '%');
	});

	$(document).ready(function(){
		assign_bootstrap_mode();
		$(window).resize(function() {
		    assign_bootstrap_mode();
		});
	});

	function assign_bootstrap_mode() {
	    width = $( window ).width();
	    var mode 	 = '';
	    var variabel = [
	    				['#landing-search', 'hidden-xs'],
	    				['#landing-login','hidden-xs'],
	    				['ul#navbarCollapse>li>a','menu-responsive'],
	    				['navbarCollapse','padd20'],
	    				['isearch','navbar-gi-rm5'],
	    				['div.alogin','alogin-top'],
	    				['div.alogin-konten','hidden-xs'],
	    				['div.alogin-konten','hidden-xs'],
	    				['ul#tab-personal > li','wauto'],
	    				['ul#tabs-campaigns > li', 'wauto'],
	    				['div.profile-tab-foto','cposition']
	    			];

	    			

	    if (width<768) {
	    	removeAddClass(variabel, 'add');
	    }else{
	    	removeAddClass(variabel, 'remove');
	    }
	}



	function removeAddClass(varclass, type)
	{
		for(i=0; i<varclass.length; i++)
		{
			if(type == "add")
			{
				$(varclass[i][0]).addClass(varclass[i][1]);
			}

			if(type == "remove")
			{
				$(varclass[i][0]).removeClass(varclass[i][1]);
			}
		}
	}
	
<!--Start of Tawk.to Script-->
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5923e6b18028bb73270474ac/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
<!--End of Tawk.to Script-->

$('#notifications').slideDown('slow').delay(3000).slideUp('slow');
 $( ".fmdonasi" ).submit(function( event ) {
        var val = $( ".donationNominal" ).val();
   
        if(isNaN(val)){
            // sweetAlert("Oops...", "Harus Angka", "error");
            // alert("Harus Number");
            notifcoy("Maaf...", "Harus Angka","info");
            event.preventDefault();
        }
        if ( parseInt(val) < 29999 || val == "") {
            // sweetAlert("Oops...", "Nilai sedakah minimal Rp 30.000", "error");
            notifcoy("Maaf...", "Nilai sedakah minimal Rp 30.000","info");
            // alert("Nilai sedakah minimal Rp 30.000");
            event.preventDefault();
        }
    });

function notifcoy(judul,isi, tipe){
    swal({
        title: judul,
        text: isi,
        // type: tipe,
        imageUrl:"http://berzakat.id/assets/images/errorimg.png",
        confirmButtonColor: "#5cb85c",
        confirmButtonText: "Mengerti",
    });
}

</script>

