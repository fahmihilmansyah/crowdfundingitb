<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{META_DESCRIPTION}">
    <meta name="author" content="{META_AUTHOR}">
    <meta name="title" content="MumuApps.id | {META_TITLE}">
    <meta name="keyword" content="{META_KEYWORD}">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="<?php echo base_url() ?>">
    <meta property="og:url" content="{META_URL}">
    <meta property="og:title" content="{META_TIPE} - {META_TITLE}">
    <meta property="og:description" content="{META_DESCRIPTION}">
    <meta property="og:image" content="{META_IMG}">
    <title>MumuApps.id | {META_TITLE}</title>
    <meta name="google-site-verification" content="z25T2qzRkrLtQViGARxao473Yf7WT5Qi63d7QfcIf58" /><link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>/assets/icons/apple-touch-icon.png">

    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>/assets/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>/assets/icons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>/assets/icons/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url() ?>/assets/icons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '542214526701191');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=542214526701191&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Bootstrap core CSS -->
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/css/heroic-features.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/css/rjcustoms.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/owl-carousel/owl.theme.default.min.css" rel="stylesheet">


    <!-- Bootstrap core JavaScript -->
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/jquery/jquery.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mganeshabisa/js/rjcustoms.js"></script>


</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-main fixed-top">
    <div class="container">
        <div class="mr-auto">
<!--            <a href="#" class="toggle-menu" id="toggle-menu"><i class="icofont-navigation-menu text-white"></i></a>-->
            <div class="inline-block title-font semibold ml-10" href="#">
               <a href="<?php echo base_url(); ?>"><img src="<?php URI::baseURL(); ?>assets/images/logo/logomumu.png" width="100"></a>
            </div>
        </div>
        <div class="ml-auto">
            <a href="#" id="search-top"><i class="icofont-search-1"></i></a>
        </div>
    </div>
</nav>


<div class="container">