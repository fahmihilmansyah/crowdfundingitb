<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="title" content="<?php echo $_appTitle; ?>">

    <title><?php echo $_appTitle; ?> </title>
    <meta name="google-site-verification" content="z25T2qzRkrLtQViGARxao473Yf7WT5Qi63d7QfcIf58" />

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>/assets/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>/assets/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>/assets/icons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>/assets/icons/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url() ?>/assets/icons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap core CSS -->
    <link href="<?php URI::baseURL(); ?>assets/mumuapps/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php URI::baseURL(); ?>assets/mumuapps/css/heroic-features.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mumuapps/css/rjcustoms.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mumuapps/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mumuapps/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mumuapps/vendor/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/mumuapps/vendor/owl-carousel/owl.theme.default.min.css" rel="stylesheet">


    <!-- Bootstrap core JavaScript -->
    <script src="<?php URI::baseURL(); ?>assets/mumuapps/vendor/jquery/jquery.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mumuapps/vendor/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mumuapps/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/mumuapps/js/rjcustoms.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '542214526701191');
        fbq('track', 'PageView');

        // $.ajaxSetup({
        //     beforeSend: function() {
        //         console.log('haloo');
        //         // show progress spinner
        //     },
        //     complete: function() {
        //         console.log('haloo complete');
        //         // hide progress spinner
        //     }
        // });

        function detectajax() {
            var proxied = window.XMLHttpRequest.prototype.send;
            var getel = document.getElementsByClassName('modalscren');
            window.XMLHttpRequest.prototype.send = function() {
                // console.log(getel);
                getel[0].classList.add("loadings");
                // getel[0].classList.add("loadingss");
                // console.log( arguments );
                //Here is where you can add any code to process the request.
                //If you want to pass the Ajax request object, pass the 'pointer' below
                var pointer = this
                var intervalId = window.setInterval(function(){
                    if(pointer.readyState != 4){
                        return;
                    }

                    // getel[0].classList.remove("loading");

                    getel[0].classList.remove("loadings");
                    // console.log( pointer.responseText );
                    //Here is where you can add any code to process the response.
                    //If you want to pass the Ajax request object, pass the 'pointer' below
                    clearInterval(intervalId);

                }, 1);//I found a delay of 1 to be sufficient, modify it as you need.
                return proxied.apply(this, [].slice.call(arguments));
            };

        }
        detectajax();
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=542214526701191&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
<style>
    /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
    .modalscren {
        display:    none;
    /*    position:   fixed;*/
    /*    z-index:    1000;*/
    /*    top:        0;*/
    /*    left:       0;*/
    /*    height:     100%;*/
    /*    width:      100%;*/
    /*    background: rgba( 255, 255, 255, .8 )*/
    /*    !*50% 50%*!*/
    /*    no-repeat;*/
    }

    /* When the body has the loading class, we turn
       the scrollbar off with overflow:hidden */
    /*body.loading .modalscren {*/
    /*    overflow: hidden;*/
    /*}*/

    /* Anytime the body has the loading class, our
       modal element will be visible */
    .loadings {
        /*display: block;*/
        position:   fixed;
        width: 100%;
        height:100%;
        display: flex;
        align-items: center;
        z-index:    9999;
        top:        0;
        left:       0;
        bottom:       0;
        right:       0;
        height:     100%;
        width:      100%;
        background: rgba(0,0,0,.65)
            /*50% 50%*/
        no-repeat;
    }
</style>

</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-main fixed-top">
    <div class="container">
        <div class="mr-auto">
<!--            <a href="#" class="toggle-menu" id="toggle-menu"><i class="icofont-navigation-menu text-white"></i></a>-->
            <div class="inline-block title-font semibold ml-10" href="#">
               <a href="<?php echo base_url(); ?>"><img class="logo" src="<?php URI::baseURL(); ?>assets/mumuapps/img/logomumu.png"></a>
            </div>
        </div>
        <div class="ml-auto">
            <a href="#" id="search-top"><i class="icofont-search-1"></i></a>
        </div>
    </div>
</nav>


<div class="container">

    <div class="modalscren">
    <div class="container">
        <div class="d-flex justify-content-center">
            <div class="spinner-border text-success" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    </div>