
<section class="bg-main footer-info">
    <div class="container">
        <div class="row">
            <div class="col-md-6 footer-info-box">
                <div class="h3 mb-25 title-font bold">Ganesha Bisa</div>
                <div class="mb-20">
                    Ganesha Bisa adalah platform penggalangan dana yang mempromosikan berbagai bentuk wakaf dan donasi sebagai bentuk pendanaan inovasi kepada alumni ITB dan pihat-pihak yang terkait lainnya. Platform ini adalah hasil kerjasama antara Yayasan Solidarity Forever (YSF) dan Dompet Dhuafa (DD) dalam upaya memajukan pendidikan di ITB.
                </div>
                <div class="mb-20">
                    <a href="#" class="sos-link facebook" title="Berteman dengan Kami di Facebook"><i class="icofont-facebook"></i></a>
                    <a href="#" class="sos-link" title="Ikuti Kami di Twitter"><i class="icofont-twitter"></i></a>
                    <a href="#" class="sos-link" title="Ikuti Kami di Instagram"><i class="icofont-instagram"></i></a>
                </div>
            </div>
            <div class="col-md-3 footer-info-box">
                <div class="h5 mb-25 title-font bold">Pelajari Lebih Lanjut</div>
                <ul class="inline">
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Syarat dan Ketentuan</a></li>
                    <li><a href="#">Kebijakan Privasi</a></li>
                    <li><a href="#">Tentang Kami</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer-info-box">
                <div class="h5 mb-25 title-font bold">Kontak</div>
                <div class="semibold title-font mb-5">Ganesha Bisa</div>
                <table class="table table-borderless table-sm no-padding">
                    <tbody>
                    <tr>
                        <td style="width:30px;"><i class="icofont-building-alt"></i></td>
                        <td>
                            Philanthropy Building Lt. 2 <br>
                            Jl. Warung Jati Barat No. 14, Jatipadang Jakarta Selatan - 12450
                        </td>
                    </tr>
                    <tr>
                        <td><i class="icofont-ui-dial-phone"></i></td>
                        <td>021 7416050</td>
                    </tr>
                    <tr>
                        <td><i class="icofont-email"></i></td>
                        <td>mail@ganeshabisa.id</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<footer class="bg-second">
    <div class="container">
        <p class="m-0 text-center regular">Copyright &copy; 2019 - Ganesha Bisa</p>
    </div>
    <!-- /.container -->
</footer>


</body>

</html>
