<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $_appTitle; ?> </title>

    <!-- Bootstrap core CSS -->
    <link href="<?php URI::baseURL(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php URI::baseURL(); ?>assets/css/scrolling-nav.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/css/rjcustoms.css" rel="stylesheet">
    <link href="<?php URI::baseURL(); ?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">

    <link href="<?php URI::baseURL(); ?>assets/vendor/splide/css/splide.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,600,600i,700,700i|Roboto:400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
    <!-- Bootstrap core JavaScript -->
    <script src="<?php URI::baseURL(); ?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/vendor/splide/js/splide.min.js"></script>
    <script src="<?php URI::baseURL(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php URI::baseURL(); ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom JavaScript for this theme -->
    <script src="<?php URI::baseURL(); ?>assets/js/scrolling-nav.js"></script>

</head>

<body id="page-top">
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger title-font bold color-main" href="<?php echo URI::baseURL(); ?>">Ganesha Bisa</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php URI::baseURL(); ?>donasi">Donasi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php URI::baseURL(); ?>beasiswa">Beasiswa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php URI::baseURL(); ?>investasi">Investasi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php URI::baseURL(); ?>news/list">Blog</a>
                </li>
                <?php
                if(!$_loginstatus)
                {
                ?>
                <li class="nav-item dropdown no-caret">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Masuk/Daftar</a>
                    <div class="dropdown-menu dropdown-menu-right login-dropdown">
                        <div class="text-center mb-15">Silakan Login</div>
<!--                        <form class="login-form">-->
                        <?php
                        $hidden_form = array('id' => !empty($id) ? $id : '');
                        echo form_open_multipart($_lgn_default, array('id' => 'lg_default','class'=>'login-form'), $hidden_form);
                        ?>
                            <div class="form-group relative-box">
                                <i class="icofont icofont-envelope"></i>
                                <input type="text" name="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group relative-box">
                                <i class="icofont icofont-ui-lock"></i>
                                <input type="password" name="pwd" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                            <label class="custom-control-label text-small" style="padding-top:2px;" for="customCheck1">Ingat saya</label>
                                        </div>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="#" class="text-white text-small">Lupa Password?</a>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-main btn-block btn-login"><i class="icofont-login"></i> Login</button>
<!--                        </form>-->

                        <?php echo form_close(); ?>
                        <div class="text-center mt-20 text-smaller text-white">
                            Belum punya akun? <a href="<?php URI::baseURL(); ?>register" class="text-muted">Daftar</a>
                        </div>
<!--                        <div class="mt-30">-->
<!--                            <div class="text-atau mb-25">-->
<!--                                <span>Atau</span>-->
<!--                            </div>-->
<!--                            <a href="#" class="btn btn-facebook btn-block"><i class="icofont-facebook mr-20"></i> Login dengan Facebook</a>-->
<!--                            <a href="#" class="btn btn-google btn-block mt-15"><i class="icofont-google-plus mr-20"></i> Login dengan Google</a>-->
<!--                        </div>-->
                    </div>
                </li>
                <?php }else{?>
                    <?php
                    $ses = $this->session->userdata(LGI_KEY . "login_info");
                    $indexs = $this->db->query('select nb_donaturs.picture_url, nb_donaturs_d.img from nb_donaturs LEFT JOIN nb_donaturs_d on nb_donaturs.id = nb_donaturs_d.donatur where nb_donaturs.id = '.$ses['userid'])->result();
                    $gmbr = '';
                    $defgmr = '' ;
                    if(!empty($indexs[0]->img)):
                        $gmbr = base_url('assets/images/donatur/profile/'.$indexs[0]->img); ?>
                    <?php elseif(!empty($indexs[0]->picture_url)):
                        $gmbr = $indexs[0]->picture_url;
                    else:
                        $defgmr = '<span class="fa fa-user"></span>' ;
                    endif;
                        ?>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle user-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                            <?php if(!empty($defgmr)): ?>
                            <div class="img-user"><?php echo $defgmr ?></div>
                            <?php else:?>
                            <div class="img-user" style="background-image:url(<?php echo $gmbr?>);"></div>
                            <?php endif;?>
                            <span class="name-user"><?php echo $login_userdata["login_fname"] . " " . $login_userdata["login_lname"]; ?></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right login-dropdown user-dropdown">
                            <a href="<?php URI::baseURL(); ?>myprofile" class="dropdown-item"><i class="icofont-user-alt-4 mr-5"></i> Edit Profil</a>
                            <a href="<?php URI::baseURL(); ?>mydonation" class="dropdown-item"><i class="icofont-holding-hands mr-5"></i> Donasi Saya</a>
                            <a href="<?php URI::baseURL(); ?>mywallet" class="dropdown-item"><i class="icofont-wallet mr-5"></i> Dompet Aksi Balik</a>
<!--                            <a href="--><?php //URI::baseURL(); ?><!--myprofile" class="dropdown-item"><i class="icofont-heart-alt mr-5"></i> Poin Aksi Balik</a>-->
                            <hr>
                            <a href="<?php URI::baseURL(); ?>logout" class="dropdown-item"><i class="icofont-logout mr-5"></i> Keluar</a>
                        </div>
                    </li>
                <?php }?>
            </ul>
        </div>
    </div>
</nav>