{_layHeader}

<?php

echo form_open_multipart();

?>

<div class="container">

	<div class="row">

		<div class="col-md-6">

			<h2><?php echo empty($judul[0]->title)?"INI JUDUl":$judul[0]->title; ?></h2>

			<hr>

			<b>Masukkan Nominal Donasi</b><br>

			<p>Donasi minimal Rp20.000 serta kelipatan ribuan (misal: 50.000, 82.000, 105.000, dst.) </p>

			<br>

			<input type="number" name="donationNominal" value="<?php echo empty($this->session->userdata('prevnomDonation'))?"":$this->session->userdata('prevnomDonation'); ?>" placeholder="Nominal Donasi"><br>

			<h2 class="legend--large align-center">

Tinggalkan Komentar (opsional) </h2>

<p>

Hanya teks saja, tanpa kode html maupun URL </p>

<textarea name="donationCommment" class="form-input form-input--block input-max-char" rows="3" data-char="140"><?php echo empty($this->session->userdata('prevcomDonation'))?"":$this->session->userdata('prevcomDonation'); ?></textarea><br>

	<input type="submit" name="" value="Lanjutkan">

		</div>

	</div>

</div>

<?php echo form_close();?>

{_layFooter}