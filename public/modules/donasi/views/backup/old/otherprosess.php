{_layHeader}
<?php
echo form_open_multipart();
?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h3>Pilihan <?php echo empty($this->session->userdata('prevjenisDonation'))?"": $this->session->userdata('prevjenisDonation'); ?></h3>

            <hr>
            <b>Nominal Donasi</b><br>
            <p>Donasi minimal Rp20.000 serta kelipatan ribuan (misal: 50.000, 82.000, 105.000, dst.) </p>
            <br>
            <h3>Rp. <?php echo empty($this->session->userdata('prevnomDonation'))?"": number_format($this->session->userdata('prevnomDonation') ,0,"","."); ?></h3>
            <input type="hidden" name="donationNominal" readonly value="<?php echo empty($this->session->userdata('prevnomDonation'))?"":$this->session->userdata('prevnomDonation'); ?>" placeholder="Nominal Donasi"><br>
            <h2 class="legend--large align-center">
                Tinggalkan Komentar (opsional) </h2>
            <p>
                Hanya teks saja, tanpa kode html maupun URL </p>
            <textarea name="donationCommment" class="form-input form-input--block input-max-char" rows="3" data-char="140"></textarea><br>
            <input type="submit" name="" value="Lanjutkan">
        </div>
    </div>
</div>
<?php echo form_close();?>
{_layFooter}