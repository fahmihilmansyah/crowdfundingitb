{_layHeader}
<div class="container">
    <div class="row">
        <div class="col-md-12"><h2>Perhitungan Zakat</h2>
            <hr>
            <?php
            echo form_open_multipart();
            ?>
            <table class="table table-stripped">
                <tr>
                    <td>Pendapatan (Gaji per bulan)</td>
                    <td><input type="number" value="0" class="hitung"></td>
                </tr>
                <tr>
                    <td>Pendapatan Lain(Opsional)</td>
                    <td><input type="number" value="0" class="hitung"></td>
                </tr>
                <tr>
                    <td>Hutang / cicilan (Opsional)</td>
                    <td><input type="number" value="0" class="hitung"></td>
                </tr>
            </table>
            <label class="hasil btn btn-success">Rp. 0</label>
            <input type="hidden" name="otherNominal" id="otherNominal" value="0">
            <input type="hidden" name="otherJenis" value="zakatprofesi">
            <button type="submit" CLASS="btn btn-primary">LANJUT</button>
            <?php echo form_close();?>
            <!-- Begin : Javacript -->
            <script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
                <script type = "text/javascript" >
                    /**
                     * Number.prototype.format(n, x, s, c)
                     *
                     * @param integer n: length of decimal
                     * @param integer x: length of whole part
                     * @param mixed   s: sections delimiter
                     * @param mixed   c: decimal delimiter
                     */
                    Number.prototype.format = function(n, x, s, c) {
                        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                            num = this.toFixed(Math.max(0, ~~n));

                        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
                    };
                    $(document).ready(function () {

                        jQuery(document).on("change",".hitung",function () {
                            var sum = 0;
                            $(".hitung").each(function () {
                                sum += parseFloat($(this).val());
                            });
                            $(".hasil").text("Rp. "+parseInt(sum * 0.025).format(0, 3, '.', ','));
                            $("#otherNominal").val(parseInt(sum * 0.025));
                        })
                    });
            </script>
        </div>
    </div>
</div>
{_layFooter}