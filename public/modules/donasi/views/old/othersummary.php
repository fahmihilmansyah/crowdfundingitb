{_layHeader}
<?php
$nominal =  empty($summary[0]->nominal)?"":$summary[0]->nominal;
$unik =  empty($summary[0]->uniqcode)?"":$summary[0]->uniqcode;
$total = $nominal + $unik;
?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <hr>
            <b>Ringkasan Donasi Anda:</b><br>
            <table class="table table-stripped">
                <tr>
                    <td>Kode Donasi</td>
                    <td><?php echo empty($summary[0]->no_transaksi)?"":$summary[0]->no_transaksi?></td>
                </tr>
                <tr>
                    <td>Nominal Donasi</td>
                    <td>Rp. <?php echo number_format($nominal,0,"",".")?></td>
                </tr>
                <tr>
                    <td>Kode Unik</td>
                    <td>Rp. <?php echo number_format($unik,0,"",".")?></td>
                </tr>
                <tr>
                    <td><b>TOTAL</b></td>
                    <td>Rp. <?php echo number_format($total,0,"",".")?></td>
                </tr>
            </table>
            <p><b>Transfer tepat Rp <?php echo number_format($total,0,"",".")?></b> (nominal donasi + kode unik) agar donasi Anda dapat terverifikasi secara otomatis oleh sistem tanpa perlu konfirmasi.</p><br>
            <h3>Silakan transfer ke: </h3><br>
            <table class="table">
                <tr>
                    <td>Image</td>
                    <td>
                        <b></b><br>
                        Cabang: <br>
                        No. Rekening: <br>
                        Atas nama: <br>
                    </td>
                </tr>
            </table>
            <hr>
            <p>Pastikan Anda transfer sebelum <b>25 Feb 2017 16:40 WIB</b> atau donasi Anda otomatis dibatalkan oleh sistem. </p>
        </div>
    </div>
</div>
{_layFooter}