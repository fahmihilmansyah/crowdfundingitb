{_layHeader}
<?php
echo form_open_multipart();
?>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<h2><?php echo empty($judul[0]->title)?"INI JUDUl":$judul[0]->title; ?></h2>
			<hr>
			<h3><b>Identitas Anda</b></h3><br>
			<p>Sign in atau tulis email Anda untuk mendapatkan laporan progres dari campaign: </p>
			<br>
			<input type="text" name="donationName" value="" placeholder="Nama Lengkap">
			<input type="email" name="donationEmail" value="" placeholder="Email Address"><br>
			<p>Pastikan nomor ini aktif untuk menerima SMS status donasi Anda: </p>
			<input type="text" name="donationPhone" value="" placeholder="Masukan nomor HP Anda, angka saja. Format 08xxx atau 62xxx."><br>
			<H3>Pilih Metode Pembayaran</H3>
			<hr>
			<?php foreach($bank as $r):?>
			<label>
			<input type="radio" name="donationPay" value="<?php echo $r->id?>" placeholder=""> <?php echo $r->namaBank?>
			</label><br>
		<?php endforeach; ?>
	<input type="submit" name="" value="Lanjutkan">
		</div>
	</div>
</div>
<?php echo form_close();?>
{_layFooter}