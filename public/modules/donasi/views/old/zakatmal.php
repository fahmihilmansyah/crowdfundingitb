{_layHeader}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Perhitungan Zakat Mall</h2>
            <hr>
            <p>Khusus untuk harta yang telah tersimpan selama lebih dari 1 tahun(haul) dan mencapai batas tertenu(hisab)</p>
            <?php
            echo form_open_multipart();
            ?>
            <table class="table table-stripped">

                <tr>
                    <td>Nilai Deposito/Tabungan/Giro</td>
                    <td><input type="hidden" class="hitungper" value="<?php echo $hargaemas;?>"> <input type="number" value="0" class="hitung"></td>
                </tr>
                <tr>
                    <td>Nilai Properti & kendaraan ( bukan yang digunakan sehari - hari)</td>
                    <td><input type="number" value="0" class="hitung"></td>
                </tr>
                <tr>
                    <td>Emas, perak, permata atau sejenisnya</td>
                    <td><input type="number" value="0" class="hitung"></td>
                </tr>
                <tr>
                    <td>Hutang pribadi yang jatuh tempo tahun ini</td>
                    <td><input type="number" value="0" class="hitunghutang"></td>
                </tr>
            </table>
            <label class="hasil btn btn-success">Rp. 0</label>
            <input type="hidden" name="otherNominal" id="otherNominal" value="0">
            <input type="hidden" name="otherJenis" value="zakatmall">
            <button type="submit" CLASS="btn btn-primary">LANJUT</button>
            <?php echo form_close();?>
            <!-- Begin : Javacript -->
            <script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
            <script type = "text/javascript" >
                /**
                 * Number.prototype.format(n, x, s, c)
                 *
                 * @param integer n: length of decimal
                 * @param integer x: length of whole part
                 * @param mixed   s: sections delimiter
                 * @param mixed   c: decimal delimiter
                 */
                Number.prototype.format = function(n, x, s, c) {
                    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                        num = this.toFixed(Math.max(0, ~~n));

                    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
                };
                $(document).ready(function () {

                    jQuery(document).on("change",".hitung , .hitunghutang",function () {
                        var sum = 0;
                        var total = 0;
                        var nisab = 0;
                        var hutang = parseInt($(".hitunghutang").val());
                        $(".hitung").each(function () {
                            sum += parseFloat($(this).val());
                        });
                        total = parseInt(sum - hutang);
                        nisab = parseInt($(".hitungper").val()) * 12;
                        alert("nisab"+nisab+" ttotal"+total+"Hutang"+hutang);
                        if(total < nisab){
                            total = 0;
                        }
                        total = parseInt(total * 0.025);
                        $(".hasil").text("Rp. "+total.format(0, 3, '.', ','));
                        $("#otherNominal").val(total);
                    })
                });
            </script>
        </div>
    </div>
</div>
{_layFooter}