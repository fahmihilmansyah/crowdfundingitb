{_layHeader}

<?php
$url = implode("/", $this->uri->segment_array());
$dats = array('urllink' => $url);
$this->session->set_userdata($dats);
?>
<?php $cek = empty($this->session->userdata('prevnomDonation')) ? "" : "active"; ?>
<div class="content">

    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'donasi/aksi/' . $this->uri->segment(3), array('id' => 'fmdonasi','class' => 'fmdonasi mt-20'), $hidden_form);

    ?>
        <div class="form-group">
            <label>Masukkan Nominal</label>
            <input type="text" class="form-control" placeholder="Rp. 50.000">
        </div>
        <div class="form-group">
            <label>No. Handphone</label>
            <input type="text" class="form-control" placeholder="Masukkan Nomor Handphone Anda">
        </div>
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" placeholder="Masukkan Nama Anda">
            <div class="custom-control custom-checkbox mt-5">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label" for="customCheck1">Danai secara Anonim</label>
            </div>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" placeholder="Masukkan Email Anda">
        </div>
        <div class="form-group">
            <label>Komentar Dukungan Anda</label>
            <textarea class="form-control" placeholder="Berikan Komentar, Doa, atau Dukungan Anda" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label>Metode Pembayaran</label>
            <input type="text" class="form-control" placeholder="Pilih Metode Pembayaran">
        </div>
        <div class="mt-30">
            <button class="btn btn-block btn-main"><span>Danai Sekarang</span></button>
        </div>

    <?php

    echo form_close();

    ?>


    <?php if(!empty($this->session->flashdata('error'))):?>
    <div  class="alert alert-danger alert-dismissable fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <script type="text/javascript">
        setTimeout(function() {
            $('.alert').fadeOut();
        }, 10000); // <-- time in milliseconds
    </script>
<?php endif;?>
<section>
    <div class="container">
        <div class="danai-wrapper">
            <div class="mb-40">
                <div class="h3 bold title-font lh-1 color-second"><?php echo empty($judul[0]->title) ? "" : $judul[0]->title; ?></div>
            </div>
            <ul class="nav nav-pills mb-3 danai-tab justify-content-center" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-nominal-tab" data-toggle="pill" href="#pills-nominal"
                       role="tab" aria-controls="pills-nominal" aria-selected="true"><span
                                class="danai-tab-number">1</span> Nominal</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-pembayaran-tab" data-toggle="pill" href="#pills-pembayaran" role="tab"
                       aria-controls="pills-pembayaran" aria-selected="false"><span class="danai-tab-number">2</span>
                        Pembayaran</a>
                </li>
            </ul>
            <div class="tab-content danai-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-nominal" role="tabpanel"
                     aria-labelledby="pills-nominal-tab">
<!--                    <form>-->
                        <div class="form-group mb-30">
                            <label>Isi Nominal Donasi</label>
                            <div class="relative-box">
                                <span class="nominal-prefix title-font bold">Rp</span>
                                <input type="text" name="donationNominal" class="form-control input-nominal title-font bold">
                            </div>
                            <div class="text-small text-muted">Minimal Donasi Rp 30.000</div>
                        </div>
                        <div class="form-group mb-30">
                            <label>Tulis Komentar (Opsi)</label>
                            <textarea name="donationCommment" class="form-control" rows="3"></textarea>
                        </div>
                        <label href="#" class="nextbayar btn btn-main btn-block">Lanjut</label>
<!--                    </form>-->
                </div>
                <div class="tab-pane fade" id="pills-pembayaran" role="tabpanel" aria-labelledby="pills-pembayaran-tab">
<!--                    <form>-->
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input name="donationName" type="text"
                                   value="<?php echo empty($_SESSION[LGI_KEY . "login_info"]['fname']) ? "Hamba Allah" : $_SESSION[LGI_KEY . "login_info"]['fname']; ?>"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label>No HP</label>
                            <input id="donationPhone" name="donationPhone" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input name="donationEmail"
                                   value="<?php echo empty($_SESSION[LGI_KEY . "login_info"]['email']) ? "" : $_SESSION[LGI_KEY . "login_info"]['email']; ?>"
                                   type="email" class="form-control">
                        </div>
                        <!--                        <div class="form-group mb-40">-->
                        <!--                            <div class="custom-control custom-switch">-->
                        <!--                                <input type="checkbox" class="custom-control-input" id="customSwitch1">-->
                        <!--                                <label class="custom-control-label" for="customSwitch1">Sembunyikan nama saya (anonim)</label>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
                        <div class="form-group mb-30">
                            <label>Pilih Metode Pembayaran</label>
                            <?php if (!empty($bank)):

                                foreach ($bank as $r):

                                    ?>
                                    <label class="pilih-bank-box">
                                        <input type="radio" id="bank1" name="donationPay" value="<?php echo $r->id ?>">
                                        <div class="pilih-bank-text"><?php echo $r->namaBank ?></div>
                                    </label>
                                <?php endforeach; endif; ?>
                        </div>
                        <button type="submit" class="btn btn-main btn-block">Lanjut</button>
<!--                    </form>-->
                </div>
            </div>
        </div>
    </div>
</section>

<?php

echo form_close();

?>

<script>
    jQuery(document).on('click','.nextbayar',function () {
        $('#pills-pembayaran-tab').tab('show');
    });
</script>


{_layFooter}

