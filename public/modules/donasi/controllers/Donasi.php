<?php

defined('BASEPATH') or exit('No direct script access allowed');


class donasi extends NBFront_Controller

{


    private $_modList = array('donasi/donasi_model' => "donasimdl");


    protected $_data = array();

    private $_SESSION;

    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();
        // $this->nbauth_front->lookingForAuten();


        $this->_SESSION = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
            $this->session->userdata(LGI_KEY . "login_info") : '';
    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);
        $this->load->library('Fhhlib');

    }


    public function index($id = null)

    {


        if (empty($id)) {

            redirect('home');

        }

        if ($_POST) {
            $condition['where'] = array(
                'nb_campaign.id' => $this->db->escape($id),
//                'nb_campaign.valid_date >= ' => $this->db->escape(date('Y-m-d'))
            );

            $campaigndata = $this->donasimdl->custom_query("nb_campaign", $condition)->result();
            $title_campaign = $campaigndata[0]->title;
//            print_r($_POST);exit;

//            if (!empty($this->input->post('subnominal'))) {
            $datadonat = array(

                'prevnomDonation' => str_replace(".", '', $this->input->post('donationNominal')),

                'prevjenisDonation' => $this->input->post('otherJenis'),

                'prevcomDonation' => $this->input->post('donationCommment'),

                'prevanomDonation' => $this->input->post('donationAnonim'),

                'prevcampDonation' => $id,

                'statusActive' => 'active',

            );

            $this->session->set_userdata($datadonat);

//            }

//            if (!empty($this->input->post('subbayar'))) {

            $jenis = $this->session->userdata('prevcampDonation');


            $nominal = $this->session->userdata('prevnomDonation');

            $comment = $this->session->userdata('prevcomDonation');

            $anonim = $this->session->userdata('prevanomDonation');

            $donationName = $this->input->post('donationName');

            $donationEmail = $this->input->post('donationEmail');

            $donationPhone = empty($this->input->post('donationPhone'))?'081234567899':$this->input->post('donationPhone');

            $methodpay = $this->input->post('donationPay');
            $error = "";
            if (empty($jenis)) {
                $error .= '- Kesalahan sistem, hubungin administrator. <br>';
            }
            if ($nominal < 9999 || !is_numeric($nominal)) {
                $error .= '- Nominal dana harus minimal Rp.10.000 <br>';
            }
            if (empty($methodpay)) {
                $error .= '- Metode pembayaran harus dipilih. <br>';
            }
            if (empty($donationEmail) || empty($donationPhone) || strlen($donationPhone) < 9) {
                $error .= '- Email / Telp harus diisi (minimal 10 karakter) <br>';
            }
            if (!empty($error)) {
                $dataerror = array('error' => $error);
                $this->session->set_flashdata($dataerror);
                redirect('donasi/aksi/' . $id);
                exit;
            }

            $notrx = date('Ymd') . sprintf('%05d', $this->geninc());

            $uniqcode = $this->genUniq($nominal);

            $iddonatur = empty($this->_SESSION['userid']) ? '0' : $this->_SESSION['userid'];

            $insertData = array(

                'no_transaksi' => $notrx,

                'nominal' => $nominal,

                'trx_date' => date('Y-m-d'),

                'campaign' => $jenis,

                'donatur' => $iddonatur,/*ubah donaturnya jika login maka akan mengambil session loginnya*/

                'bank' => $methodpay,

                'comment' => $comment,

                'uniqcode' => 0,//($methodpay == 919 ? 0 : $uniqcode),

                'namaDonatur' => $donationName,

                'emailDonatur' => $donationEmail,

                'telpDonatur' => $donationPhone,

                'nomuniq' => $nominal,//+ ($methodpay == 919 ? 0 : $uniqcode),

                'validDate' => date('Y-m-d', strtotime('+2 days')),

                'crtd_at' => date("Y-m-d H:i:s"),

                'edtd_at' => date("Y-m-d H:i:s"),

                'month' => date("m"),
                'year' => date("Y"),

            );

            $this->donasimdl->custom_insert("nb_trans_donation", $insertData);


            $array_items = array(

                'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive', 'prevanomDonation', 'prevcampDonation');

            $this->session->unset_userdata($array_items);

            $datadonat = array(

                'doneDonation' => $notrx,

            );

            $this->session->set_userdata($datadonat);

            if ($methodpay == 'QRISVA') {
                $condition['where'] = array(
                    'nb_campaign.id' => $this->db->escape($id),
                    'nb_campaign.valid_date >= ' => $this->db->escape(date('Y-m-d'))
                );
                $campaigndata = $this->donasimdl->custom_query("nb_campaign", $condition)->result();
                $findqris = $this->donasimdl->custom_query('nb_sys_users', ['where' => ['id' => $campaigndata[0]->user_post]])->result();
                $data = $this->_data;
                $data['qris'] = $findqris;
                $data['nominal'] = $nominal;
                return $this->parser->parse("view_qris", $data);
            }
            if ($methodpay == 'SALDO_DOMPET' && !empty($this->_SESSION['userid'])) {
                $condition['where'] = array(
                    'donatur' => $this->db->escape($this->_SESSION['userid']),
                );
                $ceksaldo = $this->donasimdl->custom_query("nb_donaturs_saldo", $condition)->result();
                if ($ceksaldo[0]->balanceAmount < $nominal):
                    $dataerror = array('error' => 'Saldo anda tidak mencukupi. Saldo : ' . $ceksaldo[0]->balanceAmount);
                    $this->session->set_flashdata($dataerror);
                    redirect('donasi/aksi/' . $id);
                    exit;
                endif;
                Fhhlib::updateSaldo($this->_SESSION['userid'],$nominal,$notrx);
                Fhhlib::updateTrans('nb_trans_donation',$notrx,'verified');
                $data = $this->_data;
                $data['title'] = 'Transaksi Sukses';
                $data['trxid'] = $notrx;
                return $this->parser->parse("v_sukses", $data);
            }
            /*cek jika pemilihan melalui dompetku*/

//                if ($methodpay == 919) {
//
//                    $this->trxDeposit($iddonatur, $nominal, $notrx);
//                    $this->doVerifikasiCampaign($notrx);
//                    redirect('invoice/depodonation/'.$notrx);
//
//                }else{
//
//                    redirect('invoice/donation/'.$notrx);
//
//                }


//            }

            $idipg = Fhhlib::uuidv4Nopad();

            $arrParam['idipg'] = $idipg;
            $expmethod = explode('|', $methodpay);
            if ($expmethod[0] == 'FINPAY') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = ($donationPhone);
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                $arrParam['items'] = "Pembayaran Donasi Untuk " . $title_campaign;
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                    }
                }
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = "Pembayaran Donasi Untuk " . $title_campaign;//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_donation',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

                $this->donasimdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $res = Fhhlib::finpayIPG($arrParam);
//            $res = $this->finpayIPG($nominal,$donationEmail,$iddonatur,$donationPhone,$donationName,$notrx);
                if (!empty($res)) {
                    if ($res['status_code'] == '00') {
                        if (!empty($res['landing_page'])) {
                            redirect($res['landing_page'], 'refresh');
                        } elseif (!empty($res['redirect_url'])) {
                            redirect($res['redirect_url'], 'refresh');
                        } else {
                            $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                            $this->session->set_flashdata($dataerror);
                            redirect('donasi/index/' . $id);
                        }
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('donasi/index/' . $id);
                        exit;
                    }
                }
//                header("Location: ".$res);
                print_r($res);
                exit;
//            print_r($res);exit;
            }
            elseif ($expmethod[0] == 'MUPAYS') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = ($donationPhone);
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                $arrParam['token_login'] = !empty($this->_SESSION['mupays_token'])?$this->_SESSION['mupays_token']:"";
                $arrParam['items'] = "Pembayaran Donasi Untuk " . $title_campaign;
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                    }
                }
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = "Pembayaran Donasi Untuk " . $title_campaign;//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_donation',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

                $this->donasimdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $res = Fhhlib::devfinpayIPG($arrParam);
//            $res = $this->finpayIPG($nominal,$donationEmail,$iddonatur,$donationPhone,$donationName,$notrx);
                if (!empty($res)) {
                    if ($res['rc'] == '0000') {
                        if (!empty($res['data']['no_va'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['hasil'] = $res['data'];
                            return $this->parser->parse("view_vatrx", $datas);
                        }
                        if (!empty($res['data']['landing_page'])) {
                            return redirect($res['data']['landing_page'], 'refresh');
//                            $datas = $this->_data;
//                            $datas['hasil'] = $res['data'];
//                            return $this->parser->parse("view_vatrx", $datas);
                        }
                        if (!empty($res['data']['qris'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['url_img'] = $res['data']['qris']['url_img'];
                            $datas['nominal'] = $nominal;
                            return $this->parser->parse("view_qris", $datas);
                        }
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('donasi/index/' . $id);
                        exit;
                    }
                }
//                header("Location: ".$res);
                print_r($res);
                exit;
//            print_r($res);exit;
            }
            elseif ($expmethod[0] == 'E2PAY') {
                $cariipg = $this->donasimdl->custom_query('nb_partner_ipg', ['where' => ['id' => $this->db->escape('E2PAY')]])->result();
                $key = !empty($cariipg[0]->key) ? $cariipg[0]->key : "v1XdZSLMs9";
                $merchant_id = !empty($cariipg[0]->merchant_key) ? $cariipg[0]->merchant_key : "IF00294";
                $merchantkey = $key;//'v1XdZSLMs9';
                $merchantcode = $merchant_id;//'IF00294';
                $arrParam['url_host'] = $cariipg[0]->url_host;
                $arrParam['MerchantCode'] = $merchantcode;
                $arrParam['PaymentId'] = $expmethod[1];
                $arrParam['RefNo'] = $notrx;
                $arrParam['Amount'] = $nominal . "00";
                $arrParam['Currency'] = 'IDR';
                $arrParam['ProdDesc'] = "Pembayaran Donasi Untuk " . $title_campaign;
                $arrParam['UserName'] = $donationName;
                $arrParam['UserEmail'] = $donationEmail;
                $arrParam['UserContact'] = $donationPhone;
                $signatures = Fhhlib::E2Pay_signature($merchantkey . $merchantcode . $arrParam['RefNo'] . $arrParam['Amount'] . $arrParam['Currency']);
                $arrParam['Signature'] = $signatures;
                $arrParam['Remark'] = '';
                $arrParam['ResponseURL'] = base_url('trxipg/responsee2pay');
                $arrParam['BackendURL'] = base_url('trxipg/backende2pay');

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_donation',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

//                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $this->donasimdl->custom_insert("nb_trans_e2pay", $insipgtrx);
                $datas = $this->_data;
                $datas['parame2pay'] = $arrParam;
////            print_r($datas);
                return $this->parser->parse("view_e2pay", $datas);

                $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . $res);
                $this->session->set_flashdata($dataerror);
                redirect('donasi/index/' . $id);

            } else {
                $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . $res);
                $this->session->set_flashdata($dataerror);
                redirect('donasi/index/' . $id);
            }
            exit;
        } else {


            $condition['where'] = array(
                'nb_campaign.id' => $this->db->escape($id),
                'nb_campaign.valid_date >= ' => $this->db->escape(date('Y-m-d'))
            );

            $campaigndata = $this->donasimdl->custom_query("nb_campaign", $condition)->result();

            if (count($campaigndata) == 0) {
                redirect('/');
            }
            $findqris = $this->donasimdl->custom_query('nb_sys_users', ['where' => ['id' => $campaigndata[0]->user_post]])->result();
            /*echo $this->db->last_query();
            print_r($campaigndata);
            die();*/
            $data = $this->_data;
            $kondisi['where'] = array('nb_bank.id not in' => "('" . $this->db->escape(919) . "')");
            $bank = $this->donasimdl->custom_query("nb_bank", $kondisi)->result();
            $data['bank'] = $bank;

            $data['judul'] = $campaigndata;

            $data["_fdonasi"] = $this->parser->parse('form/donasi', $data, TRUE);

            $data["_fpembayaran"] = $this->parser->parse('form/pembayaran', $data, TRUE);
            $data['qris'] = $findqris;
            $this->parser->parse("index", $data);

        }

    }

    private function genUniq($nominal = 0)

    {

        $condition['where'] = array(

            $this->db->escape($nominal) . ' BETWEEN' => ' minlimit and maxlimit'

        );

        $generate = $this->donasimdl->custom_query("nb_incuniq", $condition)->result();

        $uniqid = $generate[0]->id;

        $uniqinc = $generate[0]->uniqinc + 1;

        if ($uniqinc > $generate[0]->maxuniq) {

            $uniqinc = $generate[0]->minuniq;

        }

        $this->donasimdl->custom_update("nb_incuniq", array('uniqinc' => $uniqinc), array('id' => $uniqid));

        return $uniqinc;

    }


    private function geninc()

    {

        $kondisi['where'] = array('id' => $this->db->escape(1));

        $generate = $this->donasimdl->custom_query("nb_inc", $kondisi)->result();

        $tgl = $generate[0]->tgl;

        $uniqinc = 0;

        if ($tgl != date("Y-m-d")) {

            $uniqinc = $uniqinc + 1;

            //echo $uniqinc."||".$tgl;

        } else {

            $uniqinc = $generate[0]->inc + 1;

            //echo "else : ".$uniqinc;

        }

        $this->donasimdl->custom_update("nb_inc", array('inc' => $uniqinc, 'tgl' => date("Y-m-d")), array('id' => $this->db->escape(1)));

        return $uniqinc;

    }




    /*untuk proses verifikasi manual*/

    /*function admverifikasi()

    {

        $data = $this->_data;

        $kondisi['where'] = array('nb_trans_donation.status not in' => "('verified')");

        $kondisi['join'] = array(

            array('table' => "nb_campaign", 'condition' => "nb_campaign.id = nb_trans_donation.campaign", "type" => "left"),

            array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_donation.bank", "type" => "left"));

        $summary = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();

        echo $this->db->last_query();

        die();

        $data['donasi'] = $summary;

        $this->parser->parse("donasi/admverifikasi", $data);

    }*/


    /*function doVerifikasi($id = null)

    {

        if (empty($id)) {

            redirect('donasi/admverifikasi');

        }

        $kondisi['where'] = array("nb_trans_donation.no_transaksi" => $this->db->escape($id), "nb_trans_donation.status" => $this->db->escape("unverified"));

        $cekdonasi = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();

        if (count($cekdonasi) != 1) {

            die("cannot proses");

        } else {

            $campaign = $cekdonasi[0]->campaign;

            $nomuniq = $cekdonasi[0]->nomuniq;



            $kondisi1['where'] = array("nb_campaign.id" => $campaign);

            $cekcampaign = $this->donasimdl->custom_query("nb_campaign", $kondisi1)->result();

            if (count($cekcampaign) > 0) {

                $sumdonatur = (int)$cekcampaign[0]->sum_donatur;

                $now = (int)$cekcampaign[0]->now;*/

    /*updated nb_campaign*/

    /*$data = array("sum_donatur" => (int)$sumdonatur + 1, 'now' => (int)$now + (int)$nomuniq, 'edtd_at' => date("Y-m-d H:i:s"));

    $where = array("nb_campaign.id" => ($campaign),);

    $this->donasimdl->custom_update("nb_campaign", $data, $where);*/

    /*update nb_trans_donation*/

    /*$data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));

    $where = array("nb_trans_donation.no_transaksi" => ($id),);

    $this->donasimdl->custom_update("nb_trans_donation", $data, $where);

    redirect('donasi/admverifikasi/');

}

}

}*/

    /*endof verifikasi campaign*/

    function trxDeposit($iddonatur = null, $nominal = null, $notransaksi = null)


    {


        $kondisi['where'] = array("nb_donaturs_saldo.donatur" => $this->db->escape($iddonatur));


        $ceksaldo = $this->donasimdl->custom_query("nb_donaturs_saldo", $kondisi)->result();


        if (count($ceksaldo) != 1) {

            $dataerror = array('error' => 'Tidak dapat diproses, mohon hubungin administrator');
            $this->session->set_flashdata($dataerror);
            $preflink = $this->session->userdata('urllink');
            redirect($preflink);
            // redirect('rogram/infaq');
            exit;


            die("cannot proses");


        } else {


            $balanceAmount = 0;


            $prevBlance = $ceksaldo[0]->balanceAmount;


            $balanceAmount = (int)$ceksaldo[0]->balanceAmount - (int)$nominal;


            if ($balanceAmount < 0) {

                $dataerror = array('error' => '- Saldo Tidak Cukup, segera isi dompet baik anda');
                $this->session->set_flashdata($dataerror);
                $preflink = $this->session->userdata('urllink');
                redirect($preflink);
                // redirect('rogram/infaq');
                exit;

                die("Maaf Saldo Tidak Cukup");


            }


            $data = array('donaturs' => $iddonatur,


                'jenistrx' => "TRX",


                'id_transaksi' => $notransaksi,


                'prevBalance' => $prevBlance,


                'amount' => $nominal,


                'balance' => $balanceAmount,


                'crtd_at' => date("Y-m-d H:i:s"),


                'edtd_at' => date("Y-m-d H:i:s"),


            );


            $this->donasimdl->custom_insert('nb_donaturs_trx', $data);


            $data = array("prevBalance" => $prevBlance,


                'balanceAmount' => $balanceAmount,


                'edtd_at' => date("Y-m-d H:i:s"));


            $where = array("nb_donaturs_saldo.donatur" => $iddonatur);


            $this->donasimdl->custom_update("nb_donaturs_saldo", $data, $where);


            /*update nb_trans_donation*/


            $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));


            $where = array("nb_trans_donation.no_transaksi" => ($notransaksi),);


            $this->donasimdl->custom_update("nb_trans_donation", $data, $where);


        }


    }

    function doVerifikasiCampaign($id = null)

    {


        $kondisi['where'] = array("nb_trans_donation.no_transaksi" => $this->db->escape($id),/* "nb_trans_donation.status" => $this->db->escape("unverified")*/);

        $cekdonasi = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();

        if (count($cekdonasi) != 1) {

            die("cannot proses 1237");

        } else {

            $campaign = $cekdonasi[0]->campaign;

            $nomuniq = $cekdonasi[0]->nomuniq;

            $notransaksi = $cekdonasi[0]->no_transaksi;

            $iddonatur = $cekdonasi[0]->donatur;

            $kodebank = $cekdonasi[0]->bank;

            $kondisi1['where'] = array("nb_campaign.id" => $campaign);

            $cekcampaign = $this->donasimdl->custom_query("nb_campaign", $kondisi1)->result();

            if (count($cekcampaign) > 0) {

                $sumdonatur = (int)$cekcampaign[0]->sum_donatur;

                $now = (int)$cekcampaign[0]->now;

                /*updated nb_campaign*/

                $data = array("sum_donatur" => (int)$sumdonatur + 1, 'now' => (int)$now + (int)$nomuniq, 'edtd_at' => date("Y-m-d H:i:s"));

                $where = array("nb_campaign.id" => ($campaign),);

                $this->donasimdl->custom_update("nb_campaign", $data, $where);

                /*update nb_trans_donation*/

                $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));

                $where = array("nb_trans_donation.no_transaksi" => ($id),);

                $this->donasimdl->custom_update("nb_trans_donation", $data, $where);

            }

        }

    }

}