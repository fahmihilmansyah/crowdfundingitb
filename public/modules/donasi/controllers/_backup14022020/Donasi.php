<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class donasi extends NBFront_Controller

{



    private $_modList = array('donasi/donasi_model' => "donasimdl");



    protected $_data = array();

    private $_SESSION;

    function __construct()

    {

        parent::__construct();



        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();
        // $this->nbauth_front->lookingForAuten();


        $this->_SESSION   = !empty($this->session->userdata(LGI_KEY . "login_info")) ? 
                                   $this->session->userdata(LGI_KEY . "login_info") : '';
    }



    private function modelsApp()

    {

        $this->load->model($this->_modList);

    }



    public function index($id = null)

    {



        if (empty($id)) {

            redirect('home');

        }

        if ($_POST) {
//            print_r($_POST);exit;

//            if (!empty($this->input->post('subnominal'))) {

                $datadonat = array(

                    'prevnomDonation' => $this->input->post('donationNominal'),

                    'prevjenisDonation' => $this->input->post('otherJenis'),

                    'prevcomDonation' => $this->input->post('donationCommment'),

                    'prevanomDonation' => $this->input->post('donationAnonim'),

                    'prevcampDonation' => $id,

                    'statusActive' => 'active',

                );

                $this->session->set_userdata($datadonat);

//            }

//            if (!empty($this->input->post('subbayar'))) {

                $jenis =  $this->session->userdata('prevcampDonation');


                $nominal = $this->session->userdata('prevnomDonation');

                $comment = $this->session->userdata('prevcomDonation');

                $anonim = $this->session->userdata('prevanomDonation');

                $donationName = $this->input->post('donationName');

                $donationEmail = $this->input->post('donationEmail');

                $donationPhone = $this->input->post('donationPhone');

                $methodpay = $this->input->post('donationPay');
                $error = "";
                if(empty($jenis)){
                    $error .= '- Kesalahan sistem, hubungin administrator. <br>';
                }
                if($nominal < 29999 || !is_numeric($nominal)){
                    $error .= '- Nominal dana harus minimal Rp.30.000 <br>';
                }
                if(empty($methodpay)){
                    $error .= '- Metode pembayaran harus dipilih. <br>';
                }
                if(empty($donationEmail) || empty($donationPhone) || strlen($donationPhone) < 9){
                    $error .= '- Email / Telp harus diisi (minimal 10 karakter) <br>';
                }
                if(!empty($error)){
                    $dataerror = array('error'=>$error);
                    $this->session->set_flashdata($dataerror);
                   print_r($dataerror);exit;
                    redirect('donasi/aksi/'.$id);
                    exit;
                }

                $notrx = date('Ymd') . sprintf('%05d', $this->geninc());

                $uniqcode = $this->genUniq($nominal);

                $iddonatur = empty($this->_SESSION['userid'])? '0' :$this->_SESSION['userid'];

                $insertData = array(

                    'no_transaksi' => $notrx,

                    'nominal' => $nominal,

                    'trx_date' => date('Y-m-d'),

                    'campaign' => $jenis,

                    'donatur' => $iddonatur,/*ubah donaturnya jika login maka akan mengambil session loginnya*/

                    'bank' => $methodpay,

                    'comment' => $comment,

                    'uniqcode' => ($methodpay == 919 ? 0 : $uniqcode),

                    'namaDonatur' => $donationName,

                    'emailDonatur' => $donationEmail,

                    'telpDonatur' => $donationPhone,

                    'nomuniq' => $nominal + ($methodpay == 919 ? 0 : $uniqcode),

                    'validDate' => date('Y-m-d', strtotime('+2 days')),

                    'crtd_at' => date("Y-m-d H:i:s"),

                    'edtd_at' => date("Y-m-d H:i:s"),

                    'month' => date("m"),
                    'year' => date("Y"),

                );

                $this->donasimdl->custom_insert("nb_trans_donation", $insertData);



                $array_items = array(

                    'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive', 'prevanomDonation', 'prevcampDonation');

                $this->session->unset_userdata($array_items);

                $datadonat = array(

                    'doneDonation' => $notrx,

                );

                $this->session->set_userdata($datadonat);

                /*cek jika pemilihan melalui dompetku*/

                if ($methodpay == 919) {

                    $this->trxDeposit($iddonatur, $nominal, $notrx);
                    $this->doVerifikasiCampaign($notrx);
                    redirect('invoice/depodonation/'.$notrx);

                }else{

                    redirect('invoice/donation/'.$notrx);

                }

//            }

            redirect('donasi/index/'.$id);

        } else {



            $condition['where'] = array(
                'nb_campaign.id' => $this->db->escape($id),
                'nb_campaign.valid_date >= ' => $this->db->escape(date('Y-m-d'))
            );

            $campaigndata = $this->donasimdl->custom_query("nb_campaign", $condition)->result();
            if(count($campaigndata) == 0){
                redirect('/');
            }
            /*echo $this->db->last_query();
            print_r($campaigndata);
            die();*/
            $data = $this->_data;
		$kondisi['where'] = array('nb_bank.id not in'=>"('".$this->db->escape(919)."')");
            $bank = $this->donasimdl->custom_query("nb_bank", $kondisi)->result();
            $data['bank']=$bank;

            $data['judul'] = $campaigndata;

            $data["_fdonasi"] = $this->parser->parse('form/donasi', $data, TRUE);

            $data["_fpembayaran"] = $this->parser->parse('form/pembayaran', $data, TRUE);

            $this->parser->parse("index", $data);

        }

    }





    private function genUniq($nominal = 0)

    {

        $condition['where'] = array(

            $this->db->escape($nominal) . ' BETWEEN' => ' minlimit and maxlimit'

        );

        $generate = $this->donasimdl->custom_query("nb_incuniq", $condition)->result();

        $uniqid = $generate[0]->id;

        $uniqinc = $generate[0]->uniqinc + 1;

        if ($uniqinc > $generate[0]->maxuniq) {

            $uniqinc = $generate[0]->minuniq;

        }

        $this->donasimdl->custom_update("nb_incuniq", array('uniqinc' => $uniqinc), array('id' => $uniqid));

        return $uniqinc;

    }



    private function geninc()

    {

        $kondisi['where'] = array('id' => $this->db->escape(1));

        $generate = $this->donasimdl->custom_query("nb_inc", $kondisi)->result();

        $tgl = $generate[0]->tgl;

        $uniqinc = 0;

        if ($tgl != date("Y-m-d")) {

            $uniqinc = $uniqinc + 1;

            //echo $uniqinc."||".$tgl;

        } else {

            $uniqinc = $generate[0]->inc + 1;

            //echo "else : ".$uniqinc;

        }

        $this->donasimdl->custom_update("nb_inc", array('inc' => $uniqinc, 'tgl' => date("Y-m-d")), array('id' => $this->db->escape(1)));

        return $uniqinc;

    }




    /*untuk proses verifikasi manual*/

    /*function admverifikasi()

    {

        $data = $this->_data;

        $kondisi['where'] = array('nb_trans_donation.status not in' => "('verified')");

        $kondisi['join'] = array(

            array('table' => "nb_campaign", 'condition' => "nb_campaign.id = nb_trans_donation.campaign", "type" => "left"),

            array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_donation.bank", "type" => "left"));

        $summary = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();

        echo $this->db->last_query();

        die();

        $data['donasi'] = $summary;

        $this->parser->parse("donasi/admverifikasi", $data);

    }*/



    /*function doVerifikasi($id = null)

    {

        if (empty($id)) {

            redirect('donasi/admverifikasi');

        }

        $kondisi['where'] = array("nb_trans_donation.no_transaksi" => $this->db->escape($id), "nb_trans_donation.status" => $this->db->escape("unverified"));

        $cekdonasi = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();

        if (count($cekdonasi) != 1) {

            die("cannot proses");

        } else {

            $campaign = $cekdonasi[0]->campaign;

            $nomuniq = $cekdonasi[0]->nomuniq;



            $kondisi1['where'] = array("nb_campaign.id" => $campaign);

            $cekcampaign = $this->donasimdl->custom_query("nb_campaign", $kondisi1)->result();

            if (count($cekcampaign) > 0) {

                $sumdonatur = (int)$cekcampaign[0]->sum_donatur;

                $now = (int)$cekcampaign[0]->now;*/

                /*updated nb_campaign*/

                /*$data = array("sum_donatur" => (int)$sumdonatur + 1, 'now' => (int)$now + (int)$nomuniq, 'edtd_at' => date("Y-m-d H:i:s"));

                $where = array("nb_campaign.id" => ($campaign),);

                $this->donasimdl->custom_update("nb_campaign", $data, $where);*/

                /*update nb_trans_donation*/

                /*$data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));

                $where = array("nb_trans_donation.no_transaksi" => ($id),);

                $this->donasimdl->custom_update("nb_trans_donation", $data, $where);

                redirect('donasi/admverifikasi/');

            }

        }

    }*/

    /*endof verifikasi campaign*/

    function trxDeposit($iddonatur = null, $nominal = null, $notransaksi = null)



    {



        $kondisi['where'] = array("nb_donaturs_saldo.donatur" => $this->db->escape($iddonatur));



        $ceksaldo = $this->donasimdl->custom_query("nb_donaturs_saldo", $kondisi)->result();



        if (count($ceksaldo) != 1) {

            $dataerror = array('error'=>'Tidak dapat diproses, mohon hubungin administrator');
                    $this->session->set_flashdata($dataerror);
                    $preflink = $this->session->userdata('urllink');
                    redirect($preflink);
                    // redirect('rogram/infaq');
                    exit;


            die("cannot proses");



        } else {



            $balanceAmount = 0;



            $prevBlance = $ceksaldo[0]->balanceAmount;





            $balanceAmount = (int)$ceksaldo[0]->balanceAmount - (int)$nominal;



            if ($balanceAmount < 0) {

                $dataerror = array('error'=>'- Saldo Tidak Cukup, segera isi dompet baik anda');
                    $this->session->set_flashdata($dataerror);
                    $preflink = $this->session->userdata('urllink');
                    redirect($preflink);
                    // redirect('rogram/infaq');
                    exit;

                die("Maaf Saldo Tidak Cukup");



            }



            $data = array('donaturs' => $iddonatur,



                'jenistrx' => "TRX",



                'id_transaksi' => $notransaksi,



                'prevBalance' => $prevBlance,



                'amount' => $nominal,



                'balance' => $balanceAmount,



                'crtd_at' => date("Y-m-d H:i:s"),



                'edtd_at' => date("Y-m-d H:i:s"),



            );



            $this->donasimdl->custom_insert('nb_donaturs_trx', $data);





            $data = array("prevBalance" => $prevBlance,



                'balanceAmount' => $balanceAmount,



                'edtd_at' => date("Y-m-d H:i:s"));



            $where = array("nb_donaturs_saldo.donatur" => $iddonatur);



            $this->donasimdl->custom_update("nb_donaturs_saldo", $data, $where);



            /*update nb_trans_donation*/



            $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));



            $where = array("nb_trans_donation.no_transaksi" => ($notransaksi),);



            $this->donasimdl->custom_update("nb_trans_donation", $data, $where);



        }



    }
    function doVerifikasiCampaign($id = null)

    {



        $kondisi['where'] = array("nb_trans_donation.no_transaksi" => $this->db->escape($id),/* "nb_trans_donation.status" => $this->db->escape("unverified")*/);

        $cekdonasi = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();

        if (count($cekdonasi) != 1) {

            die("cannot proses 1237");

        } else {

            $campaign = $cekdonasi[0]->campaign;

            $nomuniq = $cekdonasi[0]->nomuniq;

            $notransaksi = $cekdonasi[0]->no_transaksi;

            $iddonatur = $cekdonasi[0]->donatur;

            $kodebank = $cekdonasi[0]->bank;

            $kondisi1['where'] = array("nb_campaign.id" => $campaign);

            $cekcampaign = $this->donasimdl->custom_query("nb_campaign", $kondisi1)->result();

            if (count($cekcampaign) > 0) {

                $sumdonatur = (int)$cekcampaign[0]->sum_donatur;

                $now = (int)$cekcampaign[0]->now;

                /*updated nb_campaign*/

                $data = array("sum_donatur" => (int)$sumdonatur + 1, 'now' => (int)$now + (int)$nomuniq, 'edtd_at' => date("Y-m-d H:i:s"));

                $where = array("nb_campaign.id" => ($campaign),);

                $this->donasimdl->custom_update("nb_campaign", $data, $where);

                /*update nb_trans_donation*/

                $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));

                $where = array("nb_trans_donation.no_transaksi" => ($id),);

                $this->donasimdl->custom_update("nb_trans_donation", $data, $where);

            }

        }

    }

}