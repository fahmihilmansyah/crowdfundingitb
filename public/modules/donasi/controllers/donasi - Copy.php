<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class donasiold extends NBFront_Controller
{

    private $_modList = array('donasi/donasi_model' => "donasimdl");

    protected $_data = array();

    function __construct()
    {
        parent::__construct();

        // -----------------------------------
        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN
        // -----------------------------------
        $this->modelsApp();
    }

    private function modelsApp()
    {
        $this->load->model($this->_modList);
    }

    public function index($id = null)
    {

        if (empty($id)) {
            redirect('home');
        }
        if ($_POST) {
            $datadonat = array(
                'prevcampDonation' => $id,
                'prevnomDonation' => $this->input->post('donationNominal'),
                'prevcomDonation' => $this->input->post('donationCommment'),
            );
            $this->session->set_userdata($datadonat);
            redirect('donasi/payment/' . $id);
        } else {

            $condition['where'] = array('nb_campaign.id' => $this->db->escape($id));
            $campaigndata = $this->donasimdl->custom_query("nb_campaign", $condition)->result();
            $data = $this->_data;
            $data['judul'] = $campaigndata;
            $this->parser->parse("index", $data);
        }
    }

    public function payment($id = null)
    {
        $campaing = $this->session->userdata('prevcampDonation');
        $nominal = $this->session->userdata('prevnomDonation');
        $comment = $this->session->userdata('prevcomDonation');
        if (empty($id)) {
            redirect('home');
        }
        if ($campaing != $id || empty($nominal)) {
            redirect('home');
        }
        if ($_POST) {
            $donationName = $this->input->post('donationName');
            $donationEmail = $this->input->post('donationEmail');
            $donationPhone = $this->input->post('donationPhone');
            $methodpay = $this->input->post('donationPay');
            $notrx = date('Ymd') . sprintf('%05d', $this->geninc());
            $uniqcode = $this->genUniq($nominal);
            $insertData = array(
                'no_transaksi' => $notrx,
                'nominal' => $nominal,
                'trx_date' => date('Y-m-d'),
                'campaign' => $campaing,
                'donatur' => 0,/*ubah donaturnya jika login maka akan mengambil session loginnya*/
                'bank' => $methodpay,
                'comment' => $comment,
                'uniqcode' => $uniqcode,
                'namaDonatur' => $donationName,
                'emailDonatur' => $donationEmail,
                'telpDonatur' => $donationPhone,
                'nomuniq' => $nominal + $uniqcode,
                'validDate' => date('Y-m-d', strtotime('+2 days')),
                'crtd_at' => date("Y-m-d H:i:s"),
                'edtd_at' => date("Y-m-d H:i:s"),
            );
            $this->donasimdl->custom_insert("nb_trans_donation", $insertData);
            $array_items = array(
                'prevcampDonation', 'prevnomDonation', 'prevcomDonation');
            $this->session->unset_userdata($array_items);
            $datadonat = array(
                'doneDonation' => $notrx,
            );
            $this->session->set_userdata($datadonat);
            redirect('donasi/summary/' . $id);
        } else {
            $array_items = array(
                'doneDonation');
            $this->session->unset_userdata($array_items);
            $condition['where'] = array('nb_campaign.id' => $this->db->escape($id));
            $campaigndata = $this->donasimdl->custom_query("nb_campaign", $condition)->result();
            $bank = $this->donasimdl->custom_query("nb_bank")->result();
            $data = $this->_data;
            $data['judul'] = $campaigndata;
            $data['bank'] = $bank;
            $this->parser->parse("payment", $data);
        }
    }

    public function summary($id = null)
    {
        if (empty($id)) {
            redirect('home');
        }
        $iddonasi = $this->session->userdata('doneDonation');
        $condition['where'] = array('nb_campaign.id' => $this->db->escape($id));
        $campaigndata = $this->donasimdl->custom_query("nb_campaign", $condition)->result();

        $kondisi['where'] = array('nb_trans_donation.no_transaksi' => $this->db->escape($iddonasi));
        $kondisi['join'] = array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_donation.bank", "type" => "left");
        $summary = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();
        $data = $this->_data;
        $data['judul'] = $campaigndata;
        $data['summary'] = $summary;
        $this->sendMail($summary);
        $this->parser->parse("summary", $data);
    }

    private function genUniq($nominal = 0)
    {
        $condition['where'] = array(
            $this->db->escape($nominal) . ' BETWEEN' => ' minlimit and maxlimit'
        );
        $generate = $this->donasimdl->custom_query("nb_incuniq", $condition)->result();
        $uniqid = $generate[0]->id;
        $uniqinc = $generate[0]->uniqinc + 1;
        if ($uniqinc > $generate[0]->maxuniq) {
            $uniqinc = $generate[0]->minuniq;
        }
        $this->donasimdl->custom_update("nb_incuniq", array('uniqinc' => $uniqinc), array('id' => $uniqid));
        return $uniqinc;
    }

    private function geninc()
    {
        $kondisi['where'] = array('id' => $this->db->escape(1));
        $generate = $this->donasimdl->custom_query("nb_inc", $kondisi)->result();
        $tgl = $generate[0]->tgl;
        $uniqinc = 0;
        if ($tgl != date("Y-m-d")) {
            $uniqinc = $uniqinc + 1;
            //echo $uniqinc."||".$tgl;
        } else {
            $uniqinc = $generate[0]->inc + 1;
            //echo "else : ".$uniqinc;
        }
        $this->donasimdl->custom_update("nb_inc", array('inc' => $uniqinc, 'tgl' => date("Y-m-d")), array('id' => $this->db->escape(1)));
        //echo $this->db->last_query();
        return $uniqinc;
    }

    private function sendMail($summary = null)
    {

        $this->load->library('myphpmailer');
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host = "srv22.niagahoster.com";      // setting GMail as our SMTP server
        // $mail->Host       = "ssl://smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port = 465;                   // SMTP port to connect to GMail
        $mail->Username = "noreply@niatbaik.or.id";  // user email address
        $mail->Password = "#niatbaik2017#";            // password in GMail
        $mail->IsHTML(true);
        $mail->SetFrom($mail->Username, 'No Reply');  //Who is sending the email
        $mail->AddReplyTo($mail->Username, 'No Reply');  //email address that receives the response
        $mail->Subject = "Aktifasi Email";
        $data['summary'] = $summary;
        $mail->Body = $this->load->view('donasi/summary', $data, true);
        $email = $summary[0]->emailDonatur;
        echo "EMAIL: " . $email;
        $mail->AddAddress($email);

        if (!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }


        //print_r($data);
    }

    /*untuk proses verifikasi manual*/
    function admverifikasi()
    {
        $data = $this->_data;
        $kondisi['where'] = array('status not in' => "('verified')");
        $kondisi['join'] = array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_donation.bank", "type" => "left");
        $summary = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();
        $data['donasi'] = $summary;
        $this->parser->parse("donasi/admverifikasi", $data);
    }

    function doVerifikasi($id = null)
    {
        if (empty($id)) {
            redirect('donasi/admverifikasi');
        }
        $kondisi['where'] = array("nb_trans_donation.no_transaksi" => $this->db->escape($id), "nb_trans_donation.status" => $this->db->escape("unverified"));
        $cekdonasi = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();
        if (count($cekdonasi) != 1) {
            die("cannot proses");
        } else {
            $campaign = $cekdonasi[0]->campaign;
            $nomuniq = $cekdonasi[0]->nomuniq;
            /*$notransaksi = $cekdonasi[0]->no_transaksi;
            $iddonatur = $cekdonasi[0]->donatur;*/
            $kondisi1['where'] = array("nb_campaign.id" => $campaign);
            $cekcampaign = $this->donasimdl->custom_query("nb_campaign", $kondisi1)->result();
            if (count($cekcampaign) > 0) {
                $sumdonatur = (int)$cekcampaign[0]->sum_donatur;
                $now = (int)$cekcampaign[0]->now;
                /*updated nb_campaign*/
                $data = array("sum_donatur" => (int)$sumdonatur + 1, 'now' => (int)$now + (int)$nomuniq, 'edtd_at' => date("Y-m-d H:i:s"));
                $where = array("nb_campaign.id" => ($campaign),);
                $this->donasimdl->custom_update("nb_campaign", $data, $where);
                /*update nb_trans_donation*/
                $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));
                $where = array("nb_trans_donation.no_transaksi" => ($id),);
                $this->donasimdl->custom_update("nb_trans_donation", $data, $where);
                redirect('donasi/admverifikasi/');
            }
        }
    }
    /*endof verifikasi campaign*/
    /*untuk proses verifikasi manual khusus program*/
    function admverifikasiprogram()
    {
        die("belum di kerjain");
        $data = $this->_data;
        $kondisi['where'] = array('status not in' => "('verified')");
        $kondisi['join'] = array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_donation.bank", "type" => "left");
        $summary = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();
        $data['donasi'] = $summary;
        $this->parser->parse("donasi/admverifikasi", $data);

    }

    function doVerifikasiprogram($id = null)
    {
        die("belum di kerjain");
        if (empty($id)) {
            redirect('donasi/admverifikasi');
        }
        $kondisi['where'] = array("nb_trans_donation.no_transaksi" => $this->db->escape($id), "nb_trans_donation.status" => $this->db->escape("unverified"));
        $cekdonasi = $this->donasimdl->custom_query("nb_trans_donation", $kondisi)->result();
        if (count($cekdonasi) != 1) {
            die("cannot proses");
        } else {
            $campaign = $cekdonasi[0]->campaign;
            $nomuniq = $cekdonasi[0]->nomuniq;
            $kondisi1['where'] = array("nb_campaign.id" => $campaign);
            $cekcampaign = $this->donasimdl->custom_query("nb_campaign", $kondisi1)->result();
            if (count($cekcampaign) > 0) {
                $sumdonatur = (int)$cekcampaign[0]->sum_donatur;
                $now = (int)$cekcampaign[0]->now;
                /*updated nb_campaign*/
                $data = array("sum_donatur" => (int)$sumdonatur + 1, 'now' => (int)$now + (int)$nomuniq, 'edtd_at' => date("Y-m-d H:i:s"));
                $where = array("nb_campaign.id" => ($campaign),);
                $this->donasimdl->custom_update("nb_campaign", $data, $where);
                /*update nb_trans_donation*/
                $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));
                $where = array("nb_trans_donation.no_transaksi" => ($id),);
                $this->donasimdl->custom_update("nb_trans_donation", $data, $where);
                redirect('donasi/admverifikasi/');
            }
        }
    }
    /*end off verifikasi program*/
    /*fungsi untuk zakat*/
    function zakat()
    {
        if ($_POST) {
            $datadonat = array(
                'prevnomDonation' => $this->input->post('otherNominal'),
                'prevjenisDonation' => $this->input->post('otherJenis'),
            );
            $this->session->set_userdata($datadonat);
            redirect('donasi/otherprosess/');
        } else {
            $data = $this->_data;
            $this->parser->parse("donasi/zakat", $data);
        }
    }

    /*fungsi untuk zakat*/
    function zakatmal()
    {
        if ($_POST) {
            $datadonat = array(
                'prevnomDonation' => $this->input->post('otherNominal'),
                'prevjenisDonation' => $this->input->post('otherJenis'),
            );
            $this->session->set_userdata($datadonat);
            redirect('donasi/otherprosess/');
        } else {
            $data = $this->_data;
            $data['hargaemas'] = 230000;
            $this->parser->parse("donasi/zakatmal", $data);
        }
    }

    /*fungsi untuk infaq*/
    function infaq()
    {
        if ($_POST) {
            $datadonat = array(
                'prevnomDonation' => $this->input->post('otherNominal'),
                'prevjenisDonation' => $this->input->post('otherJenis'),
            );
            $this->session->set_userdata($datadonat);
            redirect('donasi/otherprosess/');
        } else {
            $data = $this->_data;
            $data['hargaemas'] = 230000;
            $this->parser->parse("donasi/infaq", $data);
        }
    }

    /*fungsi untuk shodaqoh*/
    function shodaqoh()
    {
        if ($_POST) {
            $datadonat = array(
                'prevnomDonation' => $this->input->post('otherNominal'),
                'prevjenisDonation' => $this->input->post('otherJenis'),
            );
            $this->session->set_userdata($datadonat);
            redirect('donasi/otherprosess/');
        } else {
            $data = $this->_data;
            $data['hargaemas'] = 230000;
            $this->parser->parse("donasi/shodaqoh", $data);
        }
    }

    function otherprosess()
    {
        if (empty($this->session->userdata('prevnomDonation'))) {
            redirect('donasi/zakat');
        }
        if ($_POST) {
            $datadonat = array(
                'prevcomDonation' => $this->input->post('donationCommment'),
            );
            $this->session->set_userdata($datadonat);
            redirect('donasi/otherpayment/');
        } else {
            $data = $this->_data;
            $this->parser->parse("donasi/otherprosess", $data);
        }
    }

    function otherpayment()
    {
        if (empty($this->session->userdata('prevnomDonation'))) {
            redirect('donasi/zakat');
        }
        $jenis = $this->session->userdata('prevjenisDonation');
        $nominal = $this->session->userdata('prevnomDonation');
        $comment = $this->session->userdata('prevcomDonation');
        if ($_POST) {
            $donationName = $this->input->post('donationName');
            $donationEmail = $this->input->post('donationEmail');
            $donationPhone = $this->input->post('donationPhone');
            $methodpay = $this->input->post('donationPay');
            $notrx = date('Ymd') . sprintf('%05d', $this->geninc());
            $uniqcode = $this->genUniq($nominal);
            $insertData = array(
                'no_transaksi' => $notrx,
                'nominal' => $nominal,
                'trx_date' => date('Y-m-d'),
                'jenistrx' => $jenis,
                'donatur' => 0,/*ubah donaturnya jika login maka akan mengambil session loginnya*/
                'bank' => $methodpay,
                'comment' => $comment,
                'uniqcode' => $uniqcode,
                'namaDonatur' => $donationName,
                'emailDonatur' => $donationEmail,
                'telpDonatur' => $donationPhone,
                'nomuniq' => $nominal + $uniqcode,
                'validDate' => date('Y-m-d', strtotime('+2 days')),
                'crtd_at' => date("Y-m-d H:i:s"),
                'edtd_at' => date("Y-m-d H:i:s"),
            );
            $this->donasimdl->custom_insert("nb_trans_program", $insertData);
            $array_items = array(
                'prevcampDonation', 'prevnomDonation', 'prevcomDonation');
            $this->session->unset_userdata($array_items);
            $datadonat = array(
                'doneDonation' => $notrx,
            );
            $this->session->set_userdata($datadonat);
            redirect('donasi/othersummary/');
        } else {
            $bank = $this->donasimdl->custom_query("nb_bank")->result();
            $data = $this->_data;
            $data['bank'] = $bank;
            $this->parser->parse("donasi/otherpayment", $data);
        }
    }

    function othersummary()
    {

        $iddonasi = $this->session->userdata('doneDonation');

        $kondisi['where'] = array('nb_trans_program.no_transaksi' => $this->db->escape($iddonasi));
        $kondisi['join'] = array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_program.bank", "type" => "left");
        $summary = $this->donasimdl->custom_query("nb_trans_program", $kondisi)->result();
        $data = $this->_data;
        $data['summary'] = $summary;
        //$this->sendMail($summary);
        $this->parser->parse("donasi/othersummary", $data);
    }
}