{_layHeader}
	<div id="pengurus">
		<div class="container-fluid" style="margin-bottom:30px !important">
			<div class="row">
				<div id="imgpengurus" class="susunan">
					<center>
						<h1 class="bold">VISI DAN MISI</h1>
						<br/>
						<p style="width:50%">
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
						</p>
					</center>
				</div>
							
				<div class="konten">
					<div class="row col-sm-6">
						<h3 class="sm-bold">VISI</h3>
							<p>
								YDSF Surabaya sebagai lembaga sosial yang benar-benar amanah serta mampu berperan serta secara aktif dalam mengangkat derajat dan martabat umat Islam, khususnya di Jawa Timur.
							</p>

						<h3 class="sm-bold">MISI</h3>
							<p>
								Mengumpulkan dana masyarakat/ummat baik dalam bentuk zakat, infaq, shadaqah, maupun lainnya dan menyalurkannya dengan amanah, serta secara efektif dan efisien untuk kegiatan-kegiatan:
								<ol type="1">
									<li>Meningkatkan kualitas sekolah-sekolah Islam</li>
									<li>Menyantuni dan memberdayakan anak yatim, miskin, dan terlantar</li>
									<li>Memberdayakan operasional dan fisik masjid, serta memakmurkannnya</li>
									<li>
										Membantu usaha-usaha dakwah dengan memperkuat peranan para dai, khususnya yang berada di daerah pedesaan/terpencil
									</li>
									<li>
										Memberikan bantuan kemanusiaan bagi anggota masyarakat yang mengalami musibah.
									</li>
								</ol>
							</p>
					</div>
				</div>
			</div>
		</div>
	</div>
{_layFooter}