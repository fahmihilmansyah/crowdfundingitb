<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Visimisi extends NBFront_Controller {



	/**

	 * ----------------------------------------

	 * #NB Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_modList   = array();



	protected $_data    = array();



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------

		// $this->modelsApp();

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	public function index($data = array())

	{

		$data 					= $this->_data;



		$this->parser->parse("index",$data);

	}

}

