<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Wakaf extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_modList = array(
        "kurban/kurban_model" => "infaqmdl",
    );


    protected $_data = array();

    private $_SESSION;


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

        $this->_SESSION = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
            $this->session->userdata(LGI_KEY . "login_info") : '';
    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);

        $this->load->library('Fhhlib');
        // REDIRECT IF SESSION EXIST
        // $this->nbauth_front->lookingForAuten();
    }


    public function index($data = array())

    {
        if ($_POST) {
            $datadonat = array(
                'prevnomDonation' => str_replace(".", '', $this->input->post('donationNominal')),//$this->input->post('donationNominal'),
                'prevjenisDonation' => $this->input->post('donationJenis'),
                'prevcomDonation' => $this->input->post('donationCommment'),
                'statusActive' => 'active',
            );
            $this->session->set_userdata($datadonat);

            $jenis = $this->session->userdata('prevjenisDonation');
            $nominal = $this->session->userdata('prevnomDonation');
            $comment = $this->session->userdata('prevcomDonation');
            $donationName = $this->input->post('donationName');
            $donationEmail = $this->input->post('donationEmail');
            $donationPhone = empty($this->input->post('donationPhone'))?'081234567899':$this->input->post('donationPhone');
                $methodpay = $this->input->post('donationPay');
            $error = "";
            if (empty($jenis)) {
                $error .= '- Kesalahan sistem, hubungin administrator. <br>';
            }
            if($nominal < 9999 || !is_numeric($nominal)){
                $error .= '- Nominal dana harus minimal Rp.10.000 <br>';
            }
                if(empty($methodpay)){
                    $error .= '- Metode pembayaran harus dipilih. <br>';
                }
            if (empty($donationEmail) || empty($donationPhone) || strlen($donationPhone) < 9) {
                $error .= '- Email / Telp harus diisi (minimal 10 karakter) <br>';
            }
            if (!empty($error)) {
                $dataerror = array('error' => $error);
                $this->session->set_flashdata($dataerror);
                redirect('wakaf/wakaf#gagalnotif');
                exit;
            }
            $notrx = date('Ymd') . sprintf('%05d', $this->geninc());
            $uniqcode = 0;//$this->genUniq($nominal);
            $iddonatur = empty($this->_SESSION['userid']) ? 0 : $this->_SESSION['userid'];
            $insertData = array(
                'no_transaksi' => $notrx,
                'nominal' => $nominal,
                'trx_date' => date('Y-m-d'),
                'jenistrx' => $jenis,
                'donatur' => $iddonatur,/*ubah donaturnya jika login maka akan mengambil session loginnya*/
                'bank' => $methodpay,
                'comment' => $comment,
                'uniqcode' => $uniqcode,//($methodpay == 919 ? 0 : $uniqcode),
                'namaDonatur' => $donationName,
                'emailDonatur' => $donationEmail,
                'telpDonatur' => $donationPhone,
                'nomuniq' => $nominal + $uniqcode,//($methodpay == 919 ? 0 : $uniqcode),
                'validDate' => date('Y-m-d', strtotime('+2 days')),
                'crtd_at' => date("Y-m-d H:i:s"),
                'edtd_at' => date("Y-m-d H:i:s"),
                'month' => date("m"),
                'year' => date("Y"),
            );
            $this->infaqmdl->custom_insert("nb_trans_program", $insertData);

            $array_items = array(
                'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive');
            $this->session->unset_userdata($array_items);
            $datadonat = array(
                'doneDonation' => $notrx,
            );
            $this->session->set_userdata($datadonat);
            /*cek jika pemilihan melalui dompetku*/
//                if ($methodpay == 919) {
//                    $this->trxDeposit($iddonatur, $nominal, $notrx);
//                    redirect('invoice/depoprogram/'.$notrx);
//                }else{
//                    redirect('invoice/program/'.$notrx);
//                }

            if ($methodpay == 'QRISVA') {
                $condition['where'] = array(
                    'nb_campaign.id' => $this->db->escape($id),
                    'nb_campaign.valid_date >= ' => $this->db->escape(date('Y-m-d'))
                );
                $campaigndata = $this->infaqmdl->custom_query("nb_campaign", $condition)->result();
                $findqris = $this->infaqmdl->custom_query('nb_sys_users', ['where' => ['id' => $campaigndata[0]->user_post]])->result();
                $data = $this->_data;
                $data['qris'] = $findqris;
                $data['nominal'] = $nominal;
                return $this->parser->parse("view_qris", $data);
            }

            if ($methodpay == 'SALDO_DOMPET' && !empty($this->_SESSION['userid'])) {
                $condition['where'] = array(
                    'donatur' => $this->db->escape($this->_SESSION['userid']),
                );
                $ceksaldo = $this->infaqmdl->custom_query("nb_donaturs_saldo", $condition)->result();
                if ($ceksaldo[0]->balanceAmount < $nominal):
                    $dataerror = array('error' => 'Saldo anda tidak mencukupi. Saldo : ' . $ceksaldo[0]->balanceAmount);
                    $this->session->set_flashdata($dataerror);
                    redirect('wakaf' );
                    exit;
                endif;
                Fhhlib::updateSaldo($this->_SESSION['userid'],$nominal,$notrx);
                Fhhlib::updateTrans('nb_trans_program',$notrx,'verified');
                $data = $this->_data;
                $data['title'] = 'Transaksi Sukses';
                $data['trxid'] = $notrx;
                return $this->parser->parse("v_sukses", $data);
            }

            $idipg = Fhhlib::uuidv4Nopad();

            $arrParam['idipg'] = $idipg;
            $expmethod = explode('|', $methodpay);
            if ($expmethod[0] == 'FINPAY') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = $donationPhone;
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["Wakaf",' . $nominal . ',1]]';
                    }
                }
//            $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                $arrParam['items'] = "Pembayaran Wakaf";
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = "Pembayaran Wakaf";//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_program',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
//            echo "masuk if";exit;
                $res = Fhhlib::finpayIPG($arrParam);
                if (!empty($res)) {
//                header("Location: ".$res);
//                redirect($res, 'refresh');
                    if ($res['status_code'] == '00') {

                        if (!empty($res['landing_page'])) {
                            redirect($res['landing_page'], 'refresh');
                        } elseif (!empty($res['redirect_url'])) {
                            redirect($res['redirect_url'], 'refresh');
                        } else {
                            $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                            $this->session->set_flashdata($dataerror);
                            redirect('kurban');
                        }
//                    redirect($res['landing_page'],'refresh');
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('wakaf');
                        exit;
                    }
//            print_r($res);exit;
                } else {
                    redirect('wakaf');

                }
            }elseif ($expmethod[0] == 'MUPAYS') {
                $carilist = $this->db->from('nb_list_ipg_method')->where(['partner' => $expmethod[0], 'kode_va' => $expmethod[1]])->get()->result();
                $arrParam['amount'] = $nominal;
                $arrParam['cust_email'] = $donationEmail;
                $arrParam['cust_id'] = time() . $iddonatur;
                $arrParam['cust_msisdn'] = ($donationPhone);
                $arrParam['cust_name'] = $donationName;
                $arrParam['invoice'] = $notrx;
                $arrParam['token_login'] = !empty($this->_SESSION['mupays_token'])?$this->_SESSION['mupays_token']:"";
                $arrParam['items'] = "Pembayaran Wakaf";
                if (!empty($carilist)) {
                    if ($carilist[0]->typeipg == 'emoney') {
                        $arrParam['items'] = '[["Donasi",' . $nominal . ',1]]';
                    }
                }
                $arrParam['failed_url'] = base_url('trxipg/failed/' . $idipg);
                $arrParam['return_url'] = base_url('trxipg/return/' . $idipg);
                $arrParam['success_url'] = base_url('trxipg/success/' . $idipg);
                $arrParam['timeout'] = "120";//in minutes
                $arrParam['trans_date'] = date("YmdHis");//in minutes
                $arrParam['add_info1'] = $donationName;//in minutes
                $arrParam['add_info2'] = "Pembayaran Wakaf";//in minutes
                $arrParam['sof_id'] = $expmethod[1];//in minutes
                $arrParam['sof_type'] = "pay";//in minutes

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_donation',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $res = Fhhlib::devfinpayIPG($arrParam);
//            $res = $this->finpayIPG($nominal,$donationEmail,$iddonatur,$donationPhone,$donationName,$notrx);
                if (!empty($res)) {
                    if ($res['rc'] == '0000') {
                        if (!empty($res['data']['no_va'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['hasil'] = $res['data'];
                            return $this->parser->parse("view_vatrx", $datas);
                        }
                        if (!empty($res['data']['landing_page'])) {
                            return redirect($res['data']['landing_page'], 'refresh');
                        }
                        if (!empty($res['data']['qris'])) {
//                            redirect($res['landing_page'], 'refresh');
                            $datas = $this->_data;
                            $datas['url_img'] = $res['data']['qris']['url_img'];
                            $datas['nominal'] = $nominal;
                            return $this->parser->parse("view_qris", $datas);
                        }
                        exit;
                    } else {
                        $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . json_encode($res));
                        $this->session->set_flashdata($dataerror);
                        redirect('wakaf');
                        exit;
                    }
                }
//                header("Location: ".$res);
                print_r($res);
                exit;
//            print_r($res);exit;
            }elseif($expmethod[0] == 'E2PAY') {
                $cariipg =  $this->infaqmdl->custom_query('nb_partner_ipg', ['where'=>['id' => $this->db->escape('E2PAY')]])->result();
                $key = !empty($cariipg[0]->key) ? $cariipg[0]->key : "v1XdZSLMs9";
                $merchant_id = !empty($cariipg[0]->merchant_key) ? $cariipg[0]->merchant_key : "IF00294";
                $merchantkey =$key;//'v1XdZSLMs9';
                $merchantcode =$merchant_id;//'IF00294';
                $arrParam['url_host'] = $cariipg[0]->url_host;
                $arrParam['MerchantCode'] = $merchantcode;
                $arrParam['PaymentId'] = $expmethod[1];
                $arrParam['RefNo'] = $notrx;
                $arrParam['Amount'] =$nominal."00";
                $arrParam['Currency'] = 'IDR';
                $arrParam['ProdDesc'] = "Pembayaran Wakaf ";
                $arrParam['UserName'] = $donationName;
                $arrParam['UserEmail'] = $donationEmail;
                $arrParam['UserContact'] = $donationPhone;
                $signatures = Fhhlib::E2Pay_signature($merchantkey.$merchantcode.$arrParam['RefNo'].$arrParam['Amount'].$arrParam['Currency']);
                $arrParam['Signature'] = $signatures;
                $arrParam['Remark'] = '';
                $arrParam['ResponseURL'] = base_url('trxipg/responsee2pay');
                $arrParam['BackendURL'] = base_url('trxipg/backende2pay');

                $insipgtrx = array('id' => $idipg,
                    'trxid' => $notrx,
                    'status' => 'WAITING',
                    'target_table' => 'nb_trans_program',
                    'created_ad' => date('Y-m-d H:i:s'),
                    'updated_ad' => date('Y-m-d H:i:s'),
                    'request_json' => json_encode($arrParam)
                );

//                $this->infaqmdl->custom_insert("nb_trans_ipg", $insipgtrx);
                $this->infaqmdl->custom_insert("nb_trans_e2pay", $insipgtrx);
                $datas = $this->_data;
                $datas['parame2pay']=$arrParam;
////            print_r($datas);
                return $this->parser->parse("view_e2pay", $datas);

                $dataerror = array('error' => 'Tidak dapat mengaksess sistem pembayaran. ' . $res);
                $this->session->set_flashdata($dataerror);
                redirect('donasi/index/' . $id);

            }
            exit;
            redirect('wakaf');
        } else {
            if ($this->session->userdata('prevjenisDonation') != "wakaf") {
                $array_items = array(
                    'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive');
                $this->session->unset_userdata($array_items);
            }
            $data = $this->_data;
//            $kondisi['where'] = array('nb_bank.id not in'=>"('".$this->db->escape(919)."')");
//            $bank = $this->infaqmdl->custom_query("nb_bank", $kondisi)->result();
//            $data['bank']=$bank;
//
//            // ----------------------------------------------------------------------------------------------------
//            // STATIC PAGE DATE
//            // ----------------------------------------------------------------------------------------------------
//            $spagedata              = $this->shadaqah->getDataShadaq()->num_rows();
//            $data["spage_shadaq"]   = ($spagedata > 0) ? $this->shadaqah->getDataShadaq()->row_array() : array();
//
//            $spagedata              = $this->shadaqah->getDataInfaq()->num_rows();
//            $data["spage_infaq"]    = ($spagedata > 0) ? $this->shadaqah->getDataInfaq()->row_array() : array();
//
//            $spagedata              = $this->shadaqah->getDataZakat()->num_rows();
//            $data["spage_zakat"]    = ($spagedata > 0) ? $this->shadaqah->getDataZakat()->row_array() : array();
//            // ----------------------------------------------------------------------------------------------------
//
//            $data["_fdonasi"] = $this->parser->parse('form/donasi', $data, TRUE);
//            $data["_fpembayaran"] = $this->parser->parse('form/pembayaran', $data, TRUE);

            $kondisi['where'] = array('nb_trans_program_kategori.type' => $this->db->escape("WAKAF"));
            $data['trxzakat'] = $this->infaqmdl->custom_query("nb_trans_program_kategori", $kondisi)->result();
            $this->parser->parse("index", $data);
        }
    }


    private function createSignature($arrParam, $key)
    {
        ksort($arrParam, 0);
        $signature = hash('sha256', strtoupper(implode("%", $arrParam)) . "%" . $key);;
        return $signature;
    }

    function finpayIPG($arrParam)
    {
//    function finpayIPG($amount,$email,$cust_id,$cust_msisdn,$cust_name,$invoice){

        $key = "21-DD2194-79";
        $merchant_id = "DD2194";
//        $arrParam2=array(
//            "amount" => $amount,
//            "cust_email" => $email,
//            "cust_id" => 1234849,//$cust_id,
//            "cust_msisdn" => $cust_msisdn,
//            "cust_name" => $cust_name,
//            "failed_url" => "http://194.31.53.26/test/failed.php",
//            "invoice" => $invoice,
//            "merchant_id" => $merchant_id,
//            "items" => '[["Donasi",'.$amount.',1]]',
//            "return_url" => "http://194.31.53.26/test/return.php",
//            "success_url" => "http://194.31.53.26/test/success.php",
//            "timeout" => "120", //in minutes
//            "trans_date" => date("YmdHis"),
//            "add_info1" => "Pembayaran Donasi Ganeshabisa",
//            // "add_info2" => "dua",
//            // "add_info3" => "tiga",
//            // "add_info4" => "empat",
//            // "add_info5" => "lima",
//            "sof_id" => "cc",
//            "sof_type" => "pay"
//        );
        $arrParam1 = array(
            "amount" => "1501",
            "cust_email" => "fahmi.hilmansyah@gmail.com",
            "cust_id" => "282813888",
            "cust_msisdn" => "0817170820",
            "cust_name" => "Fahmi Hilmansyah",
            "failed_url" => "http://194.31.53.26/test/failed.php",
            "invoice" => "INV12300021",
            "merchant_id" => $merchant_id,
            "items" => '[["Donasi",1501,1]]',
            "return_url" => "http://194.31.53.26/test/return.php",
            "success_url" => "http://194.31.53.26/test/success.php",
            "timeout" => "120", //in minutes
            "trans_date" => date("YmdHis"),
            "add_info1" => "Pembayaran Donasi Ganeshabisa",
            // "add_info2" => "dua",
            // "add_info3" => "tiga",
            // "add_info4" => "empat",
            // "add_info5" => "lima",
            "sof_id" => "mandiriclickpay",
            "sof_type" => "pay"
        );
        $arrParam['merchant_id'] = $merchant_id;
        //call the function, and Voila!, this is the correct signature
        $signature = $this->createSignature($arrParam, $key);
        $arrParam = array_merge($arrParam, array("mer_signature" => $signature));
// exit;

//Post the data
        $postfields = http_build_query($arrParam);
//        echo "<pre>";print_r($arrParam);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, "https://sandbox.finpay.co.id/servicescode/api/pageFinpay.php");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
//            print_r($response);exit;
            $response = json_decode($response, true);
            if ($response['status_code'] == '00') {
                return $response['landing_page'];
            } else {
                return false;
            }
            print_r($response);
        }
    }


    private function geninc()
    {
        $kondisi['where'] = array('id' => $this->db->escape(1));
        $generate = $this->infaqmdl->custom_query("nb_inc", $kondisi)->result();
        $tgl = $generate[0]->tgl;
        $uniqinc = 0;
        if ($tgl != date("Y-m-d")) {
            $uniqinc = $uniqinc + 1;
            //echo $uniqinc."||".$tgl;
        } else {
            $uniqinc = $generate[0]->inc + 1;
            //echo "else : ".$uniqinc;
        }
        $this->infaqmdl->custom_update("nb_inc", array('inc' => $uniqinc, 'tgl' => date("Y-m-d")), array('id' => $this->db->escape(1)));
        //echo $this->db->last_query();
        return $uniqinc;
    }

    private function genUniq($nominal = 0)
    {
        $condition['where'] = array(
            $this->db->escape($nominal) . ' BETWEEN' => ' minlimit and maxlimit'
        );
        $generate = $this->infaqmdl->custom_query("nb_incuniq", $condition)->result();
        $uniqid = $generate[0]->id;
        $uniqinc = $generate[0]->uniqinc + 1;
        if ($uniqinc > $generate[0]->maxuniq) {
            $uniqinc = $generate[0]->minuniq;
        }
        $this->infaqmdl->custom_update("nb_incuniq", array('uniqinc' => $uniqinc), array('id' => $uniqid));
        return $uniqinc;
    }

    private function deleteNoTrx($notrx = null)
    {
        $deldata = array('no_transaksi' => $notrx);
        $this->infaqmdl->custom_delete("nb_trans_program", $deldata);
    }

    function trxDeposit($iddonatur = null, $nominal = null, $notransaksi = null)

    {

        $kondisi['where'] = array("nb_donaturs_saldo.donatur" => $this->db->escape($iddonatur));

        $ceksaldo = $this->infaqmdl->custom_query("nb_donaturs_saldo", $kondisi)->result();

        if (count($ceksaldo) != 1) {

            $this->deleteNoTrx($notransaksi);
            $dataerror = array('error' => 'Tidak dapat diproses, mohon hubungin administrator');
            $this->session->set_flashdata($dataerror);
            $preflink = $this->session->userdata('urllink');
            redirect($preflink . "#gagalnotif");
            exit;

            die("cannot proses");

        } else {

            $balanceAmount = 0;

            $prevBlance = $ceksaldo[0]->balanceAmount;


            $balanceAmount = (int)$ceksaldo[0]->balanceAmount - (int)$nominal;

            if ($balanceAmount < 0) {

                $this->deleteNoTrx($notransaksi);
                $dataerror = array('error' => '- Saldo Tidak Cukup, segera isi dompet baik anda');
                $this->session->set_flashdata($dataerror);
                $preflink = $this->session->userdata('urllink');
                redirect($preflink . "#gagalnotif");
                exit;
                die("Maaf Saldo Tidak Cukup");

            }

            $data = array('donaturs' => $iddonatur,

                'jenistrx' => "TRX",

                'id_transaksi' => $notransaksi,

                'prevBalance' => $prevBlance,

                'amount' => $nominal,

                'balance' => $balanceAmount,

                'crtd_at' => date("Y-m-d H:i:s"),

                'edtd_at' => date("Y-m-d H:i:s"),

            );

            $this->infaqmdl->custom_insert('nb_donaturs_trx', $data);


            $data = array("prevBalance" => $prevBlance,

                'balanceAmount' => $balanceAmount,

                'edtd_at' => date("Y-m-d H:i:s"));

            $where = array("nb_donaturs_saldo.donatur" => $iddonatur);

            $this->infaqmdl->custom_update("nb_donaturs_saldo", $data, $where);

            /*update nb_trans_program*/

            $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));

            $where = array("nb_trans_program.no_transaksi" => ($notransaksi),);

            $this->infaqmdl->custom_update("nb_trans_program", $data, $where);

        }

    }

}

