<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Zakat extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_modList = array(
                                "zakat/Zakat_model" => "zakatmdl",
                                "shadaqah/Shadaqah_model" => "shadaqah"
                            );


    protected $_data = array();

    private $_SESSION;


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

        $this->_SESSION   = !empty($this->session->userdata(LGI_KEY . "login_info")) ? 
                                   $this->session->userdata(LGI_KEY . "login_info") : '';
    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);
        // $this->nbauth_front->lookingForAuten();

    }


    public function index($data = array())

    {
        if ($_POST) {
            if (!empty($this->input->post('subnominal'))) {
                $datadonat = array(
                    'prevnomDonation' => $this->input->post('donationNominal'),
                    'prevjenisDonation' => $this->input->post('otherJenis'),
                    'prevcomDonation' => $this->input->post('donationCommment'),
                    'statusActive' => 'active',
                );
                $this->session->set_userdata($datadonat);
                if(empty($this->input->post('donationNominal')) || $this->input->post('donationNominal') == 0){
                    $error = "Nominal Tidak Boleh Kosong '0'.";
                    $dataerror = array('error'=>$error);
                    $this->session->set_flashdata($dataerror);
                    redirect('zakat#gagalnotif');
                    exit;
                }
            }
            if (!empty($this->input->post('subbayar'))) {
                $jenis = $this->session->userdata('prevjenisDonation');
                $nominal = $this->session->userdata('prevnomDonation');
                $comment = $this->session->userdata('prevcomDonation');
                $donationName = $this->input->post('donationName');
                $donationEmail = $this->input->post('donationEmail');
                $donationPhone = $this->input->post('donationPhone');
                $methodpay = $this->input->post('donationPay');
                $error = "";
                if(empty($jenis)){
                    $error .= '- Kesalahan sistem, hubungin administrator. <br>';
                }
                if($nominal < 29999 || !is_numeric($nominal)){
                    $error .= '- Nominal dana harus minimal Rp.30.000 <br>';
                }
                if(empty($methodpay)){
                    $error .= '- Metode pembayaran harus dipilih.<br>';
                }
                if(empty($donationEmail) || empty($donationPhone) || strlen($donationPhone) < 9){
                    $error .= '- Email / Telp harus diisi (minimal 10 karakter) <br>';
                }
                if(!empty($error)){
                    $dataerror = array('error'=>$error);
                    $this->session->set_flashdata($dataerror);
                    redirect('zakat#gagalnotif');
                    exit;
                }
                $notrx = date('Ymd') . sprintf('%05d', $this->geninc());
                $uniqcode = $this->genUniq($nominal);
                $iddonatur = empty($this->_SESSION['userid'])?0:$this->_SESSION['userid'];
                $insertData = array(
                    'no_transaksi' => $notrx,
                    'nominal' => $nominal,
                    'trx_date' => date('Y-m-d'),
                    'jenistrx' => $jenis,
                    'donatur' => $iddonatur,/*ubah donaturnya jika login maka akan mengambil session loginnya*/
                    'bank' => $methodpay,
                    'comment' => $comment,
                    'uniqcode' => ($methodpay == 919 ? 0 : $uniqcode),
                    'namaDonatur' => $donationName,
                    'emailDonatur' => $donationEmail,
                    'telpDonatur' => $donationPhone,
                    'nomuniq' => $nominal + ($methodpay == 919 ? 0 : $uniqcode),
                    'validDate' => date('Y-m-d', strtotime('+2 days')),
                    'crtd_at' => date("Y-m-d H:i:s"),
                    'edtd_at' => date("Y-m-d H:i:s"),
                    'month' => date("m"),
                    'year' => date("Y"),
                );
                $this->zakatmdl->custom_insert("nb_trans_program", $insertData);

                $array_items = array(
                    'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive');
                $this->session->unset_userdata($array_items);
                $datadonat = array(
                    'doneDonation' => $notrx,
                );
                $this->session->set_userdata($datadonat);
                /*cek jika pemilihan melalui dompetku*/
                if ($methodpay == 919) {
                    $this->trxDeposit($iddonatur, $nominal, $notrx);
                    redirect('invoice/depoprogram/'.$notrx);
                }else{
                    redirect('invoice/program/'.$notrx);
                }
            }
            redirect('zakat');
        } else {
	    if($this->session->userdata('prevjenisDonation') != "zakatmaal" && $this->session->userdata('prevjenisDonation') != "zakatprofesi"){
                $array_items = array(
                    'prevcampDonation', 'prevnomDonation', 'prevcomDonation', 'statusActive');
                $this->session->unset_userdata($array_items);
            }
            $data = $this->_data;
            $kondisi['where'] = array('nb_bank.id not in'=>"('".$this->db->escape(919)."')");
            $bank = $this->zakatmdl->custom_query("nb_bank", $kondisi)->result();
            $data['bank']=$bank;
            
            // ----------------------------------------------------------------------------------------------------
            // STATIC PAGE DATE
            // ----------------------------------------------------------------------------------------------------
            $spagedata              = $this->shadaqah->getDataShadaq()->num_rows();
            $data["spage_shadaq"]   = ($spagedata > 0) ? $this->shadaqah->getDataShadaq()->row_array() : array();
            
            $spagedata              = $this->shadaqah->getDataInfaq()->num_rows();
            $data["spage_infaq"]    = ($spagedata > 0) ? $this->shadaqah->getDataInfaq()->row_array() : array();
            
            $spagedata              = $this->shadaqah->getDataZakat()->num_rows();
            $data["spage_zakat"]    = ($spagedata > 0) ? $this->shadaqah->getDataZakat()->row_array() : array();
            // ----------------------------------------------------------------------------------------------------

            $data["_fmall"] = $this->parser->parse('form/zakatmal', $data, TRUE);
            $data["_fprofesi"] = $this->parser->parse('form/zakatprofesi', $data, TRUE);
            $data["_fdonasi"] = $this->parser->parse('form/donasi', $data, TRUE);
            $data["_fpembayaran"] = $this->parser->parse('form/pembayaran', $data, TRUE);

            $this->parser->parse("index", $data);
        }
    }
    private function deleteNoTrx($notrx=null){
        $deldata = array('no_transaksi' => $notrx);
        $this->zakatmdl->custom_delete("nb_trans_program", $deldata);
    }
    private function geninc()
    {
        $kondisi['where'] = array('id' => $this->db->escape(1));
        $generate = $this->zakatmdl->custom_query("nb_inc", $kondisi)->result();
        $tgl = $generate[0]->tgl;
        $uniqinc = 0;
        if ($tgl != date("Y-m-d")) {
            $uniqinc = $uniqinc + 1;
            //echo $uniqinc."||".$tgl;
        } else {
            $uniqinc = $generate[0]->inc + 1;
            //echo "else : ".$uniqinc;
        }
        $this->zakatmdl->custom_update("nb_inc", array('inc' => $uniqinc, 'tgl' => date("Y-m-d")), array('id' => $this->db->escape(1)));
        //echo $this->db->last_query();
        return $uniqinc;
    }

    private function genUniq($nominal = 0)
    {
        $condition['where'] = array(
            $this->db->escape($nominal) . ' BETWEEN' => ' minlimit and maxlimit'
        );
        $generate = $this->zakatmdl->custom_query("nb_incuniq", $condition)->result();
        $uniqid = $generate[0]->id;
        $uniqinc = $generate[0]->uniqinc + 1;
        if ($uniqinc > $generate[0]->maxuniq) {
            $uniqinc = $generate[0]->minuniq;
        }
        $this->zakatmdl->custom_update("nb_incuniq", array('uniqinc' => $uniqinc), array('id' => $uniqid));
        return $uniqinc;
    }

    function trxDeposit($iddonatur = null, $nominal = null, $notransaksi = null)

    {

        $kondisi['where'] = array("nb_donaturs_saldo.donatur" => $this->db->escape($iddonatur));

        $ceksaldo = $this->zakatmdl->custom_query("nb_donaturs_saldo", $kondisi)->result();

        if (count($ceksaldo) != 1) {
            $this->deleteNoTrx($notransaksi);
            $dataerror = array('error'=>'Tidak dapat diproses, mohon hubungin administrator');
                    $this->session->set_flashdata($dataerror);
                    $preflink = $this->session->userdata('urllink');
                    redirect($preflink."#gagalnotif");
                    // redirect('program/zakat');
                    exit;
            die("cannot proses");

        } else {

            $balanceAmount = 0;

            $prevBlance = $ceksaldo[0]->balanceAmount;


            $balanceAmount = (int)$ceksaldo[0]->balanceAmount - (int)$nominal;

            if ($balanceAmount < 0) {
                
            $this->deleteNoTrx($notransaksi);
                $dataerror = array('error'=>'- Saldo Tidak Cukup, segera isi dompet baik anda');
                    $this->session->set_flashdata($dataerror);
                    $preflink = $this->session->userdata('urllink');
                    redirect($preflink."#gagalnotif");

                    // redirect('rogram/zakat');
                    exit;
                die("Maaf Saldo Tidak Cukup");

            }

            $data = array('donaturs' => $iddonatur,

                'jenistrx' => "TRX",

                'id_transaksi' => $notransaksi,

                'prevBalance' => $prevBlance,

                'amount' => $nominal,

                'balance' => $balanceAmount,

                'crtd_at' => date("Y-m-d H:i:s"),

                'edtd_at' => date("Y-m-d H:i:s"),

            );

            $this->zakatmdl->custom_insert('nb_donaturs_trx', $data);


            $data = array("prevBalance" => $prevBlance,

                'balanceAmount' => $balanceAmount,

                'edtd_at' => date("Y-m-d H:i:s"));

            $where = array("nb_donaturs_saldo.donatur" => $iddonatur);

            $this->zakatmdl->custom_update("nb_donaturs_saldo", $data, $where);

            /*update nb_trans_program*/

            $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));

            $where = array("nb_trans_program.no_transaksi" => ($notransaksi),);

            $this->zakatmdl->custom_update("nb_trans_program", $data, $where);

        }

    }

}

