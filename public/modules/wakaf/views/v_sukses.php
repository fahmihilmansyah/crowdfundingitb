{_layHeader}

<div class="content">

    <div class="card mt-10">
        <div class="card-body">
            <div class="row">
                <div class="col text-center">
                    <img src="<?php echo base_url().'assets/images/icons/checked.png' ?>" width="100"><br/>
                    <label class="btn btn-success text-white" style="margin-top: 20px;"> <?php echo $title ?></label>
                    <br/>
                    <br/>
                    <span>No Transaksi: <?php echo $trxid ?></span>
                </div>
            </div>
           
        </div>
    </div>


    {_layFooter}

