{_layHeader}

<?php
$url = implode("/", $this->uri->segment_array());
$dats = array('urllink' => $url);
$this->session->set_userdata($dats);
$sessionsl = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
    $this->session->userdata(LGI_KEY . "login_info")['email'] : '';
$sessionsn = !empty($this->session->userdata(LGI_KEY . "login_info")) ? $this->session->userdata(LGI_KEY . "login_info")['fname']." ".$this->session->userdata(LGI_KEY . "login_info")['login_lname'] : '';
$sessionsp = !empty($this->session->userdata(LGI_KEY . "login_info")) ? $this->session->userdata(LGI_KEY . "login_info")['phone'] : '';

?>
<?php $cek = empty($this->session->userdata('prevnomDonation')) ? "" : "active"; ?>
<div class="content pr-15 pl-15 bg-white" style="padding-top: 10px;">
    <?php if(!empty($this->session->flashdata('error'))):?>
        <div class="alert alert-danger alert-dismissable " role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <script type="text/javascript">
            setTimeout(function() {
                $('.alert').fadeOut();
            }, 10000); // <-- time in milliseconds
        </script>
    <?php endif;?>
    <div class="mb-40 text-center">
        <div class="h3 bold title-font lh-1 color-second"><?php echo empty($judul[0]->title) ? "" : $judul[0]->title; ?></div>
    </div>
    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'wakaf/wakaf/' . $this->uri->segment(3), array('id' => 'fmdonasi','class' => 'fmdonasi mt-20'), $hidden_form);

    ?>
    <div class="mb-20">
        <div class="form-group">
            <label>Jenis Wakaf</label>
            <select class="form-control m-input" name="donationJenis">
                <?php foreach ($trxzakat as $r): ?>
                <option value="<?php echo  $r->kategoricode ?>"><?php echo  $r->kategoriname ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label>Masukkan Nominal</label>
            <input type="text" class="form-control" id="denomnms" name="donationNominal" placeholder="Rp. 50.000">
        </div>
        <hr/>
        <div class="form-group">

            <input type="hidden" name="donationPay" id="paymetod">
            <div class="row" >
                <div class="col-12">
                    <span><b><h5>Metode Pembayaran : </h5></b></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div id="respemb"></div>
        </div>
        <hr/>
        <?php if(empty($sessionsl)): ?>
            <div class="form-group text-center">
                <span><a id="pagelogin" href="<?php echo base_url('/register')?>">Daftar</a> atau lengkapi data dibawah ini.</span>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label>Nama</label>
            <input name="donationName" type="text" <?php echo !empty($sessionsn)?'readonly':''; ?> value="<?php echo !empty($sessionsn)?$sessionsn:''; ?>" class="form-control" placeholder="Masukkan Nama Anda">
        </div>
        <div class="form-group">
            <label>No. Handphone</label>
            <input name="donationPhone" <?php echo !empty($sessionsp)?'readonly':''; ?> value="<?php echo !empty($sessionsp)?$sessionsp:''; ?>" type="text" class="form-control" placeholder="Masukkan Nomor Handphone Anda">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input name="donationEmail" <?php echo !empty($sessionsl)?'readonly':''; ?> value="<?php echo !empty($sessionsl)?$sessionsl:''; ?>" type="text" class="form-control"  placeholder="Masukkan Email Anda">
        </div>
        <div class="form-group">
            <label>Komentar Dukungan Anda</label><i>(optional)</i>
            <textarea name="donationCommment" class="form-control" placeholder="Berikan Komentar, Doa, atau Dukungan Anda"
                      rows="3"></textarea>
        </div>

        <div class="mt-30">
            <button class="btn btn-block btn-main"><span>Bayar</span></button>
        </div>

    <?php

    echo form_close();

    ?>
    <div class="mb-10"></div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>

        <script>

            <?php $paramsd = [];
            foreach ($_GET as $k=>$v):
                $paramsd[] = $k."=".$v;
            endforeach;
            $setparams = implode('&',$paramsd);
            ?>
            $("#respemb").load("<?php echo base_url('/pembayaran/index?'.$setparams); ?>");
            jQuery(document).on('click', '#pilbayar', function () {
                $("#settmpl").load("<?php echo base_url('/pembayaran/index') ?>");
            });
            var fnf = document.getElementById("denomnms");
            fnf.addEventListener('keyup', function(evt){
                var n = parseInt(this.value.replace(/\D/g,''),10);
                fnf.value = n.toLocaleString("id-ID");
            }, false);
        </script>
{_layFooter}

