{_layHeader}
<?php
$url = implode("/",$this->uri->segment_array());
$dats = array('urllink'=>$url);
$this->session->set_userdata($dats);
?>
	<div id="zakat">

		<div class="container-fluid mb20">

			<div class="row">

				<!-- PROGRAM CAMPAIGN -->

				<div id="program-zakat">

					<div class="container">

						<div clas="row">

							<div class="col-md-12">

								<div class="program-zakat-title">

									<center>

										<h2 class="bold">PROGRAM DONASI YDSF</h2>

										<hr class="line line-white">

										<h5>

											Bantuan Anda, berapa pun jumlahnya, sangat bermanfaat buat mereka yang membutuhkan. <br/>

											Kami menyalurkan seluruh bantuan amanat dari maysarakat untuk sebesar-besar kemanfaatan yang layak menerimanya.

										</h5>

									</center>

								</div>

							</div>

						</div>



						<div class="row">

							<div class="col-md-4" style="margin-bottom:20px">

								<a href="<?php echo URI::baseURL(); ?>program/infaq" class="program-zakat-list infaq-h">

									<div class="list-program">

										<center>
											<div>
												<img src="<?php URI::baseURL(); ?>assets/images/icon/noninfaq.png">
											</div>

											<h2 class="bold" style="color:#ccc">INFAQ</h2>

											<hr class="line" style="border-color:#ccc">

										</center>

									</div>

									<div class="list-konten">

										<p class="p-program" style="color:#ccc">

											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

										</p>

									</div>

									<div class="list-btn padd-rl-50" style="padding-bottom:20px">

										

									</div>

									<div class="program-zakat-add-block">

										

									</div>

								</a>

							</div>

							<div class="col-md-4" style="margin-bottom:20px">

								<a href="<?php echo URI::baseURL(); ?>program/shadaqah" class="program-zakat-list shadaqah-h">

									<div class="list-program">

										<center>
											<div>
												<img src="<?php URI::baseURL(); ?>assets/images/icon/nonshadaqah.png">
											</div>

											<h2 class="bold" style="color:#ccc">SHADAQAH</h2>

											<hr class="line" style="border-color:#ccc">

										</center>

									</div>

									<div class="list-konten">

										<p class="p-program" style="color:#ccc">

											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

										</p>

									</div>

									<div class="list-btn padd-rl-50" style="padding-bottom:20px">

										

									</div>

									<div class="program-zakat-add-block">

										

									</div>

								</a>

							</div>

							<div class="col-md-4" style="margin-bottom:20px">

								<div class="program-zakat-list">

									<div class="list-program">

										<center>

											<img src="<?php URI::baseURL(); ?>assets/images/icon/zakat.png">

											<h2 class="bold">ZAKAT MAAL</h2>

											<hr class="line">

										</center>

									</div>

									<div class="list-konten">

										<p class="p-program">

											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

										</p>

									</div>

									<div class="list-btn padd-rl-50" style="padding-bottom:20px">

									

									</div>

									<div class="program-zakat-add-block green-focus">

										

									</div>

								</div>

							</div>

						</div>

					</div>

                    <div id="kalkulator_zakatx"></div>
				</div>


				<div id="kalkulator_zakat">

					<div class="container">

						<div clas="row" id="gagalnotif" >

							<center>

								<h2 class='bold' >KALKULATOR ZAKAT</h2>

								<hr/><br>
								
<?php if(!empty($this->session->flashdata('error'))):?>
            <div class="alert alert-danger alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('error'); ?>
            </div>
            <script type="text/javascript">
            	setTimeout(function() {
				    $('.alert').fadeOut();
				}, 10000); // <-- time in milliseconds
            </script>
        <?php endif;?>

							</center>



							<br clear="all"/>



							<div class="tab-info">

								<!-- Nav tabs -->

								<div class="tab-nb">

									<ul id="tab-zakat" class="nav nav-tabs" role="tablist">

										<li role="presentation" class="active w50 border-r">

											<a href="#zakatmaal" aria-controls="zakatmaal" role="tab" data-isi="zakatmaal" data-toggle="tab" class="no-bt tabspanel">

												<center><h5 class="bold">Zakat Maal</h5></center>

											</a>

										</li>

										<li role="presentation" class="w50"">

											<a href="#zakatprofesi" aria-controls="zakatprofesi" role="tab" data-isi="zakatprofesi" data-toggle="tab" class="no-bt tabspanel">

												<center><h5 class="bold">Zakat Profesi</h5></center>

											</a>

										</li>

									</ul>



									<!-- Tab panes -->
									
									<div class="tab-content">

										<div role="tabpanel" class="tab-pane active" id="zakatmaal">

											{_fmall}

										</div>

										<div role="tabpanel" class="tab-pane" id="zakatprofesi">

											{_fprofesi}

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>



				<div id="donasi-pembayaran">

					<div class="container">

						<div clas="row" >

							<br clear="all"/>

							<div class="tab-info">

								<!-- Nav tabs -->
								<?php $cek = empty($this->session->userdata('prevnomDonation'))?"":"active";?>

								<div class="tab-nb">

									<ul id="tab-donasipembayaran" class="nav nav-tabs" role="tablist">

										<li role="presentation" class="<?php echo $cek ==""?"active":""?> w50 border-r">

											<a href="#donasi" aria-controls="donasi" role="tab" data-toggle="tab" class="no-bt">

												<center><h5 class="bold">1. Kalkulasi</h5></center>

											</a>

										</li>

										<li role="presentation" class="<?php echo $cek ==""?'':'active'?> w50"">

											<a href="#pembayaran" aria-controls="pembayaran" role="tab" data-toggle="tab" class="no-bt">

												<center><h5 class="bold">2. Pembayaran</h5></center>

											</a>

										</li>

									</ul>



									<!-- Tab panes -->

									<div class="tab-content">

										<div role="tabpanel" class="tab-pane <?php echo $cek ==""?"active":""?>" id="donasi">

											{_fdonasi}

										</div>

										<div role="tabpanel" class="tab-pane <?php echo $cek ==""?'':'active'?>" id="pembayaran">

											{_fpembayaran}

										</div>

									</div>

								</div>

							</div>



						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

{_layFooter}

<script type="text/javascript">
	var img_url = "<?php URI::baseURL(); ?>assets/images/icon/";

	$("a.shadaqah-h").hover(function(){
		$(this).children().children().children().find("img").attr("src",img_url + 'shadaqah.png');
		$(this).children().children().find("h2").attr("style","color:#0255a5");
		$(this).children().children().find("hr").attr("style","border-color:#5cb85c");
		$(this).children().next().find("p").attr("style","color:#000");
	}, function(){
		$(this).children().children().children().find("img").attr("src",img_url + 'nonshadaqah.png');
		$(this).children().children().find("h2").attr("style","color:#ccc");
		$(this).children().children().find("hr").attr("style","border-color:#ccc");
		$(this).children().next().find("p").attr("style","color:#ccc");
	});

	$("a.infaq-h").hover(function(){
		$(this).children().children().children().find("img").attr("src",img_url + 'infaq.png');
		$(this).children().children().find("h2").attr("style","color:#0255a5");
		$(this).children().children().find("hr").attr("style","border-color:#5cb85c");
		$(this).children().next().find("p").attr("style","color:#000");
	}, function(){
		$(this).children().children().children().find("img").attr("src",img_url + 'noninfaq.png');
		$(this).children().children().find("h2").attr("style","color:#ccc");
		$(this).children().children().find("hr").attr("style","border-color:#ccc");
		$(this).children().next().find("p").attr("style","color:#ccc");
	});
</script>

<script src='https://code.jquery.com/jquery-3.1.1.min.js'></script>
&nbsp;<script type = "text/javascript" >
    /**
     * Number.prototype.format(n, x, s, c)
     *
     * @param integer n: length of decimal
     * @param integer x: length of whole part
     * @param mixed   s: sections delimiter
     * @param mixed   c: decimal delimiter
     */
    Number.prototype.format = function(n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };
    $(document).ready(function () {

        jQuery(document).on("keyup",".angka, .angkahutang",function () {
            var sum = 0;
            var total = 0;
            var nisab = 0;
            var hutang = parseInt($(".angkahutang").val());
            $(".angka").each(function () {
                sum += parseFloat($(this).val());
            });
            total = parseInt(sum - hutang);
            total = parseInt(total * 0.025);
            $(".hasilhitungtxt").val("Rp. "+total.format(0, 3, '.', ','));
            $(".hasilhitung").val(total);
        });jQuery(document).on("keyup",".angkaprofesi, .angkaprofesihutang",function () {
            var sum = 0;
            var total = 0;
            var hutang = parseInt($(".angkaprofesihutang").val());
            $(".angkaprofesi").each(function () {
                sum += parseFloat($(this).val());
            });
            total = parseInt(sum - hutang);
            total = parseInt(total * 0.025);
            $(".hasilhitungtxt").val("Rp. "+total.format(0, 3, '.', ','));
            $(".hasilhitung").val(total);
        });
        jQuery(document).on("click",".tabspanel",function () {
            var isi = $(this).data('isi');
            $("#jenis").val(isi);
        })
    });
    jQuery(document).on('change','#agreement', function(){
		if (!$(this).is(':checked')) {
			window.location.href = '<?php echo base_url('login')?>';
			// alert("cilukba");
		}
	});jQuery(document).on('change','#agreement', function(){
		if (!$(this).is(':checked')) {
			window.location.href = '<?php echo base_url('login')?>';
			// alert("cilukba");
		}
	});
</script>