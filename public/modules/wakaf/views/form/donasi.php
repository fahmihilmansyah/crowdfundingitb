<div class="col-md-3">

    &nbsp;

</div>

<div class="col-md-6">

    <center>

        <?php

        $hidden_form = array('id' => !empty($id) ? $id : '');

        echo form_open_multipart(base_url('program/zakat#tab-donasipembayaran'), array('id' => 'fmdonasi', 'class'=>'fmdonasi'), $hidden_form);

        ?>


        <div>

            <label class="hasilhitung"></label>

            <input type="hidden" value="Rp. <?php echo empty($this->session->userdata('prevnomDonation')) ? "0" :
                number_format($this->session->userdata('prevnomDonation'), 0, ".", "."); ?>" id="donationNominal"
                   readonly class="form-control hasilhitungtxt don-input " placeholder="Nominal Donasi">

            <input type="number" name="donationNominal"
                   value="<?php echo empty($this->session->userdata('prevnomDonation')) ? "" :
                       $this->session->userdata('prevnomDonation'); ?>" id="donationNominal"
                   class="form-control hasilhitung don-input donationNominal" placeholder="Nominal Donasi">
        </div>
        <p>Donasi minimal Rp. 30.000</p>

        <div>
            <?php if (empty($_SESSION[LGI_KEY . "login_info"]['fname'])): ?>

                <label class="agreement">

                    <div>

                        <?php

                        $data = array(

                            'name' => 'agreement',

                            'id' => 'agreement',

                            'value' => 'accept',

                            'checked' => TRUE,

                        );


                        echo form_checkbox($data);

                        ?>

                        Donasi Sebagai Anonim

                    </div>

                </label>
            <?php endif; ?>

        </div>

        <br clear="all"/>

        <div>

            <?php

            $name = 'donationCommment';

            $value = empty($this->session->userdata('prevcomDonation')) ? "" :

                $this->session->userdata('prevcomDonation');

            $extra = 'id="donationCommment" 

						  class="form-control mini-height" 

						  placeholder="Tuliskan Komentar"';


            echo form_textarea($name, $value, $extra);

            ?>

        </div>


        <div>

            <input type="hidden" name="otherJenis" id="jenis" value="zakatmaal">

            <input type="submit" name="subnominal" class="btn btn-success" style="width:100%; margin-top:10px"
                   value="Lanjut">

        </div>

    </center>

    <?php

    echo form_close();

    ?>

</div>

<div class="col-md-3">

    &nbsp;

</div>