{_layHeader}
<div class="container-fluid grey">

			<div class="don-head">

				<div class="container">

		<!-- start create campaign main -->
			<center>
			<div class="invoice">
			
				<center>
					<h1 class="bold">TERIMA KASIH</h1>
					<img src="<?php URI::baseURL(); ?>assets/images/icon/invoice.png"></br></br>
					<h4>Terima kasih atas bantuan yang anda berikan</h4><br/>
				<div class="inv">
					<div class="invoice-">
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
							Nomor Transaksi</br>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
							7834758295893</br>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
							</br>Nominal</br>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
							</br>Rp 20.000.000</br>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
							</br>Kode unik
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
							</br>202
						</div>
						</div>
					<hr>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-left" align="left">
								<h3>Total</h3>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 nav navbar-right" align="right">
							<h3 style="color:#4cb050">Rp 20.000.000</h3>
						</div>
					<br/><br/><br/>
					<h4 align="left">Silahkan Transfer <b>Rp 20.000.202</b> (nominasi + kode unik) agar transaksi anda terverifikasi secara otomatis tanpa perlu konfirmasi ke rekening berikut:</h4>
					<br/>
					<table width="100%">
						<tr class="bold">
							<td align="left" width="60%"><img src="<?php URI::baseURL(); ?>assets/images/icon/Logo Bank.png" style="width:90%"></td>
							<td align="left" valign="top"><p style="color:#4cb050;font-size:30px;">BNI</p>No.Rekening: 2309498579</br>Atas Nama:	Si Kucing</td>
						</tr>
					</table>
					<h4 align="left">Pastikan anda transfer sebelum [Tanggal, Jam WIB] atau transaksi anda akan batal secara otomatis oleh sistem.</h4>
					<div class="btn-top-donasi">

							<a href="#" class="btn btn-success btn-donasi-lain">

								<h2 class="sm-bold hidden-xs">DONASI SEKARANG</h2>

								<h4 class="sm-bold hidden-lg hidden-md hidden-sm" style="position: relative; top:13px;">DONASI SEKARANG</h4>
							</a>

					</div>
					<br/>
					<div class="page-share-icon">
						<div class="col-md-3 col-sm-4 col-xs-3"><h3 class="bold" style="valign:top">SHARE:</h3></div>
						<p class="col-md-1 col-sm-2 col-xs-1">

							<a href="#" class="share-icon fb"></a>

						</p>

						<p class="col-md-1 col-sm-2 col-xs-1">

							<a href="#" class="share-icon twitter"></a>

						</p>

						<p class="col-md-1 col-sm-2 col-xs-1">

							<a href="#" class="share-icon google"></a>

						</p>
						
						<p class="col-md-1 col-sm-2 col-xs-1">

							<a href="#" class="share-icon linkedin"></a>

						</p>

						<p class="col-md-1 col-sm-2 col-xs-1">

							<a href="#" class="share-icon pinterest"></a>

						</p>
						
						<p class="col-md-1 col-sm-2 col-xs-1">

							<a href="#" class="share-icon share"></a>

						</p>

					</div>
				</div>
				</center>
				
			</div>
			</center>
		</div>

			</div>
	</div>
{_layFooter}