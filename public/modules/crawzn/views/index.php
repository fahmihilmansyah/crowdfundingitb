{_layHeader}

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
<?php
$url = implode("/", $this->uri->segment_array());
$dats = array('urllink' => $url);
$this->session->set_userdata($dats);
?>
<?php $cek = empty($this->session->userdata('prevnomDonation')) ? "" : "active"; ?>
<div class="content">
    <?php if(!empty($this->session->flashdata('error'))):?>
        <div class="alert alert-danger alert-dismissable " role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <script type="text/javascript">
            setTimeout(function() {
                $('.alert').fadeOut();
            }, 10000); // <-- time in milliseconds
        </script>
    <?php endif;?>
    <div class="mb-40 text-center">
        <div class="h3 bold title-font lh-1 color-second"><?php echo empty($judul[0]->title) ? "" : $judul[0]->title; ?></div>
    </div>
    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'aqiqah/aqiqah' . $this->uri->segment(3), array('id' => 'fmdonasi', 'class' => 'fmdonasi mt-20'), $hidden_form);

    ?>
    <div class="mb-20 text-center">
        <h2>Akikah</h2>
        <hr/>
    </div>
    <div class="form-group">
        <label>Jenis Akikah</label>
        <select class="form-control" name="donationNominal">
            <?php foreach ($trxkurban as $r): ?>
                <option value="<?php echo $r->kategoricode ?>|<?php echo $r->amount ?>"><?php echo $r->kategoriname ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>No. Handphone</label>
        <input name="donationPhone" type="text" class="form-control" placeholder="Masukkan Nomor Handphone Anda">
    </div>
    <div class="form-group">
        <label>Nama</label>
        <input name="donationName" type="text" class="form-control" placeholder="Masukkan Nama Anda">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input name="donationEmail" type="text" class="form-control" placeholder="Masukkan Email Anda">
    </div>
<!--    <div class="form-group">-->
<!--        <label>Komentar Dukungan Anda</label>-->
<!--        <textarea name="donationCommment" class="form-control" placeholder="Berikan Komentar, Doa, atau Dukungan Anda"-->
<!--                  rows="3"></textarea>-->
<!--    </div>-->
    <!--        <div class="form-group">-->
    <!--            <label>Metode Pembayaran</label>-->
    <!--            <input type="text" class="form-control" placeholder="Pilih Metode Pembayaran">-->
    <!--        </div>-->
    <div class="mb-20"><b>Biodata Akikah</b></div>
    <div class="form-group">
        <label>Nama yang Akikah</label>
        <input name="namaAkikah" type="text" class="form-control" placeholder="Masukkan Nama yang Akikah">
    </div>
    <div class="form-group">
        <label>Jenis Kelamin</label>
        <select class="form-control" name="jkAkikah">
            <option value="L">Laki-Laki</option>
            <option value="P">Perempuan</option>
        </select>
    </div>
    <div class="form-group">
        <label>Tempat Lahir</label>
        <input name="tmptlahirAkikah" type="text" class="form-control" placeholder="Masukkan Tempat Lahir yang Akikah">
    </div>
    <div class="form-group">
        <label>Tanggal Lahir</label>
        <input name="tgllahirAkikah" type="text" class="form-control datepicker" placeholder="Masukkan Tanggal Lahir yang Akikah">
    </div>
    <div class="form-group">
        <label>Ref. Tgl Pelaksanaan Akikah</label>
        <input name="tglpelaksanaAkikah" type="text" class="form-control datepicker" placeholder="Masukkan Tanggal Pelaksanaan yang Akikah">
    </div>
    <div class="mb-20"><b>Biodata Orang Tua</b></div>

    <div class="form-group">
        <label>Nama Ayah</label>
        <input name="namaAyah" type="text" class="form-control" placeholder="Masukkan Nama Ayah">
    </div>
    <div class="form-group">
        <label>Nama Ibu</label>
        <input name="namaIbu" type="text" class="form-control" placeholder="Masukkan Nama Ibu">
    </div>
    <div class="form-group">
        <label>Alamat Rumah</label>
        <textarea name="alamatrumah" class="form-control" placeholder="Masukkan Alamat Rumah"></textarea>
    </div>
    <div class="mb-20"><b>Methode Pembayaran</b></div>
    <div id="accordion">
        <div class="card">
            <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne"
                 aria-expanded="true" aria-controls="collapseOne">
                <h5 class="mb-0">
                    Transfer Bank
                </h5>
            </div>

            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <ul class="list-group">
                        <?php
                        $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'va'])->get()->result();
                        foreach ($carilist as $r):
                            ?>
                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="donationPay" id="<?php echo $r->kode_va ?>"
                                           value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>">
                                    <label class="form-check-label" for="<?php echo $r->kode_va ?>">
                                        <?php if (!empty($r->img)): ?>
                                            <img width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                                        <?php else:?>
                                            <?php echo $r->name ?>
                                        <?php endif; ?>
                                    </label>
                                </div>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo"
                 aria-expanded="true" aria-controls="collapseTwo">
                <h5 class="mb-0">
                    E-Money
                </h5>
            </div>

            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    <ul class="list-group">
                        <?php
                        $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'emoney'])->get()->result();
                        foreach ($carilist as $r):
                            ?>
                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="donationPay" id="<?php echo $r->kode_va ?>"
                                           value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>">
                                    <label class="form-check-label" for="<?php echo $r->kode_va ?>">
                                        <?php if (!empty($r->img)): ?>
                                            <img width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                                        <?php else:?>
                                            <?php echo $r->name ?>
                                        <?php endif; ?>
                                    </label>
                                </div>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTre" data-toggle="collapse" data-target="#collapseTre"
                 aria-expanded="true" aria-controls="collapseTre">
                <h5 class="mb-0">
                    Setor Tunai
                </h5>
            </div>

            <div id="collapseTre" class="collapse" aria-labelledby="headingTre" data-parent="#accordion">
                <div class="card-body">
                    <ul class="list-group">
                        <?php
                        $carilist = $this->db->from('nb_list_ipg_method')->where(['typeipg'=>'retail'])->get()->result();
                        foreach ($carilist as $r):
                            ?>
                            <li class="list-group-item">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="donationPay" id="<?php echo $r->kode_va ?>"
                                           value="<?php echo $r->partner."|".$r->kode_va."|".$r->id ?>">
                                    <label class="form-check-label" for="<?php echo $r->kode_va ?>">
                                        <?php if (!empty($r->img)): ?>
                                            <img width="80" src="<?php echo base_url() . "assets/images/bank/" .$r->img ?>" class="img-rounded">
                                        <?php else:?>
                                            <?php echo $r->name ?>
                                        <?php endif; ?>
                                    </label>
                                </div>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-30">
        <button class="btn btn-block btn-main"><span>Bayar</span></button>
    </div>

    <?php

    echo form_close();

    ?>
    <div class="mb-10"></div>

    <script>
        jQuery(document).on('click', '.nextbayar', function () {
            $('#pills-pembayaran-tab').tab('show');
        });
        $(".datepicker").datepicker();
    </script>


    {_layFooter}

