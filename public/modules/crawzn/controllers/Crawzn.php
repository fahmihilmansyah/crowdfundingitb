<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Crawzn extends NBFront_Controller
{


    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_modList = array(
        "crawzn/kurban_model" => "infaqmdl"
    );


    protected $_data = array();

    private $_SESSION;


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

        $this->_SESSION = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
            $this->session->userdata(LGI_KEY . "login_info") : '';
    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);
        $this->load->library('Fhhlib');
        // REDIRECT IF SESSION EXIST
        // $this->nbauth_front->lookingForAuten();
    }


    public function index($data = array())
    {
//        $this->saveImageArticle('https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png');exit;
//        echo base_url('/assets/images/articles');
        $data = json_decode(file_get_contents('php://input'), true);
        if (empty($data)) {
            echo "gak ada data";
            exit;
        }
        $data = $data['data'];
        foreach ($data as $r):
            $imgs = $this->saveImageArticle($r['img_urls']);
            $img_url = $r['img_link'];
            $url_link = $r['url_link'];
            $title = $r['title'];
            $mini_content = $r['mini_content'];
            $full_content = $r['full_content'];
            $url_link = $r['url_link'];
            $slug = URI::slug($title);
            $caricamp = $this->db->from('nb_post')->where(['slug' => $slug,'url_links'=>$url_link])->get()->num_rows();
            if (empty($caricamp)):
//                $slug = !empty($caricamp) ? $r["slug"] . ($caricamp + 1) : $slug;
                $setdata = array(
                    'img'=>$imgs,
                    'title' => $title,
                    'slug' => $slug,
                    'img_urls' => $img_url,
                    'desc' => $full_content,
                    'desc_short' => $mini_content,
                    'post_date' => date('Y-m-d'),
                    'crtd_at' => date('Y-m-d'),
                    'edtd_at' => date('Y-m-d'),
                    'author' => 'ZNews',
                    'type' => 'news',
                    'usr_post' => '10',
                    'categories' => 1,
                    'is_active' => '1',
                    'url_links'=>$url_link
                );
                $this->infaqmdl->custom_insert('post', $setdata);
            endif;
        endforeach;
        print_r($data);
        exit;
    }

    function updateimg(){
        $caricamp = $this->db->from('nb_post')->where('img is NULL',null,false)->where('img_urls is not null',null,false)->get()->result_array();
        foreach ($caricamp as $r){
            $filename = $this->saveImageArticle($r['img_urls']);
            $data = array(
                'img' => $filename,
            );
            $this->db->where('id', $r['id']);
            $this->db->update('nb_post', $data);
        }
    }

    private function saveImageArticle($url=''){
//        $url = 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
    if(empty($url)){
        return null;
    }
//Download the file using file_get_contents.
        $file_info = new finfo();
        $downloadedFileContents = @file_get_contents($url);

        if($downloadedFileContents === false){
            return  null;
            throw new Exception('Failed to download file at: ' . $url);
        }
        $mime_type = $file_info->buffer($downloadedFileContents);
        $size = getimagesize($url);
        $extension = image_type_to_extension($size[2]);
//        print_r($size);exit;
//Check to see if file_get_contents failed.
        if($downloadedFileContents === false){
            throw new Exception('Failed to download file at: ' . $url);
        }
        $namefiles = 'img_'.time().rand(0,10000).'.'.str_replace('image/','',$size['mime']);
//The path and filename that you want to save the file to.
        $fileName = APPPATH.'../assets/images/articles/'.$namefiles;
        // File to save the contents to
        $fp = fopen ($fileName, 'w+');
// Here is the file we are downloading, replace spaces with %20
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_TIMEOUT, 50);

// give curl the file pointer so that it can write to it
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $data = curl_exec($ch);//get curl response

        curl_close($ch);
        return $namefiles;
    }
    function generateimg(){
        $this->saveImageArticle();
        echo FCPATH .'assets/images/articles/';exit;
    }

}

