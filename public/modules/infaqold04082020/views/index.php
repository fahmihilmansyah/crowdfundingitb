
{_layHeader}
<?php
$url = implode("/",$this->uri->segment_array());
$dats = array('urllink'=>$url);
$this->session->set_userdata($dats);
?>

	<div id="zakat">

		<div class="container-fluid mb20">

			<div class="row">

				<!-- PROGRAM CAMPAIGN -->
				
				<div id="program-zakat">
					<div id="program-rel-position">
						<?php 
							$img = !empty($spage_infaq["m_img"]) ? "spage_program/" . $spage_infaq["m_img"] : "bg1.jpg";
						?>

						<center><img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img; ?>" style="min-width: 1366px;"></center>
					</div>

					<div id="program-mb-position" class="hidden-lg">
						<div>
							<div class="program-zakat-title hidden-lg">
								<center>
									<h2 class="bold">
										<?php 
											echo !empty($spage_infaq["m_title"]) ? strtoupper($spage_infaq["m_title"]) : "PROGRAM DONASI GaneshaBisa";
										?>
									</h2>
									<hr class="line line-white">
									<h5 style="width: 500px; height: 80px; overflow: hidden;">
									<?php
										if(!empty($spage_infaq["m_desc"])){
											echo $spage_infaq["m_desc"];
										}
										else
										{
									?>
										Bantuan Anda, berapa pun jumlahnya, sangat bermanfaat buat mereka yang membutuhkan. <br/>

										Kami menyalurkan seluruh bantuan amanat dari maysarakat untuk sebesar-besar kemanfaatan yang layak menerimanya.
									<?php
										}
									?>
									</h5>
								</center>
							</div>
							<div class="program-con-putih">
								<div class="hidden-sm hidden-md hidden-lg">
									<div style="padding: 20px;">
											<center>
												<h3 style="margin-top:20px; color:#0255a5" class="bold">
												PILIH PROGRAM</h3>
												<hr class="line">
											</center><br/>
			                				<?php 

												$name         = "program_menu";
												$value        = 'infaq';
												$extra        = 'id="program_menu" style="width:100%;"';
												$categories   = array(	
																		'shadaqah' => 'Shadaqah',
																		'zakat' => 'Zakat',
																		'infaq' => 'Infaq'
																		);

			                					echo form_dropdown($name, $categories, $value, $extra); 

			                				?>
									</div>
								</div>
								<div class="hidden-xs" style="padding-top: 15px;">
									<div class="col-sm-4" style="border-right: 1px solid #ccc">
										<a class="program-mb-href program-mb-href-a" href="<?php echo URI::baseURL(); ?>program/infaq">
											<center>
											<?php
												$img_infaq = !empty($spage_infaq["icon_img"]) ? 
																"spage_program/" . $spage_infaq["icon_img"] : 
																"icon/infaq.png"; 
											?>
											<img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img_infaq; ?>" height="140px">
											<p class="bold">Infaq</p>
											</center>
										</a>
									</div>
									<div class="col-sm-4" style="border-right: 1px solid #ccc">
										<a class="program-mb-href" href="<?php echo URI::baseURL(); ?>program/shadaqah">
											<center>
											<?php 
												$img_shadaqah = !empty($spage_shadaq["icon_img"]) ? 
																"spage_program/" . $spage_shadaq["icon_img"] : 
																"icon/shadaqah.png"; 
											?>
											<img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img_shadaqah; ?>" height="140px">

											<p class="bold">Shadaqah</p>
											</center>
										</a>
									</div>
									<div class="col-sm-4">
										<a class="program-mb-href" href="<?php echo URI::baseURL(); ?>program/zakat" >
											<center>
											<?php

												$img_zakat = !empty($spage_zakat["icon_img"]) ? 
																"spage_program/" . $spage_zakat["icon_img"] : 
																"icon/zakat.png"; 
											/*
											    if($img_zakat != "icon/zakat.png")
							                    {
							                    	$name 		= explode(".", $img_zakat);
							                    	$img_zakat  = $name[0] . "_g." . $name[1];
							                    }
							                */

											?>
											<img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img_zakat; ?>" height="140px">
											<p class="bold">Zakat</p>
											</center>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="program-abs-position" class="hidden-xs hidden-sm hidden-md">
						<div class="container">
							<div clas="row">
								<div class="col-md-12">
									<div class="program-zakat-title">
										<center>
											<h2 class="bold">
												<?php 
													echo !empty($spage_infaq["m_title"]) ? strtoupper($spage_infaq["m_title"]) : "PROGRAM DONASI GaneshaBisa";
												?>
											</h2>
											<hr class="line line-white">
											<h5 style="width: 500px; height: 80px; overflow: hidden;">
											<?php
												if(!empty($spage_infaq["m_desc"])){
													echo $spage_infaq["m_desc"];
												}
												else
												{
											?>
												Bantuan Anda, berapa pun jumlahnya, sangat bermanfaat buat mereka yang membutuhkan. <br/>

												Kami menyalurkan seluruh bantuan amanat dari maysarakat untuk sebesar-besar kemanfaatan yang layak menerimanya.
											<?php
												}
											?>
											</h5>
										</center>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-4" style="margin-bottom:20px">
									<a href="<?php echo URI::baseURL(); ?>program/infaq" class="program-zakat-list infaq-h">
										<div class="list-program">
											<center>
												<div>

													<?php

														$img_infaq = !empty($spage_infaq["icon_img"]) ? 
																		"spage_program/" . $spage_infaq["icon_img"] : 
																		"icon/noninfaq.png"; 
														
														/*$img_infaq_g = ""; 
													    if($img_infaq != "icon/noninfaq.png")
									                    {
									                    	$name 			= explode(".", $img_infaq);
									                    	$img_infaq_g  	= $name[0] . "_g." . $name[1];
									                    }*/

													?>

													<img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img_infaq; ?>">
												</div>
												<h2 class="bold">
													<?php 
														echo !empty($spage_infaq["icon_title"]) ? strtoupper($spage_infaq["icon_title"]) : "INFAQ";
													?>
												</h2>
												<hr class="line">
											</center>
										</div>

										<div class="list-konten">
											<p class="p-program">
												<?php
													if(!empty($spage_infaq["icon_desc"]))
													{
														echo $spage_infaq["icon_desc"];
													}
													else
													{
												?>
													Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
												<?php
													}
												?>
											</p>
										</div>

										<div class="list-btn padd-rl-50" style="padding-bottom:20px">

										</div>

										<div class="program-zakat-add-block green-focus">

										</div>
									</a>
								</div>

								<div class="col-md-4" style="margin-bottom:20px">
									<a href="<?php echo URI::baseURL(); ?>program/shadaqah" class="program-zakat-list shadaqah-h">
										<div class="list-program">
											<center>

												<div>
												<?php

													$img_shadaqah = !empty($spage_shadaq["icon_img"]) ? 
																	"spage_program/" . $spage_shadaq["icon_img"] : 
																	"icon/nonshadaqah.png"; 
												
													$img_shadaqah_g = ""; 
												    if($img_shadaqah != "icon/nonshadaqah.png")
								                    {
								                    	$name 			    = explode(".", $img_shadaqah);
								                    	$img_shadaqah_g  	= $name[0] . "_g." . $name[1];
								                    }
								                    

												?>
												<img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img_shadaqah_g; ?>">
												</div>
												<h2 class="bold" style="color: #ccc">
													<?php
														echo !empty($spage_shadaq["icon_title"]) ? strtoupper($spage_shadaq["icon_title"]) : "SHADAQAH"
													?>
												</h2>
												<hr class="line" style="border-color: #ccc">
											</center>
										</div>

										<div class="list-konten">
											<p class="p-program" style="color: #ccc">
												<?php
													if(!empty($spage_shadaq["icon_desc"]))
													{
														echo $spage_shadaq["icon_desc"];
													}
													else
													{
												?>
													Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
												<?php
													}
												?>
											</p>
										</div>

										<div class="list-btn padd-rl-50" style="padding-bottom:20px">

										</div>

										<div class="program-zakat-add-block">

										</div>
									</a>
								</div>

								<div class="col-md-4" style="margin-bottom:20px">
									<a href="<?php echo URI::baseURL(); ?>program/zakat" class="program-zakat-list zakatmaal-h">
										<div class="list-program">
											<center>
												<div>

													<?php

														$img_zakat = !empty($spage_zakat["icon_img"]) ? 
																		"spage_program/" . $spage_zakat["icon_img"] : 
																		"icon/nonzakat.png"; 
														
														$img_zakat_g = ""; 
													    if($img_zakat != "icon/nonzakat.png")
									                    {
									                    	$name 			= explode(".", $img_zakat);
									                    	$img_zakat_g  	= $name[0] . "_g." . $name[1];
									                    }

													?>
													<img src="<?php URI::baseURL(); ?>assets/images/<?php echo $img_zakat_g; ?>">
												</div>

												<h2 class="bold" style="color:#ccc">
													<?php
														echo !empty($spage_zakat["icon_title"]) ? strtoupper($spage_zakat["icon_title"]) : "ZAKAT";
													?>
												</h2>

												<hr class="line" style="border-color: #ccc">
											</center>
										</div>

										<div class="list-konten">
											<p class="p-program" style="color: #ccc">
												<?php
													if(!empty($spage_zakat["icon_desc"]))
													{
														echo $spage_zakat["icon_desc"];
													}
													else
													{
												?>
													Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
												<?php
													}
												?>
											</p>
										</div>

										<div class="list-btn padd-rl-50" style="padding-bottom:20px">

										</div>

										<div class="program-zakat-add-block">


										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="kalkulator_zakat" style="margin-top:150px;">

					<div class="container">

						<div clas="row" id="gagalnotif">

							<center>

								<h2 class='bold'>INFAQ</h2>

								<hr/><br/>
								<div >
<?php if(!empty($this->session->flashdata('error'))):?>
            <div class="alert alert-danger alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('error'); ?>
            </div>
            <script type="text/javascript">
            	setTimeout(function() {
				    $('.alert').fadeOut();
				}, 10000); // <-- time in milliseconds
            </script>
        <?php endif;?></div>


							</center>



							<br clear="all"/>



							<div class="tab-info">

								<!-- Nav tabs -->
								<?php $cek = empty($this->session->userdata('prevnomDonation'))?"":"active";?>
								<div class="tab-nb">

									<ul id="tab-donasipembayaran" class="nav nav-tabs" role="tablist">

										<li role="presentation" class="<?php echo $cek ==""?"active":""?> w50 border-r">

											<a href="#donasi" aria-controls="donasi" role="tab" data-toggle="tab" class="no-bt">

												<center><h5 class="bold">1. Donasi</h5></center>

											</a>

										</li>

										<li role="presentation" class="<?php echo $cek ==""?'':'active'?> w50"">

											<a href="#pembayaran" aria-controls="pembayaran" role="tab" data-toggle="tab" class="no-bt">

												<center><h5 class="bold">2. Pembayaran</h5></center>

											</a>

										</li>

									</ul>



									<!-- Tab panes -->

									<div class="tab-content">

										<div role="tabpanel" class="tab-pane <?php echo $cek ==""?"active":""?>" id="donasi">

											{_fdonasi}

										</div>

										<div role="tabpanel" class="tab-pane <?php echo $cek ==""?'':'active'?>" id="pembayaran">

											{_fpembayaran}

										</div>

									</div>

								</div>

							</div>



						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

{_layFooter}

<script type="text/javascript">
	var img_url = "<?php URI::baseURL(); ?>assets/images/";

	$("#program_menu").select2();
	
	$("a.shadaqah-h").hover(function(){
		$(this).children().children().children().find("img").attr("src",img_url + '<?php echo $img_shadaqah; ?>');
		$(this).children().children().find("h2").attr("style","color:#0255a5");
		$(this).children().children().find("hr").attr("style","border-color:#5cb85c");
		$(this).children().next().find("p").attr("style","color:#000");
	}, function(){
		$(this).children().children().children().find("img").attr("src",img_url + '<?php echo $img_shadaqah_g; ?>');
		$(this).children().children().find("h2").attr("style","color:#ccc");
		$(this).children().children().find("hr").attr("style","border-color:#ccc");
		$(this).children().next().find("p").attr("style","color:#ccc");
	});

	$("a.zakatmaal-h").hover(function(){
		$(this).children().children().children().find("img").attr("src",img_url + '<?php echo $img_zakat; ?>');
		$(this).children().children().find("h2").attr("style","color:#0255a5");
		$(this).children().children().find("hr").attr("style","border-color:#5cb85c");
		$(this).children().next().find("p").attr("style","color:#000");
	}, function(){
		$(this).children().children().children().find("img").attr("src",img_url + '<?php echo $img_zakat_g; ?>');
		$(this).children().children().find("h2").attr("style","color:#ccc");
		$(this).children().children().find("hr").attr("style","border-color:#ccc");
		$(this).children().next().find("p").attr("style","color:#ccc");
	});
	jQuery(document).on('change','#agreement', function(){
		if (!$(this).is(':checked')) {
			window.location.href = '<?php echo base_url('login')?>';
			// alert("cilukba");
		}
	});

	$('#program_menu').change(function() {
		var value = $(this).val();

		if(value == "zakat")
		{
			location.href = "<?php echo base_url(); ?>program/zakat";
		}

		if(value == "shadaqah")
		{
			location.href = "<?php echo base_url(); ?>program/shadaqah";
		}

		if(value == "infaq")
		{
			location.href = "<?php echo base_url(); ?>program/infaq";
		}
	});
</script>