{_layHeader}
<?php
$url = implode("/",$this->uri->segment_array());
$dats = array('urllink'=>$url);
$this->session->set_userdata($dats);
?>
	<div id="zakat">

		<div class="container-fluid mb20">

			<div class="row">

				<!-- PROGRAM CAMPAIGN -->

				<div id="program-zakat">

					<div class="container">

						<div clas="row">

							<div class="col-md-12">

								<div class="program-zakat-title">

									<center>

										<h2 class="bold">PROGRAM DONASI GaneshaBisa</h2>

										<hr class="line line-white">

										<h5>

											Bantuan Anda, berapa pun jumlahnya, sangat bermanfaat buat mereka yang membutuhkan. <br/>

											Kami menyalurkan seluruh bantuan amanat dari maysarakat untuk sebesar-besar kemanfaatan yang layak menerimanya.

										</h5>

									</center>

								</div>

							</div>

						</div>



						<div class="row">

							<div class="col-md-4" style="margin-bottom:20px">

								<a href="<?php echo URI::baseURL(); ?>program/infaq" class="program-zakat-list infaq-h">

									<div class="list-program">

										<center>

											<img src="<?php URI::baseURL(); ?>assets/images/icon/infaq.png">

											<h2 class="bold">INFAQ</h2>

											<hr class="line">

										</center>

									</div>

									<div class="list-konten">

										<p class="p-program">

											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

										</p>

									</div>

									<div class="list-btn padd-rl-50" style="padding-bottom:20px">

										

									</div>

									<div class="program-zakat-add-block green-focus">

										

									</div>

								</a>

							</div>

							<div class="col-md-4" style="margin-bottom:20px">

								<a href="<?php echo URI::baseURL(); ?>program/shadaqah" class="program-zakat-list shadaqah-h">

									<div class="list-program">

										<center>
											<div>
												<img src="<?php URI::baseURL(); ?>assets/images/icon/nonshadaqah.png">
											</div>

											<h2 class="bold" style="color: #ccc">SHADAQAH</h2>

											<hr class="line" style="border-color: #ccc">

										</center>

									</div>

									<div class="list-konten">

										<p class="p-program" style="color: #ccc">

											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

										</p>

									</div>

									<div class="list-btn padd-rl-50" style="padding-bottom:20px">

										

									</div>

									<div class="program-zakat-add-block">

										

									</div>

								</a>

							</div>

							<div class="col-md-4" style="margin-bottom:20px">

								<a href="<?php echo URI::baseURL(); ?>program/zakat" class="program-zakat-list zakatmaal-h">

									<div class="list-program">

										<center>
											<div>
												<img src="<?php URI::baseURL(); ?>assets/images/icon/nonzakat.png">
											</div>

											<h2 class="bold" style="color: #ccc">ZAKAT MAAL</h2>

											<hr class="line" style="border-color: #ccc">

										</center>

									</div>

									<div class="list-konten">

										<p class="p-program" style="color: #ccc">

											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

										</p>

									</div>

									<div class="list-btn padd-rl-50" style="padding-bottom:20px">

									

									</div>

									<div class="program-zakat-add-block">

										

									</div>

								</a>

							</div>

						</div>

					</div>

				</div>



				<div id="kalkulator_zakat">

					<div class="container">

						<div clas="row" id="gagalnotif">

							<center>

								<h2 class='bold'>INFAQ</h2>

								<hr/><br/>
								<div >
<?php if(!empty($this->session->flashdata('error'))):?>
            <div class="alert alert-danger alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('error'); ?>
            </div>
            <script type="text/javascript">
            	setTimeout(function() {
				    $('.alert').fadeOut();
				}, 10000); // <-- time in milliseconds
            </script>
        <?php endif;?></div>


							</center>



							<br clear="all"/>



							<div class="tab-info">

								<!-- Nav tabs -->
								<?php $cek = empty($this->session->userdata('prevnomDonation'))?"":"active";?>
								<div class="tab-nb">

									<ul id="tab-donasipembayaran" class="nav nav-tabs" role="tablist">

										<li role="presentation" class="<?php echo $cek ==""?"active":""?> w50 border-r">

											<a href="#donasi" aria-controls="donasi" role="tab" data-toggle="tab" class="no-bt">

												<center><h5 class="bold">1. Donasi</h5></center>

											</a>

										</li>

										<li role="presentation" class="<?php echo $cek ==""?'':'active'?> w50"">

											<a href="#pembayaran" aria-controls="pembayaran" role="tab" data-toggle="tab" class="no-bt">

												<center><h5 class="bold">2. Pembayaran</h5></center>

											</a>

										</li>

									</ul>



									<!-- Tab panes -->

									<div class="tab-content">

										<div role="tabpanel" class="tab-pane <?php echo $cek ==""?"active":""?>" id="donasi">

											{_fdonasi}

										</div>

										<div role="tabpanel" class="tab-pane <?php echo $cek ==""?'':'active'?>" id="pembayaran">

											{_fpembayaran}

										</div>

									</div>

								</div>

							</div>



						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

{_layFooter}

<script type="text/javascript">
	var img_url = "<?php URI::baseURL(); ?>assets/images/icon/";

	$("a.shadaqah-h").hover(function(){
		$(this).children().children().children().find("img").attr("src",img_url + 'shadaqah.png');
		$(this).children().children().find("h2").attr("style","color:#0255a5");
		$(this).children().children().find("hr").attr("style","border-color:#5cb85c");
		$(this).children().next().find("p").attr("style","color:#000");
	}, function(){
		$(this).children().children().children().find("img").attr("src",img_url + 'nonshadaqah.png');
		$(this).children().children().find("h2").attr("style","color:#ccc");
		$(this).children().children().find("hr").attr("style","border-color:#ccc");
		$(this).children().next().find("p").attr("style","color:#ccc");
	});

	$("a.zakatmaal-h").hover(function(){
		$(this).children().children().children().find("img").attr("src",img_url + 'zakat.png');
		$(this).children().children().find("h2").attr("style","color:#0255a5");
		$(this).children().children().find("hr").attr("style","border-color:#5cb85c");
		$(this).children().next().find("p").attr("style","color:#000");
	}, function(){
		$(this).children().children().children().find("img").attr("src",img_url + 'nonzakat.png');
		$(this).children().children().find("h2").attr("style","color:#ccc");
		$(this).children().children().find("hr").attr("style","border-color:#ccc");
		$(this).children().next().find("p").attr("style","color:#ccc");
	});
	jQuery(document).on('change','#agreement', function(){
		if (!$(this).is(':checked')) {
			window.location.href = '<?php echo base_url('login')?>';
			// alert("cilukba");
		}
	});
</script>