<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class KonfirmasiPembayaran extends NBFront_Controller
{

    /**
     * ----------------------------------------
     * #NB Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_modList = array("konfirmasipembayaran/Konfirmasipembayaran_model" => "konfpemb");
    protected $_data = array();
    private $_SESSION;

    function __construct()
    {
        parent::__construct();
        // -----------------------------------
        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN
        // -----------------------------------

        $this->modelsApp();
        $this->_SESSION   = !empty($this->session->userdata(LGI_KEY . "login_info")) ? 
                                   $this->session->userdata(LGI_KEY . "login_info") : '';

    }

    private function modelsApp()
    {
        $this->load->model($this->_modList);

        // REDIRECT IF SESSION EXIST
        $this->nbauth_front->lookingForAuten();
    }

    public function index($data = array())
    {
        $data = $this->_data;
        $data["action"] = base_url() . 'konfirmasi/proses';
        $data["bank"]   = $this->konfpemb->options("Pilih Bank");
        $data['trans_source']   = base_url() . "konfirmasipembayaran/KonfirmasiPembayaran/getTransaksiOptions";

        $this->parser->parse("index",$data);
    }

    public function proses_konfirmasi()
    {
        $data = $this->input->post("data");
        if(empty($data['atas_nama']) || empty($data['atas_nama']) || empty($data['atas_nama']) || empty($data['atas_nama']) || empty($data['atas_nama']) || empty($data['atas_nama'])){
            die("System Error Please contact administrator");
        }
        $data["donatur"]    = $this->_SESSION["login_uid"];
        $data["konf_date"]   = date("Y-m-d H:i:s");

        $result = $this->konfpemb->simpan($data);

        if($result)
        {
            redirect('konfirmasi/sukses');
        }
        else
        {
            $message = "Kesalahan, Data yang anda konfirmasi kemungkinan tidak ada di dalam database kami, silahkan hubungi administrator kami. Terima kasih.";
            $this->session->set_flashdata('error_msg', $message);
            redirect('konfirmasi');
        } 
    }

    public function konfirmasi_sukses()
    {   
        $data = $this->_data;
        $this->parser->parse("notifsukses",$data);
    }

    public function getTransaksiOptions(){
        $typeTransaksi = $this->input->get("tipe");

        if($typeTransaksi == "camp")
        {
            $key    = array("td.donatur" => $this->_SESSION["login_uid"], "td.status" => "unverified");
            $data   = $this->konfpemb->campaignsOptions("Pilih Transaksi",$key);
        } 
        else if($typeTransaksi == "prog")
        {
            $key    = array("donatur" => $this->_SESSION["login_uid"], "status" => "unverified", "jenistrx !=" => "deposit");
            $data   = $this->konfpemb->programsOptions("Pilih Transaksi",$key);
        }
		else if($typeTransaksi == "depo")
        {
            $key    = array("donatur" => $this->_SESSION["login_uid"], "status" => "unverified", "jenistrx" => "deposit");
            $data   = $this->konfpemb->depoOptions("Pilih Transaksi",$key);
        }
        else
        {
            $data   = array("" => "Pilih Transaksi");
        }
       
        echo json_encode($data);    
    }

    public function getNominal()
    {
        $idCampaign = $this->input->get("i");
        $typeTransaksi = $this->input->get("j");

        $data = 0;

        if($typeTransaksi == "camp")
        {
            $data = $this->konfpemb->getNominal("trans_donation", $idCampaign);
        }
        else if($typeTransaksi == "prog")
        {
            $data = $this->konfpemb->getNominal("trans_program", $idCampaign);
        }
		else if($typeTransaksi == "depo")
        {
            $data = $this->konfpemb->getNominal("trans_program", $idCampaign);
        }

        $data = !empty($data->nomuniq) ? (int) $data->nomuniq : 0;

        echo json_encode($data);
    }
}



