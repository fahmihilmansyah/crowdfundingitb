<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Konfirmasipembayaran_model extends CI_Model {

    

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from("bank");

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id] = $row->namaBank;
        }

        return $option;
    }

    public function campaignsOptions($default = '--Pilih Data--', $key = '')
    {
        $option = array();
        $this->db->select("td.no_transaksi, cm.title, td.trx_date");
        $this->db->from("trans_donation td");
        $this->db->join("campaign cm", "cm.id = td.campaign");

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->no_transaksi] = $row->no_transaksi . " : " . ucwords($row->title) . " ( Tanggal Berdonasi : " . $row->trx_date . ")";
        }

        return $option;
    }

    public function programsOptions($default = '--Pilih Data--', $key = '')
    {
        $option = array();
        $this->db->from("trans_program");

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->no_transaksi] = $row->no_transaksi . "( Tanggal Donasi Program : " . $row->trx_date . " )";
        }

        return $option;
    }
	
	public function depoOptions($default = '-Pilih Data-', $key = ''){
		$option = array();
		$this->db->from("trans_program");
		
		if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->no_transaksi] = $row->no_transaksi . "( Tanggal Deposit : " . $row->trx_date . " )";
        }

        return $option;
	}

    public function getNominal($table = "", $no_transaksi = "")
    {
        $this->db->select("nomuniq");
        $this->db->from($table);
        $this->db->where("no_transaksi", $no_transaksi);

        return $this->db->get()->row();
    }

    public function simpan($data = array()){
        $this->db->trans_begin();

        $this->db->insert("trans_konfirmasi", $data);

        if ($this->db->trans_status() === FALSE) {

            $this->db->trans_rollback();

            return FALSE;

        } else {

            $this->db->trans_commit();

            return TRUE;

        }
    }

}



?>