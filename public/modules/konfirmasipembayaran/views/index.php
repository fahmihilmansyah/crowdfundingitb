{_layHeader}

	<div id="konfirmasi">

		<div class="container-fluid">

			<div class="row" style="overflow: hidden;">

				<div class="col-md-4">

					&nbsp;

				</div>

				<div class="col-md-4">
					<div id="konfirmasi_table">

						<br/>

						<h4 class="bold">KONFIRMASI PEMBAYARAN</h4>

						<p>Silahkan isi form dibawah ini untuk mengkonfirmasi pembayaran anda</p>

						<div style="padding: 20px;">

							<h5 id="peringatan" class="alert alert-danger alert-dismissible fade in peringatan" style="display: none">

								<center>Terdapat kesalahan dalam pengisian. <br/> Silahkan cek kembali isian form anda!</center>

							</h5>
							
							<?php

								if($this->session->userdata("error_msg")){

							?>



								<p class="alert alert-warning" style="margin:0px; margin-top:10px; text-align: center">

									<?php echo $this->session->userdata("error_msg"); ?>

								</p>


							<?php

								}


							    $hidden_form = array('id' => !empty($id) ? $id : '');

							    echo form_open_multipart($action, array('id' => 'fmKonfirmasi'), $hidden_form);

						    ?>



									<div>

										<label for="message">Jenis Transaksi </label>

										<div>

			                			<?php 

											$name         = 'data[tipe]';

											$value        = '';

											$extra        = 'id="tipe" class="form-control" ';

											$role_opt     = array(

																	"" 		=> "Pilih Jenis Transaksi", 

																	"camp"	=> "Campaign", 

																	"prog" 	=> "Program",
																	
																	"depo" => "Deposit"

																 );



		                					echo form_dropdown($name, $role_opt, $value, $extra); 

			                			?>

			                			</div>

			                			<p class='label-error'></p>

									</div>



									<div>

										<label for="message">Transaksi</label>

										<div>

			                			<?php 

											$name         = 'data[transaksi]';

											$value        = '';

											$extra        = 'id="transaksi" class="form-control" data-source="' . $trans_source . '"';

											$role_opt     = array("" => "Pilih Transaksi");



		                					echo form_dropdown($name, $role_opt, $value, $extra); 

			                			?>

			                			</div>

			                			<p class='label-error'></p>

									</div>

									
									<div>

										<label for="message">Nominal yang harus dibayarkan </label>

										<div id="nominal_harus_dibayar" class="bold">
											Rp. 0
			                			</div>

									</div>


									<div>

										<label for="message">Asal Bank</label>

										<div>

			                			<?php 

											$name         = 'data[bank]';

											$value        = '';

											$extra        = 'id="bank" class="form-control" ';

											$role_opt     = array(

																	"" 		=> "Pilih Bank", 

																	"camp"	=> "Campaign", 

																	"prog" 	=> "Program"

																 );



		                					echo form_dropdown($name, $bank, $value, $extra); 

			                			?>

			                			</div>

			                			<p class='label-error'></p>

									</div>



									<div>

										<label for="message">Atas Nama</label>

										<div>

			                			<?php 

			                				$name  = 'data[atas_nama]';

			                				$value = '';

			                				$extra = 'id="atas_nama" 

			                						  class="form-control" 

			                						  placeholder="isi disini"';



			                				echo form_input($name, $value, $extra); 

			                			?>

			                			</div>

			                			<p class='label-error'></p>

									</div>



									<div>

										<label for="message">No. Rekening Asal</label>

										<div>

			                			<?php 

			                				$name  = 'data[norek_asal]';

			                				$value = '';

			                				$extra = 'id="norek_asal" 

			                						  class="form-control" 

			                						  placeholder="isi disini"';



			                				echo form_input($name, $value, $extra); 

			                			?>

			                			</div>

			                			<p class='label-error'></p>

									</div>



									<div>

										<label for="message">Nominal</label>

										<div>

			                			<?php 

			                				$name  = 'data[nominal]';

			                				$value = '';

			                				$extra = 'id="nominal" 

			                						  class="form-control" 

			                						  placeholder="isi disini"';



			                				echo form_input($name, $value, $extra); 

			                			?>

			                			</div>

			                			<p class='label-error'></p>

									</div>

									<br/>

									<button type="submit" class="btn btn-success" style="width: 100%">

										Kirim

									</button>

						    <?php

						    	echo form_close();

						    ?>

						</div>

					</div>

				</div>

				<div class="col-md-4">

					&nbsp;

				</div>

			</div>

		</div>

	</div>

{_layFooter}

<script type="text/javascript">
	select2_icon("tipe");
	select2_icon("transaksi");
	select2_icon("bank");

    $(function(){
        $("#tipe").change(function(){
            get_options("#transaksi",{tipe : $(this).val()}, '');
        });

        $("#transaksi").change(function(){
        	var tipe = $("#tipe").val();
         	$.get("<?php echo base_url(); ?>konfirmasi/getNominal?i=" + $(this).val() + "&j=" + tipe, function(res) {
		    	$("#nominal_harus_dibayar").html("Rp. "+ toRp(res));
		    }, 'json');
        });
    });

    var validator = $("#fmKonfirmasi").validate({
			ignore: [],
			invalidHandler: function(form, validator) {
			    window.location.hash = '#peringatan';
		    },
			debug: true,
			errorElement: "em",
			errorContainer: $(".peringatan"),
			errorPlacement: function(error, element) {
				error.appendTo(element.parent("div").next("p"));
			},
			highlight: function(element) {   // <-- fires when element has error
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('sukses').addClass('error');
			},
			unhighlight: function(element) { // <-- fires when element is valid
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('error').addClass('sukses');
			},
			rules: {
				'data[tipe]': {
					required: true,
				},
				'data[transaksi]': {
					required: true,
				},
				'data[bank]': {
					required: true,
				},
				'data[atas_nama]': {
					required: true,
				},
				// 'data[norek_asal]': {
				// 	required: true,
				// },
				'data[nominal]': {
					required: true,
					number: true
				},
			}, 
			messages: {
		        'data[tipe]' : "wajib disi",
		        'data[transaksi]' 	: "wajib disi",
		        'data[bank]' 		: "wajib disi",
		        'data[atas_nama]' 	: "wajib disi",
		        // 'data[norek_asal]' 	: "wajib disi",
		        'data[nominal]' 	: {
		        	required : "wajib diisi",
		        	number: "isian harus berupa angka"
		        },
		    },
			submitHandler: function(form) {
		        form.submit();
		    }
		});

</script>