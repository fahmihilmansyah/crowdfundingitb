{_layHeader}

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
<?php
$url = implode("/", $this->uri->segment_array());
$dats = array('urllink' => $url);
$this->session->set_userdata($dats);

$sessionsl = !empty($this->session->userdata(LGI_KEY . "login_info")) ?
    $this->session->userdata(LGI_KEY . "login_info")['email'] : '';
$sessionsn = !empty($this->session->userdata(LGI_KEY . "login_info")) ? $this->session->userdata(LGI_KEY . "login_info")['fname']." ".$this->session->userdata(LGI_KEY . "login_info")['login_lname'] : '';
$sessionsp = !empty($this->session->userdata(LGI_KEY . "login_info")) ? $this->session->userdata(LGI_KEY . "login_info")['phone'] : '';

?>
<?php $cek = empty($this->session->userdata('prevnomDonation')) ? "" : "active"; ?>
<div class="content pr-15 pl-15 bg-white" style="padding-top: 10px;">
    <?php if(!empty($this->session->flashdata('error'))):?>
        <div class="alert alert-danger alert-dismissable " role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <script type="text/javascript">
            setTimeout(function() {
                $('.alert').fadeOut();
            }, 10000); // <-- time in milliseconds
        </script>
    <?php endif;?>
    <div class="mb-40 text-center">
        <div class="h3 bold title-font lh-1 color-second"><?php echo empty($judul[0]->title) ? "" : $judul[0]->title; ?></div>
    </div>
    <?php

    $hidden_form = array('id' => !empty($id) ? $id : '');

    echo form_open_multipart(base_url() . 'aqiqah/aqiqah' . $this->uri->segment(3), array('id' => 'fmdonasi', 'class' => 'fmdonasi mt-20'), $hidden_form);
    $getmitra = $this->db->from('mitra')->where(['type'=>'AQIQAH'])->order_by('sortorder asc')->get()->result();

    ?>
    <div class="mb-20 text-center">
        <h2>Akikah</h2>
        <hr/>
    </div>

    <div class="form-group">
        <label>Mitra</label>
        <select class="form-control" id="listmitra">
            <?php foreach ($getmitra as $r): ?>
                <?php if(!empty($_GET['cpartner'])):  if($r->id == $_GET['cpartner']): ?>
                    <option value="<?php echo $r->id ?>"><?php echo $r->nama_mitra ?></option>
                <?php endif; else:?>
                    <option value="<?php echo $r->id ?>"><?php echo $r->nama_mitra ?></option>
                <?php endif;?>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Jenis Akikah</label>
        <select class="form-control" id="jenisakikah" name="donationNominal">
            <?php foreach ($trxkurban as $r): ?>
                <option value="<?php echo $r->kategoricode ?>|<?php echo $r->amount ?>"><?php echo $r->kategoriname ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="mb-20"><b>Biodata Akikah</b></div>
    <div class="form-group">
        <label>Nama yang Akikah</label>
        <input name="namaAkikah" type="text" class="form-control" placeholder="Masukkan Nama yang Akikah">
    </div>
    <div class="form-group">
        <label>Jenis Kelamin</label>
        <select class="form-control" name="jkAkikah">
            <option value="L">Laki-Laki</option>
            <option value="P">Perempuan</option>
        </select>
    </div>
    <div class="form-group">
        <label>Tempat Lahir</label>
        <input name="tmptlahirAkikah" type="text" class="form-control" placeholder="Masukkan Tempat Lahir yang Akikah">
    </div>
    <div class="form-group">
        <label>Tanggal Lahir</label>
        <input name="tgllahirAkikah" type="text" class="form-control datepicker" placeholder="Masukkan Tanggal Lahir yang Akikah">
    </div>
    <div class="form-group">
        <label>Ref. Tgl Pelaksanaan Akikah</label>
        <input name="tglpelaksanaAkikah" type="text" class="form-control datepicker" placeholder="Masukkan Tanggal Pelaksanaan yang Akikah">
    </div>
    <div class="mb-20"><b>Biodata Orang Tua</b></div>

    <div class="form-group">
        <label>Nama Ayah</label>
        <input name="namaAyah" type="text" class="form-control" placeholder="Masukkan Nama Ayah">
    </div>
    <div class="form-group">
        <label>Nama Ibu</label>
        <input name="namaIbu" type="text" class="form-control" placeholder="Masukkan Nama Ibu">
    </div>
    <div class="form-group">
        <label>Alamat Rumah</label>
        <textarea name="alamatrumah" class="form-control" placeholder="Masukkan Alamat Rumah"></textarea>
    </div>
    <hr/>
    <div class="form-group">

        <input type="hidden" name="donationPay" id="paymetod">
        <div class="row" >
            <div class="col-12">
                <span><b><h5>Metode Pembayaran : </h5></b></span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div id="respemb"></div>
    </div>
    <hr/>
    <?php if(empty($sessionsl)): ?>
        <div class="form-group text-center">
            <span><b></b> lengkapi data dibawah ini.</span>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <label>Nama</label>
        <input name="donationName" type="text" <?php echo !empty($sessionsn)?'readonly':''; ?> value="<?php echo !empty($sessionsn)?$sessionsn:''; ?>" class="form-control" placeholder="Masukkan Nama Anda">
    </div>
    <div class="form-group">
        <label>No. Handphone</label>
        <input name="donationPhone" <?php echo !empty($sessionsp)?'readonly':''; ?> value="<?php echo !empty($sessionsp)?$sessionsp:''; ?>" type="text" class="form-control" placeholder="Masukkan Nomor Handphone Anda">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input name="donationEmail" <?php echo !empty($sessionsl)?'readonly':''; ?> value="<?php echo !empty($sessionsl)?$sessionsl:''; ?>" type="text" class="form-control"  placeholder="Masukkan Email Anda">
    </div>
    <div class="mt-30">
        <button class="btn btn-block btn-main"><span>Bayar</span></button>
    </div>

    <?php

    echo form_close();

    ?>
    <div class="mb-10"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
    <script>

        <?php $paramsd = [];
        foreach ($_GET as $k=>$v):
            $paramsd[] = $k."=".$v;
        endforeach;
        $setparams = implode('&',$paramsd);
        ?>
        $("#respemb").load("<?php echo base_url('/pembayaran/index?'.$setparams); ?>");
        jQuery(document).on('click', '#pilbayar', function () {
            $("#settmpl").load("<?php echo base_url('/pembayaran/index') ?>");
        });
        jQuery(document).on('click', '.nextbayar', function () {
            $('#pills-pembayaran-tab').tab('show');
        });
        $('#denomnms').number( true, 0, '', '.' );
        $(".datepicker").datepicker();


        function getjeniskurban() {
            var partner = $('#listmitra').val();
            $.ajax({
                method: "GET",
                url: "<?php echo base_url('/aqiqah/aqiqah/getharga') ?>?cpart=" + partner,
                dataType: "json",
                beforeSend: function () {
                    $("#jenisakikah").empty();
                },
                success: function (data) {
                    $.each(data.data, function () {
                        $("#jenisakikah").append('<option value="' + this.kategoricode + '|' + this.amount + '">' + this.kategoriname + '</option>');
                    })
                }
            });
        }
        getjeniskurban();
        $('#listmitra').on('change',function () {
            getjeniskurban();
        })
    </script>


    {_layFooter}

