<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Campaigns extends NBFront_Controller {



	/**

	 * ----------------------------------------

	 * #NB Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_modList   = array('Campaigns_model' => 'campaign');



	protected $_data    = array();



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------

		$this->modelsApp();

        $this->load->library("pagination");

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	public function index($txt = array())

	{

		$data = $this->_data;
		$searchterm = $this->searchterm_handler($this->input->post('searchdonasi'));
		$condition["title LIKE "]  = "%" . $searchterm . "%";

		// Pagination       
        $config = array();
        $config["base_url"] = base_url() . "donasi";

//        $num_rows= $this->campaign->record_count($condition)->num_rows();
        $num_rows= $this->campaign->getCampaignKategori('','','',$condition)->num_rows();
        $total_row = !empty($num_rows) ? $num_rows : 0;

        $config['total_rows'] 	= $total_row; 
        $config['per_page'] 	= 6;
        $config['uri_segment'] 	= 2;
    	$choice 				= $config['total_rows']/$config['per_page'];
   	    $config['num_links'] 	= round($choice);

        $this->pagination->initialize($config);

    	$page =($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

//        $data["_data_campaign_list"]    = $this->campaign->fetch_data($config["per_page"], $page,$condition);
        $data["_data_campaign_list"]    = $this->campaign->getCampaignKategori('',$config["per_page"],$page,$condition)->result();

        $data["links"]      			= $this->pagination->create_links();
        $data['config'] = $config;
      	$this->parser->parse("index",$data);
	}
	public function beasiswa($txt = array())

	{

		$data = $this->_data;

		$searchterm = $this->searchterm_handler($this->input->post('searchdonasi'));
		$condition["title LIKE "]  = "%" . $searchterm . "%";

		// Pagination
        $config = array();
        $config["base_url"] = base_url() . "beasiswa";

//        $num_rows= $this->campaign->record_count($condition)->num_rows();
        $num_rows= $this->campaign->getCampaignKategori(2)->num_rows();
        $total_row = !empty($num_rows) ? $num_rows : 0;

        $config['total_rows'] 	= $total_row;
        $config['per_page'] 	= 6;
        $config['uri_segment'] 	= 2;
    	$choice 				= $config['total_rows']/$config['per_page'];
   	    $config['num_links'] 	= round($choice);

        $this->pagination->initialize($config);

    	$page =($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

//        $data["_data_campaign_list"]    = $this->campaign->fetch_data($config["per_page"], $page,$condition);
        $data["_data_campaign_list"]    = $this->campaign->getCampaignKategori(2,$config["per_page"],$page)->result();

        $data["links"]      			= $this->pagination->create_links();
        $data['config'] = $config;
        $data['titles'] = 'Beasiswa';
      	$this->parser->parse("index",$data);
	}
	public function investasi($txt = array())

	{

		$data = $this->_data;

		$searchterm = $this->searchterm_handler($this->input->post('searchdonasi'));
		$condition["title LIKE "]  = "%" . $searchterm . "%";

		// Pagination
        $config = array();
        $config["base_url"] = base_url() . "beasiswa";

//        $num_rows= $this->campaign->record_count($condition)->num_rows();
        $num_rows= $this->campaign->getCampaignKategori(3)->num_rows();
        $total_row = !empty($num_rows) ? $num_rows : 0;

        $config['total_rows'] 	= $total_row;
        $config['per_page'] 	= 6;
        $config['uri_segment'] 	= 2;
    	$choice 				= $config['total_rows']/$config['per_page'];
   	    $config['num_links'] 	= round($choice);

        $this->pagination->initialize($config);

    	$page =($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

//        $data["_data_campaign_list"]    = $this->campaign->fetch_data($config["per_page"], $page,$condition);
        $data["_data_campaign_list"]    = $this->campaign->getCampaignKategori(3,$config["per_page"],$page)->result();

        $data["links"]      			= $this->pagination->create_links();
        $data['config'] = $config;
        $data['titles'] = 'Investasi';
      	$this->parser->parse("index",$data);
	}

	public function searchterm_handler($searchterm)
	{
	    if($searchterm || $searchterm == "")
	    {
            $searchterm = str_replace(" ","%",$searchterm);
	        $this->session->set_userdata('searchdonasi', $searchterm);
	        return $searchterm;
	    }
	    elseif($this->session->userdata('searchdonasi'))
	    {
	        $searchterm = $this->session->userdata('searchdonasi');
            $searchterm = str_replace(" ","%",$searchterm);
	        return $searchterm;
	    }
	    else
	    {
	        $searchterm = "";
	        return $searchterm;
	    }
	}

	public function detail($slug = "")

	{
//        ['meta_title'] = 'asda';
//        print_r($this->config->config);exit;

		// $id =($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		//$where = array("id"=>$id);

        $data = $this->_data;
        $detailcampaign=  !empty($this->campaign->detail($slug)->result()) ? $this->campaign->detail($slug)->result() : array() ;
        $data['meta_title'] = ''.ucfirst($detailcampaign[0]->title);
        $data['meta_description'] = $detailcampaign[0]->desc_short." -  Infonya bisa dilihat di: ". base_url();
        $data['meta_keyword'] = $detailcampaign[0]->meta_keyword;
        $data['meta_author'] = "MumuApps - ".$detailcampaign[0]->fname;
        $data['meta_tipe'] = "Klik untuk membantu";

//		print_r($detailcampaign);exit;
		$data["_data_campaign_detail"] = $detailcampaign;
		$data["_data_campaign_popular"] = !empty($this->campaign->popular()->result()) ? $this->campaign->popular()->result() : array() ;
		$id_campaign           = !empty($this->campaign->getID($slug)) ? $this->campaign->getID($slug)->id : "";
		$where = array("campaign"=>$id_campaign,"status"=>"verified");

		$data["_data_donatur"] = !empty($this->campaign->donatur($where)->result()) ? $this->campaign->donatur($where)->result() : array() ;

		$this->parser->parse("detail",$data);

	}

}

