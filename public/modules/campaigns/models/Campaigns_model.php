<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Campaigns_model extends CI_Model {



	private $_table1 = "v_campaign_list";



  	private function _kunci($data = array()){

        $result = $data;



        if(!is_array($data))

            $result = array("id" => $data);



        return $result;

    }



    function changeTable($table = "")

    {

        $this->_table1 = $table;

    }



	function data($key = "")

	{

        $this->db->select("*");

        $this->db->from($this->_table1);	



        if (!empty($key) || is_array($key))

            $this->db->where($this->_kunci($key));



        return $this->db;

	}



    function record_count($condition = array()){

        $this->changeTable("v_campaign_list");

        $result = $this->data($condition)->get();

        return $result;

    }



    function fetch_data($limit, $offset, $condition = array())

    {

        $this->changeTable("v_campaign_list");

        $this->db->limit($limit, $offset);

        $result = $this->data($condition)->get();



        return !empty($result->result()) ? $result->result() : array();

    }

	

	function detail($where){

		$this->db->select("abc.id,abc.img, abc.title, 
		abc.slug, abc.author, abc.desc_short, abc.target, 
		abc.now, abc.desc, abc.update_progress, 
		to_days(abc.valid_date) - to_days(curdate()) as val, 
		efd.fname, efd.img as imgadm,
		abc.meta_title, abc.meta_description,abc.meta_keyword
		");

		$this->db->from("nb_campaign as abc");
		$this->db->join("nb_sys_users as efd","abc.user_post = efd.id");

		$this->db->where('slug', $where);

		$query = $this->db->get();

		

		return $query;

	}

	

	function popular(){

		$sql="select id, slug, title, now, img, to_days(valid_date) - to_days(curdate()) as valid from nb_campaign order by sum_donatur desc limit 3";

		return $this->db->query($sql);

	}
	
	function donatur($where){
		$this->db->order_by('no_transaksi', 'DESC');
		$this->db->select('*');
		$this->db->from('nb_trans_donation as t');
		$this->db->join('nb_donaturs_d as d', 't.donatur = d.donatur');
		$this->db->where($where);
		$query = $this->db->get();
		
		return $query;
	}

	function getID($slug = "")
	{
		$this->db->select("id");
		$this->db->from("campaign");
		$this->db->where("slug",$slug);

		return $this->db->get()->row();
	}

    function getCampaignKategori($idkat='',$limit='',$offset='',$condition=[]){
        $this->db->select('c.id AS id,
                        c.title AS title,
                        c.slug AS slug,
                        c.desc_short AS desc_short,
                        c.target AS target,
                        c.now AS now,
                        c.img AS img,
                        c.user_type AS user_type,
                        c.valid_date AS valid_date,
                        concat( c.author ) AS name_userpost,
                        to_days( c.valid_date ) - to_days(
                        curdate()) AS selisih_validate ,
                        cc.name kategori, 
                        efd.fname, 
                        efd.img as imgadm');
        $this->db->from('nb_campaign c');
        $this->db->join('nb_campaign_categories cc','cc.id = c.categories');
        $this->db->join("nb_sys_users as efd","c.user_post = efd.id");
        $this->db->where('c.is_active','1');
        $this->db->where('cc.is_active','1');
        if(!empty($condition)) {
            $this->db->where($condition);
        }
        if(!empty($idkat)){
            $this->db->where('cc.id',$idkat);
        }
        if(!empty($limit)){
            $this->db->limit($limit,$offset);
        }
        $this->db->order_by('c.now','desc');
        $this->db->order_by('c.valid_date','desc');
        return $this->db->get();
    }

}



?>