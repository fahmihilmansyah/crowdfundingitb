<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Campaigns extends NBFront_Controller {



	/**

	 * ----------------------------------------

	 * #NB Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_modList   = array('Campaigns_model' => 'campaign');



	protected $_data    = array();



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------

		$this->modelsApp();

        $this->load->library("pagination");

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	public function index($txt = array())

	{

		$data = $this->_data;
		$kondisi = array();
		if(!empty($txt)){
			$kondisi = array('title'=>$txt);
		}
		// Pagination       
        $config = array();
        $config["base_url"] = base_url() . "donasi/list";

        $num_rows= $this->campaign->record_count(null,$kondisi)->num_rows();
        $total_row = !empty($num_rows) ? $num_rows : 0;

        $config['total_rows'] = $total_row; 
        $config['per_page'] = 6;
        $config['uri_segment'] = 3;
    	$choice = $config['total_rows']/$config['per_page'];
   	    $config['num_links'] = round($choice);

        $this->pagination->initialize($config);

    	$page =($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

    	if(!empty($txt)){
			$kondisi = array('title'=>$txt);
		}
        $data["_data_campaign_list"]    = $this->campaign->fetch_data($config["per_page"], $page,null,$kondisi);
        
        $data["links"]      			= $this->pagination->create_links();
        
		$this->parser->parse("index",$data);

	}



	public function detail()

	{
		
		$data = $this->_data;
		
		$id =($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		//$where = array("id"=>$id);
		
		$data["_data_campaign_detail"] = !empty($this->campaign->detail($id)->result()) ? $this->campaign->detail($id)->result() : array() ;
		$data["_data_campaign_popular"] = !empty($this->campaign->popular()->result()) ? $this->campaign->popular()->result() : array() ;
		$data["_data_donatur"] = !empty($this->campaign->donatur($id)->result()) ? $this->campaign->donatur($id)->result() : array() ;

		$this->parser->parse("detail",$data);

	}

}

