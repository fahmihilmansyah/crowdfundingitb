{_layHeader}

<div class="container-fluid grey">

    <div class="don-head">

        <div class="container">

            <?php
            if (!empty($_data_campaign_detail)) {
                ?>

                <!-- start create campaign main -->
                <?php foreach ($_data_campaign_detail as $r) { ?>
                    <div class="main-campaign-wrap">
                        <div class="col-md-5" style="overflow:hidden; padding:0px">
                            <?php
                                $img = !empty($r->img) ? $r->img : '';
                                
                                $img = URI::existsImage('campaign/thumbnails/', 
                                                              $img, 
                                                              'noimg_front/',
                                                              'no-amini.jpg');
                            ?>

                            <img src="<?php echo $img; ?>"
                                 style="position:relative; height: 442px; width:100%">
                        </div>
                        <div class="col-md-7 main-campaign-konten">


                            <h1 class="bold">
                                <?php echo !empty($r->title) ? $r->title : "-"; ?>
                            </h1>

                            <p>
                                Oleh:
                                <?php echo !empty($r->author) ? $r->author : "-"; ?> ( Admin GaneshaBisa)
                            </p>

                            <p>
                                <?php echo !empty($r->desc_short) ? $r->desc_short : "-"; ?>
                            </p>

                            <div style="padding-top:70px;">
                                <div class="barWrapper">
                                    <div class="progress">
                                        <?php
                                        $now = !empty($r->now) ? $r->now : 0;
                                        $target = !empty($r->target) ? $r->target : 0;

                                        if ($target == 0) {
                                            $hitung = 0;
                                        } elseif (($now / $target) >= 1) {
                                            $hitung = 100;
                                        } else {
                                            $hitung = ($now / $target) * 100;
                                        }
                                        ?>
                                        <div class="progress">
                                            <div
                                                    class="progress-bar"
                                                    role="progressbar"
                                                    aria-valuenow="<?php echo $hitung; ?>"
                                                    aria-valuemin="0"
                                                    aria-valuemax="100"
                                            >
                                                <b><?php echo round($hitung, 2); ?>%</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="margin-bottom:30px">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <p class="hidden-xs hidden-sm">Terkumpul</p>
                                    <span class="sm-bold size14">
										Rp. <?php echo number_format($now, 0, ",", ".") ?>
									</span>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <p class="hidden-xs hidden-sm">&nbsp;</p>
                                    <span class="fa fa-clock-o"></span> :
                                    <span class="sm-bold size14">

										<?php
                                        $selisih_validate = !empty($r->val) ? $r->val : 0;
                                        $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;

                                        echo $selisih_validate;
                                        ?>
                                        hari Lagi
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- end create campaign -->
                <?php
            } else {
                ?>
                <div class="main-campaign-wrap">

                    <div class="col-sm-5" style="overflow:hidden">

                        <img src="<?php //URI::baseURL();
                        ?>assets/images/maincampaign/sample.jpg"

                             style="position:relative; left:-15px; height: 442px; width:100%">

                    </div>

                    <div class="col-sm-6 main-campaign-konten">

                        <h3 class="sm-bold">URGENT CAMPAIGN</h3>

                        <h1 class="bold">SAVE ROHINGYA</h1>

                        <p>oleh: Bambang</p>

                        <p>

                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

                        </p>


                        <div style="padding-top:70px;">

                            <div class="barWrapper">

                                <div class="progress">
                                    <div
                                            class="progress-bar"
                                            role="progressbar"
                                            aria-valuenow="60%"
                                            aria-valuemin="0"
                                            aria-valuemax="100"
                                    >
                                        <b>60%</b>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div style="margin-bottom:20px">
                            <div class="col-md-4">

                                Terkumpul <br/>

                                <span class="sm-bold size14">Rp. 1.723.000</span>
                            </div>

                            <div class="col-md-4">
                                <span class="fa fa-clock-o"></span> :

                                <span class="sm-bold size14">7 hari Lagi</span>

                            </div>

                            <div class="col-md-4">
                                <a href="javascript:void(0)"

                                   class="btn btn-success padd-rl-50"

                                   style="padding-left:50px; padding-right:50px;">

                                    DONASI

                                </a>
                            </div>

                        </div>

                    </div>

                </div>
                <?php
            }
            ?>
        </div>

    </div>

    <div class="don-body mb20">

        <div class="container">

            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="btn-top-donasi">
                    <?php if ($selisih_validate != 0): ?>

                        <a href="<?php echo base_url('donasi/aksi/' . $r->id); ?>" class="btn btn-success btn-donasi">

                            <h2 class="sm-bold hidden-xs">DONASI SEKARANG</h2>

                            <h4 class="sm-bold hidden-lg hidden-md hidden-sm" style="position: relative; top:13px;">
                                DONASI SEKARANG</h4>
                        </a>

                    <?php else: ?>
                        <a href="#" class="btn btn-warning btn-donasi">

                            <h2 class="sm-bold hidden-xs">CAMPAIGN TELAH SELESAI</h2>

                            <h4 class="sm-bold hidden-lg hidden-md hidden-sm" style="position: relative; top:13px;">
                                CAMPAIGN TELAH SELESAI</h4>
                        </a>
                    <?php endif; ?>
                </div>
                <div class="tab-info mtb20">

                    <!-- Nav tabs -->

                    <div class="tab-nb">

                        <ul id="tabs-campaigns" class="nav nav-tabs" role="tablist">

                            <li role="presentation" class="active w33">

                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="no-bt">

                                    <center><h4 class="bold">DETAILS</h4></center>

                                </a>

                            </li>

                            <li role="presentation" class="w33">

                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"
                                   class="border-lr no-bt">

                                    <center><h4 class="bold">UPDATES</h4></center>

                                </a>

                            </li>

                            <li role="presentation" class="w33">

                                <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" class="no-bt">

                                    <center><h4 class="bold">DONATUR</h4></center>

                                </a>

                            </li>

                        </ul>


                        <!-- Tab panes -->

                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane active" id="home">
                                <?php foreach ($_data_campaign_detail as $r) {
                                    echo $r->desc;
                                } ?>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="profile">
                                <?php foreach ($_data_campaign_detail as $r) {
									if(!empty($r->update_progress)){
										echo $r->update_progress;
									} else { ?>
										<div class="alert alert-warning" role="alert">
										  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										  <span class="sr-only"></span>
										  Belum ada Update di campaign ini
										</div>
									<?php }
                                } ?>
								
                            </div>

                            <div role="tabpanel" class="tab-pane" id="messages">
                                <?php
										if(!empty($_data_donatur)){
										foreach($_data_donatur as $r){ ?>
											<div>

												<div class="pull-left" style="margin-right:5px;">

													<img style="height:82px; weight:82px; position: relative;"

														src="<?php echo base_url('assets/images/donatur/profile/'.$r->img); ?>">

												</div>

												<div style="width:90%">

													<span><h4 style="color:#0255a5; font-weight: bold;"><?php echo "Rp" .$r->nominal; ?></h4></span>

													<span><h6><?php echo $r->namaDonatur; ?></h6></span>

													<span><?php echo date("d F y", strtotime($r->trx_date)); ?></span>
													<br/>

												</div>
												
												<div style="width:100%">
													<br/>
													<p><?php echo $r->comment; ?></p>
												</div>
												<hr>

											</div>
										<?php } }else{ ?>
											<div class="alert alert-warning" role="alert">
											  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
											  <span class="sr-only"></span>
											  Belum ada donatur di campaign ini
											</div>
										<?php } ?>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="btn-bot-donasi">
                    <?php if ($selisih_validate != 0): ?>
                        <?php foreach ($_data_campaign_detail as $r) { ?>
                            <a href="<?php echo base_url('donasi/aksi/' . $r->id); ?>" class="btn btn-success btn-donasi">
                        <?php } ?>
                        <h2 class="sm-bold hidden-xs">DONASI SEKARANG</h2>

                        <h4 class="sm-bold hidden-lg hidden-md hidden-sm" style="position: relative; top:13px;">
                            DONASI
                            SEKARANG</h4>

                        </a>
                    <?php else: ?>
                        <a href="#" class="btn btn-warning btn-donasi">

                            <h2 class="sm-bold hidden-xs">CAMPAIGN TELAH SELESAI</h2>

                            <h4 class="sm-bold hidden-lg hidden-md hidden-sm" style="position: relative; top:13px;">
                                CAMPAIGN TELAH SELESAI</h4>
                        </a>
                    <?php endif; ?>
                </div>
                <br clear="all"/>

            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">

                <div class="don-share">

                    <center>

                        <h5 class="sm-bold">SHARE</h5>

                    </center>

                    <hr class="line">

                    <br clear="all"/>

                    <div class="page-share-icon">
						<p class="col-md-2 col-sm-3 col-xs-2">

									&nbsp;
								</p>
                        <p class="col-md-2 col-sm-2 col-xs-2">

                            <a
                                    href="#"
                                    data-link="<?php echo base_url('news/' . $_data_campaign_detail[0]->slug) ?>"
                                    data-slug="<?php echo $_data_campaign_detail[0]->slug ?>"
                                    data-judul="<?php echo $_data_campaign_detail[0]->title; ?>"
                                    data-desc="<?php echo $_data_campaign_detail[0]->desc_short; ?>"
                                    class="share-icon fb fb-share" title="shared facebook"></a>

                        </p>

                        <p class="col-md-2 col-sm-2 col-xs-2">
                            <?php //foreach($_data_campaign_detail as $r){ ?>
                            <a data-link="<?php echo base_url('news/' . $_data_campaign_detail[0]->slug) ?>"
                               data-slug="<?php echo $_data_campaign_detail[0]->slug ?>"
                               data-slug="<?php echo $_data_campaign_detail[0]->slug ?>"
                               data-judul="<?php echo $_data_campaign_detail[0]->title; ?>"
                               data-desc="<?php echo $_data_campaign_detail[0]->desc_short; ?>" data-sosial="twitter"
                               target="_blank"
                               href="http://twitter.com/share?text=<?php echo $_data_campaign_detail[0]->title ?>&url=<?php echo base_url() ?>donasi/<?php echo $_data_campaign_detail[0]->slug ?>"
                               class="share-icon twitter btnsosialm" title="shared twitter"></a>
                            <?php //} ?>

                        </p>

                        <p class="col-md-2 col-sm-2 col-xs-2">

                            <a data-link="<?php echo base_url('news/' . $_data_campaign_detail[0]->slug) ?>"
                               data-slug="<?php echo $_data_campaign_detail[0]->slug ?>"
                               data-judul="<?php echo $_data_campaign_detail[0]->title; ?>"
                               data-desc="<?php echo $_data_campaign_detail[0]->desc_short ?>" data-sosial="gplus"
                               target="_blank"
                               href="http://plus.google.com/share?url=<?php echo URI::encode_url(base_url()) . 'donasi/' . $_data_campaign_detail[0]->slug ?>"
                               class="share-icon google btnsosialm" title="shared google"></a>

                        </p>

                        <p class="col-md-2 col-sm-2 col-xs-2">

                            <a data-link="<?php echo base_url('news/' . $_data_campaign_detail[0]->slug) ?>"
                               data-slug="<?php echo $_data_campaign_detail[0]->slug ?>"
                               data-judul="<?php echo $_data_campaign_detail[0]->title; ?>"
                               data-desc="<?php echo $_data_campaign_detail[0]->desc_short ?>" data-sosial="whatsapp"
                               href="javascript:void(0);" data-text="<?php echo $_data_campaign_detail[0]->title; ?>"
                               data-link="<?php echo base_url('donasi/' . $_data_campaign_detail[0]->slug) ?>"
                               class="share-icon btn whatsapp btnsosialm" title="shared whatsapp"></a>

                        </p>
                    </div>
                </div>

                <br clear="all"/>

                <div class="don-popular">

                    <div class="list-header">

                        <div class="program-campaign-title">

                            <center>

                                <h2 class="bold">DONASI POPULER</h2>

                                <hr class="line">

                            </center>

                        </div>

                    </div>

                    <div>

                        <ul class="list-popular">

                            <?php foreach ($_data_campaign_popular as $r) { ?>

                                <li>

                                    <div class="list-item">

                                        <div class="col-md-4 hidden-sm col-xs-12" style="overflow: hidden;">

                                                    <?php
                                                        $img = !empty($r->img) ? $r->img : '';
                                                        
                                                        $img = URI::existsImage('campaign/mostpopular/', 
                                                                                      $img, 
                                                                                      'noimg_front/',
                                                                                      'no-donpop.jpg');
                                                    ?>
                                            <img src="<?php echo $img; ?>" style="height:98px; width:98px; overflow:hidden">

                                        </div>

                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                                               style="color:#000;"><h4 class="sm-bold"><?php echo $r->title; ?></h4>
                                            </a>

                                            <p class="col-md-12 hidden-xs">Terkumpul:</p>

                                            <p class="col-md-12 col-xs-6">Rp <?php echo $r->now ?></p>

                                            <p class="col-md-12 col-xs-6"><span
                                                        class="fa fa-clock-o"></span> <?php echo ($r->valid <= 0) ? "0" : $r->valid ?> hari lagi
                                            </p>

                                        </div>

                                    </div>

                                </li>

                                <?php

                            }

                            ?>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

{_layFooter}
<script>
    $(document).ready(function () {
        $('.whatsapp').on("click", function (e) {
            var article = $(this).attr("data-text");
            var weburl = $(this).attr("data-link");
            var whats_app_message = encodeURIComponent(article) + " - " + encodeURIComponent(weburl);
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

                var whatsapp_url = "whatsapp://send?text=" + whats_app_message;
                window.location.href = whatsapp_url;
            } else {
                alert("Share ini hanya bisa dipakai di versi web mobile");
            }
        });
        function sharePoint(media, article) {
            $.ajax({
                url: "<?php echo base_url('share');?>",
                method: "get",
                data: {m: media, a: article},
                success: function (msg) {
                    alert();
                }
            });
        }

        $(".fb-share").on('click', function () {
            /*alert("tes");
             FB.ui(
             {
             method: 'share',
             href: 'https://developers.facebook.com/docs/',
             },
             // callback
             function(response) {
             if (response && !response.error_message) {
             alert('Posting completed.');
             } else {
             alert('Error while posting.');
             }
             }
             );*/
            var judul = $(this).data('judul');
            var link = $(this).data('link');
            var desc = $(this).data('desc');
            var slug = $(this).data('slug');
            $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
                FB.init({
//                     appId: '1901519173464657',
//                    appId: '822306031169238',
                    appId: '1931618580449735',
                    xfbml:'1',
//                    version: 'v2.3' // or v2.0, v2.1, v2.0
                    version: 'v2.9' // or v2.0, v2.1, v2.0
                });
                FB.ui({
                        method: 'share',
                        title: judul,
                        description: desc,
                        href: link,
                    },
                    function (response) {
                        if (response && !response.error_code) {
                            sharePoint('fb', judul);
                            alert('Posting completed.');
                        }/* else {
                         alert('Error while posting.');
                         }*/
                    });
            });
        });
    })
</script>