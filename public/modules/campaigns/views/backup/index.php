{_layHeader}


<!-- PROGRAM CAMPAIGN -->


<div id="program-campaign">


    <div class="container">


        <div clas="row">


            <div class="col-md-12">


                <div class="program-campaign-title">


                    <center>

                        <br/><br/>

                        <h2 class="bold">AYO DONASI</h2>


                        <hr class="line">


                        <h5>Program akan kami salurkan kepada yang membutuhkan</h5>


                    </center>


                </div>


            </div>


        </div>


        <div class="row">


            <?php

            if (!empty($_data_campaign_list)) {

                foreach ($_data_campaign_list as $r) {


                    ?>


                    <div class="col-md-4" style="margin-bottom:20px">


                        <div class="program-campaign-list">


                            <div class="list-image">

                                <?php
                              
                                $img = !empty($r->img) ? $r->img : '';
                                
                                $img = URI::existsImage('campaign/thumbnails/', 
                                                              $img, 
                                                              'noimg_front/',
                                                              'no-amini.jpg');
                                ?>

                                <img src="<?php echo $img; ?>"
                                     style="width:100%">


                            </div>


                            <div class="list-konten">


                                <h3 class="bold">
                                    <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>" >
                                    <?php
                                    $title = !empty($r->title) ? $r->title : "-";
                                    echo $title;
                                    ?>
                                    </a>
                                </h3>


                                <span>oleh:

                                    <?php
                                    $name_userpost = !empty($r->name_userpost) ? $r->name_userpost : "-";
                                    echo $r->name_userpost;
                                    ?> ( Admin GaneshaBisa)

							</span>


                                <p>

                                    <?php
                                    $desc_short = !empty($r->desc_short) ? $r->desc_short : "-";
                                    echo $r->desc_short;
                                    ?>

                                </p>


                            </div>


                            <div class="list-progress">

                                <div class="barWrapper">

                                    <div class="progress">

                                        <?php
                                        $now = !empty($r->now) ? $r->now : 0;
                                        $target = !empty($r->target) ? $r->target : 0;

                                        if ($target == 0) {
                                            $hitung = 0;
                                        } elseif (($now / $target) >= 1) {
                                            $hitung = 100;
                                        } else {
                                            $hitung = ($now / $target) * 100;
                                        }
                                        ?>

                                        <div class="progress">

                                            <div

                                                    class="progress-bar"

                                                    role="progressbar"

                                                    aria-valuenow="<?php echo $hitung; ?>"

                                                    aria-valuemin="0"

                                                    aria-valuemax="100"

                                            >

                                                <b><?php echo round($hitung, 2); ?>%</b>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>


                            <div class="list-desc">


                                <table width="100%">


                                    <tr>


                                        <td>


                                            Terkumpul <br/>


                                            <span class="sm-bold size14 blue">

											Rp. <?php echo number_format($now, 0, ",", ".") ?>

										</span>


                                        </td>


                                        <td align="right">


                                            <br/>


                                            <span class="fa fa-clock-o"></span> :


                                            <span class="sm-bold size14">

											<span class="blue">

												<?php
                                                $selisih_validate = !empty($r->selisih_validate) ? $r->selisih_validate : 0;
                                                $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;
                                                echo $selisih_validate;
                                                ?>

											</span> 

										hari Lagi</span>


                                        </td>


                                    </tr>


                                    <tr>


                                        <td colspan="2">&nbsp;</td>


                                    </tr>


                                    <tr>


                                        <td align="right" colspan="2">
                                            <?php if($selisih_validate != 0): ?>
                                            <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                                               class="btn btn-success padd-rl-50"
                                               style="padding-left:50px; padding-right:50px;">
                                                DONASI
                                            </a>
                                                <?php else: ?>
                                                <a href="<?php URI::baseURL(); ?>donasi/<?php echo $r->slug ?>"
                                                   class="btn btn-warning padd-rl-50"
                                                   style="padding-left:50px; padding-right:50px;">
                                                    DONASI TELAH SELESAI
                                                </a>
                                            <?php endif; ?>


                                        </td>


                                    </tr>


                                </table>


                            </div>


                        </div>


                    </div>


                    <?php

                }

            } else {

                ?>
                <div style="min-height: 500px;">
                    <div class="col-md-12" style="margin-bottom:20px;">
                        <div class="alert alert-warning">
                            <center>Maaf Konten yang anda cari tidak tersedia</center>
                        </div>
                    </div>
                </div>

                <?php

            }

            ?>


            <div clas="row">


                <div class="col-md-12">


                    <div class="list-all">


                        <center>


                            <?php echo $links; ?>


                        </center>


                    </div>


                </div>


            </div>


        </div>


    </div>


</div>


{_layFooter}