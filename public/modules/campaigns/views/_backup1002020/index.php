{_layHeader}


<!-- PROGRAM CAMPAIGN -->
<section>
    <div class="container">
        <div class="mb-25">
            <div class="h3 bold title-font lh-1 color-second">Ayo Donasi <?php echo !empty($titles)?$titles:'' ?>!</div>
            <div class="text-muted mb-10">Pendanaan akan disalurkan kepada yang membutuhkan</div>
        </div>
        <div class="row">

            <?php
//            echo "<pre>";
//            print_r($_data_campaign_list);exit;
            if (!empty($_data_campaign_list)) {

            foreach ($_data_campaign_list as $r) {
                ?>
                <?php

                $img = !empty($r->img) ? $r->img : '';

                $img = URI::existsImage('campaign/thumbnails/',
                    $img,
                    'noimg_front/',
                    'no-amini.jpg');
                ?>
                <?php
                $now = !empty($r->now) ? $r->now : 0;
                $target = !empty($r->target) ? $r->target : 0;

                if ($target == 0) {
                    $hitung = 0;
                } elseif (($now / $target) >= 1) {
                    $hitung = 100;
                } else {
                    $hitung = ($now / $target) * 100;
                }
                ?>
                <div class="col-md-4">
                    <div class="card overflow-hidden" style="height: 100%;">
                        <div class="bg-donasi"
                             style="background-image:url(<?php echo $img ?>);"></div>
                        <div class="card-body">
                            <div class="mb-25 font-title bold judul-donasi">
                                <?php
                                $title = !empty($r->title) ? $r->title : "-";
                                echo $title;
                                ?>
                            </div>
                            <div class="mb-10"><i class="icofont-user-alt-4 mr-5 text-muted"></i>
                                <?php
                                $name_userpost = !empty($r->name_userpost) ? $r->name_userpost : "-";
                                echo $r->name_userpost;
                                ?>
                            </div>
                            <div class="row">
                                <div class="col-7">
                                    <div class="text-muted"><i class="icofont-"></i> Terkumpul</div>
                                    <div class="bold">Rp <?php echo number_format($now, 0, ",", ".") ?></div>
                                </div>
                                <div class="col-5 text-right">
                                    <div class="text-muted"><i class="icofont-clock"></i> Batas Waktu</div>
                                    <div class="bold"><?php
                                        $selisih_validate = !empty($r->selisih_validate) ? $r->selisih_validate : 0;
                                        $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;
                                        echo $selisih_validate;
                                        ?> hari</div>
                                </div>
                            </div>

                            <div class="progress mt-5">
                                <div class="progress-bar bg-main" role="progressbar" aria-valuenow="<?php echo $hitung ?>"
                                     aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round($hitung, 2); ?>%"></div>
                            </div>
                            <div class="mt-20">
                                <div class="row">
                                    <div class="col-6">
                                        <a href="<?php echo $config['base_url']; ?>/<?php echo $r->slug ?>"
                                           class="btn btn-block btn-light"><span>Lihat Detail</span></a>
                                    </div>
                                    <div class="col-6">

                                        <?php $validate = strtotime($r->valid_date);
                                        if ($validate >= strtotime(date("Y-m-d"))) { ?>
                                            <a href="<?php echo base_url('donasi/aksi/' . $r->id); ?>"
                                               class="btn btn-block btn-main"><span>Danai Sekarang</span></a>
                                        <?php } else { ?>
                                            <a href="#" class="btn btn-block btn-info disabled"><span>Selesai</span></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            }?>
        </div>
        <?php
        $segment = $this->uri->segment(2);
        ?>
        <div class="mt-20">
            <nav aria-label="...">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i
                                class="icofont-rounded-left"></i></a>
                    </li>
                    <?php
                    for ($i = 0; $i <= $config['num_links']; $i++) {
                        $nseg = $i * $config['per_page'];

                        if($nseg == 0){
                            $nseg = '';
                        }
                        $actf = '';
                        $actfx = '';
                        if ($segment == $nseg) {
                            $actfx = 'active';
                            $actf = '<span class="sr-only">(current)</span>';
                            ?>
                            <li class="page-item <?php echo $actfx ?>"><a class="page-link"
                                                                          href="<?php echo $config['base_url'].'/'.$nseg ?>"><?php echo $i + 1 . " " . $actf; ?></a>
                            </li>

                            <?php
                        } else {
                            ?>
                            <li class="page-item "><a class="page-link"
                                                      href="<?php echo $config['base_url'].'/'.$nseg ?>"><?php echo $i + 1; ?></a></li>

                            <?php $actf = '';
                            $actfx = '';
                        }
                    } ?>
                    <li class="page-item">
                        <a class="page-link" href="#"><i class="icofont-rounded-right"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</section>



{_layFooter}