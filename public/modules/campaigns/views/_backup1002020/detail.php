{_layHeader}
<?php
if (!empty($_data_campaign_detail)) {
?>

<!-- start create campaign main -->
<?php foreach ($_data_campaign_detail as $r) { ?>
        <?php
        $img = !empty($r->img) ? $r->img : '';

        $img = URI::existsImage('campaign/thumbnails/',
            $img,
            'noimg_front/',
            'no-amini.jpg');
        ?>
        <?php
        $now = !empty($r->now) ? $r->now : 0;
        $target = !empty($r->target) ? $r->target : 0;

        if ($target == 0) {
            $hitung = 0;
        } elseif (($now / $target) >= 1) {
            $hitung = 100;
        } else {
            $hitung = ($now / $target) * 100;
        }
        ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <img src="<?php echo $img; ?>"
                     style="position:relative; height: 442px; width:100%">
            </div>
            <div class="col-md-4">
                <div class="mb-25 font-title bold judul-donasi-big">
                    <?php echo !empty($r->title) ? $r->title : "-"; ?>
                </div>
                <div class="mb-10"><i class="icofont-user-alt-4 mr-5 text-muted"></i> <?php echo !empty($r->author) ? $r->author : "-"; ?></div>
                <div class="row">
                    <div class="col-7">
                        <div class="text-muted">Terkumpul</div>
                        <div class="bold">Rp <?php echo number_format($now, 0, ",", ".") ?></div>
                    </div>
                    <div class="col-5 text-right">
                        <div class="text-muted">Batas Waktu</div>
                        <div class="bold"><?php
                            $selisih_validate = !empty($r->val) ? $r->val : 0;
                            $selisih_validate = ($selisih_validate <= 0) ? 0 : $selisih_validate;

                            echo $selisih_validate;
                            ?> hari lagi</div>
                    </div>
                </div>
                <div class="progress mt-5">
                    <div class="progress-bar bg-main progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?php echo $hitung; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $hitung; ?>%"></div>
                </div>
                <?php if($selisih_validate > 0){?>
                <a href="<?php echo base_url('donasi/aksi/' . $r->id); ?>" class="btn btn-main btn-block mt-20"><span>Danai Sekarang</span></a>
                <?php }else{?>
                        <label disabled="" class="btn btn-secondary btn-block mt-20"><span>Pendanaan Telah Selesai</span></label>
                <?php }?>
                <div class="mt-30">
                    <div>Bantu campaign ini dengan menjadi Fundraiser</div>
                    <a href="#" class="btn btn-second btn-block mt-5"><span>Jadi Fundraiser</span></a>
                </div>
            </div>
        </div>
        <div class="row mt-40">
            <div class="col-md-8">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-detail-tab" data-toggle="pill" href="#pills-detail" role="tab" aria-controls="pills-detail" aria-selected="true">Detail</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-update-tab" data-toggle="pill" href="#pills-update" role="tab" aria-controls="pills-update" aria-selected="false">Update</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-fundraiser-tab" data-toggle="pill" href="#pills-fundraiser" role="tab" aria-controls="pills-fundraiser" aria-selected="false">Fundraiser (12)</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-detail" role="tabpanel" aria-labelledby="pills-detail-tab">
                        <div class="pendana-wrapper mb-20">
                            <div class="pendana-box">
                                <div class="pendana-img title-font" style="background-image:url(https://upload.wikimedia.org/wikipedia/id/9/95/Logo_Institut_Teknologi_Bandung.png);"></div>
                                <div class="pendana-text">
                                    <div class="text-small">Program Pendanaan Dari</div>
                                    <div class="title-font semibold lh-n"><?php echo !empty($r->author) ? $r->author : "-"; ?></div>
<!--                                    <div><i class="icofont-check verif-icon"></i> <span  class="color-second text-small medium">Terverifikasi</span></div>-->
                                </div>
                            </div>
                        </div>
                        <?php echo !empty($r->desc) ? $r->desc : "-"; ?>
                    </div>
                    <div class="tab-pane fade" id="pills-update" role="tabpanel" aria-labelledby="pills-update-tab">
                        ...
                    </div>
                    <div class="tab-pane fade" id="pills-fundraiser" role="tabpanel" aria-labelledby="pills-fundraiser-tab">
                        ...
                    </div>
                </div>
                <div class="row mt-20">
                    <div class="col-md-6">
                        <?php if($selisih_validate > 0){?>
                        <a href="<?php echo base_url('donasi/aksi/' . $r->id); ?>" class="btn btn-main btn-lg btn-block"><span>Danai Sekarang</span></a>
                        <?php }else{ ?>
                        <label class="btn btn-secondary btn-lg btn-block"><span>Pendanaan Telah Selesai</span></label>
                        <?php } ?>
                    </div>
                </div>
                <div class="row mt-40 mb-40">
                    <div class="col-md-6">
                        <div class="mb-5">Beritahukan Program ini kepada Teman Lewat :</div>
                        <div class="row">
                            <div class="col-6">
                                <a href="#" class="btn btn-facebook btn-block"><span><i class="icofont-facebook"></i> Facebook</span></a>
                            </div>
                            <div class="col-6">
                                <a href="#" class="btn btn-whatsapp whatsapp btnsosialm btn-block"
                                   data-link="<?php echo base_url('donasi/' . $_data_campaign_detail[0]->slug) ?>"
                                   data-slug="<?php echo $_data_campaign_detail[0]->slug ?>"
                                   data-judul="<?php echo $_data_campaign_detail[0]->title; ?>"
                                   data-desc="<?php echo $_data_campaign_detail[0]->desc_short ?>" data-sosial="whatsapp"
                                   href="javascript:void(0);" data-text="<?php echo $_data_campaign_detail[0]->title; ?>"
                                   data-link="<?php echo base_url('donasi/' . $_data_campaign_detail[0]->slug) ?>"
                                ><span><i class="icofont-brand-whatsapp"></i> WhatsApp</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-20 bg-info text-white pt-15 pr-15 pb-15 pl-15">
                    <i class="icofont-info-circle"></i> Disclaimer : Kami Ganesha Bisa tidak mewakili dan bertanggung jawab atas informasi program ini. Segala bentuk informasi yang ada di program ini sepenuhnya milik Penggalang Dana.
                </div>
            </div>
            <div class="col-md-4">
<!--                <div class="right-content">-->
<!--                    <div class="title-font bold color-second mb-10">Pendana (194)</div>-->
<!--                    <div class="pendana-wrapper">-->
<!--                        <div class="pendana-box">-->
<!--                            <div class="pendana-img title-font" style="background-image:url(https://images.pexels.com/photos/736716/pexels-photo-736716.jpeg);"></div>-->
<!--                            <div class="pendana-text">-->
<!--                                <div class="text-muted text-small">2 Desember 2019</div>-->
<!--                                <div class="title-font semibold">Jonah Pattinson</div>-->
<!--                                <div class="medium">Rp 150.000</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="pendana-box">-->
<!--                            <div class="pendana-img title-font">H</div>-->
<!--                            <div class="pendana-text">-->
<!--                                <div class="text-muted text-small">2 Desember 2019</div>-->
<!--                                <div class="title-font semibold">Hamba Allah</div>-->
<!--                                <div class="medium">Rp 400.000</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="pendana-box">-->
<!--                            <div class="pendana-img title-font" style="background-image:url(https://images.pexels.com/photos/736716/pexels-photo-736716.jpeg);"></div>-->
<!--                            <div class="pendana-text">-->
<!--                                <div class="text-muted text-small">2 Desember 2019</div>-->
<!--                                <div class="title-font semibold">Jonah Pattinson</div>-->
<!--                                <div class="medium">Rp 150.000</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="pendana-box">-->
<!--                            <div class="pendana-img title-font">A</div>-->
<!--                            <div class="pendana-text">-->
<!--                                <div class="text-muted text-small">2 Desember 2019</div>-->
<!--                                <div class="title-font semibold">Anonim</div>-->
<!--                                <div class="medium">Rp 100.000</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="pendana-box">-->
<!--                            <div class="pendana-img title-font">A</div>-->
<!--                            <div class="pendana-text">-->
<!--                                <div class="text-muted text-small">2 Desember 2019</div>-->
<!--                                <div class="title-font semibold">Anonim</div>-->
<!--                                <div class="medium">Rp 100.000</div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <a href="#" class="btn btn-main btn-block mt-15"><span>Lihat Pendana Lain</span></a>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</section>
<?php }} ?>

{_layFooter}
<script>
    $(document).ready(function () {
        $('.whatsapp').on("click", function (e) {
            var article = $(this).attr("data-text");
            var weburl = $(this).attr("data-link");
            var whats_app_message = encodeURIComponent(article) + " - " + encodeURIComponent(weburl);
            // if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

                var whatsapp_url = "whatsapp://send?text=" + whats_app_message;
                window.location.href = whatsapp_url;
            // } else {
            //     alert("Share ini hanya bisa dipakai di versi web mobile");
            // }
        });
        function sharePoint(media, article) {
            $.ajax({
                url: "<?php echo base_url('share');?>",
                method: "get",
                data: {m: media, a: article},
                success: function (msg) {
                    alert();
                }
            });
        }

        $(".fb-share").on('click', function () {
            /*alert("tes");
             FB.ui(
             {
             method: 'share',
             href: 'https://developers.facebook.com/docs/',
             },
             // callback
             function(response) {
             if (response && !response.error_message) {
             alert('Posting completed.');
             } else {
             alert('Error while posting.');
             }
             }
             );*/
            var judul = $(this).data('judul');
            var link = $(this).data('link');
            var desc = $(this).data('desc');
            var slug = $(this).data('slug');
            $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
                FB.init({
//                     appId: '1901519173464657',
//                    appId: '822306031169238',
                    appId: '1931618580449735',
                    xfbml:'1',
//                    version: 'v2.3' // or v2.0, v2.1, v2.0
                    version: 'v2.9' // or v2.0, v2.1, v2.0
                });
                FB.ui({
                        method: 'share',
                        title: judul,
                        description: desc,
                        href: link,
                    },
                    function (response) {
                        if (response && !response.error_code) {
                            sharePoint('fb', judul);
                            alert('Posting completed.');
                        }/* else {
                         alert('Error while posting.');
                         }*/
                    });
            });
        });
    })
</script>