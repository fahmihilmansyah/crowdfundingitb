function showMessage(txt,title,type){
  new PNotify({
      title: title,
      text: txt,
      type: type,
      delay: 1000,
      styling: 'bootstrap3'
  });
}

function deleteRow(id){
  var url = $("#" + id).attr("link-target");

    swal({   
      title         : "Apakah Anda Yakin?",   
      text        : "akan menghapus data ini.",
      type        : "warning",   
      showCancelButton  : true,   
      confirmButtonColor  : "#DD6B55",   
      confirmButtonText : "Ya, Hapus!",   
      closeOnConfirm    : false 
  }, function(isConfirm){
    if(isConfirm){
        location.href = url;
    }
    });
}
  
function activateRow(id){
  var url   = $("#" + id).attr("link-target");
  var status  = $("#" + id).attr("status");
  var text    = (status == 1) ? "akan menonaktifkan data ini." : "akan mengaktifkan data ini.";

    swal({   
      title         : "Apakah Anda Yakin?",   
      text        : text,
      type        : "warning",   
      showCancelButton  : true,   
      confirmButtonColor  : "#DD6B55",   
      confirmButtonText : "Ya!",   
      closeOnConfirm    : false 
  }, function(isConfirm){
    if(isConfirm){
        location.href = url;
    }
    });
}

function cekTokenValid(url){
	var result = $.ajax({
        type: "GET",
        url  : url,
	    dataType: 'html',
	    context: document.body,
	    global: false,
	    async:false,
        success: function(result){
        	return result;
        }	
    }).responseText;

    valid = $.parseJSON(result);

	if(valid != true){
		location.href = "token/expired";
	}
}

function _s2_icon(e) {
    return "<i class='" + e.text + "'></i> " + e.text
}

function select2_icon(id) {
    $("#" + id).select2({searchable: false, formatResult: _s2_icon, formatSelection: _s2_icon, escapeMarkup: function(e) {
        return e;
    }}).on('change', function() {
        $(this).valid();
    });
}

function PreviewImage(no) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
    };
}