{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Kategori FAQ [ New ]</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Form
							<small>Input Kategori FAQ</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">

						<!-- start form for validation -->
						<h5 class="alert alert-danger alert-dismissible fade in peringatan">
							Terdapat kesalahan dalam pengisian form. Silahkan cek kembali data form anda!
						</h5>
					    <?php
						    $hidden_form = array('id' => !empty($id) ? $id : '');
						    echo form_open_multipart($action, array('id' => 'fmfaqkategori'), $hidden_form);
					    ?>
								<div>
									<label for="title">Title <span class="require">*</span> :</label>
									<div>
		                			<?php 
		                				$name  = 'data[title]';
		                				$value = !empty($default["title"]) ? $default["title"] : '';
		                				$extra = 'id="title" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="message">Deskripsi :</label>
									<div>
		                			<?php 
		                				$name  = 'data[description]';
		                				$value = !empty($default["description"]) ? $default["description"] : '';
		                				$extra = 'id="description" 
		                						  class="form-control mini-height" 
		                						  placeholder="isi disini"';

		                				echo form_textarea($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>
								
								<div>

											<label for="message">Parent <span class="require">*</span> :</label>

											<div>

			                				<?php 

												$name         = "data[parent]";

												$value        = !empty($default["parent"]) ? $default["parent"] : '';

												$extra        = 'id="parent" style="width:100%"';



			                					echo form_dropdown($name, $parent, $value, $extra); 

			                				?>

				                			</div>

				                			<p class='label-error'></p>

										</div>
								
								<br/>
								
								<div>
									<label for="message">Key Search :</label>
									<div>
		                			<?php 
		                				$name  = 'data[key_search]';
		                				$value = !empty($default["key_search"]) ? $default["key_search"] : '';
		                				$extra = 'id="key_search" 
		                						  class="form-control mini-height" 
		                						  placeholder="isi disini"';

		                				echo form_textarea($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label class="agreement">
										<div>
										<?php
											$data = array(
											        'name'          => 'agreement',
											        'id'            => 'agreement',
											        'value'         => 'accept',
											        'checked'       => FALSE,
											);

											echo form_checkbox($data);
										?>
										Saya yakin dan bertanggung jawab atas penambahan data ini
										</div>
		                				<p class='label-error'></p>
									</label>
								</div>

								<div class="actionBar">
									<a href="<?php URI::baseURL(); ?>cms/faq_des" class="btn btn-primary">
										<i class="fa fa-reply"></i> Kembali
									</a>
									<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>
								</div>
					    <?php
					    	echo form_close();
					    ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}

<script type="text/javascript">
	$(document).ready(function() {
		$('.tree').treegrid({
		    expanderExpandedClass	: 'glyphicon glyphicon-minus',
		    expanderCollapsedClass	: 'glyphicon glyphicon-plus'
		});

		var validator = $("#fmfaqkategori").validate({
			debug: true,
			errorElement: "em",
			errorContainer: $(".peringatan"),
			errorPlacement: function(error, element) {
				error.appendTo(element.parent("div").next("p"));
			},
			highlight: function(element) {   // <-- fires when element has error
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('sukses').addClass('error');
			},
			unhighlight: function(element) { // <-- fires when element is valid
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('error').addClass('sukses');
			},
			rules: {
				'data[name]': {
					required: true,
				},
				'agreement':{
					required: true
				}
			}, 
			messages: {
		        'data[name]' : "Field ini wajib disi",
		        'agreement'  : "Anda harus menyetujui pernyataan terlebih dahulu!"
		    },
			submitHandler: function(form) {
		        form.submit();
		    }
		});

		$("#agreement").click(function(){
			
		});
	});
</script>