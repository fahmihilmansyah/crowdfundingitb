{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Manajemen Bank [ View ]</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							View
							<small>Data Manajemen Bank</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">
						<table class="table table-striped table-bordered" >
							<tr>
								<td width="20%">Nama Bank</td>
								<td width="1%">:</td>
								<td><?php echo !empty($default['namaBank']) ? $default['namaBank'] : '-'; ?></td>
							</tr>
							<tr>
								<td>No Rekening</td>
								<td>:</td>
								<td><?php echo !empty($default['no_rekening']) ? $default['no_rekening'] : '-'; ?></td>
							</tr>
							<tr>
								<td width="20%">Atas Nama</td>
								<td width="1%">:</td>
								<td><?php echo !empty($default['atasnama']) ? $default['atasnama'] : '-'; ?></td>
							</tr>
							<tr>
								<td>Cabang</td>
								<td>:</td>
								<td><?php echo !empty($default['cabang']) ? $default['cabang'] : '-'; ?></td>
							</tr>
							
						</table>

                      	<div class="ln_solid"></div>

						<a href="<?php URI::baseURL(); ?>cms/manajemenbank" class="btn btn-primary">
							<i class="fa fa-reply"></i> Kembali
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}
<script type="text/javascript">
	$('.tree').treegrid({
	    expanderExpandedClass	: 'glyphicon glyphicon-minus',
	    expanderCollapsedClass	: 'glyphicon glyphicon-plus'
	});
</script>
