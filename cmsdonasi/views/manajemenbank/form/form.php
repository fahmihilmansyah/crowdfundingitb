{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Manajemen Bank [ New ]</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Form
							<small>Input Bank</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">

						<!-- start form for validation -->
						<h5 class="alert alert-danger alert-dismissible fade in peringatan">
							Terdapat kesalahan dalam pengisian form. Silahkan cek kembali data form anda!
						</h5>
					    <?php
						    $hidden_form = array('id' => !empty($id) ? $id : '');
						    echo form_open_multipart($action, array('id' => 'fmbank'), $hidden_form);
					    ?>
								<div>
									<label for="namaBank">Nama Bank <span class="require">*</span> :</label>
									<div>
		                			<?php 
		                				$name  = 'data[namaBank]';
		                				$value = !empty($default["namaBank"]) ? $default["namaBank"] : '';
		                				$extra = 'id="namaBank" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="no_rekening">No Rekening :</label>
									<div>
		                			<?php 
		                				$name  = 'data[no_rekening]';
		                				$value = !empty($default["no_rekening"]) ? $default["no_rekening"] : '';
		                				$extra = 'id="no_rekening" 
		                						  class="form-control mini-height" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>
								
								<br/>
								
								<div>
									<label for="atasnama">Atas Nama :</label>
									<div>
		                			<?php 
		                				$name  = 'data[atasnama]';
		                				$value = !empty($default["atasnama"]) ? $default["atasnama"] : '';
		                				$extra = 'id="atasnama" 
		                						  class="form-control mini-height" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>
								
								<div>
									<label for="cabang">Cabang :</label>
									<div>
		                			<?php 
		                				$name  = 'data[cabang]';
		                				$value = !empty($default["cabang"]) ? $default["cabang"] : '';
		                				$extra = 'id="cabang" 
		                						  class="form-control mini-height" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>
								
								<div>
									<label for="gambar">Gambar:</label>

											<div class="uibutton" style="overflow:hidden;">

								                <?php 

								                    $img = !empty($default["imglink"]) ? $default["imglink"] :'noimage.png'; 

								                    $url_image = base_url() . "assets/images/bank/" . $img;

								                ?>

								                <img id="uploadPreview2" src="<?php echo $url_image; ?>" class="upload" height="200px"/>

								                <br/>

								                <?php 

								                	$value = !empty($default["imglink"]) ? $default["imglink"] : '';

								                	$extra = 'class="form-control"';

								                	echo form_hidden('imglink', $value, $extra); 

								                ?>

								                <br/>

								                <div class="myuibutton">

								                    <span>Pilih Gambar</span>

								                    <?php 

								                    	$extra = "id='uploadImage2' onchange='PreviewImage(2);' multiple='multiple'";

								                    	echo form_upload('uploadedimages[]','', $extra); 

								                    ?>

								                    <!-- <input id="uploadImage2" type="file" name="data[images]" onchange="PreviewImage(2);"/> -->

								                </div>
												
                								<p class='label-error'></p>

				                			</div>
								</div>

								<br/>

								<div>
									<label class="agreement">
										<div>
										<?php
											$data = array(
											        'name'          => 'agreement',
											        'id'            => 'agreement',
											        'value'         => 'accept',
											        'checked'       => FALSE,
											);

											echo form_checkbox($data);
										?>
										Saya yakin dan bertanggung jawab atas penambahan data ini
										</div>
		                				<p class='label-error'></p>
									</label>
								</div>

								<div class="actionBar">
									<a href="<?php URI::baseURL(); ?>cms/manajemenbank" class="btn btn-primary">
										<i class="fa fa-reply"></i> Kembali
									</a>
									<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>
								</div>
					    <?php
					    	echo form_close();
					    ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}

<script type="text/javascript">
	$(document).ready(function() {
		$('.tree').treegrid({
		    expanderExpandedClass	: 'glyphicon glyphicon-minus',
		    expanderCollapsedClass	: 'glyphicon glyphicon-plus'
		});

		var validator = $("#fmbank").validate({
			debug: true,
			errorElement: "em",
			errorContainer: $(".peringatan"),
			errorPlacement: function(error, element) {
				error.appendTo(element.parent("div").next("p"));
			},
			highlight: function(element) {   // <-- fires when element has error
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('sukses').addClass('error');
			},
			unhighlight: function(element) { // <-- fires when element is valid
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('error').addClass('sukses');
			},
			rules: {
				'data[name]': {
					required: true,
				},
				'agreement':{
					required: true
				}
			}, 
			messages: {
		        'data[name]' : "Field ini wajib disi",
		        'agreement'  : "Anda harus menyetujui pernyataan terlebih dahulu!"
		    },
			submitHandler: function(form) {
		        form.submit();
		    }
		});

		$("#agreement").click(function(){
			
		});
	});
</script>