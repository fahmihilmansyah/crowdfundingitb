<!DOCTYPE html>

<html lang="en">

  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Meta, title, CSS, favicons, etc. -->

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <title><?php echo $_appTitle; ?> </title>

    

    <link rel="icon"  type="image/png" href="<?php URI::baseURL(); ?>assets/images/logo/favicon.png" />



    <?php

      echo $_cssHeader;

    ?>

  </head>



  <body class="nav-md">

    <div class="container body">

      <div class="main_container">

        <div class="col-md-3 left_col menu_fixed">

          <div class="left_col scroll-view">

            <div class="navbar nav_title" style="border: 0;">

              <a href="<?php URI::baseURL(); ?>cms" class="site_title">

                  <i class="fa fa-laptop" style="border:0px solid black !important;"></i> 

                  <span>GB CPanel</span>

              </a>

            </div>



            <div class="clearfix"></div>



            <!-- menu profile quick info -->

            <div class="profile">

              <div class="profile_pic">

                <?php
                $uid = $this->session->userdata('usr_id');
                $ck = $this->db->query("select * from nb_sys_users where nb_sys_users.id = ".$uid)->result();
                $pprofile = !empty($ck[0]->img) ? "/admprofil/".$ck[0]->img : "user.png"; ?>

                <img src="<?php URI::baseURL(); ?>assets/images/<?php echo $pprofile; ?>" alt="..." class="img-circle profile_img">

              </div>

              <div class="profile_info">

                <span>Selamat Datang,</span>

                <h2><?php echo  ucwords($_username); ?></h2>

              </div>

            </div>

            <!-- /menu profile quick info -->



            <br />



            <!-- sidebar menu -->

            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

              <div class="menu_section">

                <h3>&nbsp;</h3>

                <?php

                  echo $_menu;

                ?>

              </div>

            </div>

            <!-- /sidebar menu -->



            <!-- /menu footer buttons -->

            <!--<div class="sidebar-footer hidden-small">

              <a data-toggle="tooltip" data-placement="top" title="Settings">

                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>

              </a>

              <a data-toggle="tooltip" data-placement="top" title="FullScreen">

                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>

              </a>

              <a data-toggle="tooltip" data-placement="top" title="Lock">

                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>

              </a>

              <a data-toggle="tooltip" data-placement="top" title="Logout">

                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>

              </a>

            </div>-->

            <!-- /menu footer buttons -->

          </div>

        </div>



        <!-- top navigation -->

        <div class="top_nav">

          <div class="nav_menu">

            <nav>

              <div class="nav toggle">

                <a id="menu_toggle"><i class="fa fa-bars"></i></a>

              </div>



              <ul class="nav navbar-nav navbar-right">

                <li class="">

                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                    <?php
                        $uid = $this->session->userdata('usr_id');
                        $ck = $this->db->query("select * from nb_sys_users where nb_sys_users.id = ".$uid)->result();
                        if(!empty($ck[0]->img)){ ?>
                            <img src="<?php URI::baseURL(); ?>assets/images/admprofil/<?php echo $ck[0]->img; ?>" alt="">

                        <?php }else{ ?>
                            <img src="<?php URI::baseURL(); ?>assets/images/user.png" alt="">
                            <?php 
                        }
                    ?>
                    <?php echo  ucwords($_username); ?>, Login as ( <b><?php echo $this->session->userdata("usr_role_name"); ?></b> ) 

                    <span class=" fa fa-angle-down"></span>

                  </a>

                  <ul class="dropdown-menu dropdown-usermenu pull-right">

                    <li><a href="<?php URI::baseURL(); ?>cms/usrinfo"> Profile</a></li>

                    <li><a id="helpcms" 

                            href="javascript:void(0)" 

                            link-target="<?php URI::baseURL(); ?>cms/help" 

                            onclick="Help()">Help</a></li>

                    <li>

                        <a  id="logoutcms" 

                            href="javascript:void(0)" 

                            link-target="<?php URI::baseURL(); ?>cms/logout" 

                            onclick="Logout(this.id)"

                        >

                          <i class="fa fa-sign-out pull-right"></i> Log Out

                        </a>

                    </li>

                  </ul>

                </li>

              </ul>

            </nav>

          </div>

        </div>

        <!-- /top navigation -->