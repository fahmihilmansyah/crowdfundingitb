{_layHeader}

<!-- page content -->

<div class="right_col" role="main">

    <div class="page-title">

        <div class="title_left">

            <h3>Manajemen Donasi</h3>

        </div>

    </div>


    <div class="clearfix"></div>


    <div class="row">

        <?php echo $crumbs; ?>

    </div>


    <div class="clearfix"></div>


    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>

                        Data

                        <small>List Konfirmasi</small>

                    </h2>

                    <ul class="nav navbar-right panel_toolbox">

                        <li>

                            <a class="collapse-link">

                                <i class="fa fa-chevron-up"></i>

                            </a>

                        </li>

                        <li class="dropdown">

                            <a href="#"

                               class="dropdown-toggle"

                               data-toggle="dropdown"

                               role="button"

                               aria-expanded="false"

                            >

                                <i class="fa fa-wrench"></i>

                            </a>

                        </li>

                        <li>

                            <a class="close-link"><i class="fa fa-close"></i></a>

                        </li>

                    </ul>

                    <div class="clearfix"></div>


                </div>


                <!-- CONTENT -->
                
                <?php if (!empty($this->session->flashdata('error'))): ?>
                    <div class="alert alert-warning">
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>

                <div class="x_content">


                    <div class="clearfix"></div>

                    <br/>
                    <?php if (!empty($konfirmasi) && !empty($trans)): ?>
                    <div class="col-md-6">

                        <table class="table table-striped">
                            <tr>
                                <th colspan="2" class="text-center">Detail Transaksi</th>
                            </tr>
                            <tr>
                                <td>Transkaksi / Campaign</td>
                                <td><?php echo $trans[0]->title ?></td>
                            </tr>
                            <tr>
                                <td>Nominal Transaksi</td>
                                <td>Rp. <?php echo number_format($trans[0]->nomuniq,0,",",".") ?></td>
                            </tr>
                            <tr>
                                <td>Bank Tujuan</td>
                                <td><?php echo $trans[0]->namaBank?></td>
                            </tr>
                        </table>
                        <hr>
                        <table class="table table-striped">
                            <tr>
                                <th colspan="2" class="text-center">Konfirmasi Transaksi</th>
                            </tr>
                            <tr>
                                <td>Atas Nama</td>
                                <td><?php echo $konfirmasi[0]->atas_nama ?></td>
                            </tr>
                            <tr>
                                <td>Asal Bank</td>
                                <td><?php echo $konfirmasi[0]->namaBank ?></td>
                            </tr>
                            <tr>
                                <td>No. Rekenin Asal</td>
                                <td><?php echo $konfirmasi[0]->norek_asal ?></td>
                            </tr>
                            <tr>
                                <td>Nominal Transfer</td>
                                <td>Rp. <?php echo number_format($konfirmasi[0]->nominal,0,",",".") ?></td>
                            </tr>
                            <tr>
                                <?php $urls = "manajemendonasi/doVerifikasiProgram/";
                                    if($trans[0]->jenis == "CAMPAIGN"){
                                        $urls = "manajemendonasi/doVerifikasiCampaign/";
                                    }
                                ?>
                                <td colspan="2"><a href="<?php echo base_url($urls.$trans[0]->no_transaksi) ?>" class="btn btn-success pull-right">Approve Konfirmasi</a></td>
                            </tr>
                        </table>
                        <?php endif; ?>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>

<!-- /page content -->

{_layFooter}

