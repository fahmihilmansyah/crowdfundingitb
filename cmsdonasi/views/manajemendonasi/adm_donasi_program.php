{_layHeader}



<!-- page content -->



<div class="right_col" role="main">



    <div class="page-title">



        <div class="title_left">



            <h3>Manajemen Donasi</h3>



        </div>



    </div>





    <div class="clearfix"></div>





    <div class="row">



        <?php echo $crumbs; ?>



    </div>





    <div class="clearfix"></div>





    <div class="row">



        <div class="col-md-12 col-sm-12 col-xs-12">



            <div class="x_panel">

                <div class="x_title">

                    <h2>Data<small>List Donasi Program</small>

                    </h2>

                    <ul class="nav navbar-right panel_toolbox">

                        <li>

                            <a class="collapse-link">

                                <i class="fa fa-chevron-up"></i>

                            </a>

                        </li>

                        <li class="dropdown">



                            <a href="#"



                               class="dropdown-toggle"



                               data-toggle="dropdown"



                               role="button"



                               aria-expanded="false"



                            >



                                <i class="fa fa-wrench"></i>



                            </a>



                        </li>



                        <li>



                            <a class="close-link"><i class="fa fa-close"></i></a>



                        </li>



                    </ul>



                    <div class="clearfix"></div>





                </div>





                <!-- CONTENT -->

                <?php if (!empty($this->session->flashdata('success'))): ?>

                    <div class="alert alert-info">

                        <strong>Info!</strong> <?php echo $this->session->flashdata('success'); ?>

                    </div>

                <?php endif; ?>

                <?php if (!empty($this->session->flashdata('error'))): ?>

                    <div class="alert alert-warning">

                        <?php echo $this->session->flashdata('error'); ?>

                    </div>

                <?php endif; ?>



                <div class="x_content">





                    <div class="clearfix"></div>



                    <br/>



                    <table



                            id="manajemen_users"



                            class="table table-striped table-bordered dt-responsive nowrap"



                            width="100%"





                    >



                        <thead>



                        <tr>



                            <th>No. Transaksi</th>



                            <th>Fullname</th>



                            <th>Bank</th>

                            <th><?php echo empty($listDonasi[0]->jenistrx) ? "Campaign" : "Jenis Trx"; ?></th>



                            <th>Nominal</th>



                            <th>Status</th>



                            <th class="center">Aksi</th>



                        </tr>



                        </thead>



                        <tbody>

                        <?php foreach ($listDonasi as $row): ?>

                            <tr class="text-center">



                                <td><?php echo $row->no_transaksi; ?></td>

                                <td><?php echo $row->namaDonatur; ?></td>

                                <td><?php echo $row->namaBank; ?></td>

                                <td><?php echo empty($row->jenistrx) ? $row->title : $row->jenistrx; ?></td>

                                <td>Rp. <?php echo number_format($row->nomuniq,0,",","."); ?></td>

                                <td><?php echo $row->status; ?></td>

                                <td>


                                    <?php if (!empty($row->transaksi) && !empty($dodol)): ?>

                                        <a href="<?php echo base_url($linkurl . $row->transaksi); ?>"

                                           class="btn btn-xs btn-success">Cek</a>

                                    <?php endif; ?>
                                    <?php if ($row->status == 'unverified'): ?>
                                   <a href="javascript:void(0)"
                                       link-target="<?php echo base_url($linkurlcancel . $row->no_transaksi); ?>"

                                       onclick="aksiProses(this.id)"
		 id="<?php echo $row->no_transaksi;?>"
                                       class="btn btn-xs btn-warning cancelAlert">Cancel</a>

                                    <?php endif; ?>
                                </td>



                            </tr>

                        <?php endforeach; ?>

                        </tbody>



                    </table>



                </div>



            </div>



        </div>



    </div>



</div>



<!-- /page content -->



{_layFooter}





<script>



    $(document).ready(function () {

        $('#manajemen_users').DataTable();

    });


    function aksiProses(id){

        var url = $( "#"+id).attr("link-target");



        swal({

            title       : "Apakah Anda Yakin?",

            text        : "Akan membatalkan transaksi ?",

            type        : "warning",

            showCancelButton  : true,

            confirmButtonColor  : "#DD6B55",

            confirmButtonText : "Ya, Batalkan!",

            closeOnConfirm    : false

        }, function(isConfirm){

            if(isConfirm){

                location.href = url;

            }

        });

    }

</script>