<!DOCTYPE html>

<html lang="en">

  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Meta, title, CSS, favicons, etc. -->

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <title><?php echo $_appTitle; ?></title>



    <!-- Bootstrap -->

    <link href="<?php URI::baseURL(); ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->

    <link href="<?php URI::baseURL(); ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- NProgress -->

    <link href="<?php URI::baseURL(); ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Animate.css -->

    <link href="<?php URI::baseURL(); ?>assets/vendors/animate.css/animate.min.css" rel="stylesheet">



    <!-- Custom Theme Style -->

    <link href="<?php URI::baseURL(); ?>assets/build/css/custom.min.css" rel="stylesheet">



    <link href="<?php URI::baseURL(); ?>assets/css/ydsf.custom.css" rel="stylesheet">



    <link rel="icon"  type="image/png" href="<?php URI::baseURL(); ?>assets/images/logo/favicon.png" />



    <link href="<?php URI::baseURL(); ?>assets/vendors/pnotify/dist/pnotify.css" rel="stylesheet">

    <link href="<?php URI::baseURL(); ?>assets/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">

    <link href="<?php URI::baseURL(); ?>assets/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">



    <script src="<?php URI::baseURL(); ?>assets/js/jquery-1.11.1.min.js"></script>

    <script src="<?php URI::baseURL(); ?>assets/vendors/pnotify/dist/pnotify.js"></script>

    <script src="<?php URI::baseURL(); ?>assets/vendors/pnotify/dist/pnotify.buttons.js"></script>

    <script src="<?php URI::baseURL(); ?>assets/vendors/pnotify/dist/pnotify.nonblock.js"></script>


    <script src="<?php URI::baseURL(); ?>assets/js/jqvalidate/dist/jquery.validate.min.js"></script>

    <style type="text/css">
      .label-error
      {
        margin:0px;
        padding:0px;
        text-align: left;
        color: red
      }
    </style>

  </head>



  <body class="login">

    <div>

      <a class="hiddenanchor" id="signup"></a>

      <a class="hiddenanchor" id="signin"></a>



      <div class="login_wrapper">

        <div class="animate form login_form">

          <section class="login_content kotak">

            <form id="fmreset" action="<?php URI::baseURL(); ?>cms/reset/st_new_pwd" method="post">

              <div>

                <img src="<?php URI::baseURL(); ?>assets/images/logo/favicon.png" height="130px">

              </div>

              <h2>- Reset Password -</h2>

                <div style="display:none">

                  <?php 

                    if(!empty($this->session->flashdata('error'))){

                  ?>

                    <div id="error_msg">

                      <?php

                        echo $this->session->flashdata('error');

                      ?>

                    </div>



                    <script>

                      $(function(){

                        show($("#error_msg").html());

                      });

                    </script>

                  <?php

                    }


                    $pwd = !empty($this->session->flashdata('pwd')) ? $this->session->flashdata('pwd') : "";

                  ?>

                </div>


                <div style="display:none">

                  <?php 

                    if(!empty($this->session->flashdata('success'))){

                  ?>

                    <div id="success_msg">

                      <?php

                        echo $this->session->flashdata('success');

                      ?>

                    </div>



                    <script>

                      $(function(){

                        showSuccess($("#success_msg").html());

                      });

                    </script>

                  <?php

                    }

                  ?>

                </div>

              <div style="margin-bottom: 5px;">
                <div>
                <input type="password" 
                       class="form-control" 
                       placeholder="New Password" 
                       required="" 
                       name="pwd" 
                       id="pwd"
                       style="margin:0px;" 

                       value="<?php echo $pwd; ?>"/>
                </div>
                <p class='label-error'></p>
              </div>
              
              <div>
                <div>
                <input type="password" 
                       class="form-control" 
                       placeholder="Re-type Password" 
                       required="" 
                       id="pwd_confirm"
                       name="pwd_confirm" 
                       style="margin:0px;" 
                       value=""/>
                </div>
                <p class='label-error'></p>
              </div>
              <br/>
              <input type="hidden" name="token_pass" value="<?php echo $_token_pass; ?>">

              <input 

                  type  = "hidden" 

                  name  = "<?php echo $this->security->get_csrf_token_name(); ?>" 

                  value = "<?php echo $this->security->get_csrf_hash(); ?>"

              >

              <div style="display: block; overflow: hidden;">
                <center>
                  <input type="submit" class="btn btn-dark submit pull-right" value="Reset Password">
                </center>
              </div>



              <div class="clearfix"></div>



              <div class="separator">

                <div>

                  <h1>CMS Donasi Online</h1>

                  <p>©2017 Powered By DDTekno</p>

                </div>

              </div>

            </form>

          </section>

        </div>

      </div>

    </div>

  </body>

</html>

<script type="text/javascript">

function show(txt){

  new PNotify({

      title: 'Error Login',

      text: txt,

      type: 'error',

      delay: 1000,

      styling: 'bootstrap3'

  });

}


function showSuccess(txt){

  new PNotify({

      title: 'Sukses',

      text: txt,

      type: 'success',

      delay: 1000,

      styling: 'bootstrap3'

  });

}

var validator = $("#fmreset").validate({
      debug: true,
      errorElement: "em",
      errorContainer: $(".peringatan"),
      errorPlacement: function(error, element) {
        error.appendTo(element.parent("div").next("p"));
      },
      highlight: function(element) {   // <-- fires when element has error
          var text = $(element).parent("div").next("p");
          text.find('em').removeClass('sukses').addClass('error');
      },
      unhighlight: function(element) { // <-- fires when element is valid
          var text = $(element).parent("div").next("p");
          text.find('em').removeClass('error').addClass('sukses');
      },
      rules: {
          'pwd':{
              required: true,
              minlength : 6
          },
          'pwd_confirm':{
              equalTo   : "#pwd"
          },
      }, 
      messages: {
            'pwd'   : {
              required : "Field ini wajib diisi",
                minlength: "Masukan paling sedikit {0} karakter"
            },
            'pwd_confirm' : {
                equalTo  : "Konfirmasi password harus sama"
            }
      },
      submitHandler: function(form) {
        console.log(form.submit);
      form.submit();
            form.submit();
        }
    });

</script>

