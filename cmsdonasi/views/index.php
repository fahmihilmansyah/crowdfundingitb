{_layHeader}

	<!-- page content -->

	<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel tile fixed_height_320">
				<div class="x_title">
					<h2>Campaign</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li>
							<a class="collapse-link">
								<i class="fa fa-chevron-up"></i>
							</a>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
								<i class="fa fa-wrench"></i>
							</a>
						</li>
						<li>
							<a class="close-link">
								<i class="fa fa-close"></i>
							</a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div id="campen" style="height: 200px"></div>
				</div>
			</div>
		</div>
	</div>
<!---->
<!--	<div class="row">-->
<!--		<div class="col-md-4 col-sm-4 col-xs-12">-->
<!--			<div class="x_panel tile fixed_height_320">-->
<!--				<div class="x_title">-->
<!--					<h2>Program Zakat</h2>-->
<!--					<ul class="nav navbar-right panel_toolbox">-->
<!--						<li>-->
<!--							<a class="collapse-link">-->
<!--								<i class="fa fa-chevron-up"></i>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li class="dropdown">-->
<!--							<a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false">-->
<!--								<i class="fa fa-wrench"></i>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li>-->
<!--							<a class="close-link">-->
<!--								<i class="fa fa-close"></i>-->
<!--							</a>-->
<!--						</li>-->
<!--					</ul>-->
<!--					<div class="clearfix"></div>-->
<!--				</div>-->
<!--				<div class="x_content">-->
<!--					<div id="zakat" style="height: 200px"></div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!---->
<!--		<div class="col-md-4 col-sm-4 col-xs-12">-->
<!--			<div class="x_panel tile fixed_height_320">-->
<!--				<div class="x_title">-->
<!--					<h2>Program Infaq</h2>-->
<!--					<ul class="nav navbar-right panel_toolbox">-->
<!--						<li>-->
<!--							<a class="collapse-link">-->
<!--								<i class="fa fa-chevron-up"></i>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li class="dropdown">-->
<!--							<a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false">-->
<!--								<i class="fa fa-wrench"></i>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li>-->
<!--							<a class="close-link">-->
<!--								<i class="fa fa-close"></i>-->
<!--							</a>-->
<!--						</li>-->
<!--					</ul>-->
<!--					<div class="clearfix"></div>-->
<!--				</div>-->
<!--				<div class="x_content">-->
<!--					<div id="infaq" style="height: 200px"></div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--		<div class="col-md-4 col-sm-4 col-xs-12">-->
<!--			<div class="x_panel tile fixed_height_320">-->
<!--				<div class="x_title">-->
<!--					<h2>Program Shadaqah</h2>-->
<!--					<ul class="nav navbar-right panel_toolbox">-->
<!--						<li>-->
<!--							<a class="collapse-link">-->
<!--								<i class="fa fa-chevron-up"></i>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li class="dropdown">-->
<!--							<a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false">-->
<!--								<i class="fa fa-wrench"></i>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li>-->
<!--							<a class="close-link">-->
<!--								<i class="fa fa-close"></i>-->
<!--							</a>-->
<!--						</li>-->
<!--					</ul>-->
<!--					<div class="clearfix"></div>-->
<!--				</div>-->
<!--				<div class="x_content">-->
<!--					<div id="shadaqah" style="height: 200px"></div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!---->
<!--	</div>-->

	<div id="modal_x" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
	   
	</div>

	<!-- /page content -->

{_layFooter}

<script type="text/javascript">
	$(function(){
		$.getJSON("<?php echo base_url(); ?>cms/grafik/program?t=shadaqah", function(result){
			var dd_href = "<?php echo base_url(); ?>cms/grafik/program/pd?t=shadaqah&";
			callCharts("#shadaqah", result, dd_href);
		});

		$.getJSON("<?php echo base_url(); ?>cms/grafik/campaign", function(result){
			var dd_href = "<?php echo base_url(); ?>cms/grafik/campaign/pd?";
			callCharts("#campen", result, dd_href);
		});

		$.getJSON("<?php echo base_url(); ?>cms/grafik/program?t=zakatmaal-zakatprofesi", function(result){
			var dd_href = "<?php echo base_url(); ?>cms/grafik/program/pd?t=zakatmaal-zakatprofesi&";
			callCharts("#zakat", result, dd_href);
		});
		$.getJSON("<?php echo base_url(); ?>cms/grafik/program?t=infaq", function(result){
			var dd_href = "<?php echo base_url(); ?>cms/grafik/program/pd?t=infaq&";
			callCharts("#infaq", result, dd_href);
		});
	});

	function callCharts(id, result, dd_href){
	    $(id).highcharts({
	        chart: {
	            type: result.type
	        },
	         title: {
		        text: result.year
		    },

		    yAxis: {
		        title: {
		            text: 'Rp.'
		        }
		    },
		    legend: {
		        layout: 'vertical',
		        align: 'right',
		        verticalAlign: 'middle'
		    },
		    xAxis: {
		        type: 'category'
		    },
		    plotOptions: {
		        series: {
		            cursor: 'pointer',
		            point: {
		                events: {
		                    click: function () {
		                    	var data = {
		                    		category: this.category,
		                    		y: this.y
		                    	}

		                    	if(typeof dd_href === 'undefined')
		                    		return false;

		                    	showDialog(dd_href + 'c=' + (this.category + 1) + "&y=" + result.year, data);
		                    }
		                }
		            }
		        }
		    },
		    series: [{
		        name: 'summary',
		        data: result.datac
		    },{
		        name: 'donatur',
		        data: result.datad
		    }],
	    });
	}

	function showDialog(dd_href, param)
	{
		$("#modal_x").html('').modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true,
        }).load(dd_href, function() {

        });
	}
</script>

