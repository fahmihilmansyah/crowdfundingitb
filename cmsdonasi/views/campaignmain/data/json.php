<?php

$results['sEcho'] 			= $sEcho;
$results['iTotalRecords'] 	= $results['iTotalDisplayRecords'] = $iTotalRecords;

if(count($apps))
{
	
	$i=0;
	$no = 1;

	foreach($apps as $data)
	{
		$_key   = $this->session->userdata("_tokenaction");

		$aksi  	= '';
		$aksi  .= "<center><div class='btn-group'>";


		// EDIT BUTTON
		if($akses->getAccess("edit"))
		{
			$param  = "id=" . $data["id"] . ";";
			$param .= "token=" . $_key["editform"]["token"];
			$url    = base_url() . 'cms/campaignmain/save?param=' . ENKRIP::encode($param);
			$aksi  .= "<a href='" . $url . "' title='Set Campaign' class='btn btn-success btn-xs'><i class='fa fa-circle-o-notch'></i></a>";
		}

		

		$aksi .= "</div></center>";

		$crtd_at     = ARR::dFormat($data['crtd_at'], "d M Y");
		$edtd_at 	 = ARR::dFormat($data['edtd_at'], "d M Y");
		$class   	 = ($data['valid_status']) == "1" ? 'label label-default' : 'label label-success';
		$text    	 = ($data['valid_status'] == "1") ? 'on progress' : 'closed';

		$results['aaData'][$i] = array
			(
				ARR::cText($no),
				ucwords(strtolower($data['title'])),
				!empty($data['last_edit']) ? ARR::cText($data['last_edit']) : '<center>-</center>',
	            "<center><label class='$class'>$text</label></center>",
				$aksi,
			);	

		++$i;
		++$no;
	}
} else {
	for($i=0;$i<8;++$i) {
		$results['aaData'] = [];
	}
}

print($callback . '(' . json_encode($results) . ')');

