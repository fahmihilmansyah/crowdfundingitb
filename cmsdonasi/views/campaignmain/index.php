{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Main Campaign [ View ]</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							View
							<small>Data Campaign Main</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">
					<?php 
					$_key   = $this->session->userdata("_tokenaction");
					$param  = "id=" . 1;
					$param .= ";token=" . $_key["editform"]["token"];
					$url    = base_url() . 'cms/campaignmain/update?param=' . ENKRIP::encode($param);
					?>
					<a href="<?php echo $url ?>" title="Edit" class="btn btn-info pull-right"><i class="fa fa-flag"></i>&nbsp;Tentukan Campaign Utama</a>
						<table class="table table-striped table-bordered" >
							<tr>
								<td width="20%">Gambar</td>
								<td width="1%">:</td>
								<td>
									<?php 
										$img = !empty($default['img']) ? $default['img'] : 'noimage.png'; 
									?>
									<img src="<?php URI::baseURL(); ?>assets/images/campaign/<?php echo $img ?>"
										 height="200"/>
								</td>
							</tr>
							<tr>
								<td width="20%">Judul</td>
								<td width="1%">:</td>
								<td><?php echo !empty($default['title']) ? $default['title'] : '-'; ?></td>
							</tr>
							<tr>
								<td width="20%">Deskripsi</td>
								<td width="1%">:</td>
								<td><?php echo !empty($default['desc_short']) ? $default['desc_short'] : '-'; ?></td>
							</tr>
							<tr>
								<td width="20%">Target</td>
								<td width="1%">:</td>
								<td>
									<b>
									Rp. <?php echo !empty($default['target']) ? number_format($default['target'],2,",",".") : '-'; ?>
									</b>
								</td>
							</tr>
							<tr>
								<td width="20%">Tanggal Valid</td>
								<td width="1%">:</td>
								<td>
									<b>
									<?php 
										echo !empty($default['valid_date']) ? date('d M Y',strtotime($default['valid_date'])) : '-'; 
									?>
									</b>
								</td>
							</tr>
							<tr>
								<td width="20%">Pembuat</td>
								<td width="1%">:</td>
								<td><?php echo !empty($default['author']) ? $default['author'] : '-'; ?></td>
							</tr>
							<tr>
								<td width="20%">Update Terakhir</td>
								<td width="1%">:</td>
								<td><?php echo !empty($default['last_edit']) ? $default['last_edit'] : '-'; ?></td>
							</tr>
						</table>

                      	<div class="ln_solid"></div>

						<!--<a href="<?php URI::baseURL(); ?>cms/campaignmain" class="btn btn-primary">
							<i class="fa fa-reply"></i> Kembali
						</a>-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}
<script type="text/javascript">
	$('.tree').treegrid({
	    expanderExpandedClass	: 'glyphicon glyphicon-minus',
	    expanderCollapsedClass	: 'glyphicon glyphicon-plus'
	});
</script>
