{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Campaign Utama</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							View
							<small>Data Main Campaign</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">
						<table 
								id="artikel_kategori" 
								class="table table-striped table-bordered dt-responsive nowrap" 
								width="100%" 
								data-source="<?php echo $dsource?>"
						>
						<thead>
							<tr>
								<th>No. </th>
								<th>Campaign Title</th>
								<th>Last Edit</th>
								<th class="center">Status Campaign</th>
								<th class="center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr class="text-center">
								<td colspan="7"><i class="entypo-arrows-ccw"></i> Mengambil data...</td>
							</tr>
						</tbody>
						</table>

                      	<div class="ln_solid"></div>

						<a href="<?php URI::baseURL(); ?>cms/campaignmain" class="btn btn-primary">
							<i class="fa fa-reply"></i> Kembali
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}
<script type="text/javascript">
	$('.tree').treegrid({
	    expanderExpandedClass	: 'glyphicon glyphicon-minus',
	    expanderCollapsedClass	: 'glyphicon glyphicon-plus'
	});
</script>

<script type="text/javascript">
	var app;
	var ds;

	$(document).ready(function() {
		ds  = $('#artikel_kategori').attr('data-source');
		app = $('#artikel_kategori')
				.on('preXhr.dt', function (e, settings, data) {
       				cekTokenValid("<?php URI::baseURL(); ?>cms/cauth_dt");
    			}).DataTable({
		        "aoColumns": [
			        { "width": "5%"},
			        null,
			        { "width": "10%"},
			        { "width": "15%"},
			        { "width": "15%", "sClass": "alignCenter", "bSortable": false }
		        ],
		        "order": [[ 0, "desc" ]],
		        "bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": ds,
			    "oLanguage": {
			        "sProcessing": "<img src='../../../assets/images/loading.gif' height='50px'/>"
			    },
		        "fnServerData": function( sUrl, aoData, fnCallback, oSettings ) {
					oSettings.jqXHR = $.ajax({
						"url": sUrl,
						"data": aoData,
						"success": fnCallback,
						"dataType": "jsonp",
						"cache": false
					});
		        },
		      });
	});
</script>