
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.button {
			display: block;
			text-decoration: none;
		    background-color: #4CAF50; /* Green */
		    border: none;
		    color: white;
		    padding: 10px 22px;
		    text-align: center;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 16px;
		    border-radius: 5px;
		}
	</style>
</head>
<body>
	<h5>
		Assalamu'alaikum Wr. Wb.
	</h5>
	<p>Kami menerima permintaan untuk mengganti password akun anda, Jika kamu tidak merasa melakukan permintaan ini, abaikan pesan ini dan kontak administrator tertinggi kamu.</p>

	<p>jika kamu ingin mereset password kamu silahkan ikuti link dibawah ini. Link ini akan kadaluarsa dalam 2 jam.</p>

	<a class="button" href="<?php echo base_url(); ?>cms/reset/pg_new_pwd?token_pass=<?php echo $token; ?>">Klik Disini</a>

	<p>
	Best Regards,
	<br/>
	MumuApps.
	</p>
</body>
</html>