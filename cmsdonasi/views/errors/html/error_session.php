<?php
	// delete nb_action_permit
	$this->db->delete('nb_action_permit', array('user_id' => $this->session->userdata('usr_id')));

	// delete session action
	$this->session->sess_destroy();
?>

Anda telah menonaktifkan role/pengguna yang sedang digunakan. silahkan login menggunakan role/pengguna lain yang statusnya aktif.

<script type="text/javascript">
    window.setTimeout(function() {
        window.location.href='<?php URI::baseURL(); ?>cms';
    }, 5000);
</script>