<?php
	// delete session action
	$this->session->sess_destroy();
?>

Invalid Token, Please Contact Your Administration

<script type="text/javascript">
    window.setTimeout(function() {
        window.location.href='<?php URI::baseURL(); ?>cms';
    }, 5000);
</script>