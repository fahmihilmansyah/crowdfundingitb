<?php
	// delete session action
	$this->session->sess_destroy();
?>

Token yang anda miliki telah melebihi batas waktu penggunaan. <br/> mohon maaf, anda akan dilogout otomatis dari sistem.

<script type="text/javascript">
    window.setTimeout(function() {
        window.location.href='<?php URI::baseURL(); ?>cms';
    }, 5000);
</script>