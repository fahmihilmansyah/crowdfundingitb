<?php
	// delete nb_action_permit
	$this->db->delete('nb_action_permit', array('user_id' => $this->session->userdata('usr_id')));

	// delete session action
	$this->session->sess_destroy();
?>

maaf anda tidak memiliki hak untuk mengakses situs ini. Anda akan diredirectkan ke halaman login. terimasih.

<script type="text/javascript">
    window.setTimeout(function() {
        window.location.href='<?php URI::baseURL(); ?>cms';
    }, 5000);
</script>