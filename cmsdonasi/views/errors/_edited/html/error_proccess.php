<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:700" rel="stylesheet">
<title>Error Proses</title>

<style type="text/css">
html,body{
	margin:0px;
	padding:0px;
	font-family: 'Source Sans Pro', sans-serif;
	overflow:hidden;
	background: #efefef;
}

.container-fluid {
	padding-bottom: 20px;
	padding-left: 15%;
	padding-right: 15%;
    margin: auto;
}

.grey {
    background: #efefef;
}

.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}

.invoice {
    overflow: hidden;
    display: block;
    background: #fff;
    -webkit-box-shadow: 6px 6px 13px -8px rgba(0,0,0,0.75);
    -moz-box-shadow: 6px 6px 13px -8px rgba(0,0,0,0.75);
    box-shadow: 6px 6px 13px -8px rgba(0,0,0,0.75);
    margin-bottom: 88px;
    padding-top: 20px;
    border-radius: 20px;
    width: 80%;
}

.btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
	padding-left: 50px;
    padding-right: 50px;
}

.btn {
    display: inline-block;
    padding: 6px 15px;
    margin-bottom: 0;
    font-size: 20px;
	font-size: 2vw;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
	max-width: 80%;
}

.btn-blue {
    background: #0255a5;
    color: #fff;
    margin-top: 10px;
    padding-left: 50px;
    padding-right: 50px;
}

a {
    color: #337ab7;
    text-decoration: none;
	max-width:70%
}

</style>
</head>
<body>
<div class="container-fluid grey">

			<div class="don-head">

				<div class="container">

		<!-- start create campaign main -->
			<center>
			<img src="<?php echo url(); ?>/ydsf/assets/images/YDSF.png" style="width:20%; padding-top:10px; padding-bottom:10px">
			<div class="invoice">
			
				<center style="padding-bottom:20px">
					<span style="font-size: 170%; font-size: 3vw; width:100%;">SORRY !</span></br>
					<span style="font-size: 500%; font-size: 4.5vw; color:#0255a5">KESALAHAN PROSES</span></br>
					<img src="<?php echo url(); ?>/ydsf/assets/images/errorimg.png" style="width:40%;"></br>
					
					<!--<a class="btn btn-success">KEMBALI KE HALAMAN UTAMA</a></br>
					<a class="btn btn-blue">LAPORKAN</a>-->
				</center>
				
			</div>
			</center>
		</div>

			</div>
	</div>
</body>
</html>

<?php
    function url(){
        if(isset($_SERVER['HTTPS'])){
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }
        else{
            $protocol = 'http';
        }
        return $protocol . "://" . $_SERVER['HTTP_HOST'];
    }
?>