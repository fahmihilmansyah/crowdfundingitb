<?php

$results['sEcho'] 			= $sEcho;
$results['iTotalRecords'] 	= $results['iTotalDisplayRecords'] = $iTotalRecords;

if(count($apps))
{
	
	$i=0;
	$no = 1;

	foreach($apps as $data)
	{
		$_key   = $this->session->userdata("_tokenaction");

		$aksi  	= '';
		$aksi  .= "<center><div class='btn-group'>";

		// VIEW BUTTON
		$param  = "id=" . $data["id"] . ";";
		$url    = base_url() . 'cms/faq_kategori/view?param=' . ENKRIP::encode($param);
		$aksi  .= "<a href='" . $url . "' class='btn btn-dark btn-xs'><i class='fa fa-eye'></i></a>";

		// ACTIVATE BUTTON
		/*$btn 	= ($data["is_active"]) ? 'btn-danger' : 'btn-success';
		$value 	= ($data["is_active"]) ? 0 : 1; 
		if($akses->getAccess("edit"))
		{
			$param  = "id=" . $data["id"] . ";status=" . $value . ";";
			$param .= "token=" . $_key["activate"]["token"];
			$url    = base_url() . 'cms/faq_kategori/activate?param=' . ENKRIP::encode($param);
			$aksi  .= "<a id='activate-" . $data["id"] . "' 
						 href='javascript:void(0)' 
						 onclick='activateRow(this.id)' 
						 class='btn {$btn} btn-xs'
						 link-target='" . $url . "'
						 status='" . $data['is_active'] . "'
					  >
							<i class='fa fa-power-off'></i>
					  </a>";
		}*/

		// EDIT BUTTON
		if($akses->getAccess("edit"))
		{
			$param  = "id=" . $data["id"] . ";";
			$param .= "token=" . $_key["editform"]["token"];
			$url    = base_url() . 'cms/faq_kategori/update?param=' . ENKRIP::encode($param);
			$aksi  .= "<a href='" . $url . "' class='btn btn-success btn-xs'><i class='fa fa-edit'></i></a>";
		}

		if($akses->getAccess("delete"))
		{
			// DELETE BUTTON
			$param  = "id=" . $data["id"] . ";";
			$param .= "token=" . $_key["delete"]["token"];
			$url    = base_url() . 'cms/faq_kategori/remove?param=' . ENKRIP::encode($param);
			$aksi  .= "<a id='delete-" . $data["id"] . "' 
						 href='javascript:void(0)' 
						 onclick='deleteRow(this.id)' 
						 class='btn btn-danger btn-xs'
						 link-target='" . $url . "'
					  >
							<i class='fa fa-trash'></i>
					  </a>";
		}

		$aksi .= "</div></center>";

		$crtd_at     = ARR::dFormat($data['crtd_at'], "d M Y");
		$edtd_at 	 = ARR::dFormat($data['edtd_at'], "d M Y");
		//$class   	 = ($data['is_active']) ? 'label label-success' : 'label label-default';
		//$text    	 = ($data['is_active']) ? 'aktif' : 'tidak aktif';

		$results['aaData'][$i] = array
			(
				ARR::cText($no),
				$data['title'],
				$data['author'],
				!empty($data['last_edit']) ? $data['last_edit'] : '<center>-</center>',
	            //"<center><label class='$class'>$text</label></center>",
				$aksi,
			);	

		++$i;
		++$no;
	}
} else {
	for($i=0;$i<7;++$i) {
		$results['aaData'] = [];
	}
}

print($callback . '(' . json_encode($results) . ')');

