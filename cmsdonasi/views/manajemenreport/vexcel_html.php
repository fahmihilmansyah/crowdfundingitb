<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $title; ?></title>
</head>
<body>
<table border="1" align="center">
    <tr>
        <td colspan="6"><?php echo $title; ?></td>
    </tr>
    <tr>
        <td>NO TRANSAKSI</td>
        <td>TGL TRANSAKSI</td>
        <td>TRANSAKSI</td>
        <td>METHOD TRANSAKSI</td>
        <td>NOMINAL</td>
        <td>UNIQUE CODE</td>
    </tr>
    <?php foreach($query as $row): ?>
        <tr>
            <td><?php echo $row->no_transaksi; ?></td>
            <td><?php echo $row->trx_date; ?></td>
            <td><?php echo $row->title; ?></td>
            <td><?php echo $row->namaBank; ?></td>
            <td><?php echo $row->nominal; ?></td>
            <td><?php echo $row->uniqcode; ?></td>
        </tr>
    <?php endforeach; ?>
</table>
</body>
</html>