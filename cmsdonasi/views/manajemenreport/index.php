{_layHeader}


<!-- page content -->


<div class="right_col" role="main">


    <div class="page-title">


        <div class="title_left">


            <h3>Manajemen Report</h3>


        </div>


    </div>


    <div class="clearfix"></div>


    <div class="row">


        <?php echo $crumbs; ?>


    </div>


    <div class="clearfix"></div>


    <div class="row">


        <div class="col-md-12 col-sm-12 col-xs-12">


            <div class="x_panel">


                <div class="x_title">


                    <h2>


                        Data


                        <small>Report</small>


                    </h2>


                    <ul class="nav navbar-right panel_toolbox">


                        <li>


                            <a class="collapse-link">


                                <i class="fa fa-chevron-up"></i>


                            </a>


                        </li>


                        <li class="dropdown">


                            <a href="#"


                               class="dropdown-toggle"


                               data-toggle="dropdown"


                               role="button"


                               aria-expanded="false"


                            >


                                <i class="fa fa-wrench"></i>


                            </a>


                        </li>


                        <li>


                            <a class="close-link"><i class="fa fa-close"></i></a>


                        </li>


                    </ul>


                    <div class="clearfix"></div>


                </div>


                <!-- CONTENT -->


                <div class="x_content">


                    <div class="clearfix"></div>


                    <br/>

                    <?php

                    $action = "";

                    $method = array(

                        "method" => "post",

                    );

                    echo form_open($action, $method);

                    ?>

                    <div class="row">

                        <!--<div class="col-md-3">

                            <select name="format" class="form-control">

                                <option value="">-- FORMAT --</option>

                                <option value="excel">EXCEL</option>

                                <option value="pdf">PDF</option>

                            </select>

                        </div>-->

                        <div class="col-md-3">
                            <?php $jprog = $this->db->query('select * from nb_trans_program_kategori')->result_array(); ?>
                            <select name="jenis" id="jenis" class="form-control">

                                <option value="">-- JENIS PROGRAM --</option>
                                <?php foreach ($jprog as $r): ?>
                                <option value="<?php echo $r['kategoricode']?>"><?php echo $r['kategoriname']?></option>
                                <?php endforeach; ?>
                            </select>

                        </div>
                        <div class="col-md-3">

                            <select name="jeniscampaign" id="jeniscampaign" class="form-control">

                                <option value="">-- JENIS CAMPAIGN--</option>
                                <option value="all">ALL</option>
                                <?php foreach ($campaign as $xy): ?>
                                    <option value="<?php echo $xy->id ?>"><?php echo strtoupper($xy->title) ?></option>
                                <?php endforeach; ?>

                            </select>

                        </div>

                        <div class="col-md-3">

                            <input type="text" name="tgl_awal" value="" class="form-control has-feedback-left"

                                   required="required" id="tgl_awal">

                        </div>

                        <div class="col-md-3">

                            <input type="text" name="tgl_akhir" value="" class="form-control has-feedback-left"

                                   required="required" id="tgl_akhir">

                        </div>

                    </div>
                    <br>

                    <div class="row">

                        <div class="pull-right">

                            <button type="submit" class="btn btn-primary">Submit</button>

                            <?php if (!empty($query)): ?>

                                <label id="exportxls" class="btn btn-success">Export XLS</label>

                            <?php endif; ?>

                        </div>

                    </div>
                    <br>

                    <?php echo form_close();

                    if (!empty($query)):

                        ?>


                        <div class="row" id="tbltrx">

                            <table id="myTable" class="table table-stripped" align="center">

                                <thead>

                                <tr>

                                    <td colspan="6" align="center"><b><?php echo $title; ?></b></td>

                                </tr>
                                <tr>

                                    <td colspan="6" align="center"><b><?php echo $jenis; ?></b></td>

                                </tr>

                                <tr>

                                    <td align="center"><b>NO TRANSAKSI</b></td>

                                    <td align="center"><b>TGL TRANSAKSI</b></td>

                                    <td align="center"><b>TRANSAKSI</b></td>

                                    <td align="center"><b>METHOD TRANSAKSI</b></td>

                                    <td align="center"><b>NOMINAL</b></td>

                                    <td align="center"><b>UNIQUE CODE</b></td>
                                    <td align="center"><b>Nama</b></td>
                                    <td align="center"><b>Email</b></td>
                                    <td align="center"><b>Telp</b></td>
                                    <td align="center"><b>Komentar</b></td>

                                </tr>

                                </thead>

                                <tbody>

                                <?php 
                                $nom = 0;
                                $nomuniq = 0;
                                foreach ($query as $row): 
                                    $nom += $row->nominal;
                                    $nomuniq += $row->uniqcode;
                                    ?>

                                    <tr>

                                        <td align="center"><?php echo $row->no_transaksi; ?></td>

                                        <td align="center"><?php echo date("d M Y", strtotime($row->trx_date)); ?></td>

                                        <td align="center"><?php echo strtoupper(empty($row->title) ? $row->jenistrx : $row->title); ?></td>

                                        <td align="center"><?php echo $row->bank; ?></td>

                                        <td align="center"><?php echo number_format($row->nominal,0,".",","); ?></td>

                                        <td align="center"><?php echo number_format($row->uniqcode,0,".",","); ?></td>
                                        <td align="center"><?php echo ($row->namaDonatur); ?></td>
                                        <td align="center"><?php echo ($row->emailDonatur); ?></td>
                                        <td align="center"><?php echo ($row->telpDonatur); ?></td>
                                        <td align="center"><?php echo ($row->comment); ?></td>

                                    </tr>

                                <?php endforeach; ?>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td align="center" colspan="4"><b>TOTAL</b></td>
                                <td align="center"><b><?php echo number_format($nom,0,".",",");?></b></td><td align="center"><b><?php echo number_format($nomuniq,0,".",",");?></b></td>
                                <td colspan="4">&nbsp;</td>
                                </tr>
                                </tfoot>

                            </table>

                        </div>


                    <?php else:;

                        if ($_POST):

                            ?>

                            <table class="table table-stripped" align="center">

                                <thead>

                                <tr>

                                    <td align="center">NO TRANSAKSI</td>

                                    <td align="center">TGL TRANSAKSI</td>

                                    <td align="center">TRANSAKSI</td>

                                    <td align="center">METHOD TRANSAKSI</td>

                                    <td align="center">NOMINAL</td>

                                    <td align="center">UNIQUE CODE</td>

                                </tr>

                                </thead>

                                <tbody>

                                <tr>
                                    <td colspan="6" align="center">TIDAK ADA DATA</td>
                                </tr>

                                </tbody>

                            </table>

                        <?php endif; endif; ?>

                    <div id="editor"></div>

                </div>


            </div>


        </div>


    </div>


</div>


<!-- /page content -->


{_layFooter}

<script src="<?php echo base_url('assets/js/jspdf/jspdf.min.js') ?>"></script>

<script>

    $(document).ready(function () {

        select2_icon("jenis");
        select2_icon("jeniscampaign");
        $('#myTable').DataTable();

        $('#tgl_awal, #tgl_akhir').daterangepicker({

            singleDatePicker: true,

            singleClasses: "picker_3"

        }, function (start, end, label) {

            console.log(start.toISOString(), end.toISOString(), label);

        });
        jQuery(document).on('change',"#jenis",function () {
            if($(this).val() == ""){
                $("#jeniscampaign").attr('disabled',false);
            }else{
                $("#jeniscampaign").attr('disabled',true);
            }
        });
        jQuery(document).on('change',"#jeniscampaign",function () {
            if($(this).val() == ""){
                $("#jenis").attr('disabled',false);
            }else{
                $("#jenis").attr('disabled',true);
            }
        });
        jQuery(document).on('submit', function (e) {

            if ($("#jenis").val() == "" && $("#jeniscampaign").val() == "") {

                alert("Silahkan Pilih Jenis!");

                e.preventDefault();

            }

        })

        jQuery(document).on('click', "#exportxls", function (e) {

            /*window.open('data:application/vnd.ms-excel,'+encodeURIComponent( $("#tbltrx").html() ));

             e.preventDefault();*/
            var a = document.createElement('a');
            //getting data from our div that contains the HTML table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('tbltrx');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');
            a.href = data_type + ', ' + table_html;
            //setting the file name
            a.download = 'download.xls';
            //triggering the function
            a.click();
            //just in case, prevent default behaviour
            e.preventDefault();


        });


    });

</script>

