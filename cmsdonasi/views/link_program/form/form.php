{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Link Footer [ New ]</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Form
							<small>Input Link Footer</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">

						<!-- start form for validation -->
						<h5 class="alert alert-danger alert-dismissible fade in peringatan">
							Terdapat kesalahan dalam pengisian form. Silahkan cek kembali data form anda!
						</h5>
					    <?php
						    $hidden_form = array('id' => !empty($id) ? $id : '');
						    echo form_open_multipart($action, array('id' => 'fmlink_program'), $hidden_form);
					    ?>
								<div>
									<label for="name">Name <span class="require">*</span> :</label>
									<div>
		                			<?php 
		                				$name  = 'data[name]';
		                				$value = !empty($default["name"]) ? $default["name"] : '';
		                				$extra = 'id="name" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>
								<div>
									<label for="message">Page Type <span class="require">*</span> :</label>
									<div>
	                				<?php 
										$name         = "data[type]";
										$value        = !empty($default["type"]) ? $default["type"] : '';
										$extra        = 'id="type" style="width:100%"';
										$categories   = array("" => "Pilihan", "static" => "Statis", "other" => "Lainnya");

	                					echo form_dropdown($name, $categories, $value, $extra); 
	                				?>
		                			</div>
		                			<p class='label-error'></p>
								</div>
								<br/>

								<div>
									<label for="name">Link <span class="require">*</span> :</label>
									<div>
		                			<?php 
		                				$name  = 'data[link]';
		                				$value = !empty($default["link"]) ? $default["link"] : '';
		                				$extra = 'id="link" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				$value = explode("/", $value);

		                				$new_array = array();
		                				foreach ($value as $row) {
		                					if($row != "page")
		                					{
		                						$new_array[] = $row;
		                					}
		                				}

		                				$value = implode("/", $new_array);
		                				
		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label class="agreement">
										<div>
										<?php
											$data = array(
											        'name'          => 'agreement',
											        'id'            => 'agreement',
											        'value'         => 'accept',
											        'checked'       => FALSE,
											);

											echo form_checkbox($data);
										?>
										Saya yakin dan bertanggung jawab atas penambahan data ini
										</div>
		                				<p class='label-error'></p>
									</label>
								</div>

								<div class="actionBar">
									<a href="<?php URI::baseURL(); ?>cms/link_program" class="btn btn-primary">
										<i class="fa fa-reply"></i> Kembali
									</a>
									<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>
								</div>
					    <?php
					    	echo form_close();
					    ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}

<script type="text/javascript">

	select2_icon("type");

	$(document).ready(function() {
		$('.tree').treegrid({
		    expanderExpandedClass	: 'glyphicon glyphicon-minus',
		    expanderCollapsedClass	: 'glyphicon glyphicon-plus'
		});

		var validator = $("#fmlink_program").validate({
			debug: true,
			errorElement: "em",
			errorContainer: $(".peringatan"),
			errorPlacement: function(error, element) {
				error.appendTo(element.parent("div").next("p"));
			},
			highlight: function(element) {   // <-- fires when element has error
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('sukses').addClass('error');
			},
			unhighlight: function(element) { // <-- fires when element is valid
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('error').addClass('sukses');
			},
			rules: {
				'data[name]': {
					required: true,
				},
				'data[type]': {
					required: true,
				},
				'data[link]': {
					required: true,
				},
				'agreement':{
					required: true
				}
			}, 
			messages: {
		        'data[name]' : "Field ini wajib disi",
		        'data[type]' : "Field ini wajib disi",
		        'data[link]' : "Field ini wajib disi",
		        'agreement'  : "Anda harus menyetujui pernyataan terlebih dahulu!"
		    },
			submitHandler: function(form) {
		        form.submit();
		    }
		});

		$("#agreement").click(function(){
			
		});
	});
</script>