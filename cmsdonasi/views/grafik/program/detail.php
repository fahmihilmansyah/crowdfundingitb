

<div class="modal-dialog modal-lg">

<div class="modal-content">



	<div class="modal-header">

		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>

		</button>

		<h4 class="modal-title" id="myModalLabel">Grafik Per Hari</h4>

	</div>

	<div class="modal-body">
		<div style="padding: 10px; display: block; overflow: auto;">
			<div id="grafik_harian_program" style="height: 400px; width:800px; display: block;"></div>
		</div>
	</div>

	<div class="modal-footer">

		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

	</div>



</div>

</div>

<script type="text/javascript">

	

	$(function(){

		$.getJSON("<?php echo base_url(); ?>cms/grafik/program/dd?t=<?php echo $t; ?>&c=<?php echo $c; ?>&y=<?php echo $y; ?>", function(result){

			// var dd_href = "<?php echo base_url(); ?>cms/grafik/program/dd?c=<?php echo $c; ?>&y=<?php echo $y; ?>";

			callCharts("#grafik_harian_program", result);

		});

	});

</script>