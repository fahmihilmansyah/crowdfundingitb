{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Campaign [ New ]</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Form
							<small>Input Campaign</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">

						<!-- start form for validation -->
						<h5 class="alert alert-danger alert-dismissible fade in peringatan">
							Terdapat kesalahan dalam pengisian form. Silahkan cek kembali data form anda!
						</h5>
						<br/>
					    <?php
						    $hidden_form = array('id' => !empty($id) ? $id : '');
						    echo form_open_multipart($action, array('id' => 'fmCampaign'), $hidden_form);
					    ?>
						<div class="" role="tabpanel" data-example-id="togglable-tabs">
							<!-- HEAD TAB PANEL -->
							<ul id="CampaignTab"  class="nav nav-tabs bar_tabs" role="tablist">
								<li role="presentation" class="active">
									<a 
										href="#display" 
										id="display-tab" 
										role="tab" 
										data-toggle="tab" 
										aria-expanded="true">
										Display
									</a>
								</li>
								<li role="presentation" class="">
									<a 
										href="#konten" 
										role="tab" 
										id="konten-tab" 
										data-toggle="tab" 
										aria-expanded="false">
										Konten
									</a>
								</li>
								<li role="presentation" class="">
									<a 
										href="#komponencampaign" 
										role="tab" 
										id="komponencampaign-tab" 
										data-toggle="tab" 
										aria-expanded="false">
										Komponen Campaign
									</a>
								</li>
								<li role="presentation" class="">
									<a 
										href="#attachment" 
										role="tab" 
										id="attachment-tab" 
										data-toggle="tab" 
										aria-expanded="false">
										Gambar Campaign
									</a>
								</li>
								<li role="presentation" class="">
									<a 
										href="#metatag" 
										role="tab" 
										id="metatag-tab" 
										data-toggle="tab" 
										aria-expanded="false">
										Meta Tag
									</a>
								</li>
								<li role="presentation" class="">
									<a 
										href="#updateprogress" 
										role="tab" 
										id="updateprogress-tab" 
										data-toggle="tab" 
										aria-expanded="false">
										Update Progress
									</a>
								</li>
							</ul>

							<!-- BODY TAB PANEL -->
							<div id="CampaignTabContent" class="tab-content" style="min-height:300px">

								<!-- DISPLAY TAB -->
								<div 
									role="tabpanel" 
									class="tab-pane fade active in" 
									id="display" 
									aria-labelledby="home-tab">
										<br clear="all"/>
										<div>
											<label for="name">Title <span class="require">*</span> :</label>
											<div>
				                			<?php 
				                				$name  = 'data[title]';
				                				$value = !empty($default["title"]) ? $default["title"] : '';
				                				$extra = 'id="title" 
				                						  class="form-control" 
				                						  placeholder="isi disini"';

				                				echo form_input($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>

										<div>
											<label for="message">Kategori <span class="require">*</span> :</label>
											<div>
			                				<?php 
												$name         = "data[categories]";
												$value        = !empty($default["categories"]) ? $default["categories"] : '';
												$extra        = 'id="categories" style="width:100%"';

			                					echo form_dropdown($name, $categories, $value, $extra); 
			                				?>
				                			</div>
				                			<p class='label-error'></p>
										</div>

										<!-- <div>
											<label for="message">Type <span class="require">*</span> :</label>
											<div> -->
			                				<?php 
												// $name         = "data[type]";
												// $value        = !empty($default["type"]) ? $default["type"] : '';
												// $extra        = 'id="type" style="width:100%"';

			         //        					echo form_dropdown($name, $type, $value, $extra); 
			                				?>
				                			<!-- </div>
				                			<p class='label-error'></p>
										</div> -->

										<div>
											<label for="message">Konten Singkat <span class="require">*</span> :</label>
											<div>
				                			<?php 
				                				$name  = 'data[desc_short]';
				                				$value = !empty($default["desc_short"]) ? $default["desc_short"] : '';
				                				$extra = 'id="desc_short" 
				                						  class="form-control mini-height" 
				                						  placeholder="isi disini"';

				                				echo form_textarea($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>

										<div>
											<label for="name">Link Terkait :</label>
											<div>
				                			<?php 
				                				$name  = 'data[link_terkait]';
				                				$value = !empty($default["link_terkait"]) ? $default["link_terkait"] : '';
				                				$extra = 'id="link_terkait" 
				                						  class="form-control" 
				                						  placeholder="isi disini"';

				                				echo form_input($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>
								</div>

								<!-- KONTEN TAB -->
								<div 
									role="tabpanel" 
									class="tab-pane fade" 
									id="konten" 
									aria-labelledby="konten-tab">

										<div>
											<label for="message">Konten:</label>
											<div>
				                			<?php 
				                				$name  = 'data[desc]';
				                				$value = !empty($default["desc"]) ? $default["desc"] : '';
				                				$extra = 'id="desc" 
				                						  class="form-control mini-height" 
				                						  placeholder="isi disini"';

				                				echo form_textarea($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>
								</div>

								<!-- KONTEN TAB -->
								<div 
									role="tabpanel" 
									class="tab-pane fade" 
									id="komponencampaign" 
									aria-labelledby="komponencampaign-tab">
										<div>
											<label for="name">Target Donasi :</label>
											<div>
				                			<?php 
				                				$name  = 'data[target]';
				                				$value = !empty($default["target"]) ?$default["target"] : '0';
				                				$extra = 'id="target" 
				                						  class="form-control" 
				                						  placeholder="isi disini"';

				                				echo form_input($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>
										<div>
											<label for="name">Tanggal Valid :</label><br/>
											<div>
				                			<?php 
				                				$name  = 'data[valid_date]';
				                				$value = !empty($default["valid_date"]) ? ARR::dFormat($default["valid_date"],'m/d/Y') : '';
												$extra = 'class="form-control has-feedback-left" required="required"
												   		  id="valid_date" aria-describedby="inputSuccess2Status3"';

				                				echo form_input($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>
								</div>

								<!-- ATTACHMENT TAB -->
								<div 
									role="tabpanel" 
									class="tab-pane fade" 
									id="attachment" 
									aria-labelledby="attachment-tab">
										<div class="col-md-5 col-sm-12 col-xs-12" style="padding-left:50px">
											<label for="message">Gambar:</label>
											<div class="uibutton">
								                <?php 
								                    $img = !empty($default["img"]) ? $default["img"] :'noimage.png'; 

								                    $url_image = base_url() . "assets/images/articles/" . $img;
								                    if(!file_exists($url_image))
								                    {
								                    	$url_image = base_url() . "assets/images/articles/noimage.png";
								                    }
								                ?>
								                <img id="uploadPreview2" src="<?php echo $url_image; ?>" class="upload" height="200px"/>
								                <br/> 
								                <?php 
								                	$value = !empty($default["img"]) ? $default["img"] : '';
								                	$extra = 'class="form-control"';
								                	echo form_hidden('img', $value, $extra); 
								                ?>
								                <br/>
								                <div class="myuibutton">
								                    <span>Pilih Gambar</span>
								                    <?php 
								                    	$extra = "id='uploadImage2' onchange='PreviewImage(2);' multiple='multiple'";
								                    	echo form_upload('uploadedimages[]','', $extra); 
								                    ?>
								                    <!-- <input id="uploadImage2" type="file" name="data[images]" onchange="PreviewImage(2);"/> -->
								                </div>
                								<p class='label-error'></p>
				                			</div>
										</div>
										<div class="col-md-6 col-sm-12 col-xs-12">
											<h5>Catatan:</h5>
											<ul>
												<li>Gambar yang diupload harus berjenis <i>image/jpeg, image/jpg</i></li>
												<li>Size gambar yang diupload tidak boleh melebihi <u>1 MB</u></li>
												<li>Rekomendasi Gambar yang diupload berorientasi <b>landscape</b></li>
												<li>Size Proportional untuk gambar <u>(790 x 395)</u> px</li>
											</ul>
										</div>
								</div>

								<!-- META TAB -->
								<div 
									role="tabpanel" 
									class="tab-pane fade" 
									id="metatag" 
									aria-labelledby="metatag-tab">
										<div>
											<label for="name">Meta title <span class="require">*</span> :</label>
											<div>
				                			<?php 
				                				$name  = 'data[meta_title]';
				                				$value = !empty($default["meta_title"]) ? $default["meta_title"] : '';
				                				$extra = 'id="meta_title" 
				                						  class="form-control" 
				                						  placeholder="isi disini"';

				                				echo form_input($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>
										<div>
											<label for="message">Meta Description:</label>
											<div>
				                			<?php 
				                				$name  = 'data[meta_description]';
				                				$value = !empty($default["meta_description"]) ? $default["meta_description"] : '';
				                				$extra = 'id="meta_description" 
				                						  class="form-control mini-height" 
				                						  placeholder="isi disini"';

				                				echo form_textarea($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>
										<div>
											<label for="name">Meta Keyword <span class="require">*</span> :</label>
											<div>
				                			<?php 
				                				$name  = 'data[meta_keyword]';
				                				$value = !empty($default["meta_keyword"]) ? $default["meta_keyword"] : '';
				                				$extra = 'id="meta_keyword" 
				                						  class="form-control" 
				                						  placeholder="isi disini"';

				                				echo form_input($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>
								</div>

								<!-- UPDATE PROGRESS TAB -->
								<div 
									role="tabpanel" 
									class="tab-pane fade" 
									id="updateprogress" 
									aria-labelledby="updateprogress-tab">
										<div>
											<label for="message">Update Progress:</label>
											<div>
				                			<?php 
				                				$name  = 'data[update_progress]';
				                				$value = !empty($default["update_progress"]) ? $default["update_progress"] : '';
				                				$extra = 'id="update_progress" 
				                						  class="form-control mini-height" 
				                						  placeholder="isi disini"';

				                				echo form_textarea($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>
								</div>
							</div>
						</div>

						<br/>

						<div>
							<label class="agreement">
								<div>
								<?php
									$data = array(
									        'name'          => 'agreement',
									        'id'            => 'agreement',
									        'value'         => 'accept',
									        'checked'       => FALSE,
									);

									echo form_checkbox($data);
								?>
								Saya yakin dan bertanggung jawab atas penambahan data ini
								</div>
                				<p class='label-error'></p>
							</label>
						</div>

						<div class="actionBar">
							<a href="<?php URI::baseURL(); ?>cms/campaign" class="btn btn-primary">
								<i class="fa fa-reply"></i> Kembali
							</a>
							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>
						</div>
					    <?php
					    	echo form_close();
					    ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="height" value="0"></div>
	<div id="width" value="0"></div>
	<!-- /page content -->
{_layFooter}

<script type="text/javascript">
    CKEDITOR.replace('desc', settUpload);
    CKEDITOR.replace('update_progress', settUpload);
    // CKEDITOR.replace('mision', { toolbar : basic , height: 160});

	select2_icon("categories");
	select2_icon("type");

	$("#meta_keyword").tagit({allowSpaces: true});

    $('#target').autoNumeric('init', {aPad:false, aSep: '.', aDec: ',', aSign: ''});

	$(document).ready(function() {
		$('.tree').treegrid({
		    expanderExpandedClass	: 'glyphicon glyphicon-minus',
		    expanderCollapsedClass	: 'glyphicon glyphicon-plus'
		});

		var validator = $("#fmCampaign").validate({
			ignore: [],
			invalidHandler: function(e, validator){
		        if(validator.errorList.length)
		        $('#CampaignTab a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show');
		    },
			debug: true,
			errorElement: "em",
			errorContainer: $(".peringatan"),
			errorPlacement: function(error, element) {
				error.appendTo(element.parent("div").next("p"));
			},
			highlight: function(element) {   // <-- fires when element has error
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('sukses').addClass('error');
			},
			unhighlight: function(element) { // <-- fires when element is valid
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('error').addClass('sukses');
			},
			rules: {
				'data[title]': {
					required: true,
				},
				'data[categories]': {
					required: true,
				},
				'data[desc_short]': {
					required: true,
				},
				// 'data[type]': {
				// 	required: true,
				// },
				'agreement':{
					required: true
				},
				'images[]': {
					fileType: {
                            types: ["jpg", "jpeg"]
                        },
                    maxFileSize: {
                        "unit": "KB",
                        "size": 1000
                    },
                    minFileSize: {
                        "unit": "KB",
                        "size": "10"
                    },
	            }
			}, 
			messages: {
		        'data[title]' 		: "Field ini wajib disi",
		        'data[categories]' 	: "Field ini wajib disi",
		        'data[desc_short]' 	: "Field ini wajib disi",
		        // 'data[type]' 		: "Field ini wajib disi",
		        'agreement'  		: "Anda harus menyetujui pernyataan terlebih dahulu!",
		        'images[]'			: {
		        	fileType    : "Ekstensi file yg diterima adalah image/jpg, image/jpeg",
		        	maxFileSize : "File tidak boleh melebihi {0}{1}",
		        	minFileSize : "File tidak boleh kurang dari {0}{1}"
		        }
		    },
			submitHandler: function(form) {
				CKupdate();
		        form.submit();
		    }
		});

		$('#valid_date').daterangepicker({
			singleDatePicker: true,
			singleClasses: "picker_3"
		}, function(start, end, label) {
			console.log(start.toISOString(), end.toISOString(), label);
		});
	});

	function CKupdate(){
	    for ( instance in CKEDITOR.instances )
	        CKEDITOR.instances[instance].updateElement();
	}
</script>