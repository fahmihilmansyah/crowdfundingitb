{_layHeader}

	<!-- page content -->

	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Halaman Statis Lainnya</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Data
							<small>List Halaman Statis Lainnya</small>
						</h2>

						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>

							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>

									<i class="fa fa-wrench"></i>
								</a>
							</li>

							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>

				  	<!-- CONTENT -->

					<div class="x_content">
						<?php
							if($can_add)
							{

						?>
					  		<a href="<?php URI::baseURL(); ?>cms/spage_other/new" class="btn btn-success btn-custom pull-right">
					  			<i class="fa fa-plus border-r"></i>Tambah
					  		</a>

				  		<?php
				  			}
				  		?>
						<div class="clearfix"></div>

						<br/>

						<table 
								id="spage_other" 
								class="table table-striped table-bordered dt-responsive nowrap" 
								width="100%" 
								data-source="<?php echo $dsource?>"
						>

						<thead>
							<tr>
								<th>No. </th>
								<th>Title</th>
								<th>Jenis Halaman</th>
								<th class="center">Crtd At</th>
								<th class="center">Edtd At</th>
								<th class="center">Author</th>
								<th class="center">Status</th>
								<th class="center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr class="text-center">
								<td colspan="8"><i class="entypo-arrows-ccw"></i> Mengambil data...</td>
							</tr>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}

<?php 
	if(!empty($this->session->flashdata('success_notif')))
	{
?>
		<!-- Initialize Notification-->
		<div id="success_notif">
		  <?php echo $this->session->flashdata('success_notif'); ?>
		</div>

		<!-- Show Notification-->
		<script type="text/javascript">
		  $(function(){
		    showMessage($("#success_notif").html(), 'Aksi Sukses', 'success');
		  });
		</script>
<?php
	}
?>


<?php 
	if(!empty($this->session->flashdata('error_notif')))
	{
?>
		<!-- Initialize Notification-->
		<div id="error_notif">
		  <?php echo $this->session->flashdata('error_notif'); ?>
		</div>

		<!-- Show Notification-->
		<script type="text/javascript">
		  $(function(){
		    showMessage($("#error_notif").html(), 'Aksi Gagal', 'error');
		  });
		</script>
<?php
	}
?>


<script type="text/javascript">
	var app;
	var ds;

	$(document).ready(function() {
		ds  = $('#spage_other').attr('data-source');

		app = $('#spage_other')
				.on('preXhr.dt', function (e, settings, data) {
       				cekTokenValid("<?php URI::baseURL(); ?>cms/cauth_dt");
    			}).DataTable({
		        "aoColumns": [
			        { "width": "5%"},
			        null,
			        { "width": "10%"},
			        { "width": "10%"},
			        { "width": "10%"},
			        { "width": "10%"},
			        { "width": "10%"},
			        { "width": "15%", "sClass": "alignCenter", "bSortable": false }
		        ],
		        "order": [[ 0, "desc" ]],
		        "bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": ds,
			    "oLanguage": {
			        "sProcessing": "<img src='<?php URI::baseURL(); ?>assets/images/loading.gif' height='50px'/>"
			    },
		        "fnServerData": function( sUrl, aoData, fnCallback, oSettings ) {
					oSettings.jqXHR = $.ajax({
						"url": sUrl,
						"data": aoData,
						"success": fnCallback,
						"dataType": "jsonp",
						"cache": false
					});
		        },
		      });
	});
</script>

