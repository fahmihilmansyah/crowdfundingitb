{_layHeader}

	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Halam Statis Lainnya [ New ]</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Form
							<small>Input Data</small>
						</h2>

						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>

				  	<!-- CONTENT -->
					<div class="x_content">

						<!-- start form for validation -->

						<h5 class="alert alert-danger alert-dismissible fade in peringatan">
							Terdapat kesalahan dalam pengisian form. Silahkan cek kembali data form anda!
						</h5>

						<br/>

					    <?php

						    $hidden_form = array('id' => !empty($id) ? $id : '');
						    echo form_open_multipart($action, array('id' => 'fmartikel'), $hidden_form);
					    ?>

						<div class="" role="tabpanel" data-example-id="togglable-tabs">

							<!-- HEAD TAB PANEL -->

							<ul id="artikelTab"  class="nav nav-tabs bar_tabs" role="tablist">
								<li role="presentation" class="active">
									<a 
										href="#main" 
										id="main-tab" 
										role="tab" 
										data-toggle="tab" 
										aria-expanded="true">
										Main
									</a>
								</li>

								<li role="presentation" class="">
									<a 
										href="#submain" 
										role="tab" 
										id="submain-tab" 
										data-toggle="tab" 
										aria-expanded="false">
										Konten
									</a>
								</li>
							</ul>

							<!-- BODY TAB PANEL -->
							<div id="artikelTabContent" class="tab-content" style="min-height:300px">

								<!-- main TAB -->
								<div 
									role="tabpanel" 
									class="tab-pane fade active in" 
									id="main" 
									aria-labelledby="home-tab">

										<br clear="all"/>

										<div>
											<label for="message">Jenis Halaman <span class="require">*</span> :</label>
											<div>
			                				<?php 
												$name         = "data[page]";
												$value        = !empty($default["page"]) ? $default["page"] : '';
												$extra        = 'id="page" style="width:100%"';

												if(!empty($id)) $extra .= "disabled";

			                					echo form_dropdown($name, $page, $value, $extra); 
			                				?>
				                			</div>
				                			<p class='label-error'></p>
										</div>

										<div>
											<label for="name">Judul <span class="require">*</span> :</label>
											<div>
				                			<?php 
				                				$name  = 'data[title]';
				                				$value = !empty($default["title"]) ? $default["title"] : '';
				                				$extra = 'id="title" 
				                						  class="form-control" 
				                						  placeholder="isi disini"';

				                				echo form_input($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>

										<div>
											<label for="message">Sub Judul <span class="require">*</span> :</label>
											<div>
				                			<?php 
				                				$name  = 'data[stitle]';
				                				$value = !empty($default["stitle"]) ? $default["stitle"] : '';
				                				$extra = 'id="stitle" 
				                						  class="form-control mini-height" 
				                						  placeholder="isi disini"';

				                				echo form_textarea($name, $value, $extra); 
				                			?>
				                			</div>
				                			<p class='label-error'></p>
										</div>

										<div class="col-md-5 col-sm-12 col-xs-12" style="padding-left:50px">
											<label for="message">Gambar latar:</label>

											<div class="uibutton" style="overflow:hidden;">
								                <?php 
								                    $img = !empty($default["img"]) ? $default["img"] :'noimage.png'; 
								                    $url_image = base_url() . "assets/images/spage_other/" . $img;
								                ?>

								                <img id="uploadPreview2" src="<?php echo $url_image; ?>" class="upload" height="200px"/>

								                <br/> 

								                <?php 
								                	$value = !empty($default["img"]) ? $default["img"] : '';
								                	$extra = 'class="form-control"';
								                	echo form_hidden('img', $value, $extra); 
								                ?>

								                <br/>

								                <div class="myuibutton">
								                    <span>Pilih Gambar</span>
								                    <?php 
								                    	$extra = "id='uploadImage2' onchange='PreviewImage(2);' multiple='multiple'";
								                    	echo form_upload('uploadedimages[]','', $extra); 
								                    ?>
								                    <!-- <input id="uploadImage2" type="file" name="data[images]" onchange="PreviewImage(2);"/> -->
								                </div>
                								<p class='label-error'></p>
				                			</div>
										</div>

										<div class="col-md-6 col-sm-12 col-xs-12">
											<h5>Catatan:</h5>
											<ul>
												<li>Gambar yang diupload harus berjenis <i>image/jpeg, image/jpg</i></li>
												<li>Size gambar yang diupload tidak boleh melebihi <u>1 MB</u></li>
												<li>Rekomendasi Gambar yang diupload berorientasi <b>landscape</b></li>
												<li>Size Proportional untuk gambar <u>(790 x 395)</u> px</li>
											</ul>
										</div>
								</div>

								<!-- ATTACHMENT TAB -->
								<div 
									role="submain" 
									class="tab-pane fade" 
									id="submain" 
									aria-labelledby="attachment-tab">

										
										<div>
											<label for="message">Konten Page :</label>
											<div>
				                			<?php 
				                				$name  = 'data[desc]';
				                				$value = !empty($default["desc"]) ? $default["desc"] : '';
				                				$extra = 'id="desc" 
				                						  class="form-control mini-height" 
				                						  placeholder="isi disini"';

				                				echo form_textarea($name, $value, $extra); 
				                			?>

				                			</div>
				                			<p class='label-error'></p>
										</div>
								</div>
							</div>
						</div>

						<br clear="all"/>

						<div>
							<label class="agreement">
								<div>

								<?php

									$data = array(
									        'name'          => 'agreement',
									        'id'            => 'agreement',
									        'value'         => 'accept',
									        'checked'       => FALSE,
									);

									echo form_checkbox($data);
								?>

								Saya yakin dan bertanggung jawab atas penambahan data ini

								</div>
                				<p class='label-error'></p>
							</label>
						</div>

						<div class="actionBar">
							<a href="<?php URI::baseURL(); ?>cms/spage_other" class="btn btn-primary">
								<i class="fa fa-reply"></i> Kembali
							</a>
							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>
						</div>

					    <?php
					    	echo form_close();
					    ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="height" value="0"></div>
	<div id="width" value="0"></div>

	<!-- /page content -->

{_layFooter}

<script type="text/javascript">
    CKEDITOR.replace('desc', settUpload);

    // CKEDITOR.replace('mision', { toolbar : basic , height: 160});

	select2_icon("page");;

	$("#meta_keyword").tagit({allowSpaces: true});

	$(document).ready(function() {
		$('.tree').treegrid({
		    expanderExpandedClass	: 'glyphicon glyphicon-minus',
		    expanderCollapsedClass	: 'glyphicon glyphicon-plus'
		});

		var validator = $("#fmartikel").validate({
			ignore: [],
			invalidHandler: function(e, validator){
		        if(validator.errorList.length)
		        $('#artikelTab a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show');
		    },
			debug: true,
			errorElement: "em",
			errorContainer: $(".peringatan"),
			errorPlacement: function(error, element) {
				error.appendTo(element.parent("div").next("p"));
			},
			highlight: function(element) {   // <-- fires when element has error
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('sukses').addClass('error');
			},
			unhighlight: function(element) { // <-- fires when element is valid
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('error').addClass('sukses');
			},
			rules: {
				'data[title]': {
					required: true,
				},
				'data[stitle]': {
					required: true,
				},
				'agreement':{
					required: true
				},
				'images[]': {
					fileType: {
                            types: ["jpg", "jpeg"]
                        },
                    maxFileSize: {
                        "unit": "KB",
                        "size": 1000
                    },
                    minFileSize: {
                        "unit": "KB",
                        "size": "10"
                    },
	            }
			}, 
			messages: {
		        'data[title]' 		: "Field ini wajib disi",
		        'data[stitle]' 		: "Field ini wajib disi",
		        'agreement'  		: "Anda harus menyetujui pernyataan terlebih dahulu!",
		        'images[]'			: {
		        	fileType    : "Ekstensi file yg diterima adalah image/jpg, image/jpeg",
		        	maxFileSize : "File tidak boleh melebihi {0}{1}",
		        	minFileSize : "File tidak boleh kurang dari {0}{1}"
		        }
		    },
			submitHandler: function(form) {
				CKupdate();
		        form.submit();
		    }
		});

		$("#agreement").click(function(){

		});
	});

	function CKupdate(){
	    for ( instance in CKEDITOR.instances )
	        CKEDITOR.instances[instance].updateElement();
	}
</script>