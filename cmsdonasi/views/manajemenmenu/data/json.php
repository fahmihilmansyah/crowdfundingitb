<?php

$results['sEcho'] 			= $sEcho;
$results['iTotalRecords'] 	= $results['iTotalDisplayRecords'] = $iTotalRecords;

if(count($apps))
{
	
	$i=0;
	$no = 1;
	foreach($apps as $data)
	{
		$aksi  = '';

		$aksi .= "<center><div class='btn-group'>";
		$aksi .= "<a href='#' class='btn btn-success btn-xs'><i class='fa fa-edit'></i></a>";
		$aksi .= "<a href='#' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i></a>";
		$aksi .= "</div></center>";

		$results['aaData'][$i] = array
			(
				'<center>'.$no.'</center>',
				$data['s_field1'],
				$data['s_field2'],
				$data['s_field3'],
				$data['s_field4'],
				$data['s_field5'],
				$aksi,
			);	

		++$i;
		++$no;
	}
} else {
	for($i=0;$i<7;++$i) {
		$results['aaData'][0][$i] = 'tes';
	}
}

print($callback . '(' . json_encode($results) . ')');

