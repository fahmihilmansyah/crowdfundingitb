{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Manajemen Menu [ New ]</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Form
							<small>Input Menu</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">

						<!-- start form for validation -->
						<h5 class="alert alert-danger alert-dismissible fade in peringatan">
							Terdapat kesalahan dalam pengisian form. Silahkan cek kembali data form anda!
						</h5>
					    <?php
						    $hidden_form = array('id' => !empty($id) ? $id : '');
						    echo form_open_multipart($action, array('id' => 'fmMenus'), $hidden_form);
					    ?>
								<div>
									<label for="name">Menu <span class="require">*</span>:</label>
									<div>
		                			<?php 
		                				$name  = 'data[name]';
		                				$value = !empty($default["name"]) ? $default["name"] : '';
		                				$extra = 'id="name" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="message">URL <span class="require">*</span>:</label>
									<div>
		                			<?php 
		                				$name  = 'data[url]';
		                				$value = !empty($default["url"]) ? $default["url"] : '';
		                				$extra = 'id="url" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="message">Parent :</label>
									<div>
	                				<?php 
										$name         = "data[parent]";
										$value        = !empty($default["parent"]) ? $default["parent"] : '';
										$extra        = 'id="parent" style="width:100%"';

	                					echo form_dropdown($name, $parentopt, $value, $extra); 
	                				?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="message">Position <span class="require">*</span>:</label>
									<div>
		                			<?php 
		                				$name  = 'data[position]';
		                				$value = !empty($default["position"]) ? $default["position"] : '0';
		                				$extra = 'id="position" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="message">Icon :</label>
									<div>
	                				<?php 
										$name         = "data[icon]";
										$value        = !empty($default["icon"]) ? $default["icon"] : '';
										$extra        = 'id="icon" style="width:100%"';

	                					echo form_dropdown($name, ARR::font_awesome(), $value, $extra); 
	                				?>
		                			</div>
		                			<p class='label-error'></p>
								</div>
								
								<br/>
								
								<div>
									<label class="agreement">
										<div>
										<?php
											$data = array(
											        'name'          => 'agreement',
											        'id'            => 'agreement',
											        'value'         => 'accept',
											        'checked'       => FALSE,
											);

											echo form_checkbox($data);
										?>
										Saya yakin dan bertanggung jawab atas penambahan data ini
										</div>
		                				<p class='label-error'></p>
									</label>
								</div>

								<br/>

								<div class="actionBar">
									<a href="<?php URI::baseURL(); ?>cms/settingmenu" class="btn btn-primary">
										<i class="fa fa-reply"></i> Kembali
									</a>
									<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>
								</div>
					    <?php
					    	echo form_close();
					    ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}

<script type="text/javascript">
	select2_icon("parent");
	select2_icon("icon");
	select2_icon("is_active");

	$(document).ready(function() {
		var validator = $("#fmMenus").validate({
        	ignore: '*:not([name])',
			debug: true,
        	errorClass: "error",
			errorElement: "em",
			errorContainer: $(".peringatan"),
			errorPlacement: function(error, element) {
				error.appendTo(element.parent("div").next("p"));
			},
			highlight: function(element) {   // <-- fires when element has error
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('sukses').addClass('error');
			},
			unhighlight: function(element) { // <-- fires when element is valid
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('error').addClass('sukses');
			},
			rules: {
				'data[name]'		: {
					required	:true
				},
				'data[url]'			: {
					required	:true
				},
				'data[position]'	: {
					required	:true,
					number		:true
				},
				'agreement'	: {
					required	:true
				}
			}, 
			messages: {
		        'data[name]' 	 : "Field ini wajib diisi",
		        'data[url]'  	 : "Field ini wajib diisi",
		        'data[position]' : {
		        	required : "Field ini wajib diisi",
		        	number   : "Field harus berisi angka"
		        },
		        'data[is_active]': "Field ini wajib diisi",
		        'agreement'   	 : "Anda harus menyetujui pernyataan terlebih dahulu!"
		    },
			submitHandler: function(form) {
		        form.submit();
		    }
		});
	});

	$(document).on("change", ".select2-offscreen", function () {
        if (!$.isEmptyObject(validator.submitted)) {
           	alert("tes 1");
        }else{
        	alert("tes 2");
        }
    });
</script>