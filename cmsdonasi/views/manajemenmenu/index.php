{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Manajemen Menu</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Data
							<small>List Menu</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->
				  
					<div class="x_content">
						<?php
							if($can_add)
							{
						?>
					  		<a href="<?php URI::baseURL(); ?>cms/settingmenu/new" class="btn btn-success btn-custom pull-right">
					  			<i class="fa fa-plus border-r"></i>Tambah
					  		</a>
				  		<?php
				  			}
				  		?>
						<div class="clearfix"></div>
						<br/>
						<?php 
							echo $menuList;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}


<?php 
	if(!empty($this->session->flashdata('success_notif')))
	{
?>
		<!-- Initialize Notification-->
		<div id="success_notif">
		  <?php echo $this->session->flashdata('success_notif'); ?>
		</div>

		<!-- Show Notification-->
		<script type="text/javascript">
		  $(function(){
		    showMessage($("#success_notif").html(), 'Aksi Sukses', 'success');
		  });
		</script>
<?php
	}
?>

<script>
	$(document).ready(function() {
		$('.tree').treegrid({
		    expanderExpandedClass	: 'glyphicon glyphicon-minus',
		    expanderCollapsedClass	: 'glyphicon glyphicon-plus'
		});
	});

	function deleteRow(id){
		var url = $("#" + id).attr("link-target");

	    swal({   
		    title 				: "Apakah Anda Yakin?",   
		    text 				: "akan menghapus data ini.",
		    type 				: "warning",   
		    showCancelButton 	: true,   
		    confirmButtonColor	: "#DD6B55",   
		    confirmButtonText	: "Ya, Hapus!",   
		    closeOnConfirm		: false 
		}, function(isConfirm){
			if(isConfirm){
			   	location.href = url;
			}
	    });
	}
	
	function showMessage(txt,title,type){
	  new PNotify({
	      title: title,
	      text: txt,
	      type: type,
	      delay: 1000,
	      styling: 'bootstrap3',
	  });
	}
</script>
