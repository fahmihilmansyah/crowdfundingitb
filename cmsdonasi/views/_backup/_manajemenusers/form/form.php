{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Manajemen Users [ New ]</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Form
							<small>Input Users</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">

						<!-- start form for validation -->
						<h5 class="alert alert-danger alert-dismissible fade in peringatan">
							Terdapat kesalahan dalam pengisian form. Silahkan cek kembali data form anda!
						</h5>
					    <?php
						    $hidden_form = array('id' => !empty($id) ? $id : '');
						    echo form_open_multipart($action, array('id' => 'fmUsers'), $hidden_form);
					    ?>
								<div>
									<label for="name">Nama Lengkap <span class="require">*</span>:</label>
									<div>
		                			<?php 
		                				$name  = 'data[fname]';
		                				$value = !empty($default["fname"]) ? $default["fname"] : '';
		                				$extra = 'id="fname" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="message">Username <span class="require">*</span>:</label>
									<div>
		                			<?php 
		                				$name  = 'data[uname]';
		                				$value = !empty($default["uname"]) ? $default["uname"] : '';
		                				$extra = 'id="uname" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<?php
									if(empty($id)){
								?>

								<br/>

								<div>
									<label for="message">Password <span class="require">*</span>:</label>
									<div>
		                			<?php 
		                				$name  = 'data[pwd]';
		                				$value = !empty($default["pwd"]) ? $default["pwd"] : '';
		                				$extra = 'id="pwd" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_password($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="message">Confirm Password <span class="require">*</span>:</label>
									<div>
		                			<?php 
		                				$name  = 'pwd_confirm';
		                				$value = !empty($default["pwd_confirm"]) ? $default["pwd_confirm"] : '';
		                				$extra = 'id="pwd_confirm" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_password($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>
								<?php
									}
								?>
								<br/>

								<div>
									<label for="message">Akses Sebagai <span class="require">*</span>:</label>
									<div>
		                			<?php 
										$name         = "data[role_id]";
										$value        = !empty($default["role_id"]) ? $default["role_id"] : '';
										$extra        = 'id="role_id" class="form-control" ';

	                					echo form_dropdown($name, $role_opt, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="message">Email <span class="require">*</span>:</label>
									<div>
		                			<?php 
		                				$name  = 'data[email]';
		                				$value = !empty($default["email"]) ? $default["email"] : '';
		                				$extra = 'id="email" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="message">No Telpon <span class="require">*</span>:</label>
									<div>
		                			<?php 
		                				$name  = 'data[nope]';
		                				$value = !empty($default["nope"]) ? $default["nope"] : '';
		                				$extra = 'id="nope" 
		                						  class="form-control" 
		                						  placeholder="isi disini"';

		                				echo form_input($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>

								<div>
									<label for="message">Alamat :</label>
									<div>
		                			<?php 
		                				$name  = 'data[address]';
		                				$value = !empty($default["address"]) ? $default["address"] : '';
		                				$extra = 'id="address" 
		                						  class="form-control mini-height" 
		                						  placeholder="isi disini"';

		                				echo form_textarea($name, $value, $extra); 
		                			?>
		                			</div>
		                			<p class='label-error'></p>
								</div>

								<br/>
								
								<div>
									<label class="agreement">
										<div>
										<?php
											$data = array(
											        'name'          => 'agreement',
											        'id'            => 'agreement',
											        'value'         => 'accept',
											        'checked'       => FALSE,
											);

											echo form_checkbox($data);
										?>
										Saya yakin dan bertanggung jawab atas penambahan data ini
										</div>
		                				<p class='label-error'></p>
									</label>
								</div>

								<br/>
								<div class="actionBar">
									<a href="<?php URI::baseURL(); ?>cms/settingusers" class="btn btn-primary">
										<i class="fa fa-reply"></i> Kembali
									</a>
									<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>
								</div>
					    <?php
					    	echo form_close();
					    ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}

<script type="text/javascript">
	select2_icon("role_id");

	$(document).ready(function() {
		var validator = $("#fmUsers").validate({
        	ignore: '*:not([name])',
			debug: true,
			errorElement: "em",
			errorContainer: $(".peringatan"),
			errorPlacement: function(error, element) {
				error.appendTo(element.parent("div").next("p"));
			},
			highlight: function(element) {   // <-- fires when element has error
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('sukses').addClass('error');
			},
			unhighlight: function(element) { // <-- fires when element is valid
			    var text = $(element).parent("div").next("p");
			    text.find('em').removeClass('error').addClass('sukses');
			},
			rules: {
				'data[fname]': {
					required: true,
				},
				'agreement':{
					required: true
				},
				'data[uname]':{
					required: true
				},
				'data[pwd]':{
					required: true,
                    minlength : 6
				},
				'pwd_confirm':{
                    equalTo   : "#pwd"
				},
				'data[role_id]':{
					required:true
				},
				'data[email]':{
					required: true,
					email   : true
				},
				'data[nope]':{
					required: true,
					number:true
				}
			}, 
			messages: {
		        'data[fname]' : "Field ini wajib diisi",
		        'data[uname]' : "Field ini wajib diisi",
		        'data[pwd]'   : {
		        	required : "Field ini wajib diisi",
		            minlength: "Masukan paling sedikit {0} karakter"
		        },
		        'pwd_confirm' : {
		            equalTo  : "Konfirmasi password harus sama"
		        },
		        'data[role_id]': "Field ini wajib diisi",
		        'data[email]'  : {
		        	required : "Field ini wajib diisi",
		        	email 	 : "Format email harus benar" 
		        },
		        'data[nope]'  : {
		        	required : "Field ini wajib diisi",
		        	number   : "Field harus berisi angka"
		        },
		        'agreement'   : "Anda harus menyetujui pernyataan terlebih dahulu!"
		    },
			submitHandler: function(form) {
		        form.submit();
		    }
		});

		$("#agreement").click(function(){
			
		});
	});
</script>