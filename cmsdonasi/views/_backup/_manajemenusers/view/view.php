{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Manajemen Users [ View ]</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							View
							<small>Data Users</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">
						<table class="table table-striped table-bordered" >
							<tr>
								<td width="20%">Nama Lengkap</td>
								<td width="1%">:</td>
								<td><?php echo !empty($default['fname']) ? $default['fname'] : '-'; ?></td>
							</tr>
							<tr>
								<td>Username</td>
								<td>:</td>
								<td><?php echo !empty($default['uname']) ? $default['uname'] : '-'; ?></td>
							</tr>
							<tr>
								<td>Last login IP</td>
								<td>:</td>
								<td><?php 
										echo !empty($default['last_login_ip']) ? 
											  ARR::dFormat($default['last_login_ip'],'d M y - H:i:s') : $_SERVER['REMOTE_ADDR']; 
									?></td>
							</tr>
							<tr>
								<td>Last Login</td>
								<td>:</td>
								<td>
									<?php 
										echo !empty($default['last_login_date']) ? 
										ARR::dFormat($default['last_login_date'],'d M y - H:i:s') : ARR::dFormat(date('Y-m-d - H:i:s'),'d M y - H:i:s'); 
									?>		
								</td>
							</tr>
							<tr>
								<td>Last Online</td>
								<td>:</td>
								<td>
									<?php 
										echo !empty($default['last_online']) ? 
										ARR::dFormat($default['last_online'],'d M y - H:i:s') : '-'; 
									?>
								</td>
							</tr>
							<tr>
								<td>Created At</td>
								<td>:</td>
								<td><?php echo !empty($default['crtd_at']) ? ARR::dFormat($default['crtd_at'],'d M y - H:i:s') : '-'; ?></td>
							</tr>
							<tr>
								<td>Edited At</td>
								<td>:</td>
								<td><?php echo !empty($default['edtd_at']) ? ARR::dFormat($default['edtd_at'],'d M y - H:i:s') : '-'; ?></td>
							</tr>
							<tr>
								<td>Author</td>
								<td>:</td>
								<td><?php echo !empty($default['author']) ? $default['author'] : '-'; ?></td>
							</tr>
							<tr>
								<td>Last Edit</td>
								<td>:</td>
								<td><?php echo !empty($default['last_edit']) ? $default['last_edit'] : '-'; ?></td>
							</tr>
						</table>

                      	<div class="ln_solid"></div>

						<a href="<?php URI::baseURL(); ?>cms/settingusers" class="btn btn-primary">
							<i class="fa fa-reply"></i> Kembali
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}
