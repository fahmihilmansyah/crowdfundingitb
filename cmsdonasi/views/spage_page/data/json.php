<?php

$results['sEcho'] 			= $sEcho;
$results['iTotalRecords'] 	= $results['iTotalDisplayRecords'] = $iTotalRecords;

if(count($apps))
{
	
	$i=0;
	$no = 1;

	foreach($apps as $data)
	{
		$_key   = $this->session->userdata("_tokenaction");

		$aksi  	= '';
		$aksi  .= "<center><div class='btn-group'>";

		// VIEW BUTTON
		$param  = "id=" . $data["id"] . ";";
		$url    = base_url() . 'cms/spage_page/view?param=' . ENKRIP::encode($param);
		$aksi  .= "<a href='" . $url . "' class='btn btn-dark btn-xs'><i class='fa fa-eye'></i></a>";
		
		// EDIT BUTTON
		if($akses->getAccess("edit"))
		{
			$param  = "id=" . $data["id"] . ";";
			$param .= "token=" . $_key["editform"]["token"];
			$url    = base_url() . 'cms/spage_page/update?param=' . ENKRIP::encode($param);
			$aksi  .= "<a href='" . $url . "' class='btn btn-success btn-xs'><i class='fa fa-edit'></i></a>";
		}

		if($akses->getAccess("delete"))
		{
			// DELETE BUTTON
			$param  = "id=" . $data["id"] . ";";
			$param .= "token=" . $_key["delete"]["token"];
			$url    = base_url() . 'cms/spage_page/remove?param=' . ENKRIP::encode($param);
			$aksi  .= "<a id='delete-" . $data["id"] . "' 
						 href='javascript:void(0)' 
						 onclick='deleteRow(this.id)' 
						 class='btn btn-danger btn-xs'
						 link-target='" . $url . "'
					  >
							<i class='fa fa-trash'></i>
					  </a>";
		}

		$aksi .= "</div></center>";

		$last_online     = (!empty($data['last_online'])) ? ARR::dFormat($data['last_online'], "d M Y H:i:s") : "-";
		$last_login_date = (!empty($data['last_login_date'])) ? ARR::dFormat($data['last_login_date'], "d M Y H:i:s") : "-";
		$login_ip		 = (!empty($data["last_login_ip"])) ? $data["last_login_ip"] : '-';
		

		$results['aaData'][$i] = array
			(
				ARR::cText($no),
				$data['name'],
				$aksi,
			);	

		++$i;
		++$no;
	}
} else {
	for($i=0;$i<7;++$i) {
		$results['aaData'] = [];
	}
}

print($callback . '(' . json_encode($results) . ')');

