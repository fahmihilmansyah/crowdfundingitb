{_layHeader}
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Halaman Statis ( Jenis ) [ View ]</h3>
			</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="row">
			<?php echo $crumbs; ?>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							View
							<small>Data Jenis Halaman</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" 
								class="dropdown-toggle" 
								data-toggle="dropdown" 
								role="button" 
								aria-expanded="false"
								>
									<i class="fa fa-wrench"></i>
								</a>
							</li>
							<li>
								<a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>

					</div>

				  	<!-- CONTENT -->

					<div class="x_content">
						<table class="table table-striped table-bordered" >
							<tr>
								<td width="20%">ID</td>
								<td width="1%">:</td>
								<td><?php echo !empty($default['id']) ? $default['id'] : '-'; ?></td>
							</tr>
							<tr>
								<td>Index</td>
								<td>:</td>
								<td><?php echo !empty($default['name_key']) ? $default['name_key'] : '-'; ?></td>
							</tr>
							<tr>
								<td>Nama</td>
								<td>:</td>
								<td><?php echo !empty($default['name']) ? $default['name'] : '-'; ?></td>
							</tr>
						</table>

                      	<div class="ln_solid"></div>

						<a href="<?php URI::baseURL(); ?>cms/spage_page" class="btn btn-primary">
							<i class="fa fa-reply"></i> Kembali
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
{_layFooter}
