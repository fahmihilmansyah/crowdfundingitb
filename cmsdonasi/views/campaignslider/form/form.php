{_layHeader}

	<!-- page content -->

	<div class="right_col" role="main">

		<div class="page-title">

			<div class="title_left">

				<h3>Campaign Slider [ New ]</h3>

			</div>

		</div>

		

		<div class="clearfix"></div>



		<div class="row">

			<?php echo $crumbs; ?>

		</div>



		<div class="clearfix"></div>



		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">

				<div class="x_panel">

					<div class="x_title">

						<h2>

							Form

							<small>Input Slider Campaign</small>

						</h2>

						<ul class="nav navbar-right panel_toolbox">

							<li>

								<a class="collapse-link">

									<i class="fa fa-chevron-up"></i>

								</a>

							</li>

							<li class="dropdown">

								<a href="#" 

								class="dropdown-toggle" 

								data-toggle="dropdown" 

								role="button" 

								aria-expanded="false"

								>

									<i class="fa fa-wrench"></i>

								</a>

							</li>

							<li>

								<a class="close-link"><i class="fa fa-close"></i></a>

							</li>

						</ul>

						<div class="clearfix"></div>



					</div>



				  	<!-- CONTENT -->



					<div class="x_content">



						<!-- start form for validation -->

						<h5 class="alert alert-danger alert-dismissible fade in peringatan">

							Terdapat kesalahan dalam pengisian form. Silahkan cek kembali data form anda!

						</h5>

					    <?php

						    $hidden_form = array('id' => !empty($id) ? $id : '');

						    echo form_open_multipart($action, array('id' => 'fmcampaignslider'), $hidden_form);

					    ?>

								<div class="col-md-5 col-sm-12 col-xs-12" style="padding-left:50px">

											<label for="message">Gambar:</label>

											<div class="uibutton" style="overflow: hidden;">

								                <?php 

								                    $img = !empty($default["img"]) ? $default["img"] :'noimage.png'; 



								                    $rpath	   = FCPATH . 'assets/images/slider/' . $img;

								                    $url_image = "";



								                    if(!file_exists($rpath))

								                    {

								                    	$url_image = base_url() . "assets/images/slider/noimage.png";

								                    }

								                    else

								                    {

								                    	$url_image = base_url() . "assets/images/slider/" . $img;

								                    }

								                ?>

								                <img id="uploadPreview2" src="<?php echo $url_image; ?>" class="upload" height="200px"/>

								                <br/> 

								                <?php 

								                	$value = !empty($default["img"]) ? $default["img"] : '';

								                	$extra = 'class="form-control"';

								                	echo form_hidden('img', $value, $extra); 

								                ?>

								                <br/>

								                <div class="myuibutton">

								                    <span>Pilih Gambar</span>

								                    <?php 

								                    	$extra = "id='uploadImage2' onchange='PreviewImage(2);' multiple='multiple'";

								                    	echo form_upload('uploadedimages[]','', $extra); 

								                    ?>

								                    <!-- <input id="uploadImage2" type="file" name="data[images]" onchange="PreviewImage(2);"/> -->

								                </div>

                								<p class='label-error'></p>

				                			</div>

										</div>

										<div class="col-md-6 col-sm-12 col-xs-12">

											<h5>Catatan:</h5>

											<ul>

												<li>Gambar yang diupload harus berjenis <i>png, image/jpeg, image/jpg</i></li>

												<li>Size gambar yang diupload tidak boleh melebihi <u>1 MB</u></li>

												<li>Rekomendasi Gambar yang diupload berorientasi <b>landscape</b></li>

												<li>Size Proportional untuk gambar <u>(790 x 395)</u> px</li>

											</ul>

										</div>

										<br clear="all">

										<br>

								<div>

									<label for="title">Title <span class="require"></span> :</label>

									<div>

		                			<?php 

		                				$name  = 'data[title]';

		                				$value = !empty($default["title"]) ? $default["title"] : '';

		                				$extra = 'id="title" 

		                						  class="form-control" 

		                						  placeholder="isi disini"';



		                				echo form_input($name, $value, $extra); 

		                			?>

		                			</div>

		                			<p class='label-error'></p>

								</div>



								<div>

									<label for="message">Tipe Slider :</label>

									<div>

	                				<?php 

										$name         = "data[type]";

										$value        = !empty($default["type"]) ? $default["type"] : '';

										$extra        = 'id="type" style="width:100%"';

										$typeopt      = array("" => "Pilih Data", "1" => "Normal Slider", "2" => "Campaign Slider", "3" => "Ads Slider");



	                					echo form_dropdown($name, $typeopt, $value, $extra); 

	                				?>

		                			</div>

		                			<p class='label-error'></p>

								</div>



								<br/>

										<div>

									<label for="desc">Desc <span class="require"></span> :</label>

									<div>

		                			<?php 

		                				$name  = 'data[desc]';

		                				$value = !empty($default["desc"]) ? $default["desc"] : '';

		                				$extra = 'id="desc" 

		                						  class="form-control mini-height" 

		                						  placeholder="isi disini"';



		                				echo form_textarea($name, $value, $extra); 

		                			?>

		                			</div>

		                			<p class='label-error'></p>

								</div>



								<br/>

									<div>

									<label for="link">Link <span class="require"></span> :</label>

									<div>

		                			<?php 

		                				$name  = 'data[link]';

		                				$value = !empty($default["link"]) ? $default["link"] : '';

		                				$extra = 'id="link" 

		                						  class="form-control" 

		                						  placeholder="isi disini"';



		                				echo form_input($name, $value, $extra); 

		                			?>

		                			</div>

		                			<p class='label-error'></p>

								</div>



								<div>

									<label class="agreement">

										<div>

										<?php

											$status = (empty($default["link_d"])) ? FALSE : $default["link_d"];



											$data = array(

											        'name'          => 'link_d',

											        'id'            => 'link_d',

											        'value'         => 'display',

											        'checked'       => $status,

											);



											echo form_checkbox($data);

										?>

										Tampilkan Title, Deskripsi, dan Link

										</div>

		                				<p class='label-error'></p>

									</label>

								</div>



								<hr/>



								<div>

									<label class="agreement">

										<div>

										<?php

											$data = array(

											        'name'          => 'agreement',

											        'id'            => 'agreement',

											        'value'         => 'accept',

											        'checked'       => FALSE,

											);



											echo form_checkbox($data);

										?>

										Saya yakin dan bertanggung jawab atas penambahan data ini

										</div>

		                				<p class='label-error'></p>

									</label>

								</div>

								<div class="actionBar">

									<a href="<?php URI::baseURL(); ?>cms/campaignkategori" class="btn btn-primary">

										<i class="fa fa-reply"></i> Kembali

									</a>

									<button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan</button>

								</div>

					    <?php

					    	echo form_close();

					    ?>

					</div>

				</div>

			</div>

		</div>

	</div>

	<!-- /page content -->

{_layFooter}



<script type="text/javascript">



	select2_icon("type");



	$(document).ready(function() {

		$('.tree').treegrid({

		    expanderExpandedClass	: 'glyphicon glyphicon-minus',

		    expanderCollapsedClass	: 'glyphicon glyphicon-plus'

		});





		var validator = $("#fmcampaignslider").validate({

			debug: true,

			errorElement: "em",

			errorContainer: $(".peringatan"),

			errorPlacement: function(error, element) {

				error.appendTo(element.parent("div").next("p"));

			},

			highlight: function(element) {   // <-- fires when element has error

			    var text = $(element).parent("div").next("p");

			    text.find('em').removeClass('sukses').addClass('error');

			},

			unhighlight: function(element) { // <-- fires when element is valid

			    var text = $(element).parent("div").next("p");

			    text.find('em').removeClass('error').addClass('sukses');

			},

			rules: {

				// 'data[title]': {

				// 	required: true,

				// },

				// 'data[desc]': {

				// 	required: true,

				// },

				// 'data[link]': {

				// 	required: true,

				// },

				'uploadedimages[]': {

					fileType: {

                            types: ["png","jpg", "jpeg"]

                        },

                    maxFileSize: {

                        "unit": "KB",

                        "size": 1000

                    },

                    minFileSize: {

                        "unit": "KB",

                        "size": 10

                    },

	            },

				'agreement':{

					required: true

				}

			}, 

			messages: {

		        // 'data[title]' : "Field ini wajib disi",

		        // 'data[desc]'  : "Field ini wajib disi",

		        // 'data[link]'  : "Field ini wajib disi",

		        'agreement'   : "Anda harus menyetujui pernyataan terlebih dahulu!",

		        'uploadedimages[]'			: {

		        	fileType    : "Ekstensi file yg diterima adalah image/jpg, image/jpeg",

		        	maxFileSize : "File tidak boleh melebihi {0}{1}",

		        	minFileSize : "File tidak boleh kurang dari {0}{1}"

		        }

		    },

			submitHandler: function(form) {

		        form.submit();

		    }

		});



		$("#agreement").click(function(){

			

		});

	});

</script>