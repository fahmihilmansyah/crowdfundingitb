{_layHeader}
<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title">

        <div class="title_left">

            <h3>User Info</h3>

        </div>

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>

                        Profile


                    </h2>

                    <ul class="nav navbar-right panel_toolbox">

                        <li>

                            <a class="collapse-link">

                                <i class="fa fa-chevron-up"></i>

                            </a>

                        </li>

                        <li class="dropdown">

                            <a href="#"

                               class="dropdown-toggle"

                               data-toggle="dropdown"

                               role="button"

                               aria-expanded="false"

                            >

                                <i class="fa fa-wrench"></i>

                            </a>

                        </li>

                        <li>

                            <a class="close-link"><i class="fa fa-close"></i></a>

                        </li>

                    </ul>

                    <div class="clearfix"></div>


                </div>


                <!-- CONTENT -->


                <div class="x_content">

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                        <div class="profile_img">

                            <div id="crop-avatar">

                                <!-- Current avatar -->

                                <?php $pprofile = !empty($dsource['img']) ? $dsource['img'] : "user.png"; ?>

                                <img class="img-responsive avatar-view"
                                     src="<?php URI::baseURL(); ?>assets/images/admprofil/<?php echo $pprofile; ?>" alt="...">

                            </div>

                        </div>

                    </div>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <div id="myTabContent" class="tab-content">

                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">

                                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab"
                                                                          data-toggle="tab"
                                                                          aria-expanded="true">Profile</a>
                                </li>

                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab"
                                                                    data-toggle="tab" aria-expanded="false">Edit
                                        Profile</a>
                                </li>

                            </ul>

                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                 aria-labelledby="profile-tab">

                                <div class="col-md-1 col-sm-1 col-xs-1"></div>
                                <div class="col-md-11 col-sm-11 col-xs-11">
                                    <div>
                                        <label>Nama : </label><span> <?php echo $dsource['fname']; ?></span>
                                    </div>
                                    <div>
                                        <label>Username : </label><span> <?php echo $dsource['uname']; ?></span>
                                    </div>
                                    <div>
                                        <label>Email : </label><span> <?php echo $dsource['email']; ?></span>
                                    </div>
                                    <div>
                                        <label>Nomor HP : </label><span> <?php echo $dsource['nope']; ?></span>
                                    </div>
                                    <div>
                                        <label>Alamat : </label><span> <?php echo $dsource['address']; ?></span>
                                    </div>
                                    <div>
                                        <label>Last Online
                                            : </label><span> <?php echo $dsource['last_online']; ?></span>
                                    </div>
                                    <div>
                                        <label>Last Login IP
                                            : </label><span> <?php echo $dsource['last_login_ip']; ?></span>
                                    </div>
                                    <div>
                                        <label>Last Login Date
                                            : </label><span> <?php echo $dsource['last_login_date']; ?></span>
                                    </div>

                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                                <h5 class="alert alert-danger alert-dismissible fade in peringatan">
                                    Terdapat kesalahan dalam pengisian form. Silahkan cek kembali data form anda!
                                </h5>
                                <?php
                                $hidden_form = array('id' => !empty($id) ? $id : '');
                                echo form_open_multipart($action, array('id' => 'fruser'), $hidden_form);
                                ?>
                                <div>
                                    <label for="name">Nama :</label>
                                    <div>
                                        <?php
                                        $name = 'data[fname]';
                                        $value = !empty($dsource['fname']) ? $dsource['fname'] : '';
                                        $extra = 'id="fname" 
															  class="form-control" 
															  placeholder="isi disini"';

                                        echo form_input($name, $value, $extra);
                                        ?>
                                    </div>
                                    <p class='label-error'></p>
                                </div>

                                <div>
                                    <label for="message">Username :</label>
                                    <div>
                                        <?php
                                        $name = 'data[uname]';
                                        $value = !empty($dsource['uname']) ? $dsource['uname'] : '';
                                        $extra = 'id="uname" readonly
															  class="form-control" 
															  placeholder="isi disini"';

                                        echo form_input($name, $value, $extra);
                                        ?>
                                    </div>
                                    <p class='label-error'></p>
                                </div>

                                <div>
                                    <label for="message">Password :</label>
                                    <div>
                                        <?php
                                        $name = 'data[pwd]';
                                        $value = '';
                                        $extra = 'id="pwd" 
															  class="form-control" 
															  placeholder="isi disini"';

                                        echo form_password($name, $value, $extra);
                                        ?>
                                    </div>
                                    <p class='label-error'></p>
                                </div>

                                <div>
                                    <label for="message">No. HP :</label>
                                    <div>
                                        <?php
                                        $name = 'data[nope]';
                                        $value = !empty($dsource['nope']) ? $dsource['nope'] : '';
                                        $extra = 'id="nope" 
															  class="form-control" 
															  placeholder="isi disini"';

                                        echo form_input($name, $value, $extra);
                                        ?>
                                    </div>
                                    <p class='label-error'></p>
                                </div>

                                <div>
                                    <label for="message">Alamat :</label>
                                    <div>
                                        <?php
                                        $name = 'data[address]';
                                        $value = !empty($dsource['address']) ? $dsource['address'] : '';
                                        $extra = 'id="address" 
															  class="form-control" 
															  placeholder="isi disini"';

                                        echo form_input($name, $value, $extra);
                                        ?>
                                    </div>
                                    <p class='label-error'></p>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="message">Gambar:</label>
                                        <div class="uibutton">
                                            <?php
                                            $img = !empty($dsource["img"]) ? $dsource["img"] : 'noimage.png';
                                            $img = URI::existsImage('admprofil/',
                                                $img,
                                                'articles/',
                                                'noimage.jpg');
                                            $url_image = $img;
//                                            $url_image = base_url() . "assets/images/admprofil/" . $img;
//                                            if (!file_exists($url_image)) {
//                                                $url_image = base_url() . "assets/images/articles/noimage.png";
//                                            }
                                            ?>
                                            <img id="uploadPreview2" src="<?php echo $url_image; ?>" class="upload"
                                                 height="200px"/>
                                            <br/>
                                            <?php
                                            $value = !empty($dsource["img"]) ? $dsource["img"] : '';
                                            $extra = 'class="form-control"';
                                            echo form_hidden('img', $value, $extra);
                                            ?>
                                            <br/>
                                            <div class="myuibutton">
                                                <span>Pilih Gambar</span>
                                                <?php
                                                $extra = "id='uploadImage2' onchange='PreviewImage(2);' multiple='multiple'";
                                                echo form_upload('uploadedimages[]', '', $extra);
                                                ?>
                                            </div>
                                            <p class='label-error'></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6"><label for="message">QRIS:</label>
                                        <div class="uibutton">
                                            <?php

                                            $img = !empty($dsource["img_qris"]) ? $dsource["img_qris"] : 'noimage.png';

//                                            $url_image = base_url() . "assets/images/admprofil/" . $img;
                                            $img = URI::existsImage('admprofil/',
                                                $img,
                                                'articles/',
                                                'noimage.jpg');
                                            $url_image = $img;
//                                            if (!file_exists($url_image)) {
//                                                $url_image = base_url() . "assets/images/articles/noimage.png";
//                                            }
                                            ?>
                                            <img id="uploadPreview3" src="<?php echo $url_image; ?>" class="upload"
                                                 height="200px"/>
                                            <br/>
                                            <?php
                                            $value = !empty($dsource["img_qris"]) ? $dsource["img_qris"] : '';
                                            $extra = 'class="form-control"';
                                            echo form_hidden('img', $value, $extra);
                                            ?>
                                            <br/>
                                            <div class="myuibutton">
                                                <span>Pilih Gambar</span>
                                                <?php
                                                $extra = "id='uploadImage3' onchange='PreviewImage(3);' ";
                                                echo form_upload('uploadedimagesqris[]', '', $extra);
                                                ?>
                                            </div>
                                            <p class='label-error'></p>
                                        </div></div>
                                </div>


                                <br/>

                                <div>
                                    <label class="agreement">
                                        <div>
                                            <?php
                                            $data = array(
                                                'name' => 'agreement',
                                                'id' => 'agreement',
                                                'value' => 'accept',
                                                'checked' => FALSE,
                                            );

                                            echo form_checkbox($data);
                                            ?>
                                            Saya yakin dan bertanggung jawab atas penambahan data ini
                                        </div>
                                        <p class='label-error'></p>
                                    </label>
                                </div>

                                <div class="actionBar">
                                    <a href="<?php URI::baseURL(); ?>cms/usrinfo" class="btn btn-primary">
                                        <i class="fa fa-reply"></i> Kembali
                                    </a>
                                    <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i>
                                        Simpan
                                    </button>
                                </div>
                                <?php
                                echo form_close();
                                ?>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
<!-- /page content -->
{_layFooter}
<script type="text/javascript">
    $(document).ready(function () {
        $('.tree').treegrid({
            expanderExpandedClass: 'glyphicon glyphicon-minus',
            expanderCollapsedClass: 'glyphicon glyphicon-plus'
        });

        var validator = $("#fruser").validate({
            debug: true,
            errorElement: "em",
            errorContainer: $(".peringatan"),
            errorPlacement: function (error, element) {
                error.appendTo(element.parent("div").next("p"));
            },
            highlight: function (element) {   // <-- fires when element has error
                var text = $(element).parent("div").next("p");
                text.find('em').removeClass('sukses').addClass('error');
            },
            unhighlight: function (element) { // <-- fires when element is valid
                var text = $(element).parent("div").next("p");
                text.find('em').removeClass('error').addClass('sukses');
            },
            rules: {
                'data[name]': {
                    required: true,
                },
                'agreement': {
                    required: true
                }
            },
            messages: {
                'data[name]': "Field ini wajib disi",
                'agreement': "Anda harus menyetujui pernyataan terlebih dahulu!"
            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        $("#agreement").click(function () {

        });
    });
</script>