<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Thumb()
 * A TimThumb-style function to generate image thumbnails on the fly.
 *
 * @author Darren Craig
 * @author Lozano Hernán <hernantz@gmail.com>
 * @access public
 * @param string $src
 * @param int $width
 * @param int $height
 * @param string $image_thumb
 * @return String
 *
 */

function resize_img($src, $image_src = '') {

	// Get the CodeIgniter super object
	$CI = &get_instance();

    if (!is_file($src . $image_src[0]['file_name'])) {
        $config = array(
            'source_image' => $image_src[0]['full_path'], 
            'new_image' => $src,
            'width' => 380,
            'height' => 190
        );
        $CI->load->library('image_lib', $config);
        $CI->image_lib->resize(); 
    }
	
}
function fit_img($src, $image_src = '') {

    // Get the CodeIgniter super object
    $CI = &get_instance();

    $CI->load->library('image_lib');

    if (!is_file($src . $image_src[0]['file_name'])) {
        $width = $image_src[0]['width'];
        $height = $image_src[0]['height'];
        $w = 400;
        $h = 400;
        $config = array(
            'source_image' => $image_src[0]['full_path'],
            'new_image' => $src,
            'width' => $w,
            'height' => $h,
        );
        $config['maintain_ratio'] = false;
        $CI->image_lib->clear();
        $CI->image_lib->initialize($config);
        $CI->image_lib->fit();
    }

}

//detail campaign
function imgcampaigndetail($img){
		$CI = &get_instance();
		$config = array();
		$config['image_library'] = 'gd2';
		$config['source_image'] = FCPATH.'/assets/images/campaign/'.$img;
		$config['new_image'] = FCPATH.'/assets/images/campaign/thumb_main/';
		$config['maintain_ratio'] = FALSE;
		$config['width']         = 442;
		$config['height']       = 442;

		$CI->load->library('image_lib', $config);

		$CI->image_lib->resize();
		
		$a = base_url('assets/images/campaign/thumb_main/'.$img);
		
		return $a;
	}
	//detail campaign
function imagedetail($img){
		$CI = &get_instance();
		$config = array();
		$config['image_library'] = 'gd2';
		$config['source_image'] = './assets/images/campaign/thumbnails/'.$img;
		$config['new_image'] = './assets/images/campaign/thumbnails/detail/';
		$config['maintain_ratio'] = FALSE;
		$config['width']         = 442;
		$config['height']       = 442;

		$CI->load->library('image_lib', $config);

		$CI->image_lib->resize();

		$a = base_url('assets/images/campaign/thumbnails/thumb/'.$img);

		return $a;
	}

//list artikel
function imageartikel($img){
		$CI = &get_instance();
		$config = array();
		$config['image_library'] = 'gd2';
		$config['source_image'] = './assets/images/campaign/thumbnails/'.$img;
		$config['new_image'] = './assets/images/articles/artikellist/';
		$config['maintain_ratio'] = FALSE;
		$config['width']         = 300;
		$config['height']       = 300;

		$CI->load->library('image_lib', $config);

		$CI->image_lib->resize();
		
		$a = base_url('assets/images/articles/artikellist/'.$img);
		
		return $a;
	}

//popular artikel	
function imagepopular($img){
		$CI = &get_instance();
		$config = array();
		$config['image_library'] = 'gd2';
		$config['source_image'] = './assets/images/campaign/thumbnails/'.$img;
		$config['new_image'] = './assets/images/articles/artikelpopular/';
		$config['maintain_ratio'] = FALSE;
		$config['width']         = 125;
		$config['height']       = 125;
		$config['x_axis']       = 60;
		$config['y_axis']       = 60;
		
		$CI->load->library('image_lib');
		$CI->image_lib->initialize($config);
		$CI->image_lib->crop();
		
		$a = base_url('assets/images/articles/artikelpopular/'.$img);
		
		return $a;
	}

/* End of file thumb_helper.php */
/* Location: ./application/helpers/thumb_helper.php */
