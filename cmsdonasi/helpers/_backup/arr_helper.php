<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class arr {



	/**

	 | ----------------------------------------

	 | #Array Helper - helper

	 | ----------------------------------------

	 | #author 		    : Fahmi Hilmansyah

	 | #time created    : 4 January 2017

	 | #last developed  : Fahmi Hilmansyah

	 | #last updated    : 4 January 2017

	 | ----------------------------------------

	 */

    private $_loader;



    function __construct(){

        parent::__construct();



        $this->_loader =& get_instance();

        $this->_loader->load->database();

    }



    public static function cText($plainText = "") {

       return  "<center>" . $plainText . "</center>";

    }



    public static function dFormat($date = "", $format = "Y-m-d"){

    	return date($format, strtotime($date));

    }



    public static function tFormat($plainText = "", $format = "lower"){

    	if($format == "lower"){

    		return strtolower($plainText);

    	}



    	if($format == "upper"){

    		return strtoupper($plainText);

    	}



    	if($format == "capital"){

    		return ucwords($plainText);

    	}



    	return $plainText;

    }



    public static function getParam($param = array()){

        $data = array();



       

        if(!mb_detect_encoding($param, 'UTF-8', true)){

            return false;

        }



        $count   = array_filter(explode(';', $param));



        if(count($count) == 1)

        {

            $param  = str_replace(';', '', $param);

            $data   = explode('=', $param);



            $result = array();

            foreach ($data as $key => $value) {

                $result[$data[0]] = $data[1];

            }

        }

        else

        {



            $data   = explode(';', $param);



            $temp   = array();

            foreach ($data as $key => $value) {

                $temp[] = explode('=', $value);

            }



            foreach ($temp as $key => $value) {

                $result[$value[0]] = $value[1];

            }

        }



        return $result;

    }



    public static function font_awesome($kode = '', $default = '-- Pilih Icon --', $key = '') {

        $font_list = array();

        $font_source = FCPATH . 'assets/css/icon/icon-list.txt';

        

        if (file_exists($font_source)) {

            $font_content = file_get_contents($font_source);

            $font_list = explode(',', $font_content);

        }



        if (empty($kode)) {

            $data = array();

            foreach ($font_list as $value) {

                $value = trim($value);

                $data[$value] = $value;

            }

            if (!empty($default)) {

                return self::merge_array(array($key => $default), $data);

            } else {

                return $data;

            }

        }

    }



    public static function merge_array($array1 = array(), $array2 = array()) {

        $array = array();

        foreach ($array1 as $key => $value) {

            $array[$key] = $value;

        }

        foreach ($array2 as $key => $value) {

            $array[$key] = $value;

        }

        return $array;

    }



    public static function rupiah($number = 0, $type = 'rp'){

        if($type == 'rp'){

            return number_format($number, 0,".",".");

        }



        if($type == 'normal'){

            return preg_replace('/\./', '', $number);

        }

    }



}

