<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class uri {



	/**

	 | ----------------------------------------

	 | #URI Helper - helper

	 | ----------------------------------------

	 | #author 		    : Fahmi Hilmansyah

	 | #time created    : 4 January 2017

	 | #last developed  : Fahmi Hilmansyah

	 | #last updated    : 4 January 2017

	 | ----------------------------------------

	 */



    public static function baseURL() {

       echo base_url();

    }

	public static function slug($string, $spaceRepl = "-") {
		// Replace "/" char with "and"
		$string = str_replace("\'", "", $string);
		// Replace "/" char with "and"
		$string = str_replace('\"', "", $string);
		// Replace "/" char with "and"
		$string = str_replace("\/", "atau", $string);
		// Replace "&" char with "and"
		$string = str_replace("&", "dan", $string);
		// Delete any chars but letters, numbers, spaces and _, -
		$string = preg_replace("/[^a-zA-Z0-9 _-]/", "", $string);
		// Optional: Make the string lowercase
		$string = strtolower($string);
		// Optional: Delete double spaces
		$string = preg_replace("/[ ]+/", " ", $string);
		// Replace spaces with replacement
		$string = str_replace(" ", $spaceRepl, $string);
		return $string;
	}

	public static function encode_url($string = "")
	{
		return urlencode($string);
	}
}

