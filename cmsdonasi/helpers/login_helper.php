<?php



    function setSession($data = array()) {
        $sess_data[LGI_KEY . "login_info"] = array(
                                                "userid"        => $user_data->id,
                                                "fname"         => $user_data->fname,
                                                "email"         => $user_data->email,
                                                "statLogin"     => true,
                                                "login_uid"     => $user_data->id,
                                                "login_fname"   => $user_data->fname,
                                                "login_lname"   => $user_data->lname,
                                                "login_email"   => $user_data->email,
                                                "login_laston"  => $user_data->last_online,
                                                "login_ip"      => $user_data->last_login_ip,
                                                "login_date"    => $user_data->last_login_date,
                                                "login_type"    => "default",
                                                "login_sex"     => $user_data->sex,
                                                "login_status"  => true,
                                                "login_token"   => $token
                                             );

        $this->_loader->session->set_userdata($sess_data);
    }