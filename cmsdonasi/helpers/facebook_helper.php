<?php



defined('BASEPATH') OR exit('No direct script access allowed');







class facebook {



	/**

	 | ----------------------------------------

	 | #Array Helper - helper

	 | ----------------------------------------

	 | #author 		    : Fahmi Hilmansyah

	 | #time created    : 4 January 2017

	 | #last developed  : Fahmi Hilmansyah

	 | #last updated    : 4 January 2017

	 | ----------------------------------------

	 */



    private $_loader;



    function __construct(){



        parent::__construct();



        $this->_loader =& get_instance();



    }



    public static function getFacebookAuth()

    {

        // Facebook API Configuration

        $appId          = FB_API_ID;

        $appSecret      = FB_API_KEY;

        $redirectUrl    = base_url() . 'user_authentication/';

        $fbPermissions  = 'email';



        //Call Facebook API

        $facebook = new \Facebook\Facebook([

          'app_id' => $appId,

          'app_secret' => $appSecret,

          'default_graph_version' => 'v2.8',

          //'default_access_token' => '{access-token}', // optional

        ]);



        return $facebook;

    }



    public static function getFacebookURL()

    {

        $facebook = self::getFacebookAuth();



        $helper = $facebook->getRedirectLoginHelper();



        $permissions = ['email'];



        $loginUrl = $helper->getLoginUrl(base_url() . '/auten/redirect/facebook', $permissions);



        return $loginUrl;

    }



    public static function setSession()

    {

        

    }

}



