<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class hash {



	/**

	 | ----------------------------------------

	 | #Array Helper - helper

	 | ----------------------------------------

	 | #author 		    : Fahmi Hilmansyah

	 | #time created    : 4 January 2017

	 | #last developed  : Fahmi Hilmansyah

	 | #last updated    : 4 January 2017

	 | ----------------------------------------

	 */

    private $_loader;



    function __construct(){
        parent::__construct();
    }



	public static function getCustomHash($plainText = ""){
		return self::setCustomHash($plainText);
	}

	private static function setCustomHash($plainText, $salt = null)
	{
		$secretkey = "2c7042a2e8411121d192aaa7809a79be";

	    if ($salt === null)
	    {
	        $salt = substr($secretkey, 0, SALT_LENGTH);
	    }
	    else
	    {
	        $salt = substr($salt, 0, SALT_LENGTH);
	    }

	    return $salt . md5(base64_encode(sha1($salt . md5($plainText))));
	}


}

