<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Nbauth{



	/**

	 * ----------------------------------------

	 * #NB Auth Libraries

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	protected $_loader;



	private   $_token;

	private   $_timegenerated;

	private   $_validuntil;



	private   $_uname;

	private   $_pwd;



	private   $SESSION;



	function __construct()

	{

		$this->_loader =& get_instance();



		$this->generateToken();

	}



	private function generateToken()

	{

		$this->_token 			= md5(md5(date('Y-m-d H:i:s'))) . md5(time());

		$this->_timegenerated 	= date("Y-m-d H:i:s");

		$this->_validuntil 		= date("Y-m-d H:i:s", strtotime("+10 hour"));

	}



	public function getAuth($param = array())

	{

		$this->_uname = !empty($param["uname"]) ? $param["uname"] : "";

		$this->_pwd   = !empty($param["pwd"]) ? $param["pwd"] : "";



		$result = $this->cekUserTerdaftar()->get();



		return $result;

	}



	private function cekUserTerdaftar()

	{

		$model  = $this->_loader;

		$model->load->model("Nbauths_model");



		$result = $model->Nbauths_model->getUserTerdaftar($this->_uname,$this->getCustomHash($this->_pwd));



		return  $result;

	}



	public function registerUsersAuth($data = array()){

		$model  = $this->_loader;

		$model->load->model("Nbauths_model");



		$param = array(

					"token_code"  => $this->_token,

					"token_gnrtd" => $this->_timegenerated,

					"token_valid" => $this->_validuntil

				);



		$model->Nbauths_model->setGeneratedToken($param, $data->uname, $data->pwd);



		$this->setSessionAuth($data, $param);

	}



	private function setSessionAuth($data = array(), $param = array()){

		$newdata = array(

				"usr_id"	 		 => $data->id,

				"usr_fname"  		 => $data->fname,

				"usr_pwd"  		 	 => $data->pwd,

				"usr_uname"  		 => $data->uname,

				"usr_stat"   		 => $data->status??'',

				"usr_last_on"   	 => $data->last_online,

				"usr_login_ip"       => GET_IP,

				"usr_login_date"     => $data->last_login_date,

				"usr_role_id"        => $data->role_id,

				"usr_role_name"      => $data->role_name,

				"usr_tkn_code"       => $param["token_code"],

				"usr_tkn_gentime"    => $param["token_gnrtd"],

				"usr_tkn_valtime"    => $param["token_valid"]

			);



		$this->_loader->session->set_userdata($newdata);

	}



	public function getCustomHash($plainText = ""){

		return $this->setCustomHash($plainText);

	}



	private function setCustomHash($plainText, $salt = null)

	{

		$secretkey = "2c7042a2e8411121d192aaa7809a79be";



	    if ($salt === null)

	    {

	        $salt = substr($secretkey, 0, SALT_LENGTH);

	    }

	    else

	    {

	        $salt = substr($salt, 0, SALT_LENGTH);

	    }



	    return $salt . md5(base64_encode(sha1($salt . md5($plainText))));

	}



	public function cauth(){

		$model  = $this->_loader;

		$model->load->model("Nbauths_model");



		$this->SESSION = $this->_loader->session->userdata();



		if(empty($this->SESSION["usr_uname"]) &&  empty($this->SESSION["usr_pwd"])){

			redirect('cms/login');

		}else{

			$token = $this->SESSION['usr_tkn_code'];

			$uname = $this->SESSION['usr_uname'];

			$pwd   = $this->SESSION['usr_pwd'];



			$result = $model->Nbauths_model->cekValidToken($token, $uname, $pwd)->get();



			if($result->num_rows() > 0){

				$result     = $result->row();

				$now   		= new DateTime(date("Y-m-d H:i:s"));

				$validtime	= new DateTime($result->token_valid);



				if($now > $validtime){

					redirect('cms/token/expired');

				}

			}else{

				$this->_loader->session->set_flashdata('error', INCORRECT_UNAMEPWD);

				redirect('/cms');

			}

		}

	}



	public function cauth_dt(){

		$model  = $this->_loader;

		$model->load->model("Nbauths_model");



		$this->SESSION = $this->_loader->session->userdata();



		if(empty($this->SESSION["usr_uname"]) &&  empty($this->SESSION["usr_pwd"]) && empty($this->SESSION["usr_tkn_code"])){

			return false;

		}else{

			$token = $this->SESSION['usr_tkn_code'];

			$uname = $this->SESSION['usr_uname'];

			$pwd   = $this->SESSION['usr_pwd'];



			$result = $model->Nbauths_model->cekValidToken($token, $uname, $pwd)->get();



			if($result->num_rows() > 0){

				$result     = $result->row();

				$now   		= new DateTime(date("Y-m-d H:i:s"));

				$validtime	= new DateTime($result->token_valid);



				if($now > $validtime){

					return false;

				}

			}

		}



		return true;

	}



	public function abp($token = '', $type = '', $ctrlname = '', $action = ''){

		$model  = $this->_loader;

		$model->load->model("Nbauths_model");



		$param = array(

						"token" 	=> $token, 

						"type" 		=> $type, 

						"ctrl_name" => $ctrlname, 

						"action" 	=> $action,

						"user_id"	=> $this->_loader->session->userdata("usr_id")

					);



		$result = $model->Nbauths_model->cekValidABP($param);



		if($result<= 0){

			redirect('/cms/error/error_permition');

		}	

	}


	public function cekMail($mail = '')
	{
		$model  = $this->_loader;

		$model->load->model("Nbauths_model");

		$result = $model->Nbauths_model->ifMailExistt($mail);

		return $result;
	}
}