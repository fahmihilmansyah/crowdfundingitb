<?php



class Librariesloader{

	protected $loader;



	public function __construct($param){

		$this->loader =& get_instance();



		if(!empty($param) && is_array($param))

		{

			foreach ($param as $key => $value) 

			{

				$this->loader->load->library($value);

			}

		}

		else

		{

			$this->loader->load->library($param);

		}

	}

}