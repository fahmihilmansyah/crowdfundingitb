<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Nbmenus{



	/**

	 * ----------------------------------------

	 * #NB Menus Libraries

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	protected $_loader;



	private $SESSION;



	function __construct()

	{

		$this->_loader =& get_instance();



		$this->SESSION = $this->_loader->session->userdata();

	}



	public function getMenu($roleid = '1')

	{        

	    $html   = '<ul class="nav side-menu">';      



	    $role_id = (isset($this->SESSION["usr_role_id"])) ? $this->SESSION["usr_role_id"] : 0; 

	    

	    $html   .= $this->getMenuItem(0,$role_id);

	    

	    $html  .= '</ul>'; 



	    return $html;

	}



	private function getMenuItem($id = '', $roleid = '') 

	{

	        $sql = "SELECT

	                    *, my_menu.name as menu_name, my_menu.id as menu_id

	                FROM

	                    (

	                        SELECT

	                            `a`.*, `child`.`have_child`

	                        FROM

	                            (`nb_sys_menus` a)

	                        LEFT OUTER JOIN(

	                            SELECT

	                                parent,

	                                COUNT(*)AS have_child

	                            FROM

	                                `nb_sys_menus`

	                            GROUP BY

	                                parent

	                        )child ON `a`.id = `child`.parent

	                    )my_menu

	                JOIN `nb_sys_roles_access` b ON `b`.`menu_id` = `my_menu`.`id`

	                JOIN `nb_sys_roles` c ON `c`.`id` = `b`.`role_id` 

	                WHERE 

	                    `my_menu`.`parent` = '{$id}' 

	                AND `my_menu`.`is_active` = '1'

	                AND

	                    `b`.`role_id`   = '{$roleid}'

	                ORDER BY `my_menu`.`position` ASC

	                ";





	        $query    = $this->_loader->db->query($sql);

	        $totalRow = $query->num_rows();

	       

	        $html = "";

	        if($totalRow > 0){

	            $i = 1;

	            foreach($query->result() as $rs) {

	                

	                $url = explode("|", $rs->url);



	                if($rs->have_child > 0){

	                    $html .= '<li>

	                                <a>

	                                  <i class="fa ' . $rs->icon . '"></i>';

	                    $html .= $rs->menu_name;

	                    $html .= '    <span class="fa fa-chevron-down"></span>

	                                </a>';



	                    $html .= '<ul class="nav child_menu">';

	                    $html .= $this->getMenuItem($rs->menu_id,$rs->role_id);  

	                    $html .= '</ul>';

	                    $html .= '</li>';       

	                }else if($rs->have_child == null || $rs->have_child == "" ){

	                    $html .= '<li>';

	                    $html .= '<a href="' . base_url() . "cms/". $rs->url . '">' . $rs->menu_name . '</a>';

	                    $html .= '</li>';

					}else{



	                }

	            }		    

	        }



	        return $html;

	}

}