<?php
/**
 * Created by PhpStorm.
 * Project : berzakat
 * User: fahmihilmansyah
 * Date: 26/02/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 10.39
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */

class Fhhlib
{
    protected $_loader;
    protected $db;

    function __construct()

    {
        $this->_loader =& get_instance();
        $this->db = $this->_loader->db;
    }

    static function createSignature($arrParam, $key)
    {
        ksort($arrParam, 0);
        $signature = hash('sha256', strtoupper(implode("%", $arrParam)) . "%" . $key);;
        return $signature;
    }

    static function E2Pay_signature($source)
    {
        return base64_encode(hex2bin(sha1($source)));
    }

    static function e2payIPG($arrParam)
    {
        $postfields = http_build_query($arrParam);
//        echo "<pre>";print_r($arrParam);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, "https://sandbox.e2pay.co.id/epayment/entry.asp");
//        curl_setopt($ch, CURLOPT_URL, "https://payment.e2pay.co.id/epayment/entry.asp");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;
//            print_r($response);exit;
            $response = json_decode($response, true);
            if ($response['status_code'] == '00') {
                return $response['landing_page'];
            } else {
                return false;
            }
            print_r($response);
        }
    }

    static function updateSaldo($donatur = '', $nominal = 0, $no_transaksi = '')
    {
        $cquery = new Fhhlib();
        $carisaldo = $cquery->custom_query('nb_donaturs_saldo', ['where' => ['donatur' => $cquery->db->escape($donatur)]])->result();
        if (!empty($carisaldo)) {
            $dataupd = ['prevBalance' => $carisaldo[0]->balanceAmount, 'balanceAmount' => $carisaldo[0]->balanceAmount - $nominal];
            $datawhere = ['donatur' => $donatur];
            $cquery->custom_update('nb_donaturs_saldo', $dataupd, $datawhere);

            $data = array(
                'donaturs' => $donatur,
                'jenistrx' => 'TRX',
                'id_transaksi' => $no_transaksi,
                'prevBalance' => $carisaldo[0]->balanceAmount,
                'amount' => $nominal,
                'balance' => $carisaldo[0]->balanceAmount - $nominal,
                'crtd_at' => date('Y=m-d H:i:s'),
                'edtd_at' => date('Y=m-d H:i:s'),
            );

            $cquery->db->insert('nb_donaturs_trx', $data);
        }
    }

    static function kirimnotif($no_transaksi, $jenistrx, $namaDonatur, $emailDonatur, $nominal, $tgl_lahir,$nopolisasyki = '')
    {
        $tgl_skr = date('Y-m-d');
        $dataisi['invoice'] = $no_transaksi;
        $dataisi['tgl_trx'] = $tgl_skr;
        $dataisi['jenistrx'] = $jenistrx;
        $dataisi['fullname'] = $namaDonatur;
        $dataisi['senderName'] = 'MumuApps - System';
        $dataisi['emailTo'] = $emailDonatur;
        $dataisi['subject'] = 'Invoice Transaksi';
        $dataisi['amount'] = $nominal;
        $dataisi['no_polis'] = $nopolisasyki;
        $dataisi['tgl_lahit'] = $tgl_lahir;
        $dataisi['konten'] = 'trxipg/invoice1';
        sendmail::SendMail($dataisi);
    }

    static function updateTrans($tables = '', $no_transaksi = '', $status = 'unverified')
    {
        $cquery = new Fhhlib();
        $caritrans = $cquery->custom_query($tables, ['where' => ['no_transaksi' => $cquery->db->escape($no_transaksi)]])->result();
        if (!empty($caritrans)) {
            $dataupd = ['status' => $status, 'edtd_at' => date('Y-m-d H:i:s')];
            $datawhere = ['no_transaksi' => $no_transaksi];
            $cquery->custom_update($tables, $dataupd, $datawhere);
            if (!empty($caritrans[0]->campaign)) {
                $query = $cquery->db->query("UPDATE nb_campaign SET 
                    now = now + " . $caritrans[0]->nomuniq . " , sum_donatur = sum_donatur + 1, edtd_at= now() where id='" . $caritrans[0]->campaign . "'");
            }
        }
    }

    function otherProses($table, $jenistrx, $type)
    {

        $whereprogram['where'] = array('kategoricode' => $this->db->escape($jenistrx));
        $cariprog = $cquery->custom_query('nb_trans_program_kategori', $whereprogram)->result();
        $jenistrx = '';
        if (!empty($cariprog)):
            $jenistrx = $cariprog[0]->kategoriname;
            $expl = explode("-", $jenistrx);
            if (count($expl) == 3 && $cariprog[0]->type == 'ASURANSI_PERJALANAN') {
                $tgl_lahir = $gettrxcampaign[0]->tgl_lahir;
                $dataisipolis['paket'] = $expl[2];
                $dataisipolis['trx_id'] = $gettrxcampaign[0]->no_transaksi;
                $dataisipolis['nik'] = $gettrxcampaign[0]->no_ktp;
                $dataisipolis['nama'] = $gettrxcampaign[0]->namaDonatur;
                $dataisipolis['tgl_lahir'] = $gettrxcampaign[0]->tgl_lahir;
                $dataisipolis['jenis_kelamin'] = $gettrxcampaign[0]->jk;
                $dataisipolis['email'] = $gettrxcampaign[0]->emailDonatur;
                $dataisipolis['no_ponsel'] = $gettrxcampaign[0]->telpDonatur;
                $dataisipolis['nominal'] = $gettrxcampaign[0]->nomuniq;
                $dataisipolis['masa_asuransi'] = $expl[1];
                $polisasyki = $cquery->konekpolis($dataisipolis);
                $polisasyki = json_decode($polisasyki, true);
                $nopolisasyki = !empty($polisasyki['data']['certificate_no']) ? $polisasyki['data']['certificate_no'] : '';
                $updatetrx['no_polis'] = $nopolisasyki;
            }
            if (count($expl) == 2 && $cariprog[0]->type == 'PPOB') {
                $postdata = ['grant_type' => 'password',
                    'username' => 'mumuweb1', 'password' => 'mumuweb123'];
                $HEADER = ['authorization: Basic bG9jYWxkZXY6dDYxRklMNUJuV2U3Wmp5cDlncTNFWE1KaERZ',
                    'content-type: application/x-www-form-urlencoded'];
                $postdata = http_build_query($postdata);
                $token = $cquery->_curl_exexc("http://35.240.184.113/am/mydomain/oauth/token", $postdata, 'POST', $HEADER);
                $token = json_decode($token, true);
                $access_token = $token['access_token'];
                $HEADER = ['authorization: Bearer ' . $access_token,
                    'content-type: application/json'];
                $postdata = [
                    'fsp' => 'multibiller-hike',
                    'id' => 'mumuweb1',
                    'props' => array(
                        'callback' => base_url('trxipg/responseppob'),
                        'prodId' => $expl[1],
                        'destination' => $gettrxcampaign[0]->telpDonatur,
                    ),
                ];
                $postdata = json_encode($postdata);
                $datapulsa = $cquery->_curl_exexc("http://34.87.70.137/gateway", $postdata, 'POST', $HEADER);
                $insdt = array(
                    'id' => Fhhlib::uuidv4(),
                    'request_json' => json_encode($postdata),
                    'response_json' => $datapulsa,
                    'created_ad' => $tgl_skr,
                    'no_transaksi' => $gettrxcampaign[0]->no_transaksi
                );
                $cquery->db->insert('nb_trans_ppob_request', $insdt);
            }
            endif;
        }
    function konekpolis($data = null)
    {
        $url = 'http://194.31.53.26/ddipg/public/asuransi/asyki';//prod
        $ch = curl_init();
        $awal = $data;
        $datapack = http_build_query($awal);
        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            $datapack);
        // return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // $output contains the output string
        $output = curl_exec($ch);
        // tutup curl
        curl_close($ch);
//        insertlog json
//        Yii::$app->db->createCommand()->insert('log_json', ['trx_id' => $data['trx_id'], 'res_json' => $output, 'send_json' => json_encode($data), 'created_at' => date("Y-m-d H:i:s")])->execute();
        // menampilkan hasil curl
        return $output;
    }
    private function _curl_exexc($url = '', $postdata = '', $METHOD = 'POST', $HEADER = [])
    {
//        $postdata = http_build_query($data);
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADER);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }
    static function finpayIPG($arrParam)
    {
        $cquery = new Fhhlib();
        $cariipg = $cquery->custom_query('nb_partner_ipg', ['where' => ['id' => $cquery->db->escape('FINPAY')]])->result();
//        print_r($cariipg);exit;
//    function finpayIPG($amount,$email,$cust_id,$cust_msisdn,$cust_name,$invoice){

//        $key="21-DD2194-79";
        $key = !empty($cariipg[0]->key) ? $cariipg[0]->key : "n9AWX2Hftt";
        $merchant_id = !empty($cariipg[0]->merchant_key) ? $cariipg[0]->merchant_key : "DD2194";
//        $sofid = !empty($cariipg[0]->sod_id)?$cariipg[0]->sod_id:"cc";
//        $arrParam['sof_id']=$sofid;
//        $arrParam2=array(
//            "amount" => $amount,
//            "cust_email" => $email,
//            "cust_id" => 1234849,//$cust_id,
//            "cust_msisdn" => $cust_msisdn,
//            "cust_name" => $cust_name,
//            "failed_url" => "http://194.31.53.26/test/failed.php",
//            "invoice" => $invoice,
//            "merchant_id" => $merchant_id,
//            "items" => '[["Donasi",'.$amount.',1]]',
//            "return_url" => "http://194.31.53.26/test/return.php",
//            "success_url" => "http://194.31.53.26/test/success.php",
//            "timeout" => "120", //in minutes
//            "trans_date" => date("YmdHis"),
//            "add_info1" => "Pembayaran Donasi Ganeshabisa",
//            // "add_info2" => "dua",
//            // "add_info3" => "tiga",
//            // "add_info4" => "empat",
//            // "add_info5" => "lima",
//            "sof_id" => "cc",
//            "sof_type" => "pay"
//        );
        $arrParam1 = array(
            "amount" => "1501",
            "cust_email" => "fahmi.hilmansyah@gmail.com",
            "cust_id" => "282813888",
            "cust_msisdn" => "0817170820",
            "cust_name" => "Fahmi Hilmansyah",
            "failed_url" => "http://194.31.53.26/test/failed.php",
            "invoice" => "INV12300021",
            "merchant_id" => $merchant_id,
            "items" => '[["Donasi",1501,1]]',
            "return_url" => "http://194.31.53.26/test/return.php",
            "success_url" => "http://194.31.53.26/test/success.php",
            "timeout" => "120", //in minutes
            "trans_date" => date("YmdHis"),
            "add_info1" => "Pembayaran Donasi Ganeshabisa",
            // "add_info2" => "dua",
            // "add_info3" => "tiga",
            // "add_info4" => "empat",
            // "add_info5" => "lima",
            "sof_id" => "mandiriclickpay",
            "sof_type" => "pay"
        );
        $arrParam['merchant_id'] = $merchant_id;
        $idipg = $arrParam['idipg'];
        unset($arrParam['idipg']);
        //call the function, and Voila!, this is the correct signature
        $signature = Fhhlib::createSignature($arrParam, $key);
        $arrParam = array_merge($arrParam, array("mer_signature" => $signature));
// exit;

//        exit;
//Post the data
//        $urlfinpay = "https://sandbox.finpay.co.id/servicescode/api/pageFinpay.php";
//        $urlfinpay = "https://billhosting.finnet-indonesia.com/prepaidsystem/api/apiFinpay.php";
        $urlfinpay = !empty($cariipg[0]->url_host) ? $cariipg[0]->url_host : "https://sandbox.finpay.co.id/servicescode/api/pageFinpay.php";
        $postfields = http_build_query($arrParam);
//        echo "<pre>";
//        echo "URL HOST : ".$urlfinpay ;
//        echo "<br>";
//        echo "==================";
//        echo "REQUEST";
//        echo "<br>";
//        print_r($arrParam);
//        echo json_encode($arrParam);
//        echo "<br>";
//        echo "==================";
//        echo "<br>";
//        echo "RESPONSE";
//        echo "<br>";
//        echo "<pre>";print_r($arrParam);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $urlfinpay);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
//            print_r($response);exit;
            $dataupdate = array('request_json' => json_encode($arrParam), 'response_json' => $response);
            $datawhere = array('id' => $idipg);
            $cquery->custom_update('nb_trans_ipg', $dataupdate, $datawhere);
            $response = json_decode($response, true);
//            if($response['status_code'] == '00'){
//                return $response['landing_page'];
//            }else{
            return $response;
//            }
            print_r($response);
        }
    }

    static function devfinpayIPG($arrParam)
    {
        $cquery = new Fhhlib();
        $cariipg = $cquery->custom_query('nb_partner_ipg', ['where' => ['id' => $cquery->db->escape('MUPAYS')]])->result();
        $urlfinpay = $cariipg[0]->url_host;//'http://194.31.53.26/ddipg/public/trx/request';
        $signature = hash_hmac('sha256', $arrParam['invoice'] . '|' . $arrParam['amount'] . '|' . $arrParam['cust_email'] . '|' . $arrParam['cust_msisdn']  . '|' .$arrParam['sof_id'] , $cariipg[0]->key);
        $arrParam['signature'] = $signature;
        $postfields = http_build_query($arrParam);
//        echo "<pre>";
//        echo "URL HOST : ".$urlfinpay ;
//        echo "<br>";
//        echo "==================";
//        echo "REQUEST";
//        echo "<br>";
//        print_r($arrParam);
//        echo json_encode($arrParam);
//        echo "<br>";
//        echo "==================";
//        echo "<br>";
//        echo "RESPONSE";
//        echo "<br>";
//        echo "<pre>";print_r($arrParam);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $urlfinpay);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Pauthorization: DDTEKNOPAYMENT'
        ));
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
//            print_r($response);exit;
            $dataupdate = array('request_json' => json_encode($arrParam), 'response_json' => $response);
            $datawhere = array('id' => $arrParam['idipg']);
            $cquery->custom_update('nb_trans_ipg', $dataupdate, $datawhere);
            $response = json_decode($response, true);
//            if($response['status_code'] == '00'){
//                return $response['landing_page'];
//            }else{
            return $response;
//            }
            print_r($response);
        }
    }

    public static function uuidv4()
    {
        return time() . "-" . sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),

                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),

                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,

                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,

                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
            );
    }

    public static function uuidv4Nopad()
    {
        return time() . sprintf('%04x%04x%04x%04x%04x%04x%04x%04x',

                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),

                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),

                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,

                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,

                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
            );
    }

    function custom_query($table = null, $condition = array(), $select = "*")
    {
        $this->db->select($select);
        $this->db->from($table);
        if (is_array($condition)) {
            if (count($condition) > 0) {
                foreach ($condition as $k => $v) {
                    if ($k == 'where') {
                        $this->db->where($v, null, false);
                    }
                    if ($k == 'or_where') {
                        $this->db->or_where($v, null, false);
                    }
                    if ($k == 'join') {
                        if (isset($v['table'])) {
                            $this->db->join($v['table'], $v['condition'], $v['type']);
                        } else {
                            foreach ($v as $value) {
                                $this->db->join($value['table'], $value['condition'], $value['type']);
                            }
                        }
                    }
                    if ($k == 'order_by') {
                        if (isset($v['field'])) {
                            $this->db->order_by($v['field'], $v['type']);
                        } else {
                            foreach ($v as $value) {
                                $this->db->order_by($value['field'], $value['type']);
                            }
                        }
                    }
                    if ($k == 'group_by') {
                        $this->db->group_by($v);
                    }
                    if ($k == 'limit') {
                        $this->db->limit($v['row'], $v['offset']);
                    }
                }
            }
        }
        return $this->db->get();
    }

    function custom_update($table = null, $data = array(), $where = array())
    {
        $this->db->trans_begin();
        $this->db->where($where);
        $this->db->update($table, $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            die("ERROR: UPDATING DATA, PLEASE CONTACT ADMINISTRATOR WEB");
        } else {
            $this->db->trans_commit();
        }
    }
    static function geninc()

    {
        $cquery = new Fhhlib();
        $kondisi['where'] = array('id' => $cquery->db->escape(1));
        $generate = $cquery->custom_query("nb_inc", $kondisi)->result();
        $tgl = $generate[0]->tgl;
        $uniqinc = 0;
        if ($tgl != date("Y-m-d")) {
            $uniqinc = $uniqinc + 1;
            //echo $uniqinc."||".$tgl;
        } else {
            $uniqinc = $generate[0]->inc + 1;
            //echo "else : ".$uniqinc;
        }
        $cquery->custom_update("nb_inc", array('inc' => $uniqinc, 'tgl' => date("Y-m-d")), array('id' => $cquery->db->escape(1)));
        return $uniqinc;

    }
    static function _curl_exexc_with_header($url = '', $postdata = '', $METHOD = 'POST', $HEADER = [])
    {
//        $postdata = http_build_query($data);
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADER);

        // $output contains the output string
        $output = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }
}