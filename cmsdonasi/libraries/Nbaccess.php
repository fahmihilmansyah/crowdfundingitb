<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Nbaccess{



	/**

	 * ----------------------------------------

	 * #NB Access Libraries

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	protected $_loader;



	function __construct()

	{

		$this->_loader =& get_instance();

	}



	public function getMenuId()

	{

		$url = $this->_loader->uri->segment(2);



		$model  = $this->_loader;

		$model->load->model("Manajemenakses_model","MAkses");



		$menuid = $model->MAkses->get_menu_id($url);



		return $menuid;

	}



	public function getAccess($type = '')

	{

		$model  = $this->_loader;

		$model->load->model("Manajemenakses_model","MAkses");



		$menuid = $this->getMenuId();

		$roleid = $this->_loader->session->userdata("usr_role_id");



		$sql = '';

		switch ($type) {

			case "add":

				return $model->MAkses->getStatMenu($menuid, $roleid, "can_add");

				break;



			case "edit":

				return $model->MAkses->getStatMenu($menuid, $roleid, "can_edit");

				break;

			

			case "delete":

				return $model->MAkses->getStatMenu($menuid, $roleid, "can_delete");

				break;



			case "print":

				return $model->MAkses->getStatMenu($menuid, $roleid, "can_print");

				break;



			case "approve":

				return $model->MAkses->getStatMenu($menuid, $roleid, "can_aprove");

				break;



			case "import":

				return $model->MAkses->getStatMenu($menuid, $roleid, "can_import");

				break;



			case "export":

				return $model->MAkses->getStatMenu($menuid, $roleid, "can_export");

				break;



			default:

				return 0;

				break;

		}

	}

}