<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Nbauth_front{

	/**
	 * ----------------------------------------
	 * #NB Auth Libraries
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	protected $_loader;

	private $_login_stat = false;

	private $_SESSION;

	function __construct()
	{
		$this->_loader =& get_instance();

		$this->_loader->load->model("authentication/Nbauth_model", "auth");
	}

	public function lookingForAuten()
	{
		if(!empty($_SESSION[LGI_KEY . "login_info"]))
		{
			if($_SESSION[LGI_KEY . "login_info"]["login_status"])
			{
				$data["token"]    = $_SESSION[LGI_KEY . "login_info"]["login_token"];
				$data["uid"]   	  = $_SESSION[LGI_KEY . "login_info"]["login_uid"];
				$data["uip"]   	  = $_SESSION[LGI_KEY . "login_info"]["login_ip"];

				$cekToken = $this->_loader->auth->cekToken($data)->num_rows();

				if($cekToken > 0)
				{
					return true;
				}
				else
				{
					// echo "<PRE>";
					// print_r($_SESSION[LGI_KEY . "login_info"]);
					// die("salah token");
					redirect('auten/page/login');
				}
			}
			else
			{
				// die("salah session login status");
				redirect('auten/page/login');
			}
		}
		else
		{
			// die("salah session smw");
			redirect('auten/page/login');
		}
	}

	public function getAuthDefault($email = '', $pwd = '', $type = '')
	{
		$param['email'] = $email;
		$param['pwd']	= $pwd;

		$this->systemAuth($param);

		if(!empty($_SESSION[LGI_KEY . "login_info"])){
			if($_SESSION[LGI_KEY . "login_info"]["login_status"])
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	private function systemAuth($param = array())
	{
		$existsUser = $this->_loader->auth->existsUser($param);

		if($existsUser->num_rows() > 0)
		{
			$user_data  = $existsUser->row();
			
			$token 		= md5($param["email"] . $param["pwd"] . md5(strtotime(date("ymdhis"))) . md5(time()));
			$validdate 	= date("Y-m-d H:i:s", strtotime("+1 hour"));

			$this->_loader->auth->insert_token($token, $validdate, $user_data->id);

			$sess_data[LGI_KEY . "login_info"] = array(
													"userid" 		=> $user_data->id,
													"fname" 		=> $user_data->fname,
													"phone" 		=> $user_data->phone,
													"email" 		=> $user_data->email,
													"statLogin" 	=> true,
													"login_uid" 	=> $user_data->id,
													"login_fname" 	=> $user_data->fname,
													"login_lname" 	=> $user_data->lname,
													"login_email" 	=> $user_data->email,
													"login_laston" 	=> $user_data->last_online,
													"login_ip"	 	=> $_SERVER['REMOTE_ADDR'],
													"login_date" 	=> $user_data->last_login_date,
													"login_type"    => "default",
													"login_sex"     => $user_data->sex,
													"login_status"  => true,
													"login_token"   => $token,
													"mupays_token"   => $user_data->oauth_mupays,
												 );

			$this->_loader->session->set_userdata($sess_data);

			return true;
		}
		else
		{
			return false;
		}
	}

	private function apiAuth($param = array())
	{

	}

}