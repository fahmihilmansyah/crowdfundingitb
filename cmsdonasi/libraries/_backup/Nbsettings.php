<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Nbsettings{



	/**

	 | ----------------------------------------

	 | #NB Settings - Libraries

	 | ----------------------------------------

	 | #author 		    : Fahmi Hilmansyah

	 | #time created    : 4 January 2017

	 | #last developed  : Fahmi Hilmansyah

	 | #last updated    : 4 January 2017

	 | ----------------------------------------

	 */



	protected $loader;



	private $favicon;

    private $plugin_list 		= array();

    private $temp_plugin 		= array();

    private $list_plugin_js 	= array();

    private $list_plugin_css 	= array();

    private $list_js 			= array();

    private $list_css 			= array();

    private $list_layout 		= array();

    private $path_asset;



    private $_templateBasePath = "assets/";



	function __construct()

    {

		$this->loader =& get_instance();

		$this->config();

	}



   private function config() 

   {

        $this->path_asset = base_url() . $this->_templateBasePath;



        /*

         * Daftar Plugin

         * Format untuk mendaftarkan plugin

         * array(

         * 	  'nama_plugin' => array(

         * 			'js' => array(

         * 				'nama_file_js1',

         * 				'nama_file_js2'

         * 			),

         * 			'css' => array(

         * 				'nama_file_css1',

         * 				'nama_file_css2'

         * 			)

         * 	   )

         * )

         */

        

        $this->plugin_list = array(

            'main' =>array(

                'css'=>array(

                    'vendors/bootstrap/dist/css/bootstrap.min',

                    'vendors/font-awesome/css/font-awesome.min',

                    'vendors/nprogress/nprogress',

                    'vendors/iCheck/skins/flat/green',

                    'vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min',

                    'vendors/bootstrap-daterangepicker/daterangepicker',

                    'vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min',

                    'build/css/custom.min',

                    'vendors/datatables.net-bs/css/dataTables.bootstrap',

                    'vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min',

                    'vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min',

                    'vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min',

                    'vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min',

                    'css/ydsf.cpanel.custom',

                    'vendors/pnotify/dist/pnotify',

                    'vendors/pnotify/dist/pnotify.buttons',

                    'vendors/pnotify/dist/pnotify.nonblock',

                    'js/sweetalert/sweetalert',

                    'js/jqtreegrid/css/jquery.treegrid',

                    'js/select2-3.5.2/select2',

                    'js/select2-3.5.2/select2-bootstrap',

                    'js/jqui/jquery-ui.min',

                    'js/jqui/jquery-ui.theme.min',

                    'js/jquerytagit/css/jquery.tagit',

                    'js/jquerytagit/css/tagit.ui-zendesk',

                    'js/highchart/css/highcharts'

                ),

                'js'  => array(

                	'js/jquery-1.11.1.min',

                    'js/jqui/jquery-ui.min',

                	'vendors/bootstrap/dist/js/bootstrap.min',

                	'vendors/fastclick/lib/fastclick',

                	'vendors/nprogress/nprogress',

                	'vendors/Chart.js/dist/Chart.min',

                	'vendors/gauge.js/dist/gauge.min',

                	'vendors/bootstrap-progressbar/bootstrap-progressbar.min',

                	'vendors/iCheck/icheck.min',

                	'vendors/skycons/skycons',

 					'vendors/DateJS/build/date',

    				'vendors/moment/min/moment.min',

    				'vendors/bootstrap-daterangepicker/daterangepicker',

                    'vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min',

    				'build/js/custom.min',

                    'vendors/datatables.net/js/jquery.dataTables',

                    'vendors/datatables.net-bs/js/dataTables.bootstrap',

                    'vendors/datatables.net-buttons/js/dataTables.buttons',

                    'vendors/datatables.net-buttons-bs/js/buttons.bootstrap',

                    'vendors/datatables.net-buttons/js/buttons.flash',

                    'vendors/datatables.net-buttons/js/buttons.html5',

                    'vendors/datatables.net-buttons/js/buttons.print',

                    'vendors/datatables.net-fixedheader/js/dataTables.fixedHeader',

                    'vendors/datatables.net-keytable/js/dataTables.keyTable',

                    'vendors/datatables.net-responsive/js/dataTables.responsive',

                    'vendors/datatables.net-responsive-bs/js/responsive.bootstrap',

                    'vendors/datatables.net-scroller/js/datatables.scroller',

                    'js/bootbox.min',

                    'js/ydsf.custom',

                    'vendors/pnotify/dist/pnotify',

                    'vendors/pnotify/dist/pnotify.buttons',

                    'vendors/pnotify/dist/pnotify',

                    'vendors/pnotify/dist/pnotify.nonblock',

                    'js/sweetalert/sweetalert.min',

                    'vendors/fastclick/lib/fastclick',

                    'js/exif',

                    'js/jqvalidate/dist/jquery.validate.min',

                    'js/jqvalidate/dist/jquery.validate.file',

                    'js/jqtreegrid/js/jquery.treegrid',

                    'js/jqtreegrid/js/jquery.treegrid.bootstrap3',

                    'js/select2-3.5.2/select2.min',

                    'js/ckeditor/ckeditor',

                    'js/jquerytagit/js/tag-it',

                    'js/autoNumeric',

                    'js/highchart/highcharts',

                    'js/highchart/highcharts-3d',

                    'js/highchart/modules/data',

                    'js/highchart/modules/drilldown',

                ),

            ),

            'front' =>array(

                'css'=>array(

                    'css/ydsf.custom.fonts',

                    'vendors/bootstrap/dist/css/bootstrap.min',

                    'vendors/bootstrap/dist/css/bootstrap-social',

                    'vendors/font-awesome/css/font-awesome.min',

                    'css/ydsf.navbar',

                    'css/ydsf.main.konten',

                    'css/ydsf.custom.login',

                    'css/ydsf.custom.pager',

                    'css/ydsf.custom.slider',

                    'css/ydsf.custom.footer',

                    'css/ydsf.custom.progressbar',

                    'css/ydsf.custom.tabs',

                    'css/ydsf.custom.static',

                    'css/ydsf.custom.clearfix',

                    'css/ydsf.custom.uibutton',

                    'css/ydsf.custom.accordion',

                    'vendors/datatables.net-bs/css/dataTables.bootstrap',

                    'vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min',

                    'vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min',

                    'vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min',

                    'vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min',

                    'js/select2-3.5.2/select2',

                    'js/select2-3.5.2/select2-bootstrap',
                    'css/animate'

                ),

                'js'  => array(

                    'js/jquery-1.11.1.min',

                    'js/jqui/jquery-ui.min',

                    'vendors/bootstrap/dist/js/bootstrap.min',

                    'js/customscrollbar/jquery.mCustomScrollbar',

                    'vendors/datatables.net/js/jquery.dataTables',

                    'vendors/datatables.net-bs/js/dataTables.bootstrap',

                    'vendors/datatables.net-buttons/js/dataTables.buttons',

                    'vendors/datatables.net-buttons-bs/js/buttons.bootstrap',

                    'vendors/datatables.net-buttons/js/buttons.flash',

                    'vendors/datatables.net-buttons/js/buttons.html5',

                    'vendors/datatables.net-buttons/js/buttons.print',

                    'vendors/datatables.net-fixedheader/js/dataTables.fixedHeader',

                    'vendors/datatables.net-keytable/js/dataTables.keyTable',

                    'vendors/datatables.net-responsive/js/dataTables.responsive',

                    'vendors/datatables.net-responsive-bs/js/responsive.bootstrap',

                    'vendors/datatables.net-scroller/js/datatables.scroller',
                    
                    'js/select2-3.5.2/select2.min',

                    'js/jqvalidate/dist/jquery.validate.min',

                    'js/jqvalidate/dist/jquery.validate.file',

                    'js/ydsf.custom.front'

                ),

            ),

            'legalitas' =>array(

                'css'=>array(

                    'css/ydsf.custom.legalitas',

                ),

            ),

        );

    }



    private function reg_plugin($plugin_name) 

    {

        /*

         * Me'looping daftar plugin untuk mencocokan nama plugin yang dipanggil

         */

        $alpha = 0;

        foreach ($this->plugin_list as $key => $value) {

            if (!in_array($key, $this->temp_plugin)) {

                if ($key == $plugin_name) {

                    /*

                     * Mendaftarkan file javascript jika ada yang disertakan kedalam plugin

                     */

                    if (isset($value['js'])) {

                        $idx = 0;

                        foreach ($value['js'] as $js) {

                            if (!empty($js)) {

                                $index = str_pad($alpha, 4, "0", STR_PAD_LEFT) . str_pad($idx, 4, "0", STR_PAD_LEFT);

                                $this->list_plugin_js[$index] = $js . '.js';

                                $idx++;

                            }

                        }

                    }



                    /*

                     * Mendaftarkan file css jika ada yang disertakan kedalam plugin

                     */

                    if (isset($value['css'])) {

                        $idx = 0;

                        foreach ($value['css'] as $css) {

                            if (!empty($css)) {

                                $index = str_pad($alpha, 4, "0", STR_PAD_LEFT) . str_pad($idx, 4, "0", STR_PAD_LEFT);

                                $this->list_plugin_css[$index] = $css . '.css';

                                $idx++;

                            }

                        }

                    }



                    /*

                     * Jika nama plugin yang dipanggil sama dengan index name daftar plugin proses looping dihentikan

                     */

                    break;

                }

                $alpha++;

            } else {

                /*

                 * Jika nama plugin sudah terdaftar

                 */

                break;

            }

        }

    }



    public function set_plugin($plugin = array()) 

    {

        if (is_array($plugin)) {

            foreach ($plugin as $value) {

                $this->reg_plugin($value);

            }

        } else {

            if ($plugin == 'all') {

                foreach ($this->plugin_list as $key => $value) {

                    $this->reg_plugin($key);

                }

            } else {

                $this->reg_plugin($plugin);

            }

        }

    }



    public function get_js() 

    {

        $js_path = $this->path_asset;

        $list = "";



        $list_plugin = $this->list_plugin_js;

        

        ksort($list_plugin);

        foreach ($list_plugin as $value) {

            $list .= "<script src='" . $js_path . $value . "'></script>";

        }



        $list_js = $this->list_js;

        ksort($list_js);

        foreach ($list_js as $value) {

            $list .= "<script src='" . $js_path . $value . "'></script>";

        }



        return "<!-- Begin : Javacript -->" . $list . "<!-- End : Javacript -->";

    }



    public function get_css() 

    {

        $css_path = $this->path_asset;

        $list = "";



        $list_plugin = $this->list_plugin_css;

        ksort($list_plugin);

        foreach ($list_plugin as $value) {

            $list .= "<link  rel='stylesheet' href='" . $css_path . $value . "'>";

        }



        $list_css = $this->list_css;

        ksort($list_css);

        foreach ($list_css as $value) {

            $list .= "<link  rel='stylesheet' href='" . $css_path . $value . "'>";

        }



        return "<!-- Begin : CSS -->" . $list . "<!-- End : CSS -->";

    }



    public function set_css($css = array()) 

    {

        if (is_array($css)) {

            foreach ($css as $value) {

                $this->list_css[] = $value . '.css';

            }

        } else {

            $this->list_css[] = $css . '.css';

        }

    }



    public function set_js($js = array()) 

    {

        if (is_array($js)) {

            foreach ($js as $value) {

                $this->list_js[] = $value . '.js';

            }

        } else {

            $this->list_js[] = $js . '.js';

        }

    }

}