<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Manajemenakses extends NBAdmin_Controller {



	/**

	 * ----------------------------------------

	 * #ManajemenAkses Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_libList   = array();

	private $_modList   = array(

									'nbmenus_model' 		=> 'NBMenus',

									'manajemenakses_model' 	=> 'MAkses'

								);



	protected $_data      = array();



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------

		$this->modelsApp();

		// $this->libraryApp();

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	private function libraryApp()

	{

		$this->load->library('librariesloader', $this->_libList);

	}



	public function index($data = array())

	{

		$data 	= $this->_data;



        $this->MAkses->register_action_permition();



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Manajemen Akses', '/cms/settingakses');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["can_add"]	= $this->nbaccess->getAccess("add");

		$data["dsource"]	= base_url() . "cms/settingakses/json";



		$this->parser->parse("manajemenakses/index",$data);

	}



	public function json($roleid = '')

	{

		$default_order  = 'id';

		$order_field    = '';

		

		$order_field 	= array(

							'id',

							'name',

							'note',

							'is_active',

							'last_edit'

						 );

				

		$order_key 	= (!$this->input->get('iSortCol_0')) ? 0 : $this->input->get('iSortCol_0');

		$order 		= (!$this->input->get('iSortCol_0')) ? $default_order : $order_field[$order_key];

		$sort 		= (!$this->input->get('sSortDir_0')) ? 'DESC': $this->input->get('sSortDir_0');

		$search 	= (!$this->input->get('sSearch'))    ? '' : strtoupper($this->input->get('sSearch'));



		$limit 		= (!$this->input->get('iDisplayLength')) ? $this->_limit : $this->input->get('iDisplayLength');

		$start 		= (!$this->input->get('iDisplayStart'))  ? 0 : $this->input->get('iDisplayStart');



		$data['sEcho'] 			= (!$this->input->get('callback')) ? 0 : $this->input

																			  ->get('callback');



		$data['iTotalRecords'] 	= $this->MAkses->count_all($search,$order_field);

		

		$data['apps'] 			= $this->MAkses

									   ->get_paged_list($limit, $start, $order, $sort, $search, $order_field)

									   ->result_array();



		$data['akses']			= $this->nbaccess;



		$data['callback'] 		= $this->input->get('callback');



		$this->load->view('manajemenakses/data/json', $data);

	}



	private function form($id = '')

	{

		$data 	= $this->_data;



		if(!empty($id))

		{	

			$data["id"]	     	= $id;

			$key             	= array("id" => $id);

			$data["default"] 	= (array) $this->MAkses->get_data_form($key);

			$data["menulist"]   = $this->MAkses->showMenu($id);

		}else{

			$data["menulist"]   = $this->MAkses->showMenu();

		}



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Manajemen Akses', '/cms/settingakses');

		$this->breadcrumbs->push('Add New Data', '/cms/settingakses/new');



		$data["crumbs"]     = $this->breadcrumbs->show();

 		$data["action"]     = base_url() . '/cms/settingakses/save';



		$this->parser->parse("manajemenakses/form/form", $data);

	}



	public function add()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// cek valid token_proses

		$this->nbauth->abp($_key['addform']['token'], "GET", "ManajemenAkses", "addform");



		$this->form();

	}



	public function edit()

	{

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }

		

		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "ManajemenAkses", "editform");

		

		$this->form($param['id']);

	}



	public function read()

	{

		$data 	= $this->_data;



		// GET PARAMETER

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));

		

		if($param === false){ redirect('/cms/error/error_proccess'); }

		

        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Manajemen Akses', '/cms/settingakses');

		$this->breadcrumbs->push('View Data', '/cms/settingakses/view');



		$key              = array("id" => $param["id"]);

		$data["default"]  = (array) $this->MAkses->get_data_form($key);

		$data["crumbs"]   = $this->breadcrumbs->show();

		$data["menulist"] = $this->MAkses->showMenu($param["id"],true);



		$this->parser->parse("manajemenakses/view/view", $data);

	}



	public function simpan()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// get data from post

		$data = !empty($this->input->post("data")) ? $this->input->post("data") : array();

		

		// get id

		$id   = !empty($this->input->post("id")) ? $this->input->post("id") : '';



        $page = '';



        $this->db->trans_begin();



		if(empty($id))

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['saveasnew']['token'], "POST", "ManajemenAkses", "saveasnew");



			// add aditional data

			$data['crtd_at'] 	= date('Y-m-d H:i:s');

			$data['author']		= $this->session->userdata('usr_fname');



			$result = $this->MAkses->insert_to_db($data);

			

			if($result)

			{	



				$data = $this->input->post("list");

				$id   = $this->MAkses->get_max();

				$this->saveDetail($data, $id);

				

				$this->session->set_flashdata('success_notif', 'Data berhasil diinsert.');

				$page = '/cms/settingakses';

			}

		}

		else

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['save']['token'], "POST", "ManajemenAkses", "save");



			// add aditional data

			$data['edtd_at'] 	= date('Y-m-d H:i:s');

			$data['last_edit']	= $this->session->userdata('usr_fname');



			$result = $this->MAkses->update_to_db($data, array("id" => $id));

			if($result)

			{	





				$data = $this->input->post("list");

				$this->MAkses->delete_detail(array("role_id" => $id));

				$this->saveDetail($data, $id);



				$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

				$page = '/cms/settingakses';

			}

		}



        if ($this->db->trans_status() === FALSE) {

			$page = '/cms/error/error_proccess';



            $this->db->trans_rollback();

        } else {

            $this->db->trans_commit();

        }



		redirect($page);

	}



	private function saveDetail($data = array(), $roleid = ''){

		foreach ($data as $key => $value) {

			$detail["role_id"]  = !empty($roleid) ? $roleid : '';

			$detail["menu_id"]  = !empty($menu_id) ? $menu_id : $key;

			

			$detail["can_add"] 		= '0';

			if(array_key_exists("add", $value)){

				$detail["can_add"] = '1';

			}



			$detail["can_edit"] = '0';

			if(array_key_exists("edit", $value)){

				$detail["can_edit"] = '1';

			}



			$detail["can_delete"] = '0';

			if(array_key_exists("delete", $value)){

				$detail["can_delete"] = '1';

			}



			$detail["can_print"] = '0';

			if(array_key_exists("print", $value)){

				$detail["can_print"] = '1';

			}



			$detail["can_aprove"] = '0';

			if(array_key_exists("aprove", $value)){

				$detail["can_aprove"] = '1';

			}



			$detail["can_export"] = '0';

			if(array_key_exists("export", $value)){

				$detail["can_export"] = '1';

			}



			$detail["can_import"] = '0';

			if(array_key_exists("import", $value)){

				$detail["can_import"] = '1';

			}



			$this->MAkses->insert_to_db_detail($detail);

		}

	}



	public function delete()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "DELETE", "ManajemenAkses", "delete");



		$result = $this->MAkses->delete(array("id" => $param["id"]));



		if($result === FALSE)

		{

			redirect('/cms/error/error_proccess');

		}

		

		$this->session->set_flashdata('success_notif', 'Data berhasil dihapus.');



        redirect('/cms/settingakses');

	}



	public function aktifasi()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));

		// print_r($param); exit();

		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "ManajemenAkses", "activate");



		$data["is_active"] = $param["status"];

		$result = $this->MAkses->update_to_db($data,array("id" => $param["id"]));



		if($result === FALSE)

		{

			redirect('/cms/error/error_proccess');

		}



		if($this->session->userdata("usr_role_id") == $param["id"])

		{

			if($param["status"] == 0)

			{

				redirect('/cms/error/error_session');

			}

		}



		$this->session->set_flashdata('success_notif', 'Data berhasil diproses.');



        redirect('/cms/settingakses');

	}

}

