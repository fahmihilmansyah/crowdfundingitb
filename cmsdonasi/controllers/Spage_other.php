<?php



defined('BASEPATH') OR exit('No direct script access allowed');





class Spage_other extends NBAdmin_Controller {



	/**

	 * ----------------------------------------

	 * #spage_other Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_libList   	 = array();

	private $_modList   	 = array(

									'spage_other_model' 		=> 'SpageProgram',

								);

	

	protected $_data      	 = array();



	private $_ap_ori;

	private $_ap_thumb;

	private $_bs_ap;

	private $_bs_ap_tumb;



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------



		$this->modelsApp();

		// $this->libraryApp();



        // SET PATH IMAGE LOCATION

        $this->_ap_ori 		= FCPATH .'assets/images/spage_other/';

        $this->_ap_thumb 	= FCPATH .'assets/images/spage_other/new_thumb/';

        $this->_bs_ap		= base_url() . 'assets/images/spage_other/';

        $this->_bs_ap_tumb  = base_url() . 'assets/images/spage_other/';

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	private function libraryApp()

	{

		$this->load->library('librariesloader', $this->_libList);

	}







	public function index($data = array())

	{

		$data 	= $this->_data;



        $this->SpageProgram->register_action_permition();



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Halaman Statis ( Lainnya )', '/cms/spage_other');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["can_add"]	= $this->nbaccess->getAccess("add");

		$data["dsource"]	= base_url() . "cms/spage_other/json";



		$this->parser->parse("spage_other/index",$data);



	}







	public function json($roleid = ''){

		$default_order  = 'id';

		$order_field    = '';

		$order_field 	= array(

							'id',

							'title',

							'crtd_at',

							'edtd_at',

							'author',

							'edtd_at',

						 );



		$order_key 	= (!$this->input->get('iSortCol_0')) ? 0 : $this->input->get('iSortCol_0');

		$order 		= (!$this->input->get('iSortCol_0')) ? $default_order : $order_field[$order_key];

		$sort 		= (!$this->input->get('sSortDir_0')) ? 'DESC': $this->input->get('sSortDir_0');

		$search 	= (!$this->input->get('sSearch'))    ? '' : strtoupper($this->input->get('sSearch'));



		$limit 		= (!$this->input->get('iDisplayLength')) ? $this->_limit : $this->input->get('iDisplayLength');

		$start 		= (!$this->input->get('iDisplayStart'))  ? 0 : $this->input->get('iDisplayStart');



		$data['sEcho'] 			= (!$this->input->get('callback')) ? 0 : $this->input

																			  ->get('callback');



		$data['iTotalRecords'] 	= $this->SpageProgram->count_all($search,$order_field);



		$data['apps'] 			= $this->SpageProgram

									   ->get_paged_list($limit, $start, $order, $sort, $search, $order_field)

									   ->result_array();



		$data['akses']			= $this->nbaccess;



		$data['callback'] 		= $this->input->get('callback');



		$this->load->view('spage_other/data/json', $data);

	}



	private function form($id = ''){

		$data 	= $this->_data;



		if(!empty($id))

		{	

			$data["id"]	     = $id;

			$key             = array("id" => $id);

			$data["default"] = (array) $this->SpageProgram->get_data_form($key);

		}



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Halaman Statis ( Lainnya )', '/cms/spage_other');

		$this->breadcrumbs->push('Add New Data', '/cms/spage_other/new');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["action"]     = base_url() . 'cms/spage_other/save';

		$data["page"] 		= $this->SpageProgram->options("Pilih Halaman");



		$this->parser->parse("spage_other/form/form", $data);

	}



	public function add()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// cek valid token_proses

		$this->nbauth->abp($_key['addform']['token'], "GET", "spage_other", "addform");



		$this->form();

	}



	public function edit()

	{

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "spage_other", "editform");



		$this->form($param['id']);

	}



	public function read()

	{

		$data 	= $this->_data;



		// GET PARAMETER

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Halaman Statis ( Lainnya )', '/cms/spage_other');

		$this->breadcrumbs->push('View Data', '/cms/spage_other/view');



		$key             = array("id" => $param["id"]);

		$data["default"] = (array) $this->SpageProgram->get_data_form($key);

		$data["crumbs"]  = $this->breadcrumbs->show();



		$this->parser->parse("spage_other/view/view", $data);

	}



	public function simpan()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// get data from post

		$data = !empty($this->input->post("data")) ? $this->input->post("data") : array();



		// get id

		$id   = !empty($this->input->post("id")) ? $this->input->post("id") : '';



		// SETTING UPLOAD

		$this->upload->initialize(array(

			"upload_path" => $this->_ap_ori,

			"allowed_types" => "png|jpg|jpeg|gif",

			"overwrite" => TRUE,

			"encrypt_name" => TRUE,

			"remove_spaces" => TRUE,

			"max_size" => 30000,

			"xss_clean" => FALSE,

			"file_name" => array("image_" . date('Ymdhis'), "image_icon_" . date('Ymdhis'))

		));



		if(empty($id))

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['saveasnew']['token'], "POST", "spage_other", "saveasnew");



			if(!empty($_FILES)){

			    if ($this->upload->do_multi_upload("uploadedimages")) {

                    $return = $this->upload->get_multi_upload_data();



			        $data['img']    	 = $return[0]["orig_name"];

			  

			        // resize_img($this->_ap_thumb, $return);

			        fit_img($this->_ap_thumb,$return);



			    }else{

					redirect('/cms/error/error_upload');

			    }

			}



			$ds["is_active"] = 0;



			$result = $this->SpageProgram->update_to_db($ds, array("page" => $data["page"]));



			// add aditional data

			$data['crtd_at'] 	= date('Y-m-d H:i:s');

			$data['edtd_at'] 	= date('Y-m-d H:i:s');

			$data['author']		= $this->session->userdata('usr_fname');

			$data['is_active'] 	= "1";



			$result = $this->SpageProgram->insert_to_db($data);



			if($result)

			{	

				$this->session->set_flashdata('success_notif', 'Data berhasil diinsert.');

				redirect('/cms/spage_other');

			}

		}

		else

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['save']['token'], "POST", "spage_other", "save");



			// add aditional data

			$data['edtd_at'] 	= date('Y-m-d H:i:s');

			//$data['last_edit']	= $this->session->userdata('usr_fname');



			$result = $this->SpageProgram->update_to_db($data, array("id" => $id));



			if(!empty($_FILES["uploadedimages"]["name"][0])){

			    if ($this->upload->do_multi_upload("uploadedimages")) {

                    $return = $this->upload->get_multi_upload_data();



			        $data['img']    	 = $return[0]["orig_name"];

			        

			        resize_img($this->_ap_thumb, $return);

			        thumb($return,300,300,$this->_ap_thumb);



			        // DELETE IMAGE FROM FOLDER

			        unlink($this->_ap_ori . $this->input->post("img"));

			        unlink($this->_ap_ori . $this->input->post("icon_img"));

			        // echo $this->input->post("img"); exit();



			    	$key 	= array("id"=> $id);

					$result = $this->SpageProgram->update_to_db($data, $key);



					if($result)

					{	

						$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

						// exit();

						redirect('/cms/spage_other');

					}

			    }else{

			    	echo $this->upload->display_errors();

			    	exit();

					redirect('/cms/error/error_upload');

			    }

			}else{

		    	$key = array("id"=> $id);

				$result = $this->SpageProgram->update_to_db($data, $key);



				if($result)

				{	

					$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

					redirect('/cms/spage_other');

				}

			}

		}



		redirect('/cms/error/error_proccess');

	}



	public function delete()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));





		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "DELETE", "spage_other", "delete");





		$cekStatusActive = $this->SpageProgram->cekStatusActive(array("id" => $param["id"],"is_active" => "1"));



		if($cekStatusActive > 0)

		{

			$this->SpageProgram->update_to_db(array("is_active" => "1"),array("id" => $param["id"]));



			$this->session->set_flashdata('error_notif', 'Data gagal diproses. data yang dihapus sedang digunakan!');



        	redirect('/cms/spage_other');

		}

		else

		{

			$result = $this->SpageProgram->delete(array("id" => $param["id"]));



			if($result === FALSE)

			{

				redirect('/cms/error/error_proccess');

			}

			else

			{

		        unlink($this->_ap_ori . $param["img"]);

		        unlink($this->_ap_ori . $param["i_img"]);



				$this->session->set_flashdata('success_notif', 'Data berhasil dihapus.');



		        redirect('/cms/spage_other');

			}

		}

	}







	public function aktifasi()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "spage_other", "activate");



		$this->SpageProgram->update_to_db(array("is_active" => "0"), array("page" => $param["page"]));



		$result = $this->SpageProgram->update_to_db(

														array("is_active" => $param["status"]),

														array("id" => $param["id"], "page" => $param["page"])

													);



		if($result === FALSE)

		{

			redirect('/cms/error/error_proccess');

		}



		$cekStatusActive = $this->SpageProgram->cekStatusActive(array("page" => $param["page"],"is_active" => "1"));



		/*if($cekStatusActive <= 0)

		{

			$this->SpageProgram->update_to_db(array("is_active" => "1"),array("id" => $param["id"], "page" => $param["page"]));



			$this->session->set_flashdata('error_notif', 'Data gagal diproses. Salah satu data harus aktif!');



        	redirect('/cms/spage_other');

		}*/



		$this->session->set_flashdata('success_notif', 'Data berhasil diproses.');



        redirect('/cms/spage_other');

	}



}



