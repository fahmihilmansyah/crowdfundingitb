<?php



defined('BASEPATH') OR exit('No direct script access allowed');





class Spage_program extends NBAdmin_Controller {



	/**

	 * ----------------------------------------

	 * #spage_program Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_libList   	 = array();

	private $_modList   	 = array(

									'spage_program_model' 		=> 'SpageProgram',

								);

	

	protected $_data      	 = array();



	private $_ap_ori;

	private $_ap_thumb;

	private $_bs_ap;

	private $_bs_ap_tumb;



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------



		$this->modelsApp();

		// $this->libraryApp();



        // SET PATH IMAGE LOCATION

        $this->_ap_ori 		= FCPATH .'assets/images/spage_program/';

        $this->_ap_thumb 	= FCPATH .'assets/images/spage_program/new_thumb/';

        $this->_bs_ap		= base_url() . 'assets/images/spage_program/';

        $this->_bs_ap_tumb  = base_url() . 'assets/images/spage_program/';

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	private function libraryApp()

	{

		$this->load->library('librariesloader', $this->_libList);

	}







	public function index($data = array())

	{

		$data 	= $this->_data;



        $this->SpageProgram->register_action_permition();



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Halaman Statis ( Program )', '/cms/spage_program');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["can_add"]	= $this->nbaccess->getAccess("add");

		$data["dsource"]	= base_url() . "cms/spage_program/json";



		$this->parser->parse("spage_program/index",$data);



	}







	public function json($roleid = ''){

		$default_order  = 'id';

		$order_field    = '';

		$order_field 	= array(

							'id',

							'm_title',

							'crtd_at',

							'edtd_at',

							'author',

							'last_edit'

						 );



		$order_key 	= (!$this->input->get('iSortCol_0')) ? 0 : $this->input->get('iSortCol_0');

		$order 		= (!$this->input->get('iSortCol_0')) ? $default_order : $order_field[$order_key];

		$sort 		= (!$this->input->get('sSortDir_0')) ? 'DESC': $this->input->get('sSortDir_0');

		$search 	= (!$this->input->get('sSearch'))    ? '' : strtoupper($this->input->get('sSearch'));



		$limit 		= (!$this->input->get('iDisplayLength')) ? $this->_limit : $this->input->get('iDisplayLength');

		$start 		= (!$this->input->get('iDisplayStart'))  ? 0 : $this->input->get('iDisplayStart');



		$data['sEcho'] 			= (!$this->input->get('callback')) ? 0 : $this->input

																			  ->get('callback');



		$data['iTotalRecords'] 	= $this->SpageProgram->count_all($search,$order_field);



		$data['apps'] 			= $this->SpageProgram

									   ->get_paged_list($limit, $start, $order, $sort, $search, $order_field)

									   ->result_array();



		$data['akses']			= $this->nbaccess;



		$data['callback'] 		= $this->input->get('callback');



		$this->load->view('spage_program/data/json', $data);

	}



	private function form($id = ''){

		$data 	= $this->_data;



		if(!empty($id))

		{	

			$data["id"]	     = $id;

			$key             = array("id" => $id);

			$data["default"] = (array) $this->SpageProgram->get_data_form($key);

		}



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Halaman Statis ( Program )', '/cms/spage_program');

		$this->breadcrumbs->push('Add New Data', '/cms/spage_program/new');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["action"]     = base_url() . 'cms/spage_program/save';

		$data["tipe"] 		= array("" => "Pilih Program", "zakat" => "Zakat", "infaq" => "Infaq", "shadaqah" => "Shadaqah");



		$this->parser->parse("spage_program/form/form", $data);

	}



	public function add()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// cek valid token_proses

		$this->nbauth->abp($_key['addform']['token'], "GET", "spage_program", "addform");



		$this->form();

	}



	public function edit()

	{

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "spage_program", "editform");



		$this->form($param['id']);

	}



	public function read()

	{

		$data 	= $this->_data;



		// GET PARAMETER

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Halaman Statis ( Program )', '/cms/spage_program');

		$this->breadcrumbs->push('View Data', '/cms/spage_program/view');



		$key             = array("id" => $param["id"]);

		$data["default"] = (array) $this->SpageProgram->get_data_form($key);

		$data["crumbs"]  = $this->breadcrumbs->show();



		$this->parser->parse("spage_program/view/view", $data);

	}



	public function simpan()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// get data from post

		$data = !empty($this->input->post("data")) ? $this->input->post("data") : array();



		// get id

		$id   = !empty($this->input->post("id")) ? $this->input->post("id") : '';



		// SETTING UPLOAD

		$icon_name = "icon_" . date('Ymdhis');

		$this->upload->initialize(array(

			"upload_path" => $this->_ap_ori,

			"allowed_types" => "png|jpg|jpeg|gif",

			"overwrite" => TRUE,

			"encrypt_name" => TRUE,

			"remove_spaces" => TRUE,

			"max_size" => 30000,

			"xss_clean" => FALSE,

			"file_name" => array("image_" . date('Ymdhis'), $icon_name, $icon_name . "_g")

		));



		if(empty($id))

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['saveasnew']['token'], "POST", "spage_program", "saveasnew");



			if(!empty($_FILES)){

			    if ($this->upload->do_multi_upload("uploadedimages")) {

                    $return = $this->upload->get_multi_upload_data();



			        $data['m_img']    	 = $return[0]["orig_name"];

			        $data['icon_img']    = $return[1]["orig_name"];



			        // resize_img($this->_ap_thumb, $return);

			        // fit_img($this->_ap_thumb,$return);



			    }else{

			    	echo $this->upload->display_errors();

			    	exit();

					redirect('/cms/error/error_upload');

			    }

			}



			$ds["is_active"] = 0;



			$result = $this->SpageProgram->update_to_db($ds, array("tipe" => $data["tipe"]));



			// add aditional data

			$data['crtd_at'] 	= date('Y-m-d H:i:s');

			$data['edtd_at'] 	= date('Y-m-d H:i:s');

			$data['author']		= $this->session->userdata('usr_fname');

			$data['is_active'] 	= "1";



			$result = $this->SpageProgram->insert_to_db($data);



			if($result)

			{	

				$this->session->set_flashdata('success_notif', 'Data berhasil diinsert.');

				redirect('/cms/spage_program');

			}

		}

		else

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['save']['token'], "POST", "spage_program", "save");



			// add aditional data

			$data['edtd_at'] 	= date('Y-m-d H:i:s');

			$data['last_edit']	= $this->session->userdata('usr_fname');



			$result = $this->SpageProgram->update_to_db($data, array("id" => $id));



			if(!empty($_FILES["uploadedimages"]["name"][0])){

			    if ($this->upload->do_multi_upload("uploadedimages")) {

                    $return = $this->upload->get_multi_upload_data();



			        $data['m_img']    	 = $return[0]["orig_name"];

			        $data['icon_img']    = $return[1]["orig_name"];



			        resize_img($this->_ap_thumb, $return);

			        thumb($return,300,300,$this->_ap_thumb);



			        // DELETE IMAGE FROM FOLDER

			        unlink($this->_ap_ori . $this->input->post("m_img"));

			        unlink($this->_ap_ori . $this->input->post("icon_img"));

			        unlink($this->_ap_ori . $this->input->post("icon_img_g"));

			        // echo $this->input->post("img"); exit();



			    	$key 	= array("id"=> $id);

					$result = $this->SpageProgram->update_to_db($data, $key);



					if($result)

					{	

						$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

						// exit();

						redirect('/cms/spage_program');

					}

			    }else{

			    	echo $this->upload->display_errors();

			    	exit();

					redirect('/cms/error/error_upload');

			    }

			}else{

		    	$key = array("id"=> $id);

				$result = $this->SpageProgram->update_to_db($data, $key);



				if($result)

				{	

					$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

					redirect('/cms/spage_program');

				}

			}

		}



		redirect('/cms/error/error_proccess');

	}



	public function delete()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));





		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "DELETE", "spage_program", "delete");





		$cekStatusActive = $this->SpageProgram->cekStatusActive(array("id" => $param["id"],"is_active" => "1"));



		if($cekStatusActive > 0)

		{

			$this->SpageProgram->update_to_db(array("is_active" => "1"),array("id" => $param["id"]));



			$this->session->set_flashdata('error_notif', 'Data gagal diproses. data yang dihapus sedang digunakan!');



        	redirect('/cms/spage_program');

		}

		else

		{

			$result = $this->SpageProgram->delete(array("id" => $param["id"]));



			if($result === FALSE)

			{

				redirect('/cms/error/error_proccess');

			}

			else

			{

		        unlink($this->_ap_ori . $param["m_img"]);

		        unlink($this->_ap_ori . $param["i_img"]);



            	$name = explode(".", $param["i_img"]);

            	$img  = $name[0] . "_g." . $name[1];



		        unlink($this->_ap_ori . $img);





				$this->session->set_flashdata('success_notif', 'Data berhasil dihapus.');



		        redirect('/cms/spage_program');

			}

		}

	}







	public function aktifasi()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "spage_program", "activate");



		$this->SpageProgram->update_to_db(array("is_active" => "0"), array("tipe" => $param["tipe"]));



		$result = $this->SpageProgram->update_to_db(

														array("is_active" => $param["status"]),

														array("id" => $param["id"], "tipe" => $param["tipe"])

													);



		if($result === FALSE)

		{

			redirect('/cms/error/error_proccess');

		}



		$cekStatusActive = $this->SpageProgram->cekStatusActive(array("tipe" => $param["tipe"],"is_active" => "1"));



		/*if($cekStatusActive <= 0)

		{

			$this->SpageProgram->update_to_db(array("is_active" => "1"),array("id" => $param["id"], "tipe" => $param["tipe"]));



			$this->session->set_flashdata('error_notif', 'Data gagal diproses. Salah satu data harus aktif!');



        	redirect('/cms/spage_program');

		}*/



		$this->session->set_flashdata('success_notif', 'Data berhasil diproses.');



        redirect('/cms/spage_program');

	}



}



