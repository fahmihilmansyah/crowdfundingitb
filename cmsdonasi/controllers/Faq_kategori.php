<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class faq_kategori extends NBAdmin_Controller {



	/**

	 * ----------------------------------------

	 * #FAQKategori Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_libList   = array();

	private $_modList   = array(

									'faq_kategori_model' => 'PostKtg',

								);

	

	protected $_data      = array();



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------

		$this->modelsApp();

		// $this->libraryApp();

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	private function libraryApp()

	{

		$this->load->library('librariesloader', $this->_libList);

	}



	public function index($data = array())

	{

		$data 	= $this->_data;



        $this->PostKtg->register_action_permition();



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Kategori FAQ', '/cms/faq_kategori');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["can_add"]	= $this->nbaccess->getAccess("add");

		$data["dsource"]	= base_url() . "cms/faq_kategori/json";



		$this->parser->parse("faq_kategori/index",$data);

	}



	public function json($roleid = ''){

		$default_order  = 'id';

		$order_field    = '';

		

		$order_field 	= array(

							'id',

							'title',

							'crtd_at',

							'edtd_at',

							'author',

							'last_edit'

						 );

				

		$order_key 	= (!$this->input->get('iSortCol_0')) ? 0 : $this->input->get('iSortCol_0');

		$order 		= (!$this->input->get('iSortCol_0')) ? $default_order : $order_field[$order_key];

		$sort 		= (!$this->input->get('sSortDir_0')) ? 'DESC': $this->input->get('sSortDir_0');

		$search 	= (!$this->input->get('sSearch'))    ? '' : strtoupper($this->input->get('sSearch'));



		$limit 		= (!$this->input->get('iDisplayLength')) ? $this->_limit : $this->input->get('iDisplayLength');

		$start 		= (!$this->input->get('iDisplayStart'))  ? 0 : $this->input->get('iDisplayStart');



		$data['sEcho'] 			= (!$this->input->get('callback')) ? 0 : $this->input

																			  ->get('callback');



		$data['iTotalRecords'] 	= $this->PostKtg->count_all($search,$order_field);

		

		$data['apps'] 			= $this->PostKtg

									   ->get_paged_list($limit, $start, $order, $sort, $search, $order_field)

									   ->result_array();



		$data['akses']			= $this->nbaccess;



		$data['callback'] 		= $this->input->get('callback');



		$this->load->view('faq_kategori/data/json', $data);

	}



	private function form($id = ''){

		$data 	= $this->_data;



		if(!empty($id))

		{	

			$data["id"]	     = $id;

			$key             = array("id" => $id);

			$data["default"] = (array) $this->PostKtg->get_data_form($key);

		}



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Manajemen Users', '/cms/faq_kategori');

		$this->breadcrumbs->push('Add New Data', '/cms/faq_kategori/new');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["action"]     = base_url() . 'cms/faq_kategori/save';



		$this->parser->parse("faq_kategori/form/form", $data);

	}



	public function add()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// cek valid token_proses

		$this->nbauth->abp($_key['addform']['token'], "GET", "faq_kategori", "addform");



		$this->form();

	}



	public function edit()

	{

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }

		

		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "faq_kategori", "editform");

		

		$this->form($param['id']);

	}



	public function read()

	{

		$data 	= $this->_data;



		// GET PARAMETER

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));

		

		if($param === false){ redirect('/cms/error/error_proccess'); }

		

        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Kategori FAQ', '/cms/faq_kategori');

		$this->breadcrumbs->push('View Data', '/cms/faq_kategori/view');



		$key             = array("id" => $param["id"]);

		$data["default"] = (array) $this->PostKtg->get_data_form($key);

		$data["crumbs"]  = $this->breadcrumbs->show();



		$this->parser->parse("faq_kategori/view/view", $data);

	}



	public function simpan()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// get data from post

		$data = !empty($this->input->post("data")) ? $this->input->post("data") : array();

		

		// get id

		$id   = !empty($this->input->post("id")) ? $this->input->post("id") : '';



		if(empty($id))

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['saveasnew']['token'], "POST", "faq_kategori", "saveasnew");



			// add aditional data

			$data['crtd_at'] 	= date('Y-m-d H:i:s');

			$data['edtd_at'] 	= date('Y-m-d H:i:s');

			$data['author']		= $this->session->userdata('usr_fname');



			$result = $this->PostKtg->insert_to_db($data);

			if($result)

			{	

				$this->session->set_flashdata('success_notif', 'Data berhasil diinsert.');

				redirect('/cms/faq_kategori');

			}

		}

		else

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['save']['token'], "POST", "faq_kategori", "save");



			// add aditional data

			$data['edtd_at'] 	= date('Y-m-d H:i:s');

			$data['last_edit']	= $this->session->userdata('usr_fname');



			$result = $this->PostKtg->update_to_db($data, array("id" => $id));

			if($result)

			{	



				$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

				redirect('/cms/faq_kategori');

			}

		}

		

		redirect('/cms/error/error_proccess');

	}



	public function delete()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "DELETE", "faq_kategori", "delete");



		$result = $this->PostKtg->delete(array("id" => $param["id"]));



		if($result === FALSE)

		{

			redirect('/cms/error/error_proccess');

		}

		

		$this->session->set_flashdata('success_notif', 'Data berhasil dihapus.');



        redirect('/cms/faq_kategori');

	}



	public function aktifasi()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));

		

		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "faq_kategori", "activate");



		$data["is_active"] = $param["status"];

		$result = $this->PostKtg->update_to_db($data,array("id" => $param["id"]));



		if($result === FALSE)

		{

			redirect('/cms/error/error_proccess');

		}



		$this->session->set_flashdata('success_notif', 'Data berhasil diproses.');



        redirect('/cms/faq_kategori');

	}

}

