<?php /** * Created by PhpStorm. * User: Fahmi hilmansyah * Date: 3/27/2017 * Time: 10:21 AM */
class ManajemenCrawler extends NBAdmin_Controller
{
    protected $_data = array();
    private $_libList = array();
    private $_modList = array('Newcustom_model' => 'cusmdl');
    private $notif = "";

    function __construct()
    {
        parent::__construct();
        $this->modelsApp();
    }

    private function modelsApp()
    {
        $this->load->model($this->_modList);
    }

    function index()
    {
        $cari = $this->db->query("select * from data_bank where bank_date ='".date("Y-m-d")."' ");
        foreach ($cari as $r) {
            $this->doVerifikasiCampaign($r->jumlah);
        }
    }

    private function doVerifikasiCampaign($id = null)
    {
        $kondisi['where'] = array("nb_trans_donation.nomuniq" => $this->db->escape($id), "nb_trans_donation.status" => $this->db->escape("unverified"));
        $cekdonasi = $this->cusmdl->custom_query("nb_trans_donation", $kondisi)->result();
        if (count($cekdonasi) != 1) {
            $this->doVerifikasiProgram($id);
        } else {
            $campaign = $cekdonasi[0]->campaign;
            $nomuniq = $cekdonasi[0]->nomuniq;
            $notransaksi = $cekdonasi[0]->no_transaksi;
            $iddonatur = $cekdonasi[0]->donatur;
            $kodebank = $cekdonasi[0]->bank;
            $kondisi1['where'] = array("nb_campaign.id" => $campaign);
            $cekcampaign = $this->cusmdl->custom_query("nb_campaign", $kondisi1)->result();
            if (count($cekcampaign) > 0) {
                $sumdonatur = (int)$cekcampaign[0]->sum_donatur;
                $now = (int)$cekcampaign[0]->now;                /*updated nb_campaign*/
                $data = array("sum_donatur" => (int)$sumdonatur + 1, 'now' => (int)$now + (int)$nomuniq, 'edtd_at' => date("Y-m-d H:i:s"));
                $where = array("nb_campaign.id" => ($campaign),);
                $this->cusmdl->custom_update("nb_campaign", $data, $where);                /*update nb_trans_donation*/
                $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));
                $where = array("nb_trans_donation.no_transaksi" => ($id),);
                $this->cusmdl->custom_update("nb_trans_donation", $data, $where);                /*jika kode bank nya 919 == potong deposit*/
                if ($kodebank == 919) {
                    $this->trxDeposit($iddonatur, $nomuniq, $notransaksi, "TRX");
                }
                $this->ubahkonfirm($id);
            }
        }
    }

    private function trxDeposit($iddonatur = null, $nominal = null, $notransaksi = null, $jenis = null)
    {
        $kondisi['where'] = array("nb_donaturs_saldo.donatur" => $this->db->escape($iddonatur));
        $ceksaldo = $this->cusmdl->custom_query("nb_donaturs_saldo", $kondisi)->result();
        if (count($ceksaldo) != 1) {
            die("cannot proses");
        } else {
            $balanceAmount = 0;
            $prevBlance = $ceksaldo[0]->balanceAmount;
            if ($jenis == "DEPOSIT") {
                $balanceAmount = (int)$nominal + (int)$ceksaldo[0]->balanceAmount;
                $data = array('donaturs' => $iddonatur, 'jenistrx' => "DEPO", 'id_transaksi' => $notransaksi, 'prevBalance' => $prevBlance, 'amount' => $nominal, 'balance' => $balanceAmount, 'crtd_at' => date("Y-m-d H:i:s"), 'edtd_at' => date("Y-m-d H:i:s"),);
                $this->cusmdl->custom_insert('nb_donaturs_trx', $data);
            } else {
                $balanceAmount = (int)$ceksaldo[0]->balanceAmount - (int)$nominal;
                if ($balanceAmount < 0) {
                    die("Maaf Saldo Tidak Cukup");
                }
                $data = array('donaturs' => $iddonatur, 'jenistrx' => "TRX", 'id_transaksi' => $notransaksi, 'prevBalance' => $prevBlance, 'amount' => $nominal, 'balance' => $balanceAmount, 'crtd_at' => date("Y-m-d H:i:s"), 'edtd_at' => date("Y-m-d H:i:s"),);
                $this->cusmdl->custom_insert('nb_donaturs_trx', $data);
            }
            $data = array("prevBalance" => $prevBlance, 'balanceAmount' => $balanceAmount, 'edtd_at' => date("Y-m-d H:i:s"));
            $where = array("nb_donaturs_saldo.donatur" => $iddonatur);
            $this->cusmdl->custom_update("nb_donaturs_saldo", $data, $where);
        }
    }

    private function doVerifikasiProgram($id = null)
    {
        $kondisi['where'] = array("nb_trans_program.nomuniq" => $this->db->escape($id), "nb_trans_program.status" => $this->db->escape("unverified"));
        $cekdonasi = $this->cusmdl->custom_query("nb_trans_program", $kondisi)->result();
        if (count($cekdonasi) != 1) {
            return true;
        } else {
            $nomuniq = $cekdonasi[0]->nomuniq;
            $notransaksi = $cekdonasi[0]->no_transaksi;
            $iddonatur = $cekdonasi[0]->donatur;
            $jenistrx = $cekdonasi[0]->jenistrx;
            $kodebank = $cekdonasi[0]->bank;            /*Kondisi untuk melakukan transaksi melalui deposit*/
            if (strtoupper($jenistrx) == "DEPOSIT") {
                $this->trxDeposit($iddonatur, $nomuniq, $notransaksi, "DEPOSIT");
            } else {                /*jika kode bank nya 919 == potong deposit*/
                if ($kodebank == 919) {
                    $this->trxDeposit($iddonatur, $nomuniq, $notransaksi, "TRX");
                }
            }            /*update nb_trans_program*/
            $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));
            $where = array("nb_trans_program.no_transaksi" => ($id),);
            $this->cusmdl->custom_update("nb_trans_program", $data, $where);
            $this->notif = array('success' => 'Berhasil memproses data!');
            $this->ubahkonfirm($id);
        }
    }

    private function ubahkonfirm($notrx){
        /*update nb_trans_konfirmasi*/
        $data = array("status" => "verified");
        $where = array("nb_trans_konfirmasi.transaksi" => ($notrx),);
        $this->cusmdl->custom_update("nb_trans_konfirmasi", $data, $where);
    }
    private function libraryApp()
    {
        $this->load->library('librariesloader', $this->_libList);
    }    /*=============END==================*/
}