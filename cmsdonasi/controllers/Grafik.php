<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Grafik extends NBAdmin_Controller {



	/**

	 * ----------------------------------------

	 * #NB Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_libList   = array();

	private $_modList   = array('nbgrafik_model' => 'NBGraph');

	protected $_data    = array();



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------

		$this->modelsApp();

		// $this->libraryApp();

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	private function libraryApp()

	{

		$this->load->library('librariesloader', $this->_libList);

	}



	public function campaign($data = array())
	{
		$datac   = $this->NBGraph->getCampaignPerMonth();
		$datad	 = $this->NBGraph->getdonaturPerMonth();

		$result = array(
				"datac" => $datac,
				"datad" => $datad,
				"year" => date('Y'),
				"type" => "line"
			);

		echo json_encode($result);
	}

	public function campaign_pg_detail($data = array())
	{
		$data["c"] = $this->input->get("c");
		$data["y"] = $this->input->get("y");
		$this->parser->parse("grafik/campaign/detail",$data);
	}

	public function campaign_detail()
	{
		$c = !empty($this->input->get("c")) ? $this->input->get("c") : date('m');
		$y = !empty($this->input->get("y")) ? $this->input->get("y") : date('Y');

		$datac   = $this->NBGraph->getCampaignPerDay($c, $y);
		$datad   = $this->NBGraph->getdonaturPerDay($c, $y);
		
		
		$result = array(
				"datac" => $datac,
				"datad" => $datad,
				"year" => $c . "/" . $y,
				"type" => "line"
			);

		echo json_encode($result);
	}


	public function program($data = array())
	{
		$type   = $this->input->get("t");

		$datac   = $this->NBGraph->getProgramPerMonth($type);
		$datad   = $this->NBGraph->getdonaturProgramPerMonth($type);

		$result = array(
				"datac" => $datac,
				"datad" => $datad,
				"year" => date('Y'),
				"type" => "line"
			);

		echo json_encode($result);
	}

	public function program_pg_detail($data = array())
	{
		$data["t"] = $this->input->get("t");
		$data["c"] = $this->input->get("c");
		$data["y"] = $this->input->get("y");
		$this->parser->parse("grafik/program/detail",$data);
	}

	public function program_detail()
	{
		$t = $this->input->get("t");
		$c = !empty($this->input->get("c")) ? $this->input->get("c") : date('m');
		$y = !empty($this->input->get("y")) ? $this->input->get("y") : date('Y');

		$datac   = $this->NBGraph->getProgramPerDay($c, $y, $t);
		$datad   = $this->NBGraph->getdonaturProgramPerDay($c, $y, $t);

		$result = array(
				"datac" => $datac,
				"datad" => $datad,
				"year" => $c . "/" . $y,
				"type" => "line"
			);

		echo json_encode($result);
	}

}

