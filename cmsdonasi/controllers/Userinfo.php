<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class userinfo extends CI_Controller
{

    /**
     * ----------------------------------------
     * #UserInfo Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */

    private $_appTitle;
    private $_libList = array('nbsettings', 'nbmenus', 'nbauth');
    private $_modList = array('userinfo_model' => 'users_m');
    private $_data = array();

    private $_ap_ori;
    private $_ap_thumb;
    private $_bs_ap;
    private $_bs_ap_tumb;

    function __construct()
    {
        parent::__construct();

        $this->modelsApp();
        // SET PATH IMAGE LOCATION
        $this->_ap_ori 		= FCPATH .'assets/images/admprofil/';
        $this->_ap_thumb 	= FCPATH .'assets/images/admprofil/';
        $this->_bs_ap		= base_url() . 'assets/images/admprofil/';
        $this->_bs_ap_tumb  = base_url() . 'assets/images/admprofil/';
        
        $this->libraryApp();
        $this->initApp();
    }

    private function modelsApp()
    {
        $this->load->model($this->_modList);
    }

    private function libraryApp()
    {
        $this->load->library('librariesloader', $this->_libList);
    }

    private function initApp()
    {
        $this->nbauth->cauth();

        // GET JS CSS
        $setting = $this->nbsettings;
        $setting->set_plugin(array('main'));
        $this->_data["_jsHeader"] = $setting->get_js();
        $this->_data["_cssHeader"] = $setting->get_css();

        // GET MENU
        $menu = $this->nbmenus;
        $this->_data["_menu"] = $menu->getMenu(0);

        // USERNAME AND APPTITLE
        $this->_data["_appTitle"] = APPTITLE;
        $this->_data["_username"] = !empty($this->session->userdata("usr_uname")) ?
            $this->session->userdata("usr_uname") : "Jhon Doe";

        // SET LAYOUT
        $this->_data["_layHeader"] = $this->parser->parse('header', $this->_data, TRUE);
        $this->_data["_layFooter"] = $this->parser->parse('footer', $this->_data, TRUE);
    }

    public function index($data = array())
    {
        $data = $this->_data;

        $this->users_m->register_action_permition();

        $data["action"] = base_url() . 'cms/usrinfo/save';

        $data["id"] = $this->session->userdata("usr_id");

        $data["dsource"] = !empty($this->users_m->get_data($this->session->userdata("usr_id"))->row_array()) ? $this->users_m->get_data($this->session->userdata("usr_id"))->row_array() : array();

        $this->parser->parse("userinfo/index", $data);
    }

    public function edit()
    {
        $_key = $this->session->userdata("_tokenaction");

        // get data from post

        $data = !empty($this->input->post("data")) ? $this->input->post("data") : array();

        // get id
        $id = !empty($this->input->post("id")) ? $this->input->post("id") : '';
        $datae = array(
            'nope' => $data['nope'],
            'fname' => $data['fname'],
            'address' => $data['address'],
        );
        if(!empty($data['pwd']) || trim($data['pwd']) != ""){
            $datae['pwd'] = $this->nbauth->getCustomHash($data['pwd']);
        }

        // SETTING UPLOAD
        $this->upload->initialize(array(
            "upload_path" => $this->_ap_ori,
            "allowed_types" => "png|jpg|jpeg|gif",
            "overwrite" => TRUE,
            "encrypt_name" => TRUE,
            "remove_spaces" => TRUE,
            "max_size" => 30000,
            "xss_clean" => FALSE,
            "file_name" => array("image_" . date('Ymdhisss'))
        ));

        // cek valid token_proses

        $this->nbauth->abp($_key['save']['token'], "POST", "usrinfo", "save");
        
        if ((!empty($_FILES["uploadedimages"]["name"][0]))) {
            if ($this->upload->do_multi_upload("uploadedimages")) {
                $return = $this->upload->get_multi_upload_data();
                $datae['img'] = $return[0]["orig_name"];
                resize_img($this->_ap_thumb, $return);
            } else {
                redirect('/cms/error/error_upload');
            }
        }
        if ((!empty($_FILES["uploadedimagesqris"]["name"][0]))) {
            if ($this->upload->do_multi_upload("uploadedimagesqris")) {
                $return = $this->upload->get_multi_upload_data();
                $datae['img_qris'] = $return[0]["orig_name"];
//                resize_img($this->_ap_thumb, $return);
            } else {
                redirect('/cms/error/error_upload');
            }
        }

        $result = $this->users_m->update_data($datae, array("id" => $id));

        if ($result) {
            $this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');
            redirect('/cms/usrinfo');
        }else{
            redirect('/cms/error/error_proccess');
        }
    }
}
