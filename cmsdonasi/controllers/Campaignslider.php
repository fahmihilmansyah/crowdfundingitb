<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaignslider extends NBAdmin_Controller {

	/**
	 * ----------------------------------------
	 * #campaignslider Controller
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_libList   = array();
	private $_modList   = array(
									'campaignslider_model' => 'PostKtg',
								);
	
	protected $_data      = array();

	private $_ap_ori;
	private $_ap_thumb;
	private $_bs_ap;
	private $_bs_ap_tumb;

	function __construct()
	{
		parent::__construct();

		// -----------------------------------
		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN
		// -----------------------------------
		$this->modelsApp();
		// $this->libraryApp();
		// SET PATH IMAGE LOCATION
        $this->_ap_ori 		= FCPATH .'assets/images/slider/';
        $this->_ap_thumb 	= FCPATH .'assets/images/sliders/thumbnails/';
        $this->_bs_ap		= base_url() . 'assets/images/slider/';
        $this->_bs_ap_tumb  = base_url() . 'assets/images/slider/';
	}

	private function modelsApp()
	{		
		$this->load->model($this->_modList);
	}

	private function libraryApp()
	{
		$this->load->library('librariesloader', $this->_libList);
	}

	public function index($data = array())
	{
		$data 	= $this->_data;

        $this->PostKtg->register_action_permition();

        $this->breadcrumbs->push('Home', '/cms');
		$this->breadcrumbs->push('Kategori Artikel', '/cms/campaignslider');

		$data["crumbs"]     = $this->breadcrumbs->show();
		$data["can_add"]	= $this->nbaccess->getAccess("add");
		$data["dsource"]	= base_url() . "cms/campaignslider/json";

		$this->parser->parse("campaignslider/index",$data);
	}

	public function json($roleid = ''){
		$default_order  = 'id';
		$order_field    = '';
		
		$order_field 	= array(
							'nb_campaign_slide.id',
							'nb_campaign_slide.img',
							'nb_campaign_slide.title',
							'nb_campaign_slide.desc',
							'nb_campaign_slide.crtd_at'
						 );
				
		$order_key 	= (!$this->input->get('iSortCol_0')) ? 0 : $this->input->get('iSortCol_0');
		$order 		= (!$this->input->get('iSortCol_0')) ? $default_order : $order_field[$order_key];
		$sort 		= (!$this->input->get('sSortDir_0')) ? 'DESC': $this->input->get('sSortDir_0');
		$search 	= (!$this->input->get('sSearch'))    ? '' : strtoupper($this->input->get('sSearch'));

		$limit 		= (!$this->input->get('iDisplayLength')) ? $this->_limit : $this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))  ? 0 : $this->input->get('iDisplayStart');

		$data['sEcho'] 			= (!$this->input->get('callback')) ? 0 : $this->input
																			  ->get('callback');

		$data['iTotalRecords'] 	= $this->PostKtg->count_all($search,$order_field);
		
		$data['apps'] 			= $this->PostKtg
									   ->get_paged_list($limit, $start, $order, $sort, $search, $order_field)
									   ->result_array();

		$data['akses']			= $this->nbaccess;

		$data['callback'] 		= $this->input->get('callback');

		$this->load->view('campaignslider/data/json', $data);
	}

	private function form($id = ''){
		$data 	= $this->_data;

		if(!empty($id))
		{	
			$data["id"]	     = $id;
			$key             = array("id" => $id);
			$data["default"] = (array) $this->PostKtg->get_data_form($key);
		}

        $this->breadcrumbs->push('Home', '/cms');
		$this->breadcrumbs->push('Manajemen Slider', '/cms/campaignslider');
		$this->breadcrumbs->push('Add New Data', '/cms/campaignslider/new');

		$data["crumbs"]     = $this->breadcrumbs->show();
		$data["action"]     = base_url() . 'cms/campaignslider/save';

		$this->parser->parse("campaignslider/form/form", $data);
	}

	public function add()
	{
		$_key   = $this->session->userdata("_tokenaction");

		// cek valid token_proses
		$this->nbauth->abp($_key['addform']['token'], "GET", "campaignslider", "addform");

		$this->form();
	}

	public function edit()
	{
		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();
		$param  = ARR::getParam(ENKRIP::decode($param));

		if($param === false){ redirect('/cms/error/error_proccess'); }
		
		// cek valid token_proses
		$this->nbauth->abp($param["token"], "GET", "campaignslider", "editform");
		
		$this->form($param['id']);
	}

	public function read()
	{
		$data 	= $this->_data;

		// GET PARAMETER
		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();
		$param  = ARR::getParam(ENKRIP::decode($param));
		
		if($param === false){ redirect('/cms/error/error_proccess'); }
		
        $this->breadcrumbs->push('Home', '/cms');
		$this->breadcrumbs->push('Kategori Artikel', '/cms/campaignslider');
		$this->breadcrumbs->push('View Data', '/cms/campaignslider/view');

		$key             = array("id" => $param["id"]);
		$data["default"] = (array) $this->PostKtg->get_data_form($key);
		$data["crumbs"]  = $this->breadcrumbs->show();

		$this->parser->parse("campaignslider/view/view", $data);
	}

	public function simpan()
	{
		$_key   = $this->session->userdata("_tokenaction");

		// get data from post
		$data = !empty($this->input->post("data")) ? $this->input->post("data") : array();
		
		// get id
		$id   = !empty($this->input->post("id")) ? $this->input->post("id") : '';

		// SETTING UPLOAD
		$this->upload->initialize(array(
			"upload_path" => $this->_ap_ori,
			"allowed_types" => "png|jpg|jpeg|gif",
			"overwrite" => TRUE,
			"encrypt_name" => TRUE,
			"remove_spaces" => TRUE,
			"max_size" => 30000,
			"xss_clean" => FALSE,
			"file_name" => array("image_" . date('Ymdhis'))
		));

		// add aditional data
		$status = "0";
		if(!empty($this->input->post("link_d")))
		{
			$status = "1";
		}

		if(empty($id))
		{
			// cek valid token_proses
			$this->nbauth->abp($_key['saveasnew']['token'], "POST", "CampaignSlider", "saveasnew");

			if(!empty($_FILES)){
			    if ($this->upload->do_multi_upload("uploadedimages")) {
                    $return = $this->upload->get_multi_upload_data();
                    
			        $data['img']    = $return[0]["orig_name"];

			        resize_img($this->_ap_thumb, $return);
			    }else{
		
			    	echo $this->upload->display_errors();
			    	exit();

					redirect('/cms/error/error_upload');
			    }
			}

			$data['title'] 		= $data['title'];
			$data['desc'] 		= $data['desc'];
			$data['link'] 		= $data['link'];
			$data['link_d'] 	= $status;
			$data['crtd_at'] 	= date('Y-m-d H:i:s');
			$data['edtd_at'] 	= date('Y-m-d H:i:s');

			$result = $this->PostKtg->insert_to_db($data);
			if($result)
			{	
				$this->session->set_flashdata('success_notif', 'Data berhasil diinsert.');
				redirect('/cms/campaignslider');
			}
		}
		else
		{
			// cek valid token_proses
			$this->nbauth->abp($_key['save']['token'], "POST", "CampaignSlider", "save");

			// add aditional data
			$data['edtd_at'] 	= date('Y-m-d H:i:s');

			$result = $this->PostKtg->update_to_db($data, array("id" => $id));
			
			if(!empty($_FILES["uploadedimages"]["name"][0])){
			    if ($this->upload->do_multi_upload("uploadedimages")) {
                    $return = $this->upload->get_multi_upload_data();
                    
			        $data['img']    = $return[0]["orig_name"];

			        resize_img($this->_ap_thumb, $return);

			        // DELETE IMAGE FROM FOLDER
			        unlink($this->_ap_ori . $this->input->post("img"));
			        unlink($this->_ap_thumb . $this->input->post("img"));
			        // echo $this->input->post("img"); exit();
			    	$key 	= array("id"=> $id);
					$result = $this->PostKtg->update_to_db($data, $key);

					if($result)
					{	
						$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');
						// exit();
						redirect('/cms/campaignslider');
					}
			    }else{
			    	echo $this->upload->display_errors();
			    	exit();
					redirect('/cms/error/error_upload');
			    }
			}else{
				$data['title'] 	= $data['title'];
				$data['desc'] 	= $data['desc'];
				$data['link'] 	= $data['link'];
				$data['link_d'] = $status;
				
		    	$key = array("id"=> $id);
				$result = $this->PostKtg->update_to_db($data, $key);
				
				if($result)
				{	
					$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');
					redirect('/cms/campaignslider');
				}
			}
		}

		redirect('/cms/error/error_proccess');
	}

	public function delete()
	{
		$param = !empty($_GET["param"]) ? $_GET["param"] : array();
		$param = ARR::getParam(ENKRIP::decode($param));

		if($param === false){ redirect('/cms/error/error_proccess'); }

		// cek valid token_proses
		$this->nbauth->abp($param["token"], "DELETE", "campaignslider", "delete");
		$find = $this->PostKtg->get_data_form(array('id'=>$param["id"]));
		// DELETE IMAGE FROM FOLDER
        unlink($this->_ap_ori . $find->img);
        unlink($this->_ap_thumb . $find->img);
		$result = $this->PostKtg->delete(array("id" => $param["id"]));

		if($result === FALSE)
		{
			redirect('/cms/error/error_proccess');
		}
		
		$this->session->set_flashdata('success_notif', 'Data berhasil dihapus.');

        redirect('/cms/campaignslider');
	}

}
