<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Manajemenmenu extends NBAdmin_Controller {



	/**

	 * ----------------------------------------

	 * #ManajemenMenu Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_libList   = array();

	private $_modList   = array(

									'manajemenmenu_model'	=> 'MMenu'

							    );



	protected $_data    = array();



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------

		$this->modelsApp();

		// $this->librariesApp();

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}



	private function librariesApp()

	{

		$this->load->library('librariesloader', $this->_libList);

	}



	public function index($data = array())

	{

		$data 	= $this->_data;





        $this->MMenu->register_action_permition();



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Manajemen Menu', '/cms/settingmenu');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["can_add"]	= $this->nbaccess->getAccess("add");

		$data["menuList"]	= $this->MMenu->getMenu($this->nbaccess);

 

		$this->parser->parse("manajemenmenu/index",$data);

	}



	private function form($id = ''){

		$data 	= $this->_data;



		if(!empty($id))

		{	

			$data["id"]	     = $id;

			$key             = array("id" => $id);

			$data["default"] = (array) $this->MMenu->get_data_form($key);

		}



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Manajemen Menu', '/cms/settingmenu');

		$this->breadcrumbs->push('Add New Data', '/cms/settingmenu/new');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["action"]     = base_url() . '/cms/settingmenu/save';

		$data["parentopt"]  = $this->MMenu->options('-- Parent --', array('parent'=> 0));



		$this->parser->parse("manajemenmenu/form/form", $data);

	}



	public function add()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// cek valid token_proses

		$this->nbauth->abp($_key['addform']['token'], "GET", "ManajemenMenu", "addform");



		$this->form();

	}



	public function edit()

	{

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }

		

		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "ManajemenMenu", "editform");

		

		$this->form($param['id']);

	}



	public function read()

	{

		$data 	= $this->_data;



		// GET PARAMETER

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));

		

		if($param === false){ redirect('/cms/error/error_proccess'); }

		

        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Manajemen Menu', '/cms/settingmenu');

		$this->breadcrumbs->push('View Data', '/cms/settingmenu/view');



		$key             = array("id" => $param["id"]);

		$data["default"] = (array) $this->MMenu->get_data_form($key);

		$data["crumbs"]  = $this->breadcrumbs->show();



		$this->parser->parse("manajemenMenu/view/view", $data);

	}



	public function simpan()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// get data from post

		$data = !empty($this->input->post("data")) ? $this->input->post("data") : array();

		

		// get id

		$id   = !empty($this->input->post("id")) ? $this->input->post("id") : '';

		

		if(empty($id))

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['saveasnew']['token'], "POST", "ManajemenMenu", "saveasnew");



			// add aditional data

			$data['crtd_at'] 	= date('Y-m-d H:i:s');

			$data['author']		= $this->session->userdata('usr_fname');



			$result = $this->MMenu->insert_to_db($data);

			if($result)

			{	

				$this->session->set_flashdata('success_notif', 'Data berhasil diinsert.');

				redirect('/cms/settingmenu');

			}

		}

		else

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['save']['token'], "POST", "ManajemenMenu", "save");



			// add aditional data

			$data['edtd_at'] 	= date('Y-m-d H:i:s');

			$data['last_edit']	= $this->session->userdata('usr_fname');



			$result = $this->MMenu->update_to_db($data, array("id" => $id));

			

			if($result)

			{	



				$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

				redirect('/cms/settingmenu');

			}

		}

		

		redirect('/cms/error/error_proccess');

	}



	public function delete()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "DELETE", "ManajemenMenu", "delete");



		$result = $this->MMenu->delete(array("id" => $param["id"]));



		if($result === FALSE)

		{

			redirect('/cms/error/error_proccess');

		}

		

		$this->session->set_flashdata('success_notif', 'Data berhasil dihapus.');



        redirect('/cms/settingmenu');

	}



	public function aktifasi()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));

		

		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "ManajemenMenu", "activate");



		$data["is_active"] = $param["status"];

		$result = $this->MMenu->update_to_db($data,array("id" => $param["id"]));



		if($result === FALSE)

		{

			redirect('/cms/error/error_proccess');

		}



		if($param["parent"] == 0)

		{

			$data["is_active"] = $param["status"];

			$result = $this->MMenu->update_to_db($data,array("parent" => $param["id"]));

		}



		$this->session->set_flashdata('success_notif', 'Data berhasil diproses.');



        redirect('/cms/settingmenu');

	}

}

