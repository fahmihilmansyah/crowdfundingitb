<?php



defined('BASEPATH') OR exit('No direct script access allowed');







class Spage_page extends NBAdmin_Controller {



	/**

	 * ----------------------------------------

	 * #spage_page Controller

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_libList   = array();

	private $_modList   = array(

									'nbmenus_model' 		=> 'NBMenus',

									'spage_page_model' 		=> 'spage_page',

									'manajemenakses_model' 	=> 'MAkses'

								);



	protected $_data      = array();



	function __construct()

	{

		parent::__construct();



		// -----------------------------------

		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

		// -----------------------------------

		$this->modelsApp();

		// $this->libraryApp();

	}



	private function modelsApp()

	{		

		$this->load->model($this->_modList);

	}







	private function libraryApp()

	{

		$this->load->library('librariesloader', $this->_libList);

	}



	public function index($data = array())

	{

		$data 	= $this->_data;



        $this->spage_page->register_action_permition();



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Halaman Statis ( Jenis )', '/cms/spage_page');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["can_add"]	= $this->nbaccess->getAccess("add");

		$data["dsource"]	= base_url() . "cms/spage_page/json";



		$this->parser->parse("spage_page/index",$data);

	}







	public function json($roleid = ''){

		$default_order  = 'id';

		$order_field    = '';

		$order_field 	= array(

							'id',

							'name',

						 );



		$order_key 	= (!$this->input->get('iSortCol_0')) ? 0 : $this->input->get('iSortCol_0');

		$order 		= (!$this->input->get('iSortCol_0')) ? $default_order : $order_field[$order_key];

		$sort 		= (!$this->input->get('sSortDir_0')) ? 'DESC': $this->input->get('sSortDir_0');

		$search 	= (!$this->input->get('sSearch'))    ? '' : strtoupper($this->input->get('sSearch'));

		$limit 		= (!$this->input->get('iDisplayLength')) ? $this->_limit : $this->input->get('iDisplayLength');

		$start 		= (!$this->input->get('iDisplayStart'))  ? 0 : $this->input->get('iDisplayStart');



		$data['sEcho'] 			= (!$this->input->get('callback')) ? 0 : $this->input

																			  ->get('callback');



		$data['iTotalRecords'] 	= $this->spage_page->count_all($search,$order_field);



		$data['apps'] 			= $this->spage_page

									   ->get_paged_list($limit, $start, $order, $sort, $search, $order_field)

									   ->result_array();

		$data['akses']			= $this->nbaccess;

		$data['callback'] 		= $this->input->get('callback');



		$this->load->view('spage_page/data/json', $data);

	}



	private function form($id = ''){

		$data 	= $this->_data;



		if(!empty($id))

		{	

			$data["id"]	     = $id;

			$key             = array("id" => $id);

			$data["default"] = (array) $this->spage_page->get_data_form($key);

		}



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Halaman Statis ( Jenis )', '/cms/spage_page');

		$this->breadcrumbs->push('Add New Data', '/cms/spage_page/new');



		$data["crumbs"]     = $this->breadcrumbs->show();

		$data["action"]     = base_url() . '/cms/spage_page/save';

		$data["role_opt"]	= $this->MAkses->options("--Pilih Role--");



		$this->parser->parse("spage_page/form/form", $data);

	}



	public function add()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// cek valid token_proses

		$this->nbauth->abp($_key['addform']['token'], "GET", "spage_page", "addform");



		$this->form();

	}



	public function edit()

	{

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "GET", "spage_page", "editform");



		$this->form($param['id']);

	}



	public function read()

	{

		$data 	= $this->_data;



		// GET PARAMETER

		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param  = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



        $this->breadcrumbs->push('Home', '/cms');

		$this->breadcrumbs->push('Halaman Statis ( Jenis )', '/cms/spage_page');

		$this->breadcrumbs->push('View Data', '/cms/spage_page/view');



		$key             = array("id" => $param["id"]);

		$data["default"] = (array) $this->spage_page->get_data_form($key);

		$data["crumbs"]  = $this->breadcrumbs->show();



		$this->parser->parse("spage_page/view/view", $data);

	}



	public function simpan()

	{

		$_key   = $this->session->userdata("_tokenaction");



		// get data from post

		$data = !empty($this->input->post("data")) ? $this->input->post("data") : array();



		// get id

		$id   = !empty($this->input->post("id")) ? $this->input->post("id") : '';



		if(empty($id))

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['saveasnew']['token'], "POST", "spage_page", "saveasnew");



			// add aditional data

			$data['name_key']		= strtolower(preg_replace('/\s+/', '', $data["name"]));



			$result = $this->spage_page->insert_to_db($data);



			if($result)

			{	

				$this->session->set_flashdata('success_notif', 'Data berhasil diinsert.');

				redirect('/cms/spage_page');

			}

		}



		else

		{

			// cek valid token_proses

			$this->nbauth->abp($_key['save']['token'], "POST", "spage_page", "save");



			// add aditional data

			$data['name_key']		= strtolower(preg_replace('/\s+/', '', $data["name"]));



			$result = $this->spage_page->update_to_db($data, array("id" => $id));

			if($result)

			{	

				$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

				redirect('/cms/spage_page');

			}

		}



		redirect('/cms/error/error_proccess');

	}



	public function delete()

	{

		$param = !empty($_GET["param"]) ? $_GET["param"] : array();

		$param = ARR::getParam(ENKRIP::decode($param));



		if($param === false){ redirect('/cms/error/error_proccess'); }



		// cek valid token_proses

		$this->nbauth->abp($param["token"], "DELETE", "spage_page", "delete");



		$result = $this->spage_page->delete(array("id" => $param["id"]));



		if($result === FALSE)

		{

			redirect('/cms/error/error_proccess');

		}

		

		$this->session->set_flashdata('success_notif', 'Data berhasil dihapus.');



        redirect('/cms/spage_page');

	}

}



