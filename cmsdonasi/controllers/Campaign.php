<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Campaign extends NBAdmin_Controller
{


    /**
     * ----------------------------------------
     * #campaign Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_libList = array();

    private $_modList = array(

        'campaign_model' => 'PostCamp',

        'campaignkategori_model' => 'PostKtg'

    );


    protected $_data = array();


    private $_ap_ori;

    private $_ap_thumb;

    private $_ap_mp;

    private $_bs_ap;

    private $_bs_ap_tumb;


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

        // $this->libraryApp();


        // SET PATH IMAGE LOCATION

        $this->_ap_ori = FCPATH . 'assets/images/campaign/';

        $this->_ap_thumb = FCPATH . 'assets/images/campaign/thumbnails/';

        $this->_ap_nw_thumb = FCPATH . 'assets/images/campaign/new_thumb/';

        $this->_ap_mp = FCPATH . 'assets/images/campaign/mostpopular/';

        $this->_bs_ap = base_url() . 'assets/images/articles/';

        $this->_bs_ap_tumb = base_url() . 'assets/images/articles/';

    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);

    }


    private function libraryApp()

    {

        $this->load->library('librariesloader', $this->_libList);

    }


    public function index($data = array())

    {

        $data = $this->_data;


        $this->PostCamp->register_action_permition();


        $this->breadcrumbs->push('Home', '/cms');

        $this->breadcrumbs->push('campaign', '/cms/campaign');


        $data["crumbs"] = $this->breadcrumbs->show();

        $data["can_add"] = $this->nbaccess->getAccess("add");

        $data["dsource"] = base_url() . "cms/campaign/json";


        $this->parser->parse("campaign/index", $data);

    }


    public function json($roleid = '')
    {

        $sesrolid = $this->session->userdata('usr_role_id');
        $usrid = '';
        if ($sesrolid != 1) {
            $usrid = $this->session->userdata('usr_id');
        }
        $default_order = 'id';

        $order_field = '';


        $order_field = array(

            'id',

            'title',

            'crtd_at',

            'edtd_at',

            'author',

            'last_edit'

        );


        $order_key = (!$this->input->get('iSortCol_0')) ? 0 : $this->input->get('iSortCol_0');

        $order = (!$this->input->get('iSortCol_0')) ? $default_order : $order_field[$order_key];

        $sort = (!$this->input->get('sSortDir_0')) ? 'DESC' : $this->input->get('sSortDir_0');

        $search = (!$this->input->get('sSearch')) ? '' : strtoupper($this->input->get('sSearch'));


        $limit = (!$this->input->get('iDisplayLength')) ? $this->_limit : $this->input->get('iDisplayLength');

        $start = (!$this->input->get('iDisplayStart')) ? 0 : $this->input->get('iDisplayStart');


        $data['sEcho'] = (!$this->input->get('callback')) ? 0 : $this->input
            ->get('callback');


        $data['iTotalRecords'] = $this->PostCamp->count_all($search, $order_field, $usrid);


        $data['apps'] = $this->PostCamp
            ->get_paged_list($limit, $start, $order, $sort, $search, $order_field, $usrid)
            ->result_array();


        $data['akses'] = $this->nbaccess;


        $data['callback'] = $this->input->get('callback');


        $this->load->view('campaign/data/json', $data);

    }


    private function form($id = '')
    {

        $data = $this->_data;


        if (!empty($id)) {

            $data["id"] = $id;

            $key = array("id" => $id);

            $data["default"] = (array)$this->PostCamp->get_data_form($key);

        }


        $this->breadcrumbs->push('Home', '/cms');

        $this->breadcrumbs->push('Manajemen campaign', '/cms/campaign');

        $this->breadcrumbs->push('Add New Data', '/cms/campaign/new');


        $data["crumbs"] = $this->breadcrumbs->show();

        $data["action"] = base_url() . 'cms/campaign/save';

        $data["categories"] = $this->PostKtg->options('-- Pilih Kategori --');

        $data["type"] = array(

            "" => "-- Pilih Type --",

            "news" => "Berita",

            "articles" => "campaign",

            "videos" => "Video"

        );


        $this->parser->parse("campaign/form/form", $data);

    }


    public function add()

    {

        $_key = $this->session->userdata("_tokenaction");


        // cek valid token_proses

        $this->nbauth->abp($_key['addform']['token'], "GET", "campaign", "addform");


        $this->form();

    }


    public function edit()

    {

        $param = !empty($_GET["param"]) ? $_GET["param"] : array();

        $param = ARR::getParam(ENKRIP::decode($param));


        if ($param === false) {
            redirect('/cms/error/error_proccess');
        }


        // cek valid token_proses

        $this->nbauth->abp($param["token"], "GET", "campaign", "editform");


        $this->form($param['id']);

    }


    public function read()

    {

        $data = $this->_data;


        // GET PARAMETER

        $param = !empty($_GET["param"]) ? $_GET["param"] : array();

        $param = ARR::getParam(ENKRIP::decode($param));


        if ($param === false) {
            redirect('/cms/error/error_proccess');
        }


        $this->breadcrumbs->push('Home', '/cms');

        $this->breadcrumbs->push('campaign', '/cms/campaign');

        $this->breadcrumbs->push('View Data', '/cms/campaign/view');


        $key = array("id" => $param["id"]);

        $data["default"] = (array)$this->PostCamp->get_data_form($key);

        $data["crumbs"] = $this->breadcrumbs->show();


        $this->parser->parse("campaign/view/view", $data);

    }


    public function simpan()

    {

        $_key = $this->session->userdata("_tokenaction");


        // get data from post

        $data = !empty($this->input->post("data")) ? $this->input->post("data") : array();


        // get id

        $id = !empty($this->input->post("id")) ? $this->input->post("id") : '';


        // SETTING UPLOAD

        $this->upload->initialize(array(

            "upload_path" => $this->_ap_ori,

            "allowed_types" => "png|jpg|jpeg|gif",

            "overwrite" => TRUE,

            "encrypt_name" => TRUE,

            "remove_spaces" => TRUE,

            "max_size" => 30000,

            "xss_clean" => FALSE,

            "file_name" => array("image_" . date('Ymdhis'))

        ));

        $data["slug"] = URI::slug($data["title"], '');

        if (empty($id)) {
            $caricamp = $this->db->from('nb_campaign')->where(['slug' => $data["slug"]])->get()->num_rows();
            $data["slug"] = !empty($caricamp) ? $data["slug"] . ($caricamp + 1) : $data["slug"];

            // cek valid token_proses

            $this->nbauth->abp($_key['saveasnew']['token'], "POST", "campaign", "saveasnew");


            if (!empty($_FILES)) {

                if ($this->upload->do_multi_upload("uploadedimages")) {

                    $return = $this->upload->get_multi_upload_data();


                    $data['img'] = $return[0]["orig_name"];


                    //resize_img($this->_ap_thumb, $return);

                    //fit_img($this->_ap_thumb_main,$return);

                    resize_img($this->_ap_thumb, $return);

                    fit_img($this->_ap_nw_thumb, $return);

                    fit_img_mp($this->_ap_mp, $return);

                } else {

                    redirect('/cms/error/error_upload');

                }

            }


            // add aditional data

            $data['target'] = ARR::rupiah($data['target'], 'normal');

            $data['valid_date'] = ARR::dFormat($data['valid_date'], 'Y-m-d');

            $data['post_date'] = date('Y-m-d H:i:s');

            $data['crtd_at'] = date('Y-m-d H:i:s');

            $data['edtd_at'] = date('Y-m-d H:i:s');

            $data['user_post'] = $this->session->userdata('usr_id');

            $data['user_type'] = 'ad';

            $data['author'] = $this->session->userdata('usr_fname');


            $result = $this->PostCamp->insert_to_db($data);

            if ($result) {

                $this->session->set_flashdata('success_notif', 'Data berhasil diinsert.');

                redirect('/cms/campaign');

            }

        } else {

            // cek valid token_proses

            $this->nbauth->abp($_key['save']['token'], "POST", "campaign", "save");


            // add aditional data

            $data['target'] = ARR::rupiah($data['target'], 'normal');

            $data['valid_date'] = ARR::dFormat($data['valid_date'], 'Y-m-d');

            $data['edtd_at'] = date('Y-m-d H:i:s');

            $data['last_edit'] = $this->session->userdata('usr_fname');


            $result = $this->PostCamp->update_to_db($data, array("id" => $id));


            if (!empty($_FILES["uploadedimages"]["name"][0])) {

                if ($this->upload->do_multi_upload("uploadedimages")) {

                    $return = $this->upload->get_multi_upload_data();


                    $data['img'] = $return[0]["orig_name"];

//                    print_r($return);
//                    die();
                    resize_img($this->_ap_thumb, $return);

                    fit_img($this->_ap_nw_thumb, $return);

                    fit_img_mp($this->_ap_mp, $return);
//                    imgcampaigndetail($data['img']);

                    // DELETE IMAGE FROM FOLDER

                    $ori_img = FCPATH . 'assets/images/articles/' . $this->input->post("img");
                    if (file_exists($ori_img)) {
                        unlink($this->_ap_ori . $this->input->post("img"));
                    }

                    $thumb_img = FCPATH . 'assets/images/articles/thumbnails/' . $this->input->post("img");
                    if (file_exists($thumb_img)) {
                        unlink($this->_ap_thumb . $this->input->post("img"));
                    }

                    $nw_thumb_img = FCPATH . 'assets/images/articles/new_thumb/' . $this->input->post("img");
                    if (file_exists($nw_thumb_img)) {
                        unlink($this->_ap_nw_thumb . $this->input->post("img"));
                    }

                    $mp_img = FCPATH . 'assets/images/articles/mostpopular/' . $this->input->post("img");
                    if (file_exists($mp_img)) {
                        unlink($this->_ap_mp . $this->input->post("img"));
                    }


                    // echo $this->input->post("img"); exit();

                    $key = array("id" => $id);

                    $result = $this->PostCamp->update_to_db($data, $key);


                    if ($result) {

                        $this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

                        // exit();

                        redirect('/cms/campaign');

                    }

                } else {

                    // echo $this->upload->display_errors();

                    // exit();

                    redirect('/cms/error/error_upload');

                }

            } else {

                $key = array("id" => $id);

                $result = $this->PostCamp->update_to_db($data, $key);


                if ($result) {

                    $this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

                    redirect('/cms/campaign');

                }

            }

        }


        redirect('/cms/error/error_proccess');

    }


    public function delete()

    {

        $param = !empty($_GET["param"]) ? $_GET["param"] : array();

        $param = ARR::getParam(ENKRIP::decode($param));


        if ($param === false) {
            redirect('/cms/error/error_proccess');
        }


        // cek valid token_proses

        $this->nbauth->abp($param["token"], "DELETE", "campaign", "delete");


        $result = $this->PostCamp->delete(array("id" => $param["id"]));


        if ($result === FALSE) {

            redirect('/cms/error/error_proccess');

        }


        $this->session->set_flashdata('success_notif', 'Data berhasil dihapus.');


        redirect('/cms/campaign');

    }


    public function aktifasi()

    {

        $param = !empty($_GET["param"]) ? $_GET["param"] : array();

        $param = ARR::getParam(ENKRIP::decode($param));


        if ($param === false) {
            redirect('/cms/error/error_proccess');
        }


        // cek valid token_proses

        $this->nbauth->abp($param["token"], "GET", "campaign", "activate");


        $data["is_active"] = $param["status"];

        $result = $this->PostCamp->update_to_db($data, array("id" => $param["id"]));


        if ($result === FALSE) {

            redirect('/cms/error/error_proccess');

        }


        $this->session->set_flashdata('success_notif', 'Data berhasil diproses.');


        redirect('/cms/campaign');

    }

    function manualdonation()
    {

        $sesrolid = $this->session->userdata('usr_role_id');
//        print_r($_SESSION);exit;
        if ($sesrolid == 1) {
            if ($_POST):
                $id = $_POST['id'];
                $nominal =  str_replace(".", '', $this->input->post('nominal'));
                $this->db->query('UPDATE nb_campaign SET now = now + '.$nominal.' where id='.$this->db->escape($id));
                $notrx = date('Ymd') . sprintf('%05d', Fhhlib::geninc());
                $insertData = array(
                    'no_transaksi' => $notrx,
                    'nominal' => $nominal,
                    'trx_date' => date('Y-m-d'),
                    'campaign' => $id,
                    'donatur' => 0,/*ubah donaturnya jika login maka akan mengambil session loginnya*/
                    'bank' => 'TANPAAPLIKASI',
                    'comment' => '',
                    'uniqcode' => 0,//($methodpay == 919 ? 0 : $uniqcode),
                    'namaDonatur' =>  $this->session->userdata('usr_fname').'|'.$this->session->userdata('usr_id'),
                    'emailDonatur' => '',
                    'telpDonatur' => '',
                    'nomuniq' => $nominal,
                    'validDate' => date('Y-m-d', strtotime('+2 days')),
                    'crtd_at' => date("Y-m-d H:i:s"),
                    'edtd_at' => date("Y-m-d H:i:s"),
                    'month' => date("m"),
                    'year' => date("Y"),
                    'status'=>'verified'
                );
                $this->db->insert('nb_trans_donation',$insertData);
                $this->session->set_flashdata('success_notif', 'Data berhasil diproses.Nominal '.$nominal);

            endif;
        } else {
            $this->session->set_flashdata('error_notif', 'Anda tidak memiliki akses.');
        }
        redirect('/cms/campaign');
    }

}

