<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Artikel extends NBAdmin_Controller
{


    /**
     * ----------------------------------------
     * #artikel Controller
     * ----------------------------------------
     * #author            : Fahmi Hilmansyah
     * #time created    : 4 January 2017
     * #last developed  : Fahmi Hilmansyah
     * #last updated    : 4 January 2017
     * ----------------------------------------
     */


    private $_libList = array();

    private $_modList = array(

        'artikel_model' => 'PostArt',

        'artikelkategori_model' => 'PostKtg'

    );


    protected $_data = array();


    private $_ap_ori;

    private $_ap_thumb;

    private $_ap_mp;

    private $_ap_nw_thumb;

    private $_bs_ap;

    private $_bs_ap_tumb;


    function __construct()

    {

        parent::__construct();


        // -----------------------------------

        //  BUKA KOMENTAR JIKA AKAN DIGUNAKAN

        // -----------------------------------

        $this->modelsApp();

        // $this->libraryApp();


        // SET PATH IMAGE LOCATION

        $this->_ap_ori = FCPATH . 'assets/images/articles/';

        $this->_ap_thumb = FCPATH . 'assets/images/articles/thumbnails/';

        $this->_ap_nw_thumb = FCPATH . 'assets/images/articles/new_thumb/';

        $this->_ap_mp = FCPATH . 'assets/images/articles/mostpopular/';

        $this->_bs_ap = base_url() . 'assets/images/articles/';

        $this->_bs_ap_tumb = base_url() . 'assets/images/articles/';

    }


    private function modelsApp()

    {

        $this->load->model($this->_modList);

    }


    private function libraryApp()

    {

        $this->load->library('librariesloader', $this->_libList);

    }


    public function index($data = array())

    {

        $data = $this->_data;


        $this->PostArt->register_action_permition();


        $this->breadcrumbs->push('Home', '/cms');

        $this->breadcrumbs->push('Artikel', '/cms/artikel');


        $data["crumbs"] = $this->breadcrumbs->show();

        $data["can_add"] = $this->nbaccess->getAccess("add");

        $data["dsource"] = base_url() . "cms/artikel/json";


        $this->parser->parse("artikel/index", $data);

    }


    public function json($roleid = '')
    {

        $default_order = 'id';

        $order_field = '';


        $order_field = array(

            'id',

            'title',

            'crtd_at',

            'edtd_at',

            'author',

            'last_edit'

        );


        $order_key = (!$this->input->get('iSortCol_0')) ? 0 : $this->input->get('iSortCol_0');

        $order = (!$this->input->get('iSortCol_0')) ? $default_order : $order_field[$order_key];

        $sort = (!$this->input->get('sSortDir_0')) ? 'DESC' : $this->input->get('sSortDir_0');

        $search = (!$this->input->get('sSearch')) ? '' : strtoupper($this->input->get('sSearch'));


        $limit = (!$this->input->get('iDisplayLength')) ? $this->_limit : $this->input->get('iDisplayLength');

        $start = (!$this->input->get('iDisplayStart')) ? 0 : $this->input->get('iDisplayStart');


        $data['sEcho'] = (!$this->input->get('callback')) ? 0 : $this->input
            ->get('callback');


        $data['iTotalRecords'] = $this->PostArt->count_all($search, $order_field);


        $data['apps'] = $this->PostArt
            ->get_paged_list($limit, $start, $order, $sort, $search, $order_field)
            ->result_array();


        $data['akses'] = $this->nbaccess;


        $data['callback'] = $this->input->get('callback');


        $this->load->view('artikel/data/json', $data);

    }


    private function form($id = '')
    {

        $data = $this->_data;


        if (!empty($id)) {

            $data["id"] = $id;

            $key = array("id" => $id);

            $data["default"] = (array)$this->PostArt->get_data_form($key);

        }


        $this->breadcrumbs->push('Home', '/cms');

        $this->breadcrumbs->push('Manajemen Artikel', '/cms/artikel');

        $this->breadcrumbs->push('Add New Data', '/cms/artikel/new');


        $data["crumbs"] = $this->breadcrumbs->show();

        $data["action"] = base_url() . 'cms/artikel/save';

        $data["categories"] = $this->PostKtg->options('-- Pilih Kategori --');

        $data["type"] = array(
            "" => "-- Pilih Type --",
            "news" => "Berita",
            "articles" => "Artikel",
            "videos" => "Video",
            "promosi" => "Promosi"

        );
        $this->parser->parse("artikel/form/form", $data);

    }


    public function add()

    {

        $_key = $this->session->userdata("_tokenaction");


        // cek valid token_proses

        $this->nbauth->abp($_key['addform']['token'], "GET", "artikel", "addform");


        $this->form();

    }


    public function edit()

    {

        $param = !empty($_GET["param"]) ? $_GET["param"] : array();

        $param = ARR::getParam(ENKRIP::decode($param));


        if ($param === false) {
            redirect('/cms/error/error_proccess');
        }


        // cek valid token_proses

        $this->nbauth->abp($param["token"], "GET", "artikel", "editform");


        $this->form($param['id']);

    }


    public function read()

    {

        $data = $this->_data;


        // GET PARAMETER

        $param = !empty($_GET["param"]) ? $_GET["param"] : array();

        $param = ARR::getParam(ENKRIP::decode($param));


        if ($param === false) {
            redirect('/cms/error/error_proccess');
        }


        $this->breadcrumbs->push('Home', '/cms');

        $this->breadcrumbs->push('Artikel', '/cms/artikel');

        $this->breadcrumbs->push('View Data', '/cms/artikel/view');


        $key = array("id" => $param["id"]);

        $data["default"] = (array)$this->PostArt->get_data_form($key);

        $data["crumbs"] = $this->breadcrumbs->show();


        $this->parser->parse("artikel/view/view", $data);

    }


    public function simpan()

    {

        $_key = $this->session->userdata("_tokenaction");


        // get data from post

        $data = !empty($this->input->post("data")) ? $this->input->post("data") : array();


        // get id

        $id = !empty($this->input->post("id")) ? $this->input->post("id") : '';


        // SETTING UPLOAD

        $this->upload->initialize(array(

            "upload_path" => $this->_ap_ori,

            "allowed_types" => "png|jpg|jpeg|gif",

            "overwrite" => TRUE,

            "encrypt_name" => TRUE,

            "remove_spaces" => TRUE,

            "max_size" => 30000,

            "xss_clean" => FALSE,

            "file_name" => array("image_" . date('Ymdhis'))

        ));

        $data["slug"] = URI::slug($data["title"]);
        $caricamp = $this->db->from('nb_post')->where(['slug' => $data["slug"]])->get()->num_rows();
        $data["slug"] = !empty($caricamp) ? $data["slug"] . ($caricamp + 1) : $data["slug"];
        if (empty($id)) {

            // cek valid token_proses

            $this->nbauth->abp($_key['saveasnew']['token'], "POST", "artikel", "saveasnew");


            if (!empty($_FILES)) {

                if ($this->upload->do_multi_upload("uploadedimages")) {

                    $return = $this->upload->get_multi_upload_data();


                    $data['img'] = $return[0]["orig_name"];


                    // resize_img($this->_ap_thumb, $return);
                    // fit_img($this->_ap_thumb,$return);

                    resize_img($this->_ap_thumb, $return);

                    fit_img($this->_ap_nw_thumb, $return);

                    fit_img_mp($this->_ap_mp, $return);

                } else {

                    redirect('/cms/error/error_upload');

                }

            }


            // add aditional data

            $data['post_date'] = date('Y-m-d H:i:s');

            $data['crtd_at'] = date('Y-m-d H:i:s');

            $data['edtd_at'] = date('Y-m-d H:i:s');

            $data['author'] = $this->session->userdata('usr_fname');
            $data['usr_post'] = $this->session->userdata('usr_id');


            $result = $this->PostArt->insert_to_db($data);

            if ($result) {

                $this->session->set_flashdata('success_notif', 'Data berhasil diinsert.');

                redirect('/cms/artikel');

            }

        } else {

            // cek valid token_proses

            $this->nbauth->abp($_key['save']['token'], "POST", "artikel", "save");


            // add aditional data

            $data['edtd_at'] = date('Y-m-d H:i:s');

            $data['last_edit'] = $this->session->userdata('usr_fname');


            $result = $this->PostArt->update_to_db($data, array("id" => $id));


            if (!empty($_FILES["uploadedimages"]["name"][0])) {

                if ($this->upload->do_multi_upload("uploadedimages")) {

                    $return = $this->upload->get_multi_upload_data();


                    $data['img'] = $return[0]["orig_name"];


                    // resize_img($this->_ap_thumb, $return);

                    // thumb($return,300,300,$this->_ap_thumb);

                    resize_img($this->_ap_thumb, $return);

                    fit_img($this->_ap_nw_thumb, $return);

                    fit_img_mp($this->_ap_mp, $return);


                    // DELETE IMAGE FROM FOLDER

                    $ori_img = FCPATH . 'assets/images/articles/' . $this->input->post("img");
                    if (file_exists($ori_img)) {
                        unlink($this->_ap_ori . $this->input->post("img"));
                    }

                    $thumb_img = FCPATH . 'assets/images/articles/thumbnails/' . $this->input->post("img");
                    if (file_exists($thumb_img)) {
                        unlink($this->_ap_thumb . $this->input->post("img"));
                    }

                    $nw_thumb_img = FCPATH . 'assets/images/articles/new_thumb/' . $this->input->post("img");
                    if (file_exists($nw_thumb_img)) {
                        unlink($this->_ap_nw_thumb . $this->input->post("img"));
                    }

                    $mp_img = FCPATH . 'assets/images/articles/mostpopular/' . $this->input->post("img");
                    if (file_exists($mp_img)) {
                        unlink($this->_ap_mp . $this->input->post("img"));
                    }

                    // echo $this->input->post("img"); exit();

                    $key = array("id" => $id);

                    $result = $this->PostArt->update_to_db($data, $key);


                    if ($result) {

                        $this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

                        // exit();

                        redirect('/cms/artikel');

                    }

                } else {

                    // echo $this->upload->display_errors();

                    // exit();

                    redirect('/cms/error/error_upload');

                }

            } else {

                $key = array("id" => $id);

                $result = $this->PostArt->update_to_db($data, $key);


                if ($result) {

                    $this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');

                    redirect('/cms/artikel');

                }

            }

        }

        redirect('/cms/error/error_proccess');

    }


    public function delete()

    {

        $param = !empty($_GET["param"]) ? $_GET["param"] : array();

        $param = ARR::getParam(ENKRIP::decode($param));


        if ($param === false) {
            redirect('/cms/error/error_proccess');
        }


        // cek valid token_proses

        $this->nbauth->abp($param["token"], "DELETE", "artikel", "delete");


        $result = $this->PostArt->delete(array("id" => $param["id"]));


        if ($result === FALSE) {

            redirect('/cms/error/error_proccess');

        }


        $this->session->set_flashdata('success_notif', 'Data berhasil dihapus.');


        redirect('/cms/artikel');

    }


    public function aktifasi()

    {

        $param = !empty($_GET["param"]) ? $_GET["param"] : array();

        $param = ARR::getParam(ENKRIP::decode($param));


        if ($param === false) {
            redirect('/cms/error/error_proccess');
        }


        // cek valid token_proses

        $this->nbauth->abp($param["token"], "GET", "artikel", "activate");


        $data["is_active"] = $param["status"];

        $result = $this->PostArt->update_to_db($data, array("id" => $param["id"]));


        if ($result === FALSE) {

            redirect('/cms/error/error_proccess');

        }


        $this->session->set_flashdata('success_notif', 'Data berhasil diproses.');


        redirect('/cms/artikel');

    }

}

