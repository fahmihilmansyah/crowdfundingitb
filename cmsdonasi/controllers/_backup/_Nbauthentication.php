<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Nbauthentication extends CI_Controller {



	/**

	 * ----------------------------------------

	 * #NB Auth

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_appTitle;

	private $_libList   = array('nbauth','myphpmailer');

	private $_modList   = array();



	function __construct()

	{

		parent::__construct();



		$this->initApp();

		$this->modelsApp();

		$this->libraryApp();

	}



	private function modelsApp()

	{		

		if(!empty($this->_modList));

			$this->load->model($this->_modList);

	}



	private function libraryApp()

	{

		if(!empty($this->_libList));

			$this->load->library('librariesloader', $this->_libList);

	}



	private function initApp()

	{

		$this->_appTitle = "YDSF - Cpanel Page";

	}



	public function pauth($data = array())

	{

		$post					= $this->input->post(NULL,TRUE);



		$param                  = array(

									"uname"	=> !empty($post["uname"]) ? $post["uname"] : "",

									"pwd"	=> !empty($post["pwd"]) ? $post["pwd"] : ""

									);



		$auth  					= $this->nbauth->getAuth($param);



		if($auth->num_rows() > 0){

			$role_active 	 = $auth->row()->role_active;

			$status_pengguna = $auth->row()->is_active;



			if($role_active == 0 || $status_pengguna == 0)

			{

				$this->session->set_flashdata('error', 'Role/Pengguna belum aktif');

				$this->session->set_flashdata('uname', $param["uname"]);

				$this->session->set_flashdata('pwd', $param["pwd"]);



				redirect('cms/login');

			}



			$this->nbauth->registerUsersAuth($auth->row());



			redirect('/cms');

		}else{

			$this->session->set_flashdata('error', INCORRECT_UNAMEPWD);

			$this->session->set_flashdata('uname', $param["uname"]);

			$this->session->set_flashdata('pwd', $param["pwd"]);



			redirect('cms/login');

		}

	}



	public function login_page(){

		$data["_appTitle"] = $this->_appTitle;



		$this->load->view("login", $data);

	}



	public function error_page($page = ''){

		$this->load->view('errors/html/' . $page);

	}



	public function token_expired(){

		$this->error_page('error_expiredtoken');

	}



	public function cauth_dt(){

		$result = $this->nbauth->cauth_dt();



		echo json_encode($result);

	}	



	public function logout(){

		// delete nb_action_permit

		$this->db->delete('nb_action_permit', array('user_id' => $this->session->userdata('usr_id')));



		// delete session action

		$this->session->sess_destroy();



		redirect('cms/login');

	}

	public function pg_forgot_pass()
	{
		$data["_appTitle"] = $this->_appTitle;

		$this->parser->parse("forgotpass",$data);
	}

	public function forgot_pass()
	{
		$mail  = $this->input->post("email");

		$result = $this->nbauth->cekMail($mail);

		if($result > 0)
		{

			$token = md5(md5($mail) . time()); 
			
			$data["fp_token"] = $token;
			$data["fp_valid"] = date("Y-m-d H:i:s", strtotime("+2 hour"));

			$key 	= array("email" => $mail);

			$result = $this->db->update("nb_sys_users", $data, $key);

			if($result)
			{
				$dtmail["token"] = $token;
				$dtmail["email"] = $mail;

				$this->sendMail($dtmail, "forgotpass");

				$this->session->set_flashdata('success', 'Password telah dikirim ke email kamu, silahkan cek inbox!');
			}
			else
			{
				$this->session->set_flashdata('success', 'Password telah dikirim ke email kamu, silahkan cek inbox!');
			}

			redirect('cms/forgot');
		}
		else
		{
			$this->session->set_flashdata('error', 'Email kamu tidak terdaftar dalam sistem!');

			$this->session->set_flashdata('uname', $param["email"]);

			redirect('cms/forgot');
		}
	}



	private function sendMail($data = array(), $view = "aktifasi")
    {

        $mail = new PHPMailer();

        $mail->IsSMTP(); // we are going to use SMTP

        $mail->SMTPAuth = true; // enabled SMTP authentication

        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server

        $mail->Host = "srv10.niagahoster.com";      // setting GMail as our SMTP server

        // $mail->Host       	= "ssl://smtp.gmail.com";      // setting GMail as our SMTP server

        $mail->Port = 465;                   // SMTP port to connect to GMail

        $mail->Username = "noreply@niatbaik.co.id";  // user email address

        $mail->Password = "#niatbaik2017#";            // password in GMail

        $mail->IsHTML(true);

        $mail->SetFrom($mail->Username, 'No Reply');  //Who is sending the email

        $mail->AddReplyTo($mail->Username, 'No Reply');  //email address that receives the response

        $mail->Subject = "Reset Password Administrator";

        $mail->Body = $this->load->view('mailtemplate/' . $view, $data, true);

        // $mail->Body       	= $this->load->view('register/mailtemplate/aktifasi',$data,true);

        // $mail->AltBody    	= "Tes Pesan HTML";

        // $destino = "fahmi.hilmansyah@gmail.com"; // Who is addressed the email to

        $mail->AddAddress($data["email"]);


        // $mail->AddAttachment("images/phpmailer.gif");      // some attached files

        // $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want

        if (!$mail->Send()) {

            $data["message"] = "Error: " . $mail->ErrorInfo;
            exit();

        } else {

            $data["message"] = "Message sent correctly!";

        }
    }

    public function pg_reset_new_pass()
	{
		$this->load->model("Nbauths_model", "NBM");

		$data["_appTitle"] 		= $this->_appTitle;
		$data["_token_pass"] 	= $this->input->get("token_pass");

		$result 				= $this->NBM->ifTokenExists($data["_token_pass"]);

		if($result > 0)
		{
			$result     = $this->NBM->getValidTimeToken($data["_token_pass"]);
			$result     = !empty($result) ? $result->fp_valid : "";

			if(empty($result))
			{
				$this->error_page('error_invalidtoken');
			}
			else
			{
				$now   		= new DateTime(date("Y-m-d H:i:s"));
				$validtime	= new DateTime($result);

				if($now > $validtime)
				{
					$this->error_page('error_invalidtoken');
				}
				else
				{
					$this->parser->parse("setnewpwd",$data);
				}
			}
		}
		else
		{
			$this->error_page('error_invalidtoken');
		}
	}

	public function reset_new_pass()
	{
		$this->load->model("Nbauths_model", "NBM");

		$token 	= $this->input->post("token_pass");

		$pwd 	= $this->input->post("pwd");
		$pwd 	= $this->nbauth->getCustomHash($pwd);

		$result = $this->NBM->ifTokenExists($token);

		if($result > 0)
		{
			$key 		 = array("fp_token" => $token);
			$data["pwd"] = $pwd;
			$result 	 = $this->db->update("nb_sys_users", $data, $key);

			$this->session->set_flashdata('success', 'Password telah berhasil dirubah!');

			redirect('cms/login');
		}else{
			redirect('cms/reset/pg_new_pwd?token_pass=' . $token);
		}
	}
}

