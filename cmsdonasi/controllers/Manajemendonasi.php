<?php /** * Created by PhpStorm. * User: Fahmi hilmansyah * Date: 3/27/2017 * Time: 10:21 AM */
class Manajemendonasi extends NBAdmin_Controller
{
    protected $_data = array();
    private $_libList = array();
    private $_modList = array('Newcustom_model' => 'cusmdl');
    private $notif = "";

    function __construct()
    {
        parent::__construct();
        $this->modelsApp();
    }

    private function modelsApp()
    {
        $this->load->model($this->_modList);
    }

    function index()
    {
        $sesrolid =  $this->session->userdata('usr_role_id');
        $usrid = '';
        if($sesrolid != 1){
            $usrid = $this->session->userdata('usr_id');
        }
        $data = $this->_data;
        $this->breadcrumbs->push('Home', '/cms');
        $this->breadcrumbs->push('Manajemen Donasi Campaign', '/cms/settingakses');
        $data["crumbs"] = $this->breadcrumbs->show();
        $data['linkurl'] = "manajemendonasi/viewverifikasi/";
		$data['linkurlcancel'] = "manajemendonasi/doCancelDonation/";
        $kondisi['where'] = array('nb_trans_donation.status not in' => "('verified', 'cancel')");
        if(!empty($usrid)){
            $kondisi['where']['nb_campaign.user_post'] = ($usrid) ;
        }
        $kondisi['join'] = array(
            array('table' => "nb_campaign", 'condition' => "nb_campaign.id = nb_trans_donation.campaign", "type" => "left"), 
            array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_donation.bank", "type" => "left"),
            array('table' => "nb_trans_konfirmasi", 'condition' => "nb_trans_donation.no_transaksi = nb_trans_konfirmasi.transaksi", "type" => "left")
        );
        $kondisi['order_by'] = array('field' => 'nb_trans_donation.no_transaksi', 'type' => 'DESC');
        $summary = $this->cusmdl->custom_query("nb_trans_donation", $kondisi, "nb_bank.namaBank, nb_trans_donation.nomuniq, nb_trans_donation.namaDonatur, nb_trans_konfirmasi.transaksi, nb_trans_donation.no_transaksi, nb_trans_donation.status, nb_campaign.title")->result();
        $data['listDonasi'] = $summary;
        $this->parser->parse("manajemendonasi/adm_donasi_program", $data);
    }

    function doVerifikasiCampaign($id = null)
    {
        $kondisi['where'] = array("nb_trans_donation.no_transaksi" => $this->db->escape($id), "nb_trans_donation.status" => $this->db->escape("unverified"));
        $cekdonasi = $this->cusmdl->custom_query("nb_trans_donation", $kondisi)->result();
        if (count($cekdonasi) != 1) {
            $this->notif = array('error' => 'Ada kesalahan dalam proses data!');
            $this->session->set_flashdata($this->notif);
            redirect('cms/aprovedonation');
            exit;
        } else {
            $campaign = $cekdonasi[0]->campaign;
            $nomuniq = $cekdonasi[0]->nomuniq;
            $notransaksi = $cekdonasi[0]->no_transaksi;
            $iddonatur = $cekdonasi[0]->donatur;
            $kodebank = $cekdonasi[0]->bank;
            $kondisi1['where'] = array("nb_campaign.id" => $campaign);
            $cekcampaign = $this->cusmdl->custom_query("nb_campaign", $kondisi1)->result();
            if (count($cekcampaign) > 0) {
                $sumdonatur = (int)$cekcampaign[0]->sum_donatur;
                $now = (int)$cekcampaign[0]->now;                /*updated nb_campaign*/
                $data = array("sum_donatur" => (int)$sumdonatur + 1, 'now' => (int)$now + (int)$nomuniq, 'edtd_at' => date("Y-m-d H:i:s"));
                $where = array("nb_campaign.id" => ($campaign),);
                $this->cusmdl->custom_update("nb_campaign", $data, $where);                /*update nb_trans_donation*/
                $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));
                $where = array("nb_trans_donation.no_transaksi" => ($id),);
                $this->cusmdl->custom_update("nb_trans_donation", $data, $where);                /*jika kode bank nya 919 == potong deposit*/
                if ($kodebank == 919) {
                    $this->trxDeposit($iddonatur, $nomuniq, $notransaksi, "TRX");
                }
                $this->notif = array('success' => 'Berhasil memproses data!');
                $this->session->set_flashdata($this->notif);
                redirect('cms/aprovedonation');
            }
        }
    }

    function trxDeposit($iddonatur = null, $nominal = null, $notransaksi = null, $jenis = null)
    {
        $kondisi['where'] = array("nb_donaturs_saldo.donatur" => $this->db->escape($iddonatur));
        $ceksaldo = $this->cusmdl->custom_query("nb_donaturs_saldo", $kondisi)->result();
        if (count($ceksaldo) != 1) {
            die("cannot proses");
        } else {
            $balanceAmount = 0;
            $prevBlance = $ceksaldo[0]->balanceAmount;
            if ($jenis == "DEPOSIT") {
                $balanceAmount = (int)$nominal + (int)$ceksaldo[0]->balanceAmount;
                $data = array('donaturs' => $iddonatur, 'jenistrx' => "DEPO", 'id_transaksi' => $notransaksi, 'prevBalance' => $prevBlance, 'amount' => $nominal, 'balance' => $balanceAmount, 'crtd_at' => date("Y-m-d H:i:s"), 'edtd_at' => date("Y-m-d H:i:s"),);
                $this->cusmdl->custom_insert('nb_donaturs_trx', $data);
            } else {
                $balanceAmount = (int)$ceksaldo[0]->balanceAmount - (int)$nominal;
                if ($balanceAmount < 0) {
                    die("Maaf Saldo Tidak Cukup");
                }
                $data = array('donaturs' => $iddonatur, 'jenistrx' => "TRX", 'id_transaksi' => $notransaksi, 'prevBalance' => $prevBlance, 'amount' => $nominal, 'balance' => $balanceAmount, 'crtd_at' => date("Y-m-d H:i:s"), 'edtd_at' => date("Y-m-d H:i:s"),);
                $this->cusmdl->custom_insert('nb_donaturs_trx', $data);
            }
            $data = array("prevBalance" => $prevBlance, 'balanceAmount' => $balanceAmount, 'edtd_at' => date("Y-m-d H:i:s"));
            $where = array("nb_donaturs_saldo.donatur" => $iddonatur);
            $this->cusmdl->custom_update("nb_donaturs_saldo", $data, $where);
        }
    }

    function program()
    {
        $data = $this->_data;
        $this->breadcrumbs->push('Home', '/cms');
        $this->breadcrumbs->push('Manajemen Donasi Program', '/cms/settingakses');
        $data["crumbs"] = $this->breadcrumbs->show();
        $data['linkurl'] = "manajemendonasi/viewverifikasi/";
        $data['linkurlcancel'] = "manajemendonasi/doCancelProgram/";
//        $kondisi['where'] = array('nb_trans_program.status not in' => "('verified' , 'cancel')");
        $kondisi['join'] = array(
            array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_program.bank", "type" => "left"), 
            array('table' => "nb_trans_konfirmasi", 'condition' => "nb_trans_program.no_transaksi = nb_trans_konfirmasi.transaksi", "type" => "left"),
            array('table' => "nb_trans_ipg", 'condition' => "nb_trans_ipg.trxid = nb_trans_program.no_transaksi", "type" => "left")
        );
        $kondisi['order_by'] = array('field' => 'nb_trans_program.no_transaksi', 'type' => 'DESC');
        $summary = $this->cusmdl->custom_query("nb_trans_program", $kondisi, "nb_bank.namaBank, nb_trans_program.nomuniq, nb_trans_program.namaDonatur, nb_trans_konfirmasi.transaksi, (case when nb_trans_konfirmasi.transaksi is null then nb_trans_ipg.id else  nb_trans_konfirmasi.transaksi end ) transaksi,nb_trans_program.no_transaksi, nb_trans_program.status, nb_trans_program.jenistrx")->result();
        $data['listDonasi'] = $summary;
        $this->parser->parse("manajemendonasi/adm_donasi_program", $data);
    }

    function doCancelProgram($notrans = null)
    {
        $kondisi['where'] = array("nb_trans_program.no_transaksi" => $this->db->escape($notrans), "nb_trans_program.status" => $this->db->escape("cancel"));
        $cekdonasi = $this->cusmdl->custom_query("nb_trans_program", $kondisi)->result();
        if (count($cekdonasi) > 0) {
            $this->notif = array('error' => 'Ada kesalahan dalam proses data!');
            $this->session->set_flashdata($this->notif);
            redirect('cms/aproveprogram');
            exit;
            die("cannot proses");
        } else {
            $data = array("status" => "cancel", 'edtd_at' => date("Y-m-d H:i:s"));
            $where = array("nb_trans_program.no_transaksi" => ($notrans),);
            $this->cusmdl->custom_update("nb_trans_program", $data, $where);
            redirect('cms/aproveprogram');
            exit;
        }
    }
    function doCancelDonation($notrans = null)
    {
        $kondisi['where'] = array("nb_trans_donation.no_transaksi" => $this->db->escape($notrans), "nb_trans_donation.status" => $this->db->escape("cancel"));
        $cekdonasi = $this->cusmdl->custom_query("nb_trans_donation", $kondisi)->result();
        if (count($cekdonasi) > 0) {
            $this->notif = array('error' => 'Ada kesalahan dalam proses data!');
            $this->session->set_flashdata($this->notif);
            redirect('cms/aprovedonation');
            exit;
            die("cannot proses");
        } else {
            $data = array("status" => "cancel", 'edtd_at' => date("Y-m-d H:i:s"));
            $where = array("nb_trans_donation.no_transaksi" => ($notrans),);
            $this->cusmdl->custom_update("nb_trans_donation", $data, $where);
            redirect('cms/aprovedonation');
            exit;
        }
    }

    function viewverifikasi($notrans = null)
    {
        $data = $this->_data;
        $this->breadcrumbs->push('Home', '/cms');
        $this->breadcrumbs->push('Manajemen Donasi Program', '/cms/settingakses');
        $data["crumbs"] = $this->breadcrumbs->show();
        $qry = "SELECT * FROM (SELECT
               nb_trans_donation.no_transaksi,
               upper(nb_campaign.title)AS title,
               upper(nb_bank.namaBank) as namaBank,
               nb_trans_donation.nomuniq,
               nb_trans_donation.nominal,
               nb_trans_donation.uniqcode,
               nb_trans_donation.status,
              \"CAMPAIGN\" jenis
             FROM nb_trans_donation
               LEFT JOIN nb_bank ON nb_bank.id = nb_trans_donation.bank
               LEFT JOIN nb_campaign ON nb_campaign.id = nb_trans_donation.campaign
            UNION
            SELECT
            nb_trans_program.no_transaksi,
            upper(nb_trans_program.jenistrx) AS title,
            upper(nb_bank.namaBank) as namaBank,
            nb_trans_program.nomuniq,
            nb_trans_program.nominal,
            nb_trans_program.uniqcode,
            nb_trans_program.status,
              \"PROGRAM\" jenis
            FROM nb_trans_program
            LEFT JOIN nb_bank ON nb_bank.id = nb_trans_program.bank) xy where xy.status ='unverified' AND xy.no_transaksi = ".$this->db->escape($notrans);
        $cektransprog = $this->db->query($qry)->result();
        if(count($cektransprog) == 0){
            $notif = array('error' => 'Nomor Transaksi Tidak Ditemukan!');
            $this->session->set_flashdata($notif);
        }
        $kondisi['where'] = array("nb_trans_konfirmasi.transaksi" => $this->db->escape($notrans));
        $kondisi['join'] = array(array('table' => "nb_bank", 'condition' => "nb_bank.id = nb_trans_konfirmasi.bank", "type" => "left"),);
        $cekkonfirmasi = $this->cusmdl->custom_query("nb_trans_konfirmasi", $kondisi)->result();
        $data['trans'] = $cektransprog;
        $data['konfirmasi'] = $cekkonfirmasi;
        $this->parser->parse("manajemendonasi/donasi_konfirmasi", $data);
    }

    function doVerifikasiProgram($id = null)
    {
        if (empty($id)) {
            $this->notif = array('error' => 'Data tidak bener!');
            $this->session->set_flashdata($this->notif);
            redirect('cms/aproveprogram');
            exit;            /*/ redirect('donatur/admverifikasi');*/
        }
        $kondisi['where'] = array("nb_trans_program.no_transaksi" => $this->db->escape($id), "nb_trans_program.status" => $this->db->escape("unverified"));
        $cekdonasi = $this->cusmdl->custom_query("nb_trans_program", $kondisi)->result();
        if (count($cekdonasi) != 1) {
            $this->notif = array('error' => 'Ada kesalahan dalam proses data!');
            $this->session->set_flashdata($this->notif);
            redirect('cms/aproveprogram');
            exit;
            die("cannot proses");
        } else {
            $nomuniq = $cekdonasi[0]->nomuniq;
            $notransaksi = $cekdonasi[0]->no_transaksi;
            $iddonatur = $cekdonasi[0]->donatur;
            $jenistrx = $cekdonasi[0]->jenistrx;
            $kodebank = $cekdonasi[0]->bank;            /*Kondisi untuk melakukan transaksi melalui deposit*/
            if (strtoupper($jenistrx) == "DEPOSIT") {
                $this->trxDeposit($iddonatur, $nomuniq, $notransaksi, "DEPOSIT");
            } else {                /*jika kode bank nya 919 == potong deposit*/
                if ($kodebank == 919) {
                    $this->trxDeposit($iddonatur, $nomuniq, $notransaksi, "TRX");
                }
            }            /*update nb_trans_program*/
            $data = array("status" => "verified", 'edtd_at' => date("Y-m-d H:i:s"));
            $where = array("nb_trans_program.no_transaksi" => ($id),);
            $this->cusmdl->custom_update("nb_trans_program", $data, $where);
            $this->notif = array('success' => 'Berhasil memproses data!');
            $this->session->set_flashdata($this->notif);
            redirect('cms/aproveprogram');
            exit;            /*/ redirect('cms/aproveprogram');*/
        }
    }

    private function libraryApp()
    {
        $this->load->library('librariesloader', $this->_libList);
    }    /*=============END==================*/
}