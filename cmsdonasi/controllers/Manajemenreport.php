<?php

/**
 * Created by PhpStorm.
 * User: Fahmi hilmansyah
 * Date: 3/29/2017
 * Time: 10:25 AM
 */
class manajemenreport extends NBAdmin_Controller
{
    private $_libList = array();

    private $_modList = array('Newcustom_model' => 'cusmdl');

    protected $_data = array();

    function __construct()
    {
        parent::__construct();
        $this->modelsApp();
    }

    private function modelsApp()
    {
        $this->load->model($this->_modList);
    }

    private function libraryApp()
    {
        $this->load->library('librariesloader', $this->_libList);
    }

    function index()
    {
        echo "cilukba";
    }
    function getReport(){
        $campaign = $this->cusmdl->custom_query("nb_campaign");
        if($_POST){
            $jenis = $this->input->post('jenis');
            $jeniscampaign = $this->input->post('jeniscampaign');
            $format = $this->input->post('format');
            $tgl_awal = date("Y-m-d",strtotime($this->input->post('tgl_awal')));
            $tgl_akhir = date("Y-m-d",strtotime($this->input->post('tgl_akhir')));
            $datahasil=array();
            $kondisi = array();
            if(!empty($jeniscampaign)){
                if(strtoupper($jeniscampaign) == "ALL"):
                $kondisi['where'] = array(
                    "nb_trans_donation.status"=>$this->db->escape("verified"),
                    "nb_trans_donation.trx_date >= " => $this->db->escape($tgl_awal),
                    "nb_trans_donation.trx_date < " => $this->db->escape($tgl_akhir),
                );

                else:
                    $kondisi['where'] = array(
                        "nb_campaign.id"=>$this->db->escape($jeniscampaign),
                        "nb_trans_donation.status"=>$this->db->escape("verified"),
                        "nb_trans_donation.trx_date >= " => $this->db->escape($tgl_awal),
                        "nb_trans_donation.trx_date < " => $this->db->escape($tgl_akhir),
                    );
                endif;
                $kondisi['join']=array(
                    array('table'=>'campaign','condition'=>'nb_campaign.id = nb_trans_donation.campaign','type'=>'left'),
                    array('table'=>'nb_bank','condition'=>'nb_bank.id = nb_trans_donation.bank','type'=>'left'),
                );
                $kondisi['group_by']=array('nb_trans_donation.no_transaksi');
                $qry = $this->cusmdl->custom_query("nb_trans_donation",$kondisi)->result();

//                die($this->db->last_query());
                $judul = 'LAPORAN TRANSAKSI ('.date("d M Y",strtotime($tgl_awal)).' - '.date("d M Y",strtotime($tgl_akhir)).') ';
                /*$isi['title'] = $judul;
                $isi['query'] = $qry->result();
                $this->getExcelViews('manajemenreport/vexcel_html',$isi);*/
                $data = $this->_data;
                $this->breadcrumbs->push('Home', '/cms');

                $this->breadcrumbs->push('Manajemen Report', '/cms/settingakses');
                $data['title']=$judul;
                $data['jenis']=(strtoupper($jeniscampaign) == "ALL")?"ALL":empty($qry[0]->title)?"":strtoupper($qry[0]->title);
                $data['query']=$qry;
                $data["campaign"] = $campaign->result();
                $data["crumbs"] = $this->breadcrumbs->show();
                $this->parser->parse("manajemenreport/index", $data);

            }elseif(!empty($jenis)){
                $kondisi['where'] = array(
                    "nb_trans_program.jenistrx"=>$this->db->escape($jenis),
                    "nb_trans_program.status"=>$this->db->escape("verified"),
                    "nb_trans_program.trx_date >= " => $this->db->escape($tgl_awal),
                    "nb_trans_program.trx_date < " => $this->db->escape($tgl_akhir),
                );
                $kondisi['join']= array('table'=>'nb_bank','condition'=>'nb_bank.id = nb_trans_program.bank','type'=>'left');
                $kondisi['group_by']=array('nb_trans_program.no_transaksi');
                $qry = $this->cusmdl->custom_query("nb_trans_program",$kondisi);
                $judul = 'LAPORAN TRANSAKSI Program('.date("d M Y",strtotime($tgl_awal)).' - '.date("d M Y",strtotime($tgl_akhir)).') ';
                /*$isi['title'] = $judul;
                $isi['query'] = $qry->result();
                $this->getExcelViews('manajemenreport/vexcel_html',$isi);*/
                $data = $this->_data;
                $this->breadcrumbs->push('Home', '/cms');

                $this->breadcrumbs->push('Manajemen Report', '/cms/settingakses');
                $data['title']=$judul;
                $data['jenis']=strtoupper($jenis);
                $data['query']=$qry->result();
                $data["campaign"] = $campaign->result();
                $data["crumbs"] = $this->breadcrumbs->show();
                $this->parser->parse("manajemenreport/index", $data);
            }else{
                die('error jenis program');
            redirect('cms/report');
            }

           /* echo "<pre>";
            print_r($qry->result());*/
        }else {
            $data = $this->_data;
            $this->breadcrumbs->push('Home', '/cms');

            $this->breadcrumbs->push('Manajemen Report', '/cms/settingakses');
            $data["crumbs"] = $this->breadcrumbs->show();
            $data["campaign"] = $campaign->result();
            $this->parser->parse("manajemenreport/index", $data);
        }
    }

    function getExcelViews($views = null, $data=array() ){
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=Laporan_Transaksi.xls");
        $this->load->view($views,$data);
    }
}