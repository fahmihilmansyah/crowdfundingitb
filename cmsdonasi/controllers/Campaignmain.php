<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaignmain extends NBAdmin_Controller {

	/**
	 * ----------------------------------------
	 * #campaignmain Controller
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_libList   = array();
	private $_modList   = array(
									'campaignmain_model' => 'PostKtg',
								);
	
	protected $_data      = array();

	function __construct()
	{
		parent::__construct();

		// -----------------------------------
		//  BUKA KOMENTAR JIKA AKAN DIGUNAKAN
		// -----------------------------------
		$this->modelsApp();
		// $this->libraryApp();
	}

	private function modelsApp()
	{		
		$this->load->model($this->_modList);
	}

	private function libraryApp()
	{
		$this->load->library('librariesloader', $this->_libList);
	}

	public function index($data = array())
	{
		$data 	= $this->_data;

        $this->PostKtg->register_action_permition();

        $this->breadcrumbs->push('Home', '/cms');
		$this->breadcrumbs->push('Campaign Utama', '/cms/campaignmain');
		$key = array("id in" => "(select nb_campaign_main.id_campaign from nb_campaign_main where nb_campaign_main.status = '1')");
		$data["default"] = (array) $this->PostKtg->get_data_form($key);
		$data["crumbs"]     = $this->breadcrumbs->show();
		$data["can_add"]	= $this->nbaccess->getAccess("add");
		$data["dsource"]	= base_url() . "cms/campaignmain/json";

		$this->parser->parse("campaignmain/index",$data);
	}

	public function json($roleid = ''){
		$default_order  = 'id';
		$order_field    = '';
		
		$order_field 	= array(
							'id',
							'title',
							'crtd_at',
							'edtd_at',
							'author',
							'last_edit'
						 );
				
		$order_key 	= (!$this->input->get('iSortCol_0')) ? 0 : $this->input->get('iSortCol_0');
		$order 		= (!$this->input->get('iSortCol_0')) ? $default_order : $order_field[$order_key];
		$sort 		= (!$this->input->get('sSortDir_0')) ? 'DESC': $this->input->get('sSortDir_0');
		$search 	= (!$this->input->get('sSearch'))    ? '' : strtoupper($this->input->get('sSearch'));

		$limit 		= (!$this->input->get('iDisplayLength')) ? $this->_limit : $this->input->get('iDisplayLength');
		$start 		= (!$this->input->get('iDisplayStart'))  ? 0 : $this->input->get('iDisplayStart');

		$data['sEcho'] 			= (!$this->input->get('callback')) ? 0 : $this->input
																			  ->get('callback');

		$data['iTotalRecords'] 	= $this->PostKtg->count_all($search,$order_field);
		
		$data['apps'] 			= $this->PostKtg
									   ->get_paged_list($limit, $start, $order, $sort, $search, $order_field)
									   ->result_array();

		$data['akses']			= $this->nbaccess;

		$data['callback'] 		= $this->input->get('callback');

		$this->load->view('campaignmain/data/json', $data);
	}

	private function form($id = ''){
		$data 	= $this->_data;

		if(!empty($id))
		{	
			$data["id"]	     = $id;
			$key             = array("id" => $id);
			$data["default"] = (array) $this->PostKtg->get_data_form($key);
		}

        $this->breadcrumbs->push('Home', '/cms');
		$this->breadcrumbs->push('Campaign Main', '/cms/campaignmain');
		$this->breadcrumbs->push('Aktivasi Main Campaign', '/cms/campaignmain/new');

		$data["crumbs"]     = $this->breadcrumbs->show();
		$data["dsource"]	= base_url() . "cms/campaignmain/json";


		$this->parser->parse("campaignmain/form/form", $data);
	}


	public function edit()
	{
		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();
		$param  = ARR::getParam(ENKRIP::decode($param));

		if($param === false){ redirect('/cms/error/error_proccess'); }
		
		// cek valid token_proses
		$this->nbauth->abp($param["token"], "GET", "campaignmain", "editform");
		
		$this->form($param['id']);
	}

	public function simpan()
	{
		$_key   = $this->session->userdata("_tokenaction");

		// GET PARAMETER
		$param  = !empty($_GET["param"]) ? $_GET["param"] : array();
		$param  = ARR::getParam(ENKRIP::decode($param)); 

		// get data from post
		$data = !empty($this->input->post("data")) ? $this->input->post("data") : array();
		
		// get id
		$id   = !empty($param['id']) ? $param['id'] : '';

		
			// cek valid token_proses
			$this->nbauth->abp($_key['save']['token'], "POST", "campaignmain", "save");

			$this->PostKtg->changeTable('nb_campaign_main');
			//update additional data
			$dataUpdate=array(
				'status' => '0',
				'edtd_at' => date("Y-m-d H:i:s")
				);
			$result = $this->PostKtg->update_to_db($dataUpdate, array());
			// add aditional data
			$dataTambah['id_campaign'] 	= $id;
			$dataTambah['status'] 	= '1';
			$dataTambah['crtd_at'] 	= date('Y-m-d H:i:s');
			$dataTambah['edtd_at'] 	= date('Y-m-d H:i:s');
			$result = $this->PostKtg->insert_to_db($dataTambah);
			if($result)
			{	

				$this->session->set_flashdata('success_notif', 'Data berhasil diupdate.');
				redirect('/cms/campaignmain');
			}
		
		
		redirect('/cms/error/error_proccess');
	}

}
