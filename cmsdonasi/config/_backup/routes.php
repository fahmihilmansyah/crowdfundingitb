<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';

$route['cms'] 								= 'Nb';
$route['cms/login'] 						= 'Nbauthentication/login_page';
$route['cms/auth'] 							= 'Nbauthentication/pauth';
$route['cms/cauth_dt'] 						= 'Nbauthentication/cauth_dt';
$route['cms/token/expired']				    = 'Nbauthentication/token_expired';
$route['cms/usrinfo']						= 'userinfo';
$route['cms/logout'] 						= 'Nbauthentication/logout';
$route['cms/forgot'] 					    = 'Nbauthentication/pg_forgot_pass';
$route['cms/reset'] 						= 'Nbauthentication/forgot_pass';
$route['cms/reset/pg_new_pwd']				= 'Nbauthentication/pg_reset_new_pass';
$route['cms/reset/st_new_pwd']				= 'Nbauthentication/reset_new_pass';



// SETTING MENU
$route['cms/settingmenu']					= 'Manajemenmenu';
$route['cms/settingmenu/new']				= 'Manajemenmenu/add';
$route['cms/settingmenu/update']			= 'Manajemenmenu/edit';
$route['cms/settingmenu/remove']			= 'Manajemenmenu/delete';
$route['cms/settingmenu/save']				= 'Manajemenmenu/simpan';
$route['cms/settingmenu/view']				= 'Manajemenmenu/read';
$route['cms/settingmenu/activate']			= 'Manajemenmenu/aktifasi';

// SETTING AKSES
$route['cms/settingakses']					= 'Manajemenakses';
$route['cms/settingakses/json']				= 'Manajemenakses/json';
$route['cms/settingakses/new']				= 'Manajemenakses/add';
$route['cms/settingakses/update']			= 'Manajemenakses/edit';
$route['cms/settingakses/remove']			= 'Manajemenakses/delete';
$route['cms/settingakses/save']				= 'Manajemenakses/simpan';
$route['cms/settingakses/view']				= 'Manajemenakses/read';
$route['cms/settingakses/activate']			= 'Manajemenakses/aktifasi';

// SETTIN USERS
$route['cms/settingusers']					= 'Manajemenusers';
$route['cms/settingusers/json']				= 'Manajemenusers/json';
$route['cms/settingusers/new']				= 'Manajemenusers/add';
$route['cms/settingusers/update']			= 'Manajemenusers/edit';
$route['cms/settingusers/remove']			= 'Manajemenusers/delete';
$route['cms/settingusers/save']				= 'Manajemenusers/simpan';
$route['cms/settingusers/view']				= 'Manajemenusers/read';
$route['cms/settingusers/activate']			= 'Manajemenusers/aktifasi';


// SETTING Artikel Kategori
$route['cms/artikelkategori']				= 'Artikelkategori';
$route['cms/artikelkategori/json']			= 'Artikelkategori/json';
$route['cms/artikelkategori/new']			= 'Artikelkategori/add';
$route['cms/artikelkategori/update']		= 'Artikelkategori/edit';
$route['cms/artikelkategori/remove']		= 'Artikelkategori/delete';
$route['cms/artikelkategori/save']			= 'Artikelkategori/simpan';
$route['cms/artikelkategori/view']			= 'Artikelkategori/read';
$route['cms/artikelkategori/activate']		= 'Artikelkategori/aktifasi';


// SETTING Artikel
$route['cms/artikel']						= 'Artikel';
$route['cms/artikel/json']					= 'Artikel/json';
$route['cms/artikel/new']					= 'Artikel/add';
$route['cms/artikel/update']				= 'Artikel/edit';
$route['cms/artikel/remove']				= 'Artikel/delete';
$route['cms/artikel/save']					= 'Artikel/simpan';
$route['cms/artikel/view']					= 'Artikel/read';
$route['cms/artikel/activate']				= 'Artikel/aktifasi';

// SETTING Campaign
$route['cms/campaign']						= 'Campaign';
$route['cms/campaign/json']					= 'Campaign/json';
$route['cms/campaign/new']					= 'Campaign/add';
$route['cms/campaign/update']				= 'Campaign/edit';
$route['cms/campaign/remove']				= 'Campaign/delete';
$route['cms/campaign/save']					= 'Campaign/simpan';
$route['cms/campaign/view']					= 'Campaign/read';
$route['cms/campaign/activate']				= 'Campaign/aktifasi';


// SETTING Campaign Kategori
$route['cms/campaignkategori']				= 'Campaignkategori';
$route['cms/campaignkategori/json']			= 'Campaignkategori/json';
$route['cms/campaignkategori/new']			= 'Campaignkategori/add';
$route['cms/campaignkategori/update']		= 'Campaignkategori/edit';
$route['cms/campaignkategori/remove']		= 'Campaignkategori/delete';
$route['cms/campaignkategori/save']			= 'Campaignkategori/simpan';
$route['cms/campaignkategori/view']			= 'Campaignkategori/read';
$route['cms/campaignkategori/activate']		= 'Campaignkategori/aktifasi';


// SETTING Campaign Slider
$route['cms/campaignslider']				= 'campaignslider';
$route['cms/campaignslider/json']			= 'campaignslider/json';
$route['cms/campaignslider/new']			= 'campaignslider/add';
$route['cms/campaignslider/update']			= 'campaignslider/edit';
$route['cms/campaignslider/remove']			= 'campaignslider/delete';
$route['cms/campaignslider/save']			= 'campaignslider/simpan';

// SETTING Campaign Main
$route['cms/campaignmain']				= 'campaignmain';
$route['cms/campaignmain/json']			= 'campaignmain/json';
$route['cms/campaignmain/update']		= 'campaignmain/edit';
$route['cms/campaignmain/save']			= 'campaignmain/simpan';

// SETTING FAQ Kategori
$route['cms/faq_kategori']				= 'Faq_kategori';
$route['cms/faq_kategori/json']			= 'Faq_kategori/json';
$route['cms/faq_kategori/new']			= 'Faq_kategori/add';
$route['cms/faq_kategori/update']		= 'Faq_kategori/edit';
$route['cms/faq_kategori/remove']		= 'Faq_kategori/delete';
$route['cms/faq_kategori/save']			= 'Faq_kategori/simpan';
$route['cms/faq_kategori/view']			= 'Faq_kategori/read';
$route['cms/faq_kategori/activate']		= 'Faq_kategori/aktifasi';

// SETTING FAQ Deskripsi
$route['cms/faq_des']				= 'Faq_des';
$route['cms/faq_des/json']			= 'Faq_des/json';
$route['cms/faq_des/new']			= 'Faq_des/add';
$route['cms/faq_des/update']		= 'Faq_des/edit';
$route['cms/faq_des/remove']		= 'Faq_des/delete';
$route['cms/faq_des/save']			= 'Faq_des/simpan';
$route['cms/faq_des/view']			= 'Faq_des/read';
//$route['cms/faq_des/activate']		= 'Faq_des/aktifasi';

//SETTING Manajemen Bank
$route['cms/manajemenbank']				= 'Manajemenbank';
$route['cms/manajemenbank/json']		= 'Manajemenbank/json';
$route['cms/manajemenbank/new']			= 'Manajemenbank/add';
$route['cms/manajemenbank/update']		= 'Manajemenbank/edit';
$route['cms/manajemenbank/remove']		= 'Manajemenbank/delete';
$route['cms/manajemenbank/save']		= 'Manajemenbank/simpan';
$route['cms/manajemenbank/view']		= 'Manajemenbank/read';


$route['cms/grafik/campaign']		    = 'Grafik/campaign/$1';
$route['cms/grafik/campaign/pd']	    = 'Grafik/campaign_pg_detail';
$route['cms/grafik/campaign/dd']	    = 'Grafik/campaign_detail';

$route['cms/grafik/program']		    = 'Grafik/program/$1';
$route['cms/grafik/program/pd']	    	= 'Grafik/program_pg_detail';
$route['cms/grafik/program/dd']	    	= 'Grafik/program_detail';


//SETTING User Info
$route['cms/usrinfo/save']			   = 'userinfo/edit';

// ERROR PAGE
$route['cms/error/(:any)']					= 'Nbauthentication/error_page/$1';

/**
* ----------------------------------------
* #FRONT
* ----------------------------------------
*/

// SETTING REGISTRASi
$route['register']							= 'register/Register';
$route['register/doregister']				= 'register/Register/doregister';
$route['register/notif']					= 'register/Register/notif_aktifasi';
$route['register/aktifasi']					= 'register/Register/aktifasi';
$route['register/doaktifasi']				= 'register/Register/doaktifasi';

// DONATUR
$route['overview']							= 'donatur/Donatur';
$route['myprofile']							= 'donatur/Donatur/profile';
$route['mydonation']						= 'donatur/Donatur/mydonation';
$route['mywallet']						    = 'donatur/Donatur/dompetku';
$route['mypoin']						    = 'donatur/Donatur/mypoin';
$route['konfirmasi']						= 'konfirmasipembayaran/KonfirmasiPembayaran';
$route['konfirmasi/proses']					= 'konfirmasipembayaran/KonfirmasiPembayaran/proses_konfirmasi';
$route['konfirmasi/getOptions']				= 'konfirmasipembayaran/KonfirmasiPembayaran/getTransaksiOptions';
$route['konfirmasi/getNominal']				= 'konfirmasipembayaran/KonfirmasiPembayaran/getNominal';
$route['konfirmasi/sukses']					= 'konfirmasipembayaran/KonfirmasiPembayaran/konfirmasi_sukses';
				

// PROGRAM
$route['program']							= 'shadaqah/Shadaqah';
$route['program/shadaqah']					= 'shadaqah/Shadaqah';
$route['program/zakat']						= 'zakat/Zakat';
$route['program/infaq']						= 'infaq/Infaq';

// ABOUT US
$route['aboutus']							= 'profil/Profil';


// CAMPAIGN
$route['donasi/list']						= 'campaigns/Campaigns';
$route['donasi/list/(:any)']				= 'campaigns/Campaigns';
$route['donasi/detail/(:any)']				= 'campaigns/Campaigns/detail/$1';

$route['donasi/aksi']						= 'donasi/Donasi/index';
$route['donasi/aksi/(:any)']				= 'donasi/Donasi/index/$1';


// ARTICLES
$route['news/list']							= 'artikels/Artikels';
$route['news/list/(:any)']					= 'artikels/Artikels';
$route['news/detail/(:any)']				= 'artikels/Artikels/detail/$1';


// FOOTER
$route['visimisi']							= 'visimisi/Visimisi';
$route['sekilas']							= 'sekilas/Sekilas';
$route['legalitas']							= 'legalitas/Legalitas';
$route['pengurus']							= 'pengurus/Pengurus';
$route['home/addsub']						= 'home/Home/addsub';

$route['auten/(:any)']						= 'authentication/Nbauth/auten/$1';

$route['auten/page/login']					= 'login';
$route['auten/redirect/facebook']		    = 'authentication/Nbauth/facebook';
$route['auten/redirect/google']		   	    = 'authentication/Nbauth/google';

/*approve deposit transaksi*/
$route['cms/aprovedonation']		   	    = 'manajemendonasi/index';
$route['cms/aproveprogram']		   	    	= 'manajemendonasi/program';

/*report transaksi*/
$route['cms/report']		   	    		= 'manajemenreport/getreport';

$route['logout']							= 'authentication/Nbauth/logout';

$route['tes']								= 'tes/Tes';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
