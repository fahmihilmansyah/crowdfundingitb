<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Nbgrafik_model extends CI_Model {



	/**

	 * ----------------------------------------

	 * #NB Menus Model

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_arrayMonth = array(

							'1' => '0',

							'2' => '0',

							'3' => '0',

							'4' => '0',

							'5' => '0',

							'6' => '0',

							'7' => '0',

							'8' => '0',

							'9' => '0',

							'10' => '0',

							'11' => '0',

							'12' => '0',

						   );



	private $_arrayMonthName = array(

									"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Des"

							   );



	function __construct()

	{

		parent::__construct();

	}



	public function getCampaignPerMonth($year = ''){

		$year = !empty($year) ? $year : date('Y');



		$sql = "SELECT SUM(nominal) as sum_nominal, `month` FROM nb_trans_donation WHERE `year` = '{$year}' AND status = 'verified' GROUP BY `month`";



		$result = $this->db->query($sql)->result();



		foreach ($result as $row) {

			if(array_key_exists($row->month, $this->_arrayMonth))

			{

				$this->_arrayMonth[$row->month] = $row->sum_nominal;

			}

		}



		$new_value = array();

		$i = 1;

		foreach ($this->_arrayMonth as $key => $row) {

			$new_value[] = array(

									"name" 		=> $this->_arrayMonthName[$key - 1],

									"y" 		=> (int) $row

								);

		}



		return $new_value;

	}



	public function getCampaignPerDay($c = '', $y = '')

	{

		$year     = !empty($y) ? $y : date('Y');

		$month 	  = !empty($c) ? sprintf("%02d", $c) : sprintf("%02d", date('m'));



		$sumofday = $this->getSumOfDay($year, $month);

		$day      = array();



		for($i=1; $i<=$sumofday; $i++){$day[sprintf("%02d",$i)] = 0;}



		$year = !empty($year) ? $year : date('Y');



		$sql = "SELECT

					SUM(nominal) AS sum_nominal,

					DATE_FORMAT(`trx_date`,\"%d\") as `tgl`

				FROM

					nb_trans_donation

				WHERE

					`year` = '{$y}' AND

					`month` = '{$c}' AND

					status = 'verified'

				GROUP BY

					DATE_FORMAT(`trx_date`,\"%d\")";


		$result = $this->db->query($sql)->result();



		foreach ($result as $row) {

			if(array_key_exists($row->tgl, $day))

			{

				$day[$row->tgl] = $row->sum_nominal;

			}

		}



		$new_value = array();

		$i = 1;



		foreach ($day as $key => $row) {

			$new_value[] = array(

									"y" 		=> (int) $row,

									"name" 		=> $key,

								);

		}



		return $new_value;

	}



	public function getProgramPerMonth($type = '', $year = ''){

		$year = !empty($year) ? $year : date('Y');

		if($type == "zakatmaal-zakatprofesi")
		{
			$type = explode("-", $type);
			$jenistrx = "jenistrx = '{$type[0]}' OR jenistrx = '{$type[1]}'";
		}
		else
		{
			$type = $type;
			$jenistrx = "jenistrx = '{$type}'";
		}

		$sql = "SELECT SUM(nominal) as sum_nominal, `month` FROM nb_trans_program WHERE `year` = '{$year}' AND status = 'verified' AND {$jenistrx} GROUP BY `month`";

		// echo $sql; exit();

		$result = $this->db->query($sql)->result();



		foreach ($result as $row) {

			if(array_key_exists($row->month, $this->_arrayMonth))

			{

				$this->_arrayMonth[$row->month] = $row->sum_nominal;

			}

		}



		$new_value = array();

		$i = 1;

		foreach ($this->_arrayMonth as $key => $row) {

			$new_value[] = array(

									"y" 		=> (int) $row,

									"name" 		=> $this->_arrayMonthName[$key - 1],

								);

		}



		return $new_value;

	}





	public function getProgramPerDay($c = '', $y = '',$t = '')

	{

		$year     = !empty($y) ? $y : date('Y');

		$month 	  = !empty($c) ? sprintf("%02d", $c) : sprintf("%02d", date('m'));



		$sumofday = $this->getSumOfDay($year, $month);

		$day      = array();



		for($i=1; $i<=$sumofday; $i++){$day[sprintf("%02d",$i)] = 0;}



		$year = !empty($year) ? $year : date('Y');

		if($t == "zakatmaal-zakatprofesi")
		{
			$type = explode("-", $t);
			$jenistrx = "jenistrx = '{$type[0]}' OR jenistrx = '{$type[1]}'";
		}
		else
		{
			$type = $t;
			$jenistrx = "jenistrx = '{$type}'";
		}


		$sql = "SELECT

					SUM(nominal) AS sum_nominal,

					DATE_FORMAT(`trx_date`,\"%d\") as `tgl`

				FROM

					nb_trans_program

				WHERE

					`year` = '{$y}' AND

					`month` = '{$c}' AND

					status = 'verified' AND
 
					({$jenistrx})

				GROUP BY

					DATE_FORMAT(`trx_date`,\"%d\")";
				
		$result = $this->db->query($sql)->result();


		foreach ($result as $row) {

			if(array_key_exists($row->tgl, $day))

			{

				$day[$row->tgl] = $row->sum_nominal;

			}

		}



		$new_value = array();

		$i = 1;



		foreach ($day as $key => $row) {

			$new_value[] = array(

									"y" 		=> (int) $row,

									"name" 		=> $key,

								);

		}



		return $new_value;

	}





	public function getNameOfMonth()

	{

		return $this->_arrayMonthName;

	}



   	public function getSumOfDay($year = 0, $month = 0){

        $arrBulan       = array(

        						"01"=>31,

        						"02"=>28,

        						"03"=>31,

        						"04"=>30,

        						"05"=>31,

        						"06"=>30,

        						"07"=>31,

        						"08"=>31,

        						"09"=>30,

        						"10"=>31,

        						"11"=>30,

        						"12"=>31

        						);



        $arrBulan['02'] = (($year) % 4 == 0) ? 29 : $arrBulan['02'];

        $bulanIni       = $arrBulan[$month];



        return $bulanIni;

    }
	
	public function getdonaturPerMonth($year = ''){

		$year = !empty($year) ? $year : date('Y');



		$sql = "SELECT count(donatur) as sum_donatur, `month` FROM nb_trans_donation WHERE `year` = '{$year}' AND status = 'verified' GROUP BY `month`";



		$result = $this->db->query($sql)->result();



		foreach ($result as $row) {

			if(array_key_exists($row->month, $this->_arrayMonth))

			{

				$this->_arrayMonth[$row->month] = $row->sum_donatur;

			}

		}



		$new_value = array();

		$i = 1;

		foreach ($this->_arrayMonth as $key => $row) {

			$new_value[] = array(

									"name" 		=> $this->_arrayMonthName[$key - 1],

									"y" 		=> (int) $row

								);

		}



		return $new_value;

	}
	
	public function getdonaturPerDay($c = '', $y = '')

	{

		$year     = !empty($y) ? $y : date('Y');

		$month 	  = !empty($c) ? sprintf("%02d", $c) : sprintf("%02d", date('m'));



		$sumofday = $this->getSumOfDay($year, $month);

		$day      = array();



		for($i=1; $i<=$sumofday; $i++){$day[sprintf("%02d",$i)] = 0;}



		$year = !empty($year) ? $year : date('Y');



		$sql = "SELECT

					count(donatur) AS sum_donatur,

					DATE_FORMAT(`trx_date`,\"%d\") as `tgl`

				FROM

					nb_trans_donation

				WHERE

					`year` = '{$y}' AND

					`month` = '{$c}' AND

					status = 'verified'

				GROUP BY

					DATE_FORMAT(`trx_date`,\"%d\")";


		$result = $this->db->query($sql)->result();



		foreach ($result as $row) {

			if(array_key_exists($row->tgl, $day))

			{

				$day[$row->tgl] = $row->sum_donatur;

			}

		}



		$new_value = array();

		$i = 1;



		foreach ($day as $key => $row) {

			$new_value[] = array(

									"y" 		=> (int) $row,

									"name" 		=> $key,

								);

		}



		return $new_value;

	}
	
	public function getdonaturProgramPerMonth($type = '', $year = ''){

		$year = !empty($year) ? $year : date('Y');

		if($type == "zakatmaal-zakatprofesi")
		{
			$type = explode("-", $type);
			$jenistrx = "jenistrx = '{$type[0]}' OR jenistrx = '{$type[1]}'";
		}
		else
		{
			$type = $type;
			$jenistrx = "jenistrx = '{$type}'";
		}

		$sql = "SELECT count(donatur) as sum_donatur, `month` FROM nb_trans_program WHERE `year` = '{$year}' AND status = 'verified' AND {$jenistrx} GROUP BY `month`";

		// echo $sql; exit();

		$result = $this->db->query($sql)->result();



		foreach ($result as $row) {

			if(array_key_exists($row->month, $this->_arrayMonth))

			{

				$this->_arrayMonth[$row->month] = $row->sum_donatur;

			}

		}



		$new_value = array();

		$i = 1;

		foreach ($this->_arrayMonth as $key => $row) {

			$new_value[] = array(

									"y" 		=> (int) $row,

									"name" 		=> $this->_arrayMonthName[$key - 1],

								);

		}



		return $new_value;

	}
	
	public function getdonaturProgramPerDay($c = '', $y = '',$t = '')

	{

		$year     = !empty($y) ? $y : date('Y');

		$month 	  = !empty($c) ? sprintf("%02d", $c) : sprintf("%02d", date('m'));



		$sumofday = $this->getSumOfDay($year, $month);

		$day      = array();



		for($i=1; $i<=$sumofday; $i++){$day[sprintf("%02d",$i)] = 0;}



		$year = !empty($year) ? $year : date('Y');

		if($t == "zakatmaal-zakatprofesi")
		{
			$type = explode("-", $t);
			$jenistrx = "jenistrx = '{$type[0]}' OR jenistrx = '{$type[1]}'";
		}
		else
		{
			$type = $t;
			$jenistrx = "jenistrx = '{$type}'";
		}


		$sql = "SELECT

					count(donatur) AS sum_donatur,

					DATE_FORMAT(`trx_date`,\"%d\") as `tgl`

				FROM

					nb_trans_program

				WHERE

					`year` = '{$y}' AND

					`month` = '{$c}' AND

					status = 'verified' AND
 
					({$jenistrx})

				GROUP BY

					DATE_FORMAT(`trx_date`,\"%d\")";
				
		$result = $this->db->query($sql)->result();


		foreach ($result as $row) {

			if(array_key_exists($row->tgl, $day))

			{

				$day[$row->tgl] = $row->sum_donatur;

			}

		}



		$new_value = array();

		$i = 1;



		foreach ($day as $key => $row) {

			$new_value[] = array(

									"y" 		=> (int) $row,

									"name" 		=> $key,

								);

		}



		return $new_value;

	}

}

