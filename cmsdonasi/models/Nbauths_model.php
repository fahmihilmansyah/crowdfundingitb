<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Nbauths_model extends CI_Model {



	/**

	 * ----------------------------------------

	 * #NB Auth Model

	 * ----------------------------------------

	 * #author 		    : Fahmi Hilmansyah

	 * #time created    : 4 January 2017

	 * #last developed  : Fahmi Hilmansyah

	 * #last updated    : 4 January 2017

	 * ----------------------------------------

	 */



	private $_table1 = "view_users";



	function __construct()

	{

		parent::__construct();

	}



	public function getUserTerdaftar($uname = "", $pwd = "")

	{

		$this->db->from($this->_table1);

		$this->db->where(array("uname" => $uname, "pwd" => $pwd));



		return $this->db;

	}



	public function setGeneratedToken($param = array(), $uname = "", $pwd = ""){

        $this->db->update($this->_table1, $param, array("uname" => $uname, "pwd" => $pwd));



        if ($this->db->trans_status() === FALSE) {

            $this->db->trans_rollback();

            return FALSE;

        } else {

            $this->db->trans_commit();

            return TRUE;

        }

	}



	public function cekValidToken($token_code = '', $uname ='', $pwd = ''){

		$this->db->from($this->_table1);

		$this->db->where(array("uname" => $uname, "pwd" => $pwd, "token_code" => $token_code));



		return $this->db;

	}



	public function cekValidABP($key = array()){

		$this->db->from("nb_action_permit");

		$this->db->where($key);



		return $this->db->count_all_results();

	}


	public function ifMailExistt($mail = '')
	{
		$this->db->from("view_users");

		$this->db->where(array("email" => $mail));



		return $this->db->count_all_results();
	}

	public function ifTokenExists($token = '')
	{
		$this->db->from("sys_users");

		$this->db->where(array("fp_token" => $token));



		return $this->db->count_all_results();
	}

	public function getValidTimeToken($token = "")
	{	
		$this->db->select("fp_valid");
		$this->db->from("sys_users");
		$this->db->where(array("fp_token" => $token));

		return $this->db->get()->row();
	}

	public function updatePass()
	{
		
	}
}

