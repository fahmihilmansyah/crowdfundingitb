<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class manajemenmenu_model extends CI_Model {

	/**
	 * ----------------------------------------
	 * #Manajemen Menu Model
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_table1 = "sys_menus";

	function __construct()
	{
		parent::__construct();
	}

	private function _kunci($data = array()){
        $result = $data;

        if(!is_array($data))
            $result = array("id" => $data);

        return $result;
    }
  	
  	private function data($key = ''){
        $this->db->select("*");
        $this->db->from($this->_table1." a");	

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

	public function register_action_permition(){
		// DELETE DATA BEFORE
		$this->db->delete("nb_action_permit", array("user_id" => $_SESSION["usr_id"]));

		// OPEN ADD FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("GET", $token, "ManajemenMenu", "addform");

		// SAVE ADD FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("POST", $token, "ManajemenMenu", "saveasnew");

		// OPEN EDIT FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("GET", $token, "ManajemenMenu", "editform");

		// SAVE ADD FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("POST", $token, "ManajemenMenu", "save");

		//DELETE FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("DELETE", $token, "ManajemenMenu", "delete");

		//DELETE FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("GET", $token, "ManajemenMenu", "activate");
	}

	private function exec_register_permition($type = "", $token = "", $ctrl_name = "", $index = ""){
		$data["type"]   	= $type; 
		$data["token"]  	= $token; 
		$data["ctrl_name"]  = $ctrl_name; 
		$data["user_id"]    = $_SESSION["usr_id"];
		$data["action"] 	= $index;

		$this->load->library("nbpermit");
		$this->nbpermit->insertToPermit($data);

		$temp			   = $this->session->userdata("_tokenaction");
		$temp[$index]      = $data;
 
		$this->session->set_userdata('_tokenaction',$temp);
	}

	public function get_data_form($key = array()){
		$result = $this->data($key)->get()->row();
		$result = !empty($result) ? $result : array();
		
		return $result;
	}

	public function insert_to_db($data){
        $this->db->trans_begin();

        $this->db->insert($this->_table1, $data);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
	}

	public function update_to_db($data, $key){
        $this->db->trans_begin();
        $this->db->update($this->_table1, $data, $key);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
	}

	public function delete($key = array()){
        $this->db->trans_begin();

        $this->db->delete($this->_table1, $key);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
	}

	public function options($default = '-- Pilih Data --', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();
        
        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id] = $row->name;
        }

        return $option;
    }

	public function getMenu($akses){
	    $html   = '<table class="table table-bordered tree">';      
	    $html  .= '<thead>';
	    $html  .= '<tr>';
	    $html  .= '<th>Menu</th>';
	    $html  .= '<th class="center">Posisi</th>';
	    $html  .= '<th class="center">Status</th>';
	    $html  .= '<th class="center">';
	    $html  .= 'Aksi';
	    $html  .= '</th>';
	    $html  .= '</tr>';
	    $html  .= '</thead>';

	    $html  .= '<tbody>';
	    $html  .= $this->datamenu(0, $akses);
	    $html  .= '</tbody>';

	    $html  .= '</table>'; 

	    return $html;
	}

	function datamenu($id, $akses){
	    $sql = "SELECT
	                *, my_menu.name as menu_name, my_menu.id as menu_id
	            FROM
	                (
	                    SELECT
	                        `a`.*, `child`.`have_child`
	                    FROM
	                        (`nb_sys_menus` a)
	                    LEFT OUTER JOIN(
	                        SELECT
	                            parent,
	                            COUNT(*)AS have_child
	                        FROM
	                            `nb_sys_menus`
	                        GROUP BY
	                            parent
	                    )child ON `a`.id = `child`.parent
	                )my_menu
	            WHERE 
	                `my_menu`.`parent` = '{$id}' 
	            ORDER BY `my_menu`.`position` ASC
	            ";


	    $query    = $this->db->query($sql);
	    $totalRow = $query->num_rows();
	   
	    $html = "";
	    if($totalRow > 0){
	        $_key   = $this->session->userdata("_tokenaction");
	        foreach($query->result() as $rs) {
				$aksi  	= '';
				$aksi  .= "<center><div class='btn-group'>";

				// VIEW BUTTON
				/*$param  = "id=" . $rs->id . ";";
				$url    = base_url() . 'cms/settingmenu/view?param=' . ENKRIP::encode($param);
				$aksi  .= "<a href='" . $url . "' class='btn btn-dark btn-xs'><i class='fa fa-eye'></i></a>";*/


				// ACTIVATE BUTTON
				$btn 	= ($rs->is_active) ? 'btn-danger' : 'btn-success';
				$value 	= ($rs->is_active) ? 0 : 1; 
				if($akses->getAccess("edit"))
				{
					$param  = "id=" . $rs->id . ";status=" . $value . ";parent=" . $rs->parent . ";";
					$param .= "token=" . $_key["activate"]["token"];
					$url    = base_url() . 'cms/settingmenu/activate?param=' . ENKRIP::encode($param);
					$aksi  .= "<a id='activate-" . $rs->id . "' 
								 href='javascript:void(0)' 
								 onclick='activateRow(this.id)' 
								 class='btn {$btn} btn-xs'
								 link-target='" . $url . "'
								 status='" . $rs->is_active . "'
							  >
									<i class='fa fa-power-off'></i>
							  </a>";
				}

				// EDIT BUTTON
				if($akses->getAccess("edit"))
				{
					$param  = "id=" . $rs->id . ";";
					$param .= "token=" . $_key["editform"]["token"];
					$url    = base_url() . 'cms/settingmenu/update?param=' . ENKRIP::encode($param);
					$aksi  .= "<a href='" . $url . "' class='btn btn-info btn-xs'><i class='fa fa-edit'></i></a>";
				}

				// DELETE BUTTON
				if($akses->getAccess("delete"))
				{
					$param  = "id=" . $rs->id . ";";
					$param .= "token=" . $_key["delete"]["token"];
					$url    = base_url() . 'cms/settingmenu/remove?param=' . ENKRIP::encode($param);
					$aksi  .= "<a id='delete-" . $rs->id . "' 
								 href='javascript:void(0)' 
								 onclick='deleteRow(this.id)' 
								 class='btn btn-danger btn-xs'
								 link-target='" . $url . "'
							  >
									<i class='fa fa-trash'></i>
							  </a>";
				}

				$aksi .= "</div></center>";

				$class   = ($rs->is_active) ? 'label label-success' : 'label label-default';
				$text    = ($rs->is_active) ? 'aktif' : 'tidak aktif';

				// DETAIL TABLE
	            if($rs->have_child > 0){
	                $html .= '<tr class="treegrid-' . $rs->id . '">';
	                $html .= '<td>';
	                $html .= $rs->menu_name;
	                $html .= '</td>';

	                $html .= '<td width="10%" align="center">';
	                $html .= $this->setwrap($rs->parent, $rs->position);
	                $html .= '</td>';

	                $html .= '<td width="10%" align="center">';
	                $html .= '<center><label class="' . $class . '">' . $text . '</label>';
	                $html .= '</td>';

	                $html .= '<td width="15%" align="center">';
	                $html .= $aksi;
	                $html .= '</td>';
	                $html .= '</tr>';

	                $html .= $this->datamenu($rs->menu_id, $akses);  

	            }else if($rs->have_child == null || $rs->have_child == "" ){
	                $html .= '<tr class="treegrid-' . $rs->id . ' treegrid-parent-' . $rs->parent . '">';
	                $html .= '<td>';
	                $html .= $rs->menu_name; 
	                $html .= '</td>';

	                $html .= '<td align="center">';
	                $html .= $this->setwrap($rs->parent, $rs->position);
	                $html .= '</td>';

	                $html .= '<td width="10%" align="center">';
	                $html .= '<center><label class="' . $class . '">' . $text . '</label>';
	                $html .= '</td>';

	                $html .= '<td width="15%" align="center">';
	                $html .= $aksi;
	                $html .= '</td>';

	                $html .= '</tr>';
	            }
	        }           
	    }

	    return $html;
	}

	function setwrap($parent = '', $position){
		$wrap = '';

		if($parent == 0){
			$wrap = '<i class="pmenu_wrapper">' . $position . '</i>';
		}else{
			$wrap = $position;
		}

		return $wrap;
	}
}