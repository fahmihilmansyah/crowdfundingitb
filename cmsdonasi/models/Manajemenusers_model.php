<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class manajemenusers_model extends CI_Model {

	/**
	 * ----------------------------------------
	 * #Manajemen Akses Model
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_table1 = "sys_users";

	function __construct()
	{
		parent::__construct();
	}

  	private function _kunci($data = array()){
        $result = $data;

        if(!is_array($data))
            $result = array("id" => $data);

        return $result;
    }
  	
  	private function data($key = ''){
        $this->db->select("*");
        $this->db->from($this->_table1." a");	

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

	public function register_action_permition(){
		// DELETE DATA BEFORE
		$this->db->delete("nb_action_permit", array("user_id" => $_SESSION["usr_id"]));

		// OPEN ADD FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("GET", $token, "ManajemenUsers", "addform");

		// SAVE ADD FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("POST", $token, "ManajemenUsers", "saveasnew");

		// OPEN EDIT FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("GET", $token, "ManajemenUsers", "editform");

		// SAVE ADD FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("POST", $token, "ManajemenUsers", "save");

		//DELETE FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("DELETE", $token, "ManajemenUsers", "delete");

		//ACTIVATE FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("GET", $token, "ManajemenUsers", "activate");
	}

	private function exec_register_permition($type = "", $token = "", $ctrl_name = "", $index = ""){
		$data["type"]   	= $type; 
		$data["token"]  	= $token; 
		$data["ctrl_name"]  = $ctrl_name; 
		$data["user_id"]    = $_SESSION["usr_id"];
		$data["action"] 	= $index;

		$this->load->library("nbpermit");
		$this->nbpermit->insertToPermit($data);

		$temp			   = $this->session->userdata("_tokenaction");
		$temp[$index]      = $data;
 
		$this->session->set_userdata('_tokenaction',$temp);
	}

	public function get_paged_list($limit=10, $offset=0, $order_column='', $order_type='asc', $search='', $fields='')
	{	
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($order_column) || empty($order_type))
		{
			$this->db->order_by('id','ASC');
		} else {
			$this->db->order_by($order_column,$order_type);
		}

		return $this->db->get('sys_users',$limit,$offset);
	}

	public function count_all($search='', $fields='')
	{
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		$this->db->from('sys_users');
		return $this->db->count_all_results(); 
	}

	public function get_data_form($key = array()){
		$result = $this->data($key)->get()->row();
		$result = !empty($result) ? $result : array();
		
		return $result;
	}

	public function insert_to_db($data){
        $this->db->trans_begin();

        $this->db->insert($this->_table1, $data);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
	}

	public function update_to_db($data, $key){
        $this->db->trans_begin();
        $this->db->update($this->_table1, $data, $key);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
	}

	public function delete($key = array()){
        $this->db->trans_begin();

        $this->db->delete($this->_table1, $key);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
	}

	public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from("question_fp");

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();
        
        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id] = $row->question;
        }

        return $option;
    }
}
