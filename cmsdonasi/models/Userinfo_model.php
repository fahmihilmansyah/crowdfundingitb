<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class userinfo_model extends CI_Model {

	/**
	 * ----------------------------------------
	 * #Manajemen Akses Model
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_table1 = "sys_users";

	function __construct()
	{
		parent::__construct();
	}

  	private function _kunci($data = array()){
        $result = $data;

        if(!is_array($data))
            $result = array("id" => $data);

        return $result;
    }
  	
  	private function data($key = ''){
        $this->db->select("*");
        $this->db->from($this->_table1." a");	

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

	public function register_action_permition(){
		// DELETE DATA BEFORE
		$this->db->delete("nb_action_permit", array("user_id" => $_SESSION["usr_id"]));
		
		// SAVE ADD FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("POST", $token, "usrinfo", "save");
	}

	private function exec_register_permition($type = "", $token = "", $ctrl_name = "", $index = ""){
		$data["type"]   	= $type; 
		$data["token"]  	= $token; 
		$data["ctrl_name"]  = $ctrl_name; 
		$data["user_id"]    = $_SESSION["usr_id"];
		$data["action"] 	= $index;

		$this->load->library("nbpermit");
		$this->nbpermit->insertToPermit($data);

		$temp			   = $this->session->userdata("_tokenaction");
		$temp[$index]      = $data;
 
		$this->session->set_userdata('_tokenaction',$temp);
	}
	
	public function get_data($where){
		return $this->db->get_where("nb_sys_users",array('id' => $where ));
	}
	
	public function update_data($data,$where){
		$this->db->where($where);
		return $this->db->update("nb_sys_users",$data);
	}
}
