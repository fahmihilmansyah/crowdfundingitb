<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class manajemenakses_model extends CI_Model {

	/**
	 * ----------------------------------------
	 * #Manajemen Akses Model
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	private $_table1 = "sys_roles";
	private $_table2 = "sys_roles_access";

	function __construct()
	{
		parent::__construct();
	}

  	private function _kunci($data = array()){
        $result = $data;

        if(!is_array($data))
            $result = array("id" => $data);

        return $result;
    }
  	
  	private function data($key = ''){
        $this->db->select("*");
        $this->db->from($this->_table1." a");	

        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

	public function register_action_permition(){
		// DELETE DATA BEFORE
		$this->db->delete("nb_action_permit", array("user_id" => $_SESSION["usr_id"]));

		// OPEN ADD FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("GET", $token, "ManajemenAkses", "addform");

		// SAVE ADD FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("POST", $token, "ManajemenAkses", "saveasnew");

		// OPEN EDIT FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("GET", $token, "ManajemenAkses", "editform");

		// SAVE ADD FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("POST", $token, "ManajemenAkses", "save");

		//DELETE FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("DELETE", $token, "ManajemenAkses", "delete");

		//ON OFF FORM
		$token 	= sha1(md5(time() . rand()));
		$this->exec_register_permition("GET", $token, "ManajemenAkses", "activate");
	}

	private function exec_register_permition($type = "", $token = "", $ctrl_name = "", $index = ""){
		$data["type"]   	= $type; 
		$data["token"]  	= $token; 
		$data["ctrl_name"]  = $ctrl_name; 
		$data["user_id"]    = $_SESSION["usr_id"];
		$data["action"] 	= $index;

		$this->load->library("nbpermit");
		$this->nbpermit->insertToPermit($data);

		$temp			   = $this->session->userdata("_tokenaction");
		$temp[$index]      = $data;
 
		$this->session->set_userdata('_tokenaction',$temp);
	}

	public function get_paged_list($limit=10, $offset=0, $order_column='', $order_type='asc', $search='', $fields='')
	{	
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		if (empty($order_column) || empty($order_type))
		{
			$this->db->order_by('id','ASC');
		} else {
			$this->db->order_by($order_column,$order_type);
		}

		return $this->db->get('sys_roles',$limit,$offset);
	}

	public function count_all($search='', $fields='')
	{
		if($search!='' AND $fields!='')
		{
			$likeclause = '(';
			$i=0;
			foreach($fields as $field)
			{
				if($i==count($fields)-1) {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%'";
				} else {
					$likeclause .= "UPPER(".$field.") LIKE '%".strtoupper($search)."%' OR ";
				}
				++$i;
			}
			$likeclause .= ')';
			$this->db->where($likeclause);
		}

		$this->db->from('sys_roles');
		return $this->db->count_all_results(); 
	}

	public function get_data_form($key = array())
	{
		$result = $this->data($key)->get()->row();
		$result = !empty($result) ? $result : array();
		
		return $result;
	}

	public function get_max($key = array())
	{
		$this->db->select("MAX(id) as max");
		
		return $this->db->get($this->_table1)->row()->max;
	}

	public function get_menu_id($url = ''){
		$this->db->select("id"); 
		$this->db->from("nb_sys_menus");
		$this->db->where(array("url" => $url));

		$result = $this->db->get()->row();

		return !(empty($result)) ? $result->id : '';
	}

	public function insert_to_db($data)
	{
        return $this->db->insert($this->_table1, $data);
	}

	public function insert_to_db_detail($data)
	{
        return $this->db->insert($this->_table2, $data);
	}

	public function update_to_db($data, $key)
	{
        $this->db->trans_begin();
        $this->db->update($this->_table1, $data, $key);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
	}

	public function delete($key = array())
	{
        $this->db->trans_begin();

        $this->db->delete($this->_table1, $key);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
	}

	public function delete_detail($key = array())
	{
		return $this->db->delete($this->_table2, $key);
	}

	public function options($default = '--Pilih Data--', $key = '') 
	{
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();
        
        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id] = $row->name;
        }

        return $option;
    }


	public function showMenu($roleid = '', $view = false)
	{
	    $html   = '<table class="table table-striped table-bordered tree">';      
	    $html  .= '<thead>';
	    $html  .= '<tr>';
	    $html  .= '<th>Menu</th>';
	    $html  .= '<th class="center">';
	    $html  .= 'Add';
	    $html  .= '</th>';
	    $html  .= '<th class="center">';
	    $html  .= 'Edit';
	    $html  .= '</th>';
	    $html  .= '<th class="center">';
	    $html  .= 'Delete';
	    $html  .= '</th>';
	    $html  .= '<th class="center">';
	    $html  .= 'Approve';
	    $html  .= '</th>';
	    $html  .= '<th class="center">';
	    $html  .= 'Print';
	    $html  .= '</th>';
	    $html  .= '<th class="center">';
	    $html  .= 'Import';
	    $html  .= '</th>';
	    $html  .= '<th class="center">';
	    $html  .= 'Export';
	    $html  .= '</th>';
	    $html  .= '</tr>';
	    $html  .= '</thead>';

	    $html  .= '<tbody>';
	    $html  .= $this->datamenu(0, $roleid, $view);
	    $html  .= '</tbody>';

	    $html  .= '</table>'; 

	    return $html;
	}

	private function datamenu($id, $roleid, $view)
	{
	    $sql = "SELECT
	                *, my_menu.name as menu_name, my_menu.id as menu_id
	            FROM
	                (
	                    SELECT
	                        `a`.*, `child`.`have_child`
	                    FROM
	                        (`nb_sys_menus` a)
	                    LEFT OUTER JOIN(
	                        SELECT
	                            parent,
	                            COUNT(*)AS have_child
	                        FROM
	                            `nb_sys_menus`
	                        GROUP BY
	                            parent
	                    )child ON `a`.id = `child`.parent
	                )my_menu
	            WHERE 
	                `my_menu`.`parent` = '{$id}' 
	            ORDER BY `my_menu`.`position` ASC
	            ";


	    $query    = $this->db->query($sql);
	    $totalRow = $query->num_rows();
	   	
	   	$disabled = ($view == false) ? '' : ' disabled';

	    $html = "";
	    if($totalRow > 0){
	        foreach($query->result() as $rs) {
	            
	            $extra      = '';
	            $roleid     = !empty($roleid) ? $roleid : NULL;
	            $id         = $rs->id;
	            $parent     = $rs->parent; 

 	            if($rs->have_child > 0){
	                // -------------------------------------------------
	                // Cek All Parent
	                // -------------------------------------------------
	                $extra  = "id='check-parent-{$id}' onclick='checklistMenu(this.id)'";
	                $extra .= $disabled;

	                $html .= "<tr class='treegrid-{$id}'>";
	                $html .= "<td>";
	                $html .= "<label class='all'>";
	                $html .= form_checkbox("all[]", "all", FALSE, $extra);
	                $html .= $rs->menu_name; 
	                $html .= "</label>";
	                $html .= "</td>";

	                // -------------------------------------------------
	                // Col Parent add
	                // -------------------------------------------------
	                $extra  = "id='col-parent-add-{$id}'";
	                $extra .= "class='check-parent-{$id}' onclick='checklistMenu(this.id)'";
	                $extra .= $disabled;

	                $html .= "<td width='10%'' align='center'>";
	                $html .= form_checkbox("list[$id][add]", "add", $this->checkStatRole($rs->id, $roleid, "can_add"), $extra);
	                $html .= "</td>";

	                // -------------------------------------------------
	                // Col Parent Edit
	                // -------------------------------------------------
	                $extra  = "id='col-parent-edit-{$id}'";
	                $extra .= "class='check-parent-{$id}' onclick='checklistMenu(this.id)'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][edit]", 'edit', $this->checkStatRole($rs->id, $roleid, "can_edit"), $extra);
	                $html .= "</td>";

	                // -------------------------------------------------
	                // Col Parent Delete
	                // -------------------------------------------------
	                $extra  = "id='col-parent-delete-{$id}'";
	                $extra .= "class='check-parent-{$id}' onclick='checklistMenu(this.id)'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][delete]", "delete", $this->checkStatRole($rs->id, $roleid, "can_delete"), $extra);
	                $html .= "</td>";

	                // -------------------------------------------------
	                // Col Parent aprove
	                // -------------------------------------------------
	                $extra  = "id='col-parent-aprove-{$id}'";
	                $extra .= "class='check-parent-{$id}' onclick='checklistMenu(this.id)'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][aprove]", "aprove", $this->checkStatRole($rs->id, $roleid, "can_aprove"), $extra);

	                $html .= "</td>";

	                // -------------------------------------------------
	                // Col Parent Print
	                // -------------------------------------------------
	                $extra  = "id='col-parent-print-{$id}'";
	                $extra .= "class='check-parent-{$id}' onclick='checklistMenu(this.id)'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][print]", "print", $this->checkStatRole($rs->id, $roleid, "can_print"), $extra);
	                $html .= "</td>";

	                // -------------------------------------------------
	                // Col Parent Import
	                // -------------------------------------------------
	                $extra  = "id='col-parent-import-{$id}'";
	                $extra .= "class='check-parent-{$id}' onclick='checklistMenu(this.id)'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][import]", "import", $this->checkStatRole($rs->id, $roleid, "can_import"), $extra);
	                $html .= "</td>";

	                // -------------------------------------------------
	                // Col Parent Export
	                // -------------------------------------------------
	                $extra  = "id='col-parent-export-{$id}'";
	                $extra .= "class='check-parent-{$id}' onclick='checklistMenu(this.id)'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][export]", "export", $this->checkStatRole($rs->id, $roleid, "can_export"), $extra);
	                $html .= "</td>";

	                $html .= "</tr>";

	                $html .= $this->datamenu($rs->menu_id, $roleid, $view);  

	            }else if($rs->have_child == null || $rs->have_child == "" ){
	                // -------------------------------------------------
	                // Check Row Parent
	                // -------------------------------------------------
	                $extra  = "id='row-parent-{$id}'";
	                $extra .= "class='check-parent-{$parent}'";
	                $extra .= " row-parent-{$id}";
	                $extra .= " col-parent-{$parent}' onclick='checklistMenu(this.id)'";
	                $extra .= $disabled;

	                $html .= "<tr class='treegrid-{$id} treegrid-parent-{$parent}'>";
	                $html .= "<td>";
	                $html .= "<label class='all'>";
	                $html .= form_checkbox("all", "all", FALSE, $extra);
	                $html .= $rs->menu_name; 
	                $html .= "</label>";
	                $html .= "</td>";

	                // -------------------------------------------------
	                // CHILD
	                // -------------------------------------------------
	                $extra  = "class='check-parent-{$parent}";
	                $extra .= " row-parent-{$id}";
	                $extra .= " col-parent-add-{$parent}'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][add]", "add", $this->checkStatRole($rs->id, $roleid, "can_add"), $extra);
	                $html .= "</td>";

	                // -------------------------------------------------
	                // CHILD
	                // -------------------------------------------------
	                $extra  = "class='check-parent-{$parent}";
	                $extra .= " row-parent-{$id}";
	                $extra .= " col-parent-edit-{$parent}'";
	                $extra .= $disabled;

	                $html .= "<td width='10%'' align='center'>";
	                $html .= form_checkbox("list[$id][edit]", "edit", $this->checkStatRole($rs->id, $roleid, "can_edit"), $extra);
	                $html .= "</td>";

	                // -------------------------------------------------
	                // CHILD
	                // -------------------------------------------------
	                $extra  = "class='check-parent-{$parent}";
	                $extra .= " row-parent-{$id}";
	                $extra .= " col-parent-delete-{$parent}'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][delete]", "delete", $this->checkStatRole($rs->id, $roleid, "can_delete"), $extra);
	                $html .= "</td>";

	                // -------------------------------------------------
	                // CHILD
	                // -------------------------------------------------
	                $extra  = "class='check-parent-{$parent}";
	                $extra .= " row-parent-{$id}";
	                $extra .= " col-parent-aprove-{$parent}'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][aprove]", "aprove", $this->checkStatRole($rs->id, $roleid, "can_aprove"), $extra);
	                $html .= "</td>";
	                // -------------------------------------------------
	                // CHILD
	                // -------------------------------------------------
	                $extra  = "class='check-parent-{$parent}";
	                $extra .= " row-parent-{$id}";
	                $extra .= " col-parent-print-{$parent}'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][print]", "print", $this->checkStatRole($rs->id, $roleid, "can_print"), $extra);
	                $html .= "</td>";

	                // -------------------------------------------------
	                // CHILD
	                // -------------------------------------------------
	                $extra  = "class='check-parent-{$parent}";
	                $extra .= " row-parent-{$id}";
	                $extra .= " col-parent-import-{$parent}'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][import]", "import", $this->checkStatRole($rs->id, $roleid, "can_import"), $extra);
	                $html .= "</td>";

	                // -------------------------------------------------
	                // CHILD
	                // -------------------------------------------------
	                $extra  = "class='check-parent-{$parent}";
	                $extra .= " row-parent-{$id}";
	                $extra .= " col-parent-export-{$parent}'";
	                $extra .= $disabled;

	                $html .= "<td width='10%' align='center'>";
	                $html .= form_checkbox("list[$id][export]", "export", $this->checkStatRole($rs->id, $roleid, "can_export"), $extra);
	                $html .= "</td>";

	                $html .= "</tr>";
	            }
	        }           
	    }

	    return $html;
	}

	private function checkStatRole($menuid, $roleid, $field)
	{
	    $sql 	= "SELECT `$field` FROM nb_sys_roles_access WHERE menu_id = '{$menuid}' AND role_id = '{$roleid}'";

	    $query 	= $this->db->query($sql);
	    $result = $query->num_rows();

	    if($result > 0){
	        return $query->row()->$field;
	    }else{
	        return 0;
	    }
	}

	public function getStatMenu($menuid, $roleid, $field)
	{
		return $this->checkStatRole($menuid, $roleid, $field);
	}
}
