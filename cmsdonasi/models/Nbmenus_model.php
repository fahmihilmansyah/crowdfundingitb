<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class nbmenus_model extends CI_Model {

	/**
	 * ----------------------------------------
	 * #NB Menus Model
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	function __construct()
	{
		parent::__construct();
	}

	public function getTeks(){
		echo "ini text dari model";
	}
}
