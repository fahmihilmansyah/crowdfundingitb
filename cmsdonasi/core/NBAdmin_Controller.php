<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NBAdmin_Controller extends CI_Controller {

	/**
	 * ----------------------------------------
	 * #NBAdmin Core Controller
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */


	private $_appTitle;
	private $_libList     = array('nbsettings','nbmenus','nbauth','nbaccess');

	protected $_data      = array();
	protected $_limit     = 10;

	public function __construct() 
	{
		parent::__construct();
		$this->librariesApp();
		$this->initApp();
    }

	private function librariesApp()
	{
		$this->load->library('librariesloader', $this->_libList);
	}

 	private function initApp()
	{
		$this->nbauth->cauth();

		// GET JS CSS
		$setting           		= $this->nbsettings;
        $setting->set_plugin(array('main'));
        $this->_data["_jsHeader" ] 	= $setting->get_js();
        $this->_data["_cssHeader"] 	= $setting->get_css();
        
        // GET MENU
        $menu						= $this->nbmenus;	
        $this->_data["_menu"]		= $menu->getMenu(0);

        // USERNAME AND APPTITLE
		$this->_data["_appTitle"]  	= APPTITLE;
        $this->_data["_username"]	= !empty($this->session->userdata("usr_uname")) ? 
        									 $this->session->userdata("usr_uname") : "Jhon Doe";

        // SET LAYOUT
        $this->_data["_layHeader"]  = $this->parser->parse('header', $this->_data, TRUE);
        $this->_data["_layFooter"]  = $this->parser->parse('footer', $this->_data, TRUE);
	}
}