<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class NBFront_Controller extends CI_Controller {

	/**
	 * ----------------------------------------
	 * #NBFront Core Controller
	 * ----------------------------------------
	 * #author 		    : Fahmi Hilmansyah
	 * #time created    : 4 January 2017
	 * #last developed  : Fahmi Hilmansyah
	 * #last updated    : 4 January 2017
	 * ----------------------------------------
	 */

	protected $_appTitle;
	private $_libList     = array('nbsettings','nbauth_front');

	protected $_data      = array();
	protected $_limit     = 10;

	protected $_login_stat = false;

	public function __construct() 
	{
		parent::__construct();
		$this->librariesApp();
		$this->initApp();
		
		if($this->uri->segment(1) != "donasi")
		{
			$this->session->unset_userdata('searchdonasi');
		}
    }

	private function librariesApp()
	{
		$this->load->library('librariesloader', $this->_libList);
	}

 	private function initApp()
	{
		// GET JS CSS
		$setting           		= $this->nbsettings;
        $setting->set_plugin(array('front'));
        $this->_data["_jsHeader" ] 	= $setting->get_js();
        $this->_data["_cssHeader"] 	= $setting->get_css();

        // USERNAME AND APPTITLE
		$this->_data["_appTitle"]  	= 'MumuApps | Platform Pendanaan';

		// LOGIN STATUS
        $this->_login_status = false;
        $this->_data["_login_stat"] = $this->_login_status;

        // SET LAYOUT
        $this->_data["_lgn_default"]  = base_url() . 'auten/login?t=default';
        $this->_data["_lgn_facebook"] = base_url() . 'auten/login?t=facebook';
        $this->_data["_lgn_google"]   = base_url() . 'auten/login?t=google';

        $this->_data["_loginstatus"]  = !empty($_SESSION[LGI_KEY . "login_info"]["login_status"]) ? 
                                   			   $_SESSION[LGI_KEY . "login_info"]["login_status"] : false;
        $this->_data['mupays_url'] = 'http://core.mumuapps.id';
//        $this->_data['mupays_url'] = 'http://localhost/apiwebpg/public';
        if(!empty($_SESSION[LGI_KEY . "login_info"]))
        {
        	if($_SESSION[LGI_KEY . "login_info"]["login_status"])
        	{
        		$this->_data["login_userdata"] = $_SESSION[LGI_KEY . "login_info"];
        	}
        }

        $this->_data["_program_link"] = $this->getProgramLink();
        $this->_data["_ttgkm_link"]	  = $this->getTtgkmLink();

        $this->_data["_layHeader"]    = $this->parser->parse('template/header', $this->_data, TRUE);
        $this->_data["_layHeaderCamp"]    = $this->parser->parse('template/header_campaign', $this->_data, TRUE);
        $this->_data["_layFooter"]    = $this->parser->parse('template/footer', $this->_data, TRUE);
	}

	private function getProgramLink()
	{
		$this->db->where(array("ctg" => "ttgkm", "is_active" => "1"));
		return $this->db->get("nb_footer_link")->result_array();
	}

	private function getTtgkmLink()
	{
		$this->db->where(array("ctg" => "prgm", "is_active" => "1"));
		return $this->db->get("nb_footer_link")->result_array();
	}

}