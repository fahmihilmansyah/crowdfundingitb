-- --------------------------------------------------------
-- Host:                         156.67.211.42
-- Server version:               5.6.35-log - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for u7043207_berzakat
CREATE DATABASE IF NOT EXISTS `u7043207_berzakat` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `u7043207_berzakat`;

-- Dumping structure for table u7043207_berzakat.nb_action_permit
CREATE TABLE IF NOT EXISTS `nb_action_permit` (
  `type` enum('POST','GET','DELETE') DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `ctrl_name` varchar(50) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_action_permit: ~6 rows (approximately)
/*!40000 ALTER TABLE `nb_action_permit` DISABLE KEYS */;
REPLACE INTO `nb_action_permit` (`type`, `token`, `ctrl_name`, `action`, `user_id`) VALUES
	('GET', '95a57bb2598428b2b35dd6edfaa0e07f381a680b', 'campaign', 'addform', 5),
	('POST', 'f8b17c650d91f30d4912da754d7102abfd620134', 'campaign', 'saveasnew', 5),
	('GET', '22b66c47c3227ae19f40a350c07cf1276ee4ba80', 'campaign', 'editform', 5),
	('POST', 'ae64571dd8fd426c002d4353bba5ab425bf11e87', 'campaign', 'save', 5),
	('DELETE', '9cbe79851dc6332673023683ba86ef5c4adbdc5a', 'campaign', 'delete', 5),
	('GET', '8a7e2db6395f52a17b55eb35b627c6aab4bfbc9e', 'campaign', 'activate', 5);
/*!40000 ALTER TABLE `nb_action_permit` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_bank
CREATE TABLE IF NOT EXISTS `nb_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namaBank` varchar(255) DEFAULT NULL,
  `no_rekening` varchar(255) DEFAULT NULL,
  `atasnama` varchar(255) DEFAULT NULL,
  `cabang` varchar(255) DEFAULT NULL,
  `imglink` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_bank: ~4 rows (approximately)
/*!40000 ALTER TABLE `nb_bank` DISABLE KEYS */;
REPLACE INTO `nb_bank` (`id`, `namaBank`, `no_rekening`, `atasnama`, `cabang`, `imglink`) VALUES
	(1, 'BANK BCA', '12345678', 'Niat Baik', 'Jakpus', 'image_20170406122013.png'),
	(2, 'BANK MANDIRI', '98736522', 'Niat Baik', 'JakUt', 'image_20170406122032.png'),
	(4, 'BRI', '7320934035970', 'Niat Baik', 'Cimahi', 'image_20170406122103.png'),
	(5, 'BTN', '9820934', 'Niat Baik', 'Cimahi', 'image_20170406122120.png');
/*!40000 ALTER TABLE `nb_bank` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_campaign
CREATE TABLE IF NOT EXISTS `nb_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `desc` text,
  `desc_short` varchar(255) DEFAULT NULL,
  `update_progress` text NOT NULL,
  `link_terkait` varchar(100) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `target` int(11) DEFAULT '0' COMMENT 'target donation from campaign users',
  `now` int(11) DEFAULT '0' COMMENT 'summary donation until now',
  `sum_donatur` int(11) DEFAULT NULL COMMENT 'sum_donatur: people summary donation',
  `sum_share` int(11) DEFAULT NULL,
  `sum_read` int(11) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1' COMMENT '0:false; 1:true',
  `img` text,
  `video` text,
  `post_date` datetime DEFAULT NULL,
  `valid_date` date DEFAULT NULL,
  `valid_status` enum('1','2') NOT NULL,
  `meta_title` varchar(70) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keyword` varchar(200) DEFAULT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  `categories` int(255) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `user_post` int(255) DEFAULT NULL,
  `user_type` enum('ad','do') DEFAULT 'ad' COMMENT 'ad: administrator; do: donatur',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_campaign: ~8 rows (approximately)
/*!40000 ALTER TABLE `nb_campaign` DISABLE KEYS */;
REPLACE INTO `nb_campaign` (`id`, `title`, `desc`, `desc_short`, `update_progress`, `link_terkait`, `slug`, `target`, `now`, `sum_donatur`, `sum_share`, `sum_read`, `is_active`, `img`, `video`, `post_date`, `valid_date`, `valid_status`, `meta_title`, `meta_description`, `meta_keyword`, `crtd_at`, `edtd_at`, `author`, `last_edit`, `categories`, `location`, `user_post`, `user_type`) VALUES
	(1, 'TEs 1', '<p><u><em><strong>tesatae</strong></em></u></p>\r\n', 'tesa', '', 'tesa', 'tes-1', 0, 0, 0, NULL, NULL, '1', 'image_20170203105240.jpg', NULL, '2017-02-03 10:52:41', '2017-04-17', '1', 'tesa', 'tesa', 'esa,tea,tesagdsa', '2017-02-03', '2017-04-17', 'Luthfi Aziz', 'Luthfi Aziz', 1, NULL, 1, 'ad'),
	(2, 'TEs', '<p>tesa</p>\r\n', 'tes', '', 'tes', 'tes', 25000000, 0, 0, NULL, NULL, '1', 'image_20170203111601.jpg', NULL, '2017-02-03 11:16:02', '2017-02-10', '1', 'tes1tes', 'tes', 'tes,tes21', '2017-02-03', '2017-04-17', 'Luthfi Aziz', 'Luthfi Aziz', 1, NULL, 1, 'ad'),
	(3, 'ASTRONOM ISLAM DARI TANAH MELAYU', '<p>Sejarah mencatat kiprah ulama di Nusantara dalam ilmu falak atau astronomi. Terdapat nama&nbsp;<strong>Tahir Jalaluddin Al-Azhari</strong>. Merupakan ulama sekaligus pakar astronomi dari tanah Melayu, yang tidak hanya tenar di kawasan Indonesia namun juga Singapura hingga Malaysia. Ia lahir di Cangking, Agam, Sumatera Barat.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Syeikh Tahir merupakan satu dari banyak ulama Nusantara yang menggaungkan gerakan Islam modern dan pencerahan. Di usia sangat muda, yakni 10 tahun, ia pernah berhijrah ke Makkah untuk menuntut ilmu agama. Di Makkah ia belajar Al-Quran kepada beberapa guru selama kurang lebih 12 tahun. Ia juga berguru dalam ilmu falak, yang kemudian menginspirasinya untuk menimba ilmu di Al-Azhar Mesir di usia 28 tahun.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Sekembalinya ke tanah Minang, ia mengganti tradisi rukyah dalam memulaikan puasa dengan ilmu hisab dan ilmu falak. memantapkan pendiriannya dengan memperdalam ilmu falak dan astronomi, Syeikh Tahir kemudian memilih mengembara ke beberapa wilayah di tanah Melayu. Beberapa jejak perjalanannya di antaranya, di Singapura pada 1888, di Kesultanan Riau pada 1892, dan di Penang Malaysia pada 1899.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Pada 1903, Syeikh Tahir pernah sampai ke Surabaya hingga singgah di Buleleng dan Ampenan, Bali. Pada 1903 ia sampai di Pulau Sumbawa, lalu menuju Bima pada 1904 dan perjalannya berlanjut ke Makasar. Pengembaraannya membuatnya mendapat banyak relasi di kalangan ulama. Ia juga sempat mendirikan Majalah Al-Imam di tahun 1906 bersama para kerabatnya sesama ulama.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Syeikh Tahir juga berkontribusi pada ilmu falak dan astronomi dengan membuat karya tulis. Salah satunya yakni Nukhbatu al-Taqrīrāt fī Hisāb al-Awqāt wa Sumūt al-Qiblat bi al-Lūgārītmāt atau dalam bahasa melayu, Pati Kiraan pada Menentukan Waktu yang Lima dan Hala Kiblat dengan Logaritma.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Syeikh Tahir meninggal di Kuala Kangsar, Perak, Malaysia 26 Oktober 1956. Ia dikenang sebagai tokoh astronomi Islam yang berpengaruh, sejajar dengan Syeikh Ahmad Khatib al-Minangkabau, Ahmad Rifa&rsquo;i Kalisalak dan KH Sholeh Darat. Untuk mengenang jasanya, didirikan Pusat Falak Syeikh Tahir di Balik Pulau Pantai Aceh, Pulau Pinang Malaysia.</p>\r\n', 'Sejarah mencatat kiprah ulama di Nusantara dalam ilmu falak atau astronomi. Terdapat nama Tahir Jalaluddin Al-Azhari. Merupakan ulama sekaligus pakar astronomi dari tanah Melayu', '', '', 'astronom-islam-dari-tanah-melayu', 25000000, 0, 0, NULL, NULL, '1', 'image_20170314024707.jpg', NULL, '2017-03-14 14:47:07', '2017-03-14', '1', 'ASTRONOM', 'ASTRONOM ISLAM DARI TANAH MELAYU', 'islam,ASTRONOM,melayu', '2017-03-14', '2017-04-17', 'Adminsitrator', 'Luthfi Aziz', 1, NULL, 4, 'ad'),
	(4, 'FITNAH MEDIA SOSIAL', '<p>Sebelum kita:</p>\r\n\r\n<ul>\r\n	<li>&zwnj;memberikan comment,</li>\r\n	<li>&zwnj;memposting sebuah gambar atau meng-upload sebuah video,</li>\r\n	<li>&zwnj;men-share sebuah artikel atau men-copy paste,</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>maka perlu dicamkan bahwa:</p>\r\n\r\n<ul>\r\n	<li>&zwnj;setiap yang kita tulis,</li>\r\n	<li>&zwnj;gambar yang kita posting,</li>\r\n	<li>&zwnj;video yang kita upload,</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>AKAN DIHISAB</strong>&nbsp;oleh Allāh Subhānahu wa Ta&#39;āla. Semuanya&nbsp;<strong>TANPA TERKECUALI</strong>.</p>\r\n\r\n<p>Huruf-hurufnya akan dihisab oleh Allāh Subhānahu wa Ta&#39;āla.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>مَّا يَلْفِظُ مِن قَوْلٍ إِلَّا لَدَيْهِ رَقِيبٌ عَتِيدٌ</p>\r\n\r\n<p>&quot;Dan apapun yang keluar dari lisan anda akan dicatat oleh malaikat Raqib dan Atid.&quot;</p>\r\n\r\n<p>(QS Qāf: 18)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Apapun yang anda katakan dan diqiaskan apapun yang anda tuliskan di sosmed tersebut. Semuanya akan dicatat dan akan dihisab oleh Allāh. Allāh akan TANYA semua artikel yang kita tulis, artikel yang kita copy paste, yang kita share, yang kita berikan pada pihak lain.</li>\r\n	<li>Kalau kita comment, comment kita akan dihisab oleh Allāh.</li>\r\n	<li>Kalau kita membahas sebuah masalah di grup kita: masalah dakwah, masalah politik, masalah ekonomi pada saat ini,</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>SEMUANYA AKAN DIHISAB&nbsp;</strong>oleh Allāh Subhānahu wa Ta&#39;āla, dan semuanya akan tercatat rapi dibuku para malāikat. Bukankah Allāh berfirman dalam surat Al Infithār ayat 12:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>يَعْلَمُونَ مَا تَفْعَلُونَ</p>\r\n\r\n<p>&quot;Para malāikat-malāikat itu tahu apa yang anda ucapkan.&quot; (Al-Infithar : 12)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Rasulullah sallallahu&rsquo;alayhi wasallam mengatakan:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>مَنْ صَمَتَ نَجَا</p>\r\n\r\n<p>&quot;Barang siapa yang diam, dia akan selamat.&quot; (<em>HR Tirmidzi nomor 2425 versi maktabatu Al Ma&#39;arif nomor 2501</em>)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Fitnah di Sosisal Media juga &ldquo;menjangkit&rdquo; para wanita. Wanita-wanita yang TERBUKA AURATNYA. Ketika dia selfie lalu dia upload fotonya di MEDIA SOSIALnya, kemudian tahun depan dia TAUBAT, fotonya masih ada atau tidak? Bisa&nbsp;<strong>BEREDAR TERUS FOTONYA</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Sudah saatnya kita lebih bijak dalam ber-MEDIA SOSIAL di zaman sekarang. Lebih bijak lagi dalam menggunakan fasilitas ini dan &nbsp;pikirkanlah 1000 kali untuk menaruh sesuatu di media sosial (medsos). Perbaiki niat kita saat mengunggah sesuatu ke dunia maya, seperti Sabda Rasulullah sallallahu&rsquo;alayhi wasallam</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>إِنَّمَا الأَعْمَالُ بِالنِّيَّاتِ</p>\r\n\r\n<p>&quot;Sesungguhnya amal ibadah itu tergantung niatnya.&quot;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bila kita meniatkan apapun yang kita unggah ke dunia maya aadalah untuk ibadah dan berdakwah, kita ikhlas akan itu semua. Maka sungguh itu adalah lumbung pahala yang luar biasa.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Waallahu ta&#39; &#39;ala a&#39;lam bishowab.</p>\r\n', 'Apapun yang anda katakan dan diqiaskan apapun yang anda tuliskan di sosmed tersebut. Semuanya akan dicatat dan akan dihisab oleh Allāh. Allāh akan TANYA semua artikel yang kita tulis, artikel yang kita copy paste, yang kita share, yang kita berikan pada p', '', '', 'fitnah-media-sosial', 50000000, 0, 0, 50, 110, '1', 'image_20170314031108.jpg', NULL, '2017-03-14 15:11:08', '2017-03-14', '1', 'Fitnah', 'FITNAH MEDIA SOSIAL', 'fitnah,media,sosial', '2017-03-14', '2017-04-17', 'Adminsitrator', 'Luthfi Aziz', 1, NULL, 4, 'ad'),
	(5, 'PERSIAPAN "RODA" MANDIRI BERSAMA YDSF', '<p>(07/02) Tim Zakat untuk Mustahik (ZUM) ke Manyar Pucang Sidoarjo untuk menyaksikan proses pembuatan rombong untuk bantuan mustahik.<br />\r\nProgram yang bertajuk RODA (Rombong untuk Dhuafa) salah satu bantuan ZUM untuk modal usaha pada dhuafa yang dirupakan dalam bentuk rombong.</p>\r\n\r\n<p>ZUM merupakan program penyaluran bantuan yang sifatnya karitas, pemberian bantuannya berupa uang tuna.</p>\r\n\r\n<p>1 unit rombong ini ini lengkap dengan becak kayuh serta modal untuk isi keperluan rombongnya,&nbsp;Penerima manfaat rombong ini adalah 2 mustahik wilayah surabaya, yaitu penjual pentol dan penjual rujak buah.</p>\r\n\r\n<p>&quot;Harapan kami program ini menjadi produktif tidak hanya bersifat karitas, tapi bisa membantu yang diinginkan mustahik untuk bisa mandiri secara ekonomi, karena yang terpenting bagi mereka adalah bisa bertahan ekonominya untuk menjalani hidup.&quot; Ungkap&nbsp;<a href="https://www.facebook.com/andri.septiono">Andri Septiono</a>&nbsp;selaku Penanggung Jawab ZUM.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '(07/02) Tim Zakat untuk Mustahik (ZUM) ke Manyar Pucang Sidoarjo untuk menyaksikan proses pembuatan rombong untuk bantuan mustahik.\r\nProgram yang bertajuk RODA (Rombong untuk Dhuafa) salah satu bantuan ZUM untuk modal usaha pada dhuafa yang dirupakan dala', '', '', 'persiapan-roda-mandiri-bersama-ydsf', 5000000, 0, 0, 60, 90, '1', 'image_20170314034613.jpg', NULL, '2017-03-14 15:46:13', '2017-03-14', '1', 'roda', 'PERSIAPAN "RODA" MANDIRI BERSAMA YDSF', 'ydsf,mandiri', '2017-03-14', '2017-04-17', 'Adminsitrator', 'Luthfi Aziz', 1, NULL, 4, 'ad'),
	(6, 'MANFAAT YANG BERPUTAR', '<p>Banyak diantara kita memiliki banyak barang yang tidak terpakai di rumah kita. Terkadang hingga akhirnya dibuang karena tidak terpakai. Sedangkan banyak yang mungkin bisa kita manfaatkan atau bermanfaat bagi orang lain, seperti apa yang dilakukan Ainur Leksono. Berawal dari kecelakaan yang di alami oleh dia saat berkerja sehingga harus bersandar di kursi roda selama 1 tahun. Setelah proses pengobatan selama 1 tahun itu akhirnya Allah beri Ainur kesembuhan. &ldquo;Alhamdulillah Allah beri kesembuhan untuk saya&rdquo; cerita Ainur.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Kesembuhan ini tidak dilupakan oleh Ainur. Kursi roda yang tidak terpakai lagi dia sumbangkan melalui YDSF yang nantinya akan digunakan kembali untuk kebutuhan umat. &ldquo;Semoga jadi berkah bagi yang lain&rdquo; tutur dia. Aksi yang seharusnya menjadi contoh untuk kita semua. Bisa jadi barang yang kita miliki bisa bermanfaat bahkan sangat membantu orang lain. Yang menjadi Manfaat yang berputar dan keberkahan yang Jariyah, Insyaa Allah. (put)</p>\r\n', 'Banyak diantara kita memiliki banyak barang yang tidak terpakai di rumah kita. Terkadang hingga akhirnya dibuang karena tidak terpakai.', '', '', 'manfaat-yang-berputar', 5000000, 0, 0, NULL, NULL, '1', 'image_20170314034837.jpg', NULL, '2017-03-14 15:48:37', '2017-03-14', '1', 'MANFAAT', 'MANFAAT YANG BERPUTAR', 'manfaat', '2017-03-14', '2017-04-17', 'Adminsitrator', 'Luthfi Aziz', 1, NULL, 4, 'ad'),
	(7, 'MAKNA HIDUPp', '<h4>MAKNA HIDUPp</h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&ldquo;Kehidupan yang dijalani seseorang bisa saja menjadi sebab kebahagiannya atau menjadi sebab penderitaan dan kesengsaraannya.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Setiap detik, setiap hari bahkan setiap tahun yang berlalu dalam kehidupan manusia bisa saja membawanya pada kecintaan &amp; keridhoan Allah, sehingga dia masuk diantara orang-orang yang mendapatkan kemenangan dan surga, atau sebaliknya, ia berlalu dan membawanya pada neraka dan kemurkaan Allah -wal&rsquo;iyadzubillah-.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Kehidupan ini juga bisa saja membuat kita tertawa sesaat lalu dengan sekejap membuat kita menangis untuk selama-lamanya. Atau sebaliknya, dia membuat kita menangis sesaat untuk bisa tertawa gembira selama-lamanya.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Orang yang akan meraih sukses dan bahagia adalah mereka yang menatap dunia ini dan tahu apa yang harus dilakukannya.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Allah telah menjadikan hidup ini sebagai tempat ujian, dimana dengan ujian itu akan nampak hakikat setiap hamba. Siapa yang meraih rahmat Allah, maka dia akan bahagia selama-lamanya, begitu juga sebaliknya.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Kita tidak tau akhir dari semua ini.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Boleh jadi dalam hidup yang dijalani seorang hamba ada saat dimana dia mendapati dirinya mulai jauh dari Allah, hatinya kemudian termenung sendiri, menangisi masa lalunya, air mata taubatpun membasahi pipinya, lalu dengan tangisan itu Allah mengampuninya dan memasukkannya ke dalam surga.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Namun ada juga sebaliknya dimana ada sebagian hamba melakukan dosa, dia menganggap remeh dosa tersebut, hingga akhirnya sifat meremehkan itu menjadi sebab kesengsaraannya selama-lamanya.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Agar kehidupan ini semakin bermakna maka dalam setiap waktu yang kita jalani seharusnya kita bertanya:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Berapa banyak kita tertawa dalam hidup ini&hellip;.?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Dan apakah tawa itu pada sesuatu yang diridhoi Allah atau tidak&hellip;?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Berapa banyak kita-kita bersenang-senang dalam hidup ini&hellip;?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Dan apakah kesenangan itu diridhoi Allah atau tidak&hellip;?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Berapa banyak kita melewatkan malam dengan bergadang..?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Apakah kita melewatinya dengan sesuatu yang mendatangkan ridho Allah atau tidak..?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Pertanyaan demi pertanyaan..</p>\r\n\r\n<p>Mungkin ada orang yang bertanya untuk apa semua tanya itu&hellip;?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Iya, setiap saat kita perlu bertanya pada diri sendiri, karena tak ada sedetikpun dari waktu yang kita lalui melaikan kita dalam limpahan karunia Allah.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Maka sangat memalukan bila kita makan dan minum dari nikmat Allah, berjalan diatas bumi Allah, namun semua itu tidak membuat kita terpanggil untuk mendekat dan mempersembahkan amal terbaik disisi-Nya.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Coba renungkan&hellip; ketika kita bangun dipagi hari, mata kita terbuka, pendengaran kita kembali seperti semula, raga kita kembali bergerak, maka siapa yang menjaga agar semua organ tubuh itu tetap berfungsi dengan baik hingga kita memasuki pagi..?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bertanyalah pada diri&hellip; Apakah Rabb yang telah menciptakan dan menjaga fungsi dari seluruh organ tubuh itu ridho bila ciptaan-Nya digunakan untuk bermaksiat pada-Nya&hellip;?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Selamat menjawab&hellip;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>(Diringkas dari nasehat Syaikh Muhammad bin Muhammad Mukhtar as-Syanqithi &ndash; hafidzohullah &ndash; dengan sedikit penyelarasan)</p>\r\n', '“Kehidupan yang dijalani seseorang bisa saja menjadi sebab kebahagiannya atau menjadi sebab penderitaan dan kesengsaraannya.', '', '', 'makna-hidupp', 15000000, 0, 0, NULL, NULL, '1', 'image_20170314035330.jpg', NULL, '2017-03-14 15:53:30', '2017-03-14', '1', 'makna', 'MAKNA HIDUP', 'makna,hidup', '2017-03-14', '2017-04-17', 'Adminsitrator', 'Luthfi Aziz', 1, NULL, 4, 'ad'),
	(8, 'isi disini', '<p>hallo</p>\r\n', 'alsdkfajsdf', '<p><iframe frameborder="0" height="315" src="https://www.youtube.com/embed/rc7cmd0mB8E" width="560"></iframe></p>\r\n', '', 'isi-disini', 5000000, 951848, 4, NULL, NULL, '1', 'image_20170523043249.jpg', NULL, '2017-03-29 11:03:43', '2017-06-02', '1', '', '', '', '2017-03-29', '2017-05-23', 'Adminsitrator', 'Adminsitrator12', 1, NULL, 4, 'ad');
/*!40000 ALTER TABLE `nb_campaign` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_campaign_categories
CREATE TABLE IF NOT EXISTS `nb_campaign_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `note` text,
  `is_active` enum('0','1') DEFAULT '1' COMMENT '0:false; 1:true',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_campaign_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `nb_campaign_categories` DISABLE KEYS */;
REPLACE INTO `nb_campaign_categories` (`id`, `name`, `note`, `is_active`, `crtd_at`, `edtd_at`, `author`, `last_edit`) VALUES
	(1, 'Campaign 11', 'Campaign 1', '1', '2017-02-03', '2017-03-29', 'Luthfi Aziz', 'Adminsitrator'),
	(2, 'Kategori 2', 'ini adalh kategori 2', '1', '2017-03-29', '2017-03-29', 'Adminsitrator', NULL);
/*!40000 ALTER TABLE `nb_campaign_categories` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_campaign_main
CREATE TABLE IF NOT EXISTS `nb_campaign_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_campaign` int(11) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `crtd_at` datetime DEFAULT NULL,
  `edtd_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_campaign` (`id_campaign`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_campaign_main: ~5 rows (approximately)
/*!40000 ALTER TABLE `nb_campaign_main` DISABLE KEYS */;
REPLACE INTO `nb_campaign_main` (`id`, `id_campaign`, `status`, `crtd_at`, `edtd_at`) VALUES
	(1, 1, '0', '2017-02-07 00:00:00', '2017-03-29 11:12:44'),
	(2, 1, '0', '2017-02-07 14:03:32', '2017-03-29 11:12:44'),
	(3, 2, '0', '2017-02-07 14:03:47', '2017-03-29 11:12:44'),
	(4, 7, '0', '2017-03-28 10:13:36', '2017-03-29 11:12:44'),
	(5, 8, '1', '2017-03-29 11:12:44', '2017-03-29 11:12:44');
/*!40000 ALTER TABLE `nb_campaign_main` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_campaign_point
CREATE TABLE IF NOT EXISTS `nb_campaign_point` (
  `id` int(11) NOT NULL,
  `point` varchar(255) DEFAULT NULL,
  `categories` int(11) DEFAULT NULL,
  `campaign` int(11) DEFAULT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_campaign_point: ~0 rows (approximately)
/*!40000 ALTER TABLE `nb_campaign_point` DISABLE KEYS */;
/*!40000 ALTER TABLE `nb_campaign_point` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_campaign_slide
CREATE TABLE IF NOT EXISTS `nb_campaign_slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `desc` text,
  `link` text,
  `link_d` enum('0','1') NOT NULL DEFAULT '1',
  `type` enum('1','2') NOT NULL DEFAULT '1',
  `crtd_at` datetime DEFAULT NULL,
  `edtd_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_campaign_slide: ~2 rows (approximately)
/*!40000 ALTER TABLE `nb_campaign_slide` DISABLE KEYS */;
REPLACE INTO `nb_campaign_slide` (`id`, `img`, `title`, `desc`, `link`, `link_d`, `type`, `crtd_at`, `edtd_at`) VALUES
	(2, 'image_20170208045532.jpg', 'coba lah', 'asdad', 'sadasd', '1', '1', '2017-02-08 16:55:32', '2017-04-21 13:44:29'),
	(3, 'image_20170316041234.jpg', 'coba repl', 'dasdas', 'sdasd', '1', '1', '2017-02-11 05:43:32', '2017-02-11 05:43:32');
/*!40000 ALTER TABLE `nb_campaign_slide` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_crawler_cek
CREATE TABLE IF NOT EXISTS `nb_crawler_cek` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(255) DEFAULT NULL,
  `nomuniq` decimal(20,0) DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `bank_crawler` varchar(255) DEFAULT NULL,
  `crtd_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_crawler_cek: 0 rows
/*!40000 ALTER TABLE `nb_crawler_cek` DISABLE KEYS */;
/*!40000 ALTER TABLE `nb_crawler_cek` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_donaturs
CREATE TABLE IF NOT EXISTS `nb_donaturs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(50) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `last_online` varchar(255) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `login_type` enum('default','oauth') DEFAULT 'default',
  `is_activate` enum('true','false','') DEFAULT 'false',
  `activated` enum('active','not-confirm') DEFAULT 'not-confirm',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL COMMENT 'last author',
  `forgotpass` int(11) DEFAULT NULL,
  `token_aktifasi` varchar(255) DEFAULT NULL,
  `oauth_provider` varchar(150) DEFAULT NULL,
  `oauth_uid` varchar(255) DEFAULT NULL,
  `profile_url` varchar(255) DEFAULT NULL,
  `picture_url` varchar(255) DEFAULT NULL,
  `fp_token` text NOT NULL,
  `fp_valid` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_donaturs: ~27 rows (approximately)
/*!40000 ALTER TABLE `nb_donaturs` DISABLE KEYS */;
REPLACE INTO `nb_donaturs` (`id`, `uname`, `pwd`, `email`, `last_online`, `last_login_ip`, `last_login_date`, `login_type`, `is_activate`, `activated`, `crtd_at`, `edtd_at`, `author`, `last_edit`, `forgotpass`, `token_aktifasi`, `oauth_provider`, `oauth_uid`, `profile_url`, `picture_url`, `fp_token`, `fp_valid`) VALUES
	(1, NULL, NULL, 'luthfi.an.25mei1991@gmail.com', NULL, '36.72.67.28', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'facebook', '221315988273703', NULL, NULL, '', '0000-00-00 00:00:00'),
	(2, NULL, NULL, 'fahmi.hilmansyah@gmail.com', NULL, '36.72.67.28', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'google', '102803666437573381460', NULL, 'https://lh5.googleusercontent.com/-fn3VwvNqHRY/AAAAAAAAAAI/AAAAAAAAALs/bEH7P340dQ8/photo.jpg', '', '0000-00-00 00:00:00'),
	(5, NULL, '2c7042a2e8efb394aa009d020c049d7fb33dfd040e', 'mychildhoodat91@gmail.com', NULL, NULL, NULL, 'default', 'true', 'active', '2017-04-03', '2017-04-03', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(7, NULL, '2c7042a2e89c2485e5ecd446e37e612817d470b6d6', 'ryan@niatbaik.co.id', NULL, NULL, NULL, 'default', 'true', 'active', '2017-04-03', '2017-04-06', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(9, NULL, NULL, 'endraabdulhadi@yahoo.com', NULL, '36.79.251.183', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'facebook', '10206947836493943', NULL, 'http://graph.facebook.com/10206947836493943/picture?type=large', '', '0000-00-00 00:00:00'),
	(10, NULL, NULL, 'developer.luthfi@gmail.com', NULL, '180.253.154.181', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'google', '101979030788614292332', NULL, NULL, '', '0000-00-00 00:00:00'),
	(11, NULL, '2c7042a2e88e27a3373d7e9c8b072b57b11da47401', 'ripfly71stone@gmail.com', NULL, NULL, NULL, 'default', 'true', 'active', '2017-04-06', '2017-04-06', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(12, NULL, NULL, 'sempak.melar1010@gmail.com', NULL, '36.72.67.28', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'google', '107680584243505245194', NULL, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '', '0000-00-00 00:00:00'),
	(13, NULL, NULL, 'abdulhadi.endra@gmail.com', NULL, '36.72.67.28', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'google', '109786741345158186504', NULL, 'https://lh5.googleusercontent.com/-cO-5pTQrNJM/AAAAAAAAAAI/AAAAAAAABLo/_a7v_2FEaBo/photo.jpg', '', '0000-00-00 00:00:00'),
	(14, NULL, '2c7042a2e83f2ea4e5234b853522ba5def3027c9ba', 'agus@suaramuslim.net', NULL, NULL, NULL, 'default', 'true', 'active', '2017-04-07', '2017-04-07', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(15, NULL, NULL, 'fahmiinayatulloh@yahoo.com', NULL, '103.213.131.112', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'facebook', '767673576744755', NULL, NULL, '', '0000-00-00 00:00:00'),
	(16, NULL, NULL, NULL, NULL, '36.80.40.48', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'facebook', '1446020875449962', NULL, NULL, '', '0000-00-00 00:00:00'),
	(17, NULL, NULL, 'niatbaikindonesia@gmail.com', NULL, '36.72.104.243', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'google', '100155055974259955496', NULL, 'https://lh5.googleusercontent.com/-1yIo9NePOTI/AAAAAAAAAAI/AAAAAAAAABc/Efbp2C1hy8o/photo.jpg', '', '0000-00-00 00:00:00'),
	(18, NULL, NULL, 'dinidiph@gmail.com', NULL, '118.137.45.99', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'facebook', '1459874540751289', NULL, NULL, '', '0000-00-00 00:00:00'),
	(19, NULL, NULL, 'fahmi.hilmansyah@gmail.com', NULL, '36.72.67.28', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'facebook', '10210589500671949', NULL, 'http://graph.facebook.com/10210589500671949/picture?type=large', '', '0000-00-00 00:00:00'),
	(20, NULL, NULL, 'ripfly71stone@gmail.com', NULL, '36.72.104.243', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'google', '101604988685381971035', NULL, 'https://lh4.googleusercontent.com/-WjCqHaEEIaE/AAAAAAAAAAI/AAAAAAAABAU/nv_wX0xFY2M/photo.jpg', '', '0000-00-00 00:00:00'),
	(21, NULL, NULL, 'akirasan.jrs@gmail.com', NULL, '36.79.251.183', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'facebook', '1615169621844468', NULL, 'http://graph.facebook.com/1615169621844468/picture?type=large', '', '0000-00-00 00:00:00'),
	(23, NULL, NULL, 'berzakatdotcom@gmail.com', NULL, '36.72.154.62', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'facebook', '167031153825785', NULL, 'http://graph.facebook.com/167031153825785/picture?type=large', '', '0000-00-00 00:00:00'),
	(48, NULL, '2c7042a2e8257f4b3f9ff8bb5f2cbf93aeb5ec0bcc', 'luthfiaziznugraha@gmail.com', NULL, NULL, NULL, 'default', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(49, NULL, NULL, 'luthfi.an.25mei1991@gmail.com', NULL, '36.78.208.170', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'facebook', '240739246331377', NULL, 'http://graph.facebook.com/240739246331377/picture?type=large', '', '0000-00-00 00:00:00'),
	(50, NULL, '2c7042a2e8b6f4efdd9968bbb5a00da6479dd4f794', 'endra@gmail.com', NULL, NULL, NULL, 'default', 'false', 'not-confirm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(53, NULL, '2c7042a2e8b6f4efdd9968bbb5a00da6479dd4f794', '3ndr4ah@gmail.com', NULL, NULL, NULL, 'default', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(57, NULL, NULL, 'akunresmi.ricky@gmail.com', NULL, NULL, NULL, 'default', 'false', 'not-confirm', '2017-05-16', '2017-05-16', NULL, NULL, NULL, '683d77c8681dbb78ff708d1a12bc837e', NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(58, NULL, '2c7042a2e8095e387f905f17fd1104c8da5fc9bc48', 'master_baim@yahoo.co.id', NULL, NULL, NULL, 'default', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(59, NULL, '2c7042a2e8095e387f905f17fd1104c8da5fc9bc48', 'baim@yahoo.co.id', NULL, NULL, NULL, 'default', 'false', 'not-confirm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(60, NULL, '2c7042a2e8095e387f905f17fd1104c8da5fc9bc48', 'baim@gg.com', NULL, NULL, NULL, 'default', 'false', 'not-confirm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(61, NULL, '2c7042a2e8095e387f905f17fd1104c8da5fc9bc48', 'yagami@gmail.com', NULL, NULL, NULL, 'default', 'false', 'not-confirm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(62, NULL, '2c7042a2e84e569b8c19e537bf022b7aed7fa2535e', 'nurulfbre@gmail.com', NULL, NULL, NULL, 'default', 'true', 'active', '2017-05-23', '2017-05-23', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00'),
	(63, NULL, NULL, 'infoydsfjakarta@gmail.com', NULL, '103.213.131.28', NULL, 'oauth', 'true', 'active', NULL, NULL, NULL, NULL, NULL, NULL, 'facebook', '10211832225499800', NULL, 'http://graph.facebook.com/10211832225499800/picture?type=large', '', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `nb_donaturs` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_donaturs_d
CREATE TABLE IF NOT EXISTS `nb_donaturs_d` (
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `sex` enum('male','female') DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `address` text,
  `address_alt` text,
  `phone` varchar(15) DEFAULT NULL,
  `donatur` int(11) NOT NULL DEFAULT '0',
  `info` text,
  `img` text,
  PRIMARY KEY (`donatur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_donaturs_d: ~28 rows (approximately)
/*!40000 ALTER TABLE `nb_donaturs_d` DISABLE KEYS */;
REPLACE INTO `nb_donaturs_d` (`fname`, `lname`, `sex`, `date_birth`, `address`, `address_alt`, `phone`, `donatur`, `info`, `img`) VALUES
	('Luthfi', 'An', 'male', NULL, NULL, NULL, NULL, 1, NULL, NULL),
	('fahmi', 'hilmansyah', 'male', NULL, NULL, NULL, '0812373778', 2, 'ada deh', ''),
	('Fahmi Hilmansyah', NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL),
	('Si Kucing Ngeong', NULL, NULL, NULL, NULL, NULL, '08129127598712', 7, '', 'image_20170406102500.png'),
	('Endra', 'Hadi', 'male', NULL, NULL, NULL, NULL, 9, NULL, NULL),
	('Aziz', 'Luthfi', 'male', NULL, NULL, NULL, NULL, 10, NULL, NULL),
	('Ryan', NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL),
	('Sempak', 'Melar', NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL),
	('abdulhadi', 'endra', NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL),
	('Agus', NULL, NULL, NULL, NULL, NULL, NULL, 14, NULL, NULL),
	('Fahmi', 'Inayatulloh', 'male', NULL, NULL, NULL, NULL, 15, NULL, NULL),
	('Risma', 'Fitriselfira', 'female', NULL, NULL, NULL, NULL, 16, NULL, NULL),
	('NIAT', 'BAIK', NULL, NULL, NULL, NULL, NULL, 17, NULL, NULL),
	('Dini', 'Prihartati', 'female', NULL, NULL, NULL, NULL, 18, NULL, NULL),
	('Fahmi', 'Hilmansyah', 'male', NULL, NULL, NULL, NULL, 19, NULL, NULL),
	('Rururu', '', 'male', NULL, NULL, NULL, NULL, 20, NULL, NULL),
	('Adhi', 'Herdiansyah', 'male', NULL, NULL, NULL, NULL, 21, NULL, NULL),
	('Berzakat', 'ID', 'female', NULL, NULL, NULL, NULL, 23, NULL, NULL),
	('Fahmi Hilmansyah', NULL, NULL, NULL, 'Cimahi', NULL, '085721547448', 48, 'Saya seorang programmer', NULL),
	('Luthfi', 'An', 'male', NULL, NULL, NULL, NULL, 49, NULL, NULL),
	('endra', NULL, NULL, NULL, NULL, NULL, NULL, 50, NULL, NULL),
	('andre', NULL, NULL, NULL, NULL, NULL, '0857266177280', 53, '', NULL),
	('ricky', NULL, NULL, NULL, NULL, NULL, NULL, 57, NULL, NULL),
	('Ibrahim Aghythara', NULL, NULL, NULL, NULL, NULL, NULL, 58, NULL, NULL),
	('Ibrahim', NULL, NULL, NULL, NULL, NULL, NULL, 59, NULL, NULL),
	('Ibrahim', NULL, NULL, NULL, NULL, NULL, NULL, 60, NULL, NULL),
	('Light Yagami', NULL, NULL, NULL, NULL, NULL, NULL, 61, NULL, NULL),
	('nurul', NULL, NULL, NULL, NULL, NULL, NULL, 62, NULL, NULL),
	('Laznas YDSF Jakarta', 'Jakarta', NULL, NULL, NULL, NULL, '081385888100', 63, 'Lembaga Amil Zakat Nasional SK Kemenag No 206 Tahun 2016', NULL);
/*!40000 ALTER TABLE `nb_donaturs_d` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_donaturs_mba
CREATE TABLE IF NOT EXISTS `nb_donaturs_mba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `donatur` int(11) DEFAULT NULL,
  `ccode` int(5) DEFAULT NULL,
  `ccode_valid` datetime DEFAULT NULL,
  `ccode_status` enum('0','1') DEFAULT '1' COMMENT '0:expired, 1:valid',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u7043207_berzakat.nb_donaturs_mba: ~12 rows (approximately)
/*!40000 ALTER TABLE `nb_donaturs_mba` DISABLE KEYS */;
REPLACE INTO `nb_donaturs_mba` (`id`, `donatur`, `ccode`, `ccode_valid`, `ccode_status`) VALUES
	(31, 47, 52945, '2017-05-08 15:46:19', '0'),
	(32, 47, 47710, '2017-05-08 15:52:25', '0'),
	(33, 48, 99314, '2017-05-08 16:16:29', '0'),
	(34, 48, 32278, '2017-05-08 16:17:11', '0'),
	(35, 50, 44612, '2017-05-09 15:43:48', '1'),
	(38, 53, 70000, '2017-05-10 12:18:11', '0'),
	(39, 53, 64102, '2017-05-10 12:18:40', '0'),
	(40, 58, 29585, '2017-05-16 18:37:29', '0'),
	(41, 59, 29579, '2017-05-17 16:44:12', '1'),
	(42, 60, 72722, '2017-05-17 17:29:52', '1'),
	(43, 61, 23977, '2017-05-17 17:36:03', '1'),
	(44, 58, 35273, '2017-05-18 10:37:45', '0');
/*!40000 ALTER TABLE `nb_donaturs_mba` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_donaturs_saldo
CREATE TABLE IF NOT EXISTS `nb_donaturs_saldo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `donatur` int(11) DEFAULT NULL,
  `prevBalance` decimal(20,0) DEFAULT NULL,
  `balanceAmount` decimal(20,0) DEFAULT NULL,
  `crtd_at` datetime DEFAULT NULL,
  `edtd_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_donaturs_saldo: ~36 rows (approximately)
/*!40000 ALTER TABLE `nb_donaturs_saldo` DISABLE KEYS */;
REPLACE INTO `nb_donaturs_saldo` (`id`, `donatur`, `prevBalance`, `balanceAmount`, `crtd_at`, `edtd_at`) VALUES
	(1, 1, 0, 0, '2017-04-03 16:16:01', '2017-04-03 16:16:01'),
	(2, 2, 351581, 251581, '2017-04-03 16:18:03', '2017-04-06 12:37:17'),
	(3, 3, 0, 0, '2017-04-03 16:21:03', '2017-04-03 16:21:03'),
	(4, 4, 0, 0, '2017-04-03 16:24:07', '2017-04-03 16:24:07'),
	(5, 5, 0, 0, '2017-04-03 16:27:44', '2017-04-03 16:27:44'),
	(6, 6, 0, 0, '2017-04-03 16:33:08', '2017-04-03 16:33:08'),
	(7, 7, 117739025, 117639, '2017-04-03 16:42:30', '2017-04-05 17:07:27'),
	(8, 8, 0, 0, '2017-04-03 16:47:35', '2017-04-03 16:47:35'),
	(9, 9, 0, 0, '2017-04-05 15:42:20', '2017-04-05 15:42:20'),
	(10, 10, 0, 0, '2017-04-05 16:06:54', '2017-04-05 16:06:54'),
	(11, 11, 0, 0, '2017-04-06 09:44:35', '2017-04-06 09:44:35'),
	(12, 12, 0, 0, '2017-04-06 10:24:55', '2017-04-06 10:24:55'),
	(13, 13, 0, 0, '2017-04-07 14:30:38', '2017-04-07 14:30:38'),
	(14, 14, 0, 0, '2017-04-07 14:59:48', '2017-04-07 14:59:48'),
	(15, 15, 0, 0, '2017-04-07 15:05:35', '2017-04-07 15:05:35'),
	(16, 16, 0, 0, '2017-04-08 11:34:44', '2017-04-08 11:34:44'),
	(17, 17, 0, 0, '2017-04-10 11:25:28', '2017-04-10 11:25:28'),
	(18, 18, 0, 0, '2017-04-11 19:55:49', '2017-04-11 19:55:49'),
	(19, 19, 0, 0, '2017-04-12 14:43:40', '2017-04-12 14:43:40'),
	(20, 20, 0, 0, '2017-04-18 15:52:14', '2017-04-18 15:52:14'),
	(21, 21, 0, 0, '2017-04-19 17:01:41', '2017-04-19 17:01:41'),
	(22, 22, 0, 0, '2017-04-25 11:45:28', '2017-04-25 11:45:28'),
	(23, 23, 0, 0, '2017-05-04 14:04:49', '2017-05-04 14:04:49'),
	(32, 47, 0, 0, '2017-05-08 00:00:00', '2017-05-08 00:00:00'),
	(33, 48, 0, 50624, '2017-05-08 00:00:00', '2017-05-16 18:43:44'),
	(34, 49, 0, 0, '2017-05-09 10:49:12', '2017-05-09 10:49:12'),
	(35, 50, 0, 0, '2017-05-09 00:00:00', '2017-05-09 00:00:00'),
	(38, 53, 0, 1000620, '2017-05-10 00:00:00', '2017-05-11 16:57:19'),
	(39, 54, 0, 0, '2017-05-12 11:33:47', '2017-05-12 11:33:47'),
	(40, 55, 0, 0, '2017-05-12 11:36:34', '2017-05-12 11:36:34'),
	(41, 56, 0, 0, '2017-05-12 16:29:27', '2017-05-12 16:29:27'),
	(42, 57, 0, 0, '2017-05-16 15:51:53', '2017-05-16 15:51:53'),
	(43, 58, 0, 0, '2017-05-16 00:00:00', '2017-05-16 00:00:00'),
	(44, 59, 0, 0, '2017-05-17 00:00:00', '2017-05-17 00:00:00'),
	(45, 60, 0, 0, '2017-05-17 00:00:00', '2017-05-17 00:00:00'),
	(46, 61, 0, 0, '2017-05-17 00:00:00', '2017-05-17 00:00:00'),
	(47, 62, 0, 0, '2017-05-23 16:10:57', '2017-05-23 16:10:57'),
	(48, 63, 0, 0, '2017-05-30 13:45:24', '2017-05-30 13:45:24');
/*!40000 ALTER TABLE `nb_donaturs_saldo` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_donaturs_trx
CREATE TABLE IF NOT EXISTS `nb_donaturs_trx` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `donaturs` int(11) DEFAULT NULL,
  `jenistrx` enum('TRX','DEPO','ERROR') DEFAULT 'ERROR',
  `id_transaksi` varchar(255) DEFAULT NULL,
  `prevBalance` decimal(20,0) DEFAULT NULL COMMENT 'saldo sebelumnya',
  `amount` decimal(20,0) DEFAULT NULL COMMENT 'transaksinya',
  `balance` decimal(20,0) NOT NULL COMMENT 'saldo akhir',
  `crtd_at` datetime DEFAULT NULL,
  `edtd_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`balance`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_donaturs_trx: ~20 rows (approximately)
/*!40000 ALTER TABLE `nb_donaturs_trx` DISABLE KEYS */;
REPLACE INTO `nb_donaturs_trx` (`id`, `donaturs`, `jenistrx`, `id_transaksi`, `prevBalance`, `amount`, `balance`, `crtd_at`, `edtd_at`) VALUES
	(1, 7, 'DEPO', '2017040300022', 0, 129397467, 129397467, '2017-04-03 17:31:10', '2017-04-03 17:31:10'),
	(2, 7, 'DEPO', '2017040300021', 129397467, 50557, 129448024, '2017-04-03 17:31:11', '2017-04-03 17:31:11'),
	(3, 7, 'DEPO', '2017040300020', 129448024, 1223515125, 1352963149, '2017-04-03 17:31:12', '2017-04-03 17:31:12'),
	(4, 7, 'TRX', '2017040300023', 1352963149, 100000000, 1252963149, '2017-04-03 17:34:33', '2017-04-03 17:34:33'),
	(5, 7, 'TRX', '2017040400005', 1252963149, 100000, 1252863149, '2017-04-04 14:52:11', '2017-04-04 14:52:11'),
	(6, 7, 'TRX', '2017040500003', 1252863149, 124124124, 1128739025, '2017-04-05 09:59:18', '2017-04-05 09:59:18'),
	(7, 7, 'TRX', '2017040500014', 1128739025, 1000000, 1127739025, '2017-04-05 15:33:26', '2017-04-05 15:33:26'),
	(8, 2, 'DEPO', '2017040500016', 0, 2501009, 2501009, '2017-04-05 15:46:39', '2017-04-05 15:46:39'),
	(9, 2, 'TRX', '2017040500017', 2501009, 100000, 2401009, '2017-04-05 15:47:38', '2017-04-05 15:47:38'),
	(10, 2, 'DEPO', '2017040500006', 2401009, 100572, 2501581, '2017-04-05 16:08:35', '2017-04-05 16:08:35'),
	(11, 7, 'TRX', '2017040500020', 1127739025, 5000000, 1122739025, '2017-04-05 16:18:26', '2017-04-05 16:18:26'),
	(12, 7, 'TRX', '2017040500021', 1122739025, 5000000, 1117739025, '2017-04-05 16:19:00', '2017-04-05 16:19:00'),
	(13, 7, 'TRX', '2017040500022', 1117739025, 1000000000, 117739025, '2017-04-05 16:19:43', '2017-04-05 16:19:43'),
	(14, 7, 'TRX', '2017040500025', 117739025, 100000, 117639025, '2017-04-05 17:07:27', '2017-04-05 17:07:27'),
	(15, 2, 'TRX', '2017040600009', 2501581, 1000000, 1501581, '2017-04-06 10:45:40', '2017-04-06 10:45:40'),
	(16, 2, 'TRX', '2017040600011', 1501581, 1000000, 501581, '2017-04-06 10:46:50', '2017-04-06 10:46:50'),
	(17, 2, 'TRX', '2017040600014', 501581, 150000, 351581, '2017-04-06 12:32:36', '2017-04-06 12:32:36'),
	(18, 2, 'TRX', '2017040600015', 351581, 100000, 251581, '2017-04-06 12:37:17', '2017-04-06 12:37:17'),
	(19, 53, 'DEPO', '2017051100006', 0, 1000620, 1000620, '2017-05-11 16:57:19', '2017-05-11 16:57:19'),
	(20, 48, 'DEPO', '2017051600001', 0, 50624, 50624, '2017-05-16 18:43:44', '2017-05-16 18:43:44');
/*!40000 ALTER TABLE `nb_donaturs_trx` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_donatur_tokens
CREATE TABLE IF NOT EXISTS `nb_donatur_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `valid_until` datetime DEFAULT NULL,
  `donatur` int(255) DEFAULT NULL,
  `donatur_ip` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=351 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_donatur_tokens: ~347 rows (approximately)
/*!40000 ALTER TABLE `nb_donatur_tokens` DISABLE KEYS */;
REPLACE INTO `nb_donatur_tokens` (`id`, `code`, `valid_until`, `donatur`, `donatur_ip`) VALUES
	(1, 'de847df79d25ca9fd20f1683f60047ec', '2017-04-03 17:16:01', 1, '36.80.90.145'),
	(2, '918f2451fba2cd5cfb866822fc2d51b7', '2017-04-03 17:18:03', 2, '36.80.90.145'),
	(3, 'f74d3d0f3cf460c17e7902952f768e11', '2017-04-03 17:21:03', 3, '36.80.90.145'),
	(4, 'b2042c22bfceeae9a4282d1f242300c6', '2017-04-03 17:24:07', 4, '36.80.90.145'),
	(5, '49a9f9c30f0a01d3f867b1110a3043b2', '2017-04-03 17:24:57', 2, '36.80.90.145'),
	(6, '4daee770a0666e3fda21c6a85300e4f8', '2017-04-03 17:29:13', 5, '36.80.90.145'),
	(7, '896ddab5df8167722ce7d5d70ca6dade', '2017-04-03 17:29:14', 5, '36.80.90.145'),
	(8, '3cfe0fc412e4f8047f389aadd4837223', '2017-04-03 17:34:08', 6, '36.80.90.145'),
	(9, 'ec8dcc809980aae50f6977b98ff515c8', '2017-04-03 17:47:39', 7, '36.80.90.145'),
	(10, '8d76a852a9dbfa1f06538c9c9c8b2b4e', '2017-04-03 18:31:37', 7, '36.80.90.145'),
	(11, '98393ffc7c60e9d48e22774a5b139751', '2017-04-03 18:42:24', 7, '36.80.90.145'),
	(12, '23e4b031acb9ef23975a2eeb047435e7', '2017-04-03 18:55:56', 7, '36.80.90.145'),
	(13, 'd11b709718ad548ae17c61545685fa4b', '2017-04-04 09:59:19', 7, '36.80.90.145'),
	(14, 'be0af94d77dcde46a7a1dfef593bc90a', '2017-04-04 15:51:49', 7, '180.253.166.90'),
	(15, 'f2e4826f945575c0e9db125eae65e19b', '2017-04-04 18:50:12', 4, '36.72.123.217'),
	(16, 'ed87a75d3c9f2bc726a4e910caef3d09', '2017-04-05 10:57:31', 7, '180.253.154.181'),
	(17, 'ba89a5f49923e09e2ec78be3078108f8', '2017-04-05 11:34:02', 2, '180.253.154.181'),
	(18, 'c6c5d9a13616cf90c68e8cd032a8edac', '2017-04-05 11:39:48', 4, '180.253.154.181'),
	(19, '0e07c6d117b1e73286c91fa7fbde572e', '2017-04-05 14:42:02', 4, '180.253.154.181'),
	(20, 'f90df5548bc6ad45ea0acc59f0b62668', '2017-04-05 15:09:59', 4, '180.253.154.181'),
	(21, '0fd4a29c4edcc4ffb418c19872b44510', '2017-04-05 16:32:56', 7, '180.253.154.181'),
	(22, '135234bf37384fcafc8835a9327d268d', '2017-04-05 16:38:15', 2, '180.253.154.181'),
	(23, 'e4458360da22bad2ef68eeb1b74ba9e5', '2017-04-05 16:42:20', 9, '180.253.154.181'),
	(24, 'ebcab5e93fef3d469a5248f25a9d5916', '2017-04-05 16:47:03', 2, '180.253.154.181'),
	(25, '6e7f0bfbbef972dab9fe856b7689169a', '2017-04-05 17:06:54', 10, '180.253.154.181'),
	(26, '741c506f51ce9bf47b6548159f953028', '2017-04-05 17:07:45', 10, '180.253.154.181'),
	(27, '1873910234d3f2a4145a9612a52dd042', '2017-04-05 17:08:14', 10, '180.253.154.181'),
	(28, 'e7c473e2e26954d1890c258422cf7d09', '2017-04-05 17:09:18', 7, '180.253.154.181'),
	(29, '3f66f8df99ac687e41647522a158c8a5', '2017-04-05 17:12:57', 1, '180.253.154.181'),
	(30, '1dd29ff55bc703cb3cef3da3d2d4b2b9', '2017-04-05 17:13:15', 1, '180.253.154.181'),
	(31, '9e0af60f70f2d9f94ff08a52ef6554df', '2017-04-05 17:13:43', 5, '180.253.154.181'),
	(32, 'a02a5c2fcf2473d1c3cbf5ff98de6b12', '2017-04-05 17:16:36', 4, '180.253.154.181'),
	(33, '6473117809ad73791ad29a2626fc3002', '2017-04-05 18:14:51', 7, '180.253.154.181'),
	(34, 'a96dfee27df38018cb9695fd36e8696a', '2017-04-06 10:01:28', 4, '180.253.154.181'),
	(35, '7702f1d7478dc48b46e9c136261e261c', '2017-04-06 10:01:29', 4, '180.253.154.181'),
	(36, '089f8e1578a1397d5c9fdca99c6d509d', '2017-04-06 10:01:30', 4, '180.253.154.181'),
	(37, 'ddbefd8bce4a6a84ffb1adea1ed85c54', '2017-04-06 10:01:31', 4, '180.253.154.181'),
	(38, '0490a25203edda7d93e093f43ccc133c', '2017-04-06 10:01:32', 4, '180.253.154.181'),
	(39, '21c02ac3ac68fa2b92fdb8f4c5b65000', '2017-04-06 10:01:33', 4, '180.253.154.181'),
	(40, '8e3b393c569721df1d49ad2070d2d0bc', '2017-04-06 10:01:34', 4, '180.253.154.181'),
	(41, '56161a0c249a837f8f156721cb0ef78e', '2017-04-06 10:01:35', 4, '180.253.154.181'),
	(42, 'ffaef6e45390229dd6923c0810465eae', '2017-04-06 10:01:36', 4, '180.253.154.181'),
	(43, '1d6f5643e0fc3d25d5cd9db535b2f484', '2017-04-06 10:01:37', 4, '180.253.154.181'),
	(44, '5292cf21d8f284c53110d8def215a60d', '2017-04-06 10:01:38', 4, '180.253.154.181'),
	(45, '94b2625620796d777e21496152b11120', '2017-04-06 10:01:40', 4, '180.253.154.181'),
	(46, '94b2625620796d777e21496152b11120', '2017-04-06 10:01:40', 4, '180.253.154.181'),
	(47, '324f332fcb68c2b16ea570726ebfd730', '2017-04-06 10:01:41', 4, '180.253.154.181'),
	(48, '7e5bcb7e92cb5b8fdd9bf7a53e566a21', '2017-04-06 10:01:42', 4, '180.253.154.181'),
	(49, '9b886ce3e794cd9fc92b2673215fdf12', '2017-04-06 10:01:43', 4, '180.253.154.181'),
	(50, '7258ba335a6bd1b7570659439c2109ac', '2017-04-06 10:01:44', 4, '180.253.154.181'),
	(51, '3b72b63126048ea763272310297e0abe', '2017-04-06 10:01:45', 4, '180.253.154.181'),
	(52, '9d45a238369caa38e4a126fcdc171f98', '2017-04-06 10:01:47', 4, '180.253.154.181'),
	(53, '2327b14cdf75edb908e28c8a77b64efb', '2017-04-06 10:01:48', 4, '180.253.154.181'),
	(54, '36c16172c971bf4dedec8df8c88f4b61', '2017-04-06 10:01:49', 4, '180.253.154.181'),
	(55, '4d6b4dfac971059444eae9e1fdac5da5', '2017-04-06 10:01:51', 4, '180.253.154.181'),
	(56, '4d6b4dfac971059444eae9e1fdac5da5', '2017-04-06 10:01:51', 4, '180.253.154.181'),
	(57, '78a89027b77be84e9c72afca134fd098', '2017-04-06 10:01:52', 4, '180.253.154.181'),
	(58, 'd18191816f4dfb6c60dc55e8a6eb73e8', '2017-04-06 10:01:53', 4, '180.253.154.181'),
	(59, '40a000209c536030c742ba710d9aa077', '2017-04-06 10:01:54', 4, '180.253.154.181'),
	(60, 'c187c7d697525652760ece0459669dc7', '2017-04-06 10:01:55', 4, '180.253.154.181'),
	(61, '0d2f4ed994cd7e0aad59cf2f517ac0ec', '2017-04-06 10:01:56', 4, '180.253.154.181'),
	(62, '0cfb41f88dce51a2e706a2eadc0402f2', '2017-04-06 10:01:57', 4, '180.253.154.181'),
	(63, '815873a466f236353b697bf1b5ec75f1', '2017-04-06 10:01:58', 4, '180.253.154.181'),
	(64, '8e8d59eff76d317ca1a723ac5507f00c', '2017-04-06 10:06:09', 4, '180.253.154.181'),
	(65, '8e8d59eff76d317ca1a723ac5507f00c', '2017-04-06 10:06:09', 4, '180.253.154.181'),
	(66, 'fe1d1be33afc498cc534df4e6c4af9a2', '2017-04-06 10:06:10', 4, '180.253.154.181'),
	(67, '88a2365420133120cb977481a0cdba48', '2017-04-06 10:06:11', 4, '180.253.154.181'),
	(68, '88a2365420133120cb977481a0cdba48', '2017-04-06 10:06:11', 4, '180.253.154.181'),
	(69, '66c7244c12625bb530dfd810b05e5216', '2017-04-06 10:06:12', 4, '180.253.154.181'),
	(70, '32c4c66b32b0bb1ca435651fe3548ca3', '2017-04-06 10:06:13', 4, '180.253.154.181'),
	(71, '32c4c66b32b0bb1ca435651fe3548ca3', '2017-04-06 10:06:13', 4, '180.253.154.181'),
	(72, '4ecaa676c285222019bef47a890525a7', '2017-04-06 10:06:14', 4, '180.253.154.181'),
	(73, 'e768d69447ac15b7a03632c3857a4a49', '2017-04-06 10:06:15', 4, '180.253.154.181'),
	(74, '3108dbf05c8ebf4f952a3a77f8071de4', '2017-04-06 10:06:16', 4, '180.253.154.181'),
	(75, '3634969ecdac1e98bde3d78cb3f734e0', '2017-04-06 10:06:17', 4, '180.253.154.181'),
	(76, '1e6590ce962b2788f28f69dc7961c8ba', '2017-04-06 10:06:18', 4, '180.253.154.181'),
	(77, '3c62897fb918b1a3b570213d3b615dec', '2017-04-06 10:06:19', 4, '180.253.154.181'),
	(78, '63b97504e61b67e7a0c22ea7ec29d7f2', '2017-04-06 10:06:20', 4, '180.253.154.181'),
	(79, '5b25337d88b1b69984fb4afbc7940fac', '2017-04-06 10:06:21', 4, '180.253.154.181'),
	(80, '3ad8457391952e5b86040db6da8241f7', '2017-04-06 10:06:22', 4, '180.253.154.181'),
	(81, '8e3eeb3eef5a6dd46ed18e364edc3618', '2017-04-06 10:06:23', 4, '180.253.154.181'),
	(82, 'f22413c4db1f0e45ab546c91678ad83a', '2017-04-06 10:06:24', 4, '180.253.154.181'),
	(83, '235815d2dd4bb914ce0d065e00233310', '2017-04-06 10:06:25', 4, '180.253.154.181'),
	(84, '6dc80b5c03f58d753a8e357abb7e151c', '2017-04-06 10:06:26', 4, '180.253.154.181'),
	(85, 'd5a929b581a2e030345d05eb1967731b', '2017-04-06 10:06:27', 4, '180.253.154.181'),
	(86, 'b6578c1f2097ae746eac7d820847bc77', '2017-04-06 10:06:28', 4, '180.253.154.181'),
	(87, 'e7838a41bcd06b04fbcfe08ecf6eba60', '2017-04-06 10:06:29', 4, '180.253.154.181'),
	(88, '7f59f0bea395297c7d448d06e81913e6', '2017-04-06 10:06:30', 4, '180.253.154.181'),
	(89, '97d101806f3f32eaf9d2091050bdaf23', '2017-04-06 10:06:31', 4, '180.253.154.181'),
	(90, '679b959868b808e6976a12d96da04a53', '2017-04-06 10:06:32', 4, '180.253.154.181'),
	(91, '3c774b8c69e98cda77030bf3561c7ec2', '2017-04-06 10:06:33', 4, '180.253.154.181'),
	(92, '6e72e0b40c11ab7e985ac97e293e4c60', '2017-04-06 10:06:34', 4, '180.253.154.181'),
	(93, 'db18bd5b760d54be709b68700efc6461', '2017-04-06 10:06:35', 4, '180.253.154.181'),
	(94, 'c302b27382b7c4c6a41c7f1815c290e6', '2017-04-06 10:06:36', 4, '180.253.154.181'),
	(95, '8d5d1c3de32a0701971a352d00c83abb', '2017-04-06 10:06:37', 4, '180.253.154.181'),
	(96, 'd01aa3fe5a8023222acc6d54950b157f', '2017-04-06 10:06:38', 4, '180.253.154.181'),
	(97, '6e3b752c3c1dffe62bc9584cf927354e', '2017-04-06 10:06:39', 4, '180.253.154.181'),
	(98, 'c859588e621d70508d6588def53abc1e', '2017-04-06 10:06:40', 4, '180.253.154.181'),
	(99, '77ee3d1958e9f3c0d0704845c0ec47ea', '2017-04-06 10:06:41', 4, '180.253.154.181'),
	(100, '353ff9ead3ae8df1d9cf504b9930fe5c', '2017-04-06 10:06:42', 4, '180.253.154.181'),
	(101, 'de300f3580ad688c8dcfaec39904d0d9', '2017-04-06 10:06:44', 4, '180.253.154.181'),
	(102, 'de300f3580ad688c8dcfaec39904d0d9', '2017-04-06 10:06:44', 4, '180.253.154.181'),
	(103, '466875e36daf3a20b70a47e5ebb39946', '2017-04-06 10:06:45', 4, '180.253.154.181'),
	(104, 'd29444613d16988fd39c21e749593058', '2017-04-06 10:06:51', 4, '180.253.154.181'),
	(105, '5e14888e864735c3a3d7dc3f883cd776', '2017-04-06 10:06:52', 4, '180.253.154.181'),
	(106, '5e14888e864735c3a3d7dc3f883cd776', '2017-04-06 10:06:52', 4, '180.253.154.181'),
	(107, 'fd8458f5952cd5a97920bbaff39be3c0', '2017-04-06 10:06:54', 4, '180.253.154.181'),
	(108, '458cda0cf382ef942489c0ccca7a5f4e', '2017-04-06 10:06:55', 4, '180.253.154.181'),
	(109, '470db2a8f7d83cc065e49508d067dc23', '2017-04-06 10:06:56', 4, '180.253.154.181'),
	(110, 'a15d39c08d4bc48e3b4c0ef59c535476', '2017-04-06 10:06:57', 4, '180.253.154.181'),
	(111, 'a15d39c08d4bc48e3b4c0ef59c535476', '2017-04-06 10:06:57', 4, '180.253.154.181'),
	(112, 'ba93bcd097526ba6d1179ebdb4c69962', '2017-04-06 10:06:58', 4, '180.253.154.181'),
	(113, 'fe7f1824eb44c89345208881b3115831', '2017-04-06 10:06:59', 4, '180.253.154.181'),
	(114, '22edc9bb1708ab75940aef6e7a4362bb', '2017-04-06 10:07:00', 4, '180.253.154.181'),
	(115, '8c702664d5ae32a9205f4da61dd1368e', '2017-04-06 10:07:01', 4, '180.253.154.181'),
	(116, '3eb5a3a1b7f3d4f8aa9e3a96fc3c7125', '2017-04-06 10:07:02', 4, '180.253.154.181'),
	(117, '111de85268680797153a9f38b1b8c491', '2017-04-06 10:07:03', 4, '180.253.154.181'),
	(118, '09f19375449a34f2a643e4a0c5225599', '2017-04-06 10:07:04', 4, '180.253.154.181'),
	(119, '28bb5aea4166cc5348a2155c3486a68e', '2017-04-06 10:07:05', 4, '180.253.154.181'),
	(120, 'bf1ea3ab7beaa40e7c4f8003cc82890c', '2017-04-06 10:07:06', 4, '180.253.154.181'),
	(121, 'bf1ea3ab7beaa40e7c4f8003cc82890c', '2017-04-06 10:07:06', 4, '180.253.154.181'),
	(122, '49f947945e30189107fb0355a9ac7681', '2017-04-06 10:07:07', 4, '180.253.154.181'),
	(123, '8bd429d3687ea9e3d7acfc7e3a7b01a3', '2017-04-06 10:07:08', 4, '180.253.154.181'),
	(124, '7139755e4df32baae852a8ea113072af', '2017-04-06 10:07:09', 4, '180.253.154.181'),
	(125, '06591dce45debff1a6a092166909e3bf', '2017-04-06 10:07:36', 4, '180.253.154.181'),
	(126, '82d3e18244b5b2b01415290a0c664eb2', '2017-04-06 10:07:37', 4, '180.253.154.181'),
	(127, 'f29892108f6d0dd7c3791b9f1bdeb253', '2017-04-06 10:07:38', 4, '180.253.154.181'),
	(128, 'afc230d74e9e1d958995ac51c9b0eac7', '2017-04-06 10:07:39', 4, '180.253.154.181'),
	(129, '7d22e5a3d0c8c77f938fe37413ab97f0', '2017-04-06 10:07:43', 4, '180.253.154.181'),
	(130, 'b941138a7ec87847b405bf1179539926', '2017-04-06 10:07:44', 4, '180.253.154.181'),
	(131, '010ec11e4aa1eeae5c7cd62b35e72c17', '2017-04-06 10:07:45', 4, '180.253.154.181'),
	(132, '3cb3eaa0cde28268970d92054a3fa837', '2017-04-06 10:07:46', 4, '180.253.154.181'),
	(133, '8f46dfbfd0792cc0cde1c065c71936cf', '2017-04-06 10:07:47', 4, '180.253.154.181'),
	(134, '7be25e367baeba1df3458292081b84d8', '2017-04-06 10:07:54', 4, '180.253.154.181'),
	(135, '80c605668a9d009f29160a2b34d60ff9', '2017-04-06 10:07:55', 4, '180.253.154.181'),
	(136, '80c605668a9d009f29160a2b34d60ff9', '2017-04-06 10:07:55', 4, '180.253.154.181'),
	(137, 'd85a3bdab1f68f746b9570a0c7323102', '2017-04-06 10:07:56', 4, '180.253.154.181'),
	(138, '24bc5189d83be8ed461f380c19a5d57f', '2017-04-06 10:07:57', 4, '180.253.154.181'),
	(139, '5fb5a9a190f38c342a3a896dec92dd2f', '2017-04-06 10:07:58', 4, '180.253.154.181'),
	(140, 'a133b5b216eda7aeda94113694f5d85c', '2017-04-06 10:07:59', 4, '180.253.154.181'),
	(141, '9210b9a94c448468a8a0b92a85ee5f37', '2017-04-06 10:08:00', 4, '180.253.154.181'),
	(142, '7f74bf47aff89d309a2e7ddc7388e6e2', '2017-04-06 10:08:01', 4, '180.253.154.181'),
	(143, '7cc30984371e63d5a193ba88515ace1f', '2017-04-06 10:08:02', 4, '180.253.154.181'),
	(144, '162146fb581cf607c358630d5b67a6d7', '2017-04-06 10:08:03', 4, '180.253.154.181'),
	(145, '8c880760249ceb035166654500b027ae', '2017-04-06 10:08:04', 4, '180.253.154.181'),
	(146, 'f986a5c9976bc386d16c743b43802c5b', '2017-04-06 10:50:11', 11, '180.253.154.181'),
	(147, '5ea5234b03cfb06583635d6624555dfa', '2017-04-06 11:16:21', 7, '180.253.154.181'),
	(148, '5c0908e85470aa9b4bf1841f8169d983', '2017-04-06 11:20:58', 2, '180.253.154.181'),
	(149, '12849696636f4e1742c1dd27d551eb73', '2017-04-06 11:20:59', 2, '180.253.154.181'),
	(150, '7cb484bc8b009cbd2cd0c5f22658540d', '2017-04-06 11:21:00', 2, '180.253.154.181'),
	(151, '176b6a714ae30a77eaf0e7fdd54222c5', '2017-04-06 11:21:01', 2, '180.253.154.181'),
	(152, 'e88be3d6d6a955835f6d8dbdfbfe94c9', '2017-04-06 11:21:02', 2, '180.253.154.181'),
	(153, 'c413836ad1a0ee7f3f31e13614ec22a1', '2017-04-06 11:21:03', 2, '180.253.154.181'),
	(154, 'ab618cd24cbd602e8a3d37512c79743b', '2017-04-06 11:21:04', 2, '180.253.154.181'),
	(155, '94b619e447b059b755a4014370aed92a', '2017-04-06 11:21:05', 2, '180.253.154.181'),
	(156, '45dd1e723f4d4e17762913ceac94112c', '2017-04-06 11:21:06', 2, '180.253.154.181'),
	(157, 'f62b539403791960b91c9efa4b21157d', '2017-04-06 11:21:07', 2, '180.253.154.181'),
	(158, 'f62b539403791960b91c9efa4b21157d', '2017-04-06 11:21:07', 2, '180.253.154.181'),
	(159, 'a647d98938865866b8f5d499c731927e', '2017-04-06 11:21:08', 2, '180.253.154.181'),
	(160, '472538fee74aa4c5571944eddcadc693', '2017-04-06 11:21:09', 2, '180.253.154.181'),
	(161, '28d49a839e574d44773499aca12c53f4', '2017-04-06 11:21:10', 2, '180.253.154.181'),
	(162, '737a1f7d7a34d11040e37c68a83d6ec4', '2017-04-06 11:21:11', 2, '180.253.154.181'),
	(163, '6bfff83c282edf09ebe922695804000b', '2017-04-06 11:21:12', 2, '180.253.154.181'),
	(164, '6bfff83c282edf09ebe922695804000b', '2017-04-06 11:21:12', 2, '180.253.154.181'),
	(165, '274a86feeedb4fe082c3ec005e9d0ff3', '2017-04-06 11:21:14', 2, '180.253.154.181'),
	(166, '274a86feeedb4fe082c3ec005e9d0ff3', '2017-04-06 11:21:14', 2, '180.253.154.181'),
	(167, '5fc5c678c308c0202b8cdbec07ff8533', '2017-04-06 11:21:25', 2, '180.253.154.181'),
	(168, 'e5be0e926eac064f564dbe8147978973', '2017-04-06 11:21:26', 2, '180.253.154.181'),
	(169, '7a81e412c7ae4f1278036cafe372007a', '2017-04-06 11:21:27', 2, '180.253.154.181'),
	(170, 'd81f62f383de1b54681c7c81d1f26c4d', '2017-04-06 11:21:28', 2, '180.253.154.181'),
	(171, 'd81f62f383de1b54681c7c81d1f26c4d', '2017-04-06 11:21:28', 2, '180.253.154.181'),
	(172, '78fcaa40677007955bfebe4273155c99', '2017-04-06 11:21:29', 2, '180.253.154.181'),
	(173, '0f63d982706b4d09b21f465cf92fbc6e', '2017-04-06 11:21:30', 2, '180.253.154.181'),
	(174, 'eddf74efeb3c4fbda73b9b7e997dc5d0', '2017-04-06 11:21:31', 2, '180.253.154.181'),
	(175, '749b5cdf4f0094b455c2ad01fb210d0f', '2017-04-06 11:21:32', 2, '180.253.154.181'),
	(176, '56fb5c2af34c7bc9cbf1b21dd15f6fad', '2017-04-06 11:21:33', 2, '180.253.154.181'),
	(177, '642ae56461df0bdd784460fe11f74a13', '2017-04-06 11:21:34', 2, '180.253.154.181'),
	(178, '642ae56461df0bdd784460fe11f74a13', '2017-04-06 11:21:34', 2, '180.253.154.181'),
	(179, 'ba6def58f87074c84c8bb7f5ab09a690', '2017-04-06 11:21:35', 2, '180.253.154.181'),
	(180, '84d62eb89e76f2fd23f6d603829c6ce1', '2017-04-06 11:21:36', 2, '180.253.154.181'),
	(181, '2c0b17414fcc075af6b1c94b759ae179', '2017-04-06 11:21:37', 2, '180.253.154.181'),
	(182, 'dfc4a0177e6eeeeb24a21ad21d8b2ead', '2017-04-06 11:21:38', 2, '180.253.154.181'),
	(183, '13c4667fcf811d97f99fdf470a062c87', '2017-04-06 11:21:39', 2, '180.253.154.181'),
	(184, '0b821312855d257f40fab6bf5cd1d66c', '2017-04-06 11:21:40', 2, '180.253.154.181'),
	(185, 'ccfa3af403025f6c3f6fad775abe8a0b', '2017-04-06 11:21:41', 2, '180.253.154.181'),
	(186, '4e2ed5cfc14c8cfa1de786ed2c485ce1', '2017-04-06 11:21:42', 2, '180.253.154.181'),
	(187, 'f5ed5f75b0f878fd582fe4367ca230f3', '2017-04-06 11:21:43', 2, '180.253.154.181'),
	(188, 'f5ed5f75b0f878fd582fe4367ca230f3', '2017-04-06 11:21:43', 2, '180.253.154.181'),
	(189, 'fd53c18e0cf0007f579b74821c4486ca', '2017-04-06 11:21:44', 2, '180.253.154.181'),
	(190, '2623f6609c693db096da252c82cb6469', '2017-04-06 11:21:45', 2, '180.253.154.181'),
	(191, 'b293c7172f9d77445f7df0d35a96e473', '2017-04-06 11:21:46', 2, '180.253.154.181'),
	(192, 'a687afee385e7fd459fa0d78b498bcc1', '2017-04-06 11:21:47', 2, '180.253.154.181'),
	(193, '75b95cbbc9b7f8266889fa5c82307ae2', '2017-04-06 11:22:02', 2, '180.253.154.181'),
	(194, '75b95cbbc9b7f8266889fa5c82307ae2', '2017-04-06 11:22:02', 2, '180.253.154.181'),
	(195, '39aaa9391f06f791aaee47f92596483f', '2017-04-06 11:22:03', 2, '180.253.154.181'),
	(196, '6d93d1eb2275e5f5e1f5d2d55e899160', '2017-04-06 11:22:04', 2, '180.253.154.181'),
	(197, 'd423036c54580534b56b5c7b59a0d67e', '2017-04-06 11:22:05', 2, '180.253.154.181'),
	(198, '80a6d5779d06ea9a4df449641f951560', '2017-04-06 11:22:06', 2, '180.253.154.181'),
	(199, '8c38bc73987ab8938abe128661ab2afb', '2017-04-06 11:22:07', 2, '180.253.154.181'),
	(200, 'd6e38e88a95a83af706c2b382f48f5cd', '2017-04-06 11:22:08', 2, '180.253.154.181'),
	(201, '1ed763d05adc7d8edd9c455ac7c47f92', '2017-04-06 11:22:09', 2, '180.253.154.181'),
	(202, '3f8b48690be653b6deba4c76d8d5b9a0', '2017-04-06 11:22:10', 2, '180.253.154.181'),
	(203, '3f8b48690be653b6deba4c76d8d5b9a0', '2017-04-06 11:22:10', 2, '180.253.154.181'),
	(204, 'e2e3766ec865d0b210d1c435a7c86919', '2017-04-06 11:22:11', 2, '180.253.154.181'),
	(205, '639b87a7a068ad646aa85cdba15fb0a9', '2017-04-06 11:22:12', 2, '180.253.154.181'),
	(206, 'b0a54f0fe61ebeb99531a2a4b4559c51', '2017-04-06 11:22:13', 2, '180.253.154.181'),
	(207, 'a829d5950fdb8150bb4a209c73da0795', '2017-04-06 11:22:14', 2, '180.253.154.181'),
	(208, 'd2685c0157905389a84ccc75a345e102', '2017-04-06 11:22:15', 2, '180.253.154.181'),
	(209, '998cb9818f170c6a1d94d87272ed91c8', '2017-04-06 11:22:16', 2, '180.253.154.181'),
	(210, '4bf25e71cdddf6cf24d62c581ccad51b', '2017-04-06 11:22:17', 2, '180.253.154.181'),
	(211, '9d7d2c8c7ca3e6fe6344c13906c69248', '2017-04-06 11:22:18', 2, '180.253.154.181'),
	(212, '661559b386816b7d45ea89d8abd4f3ea', '2017-04-06 11:22:19', 2, '180.253.154.181'),
	(213, 'bdd21cb906ef616988ad0a8ee787da81', '2017-04-06 11:22:50', 2, '180.253.154.181'),
	(214, 'bdd21cb906ef616988ad0a8ee787da81', '2017-04-06 11:22:50', 2, '180.253.154.181'),
	(215, '5727446bf5e254099395c7170b924a7e', '2017-04-06 11:22:51', 2, '180.253.154.181'),
	(216, '32466a0d6d35677d5d91246e0aebf91e', '2017-04-06 11:22:52', 2, '180.253.154.181'),
	(217, '8b0756ecf68823c695d96a7628680d17', '2017-04-06 11:22:53', 2, '180.253.154.181'),
	(218, '7444a610009b98e1a81152731c7f27e3', '2017-04-06 11:22:54', 2, '180.253.154.181'),
	(219, '9c76d5ebae4ad3298f26ed4206d5d69e', '2017-04-06 11:22:55', 2, '180.253.154.181'),
	(220, 'a531bacc1d59d5cf3fc17c78637944c9', '2017-04-06 11:22:56', 2, '180.253.154.181'),
	(221, 'f076d937871fbafe6eb79f58b49f5a17', '2017-04-06 11:22:57', 2, '180.253.154.181'),
	(222, '6ac7745f53b653d8e541a2df265fc65a', '2017-04-06 11:22:58', 2, '180.253.154.181'),
	(223, 'a4dd69cfd87e0b685d890ffb81a0f452', '2017-04-06 11:22:59', 2, '180.253.154.181'),
	(224, '7172a2c30a4fc46e363678229172b82f', '2017-04-06 11:23:00', 2, '180.253.154.181'),
	(225, 'd120359347fa68874cdaf1e9dc20f8a9', '2017-04-06 11:23:01', 2, '180.253.154.181'),
	(226, 'd120359347fa68874cdaf1e9dc20f8a9', '2017-04-06 11:23:01', 2, '180.253.154.181'),
	(227, '7fcce38f6749190f8bffee0185e7797b', '2017-04-06 11:23:02', 2, '180.253.154.181'),
	(228, 'd52d5eb9897b9026827063887ac972eb', '2017-04-06 11:23:03', 2, '180.253.154.181'),
	(229, 'd53962b7a59131f307d5eae8fa7cf879', '2017-04-06 11:23:04', 2, '180.253.154.181'),
	(230, '4b25683fc1fc4a23937d406650a64a67', '2017-04-06 11:23:05', 2, '180.253.154.181'),
	(231, '1060a2de7aedc13c374c4cd4bd810728', '2017-04-06 11:23:06', 2, '180.253.154.181'),
	(232, 'f326a6b01c0eeeb52b7aa4d9d47fd1f1', '2017-04-06 11:24:55', 12, '180.253.154.181'),
	(233, '5fa5c6b0cab59154b2cd850257ea298e', '2017-04-06 11:24:56', 12, '180.253.154.181'),
	(234, '5fa5c6b0cab59154b2cd850257ea298e', '2017-04-06 11:24:56', 12, '180.253.154.181'),
	(235, '8392520e14cbbc09ec9379f61146130b', '2017-04-06 11:24:57', 12, '180.253.154.181'),
	(236, '9022fce69ee9cf322d1b3de1b3f0e561', '2017-04-06 11:24:58', 12, '180.253.154.181'),
	(237, '291fb4406f7dfe41d263f6f9874adf90', '2017-04-06 11:24:59', 12, '180.253.154.181'),
	(238, '291fb4406f7dfe41d263f6f9874adf90', '2017-04-06 11:24:59', 12, '180.253.154.181'),
	(239, '79aa6420031c1be3ae34e5d36dda0103', '2017-04-06 11:25:00', 12, '180.253.154.181'),
	(240, 'e3bc15e1c0e79221d5d5a0364c0d4852', '2017-04-06 11:25:01', 12, '180.253.154.181'),
	(241, '902044cbd71f8346007f9ab8ef7ee9ab', '2017-04-06 11:25:02', 12, '180.253.154.181'),
	(242, 'ed31d83e921b045ed14d499bb4d96f8d', '2017-04-06 11:25:03', 12, '180.253.154.181'),
	(243, 'e1aa5d0fb71f899a7d9c41b914c3583f', '2017-04-06 11:25:04', 12, '180.253.154.181'),
	(244, 'e1aa5d0fb71f899a7d9c41b914c3583f', '2017-04-06 11:25:04', 12, '180.253.154.181'),
	(245, 'b6d1e975298e5a5a4b4c4840ddae554f', '2017-04-06 11:25:05', 12, '180.253.154.181'),
	(246, 'e44c5faa97e0dcbc86c5c507d0f59daa', '2017-04-06 11:25:06', 12, '180.253.154.181'),
	(247, '34b9aa2765983bf7133b68f955c94907', '2017-04-06 11:25:07', 12, '180.253.154.181'),
	(248, '74ea12cb4153a438c0bc7a68b9121367', '2017-04-06 11:25:08', 12, '180.253.154.181'),
	(249, '1dc15ec065dbbbfe859c6b36518ffbf3', '2017-04-06 11:25:09', 12, '180.253.154.181'),
	(250, '903834781d666821c01eea718a57ebc2', '2017-04-06 11:25:10', 12, '180.253.154.181'),
	(251, 'aa6e0f7599bcd32cfd4c1a22952cec4e', '2017-04-06 11:25:11', 12, '180.253.154.181'),
	(252, 'ec2c2c5565ea22779414a4fcfc61facd', '2017-04-06 11:25:12', 12, '180.253.154.181'),
	(253, 'ec2c2c5565ea22779414a4fcfc61facd', '2017-04-06 11:25:12', 12, '180.253.154.181'),
	(254, '3595caf7dbd1a3bc178718d6272955d8', '2017-04-06 11:25:13', 12, '180.253.154.181'),
	(255, 'd74bdd6ee81a7e8cf5df14e54db747d0', '2017-04-06 11:25:14', 12, '180.253.154.181'),
	(256, 'd74bdd6ee81a7e8cf5df14e54db747d0', '2017-04-06 11:25:14', 12, '180.253.154.181'),
	(257, 'ad1d0d7f235c94b52ce1d4ad1c08bcc6', '2017-04-06 11:25:15', 12, '180.253.154.181'),
	(258, 'd62d25e3bae900bc37d2060dff54f130', '2017-04-06 11:25:16', 12, '180.253.154.181'),
	(259, '8a50de0641d570874b16c16abaf33dcc', '2017-04-06 11:25:17', 12, '180.253.154.181'),
	(260, '4345c2f433ee69c06c47def887e275a0', '2017-04-06 11:25:18', 12, '180.253.154.181'),
	(261, 'de7b8697c9d4cff9bf66868731f3fa70', '2017-04-06 11:25:19', 12, '180.253.154.181'),
	(262, 'de7b8697c9d4cff9bf66868731f3fa70', '2017-04-06 11:25:19', 12, '180.253.154.181'),
	(263, 'fdf3aaf4b9c2d81215206e9472bce8ec', '2017-04-06 11:25:20', 12, '180.253.154.181'),
	(264, '749f9597f9b42c4d5b5df7b6b221321e', '2017-04-06 11:28:19', 2, '180.253.154.181'),
	(265, '1192a918505e941b5e1448e0ad03e231', '2017-04-06 11:56:14', 12, '180.253.154.181'),
	(266, '6dbc197e142a15a591b5cfd4552cac09', '2017-04-06 11:56:15', 12, '180.253.154.181'),
	(267, '85c70b27cf710cf6b3e69857cc89d53a', '2017-04-06 11:56:16', 12, '180.253.154.181'),
	(268, '85c70b27cf710cf6b3e69857cc89d53a', '2017-04-06 11:56:16', 12, '180.253.154.181'),
	(269, '5be33893906eae4f2a38c970c7dcb93c', '2017-04-06 11:56:17', 12, '180.253.154.181'),
	(270, '4ab7eaf5d8f523e551af275f6bfe0a68', '2017-04-06 11:56:18', 12, '180.253.154.181'),
	(271, 'ded6a45d1de42850b66b95d000b22c08', '2017-04-06 11:56:19', 12, '180.253.154.181'),
	(272, 'ded6a45d1de42850b66b95d000b22c08', '2017-04-06 11:56:19', 12, '180.253.154.181'),
	(273, 'a0eea76f95eebfc6cb737dc1269de9b9', '2017-04-06 11:56:20', 12, '180.253.154.181'),
	(274, 'c746eecdfb45cac69eac7102bffb027b', '2017-04-06 11:56:21', 12, '180.253.154.181'),
	(275, 'c346ea8db26a2efe43723a2c0735c8f5', '2017-04-06 11:56:22', 12, '180.253.154.181'),
	(276, 'c346ea8db26a2efe43723a2c0735c8f5', '2017-04-06 11:56:22', 12, '180.253.154.181'),
	(277, '6b62e3ef08c8aa60886d12da0c7acdec', '2017-04-06 11:56:23', 12, '180.253.154.181'),
	(278, '2b298156ae6a924935cf229da92b3bc1', '2017-04-06 11:56:24', 12, '180.253.154.181'),
	(279, '2b298156ae6a924935cf229da92b3bc1', '2017-04-06 11:56:24', 12, '180.253.154.181'),
	(280, '6ba66e021685ce413904e8d094332ec9', '2017-04-06 12:08:07', 7, '180.253.154.181'),
	(281, '3d417c1be37cc68efbaf20202b26eb5e', '2017-04-06 13:18:46', 5, '180.253.154.181'),
	(282, '1bf66287962e757a1773422c968f4772', '2017-04-06 13:38:29', 5, '180.253.154.181'),
	(283, '4180e6a56c2b31af5f36e3c7e1911ca9', '2017-04-06 14:17:06', 4, '180.253.154.181'),
	(284, '2e67e9936f86de581e8db247aea90de7', '2017-04-06 14:45:57', 7, '180.253.154.181'),
	(285, '4025486b0addf5d51e6fba11451b8534', '2017-04-06 14:59:52', 7, '180.253.154.181'),
	(286, '6417758175aa6e453f9e0c3d5a5a98ee', '2017-04-06 17:29:10', 7, '180.253.154.181'),
	(287, '7ff576671d40034f75edd60a535ea817', '2017-04-06 17:32:24', 7, '180.253.154.181'),
	(288, 'd46c74f092576558355216d447f011b2', '2017-04-06 17:37:42', 12, '180.253.154.181'),
	(289, 'bcf743129a194f180240937b5fd2dcf0', '2017-04-06 20:14:53', 5, '112.215.170.178'),
	(290, '1ae94279cbb248e2fe0edcb6170d76d4', '2017-04-07 10:13:31', 7, '36.80.40.48'),
	(291, 'a617fbae50eb3f6e5c49a15855f9a92c', '2017-04-07 15:30:38', 13, '103.213.131.112'),
	(292, 'eecdc6fd4c300b012396921af0e3ba56', '2017-04-07 16:03:47', 14, '103.213.131.112'),
	(293, '116d5b4ba2f545bd15278f324d229b00', '2017-04-07 16:05:34', 15, '103.213.131.112'),
	(294, 'f218aaaf0c4dc453418e5651dfeba9a0', '2017-04-07 17:35:51', 7, '36.80.40.48'),
	(295, 'df5639bf51f1105a4abfcaa6319d0309', '2017-04-08 12:34:44', 16, '36.80.40.48'),
	(296, '681cb1a3f2cb03fd51f8e909d003fb2d', '2017-04-08 13:23:59', 13, '36.80.40.48'),
	(297, '4a8a2014a9c659eecbd2afba2b6d4264', '2017-04-08 16:38:48', 4, '36.80.40.48'),
	(298, 'ac2c2f95d74e8950b2a0c6677ab7295b', '2017-04-10 11:26:12', 2, '36.80.40.48'),
	(299, '15246c1934e59176aaa83f07f6f50d41', '2017-04-10 12:21:46', 5, '36.80.40.48'),
	(300, '9137100c48724af3a6ee904cc00f1d78', '2017-04-10 12:23:39', 2, '36.80.40.48'),
	(301, 'a969a4fdf32366db7d022fc823072fda', '2017-04-10 12:25:28', 17, '36.80.40.48'),
	(302, 'a5653ad3923e7860013994aac816a1dc', '2017-04-11 18:11:05', 9, '36.72.67.28'),
	(303, '1aec9a3c79686f98f9a15f91afb5e3bc', '2017-04-11 18:11:59', 13, '36.72.67.28'),
	(304, 'd1589e74b422d4396e79d7e69de886d9', '2017-04-11 20:55:49', 18, '118.137.45.99'),
	(305, 'e720241d2fa63ac1f8dedf1e3561c30c', '2017-04-11 22:51:13', 1, '36.72.67.28'),
	(306, 'e5f5d7658c735b2af3b92479d7d5e2c7', '2017-04-12 12:02:41', 2, '36.72.67.28'),
	(307, '694c5e23d8e98fbdca96f7dabe1b00f2', '2017-04-12 14:37:36', 9, '36.72.67.28'),
	(308, '3e8a1687cf78a2f5caf8acd55dd40e9c', '2017-04-12 15:43:40', 19, '36.72.67.28'),
	(309, '5f6303acc179226f09fbf6962453e6dc', '2017-04-12 15:43:48', 19, '36.72.67.28'),
	(310, 'a818cb85af60cd19721fcb09b2a0fe78', '2017-04-12 15:45:44', 19, '36.72.67.28'),
	(311, '99de56e4c1cb4cf79afc979ca425932c', '2017-04-12 15:47:21', 19, '36.72.67.28'),
	(312, '5f3440a1834bbeeaeee7d51551b8fd47', '2017-04-12 15:48:32', 0, '36.72.67.28'),
	(313, 'a7bf8f4aa1f511c94883b597205281b7', '2017-04-12 15:48:41', 0, '36.72.67.28'),
	(314, 'c58727f94819eb8019b988767991429b', '2017-04-12 15:50:32', 0, '36.72.67.28'),
	(315, '505794f0eae945827c011a7a996407eb', '2017-04-12 15:55:23', 12, '36.72.67.28'),
	(316, '9adf9f524a9cc2cddaa20d5eff151964', '2017-04-12 15:59:56', 2, '36.72.67.28'),
	(317, 'fac124f7027c392c96d42d2a6b4eb286', '2017-04-12 16:00:08', 2, '36.72.67.28'),
	(318, 'fe7d5e32b2ffa79061fffcc86fb33a88', '2017-04-12 16:01:57', 9, '36.72.67.28'),
	(319, '87b07a5de0d6822c7b8e38fbb7cb0410', '2017-04-12 16:02:20', 13, '36.72.67.28'),
	(320, '951f7d3ffb5357772b9c3f526350bd66', '2017-04-13 15:49:39', 2, '36.72.67.28'),
	(321, '33201ef6aff752fdcbd63b6a9b49857c', '2017-04-13 16:48:47', 2, '36.72.67.28'),
	(322, 'a140989e268533d167882b1974d87fe4', '2017-04-13 18:47:40', 2, '36.72.67.28'),
	(323, 'b7bba356d905f8e9b43dddfbd57a849f', '2017-04-14 16:42:36', 4, '36.72.67.28'),
	(324, 'ba7254e9c28a98e46fea5fa9f8b21051', '2017-04-14 18:03:47', 9, '36.72.67.28'),
	(325, 'e7990c58fc90a88066f90d3eea60d1fc', '2017-04-14 18:04:06', 13, '36.72.67.28'),
	(326, '9ad3da3d1b7a722979db4695aa8289d2', '2017-04-17 11:05:16', 4, '36.79.251.183'),
	(327, 'ac0ff61ce3fb61cb7ea840233253578b', '2017-04-17 17:24:31', 9, '36.79.251.183'),
	(328, 'f2a6d655e9fada7391e90e7586d76bf3', '2017-04-18 16:52:14', 20, '36.79.251.183'),
	(329, '2f441fe68e9b5653643cd7c34b91c0f5', '2017-04-19 18:01:41', 21, '36.79.251.183'),
	(330, '2c2631e9fb8a5e54e202a91dd8d2bd1e', '2017-04-25 12:45:28', 22, '36.72.146.215'),
	(331, '1515f1e9a816fdd21e78a4da85715abd', '2017-04-27 12:00:32', 22, '36.72.104.243'),
	(332, 'b70b05168d3b8ab825c59d23bd8271d9', '2017-04-27 13:11:42', 20, '36.72.104.243'),
	(333, '9862ea1f6fc8c78f1dc2054edf7bf90c', '2017-04-27 13:21:23', 17, '36.72.104.243'),
	(334, '8da540d7fc30afaf064d5b5f5eb9c569', '2017-04-27 13:44:46', 4, '36.72.104.243'),
	(335, '8c892aaeba8eb6780273f730b9e201ed', '2017-05-04 15:04:49', 23, '36.72.154.62'),
	(336, '067f5d301ab0ae29b7bfdba700847646', '2017-05-09 11:49:12', 49, '36.72.124.191'),
	(337, '1da3b39a988f56506b11b40f1544967e', '2017-05-11 12:29:10', 48, '36.72.124.191'),
	(338, '2ee9be550247b8fdf2d2341d50b11971', '2017-05-11 12:38:23', 48, '36.72.124.191'),
	(339, 'e8497249c7076739c2e9e82bd4bedb65', '2017-05-11 16:10:30', 49, '36.78.208.170'),
	(340, '71df7a589bfa65b670329e5520affa44', '2017-05-11 17:46:49', 53, '36.78.208.170'),
	(341, 'd5b9cc18b9a40bf4b0193a8e02e8d6c6', '2017-05-12 12:38:14', 55, '36.78.208.170'),
	(342, '3ec0caeac43d1137f5a86125043c4489', '2017-05-12 15:38:42', 55, '36.78.208.170'),
	(343, 'a0d644f6fec87eadae9eba47923b9e47', '2017-05-12 17:28:06', 55, '36.78.208.170'),
	(344, 'be57fdacfd06a194ab74063d3b210999', '2017-05-12 17:34:59', 56, '36.78.208.170'),
	(345, '05bfe58f9539ba8e237b5bcc801c2c38', '2017-05-12 18:35:44', 55, '36.78.208.170'),
	(346, 'f9cd451f309a7f06bf3e8bd68e23f577', '2017-05-16 19:42:59', 48, '36.80.93.130'),
	(347, 'bff743eae8f300864ccb73bd71f3162b', '2017-05-17 10:19:30', 48, '36.80.93.130'),
	(348, '23e7aae85f04d8701e708e77017eb7f3', '2017-05-23 17:11:38', 62, '36.79.250.240'),
	(349, 'f3c7143241e2aa96c1f90fe6e1a5bd91', '2017-05-23 18:04:40', 62, '36.79.250.240'),
	(350, '9a018f0b35d87e8d6492db97a99b3f4f', '2017-05-30 14:45:24', 63, '103.213.131.28');
/*!40000 ALTER TABLE `nb_donatur_tokens` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_faq_child
CREATE TABLE IF NOT EXISTS `nb_faq_child` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `description` text,
  `parent` int(11) DEFAULT NULL,
  `key_search` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_faq_child: ~7 rows (approximately)
/*!40000 ALTER TABLE `nb_faq_child` DISABLE KEYS */;
REPLACE INTO `nb_faq_child` (`id`, `title`, `description`, `parent`, `key_search`) VALUES
	(1, 'Contoh galang dananya seperti apa?', 'Mulai dari galang dana untuk bantuan biaya pengobatan, memberikan beasiswa bagi yang membutuhkan, program yayasan/panti asuhan, mendirikan rumah ibadah/sekolah/infrastruktur umum, santunan bencana alam hingga galang dana antar alumni/komunitas, dll.', 1, 'Contoh galang dananya seperti apa?'),
	(2, 'Apakah YDSF legal dan mendapat izin dari pemerintah untuk melakukan penggalangan dana? ', 'YDSF memiliki badan hukum Yayasan untuk pengelolaan dana publik yang melakukan penggalangan dana dan berdonasi di situs YDSF.com.\r\n\r\nTerkait regulasi pemerintah, UU No.9/1961 tentang Pengumpulan Uang dan Barang tidak memberikan panduan spesifik mengenai donasi online, situs crowdfunding.\r\n\r\nKami menjaga hubungan baik dengan Kementrian Sosial Republik Indonesia terkait operasi dan pengembangan situs YDSF.', 1, 'Apakah YDSF legal dan mendapat izin dari pemerintah untuk melakukan penggalangan dana? '),
	(3, 'Apakah YDSF melakukan verifikasi terhadap proposal galang dana yang masuk? ', '\r\nSetiap penggalang dana (campaigner) wajib melampirkan ID KTP, foto diri terbaru, akun social media, dan jika diperlukan akan diwawancarai melalui telfon untuk melewati proses verifikasi.\r\n\r\nJika tidak terverifikasi, maka user tersebut tidak dapat mencairkan donasi yang terkumpul.\r\n\r\nKitabisa juga mewajibkan campaigner membuat update & laporan penggunaan dana melalui halaman campaign Kitabisa.com.\r\n\r\n\r\nSebagai open platform kami tidak bisa sepenuhnya memastikan keaslian dan hasil dari sebuah campaign penggalangan dana.\r\n\r\n\r\nUntuk itu kami menyarankan donatur untuk berdonasi ke campaign yang dikenal/dipercaya. Campaign yang terpercaya umumnya sudah mendapatkan donatur awal (tidak 0%), memberikan keterangan lengkap beserta kontak, dan aktif menulis update/laporan.\r\n', 1, 'Apakah YDSF melakukan verifikasi terhadap proposal galang dana yang masuk? '),
	(4, 'Bagaimana cara menambah saldo dompet kebaikan? ', ' Login ke akun Anda\r\n Klik menu Dompet Kebaikan\r\n Klik tombol Tambah deposit di menu Dompet Kebaikan\r\n Ikuti instruksi yang diminta. Di akhir proses, Anda akan diberikan no. rekening bank dan kode unik.\r\n Transfer tepat sesuai dengan tagihan\r\n Seperti halnya berdonasi, jangan lupa mencantumkan kode unik di transfer Anda.\r\n Silakan kirimkan bukti transfer ke support@kitabisa.com.\r\n Tim Kitabisa akan memverifikasikan top up Anda (proses verifikasi top up dilakukan secara manual).\r\n', 4, 'Bagaimana cara menambah saldo dompet kebaikan? '),
	(5, 'Apa itu dompet kebaikan? ', 'Dompet kebaikan diciptakan untuk memudahkan Anda berdonasi tanpa harus melakukan transfer/mobile/sms banking setiap kali Anda melakukan donasi (transfer sekali, donasi berkali-kali hingga saldo habis).\r\n\r\n\r\nAnda dapat mengisi saldo dompet kebaikan melalui menu tambah saldo.\r\n\r\nDompet kebaikan juga ditujukan untuk mencairkan donasi yang terkumpul saat Anda menggalang dana di Kitabisa\r\n', 4, 'Apa itu dompet kebaikan?'),
	(6, 'Bagaimana cara mencairkan donasi yang sudah terkumpul? ', 'Berikut cara untuk melakukan pencairan:\r\n1. Silakan login ke akun Anda\r\n2. Klik menu Dompet Kebaikan\r\n3. Klik menu Pencairan\r\n4. Silakan mengisi form pencairan dengan identitas bank tujuan transfer (dapat Anda isi dengan akun bank target campaign secara langsung)\r\n5. Silakan menginput 6 digit kode aktivasi pencairan yang dikirimkan melalui email dan sms\r\n\r\nJika Anda tidak mendapatkan kode verifikasi, silakan klik tab bertuliskan batalkan, kemudian sistem kami akan mengirimkan ulang kode verifikasi pencairan ke email dan no handphone Anda.\r\n\r\nPencairan hanya akan dilakukan pada hari kerja.\r\nApabila akun bank tujuan pencairan di luar BCA/BNI/BRI/Mandiri, maka dana baru akan masuk ke rekening Anda maksimal 3 hari setelah pencairan.\r\nKeterlambatan tersebut disebabkan oleh kebijakan waktu transfer antarbank.\r\n', 2, 'Bagaimana cara mencairkan donasi yang sudah terkumpul? '),
	(8, 'Instant Payment ', 'Instant Payment merupakan sistem yang didisain untuk campaigner, sehingga perolehan donasi dapat masuk secara otomatis ke fitur dompet kebaikan.\r\n\r\nDengan adanya sistem tersebut, perolehan donasi campaign akan masuk secara otomatis ke fitur dompet kebaikan campaigner dalam kurun waktu 3x24 jam, berapapun nominal donasi yang diperoleh.\r\n\r\nCampaigner dapat melakukan pencairan kapanpun, baik ketika campaign masih berjalan, maupun ketika masa campaign telah berakhir.\r\n', 2, 'Instant Payment ');
/*!40000 ALTER TABLE `nb_faq_child` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_faq_parent
CREATE TABLE IF NOT EXISTS `nb_faq_parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `display_first` enum('0','1') NOT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_faq_parent: ~4 rows (approximately)
/*!40000 ALTER TABLE `nb_faq_parent` DISABLE KEYS */;
REPLACE INTO `nb_faq_parent` (`id`, `title`, `display_first`, `crtd_at`, `edtd_at`, `last_edit`, `author`) VALUES
	(1, 'Panduan Umum ', '0', '2017-04-13', '2017-04-13', NULL, 'Adminsitrator'),
	(2, 'Pencairan Dana', '1', '2017-04-13', '2017-04-13', NULL, 'Adminsitrator'),
	(3, 'Untuk Donatur ', '0', '2017-04-13', '2017-04-13', NULL, 'Adminsitrator'),
	(4, 'Dompet Kebaikan', '0', '2017-04-13', '2017-04-13', NULL, 'Adminsitrator');
/*!40000 ALTER TABLE `nb_faq_parent` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_footer_link
CREATE TABLE IF NOT EXISTS `nb_footer_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `type` enum('static','other') DEFAULT NULL,
  `ctg` enum('ttgkm','prgm') DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '0',
  `crtd_at` varchar(255) DEFAULT NULL,
  `edtd_at` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `last_edit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_footer_link: ~10 rows (approximately)
/*!40000 ALTER TABLE `nb_footer_link` DISABLE KEYS */;
REPLACE INTO `nb_footer_link` (`id`, `name`, `link`, `type`, `ctg`, `is_active`, `crtd_at`, `edtd_at`, `author`, `last_edit`) VALUES
	(1, 'Profil', 'page/profilydsf', 'static', 'ttgkm', '1', '2017-04-12 02:46:53', '2017-04-13 17:15:34', 'Adminsitrator', 'Adminsitrator'),
	(3, 'Donasi', 'donasi', 'other', 'prgm', '1', '2017-04-12 02:55:15', '2017-04-17 16:21:41', 'Adminsitrator', 'Luthfi Aziz'),
	(4, 'Visi Misi', 'page/visimisi', 'static', 'ttgkm', '1', '2017-04-12 18:50:14', '2017-04-12 18:50:14', 'Adminsitrator', NULL),
	(5, 'Sekilas YDSF', 'page/sekilasydsf', 'static', 'ttgkm', '1', '2017-04-12 19:06:37', '2017-04-12 19:06:37', 'Adminsitrator', NULL),
	(6, 'Legalitas', 'page/legalitas', 'static', 'ttgkm', '1', '2017-04-12 19:07:02', '2017-04-12 19:07:02', 'Adminsitrator', NULL),
	(7, 'Susunan Pengurus', 'page/pengurus', 'static', 'ttgkm', '1', '2017-04-12 19:07:29', '2017-04-12 19:07:29', 'Adminsitrator', NULL),
	(8, 'Program', 'program', 'other', 'prgm', '1', '2017-04-12 19:08:07', '2017-04-12 19:08:30', 'Adminsitrator', 'Adminsitrator'),
	(9, 'FAQ', 'faq', 'other', 'prgm', '1', '2017-04-12 19:08:52', '2017-04-12 19:08:52', 'Adminsitrator', NULL),
	(10, 'Syarat dan Ketentuan', 'page/syrtdankttn', 'static', 'prgm', '1', '2017-04-12 19:09:16', '2017-04-12 19:13:55', 'Adminsitrator', 'Adminsitrator'),
	(11, 'Kebijakan Privasi', 'page/kebijakan', 'static', 'prgm', '1', '2017-04-12 19:11:49', '2017-04-12 19:11:49', 'Adminsitrator', NULL);
/*!40000 ALTER TABLE `nb_footer_link` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_inc
CREATE TABLE IF NOT EXISTS `nb_inc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inc` bigint(20) NOT NULL DEFAULT '1',
  `ket` varchar(255) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_inc: ~0 rows (approximately)
/*!40000 ALTER TABLE `nb_inc` DISABLE KEYS */;
REPLACE INTO `nb_inc` (`id`, `inc`, `ket`, `tgl`) VALUES
	(1, 2, 'increment idtransaksi', '2017-05-30');
/*!40000 ALTER TABLE `nb_inc` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_incuniq
CREATE TABLE IF NOT EXISTS `nb_incuniq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minlimit` decimal(20,0) DEFAULT NULL COMMENT 'minimal limit nominal',
  `maxlimit` decimal(20,0) DEFAULT NULL COMMENT 'max nominal limit',
  `uniqinc` int(11) DEFAULT NULL COMMENT 'nilai unique saat ini',
  `minuniq` int(11) DEFAULT NULL COMMENT 'min unique inc',
  `maxuniq` varchar(255) DEFAULT NULL COMMENT 'max unique inc ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_incuniq: ~5 rows (approximately)
/*!40000 ALTER TABLE `nb_incuniq` DISABLE KEYS */;
REPLACE INTO `nb_incuniq` (`id`, `minlimit`, `maxlimit`, `uniqinc`, `minuniq`, `maxuniq`) VALUES
	(1, 0, 1000000, 641, 501, '999'),
	(2, 1000001, 5000000, 1017, 1001, '1999'),
	(3, 5000001, 10000000, 2003, 2001, '4999'),
	(4, 10000001, 50000000, 5001, 5001, '5001'),
	(5, 50000001, 999999999, 10011, 10001, '99999');
/*!40000 ALTER TABLE `nb_incuniq` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_mutasi_rekening
CREATE TABLE IF NOT EXISTS `nb_mutasi_rekening` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank` varchar(20) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `type` varchar(10) NOT NULL,
  `total` int(11) NOT NULL,
  `balanceposition` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `checkdate` date NOT NULL,
  `checkdatetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_mutasi_rekening: ~0 rows (approximately)
/*!40000 ALTER TABLE `nb_mutasi_rekening` DISABLE KEYS */;
/*!40000 ALTER TABLE `nb_mutasi_rekening` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_post
CREATE TABLE IF NOT EXISTS `nb_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `img` text,
  `desc` text,
  `desc_short` varchar(255) DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL COMMENT 'news, videos, articles',
  `link_terkait` text,
  `slug` varchar(150) NOT NULL,
  `coment_status` enum('open','closed') DEFAULT NULL,
  `sum_coment` int(11) DEFAULT NULL,
  `sum_read` int(11) DEFAULT '0',
  `sum_share` int(11) DEFAULT '0',
  `is_active` enum('0','1') DEFAULT '1' COMMENT '0:false; 1:true',
  `meta_title` varchar(70) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL COMMENT 'last author',
  `categories` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='tabel artikel';

-- Dumping data for table u7043207_berzakat.nb_post: ~13 rows (approximately)
/*!40000 ALTER TABLE `nb_post` DISABLE KEYS */;
REPLACE INTO `nb_post` (`id`, `title`, `img`, `desc`, `desc_short`, `post_date`, `type`, `link_terkait`, `slug`, `coment_status`, `sum_coment`, `sum_read`, `sum_share`, `is_active`, `meta_title`, `meta_description`, `meta_keyword`, `crtd_at`, `edtd_at`, `author`, `last_edit`, `categories`) VALUES
	(1, 'UMMAT ISLAM, UMAT YANG TERBAIK', 'image_20170314105214.jpg', '<p>UMMAT ISLAM, UMMAT YANG TERBAIK<br />\r\n*Ustadz DR. Syafiq Riza Basalamah. MA.*<br />\r\n<br />\r\nSeorang muslim haruslah senantiasa bersyukur karena tidak ada suatu nikmat kecuali itu murni dari Allah Ta&#39;ala.<br />\r\nAda rukun bagi orang yang bersyukur :</p>\r\n\r\n<p><br />\r\n1. <strong>*Dengan hati* (menyakini bahwa nikmat itu datang hanyalah dari Allah).</strong><br />\r\n2. <strong>*Dengan lisan* (mengucapkan kalimat tanda bersyukur).</strong><br />\r\n3.<strong> *Dengan amal perbuatan*</strong></p>\r\n\r\n<p>Diantara dari sekian banyak keistimewaan ummat Islam :</p>\r\n\r\n<p><br />\r\n1. *Adalah ummat terbaik*<br />\r\nAllah Ta&rsquo;ala berfirman<br />\r\n,كُنْتُمْ خَيْرَ أُمَّةٍ أُخْرِجَتْ لِلنَّاسِ تَأْمُرُونَ بِالْمَعْرُوفِ وَتَنْهَوْنَ عَنِ الْمُنْكَرِ وَتُؤْمِنُونَ بِاللَّهِ&ldquo;<br />\r\n_&quot;Kamu adalah umat yang terbaik yang dilahirkan untuk manusia, menyuruh kepada yang ma&rsquo;ruf, dan mencegah dari yang munkar, dan beriman kepada Allah.&rdquo;_ (QS. Ali Imron: 110)</p>\r\n\r\n<p><br />\r\n2. *Ummat Islam tidak mungkin besatu dan bersepakat dalam kesesatan.*<br />\r\n(Kenapa ummat Yahudi bisa rusak? 》Karena mereka bersepakat merubah agamanya)<br />\r\n●Sekalipun ada ummat Yahudi dan Nasrani yang masih berpegang dengan tauhid tentunya sudah masuk Islam.</p>\r\n\r\n<p><br />\r\n3. *Ummat Islam adalah ummat pertama yang masuk surga.*<br />\r\nRasulullah ﷺ bersabda _&quot;Kita adalah ummat yang terakhir, tapi kita adalah ummat pertama yang akan masuk surga&quot;_.</p>\r\n\r\n<p><br />\r\n4. *Ummat Islam adalah ummat yang _washatan_ (pertengahan).* (ummat yang tidak berlebihan dan tidak meremehkan terhadap sesuatu)</p>\r\n\r\n<p><br />\r\n5. *Ada 70.000 ummat Islam yang masuk surga tanpa hisab dan tanpa adzab.*<br />\r\nRasulullah ﷺ bersabda, _&ldquo;Mereka adalah orang-orang yang tidak meruqyah, tidak meminta untuk diruqyah, tidak melakukan thiyaroh (beranggapan sial) dan hanya kepada Allah mereka bertawakal.&rdquo;_ [HR. Bukhari]</p>\r\n\r\n<p><br />\r\n6. *Salah satu keutamaan ummat Islam adalah 》Sholat Isya&#39;.*<br />\r\n(Sholat isya&#39; hanya dimiliki ummat Islam.. Ummat terdahulu hanya sholat sampai maghrib).<br />\r\nAda orang-orang yang Nabi ﷺ *Berlepas Diri* dari mereka.</p>\r\n\r\n<p>*Rasulullah ﷺ bersabda&nbsp; :*<br />\r\n<strong>&gt; &quot;Bukan termasuk golongan kami, orang yang tidak membaca Al-Qur&#39;an dengan tajwid (membaguskan bacaannya).&quot;_</strong><br />\r\n<strong>[HR. Abu Daud]</strong><br />\r\n<strong>&gt; &quot;Bukan termasuk golongan kami, orang yang merusak hubungan suami dan istri.&quot;_</strong><br />\r\n<strong>[HR. Abu Daud]</strong><br />\r\n<strong>&gt; &quot;Bukan termasuk golongan kami, orang yang menipu.&quot;_</strong><br />\r\n<strong>[HR. Muslim]</strong><br />\r\n<strong>&gt; &quot;Bukan termasuk golongan kami, orang yang mendapat musibah dia memukul pipinya, merobek bajunya (meratapi kesedihan)&quot;._</strong><br />\r\n<strong>[HR. Bukhari dan Muslim]</strong><br />\r\n<strong>&gt; &quot;Bukan termasuk golongan kami, orang laki-laki yang menyerupai wanita, dan wanita yang menyerupai laki-laki.&quot;_</strong><br />\r\n<strong>[HR. Ahmad]</strong><br />\r\n<strong>&gt; &quot;Bukan termasuk golongan kami, orang yang menyerupai ummat diantara kita. Kalian jangan menyerupai Yahudi dan Nasrani.&quot;_ [HR. Tirmidzi]</strong><br />\r\n<strong>&gt; &quot;Bukan termasuk golongan kami, orang yang mengamalkan sunnah ummat lain (amalan orang kafir).&quot;_[Shohihul Jami&#39;]</strong><br />\r\n<strong>&gt; &quot;Bukan termasuk golongan kami, orang yang percaya dengan nasib sial, mengundi nasib, orang yang meramal dan yang mendatangi tukang ramal, penyihir dan yang mendatangi tukang sihir.&quot;_</strong><br />\r\n<strong>&gt; &quot;Bukan termasuk golongan kami, orang yang bersumpah demi amanah&quot;._ [HR. Ahmad]</strong><br />\r\n<strong>● Rasulullah ﷺ bersabda _&quot;barangsiapa bersumpah dengan nama selain Allah maka dia telah syirik&quot;._</strong><br />\r\n<strong>&gt; &quot;Bukan termasuk golongan kami, orang uang tidak menyayangi yang lebih kecil dan tidak menghormati yang lebih tua.&quot;_</strong><br />\r\n<strong>&gt; &quot;Siapa yang benci dari sunnahku maka bukan dari golonganku&quot;._</strong><br />\r\n<strong>&gt; &quot;Barangsiapa yang memberontak kepada ummatku (ulil amri) dengan membawa pedang, dan tidak peduli dengan yang baik dan yang buruk, serta membunug oran kafir dalam perjanjian maka dia bukan golonganku._ [HR. Muslim]</strong><br />\r\n<strong>&gt; &quot;Kau jangan tinggalkan sholat lima waktu dengan sengaja. Barangsiapa meninggalkan sholat wajib dengan sengaja makan telah terlepas dia dari perjanjiannya._</strong></p>\r\n\r\n<p><br />\r\n<strong>●Pembeda antara seorang muslim dengan kafir adalah sholat.</strong></p>\r\n', 'tes', '2017-04-24', 'articles', '', 'ummat-islam-umat-yang-terbaik', NULL, NULL, 0, 0, '1', 'Tes', '', 'tes,tes doang,coba', '2017-01-25', '2017-04-18', 'Luthfi Aziz', 'Endraaa', 1),
	(2, '12 Tips Menjadi Keluarga Sakinah', 'image_20170314104124.jpg', '<div>12 Kiat Menjadi Keluarga Sakinah</div>\r\n\r\n<div>By : Ustadz DR. Syafiq Riza Basalamah. MA</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>1. Perbaiki diri untuk orangtua dan perbanyak taubat</div>\r\n\r\n<div>Cari pasangan hidup yang baik sehingga mendapatkan keturunan yang baik. Selain itu perbanyaklah taubat karena dengan taubat bisa menjadikan orang tersebut lebih baik dari orang yang belum melakukan dosa semisalnya :</div>\r\n\r\n<div>لَقَدْ تَابَتْ تَوْبَةً لَوْ قُسِمَتْ بَيْنَ سَبْعِينَ مِنْ أَهْلِ الْمَدِينَةِ لَوَسِعَتْهُمْ وَهَلْ وَجَدْتَ تَوْبَةً أَفْضَلَ مِنْ أَنْ جَادَتْ بِنَفْسِهَا لِلَّهِ تَعَالَى</div>\r\n\r\n<div>&ldquo;Wanita ini telah bertaubat dengan taubat yang seandainya taubatnya tersebut dibagi kepada 70 orang dari penduduk Madinah maka itu bisa mencukupi mereka. Apakah engkau dapati taubat yang lebih baik dari seseorang mengorbankan jiwanya karena Allah Ta&rsquo;ala?&rdquo; (HR. Muslim no. 1696).</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>2. Makan Yang Halal</div>\r\n\r\n<div>كُلُّ جِسْمٍ نَبَتَ مَنْ سُحْتٍ فَالنَّارُ أَوْلَى بِهِ</div>\r\n\r\n<div>Setiap jasad yang tumbuh dari harta haram, maka neraka layak untuknya<br />\r\n(Majmu&rsquo; al-Fatawa, 21:541).</div>\r\n\r\n<div>All&acirc;h juga berfirman :<br />\r\nيَا أَيُّهَا الَّذِينَ آمَنُوا كُلُوا مِنْ طَيِّبَاتِ مَا رَزَقْنَاكُمْ<br />\r\n&ldquo;Wahai orang-orang yang beriman, makanlah makanan yang baik dari rezeki yang Kami berikan kepada kalian&rdquo;[al-Baqarah/2:172].</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>3. Banyak untuk punya anak yang sholih dan berdoa setelah mempunyai anak<br />\r\nNabi Ibrahim &lsquo;alaihis salaam berkata,</div>\r\n\r\n<div><br />\r\nرَبِّ هَبْ لِي مِنَ الصَّالِحِينَ<br />\r\n&ldquo;Robbi hablii minash shoolihiin&rdquo; [Ya Rabbku, anugrahkanlah kepadaku (seorang anak) yang termasuk orang-orang yang saleh]&rdquo;. (QS. Ash Shaffaat: 100).</div>\r\n\r\n<div>رَبِّ اجْعَلْنِي مُقِيمَ الصَّلَاةِ وَمِنْ ذُرِّيَّتِي ۚ رَبَّنَا وَتَقَبَّلْ دُعَاءِ<br />\r\nYa Tuhanku, jadikanlah aku dan anak cucuku orang-orang yang tetap mendirikan shalat, ya Tuhan kami, perkenankanlah doaku.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>رَبَّنَا هَبْ لَنَا مِنْ أَزْوَاجِنَا وَذُرِّيَّاتِنَا قُرَّةَ أَعْيُنٍ وَاجْعَلْنَا لِلْمُتَّقِينَ إِمَامًا<br />\r\n&ldquo;Robbanaa hab lanaa min azwajinaa wa dzurriyatinaa qurrota a&rsquo;yun waj&rsquo;alnaa lil muttaqiina imaamaa&rdquo; [Ya Rabb kami, anugerahkanlah kepada kami, isteri-isteri kami dan keturunan kami sebagai penyenang hati (kami), dan jadikanlah kami imam bagi orang-orang yang bertakwa]. (QS. Al Furqon: 74)</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>4. Mengenal Allah<br />\r\nDisini peran orangtua dituntut untuk berilmu sehingga bisa mengajarkan kepada anak-anaknya.<br />\r\nإِنَّمَا يَخْشَى اللَّهَ مِنْ عِبَادِهِ الْعُلَمَاءُ<br />\r\n&ldquo;Sesungguhnya yang takut kepada Allah di antara hamba-hamba-Nya, hanyalah ulama&rdquo; (QS. Fathir: 28).</div>\r\n\r\n<div>Cara mengenal Allah &lsquo;Azza Wa Jalla dengan cara :</div>\r\n\r\n<div><br />\r\n1. Menghafal<br />\r\n2. Memahami<br />\r\n3. Berdoa dengan nama-namaNya<br />\r\n4. Beribadah dengan kandungan nama-namaNya.</div>\r\n\r\n<div>5. Memberikan suri tauladan kepada istri dan anak-anaknya.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>&hellip;&hellip; وَكَانَ أَبُوهُمَا صَالِحًا &hellip;..<br />\r\nKhidir dan Nabi Musa &lsquo;Alaihissalam memperbaiki dinding anak yatim dan Allah menjaga hartanya dikarenakan orangtua mereka adalah orangtua yang shalih.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>6. Memberikan Nasihat kepada Anaknya</div>\r\n\r\n<div>Memberikan nasihat tidak harus pada saat resmi, karena suasana pada saat santai menjadikan hati lebih terbuka.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>7. Menyuruh Keluarganya Sholat</div>\r\n\r\n<div>Allah berfirman mengenai Ismail alaihissalam<br />\r\nوَاذْكُرْ فِي الْكِتَابِ إِسْمَاعِيلَ ۚإِنَّهُ كَانَ صَادِقَ الْوَعْدِ وَكَانَ رَسُولًا نَّبِيًّا وَكَانَ يَأْمُرُ أَهْلَهُ بِالصَّلَاةِ وَالزَّكَاةِ وَكَانَ عِندَ رَبِّهِ مَرْضِيًّا<br />\r\n&ldquo;Dan ceritakanlah (hai Muhammad kepada mereka) kisah Ismail (yang tersebut) di dalam Al Quran. Sesungguhnya ia adalah seorang yang benar janjinya, dan dia adalah seorang rasul dan nabi. Dan ia menyuruh keluarganya untuk shalat dan menunaikan zakat, dan ia adalah seorang yang diridhai di sisi Tuhannya&rdquo; (Maryam: 54, 55)</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>8. Berkata-kata atau berucap dengan ucapan yang baik</div>\r\n\r\n<div>Rasulullah shallallahu&rsquo;alaihiwasallam bersabda,<br />\r\nلَيْسَ الْمُؤْمِنُ بِالطَّعَّانِ وَلَا اللَّعَّانِ وَلَا الْفَاحِشِ وَلَا الْبَذِيءِ<br />\r\n&ldquo;Seorang mukmin bukanlah orang yang banyak mencela, bukan orang yang banyak melaknat, bukan pula orang yang keji (buruk akhlaqnya), dan bukan orang yang jorok omongannya&rdquo; (HR. Tirmidzi, no. 1977; Ahmad, no. 3839 dan lain-lain)</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>9. Jadikan rumah kita adalah Rumah Qur&rsquo;an</div>\r\n\r\n<div>Nabi shallallahu &lsquo;alaihi wa sallam bersabda,<br />\r\nلاَ تَجْعَلُوا بُيُوتَكُمْ مَقَابِرَ إِنَّ الشَّيْطَانَ يَنْفِرُ مِنَ الْبَيْتِ الَّذِى تُقْرَأُ فِيهِ سُورَةُ الْبَقَرَةِ<br />\r\n&ldquo;Janganalah jadYayasan Nidaul Fithrah:<br />\r\nikan rumah kalian seperti kuburan karena setan itu lari dari rumah yang didalamnya dibacakan surat Al Baqarah.&rdquo; (HR. Muslim no. 1860)</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>10. Pendidikan Akhlaq yang baik<br />\r\n- Akhlak bisa baik dan buruk sedangkan adab selalu baik.<br />\r\n- Akhlak bisa bawaan lahir dan bisa dipelajari. Sedangkan adab biasanya dipelajari</div>\r\n\r\n<div>Puncak dari akhlak<br />\r\n1. Keberanian<br />\r\n2. Berbagi<br />\r\n3. Sabar<br />\r\n4. Adil</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>11. Menjaga silahturahmi<br />\r\nAjak anak-anak untuk mengunjungi keluarga sehingga menumbuhkan rasa cinta sesama saudara.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>12. Memberikan perhatian khusus kepada anak perempuan</div>\r\n\r\n<div>Surat An Nahl 58-59<br />\r\nوَإِذَا بُشِّرَ أَحَدُهُمْ بِالْأُنْثَىٰ ظَلَّ وَجْهُهُ مُسْوَدًّا وَهُوَ كَظِيمٌ<br />\r\n58. Dan apabila seseorang dari mereka diberi kabar dengan (kelahiran) anak perempuan, hitamlah (merah padamlah) mukanya, dan dia sangat marah.</div>\r\n\r\n<div>يَتَوَارَىٰ مِنَ الْقَوْمِ مِنْ سُوءِ مَا بُشِّرَ بِهِ ۚ أَيُمْسِكُهُ عَلَىٰ هُونٍ أَمْ يَدُسُّهُ فِي التُّرَابِ ۗ أَلَا سَاءَ مَا يَحْكُمُونَ<br />\r\n59. Ia menyembunyikan dirinya dari orang banyak, disebabkan buruknya berita yang disampaikan kepadanya. Apakah dia akan memeliharanya dengan menanggung kehinaan ataukah akan menguburkannya ke dalam tanah (hidup-hidup)?. Ketahuilah, alangkah buruknya apa yang mereka tetapkan itu.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>Dari Uqbah bin Amir radhiyallahu &lsquo;anhu, bahwa Rasulullah shallallahu &lsquo;alaihi wa sallam bersabda,<br />\r\nمَنْ كَانَ لَهُ ثَلَاثُ بَنَاتٍ فَصَبَرَ عَلَيْهِنَّ، وَأَطْعَمَهُنَّ، وَسَقَاهُنَّ، وَكَسَاهُنَّ مِنْ جِدَتِهِ كُنَّ لَهُ حِجَابًا مِنَ النَّارِ يَوْمَ الْقِيَامَةِ<br />\r\nSiapa yang memiliki 3 anak perempuan, lalu dia bersabar, memberinya makan, minum, dan pakaian dari hasil usahanya, maka semuanya akan menjadi tameng dari neraka pada hari kiamat.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>Tentu keluarga yang sakinah adalah dambaan setiap mereka yang sudah atau akan menikah. Hanya keluarga yang menjadikan Allah sebagai &quot;poros&quot; dalam rumah tangganya yang Insyaa Allah akan Dia berikan anugrah &quot;Keluarga Sakinah&quot;.</div>\r\n', '12 Tips Menjadi Keluarga Sakinah', '2017-04-23', 'articles', 'tes', '12-tips-menjadi-keluarga-sakinah', NULL, NULL, 0, 0, '1', 'tes aja', 'teag aaj', 'tesa,hhh,cobain aja', '2017-02-01', '2017-04-18', 'Luthfi Aziz', 'Endraaa', 1),
	(3, 'Krian Bangkit Bersama YDSF', 'image_20170314110633.jpg', '<div class="row">\r\n<div class="col-sm-12">\r\n<div class="mt-30">\r\n<h4>Krian Bangkit Bersama YDSF</h4>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="event-details">\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:justify"><span style="font-family:\'Times New Roman\',\'serif\'; font-size:12pt">Recovery pasca bencana puting beliung krian</span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:\'Times New Roman\',\'serif\'; font-size:12pt">Krian, 8/03 Bencana angin puting beliung 15 Februari lalu yang melanda 3 desa / RT di wilayah Krian menyisakan bangunan-bangunan yang rusak. Diantaranya adalah rumah-rumah, tempat ibadah dan bangunan sekolah. YDSF memberikan bantuan material pada bangunan mushollah Sabilun Najah di Dusun Kanigoro RT 10 RW 03 Desa Keboharan Krian yang rusaknya parah. Nilai bantuan yang diberikan adalah Rp. 15 juta. Lukman Anshori selaku takmir musholla Sabilun Najah sewaktu kejadian sedang mengajar di TPQ Sabilun Najah.</span></p>\r\n</div>\r\n\r\n<p><img alt="" src="/ydsf/assets/js/kcfinder/upload/images/2-puting-beliun-2017.jpg" style="height:347px; width:521px" /></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:\'Times New Roman\',\'serif\'; font-size:12pt">Tepatnya hari Rabu (15/02) jam 14-14.30 angin sangat kencang dan berputar-putar tiba-tiba muncul, sehingga pohon-pohon tumbang, atap-atap rumah banyak yang melayang, bangunan ada yang roboh. &ldquo;Kondisi musholla saat itu teras depan hilang hanya tersisa tempat wudhu dan keseluruhan bangunannya rusak berat. Karena kerusakannya yang begitu parah maka pihak takmir bermusyawarah dengan warga sekitar memutuskan untuk membongkar total musholla tersebut dengan pertimbangan bisa membahayakan bila hanya diperbaiki saja. Lalu bantuan material dari YDSF segera kami pakai untuk membangun kembali.&rdquo; Jelas Lukman Anshori.</span><br />\r\n<span style="font-family:\'Times New Roman\',\'serif\'; font-size:12pt">Bangunan lain yang mendapat bantuan YDSF Musholla Baitur Rochman Dsn. Kanigoro desa Keboharan Rt 11 Rw 04 mengalami kerusakan atap samping, nilai bantuan material Rp. 5 juta, dan PAUD Al-Husainy, Kembang sore rt 01 rw 02 terung kulon krian, yang separoh atapnya bangunannya hilang mendapat bantuan 10 juta. &ldquo;Semoga bantuan ini menjadi amal jariyah dan menambah keberkahan bagi para donatur YDSF&rdquo; Ungkap Thantowi selaku jungut YDSF Sidoarjo.</span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:\'Times New Roman\',\'serif\'; font-size:12pt"><img alt="" src="/ydsf/assets/js/kcfinder/upload/images/4-puting-beliun-2017.jpg" style="height:427px; width:640px" /></span></p>\r\n', 'Tes', '2017-03-14', 'articles', '', 'krian-bangkit-bersama-ydsf', NULL, NULL, 0, 0, '1', 'Tes', '', 'tes,tes1', '2017-03-14', '2017-04-18', 'Luthfi Aziz', 'Endraaa', 1),
	(4, 'Belajar Persalinan Dari Maryam', 'image_20170314111005.jpg', '<div class="row">\r\n<div class="col-sm-12">\r\n<div class="mt-30">\r\n<h4>Belajar Persalinan Dari Maryam</h4>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class="event-details">\r\n<p>Persalinan Maryam, sebuah seminar kesehatan yang digelar YDSF bersama pembicara Bidan Mugi Rahayu. AMD. KEB. S. FIL, Ahad (26/2), di Hotel Sahid Surabaya. Pembahasan dalam Persalinan Maryam bukan mengenai Maryam yang melahirkan Isa tanpa ayah, namun Persalinan Maryam yang merupakan persalinan fisiologis (normal).</p>\r\n\r\n<p>Maryam adalah salah satu dari empat wanita yang dijamin masuk surga. Wanita terbaik sepanjang masa di dunia maupun akhirat. Maryam sampai diabadikan dalam surat Quran. Bidan Rahayu dalam kesempatan itu menyampaikan, bahwa mempelajari kehamilan dan persalinan, para ibu bisa belajar dari surat Maryam ayat 22-26.</p>\r\n\r\n<p>&ldquo;Yang diperlukan ibu dalam kelahiran adalah besar hati, berprasangka baik, dan jangan fokus pada sakitnya melainkan fokus pada pahala. Karena sakit itu menghilangkan dosa, kontraksi yang dirasakan ibu akan menggugurkan dosa. Lalu masihkah kita mengeluh dengan kehamilan dan kelahiran?&rdquo; begitu yang dikatakan bidan Mugi Rahayu&nbsp; kepada para peserta yang hadir.</p>\r\n\r\n<p><img alt="" src="/ydsf/assets/js/kcfinder/upload/images/800x400-1-persalinan-maryam-1.jpg" style="height:289px; width:578px" /></p>\r\n\r\n<p>Selain tak mengkhawatirkan rasa sakit, bidan asal Yogyakarta tersebut juga menghimbau agar para ibu mengutamakan pertolongan Allah yakni berdoa saat persalinan.</p>\r\n\r\n<p>Hal yang juga dijelaskan dalam seminar adalah mengenai posisi melahirkan yang baik, yakni dengan posisi duduk, posisi dalam menyusui buah hati, yakni si bayi berada di atas ibundanya agar terhindar dari kasus kematian bayi tersedak air susu.</p>\r\n\r\n<p>Hikmah gerakan shalat juga dijelaskan dalam seminar, beberapa di antaranya baik untuk mengatasi sakit punggung di kala hamil, juga memperbaiki janin yang sungsang.</p>\r\n\r\n<p>Dari seminar yang dihadiri ratusan orang dari beragam profesi itu, dapat ditarik tiga hal yang dapat diterapkan, yakni berdoa, berpikir positif, dan ingat pada tujuan utama yaitu Allah. Dengan belajar dari Maryam, Insya Allah anak akan selamat dunia akhirat, dan menjadi salih baik untuk diri sendiri, keluarga, dan lingkungannya.</p>\r\n</div>\r\n', 'Persalinan Maryam, sebuah seminar kesehatan yang digelar YDSF bersama pembicara Bidan Mugi Rahayu. AMD. KEB. S. FIL, Ahad (26/2), di Hotel Sahid Surabaya. Pembahasan dalam Persalinan Maryam bukan mengenai Maryam yang melahirkan Isa tanpa ayah, namun Persa', '2017-03-14', 'articles', '', 'belajar-persalinan-dari-maryam', NULL, NULL, 0, 0, '1', 'TEs', 'tes', 'tes', '2017-03-14', '2017-04-18', 'Luthfi Aziz', 'Endraaa', 1),
	(5, ' NIA MEWUJUDKAN CITA SUAMI', 'image_20170314111350.jpg', '<p>Makara (Maju Karya Sejahtera), itulah nama usaha katering milik seorang anggota Komunitas Usaha Mandiri (KUM) YDSF asal Bandung, Nia Kurniati. Ibu dua anak ini ingin meneruskan cita-cita suaminya yang meninggal karena kanker testis duatahun silam: mempunyai CV.</p>\r\n\r\n<p>Alhamdulillah usaha kateringnya sudah berbadan resmi, CV Makara. Ini meneruskan keinginan almarhum suami,&rdquo; tuturnya. Mantan guru Taman Kanak Kanak ini biasa melayani pesanan nasi kotak, tumpeng, hingga acara prasmanan untuk pesta. Menu-menu yang disajikan pun sesuai permintaan. Selain harga yang terjangkau mulai Rp 13-20 ribu per porsi, Nia memberikan kemudahan&nbsp;bagi pelanggannya, yakni memberikan gratis ongkos kirim minimal pemesanan 20 kotak. Untuk kualitas makanan, Nia selalu menjaganya. Itulah mengapa hingga kini banyak pelanggan yang melakukan pesanan ulang (repeat order).</p>\r\n\r\n<p>Kiat dalam menjalani usaha katering perempuan kelahiran tahun 1978 ini adalah menambah jaringan lewat silaturahmi. Menurut dia, promo dari mulut ke mulut lebih cepat efeknya ketimbang ia memasang iklan di internet. Sejauh ini Makara Katering telah menerima banyak pesanan dari wilayah Sidoarjo dan Surabaya.</p>\r\n\r\n<p>Ibunda dari Yasmin dan Aizza ini merupakan ibu yang hebat. Ia bisa membagi waktu antara mengurus anak dan mengerjakan pesanan katering seorang diri. Semua dikerjakannya di rumahnya yang beralamat di Surya Asri 2 blok E4/16 Sidoarjo. Ia baru dibantu oleh tenaga tetangga jika pesanan mencapai 100 porsi lebih.</p>\r\n\r\n<p>Untuk tiap harinya, Nia biasa membuatkan pesanan nasi kotak untuk makan siang karyawan perusahaan sebanyak lebih kurang 15 porsi. Sebelum mengerjakan pesanan, Nia selalu terlebih dahulu menjalankan kewajiban mengantar anak ke sekolah. Setelah memasak pesanan, ia juga yang mengantar ke tempat pemesan. Sore harinya Nia tak lupa menjemput anak-anaknya di sekolah. Sebagai orangtua tunggal, Nia terus bersemangat untuk menjalani rutinitas itu demi anak-anaknya.</p>\r\n\r\n<p><strong>Terbantu</strong><br />\r\nSejak menjadi anggota KUM, Nia merasa terbantu untuk melancarkan usahanya. Pinjaman modal yang pernah ia terima difungsikan untuk membeli kotak nasi.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="/ydsf/assets/js/kcfinder/upload/images/1-kium-cinta-suami.jpg" style="height:269px; width:273px" /></p>\r\n\r\n<p>&ldquo;Bisa sampai ratusan order saat mendapat bantuan dari YDSF. Dengan ikut KUM ini saya merasa terbantu. Bahkan ada teman yang tertarik untuk gabung,&rdquo; katanya.</p>\r\n\r\n<p>Bagi yang baru memulai usaha dengan problem rata-rata butuh pinjaman modal, KUM ini bisa membuat mereka yang merintis lebih percaya diri.</p>\r\n\r\n<p>&rdquo;Harapan saya ke depan sih tidak sekadar diberi pinjaman, saja tapi juga diberi pembinaan terkait marketing dan manajemen keuangan,&rdquo; tuturnya.</p>\r\n\r\n<p>Nia mempunyai harapan memiliki tempat khusus untuk menjalankan usaha kateringnya. Ia juga berharap usahanya terus berkembang dan memiliki tenaga marketing untuk membantu mempromosikan katering miliknya. Ia berharap citanya terwujud.<br />\r\n<strong>Naskah: ayu|foto: anggun</strong></p>\r\n\r\n<p style="text-align:center"><strong>&ldquo;Bisa sampai ratusan order saat mendapat bantuan dari YDSF.</strong><br />\r\n<strong>Dengan ikut KUM ini saya merasa terbantu. Bahkan ada teman</strong><br />\r\n<strong>yang tertarik untuk gabung,&rdquo;</strong></p>\r\n', 'Makara (Maju Karya Sejahtera), itulah nama usaha katering milik seorang anggota Komunitas Usaha Mandiri (KUM) YDSF asal Bandung, Nia Kurniati. Ibu dua anak ini ingin meneruskan cita-cita suaminya yang meninggal karena kanker testis duatahun silam: mempuny', '2017-03-14', 'articles', '', '-nia-mewujudkan-cita-suami', NULL, NULL, 0, 0, '1', 'tes', '', 'tes,tes1', '2017-03-14', '2017-04-18', 'Luthfi Aziz', 'Endraaa', 1),
	(6, 'SUNNAH-SUNNAH DI DALAM BERWUDHU', 'image_20170314114827.jpg', '<p>Islam adalah agama yang sempurna yang mengajarkan segala permasalah, termasuk masalah beribadah. Tentu bersuci dalam beribadah adalah sebuah keterkaitan yang tidak bisa putus. Di dalam bersuci juga terdapat sunnah-sunnah yang Rasulullah ajarkan.</p>\r\n\r\n<p>Sunnah-sunnah ini adalah penyempurna wudhu&#39; seseorang dan dianjurkan seseorang Muslim untuk menyempurnakan wudhu&#39;nya sebisa mungkin, semampu yang dia bisa lakukan walaupun dalam keadaan yang kurang disukai. Misalnya:</p>\r\n\r\n<p>&radic; Pada saat musim panas sehingga tidak tersedia kecuali air panas untuk berwudhu. Atau sebaliknya,</p>\r\n\r\n<p>&radic; Pada musim dingin tidak tersedia kecuali air yang sangat dingin untuk berwudhu.</p>\r\n\r\n<p>Maka tatkala seseorang menemui kondisi tersebut dan dia tetap menyempurnakan wudhu&#39;nya, maka dia akan mendapatkan keutamaan sebagaimana yang disabdakan oleh Rasulullah shallAllahu &#39;alayhi wa sallam.</p>\r\n\r\n<p>Kata Beliau:</p>\r\n\r\n<p>أَلاَ أَدُلُّكُمْ عَلَى مَا يَمْحُو اللَّهُ بِهِ الْخَطَايَا وَيَرْفَعُ بِهِ الدَّرَجَاتِ. قَالُوا: بَلَى يَا رَسُولَ اللَّهِ. قَالَ: إِسْبَاغُ الْوُضُوءِ عَلَى الْمَكَارِهِ وَكَثْرَةُ الْخُطَا إِلَى الْمَسَاجِدِ وَانْتِظَارُ الصَّلاَةِ بَعْدَ الصَّلاَةِ فَذَلِكُمُ الرِّبَاطُ.</p>\r\n\r\n<p>&quot;Rasulullah bertanya kepada para shahabatnya: &#39;Maukah kalian aku tunjukkan pada amalan yang menjadikan Allah menghapus dosa-dosa kalian dan mengangkat derajat kalian?&#39;</p>\r\n\r\n<p>Mereka (para shahabat). menjawab: &#39;Mau, ya Rasulullah.&#39;</p>\r\n\r\n<p>Maka Beliau bersabda: &#39;Menyempurnakan wudhu&#39; walaupun pada keadaan yang tidak disukai, memperbanyak langkah menuju masjid, dan menunggu shalat berikutnya sesudah selesai shalat, maka ketahuilah itu adalah ribath&#39;.&quot; (HR Muslim, Nasai dan Tirmidzi)</p>\r\n\r\n<p>&rArr; Ar-Ribath adalah salah satu amalan dalam jihad yaitu menjaga perbatasan dalam perang. Oleh karena itu, kita berusaha semaksimal mungkin untuk bisa menyempurnakan wudhu&#39; kita. Para Sahabat sekalian yang dirahmati Allah Subhanahu wa Ta&rsquo;ala.</p>\r\n\r\n<p>Ada beberapa Sunnah dalam Berwudhu :</p>\r\n\r\n<ul>\r\n	<li><strong>Dengan mengucapkan</strong></li>\r\n	<li><strong>Bismillah Mencuci kedua telapak tangan sebelum memasukkannya ke dalam bejana</strong></li>\r\n	<li><strong>Berkumur-kumur dan juga mengeluarkan air dari hidung</strong></li>\r\n	<li><strong>Mengusap seluruh kepala</strong></li>\r\n	<li><strong>Mengusap kedua telinga, baik bagian luar dan juga bagian dalamnya dengan air yang baru</strong></li>\r\n	<li><strong>Menyela-nyela janggut yang tebal &rArr; Agar air wudhu&#39; nya merata.</strong></li>\r\n</ul>\r\n\r\n<p>Dalam hadits Anas bin Malik radhiyAllahu Ta&#39;ala &#39;anhu:</p>\r\n\r\n<p>أَنَّ النبي صَلَّى الله عَلَيْهِ وَسَلَّمَ كَانَ إِذَا تَوَضَّأَ أَخَذَ كَفًّا مِنْ مَاءٍ فَأَدْخَلَهُ تَحْتَ حَنَكِهِ فَخَلَّلَ بِهِ لِحْيَتَهُ</p>\r\n\r\n<p>&quot;Nabi shallAllahu &#39;alayhi wa sallam apabila Beliau berwudhu, Beliau mengambil air setelapak tangan, kemudian memasukkan air ke dalam janggutnya dari bawah dagunya, kemudian menyela-nyela janggutnya.&quot; (HR. Abu Dawud)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Menyela-nyela/membasuh di sela-sela jari-jari tangan dan jari-jari kaki</strong></li>\r\n</ul>\r\n\r\n<p>Dalam hadits Rasulullah shallAllahu &#39;alayhi wa sallam bersabda:</p>\r\n\r\n<p>إِذَا تَوَضَّأْتَ فَخَلِّلْ الْأَصَابِعَ يَدَيْكَ وَرِجْلَيْكَ</p>\r\n\r\n<p>&quot;Apabila kamu berwudhu, maka basuhlah di sela-sela jari tangan dan jari-jari kaki.&quot; (HR. Tirmidzi)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Mendahulukan bagian kanan, kemudian mengulang tiga kali dan muwalat</strong>&nbsp;&rArr; Muwalat: berkesinambungan antara anggota wudhu&#39; satu kemudian anggota wudhu&#39; yang lainnya telah disebutkan di dalam hadits &#39;Utsman yang telah lalu. berdo&#39;a setelah berwudhu.</li>\r\n</ul>\r\n\r\n<p>Dalam hadits Muslim, Rasulullah shallAllahu &#39;alayhi wa sallam bersabda:</p>\r\n\r\n<p>مَا مِنْكُمْ مِنْ أَحَدٍ يَتَوَضَّأُ فَيُبْلِغُ أَوْ فَيُسْبِغُ الْوَضُوءَ ثُمَّ يَقُولُ أَشْهَدُ أَنْ لَا إِلَهَ إِلَّا اللَّهُ وَأَنَّ مُحَمَّدًا عَبْدُ اللَّهِ وَرَسُولُهُ إِلَّا فُتِحَتْ لَهُ أَبْوَابُ الْجَنَّةِ الثَّمَانِيَةُ يَدْخُلُ مِنْ أَيِّهَا شَاءَ</p>\r\n\r\n<p>&quot;Setiap orang diantara kalian yang berwudhu dan mencapai puncak atau menyempurnakan wudhu&#39;nya, kemudian berdo&#39;a:</p>\r\n\r\n<p><em><strong>&#39;Asyhadu an la ilaha illAllah wa anna Muhammadar Rasulullah.&#39;</strong></em></p>\r\n\r\n<p>[Aku bersaksi bahwasanya tiada ilah-tidak ada Tuhan-yang berhak disembah selain Allah dan bahwasanya Nabi Muhammad adalah utusan Allah Subhanahu wa Ta&rsquo;ala]</p>\r\n\r\n<p>Maka akan dibukakan baginya delapan pintu surga, dan dia bisa masuk dari mana saja pintu yang dia kehendaki.&quot;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>(Dari Berbagai Sumber)</p>\r\n', 'Islam adalah agama yang sempurna yang mengajarkan segala permasalah, termasuk masalah beribadah. Tentu bersuci dalam beribadah adalah sebuah keterkaitan yang tidak bisa putus. Di dalam bersuci juga', '2017-03-14', 'articles', '', 'sunnah-sunnah-di-dalam-berwudhu', NULL, NULL, 0, 0, '1', 'Sunnah', 'SUNNAH-SUNNAH DI DALAM BERWUDHU', 'Sunnah,wudhu', '2017-03-14', '2017-04-18', 'Adminsitrator', 'Endraaa', 1),
	(7, 'BANGKIT DARI BENCANA BERSAMA YDSF', 'image_20170314115844.jpg', '<p>Bencana puting beliung secara mendadak menghantam Sidoarjo. Sebanyak ratrusan rumah dari 4 dusun di Krian, Sidoarjo tersapu oleh bencana ini. Penanganan cepat harus segera sudah dilakukan untuk menghindari dampak psikis pada masyarakat. Sebagai Lembaga zakat yang bertaraf nasional dan sekaligus menjadi Yayasan yang bergerak di dalam bidang kemanusiaan, YDSF selalu mengutamakan&nbsp;<em>Recovery</em>&nbsp;yang cepat bagi para korban.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bersama Unit ACT (Aksi Cepat Tanggap), YDSF langsung bergerak menuju lokasi bencana. Bantuan-bantuan seperti 250 nasi kotak dan minum disalurkan di desa terungkulon RT 6/RW 1 Krian, Sidoarjo. Tentu tidak hanya YDSF yang beraksi di sini, tetapi bantuan dari kita semua. Bantuan yang sedikit tetapi berdampak besar bagi mereka. Membantu mengangkat mereka kembali untuk bangkit dari bencana tidak hanya bersama YDSF, tetapi bersama kita semua.</p>\r\n', 'Bencana puting beliung secara mendadak menghantam Sidoarjo. Sebanyak ratrusan rumah dari 4 dusun di Krian, Sidoarjo tersapu oleh bencana ini. Penanganan cepat harus segera sudah dilakukan untuk', '2017-03-14', 'news', '', 'bangkit-dari-bencana-bersama-ydsf', NULL, NULL, 0, 0, '1', 'Bangkit', 'BANGKIT DARI BENCANA BERSAMA YDSF', 'bangkit,bencana,ydsf', '2017-03-14', '2017-04-18', 'Adminsitrator', 'Endraaa', 1),
	(8, 'DI BRUNEI, BERBEDA KHUTBAH, PELANGGARAN!', 'image_20170314023053.jpg', '<h4>Brunei Darussalam dan Indonesia adalah tetangga dekat. Negara dengan penduduk hanya 370 ribu orang ini tertelak di bagian utara Pulau Borneo, masih satu pulau dengan Kalimantan. Dengan bahasa Melayu, negeri bersistem kerajaan ini memiliki peranan penting di dunia. Meskipun secara wilayah tergolong cukup kecil, namun pantas disebut sebagai negara maju. Menurut Dana Moneter Internasional, Brunei memiliki produk domestik bruto per kapita terbesar kelima di dunia. Forbes bahkan menempatkan Brunei sebagai negara terkaya kelima dari 182 negara (Wikipedia.org).</h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Sebelum abad ke-16, Brunei memiliki peranan cukup penting dalam penyebaran Islam di wilayah Kalimantan. Bisa jadi karena hal ini pula antara Brunei dan Indonesia memiliki kemiripan dalam bermazhab, Syafi&rsquo;i. Bahkan secara konsep akidah yang dipegang sudah termaktub &ldquo;Melayu Islam Baraja&rdquo; dengan pedoman Ahlussunnah Wal Jamaah. Begitu kentalnya agama Islam dalam sistem negara di Brunei menjadikan pengelolaan masjid patut disimak karena memiliki corak yang khas.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&ldquo;Di Brunei, semua masjid merupakan milik negara. Pegawainya juga digaji oleh negara layaknya Pegawai Negeri Sipil. Tidak sembarang orang boleh menjadi imam di suatu masjid karena harus memenuhi kriteria tertentu,&rdquo; tulis Fajar Nur Aly yang bekerja sebagai Islamic Art Advertising di Brunei Darussalam melalui komunikasi berpesanan virtual. Negara dengan luas wilayah 5.756 km atau hampir dua kali lipat luas kota Surabaya ini, memiliki masjid yang dikelola pemerintah.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bahkan keterlibatan pemerintah sudah dimulai sejak pembangunannya. Maka secara perizinan juga diatur ketat oleh pemerintah. &ldquo;Bahkan ada sejumlah uang dari masyarakat, juga ada tanah wakaf untuk masjid, tetap harus memakai izin,&rdquo; tulis Fajar Nur Aly. Hal ini pula yang menjadikan antara satu masjid dengan yang lain tidak ada warna golongan. &ldquo;Jika ada yang ingin mengadakan kegiatan untuk sebuah golongan, harus mendapat izin dari kementerian agama. Kemudian diberikan ke masjid,&rdquo; tulis alumni Pondok Modern Darussalam Gontor.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>mengaji atau hanya belajar agama. Semua difungsikan sama. Hal ini efek dari keteguhan pemerintah sebagai pengelola, sehingga agama Islam yang dikenal di Brunei hanya satu warna saja. &ldquo;Bahkan isi khutbah shalat Jumat harus seragam. Tidak ada yang berani berbeda, semua sudah diatur oleh kementerian agama. Jika ada yang berbeda, maka ditindak sebagai sebuah pelanggaran besar,&rdquo; tulis ayah tiga anak ini. Masih menurut Fajar, dengan sistem yang demikian, kegiatan keagamaan benar-benar terpusat di masjid. Di Brunei, tidak ada yang namanya Taman Pendidikan al Quran di rumah-rumah seperti yang ada di Indonesia. Semua sudah tersedia di Masjid. &ldquo;Anak-anak yang mau mengaji harus pergi ke masjid, atau mengundang ustaz privat di rumah&rdquo;.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Yang unik menurut Fajar, masjid di Brunei dikelola oleh seorang takmir dengan dua atau tiga orang imam inti yang digaji oleh negara. &ldquo;Kemungkinan sangat kecil masjid di Brunei kebingungan imam, apalagi sampai tidak ada yang mau mengumandangkan azan. Fasilitas masjid juga cukup baik. Semua memiliki pendingin ruangan,&rdquo; tulis ahli kaligrafi itu.</p>\r\n', 'Brunei Darussalam dan Indonesia adalah tetangga dekat. Negara dengan penduduk hanya 370 ribu orang ', '2017-03-14', 'articles', '', 'di-brunei-berbeda-khutbah-pelanggaran', NULL, NULL, 0, 0, '1', 'khutbah', 'DI BRUNEI, BERBEDA KHUTBAH, PELANGGARAN!', 'brunei,khutbah,pelanggaran', '2017-03-14', '2017-04-18', 'Adminsitrator', 'Endraaa', 1),
	(9, 'KEBERSIHAN, SEBAGIAN DARI IMAN', 'image_20170314023439.jpg', '<p>Pasca Banjir bandang yang terjadi pada Rabu (21/9/2016) di kabupaten Garut membuat kondisi lingkungan menjadi kotor. Kebersihan yang menjadi salah satu dari cabang iman menjadi sedikit tergerus karena kotornya banjir yang menerjang. Termasuk yang terkena dampak banjir ini adalah pemukiman warga. Salah satunya di Kp. Leuwiereng Desa Mekarsari Kec. Cikajang Kab. Garut.</p>\r\n\r\n<p><br />\r\nSering kita seakan mengabaikan kebersihan dari kamar mandi kita. DInkeseharian kita saja sering kita lupakan, apalagi setelah terjadinya banjir seperti yang di alami warga Desa Mekarsari. Sebagai salah satu Yayasan Nasional dan juga termasuk Lembaga Dakwah, YDSF hadir untuk membantu. Bersama Tim Unit Aksi Cepat (UAC) YDSF, tepat tanggal (14/2) tim UAC meninjau hasil pembangunan MCK di beberapa titik wilayah terdampak banjir bandang Garut. Alhamdulillah&nbsp; fasilitas MCK telah selesai dibangun dan bisa dimanfaatkan warga sekitar.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ahmad Munawar Zaidin selaku pengelola pendidikan di Kp. Leuwiereng Desa Mekarsari Kec. Cikajang Kab. Garut menyampaikan, MCK yang dibangun disini manfaatnya luar biasa, bukan hanya warga sekitar yang menggunakannya, warga yang jauhpun menggunakannya. Bahkan sekarang dipakai oleh banyak tamu yang datang ke lokasi wisata setempat.</p>\r\n\r\n<p>Dengan adanya Bantuan MCK ini diharapkan Kebersihan menjadi prioritas utama tidak hanya di kehidupan sehari-hari, juga beribadah. Karena kebersihan adalah sebagian daripada Iman</p>\r\n', 'Pasca Banjir bandang yang terjadi pada Rabu (21/9/2016) di kabupaten Garut membuat kondisi lingkungan menjadi kotor. Kebersihan yang menjadi salah satu dari cabang iman menjadi sedikit tergerus karena kotornya banjir yang menerjang. Termasuk yang terkena ', '2017-03-14', 'articles', '', 'kebersihan-sebagian-dari-iman', NULL, NULL, 0, 0, '1', 'Kebersihan', 'KEBERSIHAN, SEBAGIAN DARI IMAN', 'bersih,iman', '2017-03-14', '2017-04-18', 'Adminsitrator', 'Endraaa', 1),
	(10, 'YDSF DAN KEMAJUAN PEDAGANG KECIL', 'image_20170314024135.jpg', '<p>Sebagai Yayasan yang berskala nasional, tentu bidang kerja semakin besar pula, termasuk melayani pedagang kecil. Tepat Senin (13/2/17) YDSF memberikan bantuan alat usaha berupa 2 rombong / gerobak dagang untuk membantu kemajuan usaha dari 2 pengusaha</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Rombong untuk Machsum yang direncakan untuk dibuat usaha Rujak Buah</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Rombong untuk Katmin yang direncanakan untuk berjualan makanan Pentol</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&quot;Semoga dari rombong ini bisa menjadi titik awal dalam menghidupi keluarga&quot; tutur Katmin.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>asdfasdfasdf</p>\r\n', 'Sebagai Yayasan yang berskala nasional, tentu bidang kerja semakin besar pula, termasuk melayani pedagang kecil. Tepat Senin (13/2/17) YDSF memberikan bantuan alat usaha berupa 2 rombong / gerobak dagang untuk membantu kemajuan usaha dari 2 pengusaha asas', '2017-03-14', 'articles', '', 'ydsf-dan-kemajuan-pedagang-kecil', NULL, NULL, 0, 0, '1', 'Pedagang', 'YDSF DAN KEMAJUAN PEDAGANG KECIL', 'ydsf,pedagang', '2017-03-14', '2017-04-18', 'Adminsitrator', 'Endraaa', 1),
	(11, 'TIM YDSF SALURKAN BANTUAN TAHAP 2 DI AREA LONGSOR PONOROGO', 'image_20170406125151.jpg', '<p>Bencana longsor di Desa Banaran, Kecamatan Pulung, Ponorogo, Jawa Timur pada Sabtu (1/4/2017) lalu masih menyisakan beberapa hal. Selama di pengungsian, warga masih kesulitan untuk menemukan tempat yang layak untuk berteduh. Anak-anak pun masih trauma dan masih terbatas aktivitasnya. Proses evakuasi masih berlanjut di tengah medan yang sulit. Sekitar 28 orang masih belum ditemukan.</p>\r\n\r\n<p>Berikut ini kondisi yang dilaporkan M. Imron Wahyudi, petugas YDSF yang terjun di lokasi pada Selasa (4/4/2017):</p>\r\n\r\n<ol>\r\n	<li>Jumlah Orang Yang Masih Tertimbun Longsor 25 Orang (Belum Ditemukan)</li>\r\n	<li>Tiga Orang Sudah ditemukan, Namun Dalam Keadaan Meninggal (Katemi (70 thn), Iwan (25 th), Sunadi (35 th)</li>\r\n	<li>Dua dusun terkena dampak kena longsor langsung (Desa Tangkil dan Krajan)</li>\r\n	<li>Ada enam lokasi pengungsian Yang Tersebar, Yaitu</li>\r\n	<li>Rumah Pak indar (Pomahan-Krajan) 55 orang,</li>\r\n	<li>Musholla Gondangsari 125 orang,</li>\r\n	<li>Rumah Pak Purwanto Krajan 75 orang,</li>\r\n	<li>Rumah Bu Sri Natun Gondangsari 15 orang,</li>\r\n	<li>Rumah Pak Wahyudinanto Mudin Gondangsari 9 orang,</li>\r\n	<li>Rumah Pak Lurah Sarno 30 orang</li>\r\n</ol>\r\n\r\n<p>Sumber dari Pak Misri, Perangkat Desa Banaran Ponorogo</p>\r\n\r\n<p>&nbsp;&nbsp;Pada hari itu (4/4/2017) tim YDSF menyerahkan sejumlah bantuan berupa:<br />\r\n1. Selimut 40 lembar<br />\r\n2. Tikar spon 60 lembar<br />\r\n3. Lilin 2 dus @ 50 pak. 1 pak berisi 6 batang lilin.<br />\r\n4. Korek gas 2 pak @100 buah<br />\r\n5. Obat ringan 10 pak @ 10 sachet<br />\r\n6. Biskuit untuk anak 39 bungkus.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="http://ydsf.org/assets/media/2017/04/05/396/1-longsor-ponorogo2-ydsf2.jpg" style="height:315px; width:525px" /></p>\r\n\r\n<p><img alt="" src="http://ydsf.org/assets/media/2017/04/05/397/1-longsor-ponorogo2-ydsf3.jpg" style="height:301px; width:502px" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="http://ydsf.org/assets/media/2017/04/05/398/1-longsor-ponorogo2-ydsf4.jpg" style="height:408px; width:544px" /></p>\r\n', 'Bencana longsor di Desa Banaran, Kecamatan Pulung, Ponorogo, Jawa Timur pada Sabtu (1/4/2017) lalu masih menyisakan beberapa hal. Selama di pengungsian, warga masih kesulitan untuk menemukan tempat yang layak untuk berteduh. Anak-anak pun masih trauma dan', '2017-04-06', 'articles', '', 'tim-ydsf-salurkan-bantuan-tahap-2-di-area-longsor-ponorogo', NULL, NULL, 0, 0, '1', 'Bantuan', 'Bencana longsor di Desa Banaran, Kecamatan Pulung, Ponorogo, Jawa Timur pada Sabtu (1/4/2017) lalu masih menyisakan beberapa hal. Selama di pengungsian, warga masih kesulitan untuk menemukan tempat yang layak untuk berteduh. Anak-anak pun masih traum', 'YDSF,bantuan,longsor', '2017-04-06', '2017-04-18', 'Adminsitrator', 'Endraaa', 1),
	(12, 'ZAKIR NAIK AJAK MUSLIM INDONESIA GIAT BERDAKWAH', 'image_20170406125355.jpg', '<p>Zakir Naik Ajak Muslim Indonesia Giat Berdakwah</p>\r\n\r\n<p>Dakwah adalah kewajiban bagi setiap muslim. Itulah inti dari kuliah umum Dr. Zakir Naik di Universitas Darussalam Gontor Ponorogo, Jawa Timur, Selasa (4/4/2017). Untuk itu, setiap muslim harus memahami ayat-ayat ketuhanan di dalam Al Quran. &ldquo;Kita lihat surat Ali Imran ayat 64. Artinya, &lsquo;Katakanlah, &lsquo;Hai Ahli Kitab, marilah (berpegang) kepada suatu kalimat (ketetapan) yang tidak ada perselisihan antara kami dan kamu, bahwa tidak kita sembah kecuali Allah dan tidak kita persekutukan Dia dengan sesuatupun dan tidak (pula) sebagian kita menjadikan sebagian yang lain sebagai tuhan selain Allah.&rsquo; Jika mereka berpaling maka katakanlah kepada mereka, &lsquo;Saksikanlah, bahwa kami adalah orang-orang yang muslim.&rsquo;&quot;</p>\r\n\r\n<p>Ayat ini, lanjut ia, adalah ketentuan paling penting dalam usaha dakwah umat Islam kepada umat lain. Bahwa tiada tuhan yang berhak disembah kecuali Allah, Tuhan yang Mahakuasa. Konsep ketuhanan yang jelas akan menjadi bekal bagi kita dalam berdakwah. Dalam kuliah yang berjudul&nbsp;<em>Religion in Right Perspective</em>&nbsp;ini, tokoh Islam asal India ini bahwa membahas agama-agama haruslah membahas konsep ketuhanan terlebih dahulu.</p>\r\n\r\n<p>Zakir mendeskripsikan secara detil konsep ketuhanan di Hindu, Kristen, Yahudi dan Islam dengan mengupas ayat-ayat ketuhanan di masing-masing kitab suci secara gamblang. &ldquo;Kesimpulan saya, berdasar kitab Upanisad/Weda, Taurat, dan Injil bahwa sejatinya konsep Tuhan sudah jelas. Bahwa hanya ada Tuhan yang Esa. Bahkan di Upanisad disebut Tuhan yang Mahaesa tidak punya bapak, tidak punya ibu, tidak bergambar, tidak berupa patung, ataupun lukisan. Dialah Tuhan yang Mahakuasa,&rdquo; jelas tokoh yang telah berceramah di semua benua ini.</p>\r\n\r\n<p>&ldquo;Bahkan hanya Al Quran-lah satu-satunya kitab suci di luar Kristen yang mengakui Isa/Yesus sebagai utusan Tuhan. Saya menantang siapa saja yang berani menunjukkan ayat Injil yang memuat pernyataan tegas bahwa Isa/Yesus mengatakan: saya adalah tuhan atau &nbsp;sembahlah saya,&rdquo; jelasnya di hadapan 15 ribu hadirin yang memadati lapangan kampus Unida Gontor.</p>\r\n\r\n<p>Dalam diskusi agama yang besar ini, hadirin dibuat terharu dengan proses keislaman tiga orang nonmuslim yang hadir. Ada dua pria dan satu wanita yang mengikrarkan dua kalimat syahadat dalam acara tersebut. YDSF mengirimkan 30 orang dalam acara itu yang terdiri dari 15 dai, 5 mahasiswa binaan, dan 10 karyawan. YDSF mendapat undangan khusus dari pihak Unida dalam rangkaian safari dakwah Zakir Naik di Indonesia.</p>\r\n', 'Dakwah adalah kewajiban bagi setiap muslim. Itulah inti dari kuliah umum Dr. Zakir Naik di Universitas Darussalam Gontor Ponorogo, Jawa Timur, Selasa (4/4/2017). Untuk itu, setiap muslim harus memahami ayat-ayat ketuhanan di dalam Al Quran.', '2017-04-06', 'articles', '', 'zakir-naik-ajak-muslim-indonesia-giat-berdakwah', NULL, NULL, 0, 0, '1', 'Zakir Naik', 'ZAKIR NAIK AJAK MUSLIM INDONESIA GIAT BERDAKWAH', 'Zakir Naik,Dakwah', '2017-04-06', '2017-04-18', 'Adminsitrator', 'Endraaa', 1),
	(13, 'Berzakat', 'image_20170421113957.jpg', '<p>Seseorang akan terkena kewajiban zakat,&nbsp; jika telah terpenuhi syarat-syarat di bawah ini :<br />\r\n<br />\r\n<strong><em>Syarat Pertama :&nbsp; Harta tersebut harus didapatkan dengan cara yang halal.</em></strong><br />\r\n<strong><em>Syarat Kedua :&nbsp; Kepemilikan secara penuh.</em></strong><br />\r\n<strong><em>Syarat Ketiga : Harta tersebut berkembang.</em></strong><br />\r\n<strong><em>Syarat Keempat&nbsp; : Nishab</em></strong><br />\r\n<strong><em>Syarat Keempat :&nbsp; Melebihi kebutuhan&nbsp; pokok.</em></strong><br />\r\n<strong><em>Syarat Keenam :&nbsp; Haul</em></strong><br />\r\n&nbsp;</p>\r\n', 'Berzakat merupakan', '2017-04-21', 'articles', '', 'berzakat', NULL, NULL, 0, 0, '1', '<head> <meta name=”description” content=”Berzakat”> </head>', '<meta name=\'description\' content=\'Seseorang akan terkena kewajiban zakat,  jika telah terpenuhi syarat-syarat di bawah ini :\r\n\r\nSyarat Pertama :  Harta tersebut harus didapatkan dengan cara yang halal.\r\nSyarat Kedua :  Kepemilikan secara penuh.\r\nSyar', '<meta name=\'keywords\' content=\'Kewajiban Berzakat\' />', '2017-04-21', '2017-05-12', 'Dini', 'Luthfi Aziz', 1);
/*!40000 ALTER TABLE `nb_post` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_post_categories
CREATE TABLE IF NOT EXISTS `nb_post_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1' COMMENT '0:false; 1:true',
  `note` text,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_post_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `nb_post_categories` DISABLE KEYS */;
REPLACE INTO `nb_post_categories` (`id`, `name`, `is_active`, `note`, `crtd_at`, `edtd_at`, `author`, `last_edit`) VALUES
	(1, 'Kategori 1', '1', 'Kategori 1', '2017-02-03', '2017-02-03', 'Luthfi Aziz', NULL),
	(2, 'kategori', '1', 'wwwww', '2017-03-29', '2017-05-12', 'Adminsitrator', 'Luthfi Aziz');
/*!40000 ALTER TABLE `nb_post_categories` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_question_message
CREATE TABLE IF NOT EXISTS `nb_question_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `donatur` int(255) DEFAULT NULL,
  `admin` int(255) DEFAULT NULL,
  `question` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_question_message: ~0 rows (approximately)
/*!40000 ALTER TABLE `nb_question_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `nb_question_message` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_question_ticket
CREATE TABLE IF NOT EXISTS `nb_question_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `message` text,
  `type` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `donatur` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_question_ticket: ~0 rows (approximately)
/*!40000 ALTER TABLE `nb_question_ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `nb_question_ticket` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_share
CREATE TABLE IF NOT EXISTS `nb_share` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `media` varchar(100) DEFAULT NULL,
  `crtd_at` datetime DEFAULT NULL,
  `edtd_at` datetime DEFAULT NULL,
  `jumshare` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_share: 0 rows
/*!40000 ALTER TABLE `nb_share` DISABLE KEYS */;
/*!40000 ALTER TABLE `nb_share` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_spage_other
CREATE TABLE IF NOT EXISTS `nb_spage_other` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `stitle` varchar(150) DEFAULT NULL,
  `desc` text,
  `img` text,
  `is_active` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `page` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_spage_other: ~7 rows (approximately)
/*!40000 ALTER TABLE `nb_spage_other` DISABLE KEYS */;
REPLACE INTO `nb_spage_other` (`id`, `title`, `stitle`, `desc`, `img`, `is_active`, `crtd_at`, `edtd_at`, `author`, `page`) VALUES
	(1, 'Sekilas YDSF', 'Sekilas YDSF', '<p>Tes Aja</p>\r\n', 'image_20170411013109.jpg', '1', '2017-04-11', '2017-04-11', 'Adminsitrator', 2),
	(3, 'PROFIL YDSF', 'Lorem Ipsum Dolor Siame', '<p>How How</p>\r\n', 'image_20170411105402.jpg', '1', '2017-04-11', '2017-04-11', 'Adminsitrator', 3),
	(4, 'Legalitas', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 'image_20170512024201.jpg', '1', '2017-05-12', '2017-05-12', 'Luthfi Aziz', 5),
	(5, 'Pengurus', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 'image_20170512024301.jpg', '1', '2017-05-12', '2017-05-12', 'Luthfi Aziz', 4),
	(6, 'Visi dan Misi', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 'image_20170512024334.jpg', '1', '2017-05-12', '2017-05-12', 'Luthfi Aziz', 6),
	(7, 'Syarat & Ketentuan', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 'image_20170512024419.jpg', '1', '2017-05-12', '2017-05-12', 'Luthfi Aziz', 8),
	(8, 'Kebijakan Privasi', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n', 'image_20170512024448.jpg', '1', '2017-05-12', '2017-05-12', 'Luthfi Aziz', 7);
/*!40000 ALTER TABLE `nb_spage_other` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_spage_page
CREATE TABLE IF NOT EXISTS `nb_spage_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_key` varchar(150) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_spage_page: ~7 rows (approximately)
/*!40000 ALTER TABLE `nb_spage_page` DISABLE KEYS */;
REPLACE INTO `nb_spage_page` (`id`, `name_key`, `name`) VALUES
	(2, 'sekilasydsf', 'Sekilas YDSF'),
	(3, 'profilydsf', 'Profil YDSF'),
	(4, 'pengurus', 'Pengurus'),
	(5, 'legalitas', 'Legalitas'),
	(6, 'visimisi', 'Visi Misi'),
	(7, 'kebijakan', 'Kebijakan'),
	(8, 'syrtdankttn', 'Syrt dan Kttn');
/*!40000 ALTER TABLE `nb_spage_page` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_spage_program
CREATE TABLE IF NOT EXISTS `nb_spage_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `m_img` text,
  `m_title` varchar(100) DEFAULT NULL,
  `m_desc` text,
  `icon_img` text,
  `icon_img_g` text,
  `icon_title` varchar(100) DEFAULT NULL,
  `icon_desc` text,
  `tipe` enum('zakat','shadaqah','infaq') DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `last_edit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_spage_program: ~3 rows (approximately)
/*!40000 ALTER TABLE `nb_spage_program` DISABLE KEYS */;
REPLACE INTO `nb_spage_program` (`id`, `m_img`, `m_title`, `m_desc`, `icon_img`, `icon_img_g`, `icon_title`, `icon_desc`, `tipe`, `is_active`, `crtd_at`, `edtd_at`, `author`, `last_edit`) VALUES
	(8, 'image_20170523042605.jpg', 'Zakat', 'Bantuan Anda, berapa pun jumlahnya, sangat bermanfaat buat mereka yang membutuhkan.\r\nKami menyalurkan seluruh bantuan amanat dari maysarakat untuk sebesar-besar kemanfaatan yang layak menerimanya. ', 'icon_20170523042605.png', NULL, 'ZAKAT MAAL', 'Lorem Ipsum', 'zakat', '1', '2017-04-12', '2017-05-23', 'Adminsitrator', 'Adminsitrator12'),
	(13, 'image_20170412030718.jpg', 'PROGRAM DONASI YDSFS', 'Bantuan Anda, berapa pun jumlahnya, sangat bermanfaat buat mereka yang membutuhkan.\r\nKami menyalurkan seluruh bantuan amanat dari maysarakat untuk sebesar-besar kemanfaatan yang layak menerimanya. ', 'icon_20170412030718.png', NULL, 'SHADAQAH', 'Subtitle 3', 'shadaqah', '1', '2017-04-12', '2017-04-12', 'Adminsitrator', NULL),
	(14, 'image_20170412054527.jpg', 'Infaq', 'Infaq', 'icon_20170412054527.png', NULL, 'INFAQ', 'Infaq', 'infaq', '1', '2017-04-12', '2017-05-12', 'Adminsitrator', 'Luthfi Aziz');
/*!40000 ALTER TABLE `nb_spage_program` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_subscribe
CREATE TABLE IF NOT EXISTS `nb_subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table u7043207_berzakat.nb_subscribe: 3 rows
/*!40000 ALTER TABLE `nb_subscribe` DISABLE KEYS */;
REPLACE INTO `nb_subscribe` (`id`, `email`) VALUES
	(4, 'abdulhdi.endra@gmail.com'),
	(5, 'diniprita@gmail.com'),
	(7, 'nurulfbre@gmail.com');
/*!40000 ALTER TABLE `nb_subscribe` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_sys_forgot_password
CREATE TABLE IF NOT EXISTS `nb_sys_forgot_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) DEFAULT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='tabel pertanyaan lupa password';

-- Dumping data for table u7043207_berzakat.nb_sys_forgot_password: ~6 rows (approximately)
/*!40000 ALTER TABLE `nb_sys_forgot_password` DISABLE KEYS */;
REPLACE INTO `nb_sys_forgot_password` (`id`, `question`, `crtd_at`, `edtd_at`, `author`, `last_edit`) VALUES
	(1, 'apa hobimu?', '2017-01-05', NULL, 'luthfi', 'luthfi'),
	(2, 'apa makanan kesukaanmu?', '2017-01-05', NULL, 'luthfi', 'luthfi'),
	(3, 'apa buku bacaan kesukaanmu?', '2017-01-05', NULL, 'luthfi', 'luthfi'),
	(4, 'siapa nama ibumu?', '2017-01-05', NULL, 'luthfi', 'luthfi'),
	(5, 'siapa nama ayahmu?', '2017-01-05', NULL, 'luthfi', 'luthfi'),
	(6, 'kamu memiliki berapa saudara?', '2017-01-05', NULL, 'luthfi', 'luthfi');
/*!40000 ALTER TABLE `nb_sys_forgot_password` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_sys_logs
CREATE TABLE IF NOT EXISTS `nb_sys_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `activity` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_sys_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `nb_sys_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `nb_sys_logs` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_sys_menus
CREATE TABLE IF NOT EXISTS `nb_sys_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `url` text NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `position` int(2) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `is_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0:false; 1:true',
  `crtd_at` datetime NOT NULL,
  `edtd_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `author` varchar(50) NOT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_sys_menus: ~27 rows (approximately)
/*!40000 ALTER TABLE `nb_sys_menus` DISABLE KEYS */;
REPLACE INTO `nb_sys_menus` (`id`, `name`, `url`, `parent`, `position`, `icon`, `level`, `is_active`, `crtd_at`, `edtd_at`, `author`, `last_edit`) VALUES
	(3, 'Setting', '#', 0, 1, 'fa fa-gears', 0, '1', '2015-08-25 09:22:56', '2017-01-24 17:45:48', 'system', 'Luthfi Aziz'),
	(4, 'Manajemen Menu', 'settingmenu', 3, 1, '', 1, '1', '2015-08-25 09:23:45', '2017-01-24 17:22:45', 'system', 'Luthfi Aziz'),
	(5, 'Manajemen Akses', 'settingakses', 3, 2, '', 1, '1', '2015-08-25 09:24:08', '2017-01-24 17:22:47', 'system', 'system'),
	(6, 'Manajemen User', 'settingusers', 3, 3, '', 1, '1', '2015-08-25 09:24:36', '2017-01-24 17:22:50', 'system', 'system'),
	(7, 'Manajemen Artikel', 'artikel', 0, 2, 'fa fa-rss-square', 0, '1', '2017-01-18 19:11:29', '2017-01-24 17:45:51', 'Luthfi Aziz', 'Luthfi Aziz'),
	(8, 'Kategori Artikel', 'artikelkategori', 7, 1, '', 0, '1', '2017-01-18 19:40:00', '2017-01-24 17:22:54', 'Luthfi Aziz', NULL),
	(9, 'Artikel', 'artikel', 7, 2, '', 0, '1', '2017-01-18 19:40:19', '2017-05-12 14:54:02', 'Luthfi Aziz', 'Luthfi Aziz'),
	(10, 'Manajemen Campaign', '#', 0, 3, 'fa fa-bookmark', 0, '1', '2017-01-18 19:41:33', '2017-02-11 22:03:15', 'Luthfi Aziz', 'Luthfi Aziz'),
	(11, 'Kategori Campaign', 'campaignkategori', 10, 1, '', 0, '1', '2017-01-18 19:42:16', '2017-02-11 22:03:01', 'Luthfi Aziz', 'Luthfi Aziz'),
	(12, 'Campaign', 'campaign', 10, 2, '', 0, '1', '2017-01-18 19:42:48', '2017-02-11 22:03:15', 'Luthfi Aziz', NULL),
	(13, 'Manajemen Bank', 'manajemenbank', 3, 4, '', 0, '1', '2017-03-27 15:21:42', '2017-03-27 16:11:51', 'Luthfi Aziz', 'Luthfi Aziz'),
	(14, 'FAQ', '#', 0, 6, 'fa fa-question-circle', 0, '1', '2017-03-27 15:33:43', '2017-04-10 10:49:09', 'Luthfi Aziz', 'Adminsitrator'),
	(15, 'FAQ kategori', 'faq_kategori', 14, 1, '', 0, '1', '2017-03-27 15:35:19', '0000-00-00 00:00:00', 'Luthfi Aziz', NULL),
	(16, 'Deskripsi', 'faq_des', 14, 2, '', 0, '1', '2017-03-27 15:37:11', '0000-00-00 00:00:00', 'Luthfi Aziz', NULL),
	(17, 'Campaign Slider', 'campaignslider', 10, 3, '', 0, '1', '2017-03-28 10:06:42', '0000-00-00 00:00:00', 'Adminsitrator', NULL),
	(18, 'Campaign Main', 'campaignmain', 10, 4, '', 0, '1', '2017-03-28 10:07:25', '0000-00-00 00:00:00', 'Adminsitrator', NULL),
	(23, 'Manajemen Donasi', '#', 0, 4, 'fa fa-credit-card', 0, '1', '2017-03-29 16:03:43', '2017-04-10 10:47:54', 'Adminsitrator', 'Adminsitrator'),
	(24, 'Approve Donation', 'aprovedonation', 23, 1, '', 0, '1', '2017-03-29 16:04:18', '2017-03-29 16:07:40', 'Adminsitrator', 'Adminsitrator'),
	(25, 'Approve Program', 'aproveprogram', 23, 2, '', 0, '1', '2017-03-29 16:04:48', '2017-03-29 16:07:57', 'Adminsitrator', 'Adminsitrator'),
	(26, 'Report', 'report', 23, 0, '', 0, '1', '2017-04-04 13:42:55', '2017-04-04 13:50:26', 'Adminsitrator', 'Adminsitrator'),
	(27, 'Halaman Statis', '#', 0, 5, 'fa fa-desktop', 0, '1', '2017-04-10 10:48:31', '2017-04-10 10:51:12', 'Adminsitrator', 'Adminsitrator'),
	(28, 'Jenis Halaman', 'spage_page', 27, 1, '', 0, '1', '2017-04-10 10:50:33', '0000-00-00 00:00:00', 'Adminsitrator', NULL),
	(29, 'Halaman Program', 'spage_program', 27, 0, '', 0, '1', '2017-04-10 12:25:19', '0000-00-00 00:00:00', 'Adminsitrator', NULL),
	(30, 'Halaman Lainnya', 'spage_other', 27, 2, '', 0, '1', '2017-04-10 12:25:58', '0000-00-00 00:00:00', 'Adminsitrator', NULL),
	(31, 'Footer Link', '#', 0, 7, 'fa fa-external-link', 0, '1', '2017-04-11 22:57:49', '2017-04-11 22:59:14', 'Adminsitrator', 'Adminsitrator'),
	(32, 'Tentang Kami', 'link_ttgkm', 31, 1, '', 0, '1', '2017-04-11 22:58:23', '0000-00-00 00:00:00', 'Adminsitrator', NULL),
	(33, 'Program', 'link_program', 31, 2, '', 0, '1', '2017-04-11 22:58:53', '0000-00-00 00:00:00', 'Adminsitrator', NULL);
/*!40000 ALTER TABLE `nb_sys_menus` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_sys_roles
CREATE TABLE IF NOT EXISTS `nb_sys_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `note` text,
  `status` enum('off','on') DEFAULT NULL,
  `crtd_at` datetime NOT NULL,
  `edtd_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `is_delete` enum('true','false') DEFAULT 'false',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_sys_roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `nb_sys_roles` DISABLE KEYS */;
REPLACE INTO `nb_sys_roles` (`id`, `name`, `note`, `status`, `crtd_at`, `edtd_at`, `author`, `last_edit`, `is_active`, `is_delete`) VALUES
	(1, 'Administrator', 'Adminsitratorzz', NULL, '2017-01-11 11:14:34', '2017-04-11 22:59:39', 'Luthfi', 'Adminsitrator', '1', 'false');
/*!40000 ALTER TABLE `nb_sys_roles` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_sys_roles_access
CREATE TABLE IF NOT EXISTS `nb_sys_roles_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `can_add` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_edit` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_delete` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_print` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_aprove` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_import` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_export` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `role_id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=289 DEFAULT CHARSET=utf8 COMMENT='Roles Access';

-- Dumping data for table u7043207_berzakat.nb_sys_roles_access: ~27 rows (approximately)
/*!40000 ALTER TABLE `nb_sys_roles_access` DISABLE KEYS */;
REPLACE INTO `nb_sys_roles_access` (`id`, `can_add`, `can_edit`, `can_delete`, `can_print`, `can_aprove`, `can_import`, `can_export`, `role_id`, `menu_id`) VALUES
	(262, '1', '1', '1', '1', '1', '1', '1', 1, 3),
	(263, '1', '1', '1', '1', '1', '1', '1', 1, 4),
	(264, '1', '1', '1', '1', '1', '1', '1', 1, 5),
	(265, '1', '1', '1', '1', '1', '1', '1', 1, 6),
	(266, '1', '1', '1', '1', '1', '1', '1', 1, 13),
	(267, '1', '1', '1', '1', '1', '1', '1', 1, 7),
	(268, '1', '1', '1', '1', '1', '1', '1', 1, 8),
	(269, '1', '1', '1', '1', '1', '1', '1', 1, 9),
	(270, '1', '1', '1', '1', '1', '1', '1', 1, 10),
	(271, '1', '1', '1', '1', '1', '1', '1', 1, 11),
	(272, '1', '1', '1', '1', '1', '1', '1', 1, 12),
	(273, '1', '1', '1', '1', '1', '1', '1', 1, 17),
	(274, '1', '1', '1', '1', '1', '1', '1', 1, 18),
	(275, '1', '1', '1', '1', '1', '1', '1', 1, 23),
	(276, '1', '1', '1', '1', '1', '1', '1', 1, 26),
	(277, '1', '1', '1', '1', '1', '1', '1', 1, 24),
	(278, '1', '1', '1', '1', '1', '1', '1', 1, 25),
	(279, '1', '1', '1', '1', '1', '1', '1', 1, 27),
	(280, '1', '1', '1', '1', '1', '1', '1', 1, 29),
	(281, '1', '1', '1', '1', '1', '1', '1', 1, 28),
	(282, '1', '1', '1', '1', '1', '1', '1', 1, 30),
	(283, '1', '1', '1', '1', '1', '1', '1', 1, 14),
	(284, '1', '1', '1', '1', '1', '1', '1', 1, 15),
	(285, '1', '1', '1', '1', '1', '1', '1', 1, 16),
	(286, '1', '1', '1', '1', '1', '1', '1', 1, 31),
	(287, '1', '1', '1', '1', '1', '1', '1', 1, 32),
	(288, '1', '1', '1', '1', '1', '1', '1', 1, 33);
/*!40000 ALTER TABLE `nb_sys_roles_access` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_sys_sessions
CREATE TABLE IF NOT EXISTS `nb_sys_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_sys_sessions: ~0 rows (approximately)
/*!40000 ALTER TABLE `nb_sys_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `nb_sys_sessions` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_sys_users
CREATE TABLE IF NOT EXISTS `nb_sys_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `uname` varchar(25) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `nope` int(15) DEFAULT NULL,
  `address` text,
  `img` text NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0:false; 1:true',
  `last_online` datetime DEFAULT NULL,
  `last_login_ip` varchar(100) DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `token_code` varchar(255) DEFAULT NULL,
  `token_gnrtd` datetime DEFAULT NULL,
  `token_valid` datetime DEFAULT NULL,
  `crtd_at` datetime NOT NULL,
  `edtd_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `forgotpass` int(11) DEFAULT NULL,
  `forgotpass_answer` text,
  `fp_token` text NOT NULL,
  `fp_valid` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `nb_sys_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `nb_sys_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_sys_users: ~5 rows (approximately)
/*!40000 ALTER TABLE `nb_sys_users` DISABLE KEYS */;
REPLACE INTO `nb_sys_users` (`id`, `fname`, `uname`, `pwd`, `email`, `nope`, `address`, `img`, `is_active`, `last_online`, `last_login_ip`, `last_login_date`, `token_code`, `token_gnrtd`, `token_valid`, `crtd_at`, `edtd_at`, `author`, `last_edit`, `role_id`, `forgotpass`, `forgotpass_answer`, `fp_token`, `fp_valid`) VALUES
	(1, 'Luthfi Aziz', 'luthfi2', '2c7042a2e8b6f4efdd9968bbb5a00da6479dd4f794', 'developer.luthfi@gmail.com', 2147483647, 'BTN Griya Nugratama', '', '1', '2017-05-16 10:01:44', '36.80.93.130', '2017-05-17 09:26:40', 'ddb7b353b944f063c99e2b8f62450419997ab0dc861d1c1d3dcacbc356802d50', '2017-05-17 09:26:40', '2017-05-17 10:26:40', '2017-01-11 12:34:38', '2017-02-21 18:34:19', '', 'Luthfi Aziz', 1, 1, NULL, '74916ccf5784bde16043ed1dff4b4e03', '2017-05-11 18:55:35'),
	(4, 'Adminsitrator12', 'admin', '2c7042a2e8e09a93105fc9d6c07310e6913450811c', 'abdulhadi.endra@gmail.com', 2147483647, 'administrators120', 'image_20170414050203.png', '1', '2017-05-23 18:21:27', '36.79.250.240', '2017-05-23 18:20:41', '22a33921e0ce0e489bba5e3636ff03e5b69b8fa2e71cf77e5410ecdbc8e66463', '2017-05-23 18:20:41', '2017-05-23 19:20:41', '2017-03-13 15:46:38', '2017-04-04 21:26:14', 'Luthfi Aziz', 'Adminsitrator', 1, NULL, NULL, 'cc1db834912f4b3637084f92d1e0c41e', '2017-05-23 18:15:32'),
	(5, 'Endraaa', 'endra', '2c7042a2e8b6f4efdd9968bbb5a00da6479dd4f794', 'endra@niatbaik.co.id', 12345, '', 'image_20170414050601.jpg', '1', '2017-04-14 16:49:59', '36.72.124.191', '2017-05-11 11:25:56', '0bacb69df769bb76390ebbe8d244a329d47f012205494ea7416e796a0a07f733', '2017-05-11 11:25:56', '2017-05-11 12:25:56', '2017-03-29 07:12:39', '2017-03-29 07:20:04', 'Adminsitrator', 'Adminsitrator', 1, NULL, NULL, '', '0000-00-00 00:00:00'),
	(6, 'nurul', 'nurul', '2c7042a2e8b6f4efdd9968bbb5a00da6479dd4f794', 'nurul@niatbaik.co.id', 2147483647, 'permata', '', '0', NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-03 14:45:12', '2017-04-03 14:45:12', 'Luthfi Aziz', NULL, 1, NULL, NULL, '', '0000-00-00 00:00:00'),
	(7, 'Dini', 'dini', '2c7042a2e8454b57e9ed27c325e2a017feeef15ebe', 'dini@suaramuslim.net', 2147483647, '', '', '1', '2017-04-21 13:39:54', '103.213.130.252', '2017-04-21 13:35:27', '4bacd04417f125383b831c89ee1c726548972dea2b9a012808bcd338a5f60cad', '2017-04-21 13:35:27', '2017-04-21 14:35:27', '2017-04-21 11:18:29', '2017-04-21 11:18:29', 'Luthfi Aziz', NULL, 1, NULL, NULL, '', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `nb_sys_users` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_trans_donation
CREATE TABLE IF NOT EXISTS `nb_trans_donation` (
  `no_transaksi` varchar(255) NOT NULL COMMENT 'no transaksi ',
  `no_rekening` varchar(50) NOT NULL,
  `nominal` decimal(20,0) NOT NULL COMMENT 'nilai donasi yang dimasukin',
  `trx_date` date NOT NULL,
  `month` int(2) NOT NULL,
  `year` int(4) NOT NULL,
  `campaign` int(11) NOT NULL,
  `donatur` int(11) NOT NULL,
  `rekening` int(11) NOT NULL,
  `bank` int(11) DEFAULT NULL COMMENT 'kode bank dari table bank',
  `comment` text COMMENT 'komentar donasi payment',
  `uniqcode` decimal(20,0) DEFAULT NULL COMMENT 'kode unique',
  `status` enum('verified','unverified','cancel') DEFAULT 'unverified',
  `crtd_at` datetime DEFAULT NULL,
  `edtd_at` datetime DEFAULT NULL,
  `namaDonatur` varchar(255) DEFAULT NULL,
  `emailDonatur` varchar(255) DEFAULT NULL,
  `telpDonatur` varchar(255) DEFAULT NULL,
  `validDate` date DEFAULT NULL,
  `nomuniq` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`no_transaksi`),
  KEY `bank` (`bank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_trans_donation: ~6 rows (approximately)
/*!40000 ALTER TABLE `nb_trans_donation` DISABLE KEYS */;
REPLACE INTO `nb_trans_donation` (`no_transaksi`, `no_rekening`, `nominal`, `trx_date`, `month`, `year`, `campaign`, `donatur`, `rekening`, `bank`, `comment`, `uniqcode`, `status`, `crtd_at`, `edtd_at`, `namaDonatur`, `emailDonatur`, `telpDonatur`, `validDate`, `nomuniq`) VALUES
	('2017041300002', '', 150000, '2017-04-13', 4, 2017, 8, 2, 0, 1, 'test campaign', 610, 'unverified', '2017-04-13 17:48:03', '2017-04-13 17:48:03', 'fahmi hilmansyah', 'fahmi.hilmansyah@gmail.com', '081238213232', '2017-04-15', 150610),
	('2017041300003', '', 250000, '2017-04-13', 4, 2017, 8, 2, 0, 1, '250k', 611, 'verified', '2017-04-13 17:48:59', '2017-04-13 17:59:04', 'fahmi hilmansyah', 'fahmi.hilmansyah@gmail.com', '0843323123123', '2017-04-15', 250611),
	('2017041700001', '', 200000, '2017-04-17', 4, 2017, 8, 9, 0, 1, '', 612, 'verified', '2017-04-17 16:27:02', '2017-04-17 16:30:36', 'Endra', 'endraabdulhadi@yahoo.com', '089666905702', '2017-04-19', 200612),
	('2017041800001', '', 50000, '2017-04-18', 4, 2017, 8, 20, 0, 1, 'yaho', 613, 'unverified', '2017-04-18 15:58:14', '2017-04-18 15:58:14', 'Rururu', 'ripfly71stone@gmail.com', '08571234567890', '2017-04-20', 50613),
	('2017041900001', '', 100000, '2017-04-19', 4, 2017, 8, 0, 0, 5, 'as', 614, 'cancel', '2017-04-19 14:20:41', '2017-05-12 17:02:04', 'Ruurururuu', 'ryan@niatbaik.co.id', '129728591856', '2017-04-21', 100614),
	('2017051700001', '', 400000, '2017-05-17', 5, 2017, 8, 48, 0, 1, '', 625, 'verified', '2017-05-17 09:22:03', '2017-05-17 09:27:54', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085931235677', '2017-05-19', 400625),
	('2017052300001', '', 100000, '2017-05-23', 5, 2017, 8, 0, 0, 1, '', 637, 'unverified', '2017-05-23 17:10:40', '2017-05-23 17:10:40', 'Hamba Allah', 'nurulfbre@gmail.com', '08976547875', '2017-05-25', 100637),
	('2017053000001', '', 35000, '2017-05-30', 5, 2017, 8, 0, 0, 2, 'kereners', 640, 'unverified', '2017-05-30 12:29:04', '2017-05-30 12:29:04', 'Fahmi Inayatulloh', 'fahmiinayatulloh@gmail.com', '081293407183', '2017-06-01', 35640),
	('2017053000002', '', 32000, '2017-05-30', 5, 2017, 8, 63, 0, 4, '', 641, 'unverified', '2017-05-30 13:50:44', '2017-05-30 13:50:44', 'Ydsf', 'infoydsfjakarta@gmail.com', '081385888100', '2017-06-01', 32641);
/*!40000 ALTER TABLE `nb_trans_donation` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_trans_konfirmasi
CREATE TABLE IF NOT EXISTS `nb_trans_konfirmasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe` enum('camp','prog') DEFAULT NULL,
  `img` text,
  `konf_date` datetime DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `atas_nama` varchar(100) DEFAULT NULL,
  `norek_asal` varchar(50) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  `donatur` int(255) DEFAULT NULL,
  `transaksi` bigint(11) DEFAULT NULL,
  `status` enum('verified','unverified','cancel') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table u7043207_berzakat.nb_trans_konfirmasi: ~8 rows (approximately)
/*!40000 ALTER TABLE `nb_trans_konfirmasi` DISABLE KEYS */;
REPLACE INTO `nb_trans_konfirmasi` (`id`, `tipe`, `img`, `konf_date`, `bank`, `atas_nama`, `norek_asal`, `nominal`, `donatur`, `transaksi`, `status`) VALUES
	(5, 'camp', NULL, '2017-04-06 19:44:54', 1, 'Luthfi', '7000000222', 200000, 5, 2017040500027, NULL),
	(8, 'camp', NULL, '2017-04-10 11:33:31', 1, 'Bolololo', '1234232321', 75000, 2, 2017040600016, NULL),
	(9, 'prog', NULL, '2017-04-13 14:51:34', 1, 'Fahmi Ganteng', '12345678888', 100000, 2, 2017041300001, NULL),
	(10, 'camp', NULL, '2017-04-13 17:49:48', 1, 'si siuk', '091230293222', 251000, 2, 2017041300003, NULL),
	(11, 'camp', NULL, '2017-04-17 16:27:55', 1, 'andrea', '123456780', 200612, 9, 2017041700001, NULL),
	(12, '', NULL, '2017-05-11 16:50:11', 2, 'Andre', '872781829929187', 1000620, 53, 2017051100006, NULL),
	(13, '', NULL, '2017-05-16 18:43:28', 2, 'Fahmi Hilmansyah', '99299198321', 50624, 48, 2017051600001, NULL),
	(14, 'camp', NULL, '2017-05-17 09:27:39', 1, 'Fahmi Hilmansyah', '7667678687678', 400625, 48, 2017051700001, NULL);
/*!40000 ALTER TABLE `nb_trans_konfirmasi` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_trans_point
CREATE TABLE IF NOT EXISTS `nb_trans_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(255) DEFAULT NULL,
  `value` int(11) NOT NULL,
  `date` date NOT NULL,
  `month` int(2) NOT NULL,
  `year` int(4) NOT NULL,
  `donatur` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_trans_point: ~0 rows (approximately)
/*!40000 ALTER TABLE `nb_trans_point` DISABLE KEYS */;
/*!40000 ALTER TABLE `nb_trans_point` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_trans_program
CREATE TABLE IF NOT EXISTS `nb_trans_program` (
  `no_transaksi` varchar(255) NOT NULL COMMENT 'no transaksi ',
  `no_rekening` varchar(50) NOT NULL,
  `nominal` decimal(20,0) NOT NULL COMMENT 'nilai donasi yang dimasukin',
  `trx_date` date NOT NULL,
  `month` int(2) NOT NULL,
  `year` int(4) NOT NULL,
  `jenistrx` varchar(50) NOT NULL,
  `donatur` int(11) NOT NULL,
  `rekening` int(11) NOT NULL,
  `bank` int(11) DEFAULT NULL COMMENT 'kode bank dari table bank',
  `comment` text COMMENT 'komentar donasi payment',
  `uniqcode` decimal(20,0) DEFAULT NULL COMMENT 'kode unique',
  `status` enum('verified','unverified','cancel') DEFAULT 'unverified',
  `crtd_at` datetime DEFAULT NULL,
  `edtd_at` datetime DEFAULT NULL,
  `namaDonatur` varchar(255) DEFAULT NULL,
  `emailDonatur` varchar(255) DEFAULT NULL,
  `telpDonatur` varchar(255) DEFAULT NULL,
  `validDate` date DEFAULT NULL,
  `nomuniq` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`no_transaksi`),
  KEY `bank` (`bank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_trans_program: ~22 rows (approximately)
/*!40000 ALTER TABLE `nb_trans_program` DISABLE KEYS */;
REPLACE INTO `nb_trans_program` (`no_transaksi`, `no_rekening`, `nominal`, `trx_date`, `month`, `year`, `jenistrx`, `donatur`, `rekening`, `bank`, `comment`, `uniqcode`, `status`, `crtd_at`, `edtd_at`, `namaDonatur`, `emailDonatur`, `telpDonatur`, `validDate`, `nomuniq`) VALUES
	('2017041300001', '', 100000, '2017-04-13', 4, 2017, 'shadaqah', 2, 0, 1, 'asdsa', 609, 'verified', '2017-04-13 14:50:09', '2017-04-13 17:58:56', 'fahmi hilmansyah', 'fahmi.hilmansyah@gmail.com', '08237273273', '2017-04-15', 100609),
	('2017042700001', '', 200000, '2017-04-27', 4, 2017, 'deposit', 22, 0, 1, '', 615, 'cancel', '2017-04-27 12:03:52', '2017-05-12 15:41:52', 'Nurul Fadilah', 'nurulfbre@gmail.com', '087654324567', '2017-04-29', 200615),
	('2017051100001', '', 210000, '2017-05-11', 5, 2017, 'zakatmaal', 0, 0, 1, '', 616, 'cancel', '2017-05-11 11:29:49', '2017-05-12 15:41:56', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '708123231', '2017-05-13', 210616),
	('2017051100002', '', 1200000, '2017-05-11', 5, 2017, 'zakatmaal', 48, 0, 1, 'dasda', 1017, 'cancel', '2017-05-11 11:34:06', '2017-05-12 15:42:01', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '081237232233', '2017-05-13', 1201017),
	('2017051100003', '', 30000, '2017-05-11', 5, 2017, 'shadaqah', 0, 0, 1, '', 617, 'unverified', '2017-05-11 15:09:49', '2017-05-11 15:09:49', 'Hamba Allah', 'nn@gmail.com', '083746282374', '2017-05-13', 30617),
	('2017051100005', '', 30000, '2017-05-11', 5, 2017, 'shadaqah', 49, 0, 1, '', 619, 'unverified', '2017-05-11 15:11:25', '2017-05-11 15:11:25', 'Luthfi', 'luthfi.an.25mei1991@gmail.com', '085931235677', '2017-05-13', 30619),
	('2017051100006', '', 1000000, '2017-05-11', 5, 2017, 'deposit', 53, 0, 1, '', 620, 'verified', '2017-05-11 16:47:59', '2017-05-11 16:57:19', 'andre', '3ndr4ah@gmail.com', '0857266177280', '2017-05-13', 1000620),
	('2017051200001', '', 30000, '2017-05-12', 5, 2017, 'infaq', 0, 0, 2, '', 621, 'unverified', '2017-05-12 10:54:43', '2017-05-12 10:54:43', 'Hamba Allah', 'nn@gmail.com', '086839847538', '2017-05-14', 30621),
	('2017051200002', '', 30000, '2017-05-12', 5, 2017, 'shadaqah', 0, 0, 1, '', 622, 'unverified', '2017-05-12 10:56:42', '2017-05-12 10:56:42', 'Hamba Allah', 'nn@gmail.com', '08765433457', '2017-05-14', 30622),
	('2017051200003', '', 50000, '2017-05-12', 5, 2017, 'deposit', 55, 0, 1, '', 623, 'unverified', '2017-05-12 11:46:34', '2017-05-12 11:46:34', 'nurul', 'nurulfadilah84@gmail.com', '089765432112', '2017-05-14', 50623),
	('2017051600001', '', 50000, '2017-05-16', 5, 2017, 'deposit', 48, 0, 2, 'send from mobile', 624, 'verified', '2017-05-16 18:41:48', '2017-05-16 18:43:44', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-18', 50624),
	('2017051900001', '', 50000, '2017-05-19', 5, 2017, 'deposit', 48, 0, 1, 'send from mobile', 626, 'unverified', '2017-05-19 15:44:51', '2017-05-19 15:44:51', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-21', 50626),
	('2017051900002', '', 50000, '2017-05-19', 5, 2017, 'deposit', 48, 0, 1, 'send from mobile', 627, 'unverified', '2017-05-19 15:45:55', '2017-05-19 15:45:55', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-21', 50627),
	('2017051900003', '', 50000, '2017-05-19', 5, 2017, 'deposit', 48, 0, 2, 'send from mobile', 628, 'unverified', '2017-05-19 15:47:15', '2017-05-19 15:47:15', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-21', 50628),
	('2017051900004', '', 50000, '2017-05-19', 5, 2017, 'deposit', 48, 0, 2, 'send from mobile', 629, 'unverified', '2017-05-19 15:48:11', '2017-05-19 15:48:11', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-21', 50629),
	('2017052200001', '', 50000, '2017-05-22', 5, 2017, 'deposit', 48, 0, 2, 'send from mobile', 630, 'unverified', '2017-05-22 10:44:49', '2017-05-22 10:44:49', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-24', 50630),
	('2017052200002', '', 50000, '2017-05-22', 5, 2017, 'deposit', 48, 0, 2, 'send from mobile', 631, 'unverified', '2017-05-22 12:36:07', '2017-05-22 12:36:07', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-24', 50631),
	('2017052200003', '', 50000, '2017-05-22', 5, 2017, 'deposit', 48, 0, 1, 'send from mobile', 632, 'unverified', '2017-05-22 13:28:04', '2017-05-22 13:28:04', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-24', 50632),
	('2017052200004', '', 50000, '2017-05-22', 5, 2017, 'deposit', 48, 0, 1, 'send from mobile', 633, 'unverified', '2017-05-22 13:43:22', '2017-05-22 13:43:22', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-24', 50633),
	('2017052200005', '', 50000, '2017-05-22', 5, 2017, 'deposit', 48, 0, 2, 'send from mobile', 634, 'unverified', '2017-05-22 13:43:43', '2017-05-22 13:43:43', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-24', 50634),
	('2017052200006', '', 50000, '2017-05-22', 5, 2017, 'deposit', 48, 0, 1, 'send from mobile', 635, 'unverified', '2017-05-22 13:56:54', '2017-05-22 13:56:54', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-24', 50635),
	('2017052200007', '', 50000, '2017-05-22', 5, 2017, 'deposit', 48, 0, 1, 'send from mobile', 636, 'unverified', '2017-05-22 13:57:15', '2017-05-22 13:57:15', 'Fahmi Hilmansyah', 'luthfiaziznugraha@gmail.com', '085721547448', '2017-05-24', 50636),
	('2017052300002', '', 100000, '2017-05-23', 5, 2017, 'shadaqah', 0, 0, 1, '', 638, 'unverified', '2017-05-23 17:13:29', '2017-05-23 17:13:29', 'Hamba Allah', 'nurulfbre@gmail.com', '086756774567', '2017-05-25', 100638),
	('2017052300003', '', 100000, '2017-05-23', 5, 2017, 'infaq', 0, 0, 1, '', 639, 'unverified', '2017-05-23 17:16:33', '2017-05-23 17:16:33', 'Hamba Allah', 'nurulfbre@gmail.com', '085387872493', '2017-05-25', 100639);
/*!40000 ALTER TABLE `nb_trans_program` ENABLE KEYS */;

-- Dumping structure for table u7043207_berzakat.nb_unique_code
CREATE TABLE IF NOT EXISTS `nb_unique_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` double(255,0) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table u7043207_berzakat.nb_unique_code: ~0 rows (approximately)
/*!40000 ALTER TABLE `nb_unique_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `nb_unique_code` ENABLE KEYS */;

-- Dumping structure for view u7043207_berzakat.nb_view_users
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_view_users` (
	`id` INT(10) UNSIGNED NOT NULL,
	`fname` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`uname` VARCHAR(25) NOT NULL COLLATE 'utf8_general_ci',
	`pwd` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`email` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`nope` INT(15) NULL,
	`address` TEXT NULL COLLATE 'utf8_general_ci',
	`is_active` ENUM('0','1') NOT NULL COMMENT '0:false; 1:true' COLLATE 'utf8_general_ci',
	`last_online` DATETIME NULL,
	`last_login_ip` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`last_login_date` DATETIME NULL,
	`token_code` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`token_gnrtd` DATETIME NULL,
	`token_valid` DATETIME NULL,
	`crtd_at` DATETIME NOT NULL,
	`edtd_at` TIMESTAMP NOT NULL,
	`author` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`last_edit` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`role_id` INT(10) UNSIGNED NOT NULL,
	`forgotpass` INT(11) NULL,
	`forgotpass_answer` TEXT NULL COLLATE 'utf8_general_ci',
	`role_name` VARCHAR(100) NOT NULL COLLATE 'utf8_general_ci',
	`role_active` ENUM('0','1') NULL COMMENT '0:false; 1:true' COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_artikel_detail
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_artikel_detail` (
	`id` INT(11) NOT NULL,
	`title` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`desc` TEXT NULL COLLATE 'utf8_general_ci',
	`post_date` DATE NULL,
	`img` TEXT NULL COLLATE 'utf8_general_ci',
	`name_userpost` VARCHAR(50) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_artikel_list
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_artikel_list` (
	`id` INT(11) NOT NULL,
	`title` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`slug` VARCHAR(150) NOT NULL COLLATE 'utf8_general_ci',
	`desc_short` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`post_date` DATE NULL,
	`img` TEXT NULL COLLATE 'utf8_general_ci',
	`name_userpost` VARCHAR(50) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_artikel_list_mp
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_artikel_list_mp` (
	`id` INT(11) NOT NULL,
	`title` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`slug` VARCHAR(150) NOT NULL COLLATE 'utf8_general_ci',
	`desc_short` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`post_date` DATE NULL,
	`img` TEXT NULL COLLATE 'utf8_general_ci',
	`name_userpost` VARCHAR(50) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_campaign_detail
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_campaign_detail` (
	`id` INT(11) NOT NULL,
	`title` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`desc` TEXT NULL COLLATE 'utf8_general_ci',
	`update_progress` TEXT NOT NULL COLLATE 'utf8_general_ci',
	`sum_donatur` BIGINT(11) NOT NULL,
	`target` INT(11) NULL COMMENT 'target donation from campaign users',
	`now` INT(11) NULL COMMENT 'summary donation until now',
	`img` TEXT NULL COLLATE 'utf8_general_ci',
	`user_type` ENUM('ad','do') NULL COMMENT 'ad: administrator; do: donatur' COLLATE 'utf8_general_ci',
	`valid_date` DATE NULL,
	`name_userpost` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`selisih_validate` INT(7) NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_campaign_list
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_campaign_list` (
	`id` INT(11) NOT NULL,
	`title` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`slug` VARCHAR(150) NOT NULL COLLATE 'utf8_general_ci',
	`desc_short` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`target` INT(11) NULL COMMENT 'target donation from campaign users',
	`now` INT(11) NULL COMMENT 'summary donation until now',
	`img` TEXT NULL COLLATE 'utf8_general_ci',
	`user_type` ENUM('ad','do') NULL COMMENT 'ad: administrator; do: donatur' COLLATE 'utf8_general_ci',
	`valid_date` DATE NULL,
	`name_userpost` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`selisih_validate` INT(7) NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_campaign_list_mp
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_campaign_list_mp` (
	`id` INT(11) NOT NULL,
	`title` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`slug` VARCHAR(150) NOT NULL COLLATE 'utf8_general_ci',
	`desc_short` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`post_date` DATETIME NULL,
	`img` TEXT NULL COLLATE 'utf8_general_ci',
	`name_userpost` VARCHAR(50) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_campaign_main
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_campaign_main` (
	`id` INT(11) NOT NULL,
	`title` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`slug` VARCHAR(150) NOT NULL COLLATE 'utf8_general_ci',
	`desc_short` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`target` INT(11) NULL COMMENT 'target donation from campaign users',
	`now` INT(11) NULL COMMENT 'summary donation until now',
	`img` TEXT NULL COLLATE 'utf8_general_ci',
	`user_type` ENUM('ad','do') NULL COMMENT 'ad: administrator; do: donatur' COLLATE 'utf8_general_ci',
	`valid_date` DATE NULL,
	`name_userpost` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`selisih_validate` INT(7) NULL
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_campaign_sliders
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_campaign_sliders` (
	`id` INT(11) NOT NULL,
	`img` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`title` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`desc` TEXT NULL COLLATE 'latin1_swedish_ci',
	`type` ENUM('1','2') NOT NULL COLLATE 'latin1_swedish_ci',
	`link` TEXT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_code_confirmation
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_code_confirmation` (
	`email` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`ccode` INT(5) NULL,
	`ccode_valid` DATETIME NULL
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_donaturs_data
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_donaturs_data` (
	`id` INT(11) NOT NULL,
	`uname` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`pwd` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`email` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`last_online` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`last_login_ip` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`last_login_date` DATETIME NULL,
	`login_type` ENUM('default','oauth') NULL COLLATE 'utf8_general_ci',
	`is_activate` ENUM('true','false','') NULL COLLATE 'utf8_general_ci',
	`activated` ENUM('active','not-confirm') NULL COLLATE 'utf8_general_ci',
	`crtd_at` DATE NULL,
	`edtd_at` DATE NULL,
	`author` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`last_edit` VARCHAR(50) NULL COMMENT 'last author' COLLATE 'utf8_general_ci',
	`forgotpass` INT(11) NULL,
	`token_aktifasi` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`oauth_provider` VARCHAR(150) NULL COLLATE 'utf8_general_ci',
	`oauth_uid` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`profile_url` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`picture_url` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`fname` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`lname` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`sex` ENUM('male','female') NULL COLLATE 'utf8_general_ci',
	`date_birth` DATE NULL,
	`address` TEXT NULL COLLATE 'utf8_general_ci',
	`address_alt` TEXT NULL COLLATE 'utf8_general_ci',
	`phone` VARCHAR(15) NULL COLLATE 'utf8_general_ci',
	`donatur` INT(11) NOT NULL,
	`info` TEXT NULL COLLATE 'utf8_general_ci',
	`img` TEXT NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_donatur_oauth
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_donatur_oauth` (
	`id` INT(11) NOT NULL,
	`uname` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`pwd` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`email` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`last_online` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`last_login_ip` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`last_login_date` DATETIME NULL,
	`login_type` ENUM('default','oauth') NULL COLLATE 'utf8_general_ci',
	`is_activate` ENUM('true','false','') NULL COLLATE 'utf8_general_ci',
	`activated` ENUM('active','not-confirm') NULL COLLATE 'utf8_general_ci',
	`crtd_at` DATE NULL,
	`edtd_at` DATE NULL,
	`author` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`last_edit` VARCHAR(50) NULL COMMENT 'last author' COLLATE 'utf8_general_ci',
	`forgotpass` INT(11) NULL,
	`token_aktifasi` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`oauth_provider` VARCHAR(150) NULL COLLATE 'utf8_general_ci',
	`oauth_uid` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`profile_url` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`picture_url` VARCHAR(255) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_footer_program
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_footer_program` (
	`id` INT(11) NOT NULL,
	`name` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`link` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`type` ENUM('static','other') NULL COLLATE 'utf8_general_ci',
	`ctg` ENUM('ttgkm','prgm') NULL COLLATE 'utf8_general_ci',
	`is_active` ENUM('0','1') NULL COLLATE 'utf8_general_ci',
	`crtd_at` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`edtd_at` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`author` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`last_edit` VARCHAR(255) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_footer_ttgkm
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_footer_ttgkm` (
	`id` INT(11) NOT NULL,
	`name` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`link` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`type` ENUM('static','other') NULL COLLATE 'utf8_general_ci',
	`ctg` ENUM('ttgkm','prgm') NULL COLLATE 'utf8_general_ci',
	`is_active` ENUM('0','1') NULL COLLATE 'utf8_general_ci',
	`crtd_at` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`edtd_at` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`author` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`last_edit` VARCHAR(255) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_historydmpt
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_historydmpt` (
	`no_transaksi` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`trx_date` DATETIME NULL,
	`donatur` INT(11) NOT NULL,
	`jenistrx` VARCHAR(5) NULL COLLATE 'latin1_swedish_ci',
	`amount` DECIMAL(20,0) NULL,
	`bank` INT(11) NULL,
	`status` VARCHAR(10) NULL COLLATE 'utf8_general_ci',
	`jenistrans` VARCHAR(7) NOT NULL COLLATE 'utf8mb4_general_ci',
	`tipetrans` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`tipetrans_id` VARCHAR(11) NOT NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_historytrx
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_historytrx` (
	`no_transaksi` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`jenistrx` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`trx_date` DATE NOT NULL,
	`donatur` INT(11) NOT NULL,
	`status` VARCHAR(10) NULL COLLATE 'utf8_general_ci',
	`nomuniq` DECIMAL(20,0) NULL,
	`campaign` VARCHAR(11) NOT NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_historytrx_u
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_historytrx_u` (
	`no_transaksi` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`jenistrx` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`trx_date` DATE NOT NULL,
	`donatur` INT(11) NOT NULL,
	`status` VARCHAR(10) NULL COLLATE 'utf8_general_ci',
	`nomuniq` DECIMAL(20,0) NULL,
	`campaign` VARCHAR(11) NOT NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_post_list
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_post_list` (
	`id` INT(11) NOT NULL,
	`title` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`slug` VARCHAR(150) NOT NULL COLLATE 'utf8_general_ci',
	`img` TEXT NULL COLLATE 'utf8_general_ci',
	`desc_short` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`post_date` DATE NULL,
	`edtd_at` DATE NULL,
	`sum_comment` BIGINT(11) NOT NULL,
	`sum_read` BIGINT(11) NOT NULL,
	`sum_share` BIGINT(11) NOT NULL,
	`categories` INT(11) NULL,
	`categories_name` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`author` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`last_edit` VARCHAR(50) NULL COMMENT 'last author' COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_v_spage_other
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `nb_v_spage_other` (
	`id` INT(11) NOT NULL,
	`title` VARCHAR(150) NULL COLLATE 'utf8_general_ci',
	`stitle` VARCHAR(150) NULL COLLATE 'utf8_general_ci',
	`desc` TEXT NULL COLLATE 'utf8_general_ci',
	`img` TEXT NULL COLLATE 'utf8_general_ci',
	`is_active` ENUM('0','1') NULL COMMENT '0:false; 1:true' COLLATE 'utf8_general_ci',
	`crtd_at` DATE NULL,
	`edtd_at` DATE NULL,
	`author` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`page` INT(11) NULL,
	`name_key` VARCHAR(150) NULL COLLATE 'utf8_general_ci',
	`name` VARCHAR(150) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view u7043207_berzakat.nb_view_users
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_view_users`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_view_users` AS select `su`.`id` AS `id`,`su`.`fname` AS `fname`,`su`.`uname` AS `uname`,`su`.`pwd` AS `pwd`,`su`.`email` AS `email`,`su`.`nope` AS `nope`,`su`.`address` AS `address`,`su`.`is_active` AS `is_active`,`su`.`last_online` AS `last_online`,`su`.`last_login_ip` AS `last_login_ip`,`su`.`last_login_date` AS `last_login_date`,`su`.`token_code` AS `token_code`,`su`.`token_gnrtd` AS `token_gnrtd`,`su`.`token_valid` AS `token_valid`,`su`.`crtd_at` AS `crtd_at`,`su`.`edtd_at` AS `edtd_at`,`su`.`author` AS `author`,`su`.`last_edit` AS `last_edit`,`su`.`role_id` AS `role_id`,`su`.`forgotpass` AS `forgotpass`,`su`.`forgotpass_answer` AS `forgotpass_answer`,`sr`.`name` AS `role_name`,`sr`.`is_active` AS `role_active` from (`nb_sys_users` `su` join `nb_sys_roles` `sr` on((`sr`.`id` = `su`.`role_id`)));

-- Dumping structure for view u7043207_berzakat.nb_v_artikel_detail
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_artikel_detail`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_artikel_detail` AS select `np`.`id` AS `id`,`np`.`title` AS `title`,`np`.`desc` AS `desc`,`np`.`post_date` AS `post_date`,`np`.`img` AS `img`,concat(`np`.`author`) AS `name_userpost` from (`nb_post` `np` join `nb_post_categories` `npc` on((`npc`.`id` = `np`.`categories`))) where (`np`.`is_active` = '1') order by `np`.`id` desc;

-- Dumping structure for view u7043207_berzakat.nb_v_artikel_list
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_artikel_list`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_artikel_list` AS select `np`.`id` AS `id`,`np`.`title` AS `title`,`np`.`slug` AS `slug`,`np`.`desc_short` AS `desc_short`,`np`.`post_date` AS `post_date`,`np`.`img` AS `img`,concat(`np`.`author`) AS `name_userpost` from (`nb_post` `np` join `nb_post_categories` `npc` on((`npc`.`id` = `np`.`categories`))) where (`np`.`is_active` = '1') order by `np`.`id` desc;

-- Dumping structure for view u7043207_berzakat.nb_v_artikel_list_mp
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_artikel_list_mp`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_artikel_list_mp` AS select `np`.`id` AS `id`,`np`.`title` AS `title`,`np`.`slug` AS `slug`,`np`.`desc_short` AS `desc_short`,`np`.`post_date` AS `post_date`,`np`.`img` AS `img`,concat(`np`.`author`) AS `name_userpost` from (`nb_post` `np` join `nb_post_categories` `npc` on((`npc`.`id` = `np`.`categories`))) where (`np`.`is_active` = '1') order by `np`.`sum_read` desc limit 4;

-- Dumping structure for view u7043207_berzakat.nb_v_campaign_detail
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_campaign_detail`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_campaign_detail` AS select `nc`.`id` AS `id`,`nc`.`title` AS `title`,`nc`.`desc` AS `desc`,`nc`.`update_progress` AS `update_progress`,ifnull(`nc`.`sum_donatur`,0) AS `sum_donatur`,`nc`.`target` AS `target`,`nc`.`now` AS `now`,`nc`.`img` AS `img`,`nc`.`user_type` AS `user_type`,`nc`.`valid_date` AS `valid_date`,concat(`nc`.`author`) AS `name_userpost`,if((ifnull((to_days(`nc`.`valid_date`) - to_days(curdate())),0) <= 0),0,ifnull((to_days(`nc`.`valid_date`) - to_days(curdate())),0)) AS `selisih_validate` from (`nb_campaign` `nc` join `nb_campaign_categories` `ncc` on((`ncc`.`id` = `nc`.`categories`))) where ((`nc`.`is_active` = '1') and (`nc`.`user_type` = 'ad')) order by `nc`.`id` desc;

-- Dumping structure for view u7043207_berzakat.nb_v_campaign_list
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_campaign_list`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_campaign_list` AS select `nc`.`id` AS `id`,`nc`.`title` AS `title`,`nc`.`slug` AS `slug`,`nc`.`desc_short` AS `desc_short`,`nc`.`target` AS `target`,`nc`.`now` AS `now`,`nc`.`img` AS `img`,`nc`.`user_type` AS `user_type`,`nc`.`valid_date` AS `valid_date`,concat(`nc`.`author`) AS `name_userpost`,if((ifnull((to_days(`nc`.`valid_date`) - to_days(curdate())),0) <= 0),0,ifnull((to_days(`nc`.`valid_date`) - to_days(curdate())),0)) AS `selisih_validate` from (`nb_campaign` `nc` join `nb_campaign_categories` `ncc` on((`ncc`.`id` = `nc`.`categories`))) where ((`nc`.`is_active` = '1') and (`nc`.`user_type` = 'ad')) order by `nc`.`id` desc;

-- Dumping structure for view u7043207_berzakat.nb_v_campaign_list_mp
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_campaign_list_mp`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_campaign_list_mp` AS select `nc`.`id` AS `id`,`nc`.`title` AS `title`,`nc`.`slug` AS `slug`,`nc`.`desc_short` AS `desc_short`,`nc`.`post_date` AS `post_date`,`nc`.`img` AS `img`,concat(`nc`.`author`) AS `name_userpost` from (`nb_campaign` `nc` join `nb_campaign_categories` `ncc` on((`ncc`.`id` = `nc`.`categories`))) where (`nc`.`is_active` = '1') order by `nc`.`sum_share` desc limit 3;

-- Dumping structure for view u7043207_berzakat.nb_v_campaign_main
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_campaign_main`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_campaign_main` AS select `c`.`id` AS `id`,`c`.`title` AS `title`,`c`.`slug` AS `slug`,`c`.`desc_short` AS `desc_short`,`c`.`target` AS `target`,`c`.`now` AS `now`,`c`.`img` AS `img`,`c`.`user_type` AS `user_type`,`c`.`valid_date` AS `valid_date`,concat(`c`.`author`) AS `name_userpost`,(to_days(`c`.`valid_date`) - to_days(curdate())) AS `selisih_validate` from (`nb_campaign_main` `m` join `nb_campaign` `c` on((`c`.`id` = `m`.`id_campaign`))) where ((`m`.`status` = '1') and (`c`.`is_active` = '1') and (`c`.`user_type` = 'ad'));

-- Dumping structure for view u7043207_berzakat.nb_v_campaign_sliders
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_campaign_sliders`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_campaign_sliders` AS select `cs`.`id` AS `id`,`cs`.`img` AS `img`,`cs`.`title` AS `title`,`cs`.`desc` AS `desc`,`cs`.`type` AS `type`,`cs`.`link` AS `link` from `nb_campaign_slide` `cs`;

-- Dumping structure for view u7043207_berzakat.nb_v_code_confirmation
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_code_confirmation`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_code_confirmation` AS select `d`.`email` AS `email`,`c`.`ccode` AS `ccode`,`c`.`ccode_valid` AS `ccode_valid` from (`nb_donaturs_mba` `c` join `nb_donaturs` `d` on((`d`.`id` = `c`.`donatur`))) where (`c`.`ccode_status` = '1');

-- Dumping structure for view u7043207_berzakat.nb_v_donaturs_data
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_donaturs_data`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_donaturs_data` AS select `nd`.`id` AS `id`,`nd`.`uname` AS `uname`,`nd`.`pwd` AS `pwd`,`nd`.`email` AS `email`,`nd`.`last_online` AS `last_online`,`nd`.`last_login_ip` AS `last_login_ip`,`nd`.`last_login_date` AS `last_login_date`,`nd`.`login_type` AS `login_type`,`nd`.`is_activate` AS `is_activate`,`nd`.`activated` AS `activated`,`nd`.`crtd_at` AS `crtd_at`,`nd`.`edtd_at` AS `edtd_at`,`nd`.`author` AS `author`,`nd`.`last_edit` AS `last_edit`,`nd`.`forgotpass` AS `forgotpass`,`nd`.`token_aktifasi` AS `token_aktifasi`,`nd`.`oauth_provider` AS `oauth_provider`,`nd`.`oauth_uid` AS `oauth_uid`,`nd`.`profile_url` AS `profile_url`,`nd`.`picture_url` AS `picture_url`,`ndd`.`fname` AS `fname`,`ndd`.`lname` AS `lname`,`ndd`.`sex` AS `sex`,`ndd`.`date_birth` AS `date_birth`,`ndd`.`address` AS `address`,`ndd`.`address_alt` AS `address_alt`,`ndd`.`phone` AS `phone`,`ndd`.`donatur` AS `donatur`,`ndd`.`info` AS `info`,`ndd`.`img` AS `img` from (`nb_donaturs` `nd` join `nb_donaturs_d` `ndd` on((`ndd`.`donatur` = `nd`.`id`)));

-- Dumping structure for view u7043207_berzakat.nb_v_donatur_oauth
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_donatur_oauth`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_donatur_oauth` AS select `nb_donaturs`.`id` AS `id`,`nb_donaturs`.`uname` AS `uname`,`nb_donaturs`.`pwd` AS `pwd`,`nb_donaturs`.`email` AS `email`,`nb_donaturs`.`last_online` AS `last_online`,`nb_donaturs`.`last_login_ip` AS `last_login_ip`,`nb_donaturs`.`last_login_date` AS `last_login_date`,`nb_donaturs`.`login_type` AS `login_type`,`nb_donaturs`.`is_activate` AS `is_activate`,`nb_donaturs`.`activated` AS `activated`,`nb_donaturs`.`crtd_at` AS `crtd_at`,`nb_donaturs`.`edtd_at` AS `edtd_at`,`nb_donaturs`.`author` AS `author`,`nb_donaturs`.`last_edit` AS `last_edit`,`nb_donaturs`.`forgotpass` AS `forgotpass`,`nb_donaturs`.`token_aktifasi` AS `token_aktifasi`,`nb_donaturs`.`oauth_provider` AS `oauth_provider`,`nb_donaturs`.`oauth_uid` AS `oauth_uid`,`nb_donaturs`.`profile_url` AS `profile_url`,`nb_donaturs`.`picture_url` AS `picture_url` from `nb_donaturs`;

-- Dumping structure for view u7043207_berzakat.nb_v_footer_program
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_footer_program`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_footer_program` AS select `nb_footer_link`.`id` AS `id`,`nb_footer_link`.`name` AS `name`,`nb_footer_link`.`link` AS `link`,`nb_footer_link`.`type` AS `type`,`nb_footer_link`.`ctg` AS `ctg`,`nb_footer_link`.`is_active` AS `is_active`,`nb_footer_link`.`crtd_at` AS `crtd_at`,`nb_footer_link`.`edtd_at` AS `edtd_at`,`nb_footer_link`.`author` AS `author`,`nb_footer_link`.`last_edit` AS `last_edit` from `nb_footer_link` where (`nb_footer_link`.`ctg` = 'prgm');

-- Dumping structure for view u7043207_berzakat.nb_v_footer_ttgkm
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_footer_ttgkm`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_footer_ttgkm` AS select `nb_footer_link`.`id` AS `id`,`nb_footer_link`.`name` AS `name`,`nb_footer_link`.`link` AS `link`,`nb_footer_link`.`type` AS `type`,`nb_footer_link`.`ctg` AS `ctg`,`nb_footer_link`.`is_active` AS `is_active`,`nb_footer_link`.`crtd_at` AS `crtd_at`,`nb_footer_link`.`edtd_at` AS `edtd_at`,`nb_footer_link`.`author` AS `author`,`nb_footer_link`.`last_edit` AS `last_edit` from `nb_footer_link` where (`nb_footer_link`.`ctg` = 'ttgkm');

-- Dumping structure for view u7043207_berzakat.nb_v_historydmpt
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_historydmpt`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_historydmpt` AS select `tp`.`no_transaksi` AS `no_transaksi`,`trx`.`crtd_at` AS `trx_date`,`tp`.`donatur` AS `donatur`,`trx`.`jenistrx` AS `jenistrx`,`trx`.`amount` AS `amount`,`tp`.`bank` AS `bank`,`tp`.`status` AS `status`,'program' AS `jenistrans`,`tp`.`jenistrx` AS `tipetrans`,'' AS `tipetrans_id` from (`nb_donaturs_trx` `trx` join `nb_trans_program` `tp` on((`tp`.`no_transaksi` = convert(`trx`.`id_transaksi` using utf8)))) union select `td`.`no_transaksi` AS `no_transaksi`,`trx`.`crtd_at` AS `trx_date`,`td`.`donatur` AS `donatur`,`trx`.`jenistrx` AS `jenistrx`,`trx`.`amount` AS `amount`,`td`.`bank` AS `bank`,`td`.`status` AS `status`,'donasi' AS `jenistrans`,'donasi' AS `tipetrans`,`td`.`campaign` AS `tipetrans_id` from (`nb_donaturs_trx` `trx` join `nb_trans_donation` `td` on((`td`.`no_transaksi` = convert(`trx`.`id_transaksi` using utf8))));

-- Dumping structure for view u7043207_berzakat.nb_v_historytrx
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_historytrx`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_historytrx` AS select `nb_v_historytrx_u`.`no_transaksi` AS `no_transaksi`,`nb_v_historytrx_u`.`jenistrx` AS `jenistrx`,`nb_v_historytrx_u`.`trx_date` AS `trx_date`,`nb_v_historytrx_u`.`donatur` AS `donatur`,`nb_v_historytrx_u`.`status` AS `status`,`nb_v_historytrx_u`.`nomuniq` AS `nomuniq`,`nb_v_historytrx_u`.`campaign` AS `campaign` from `nb_v_historytrx_u` where ((`nb_v_historytrx_u`.`jenistrx` <> 'deposit') and (`nb_v_historytrx_u`.`donatur` <> 0));

-- Dumping structure for view u7043207_berzakat.nb_v_historytrx_u
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_historytrx_u`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_historytrx_u` AS select `ntd`.`no_transaksi` AS `no_transaksi`,'donasi' AS `jenistrx`,`ntd`.`trx_date` AS `trx_date`,`ntd`.`donatur` AS `donatur`,`ntd`.`status` AS `status`,`ntd`.`nomuniq` AS `nomuniq`,`ntd`.`campaign` AS `campaign` from `nb_trans_donation` `ntd` where (`ntd`.`status` = 'verified') union select `ntp`.`no_transaksi` AS `no_transaksi`,`ntp`.`jenistrx` AS `jenistrx`,`ntp`.`trx_date` AS `trx_date`,`ntp`.`donatur` AS `donatur`,`ntp`.`status` AS `status`,`ntp`.`nomuniq` AS `nomuniq`,'' AS `campaign` from `nb_trans_program` `ntp` where (`ntp`.`status` = 'verified');

-- Dumping structure for view u7043207_berzakat.nb_v_post_list
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_post_list`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_post_list` AS select `np`.`id` AS `id`,`np`.`title` AS `title`,`np`.`slug` AS `slug`,`np`.`img` AS `img`,`np`.`desc_short` AS `desc_short`,`np`.`post_date` AS `post_date`,`np`.`edtd_at` AS `edtd_at`,ifnull(`np`.`sum_coment`,0) AS `sum_comment`,ifnull(`np`.`sum_read`,0) AS `sum_read`,ifnull(`np`.`sum_share`,0) AS `sum_share`,`np`.`categories` AS `categories`,`npc`.`name` AS `categories_name`,`np`.`author` AS `author`,`np`.`last_edit` AS `last_edit` from (`nb_post` `np` join `nb_post_categories` `npc` on((`npc`.`id` = `np`.`categories`))) where ((`np`.`is_active` = '1') and (`npc`.`is_active` = '1') and (`np`.`type` = 'articles')) order by `np`.`post_date`,`np`.`id` desc;

-- Dumping structure for view u7043207_berzakat.nb_v_spage_other
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `nb_v_spage_other`;
CREATE ALGORITHM=UNDEFINED DEFINER=`u7043207`@`localhost` SQL SECURITY DEFINER VIEW `nb_v_spage_other` AS select `a`.`id` AS `id`,`a`.`title` AS `title`,`a`.`stitle` AS `stitle`,`a`.`desc` AS `desc`,`a`.`img` AS `img`,`a`.`is_active` AS `is_active`,`a`.`crtd_at` AS `crtd_at`,`a`.`edtd_at` AS `edtd_at`,`a`.`author` AS `author`,`a`.`page` AS `page`,`b`.`name_key` AS `name_key`,`b`.`name` AS `name` from (`nb_spage_other` `a` join `nb_spage_page` `b` on((`b`.`id` = `a`.`page`)));

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
