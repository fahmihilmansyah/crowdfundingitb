/*
Navicat MySQL Data Transfer

Source Server         : root@localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : niatbaik

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-02-07 10:54:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for nb_action_log
-- ----------------------------
DROP TABLE IF EXISTS `nb_action_log`;
CREATE TABLE `nb_action_log` (
  `id` int(11) NOT NULL,
  `type` enum('POST','GET','DELETE') DEFAULT NULL,
  `desc` text,
  `date` date DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_action_log
-- ----------------------------

-- ----------------------------
-- Table structure for nb_action_permit
-- ----------------------------
DROP TABLE IF EXISTS `nb_action_permit`;
CREATE TABLE `nb_action_permit` (
  `type` enum('POST','GET','DELETE') DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `ctrl_name` varchar(50) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_action_permit
-- ----------------------------

-- ----------------------------
-- Table structure for nb_api_tokens
-- ----------------------------
DROP TABLE IF EXISTS `nb_api_tokens`;
CREATE TABLE `nb_api_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL COMMENT 'apidevice_id (api_devices)',
  `code` varchar(255) DEFAULT NULL,
  `valid_until` datetime DEFAULT NULL,
  `ip` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14088 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_api_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for nb_campaign
-- ----------------------------
DROP TABLE IF EXISTS `nb_campaign`;
CREATE TABLE `nb_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `desc` text,
  `desc_short` varchar(255) DEFAULT NULL,
  `link_terkait` varchar(100) DEFAULT NULL,
  `target` int(11) DEFAULT '0' COMMENT 'target donation from campaign users',
  `now` int(11) DEFAULT '0' COMMENT 'summary donation until now',
  `sum_donatur` int(11) DEFAULT NULL COMMENT 'sum_donatur: people summary donation',
  `sum_share` int(11) DEFAULT NULL,
  `sum_read` int(11) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1' COMMENT '0:false; 1:true',
  `img` text,
  `video` text,
  `post_date` datetime DEFAULT NULL,
  `valid_date` date DEFAULT NULL,
  `meta_title` varchar(70) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keyword` varchar(200) DEFAULT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  `categories` int(255) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `user_post` int(255) DEFAULT NULL,
  `user_type` enum('ad','do') DEFAULT 'ad' COMMENT 'ad: administrator; do: donatur',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_campaign
-- ----------------------------
INSERT INTO `nb_campaign` VALUES ('1', 'TEs 1', '<p><u><em><strong>tesatae</strong></em></u></p>\r\n', 'tesa', 'tesa', '0', '0', null, null, null, '1', 'image_20170203105240.jpg', null, '2017-02-03 10:52:41', null, 'tesa', 'tesa', 'esa,tea,tesagdsa', '2017-02-03', '2017-02-03', 'Luthfi Aziz', null, '1', null, '1', 'ad');
INSERT INTO `nb_campaign` VALUES ('2', 'TEs', '<p>tesa</p>\r\n', 'tes', 'tes', '25000000', '0', null, null, null, '1', 'image_20170203111601.jpg', null, '2017-02-03 11:16:02', '2017-02-10', 'tes1tes', 'tes', 'tes,tes21', '2017-02-03', '2017-02-03', 'Luthfi Aziz', 'Luthfi Aziz', '1', null, '1', 'ad');

-- ----------------------------
-- Table structure for nb_campaign_categories
-- ----------------------------
DROP TABLE IF EXISTS `nb_campaign_categories`;
CREATE TABLE `nb_campaign_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `note` text,
  `is_active` enum('0','1') DEFAULT '1' COMMENT '0:false; 1:true',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_campaign_categories
-- ----------------------------
INSERT INTO `nb_campaign_categories` VALUES ('1', 'Campaign 1', 'Campaign 1', '1', '2017-02-03', '2017-02-03', 'Luthfi Aziz', null);

-- ----------------------------
-- Table structure for nb_campaign_point
-- ----------------------------
DROP TABLE IF EXISTS `nb_campaign_point`;
CREATE TABLE `nb_campaign_point` (
  `id` int(11) NOT NULL,
  `point` varchar(255) DEFAULT NULL,
  `categories` int(11) DEFAULT NULL,
  `campaign` int(11) DEFAULT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_campaign_point
-- ----------------------------

-- ----------------------------
-- Table structure for nb_dompet
-- ----------------------------
DROP TABLE IF EXISTS `nb_dompet`;
CREATE TABLE `nb_dompet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `point_total` int(255) DEFAULT NULL,
  `saldo_total` int(11) DEFAULT NULL,
  `donatur_id` int(11) DEFAULT NULL,
  `crtd_at` varchar(255) DEFAULT NULL,
  `edtd_at` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `last_edit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_dompet
-- ----------------------------

-- ----------------------------
-- Table structure for nb_dompet_in
-- ----------------------------
DROP TABLE IF EXISTS `nb_dompet_in`;
CREATE TABLE `nb_dompet_in` (
  `value` int(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `note` text,
  `dompet` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_dompet_in
-- ----------------------------

-- ----------------------------
-- Table structure for nb_dompet_out
-- ----------------------------
DROP TABLE IF EXISTS `nb_dompet_out`;
CREATE TABLE `nb_dompet_out` (
  `value` int(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `note` text,
  `dompet` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_dompet_out
-- ----------------------------

-- ----------------------------
-- Table structure for nb_dompet_point
-- ----------------------------
DROP TABLE IF EXISTS `nb_dompet_point`;
CREATE TABLE `nb_dompet_point` (
  `id` int(11) NOT NULL,
  `point` varchar(255) DEFAULT NULL,
  `type` enum('camp','dona') DEFAULT NULL COMMENT 'camp: camp; dona: donation',
  `date` date DEFAULT NULL,
  `dompet_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_dompet_point
-- ----------------------------

-- ----------------------------
-- Table structure for nb_donaturs
-- ----------------------------
DROP TABLE IF EXISTS `nb_donaturs`;
CREATE TABLE `nb_donaturs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(50) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `last_online` varchar(255) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_activate` enum('true','false','') DEFAULT 'false',
  `activated` enum('active','not-confirm') DEFAULT 'active',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL COMMENT 'last author',
  `forgotpass` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_donaturs
-- ----------------------------

-- ----------------------------
-- Table structure for nb_donaturs_d
-- ----------------------------
DROP TABLE IF EXISTS `nb_donaturs_d`;
CREATE TABLE `nb_donaturs_d` (
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `sex` enum('male','female') DEFAULT NULL,
  `address` text,
  `address_alt` text,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `donatur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_donaturs_d
-- ----------------------------

-- ----------------------------
-- Table structure for nb_lembaga_profil
-- ----------------------------
DROP TABLE IF EXISTS `nb_lembaga_profil`;
CREATE TABLE `nb_lembaga_profil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `address` text,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_lembaga_profil
-- ----------------------------

-- ----------------------------
-- Table structure for nb_lembaga_rekening
-- ----------------------------
DROP TABLE IF EXISTS `nb_lembaga_rekening`;
CREATE TABLE `nb_lembaga_rekening` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `rek_num` int(11) DEFAULT NULL,
  `is_activate` enum('true','false') DEFAULT 'false',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_lembaga_rekening
-- ----------------------------

-- ----------------------------
-- Table structure for nb_mutasi_rekening
-- ----------------------------
DROP TABLE IF EXISTS `nb_mutasi_rekening`;
CREATE TABLE `nb_mutasi_rekening` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank` varchar(20) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `type` varchar(10) NOT NULL,
  `total` int(11) NOT NULL,
  `balanceposition` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `checkdate` date NOT NULL,
  `checkdatetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nb_mutasi_rekening
-- ----------------------------

-- ----------------------------
-- Table structure for nb_post
-- ----------------------------
DROP TABLE IF EXISTS `nb_post`;
CREATE TABLE `nb_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `img` text,
  `desc` text,
  `desc_short` varchar(255) DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL COMMENT 'news, videos, articles',
  `link_terkait` text,
  `coment_status` enum('open','closed') DEFAULT NULL,
  `sum_coment` int(11) DEFAULT NULL,
  `sum_read` int(11) DEFAULT '0',
  `sum_share` int(11) DEFAULT '0',
  `is_active` enum('0','1') DEFAULT '1' COMMENT '0:false; 1:true',
  `meta_title` varchar(70) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL COMMENT 'last author',
  `categories` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='tabel artikel';

-- ----------------------------
-- Records of nb_post
-- ----------------------------
INSERT INTO `nb_post` VALUES ('1', 'TEs', 'image_20170201112358.jpg', '', 'tes', null, '', '', null, null, '0', '0', '1', '', '', '', '2017-01-25', '2017-02-01', 'Luthfi Aziz', 'Luthfi Aziz', '0');
INSERT INTO `nb_post` VALUES ('2', 'Tes ahja', 'image_20170201112312.png', '<p><strong>tes aja aha</strong></p>\r\n', 'tes ah', null, 'articles', 'tes', null, null, '0', '0', '1', 'tes aja', 'teag aaj', 'tesa,hhh,cobain aja', '2017-02-01', '2017-02-03', 'Luthfi Aziz', 'Luthfi Aziz', '1');

-- ----------------------------
-- Table structure for nb_post_categories
-- ----------------------------
DROP TABLE IF EXISTS `nb_post_categories`;
CREATE TABLE `nb_post_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1' COMMENT '0:false; 1:true',
  `note` text,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_post_categories
-- ----------------------------
INSERT INTO `nb_post_categories` VALUES ('1', 'Kategori 1', '1', 'Kategori 1', '2017-02-03', '2017-02-03', 'Luthfi Aziz', null);

-- ----------------------------
-- Table structure for nb_question_message
-- ----------------------------
DROP TABLE IF EXISTS `nb_question_message`;
CREATE TABLE `nb_question_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `donatur` int(255) DEFAULT NULL,
  `admin` int(255) DEFAULT NULL,
  `question` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_question_message
-- ----------------------------

-- ----------------------------
-- Table structure for nb_question_ticket
-- ----------------------------
DROP TABLE IF EXISTS `nb_question_ticket`;
CREATE TABLE `nb_question_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `message` text,
  `type` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `donatur` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_question_ticket
-- ----------------------------

-- ----------------------------
-- Table structure for nb_sys_forgot_password
-- ----------------------------
DROP TABLE IF EXISTS `nb_sys_forgot_password`;
CREATE TABLE `nb_sys_forgot_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) DEFAULT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='tabel pertanyaan lupa password';

-- ----------------------------
-- Records of nb_sys_forgot_password
-- ----------------------------
INSERT INTO `nb_sys_forgot_password` VALUES ('1', 'apa hobimu?', '2017-01-05', null, 'luthfi', 'luthfi');
INSERT INTO `nb_sys_forgot_password` VALUES ('2', 'apa makanan kesukaanmu?', '2017-01-05', null, 'luthfi', 'luthfi');
INSERT INTO `nb_sys_forgot_password` VALUES ('3', 'apa buku bacaan kesukaanmu?', '2017-01-05', null, 'luthfi', 'luthfi');
INSERT INTO `nb_sys_forgot_password` VALUES ('4', 'siapa nama ibumu?', '2017-01-05', null, 'luthfi', 'luthfi');
INSERT INTO `nb_sys_forgot_password` VALUES ('5', 'siapa nama ayahmu?', '2017-01-05', null, 'luthfi', 'luthfi');
INSERT INTO `nb_sys_forgot_password` VALUES ('6', 'kamu memiliki berapa saudara?', '2017-01-05', null, 'luthfi', 'luthfi');

-- ----------------------------
-- Table structure for nb_sys_logs
-- ----------------------------
DROP TABLE IF EXISTS `nb_sys_logs`;
CREATE TABLE `nb_sys_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `activity` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_sys_logs
-- ----------------------------

-- ----------------------------
-- Table structure for nb_sys_menus
-- ----------------------------
DROP TABLE IF EXISTS `nb_sys_menus`;
CREATE TABLE `nb_sys_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `url` text NOT NULL,
  `parent` int(2) DEFAULT NULL,
  `position` int(2) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `is_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0:false; 1:true',
  `crtd_at` datetime NOT NULL,
  `edtd_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_sys_menus
-- ----------------------------
INSERT INTO `nb_sys_menus` VALUES ('3', 'Setting', '#', '0', '1', 'fa fa-gears', '0', '1', '2015-08-25 09:22:56', '2017-01-24 17:45:48', 'system', 'Luthfi Aziz');
INSERT INTO `nb_sys_menus` VALUES ('4', 'Manajemen Menu', 'settingmenu', '3', '1', '', '1', '1', '2015-08-25 09:23:45', '2017-01-24 17:22:45', 'system', 'Luthfi Aziz');
INSERT INTO `nb_sys_menus` VALUES ('5', 'Manajemen Akses', 'settingakses', '3', '2', '', '1', '1', '2015-08-25 09:24:08', '2017-01-24 17:22:47', 'system', 'system');
INSERT INTO `nb_sys_menus` VALUES ('6', 'Manajemen User', 'settingusers', '3', '3', '', '1', '1', '2015-08-25 09:24:36', '2017-01-24 17:22:50', 'system', 'system');
INSERT INTO `nb_sys_menus` VALUES ('7', 'Manajemen Artikel', 'artikel', '0', '2', 'fa fa-rss-square', '0', '1', '2017-01-18 19:11:29', '2017-01-24 17:45:51', 'Luthfi Aziz', 'Luthfi Aziz');
INSERT INTO `nb_sys_menus` VALUES ('8', 'Kategori Artikel', 'artikelkategori', '7', '1', '', '0', '1', '2017-01-18 19:40:00', '2017-01-24 17:22:54', 'Luthfi Aziz', null);
INSERT INTO `nb_sys_menus` VALUES ('9', 'Artikel', 'artikel', '7', '2', '', '0', '1', '2017-01-18 19:40:19', '2017-01-25 10:51:11', 'Luthfi Aziz', 'Luthfi Aziz');
INSERT INTO `nb_sys_menus` VALUES ('10', 'Manajemen Campaign', '#', '0', '3', 'fa fa-bookmark', '0', '1', '2017-01-18 19:41:33', '2017-01-24 17:46:39', 'Luthfi Aziz', 'Luthfi Aziz');
INSERT INTO `nb_sys_menus` VALUES ('11', 'Kategori Campaign', 'campaignkategori', '10', '1', '', '0', '1', '2017-01-18 19:42:16', '2017-01-24 21:44:51', 'Luthfi Aziz', 'Luthfi Aziz');
INSERT INTO `nb_sys_menus` VALUES ('12', 'Campaign', 'campaign', '10', '2', '', '0', '1', '2017-01-18 19:42:48', '2017-01-28 23:46:43', 'Luthfi Aziz', null);

-- ----------------------------
-- Table structure for nb_sys_roles
-- ----------------------------
DROP TABLE IF EXISTS `nb_sys_roles`;
CREATE TABLE `nb_sys_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `note` text,
  `status` enum('off','on') DEFAULT NULL,
  `crtd_at` datetime NOT NULL,
  `edtd_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `is_delete` enum('true','false') DEFAULT 'false',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_sys_roles
-- ----------------------------
INSERT INTO `nb_sys_roles` VALUES ('1', 'Administrator', 'Adminsitrator', null, '2017-01-11 11:14:34', '2017-01-24 17:07:20', 'Luthfi', 'Luthfi Aziz', '1', 'false');

-- ----------------------------
-- Table structure for nb_sys_roles_access
-- ----------------------------
DROP TABLE IF EXISTS `nb_sys_roles_access`;
CREATE TABLE `nb_sys_roles_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `can_add` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_edit` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_delete` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_print` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_aprove` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_import` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `can_export` enum('0','1') DEFAULT '0' COMMENT '0:false; 1:true',
  `role_id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `nb_sys_roles_access_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `nb_sys_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nb_sys_roles_access_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `nb_sys_menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COMMENT='Roles Access';

-- ----------------------------
-- Records of nb_sys_roles_access
-- ----------------------------
INSERT INTO `nb_sys_roles_access` VALUES ('92', '1', '1', '1', '1', '1', '1', '1', '1', '3');
INSERT INTO `nb_sys_roles_access` VALUES ('93', '1', '1', '1', '1', '1', '1', '1', '1', '4');
INSERT INTO `nb_sys_roles_access` VALUES ('94', '1', '1', '1', '1', '1', '1', '1', '1', '5');
INSERT INTO `nb_sys_roles_access` VALUES ('95', '1', '1', '1', '1', '1', '1', '1', '1', '6');
INSERT INTO `nb_sys_roles_access` VALUES ('96', '1', '1', '1', '1', '1', '1', '1', '1', '7');
INSERT INTO `nb_sys_roles_access` VALUES ('97', '1', '1', '1', '1', '1', '1', '1', '1', '8');
INSERT INTO `nb_sys_roles_access` VALUES ('98', '1', '1', '1', '1', '1', '1', '1', '1', '9');
INSERT INTO `nb_sys_roles_access` VALUES ('99', '1', '1', '1', '1', '1', '1', '1', '1', '10');
INSERT INTO `nb_sys_roles_access` VALUES ('100', '1', '1', '1', '1', '1', '1', '1', '1', '11');
INSERT INTO `nb_sys_roles_access` VALUES ('101', '1', '1', '1', '1', '1', '1', '1', '1', '12');

-- ----------------------------
-- Table structure for nb_sys_sessions
-- ----------------------------
DROP TABLE IF EXISTS `nb_sys_sessions`;
CREATE TABLE `nb_sys_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_sys_sessions
-- ----------------------------

-- ----------------------------
-- Table structure for nb_sys_users
-- ----------------------------
DROP TABLE IF EXISTS `nb_sys_users`;
CREATE TABLE `nb_sys_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `uname` varchar(25) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `nope` int(15) DEFAULT NULL,
  `address` text,
  `is_active` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0:false; 1:true',
  `last_online` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `last_login_ip` varchar(100) DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `token_code` varchar(255) DEFAULT NULL,
  `token_gnrtd` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `token_valid` datetime DEFAULT NULL,
  `crtd_at` datetime NOT NULL,
  `edtd_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  `last_edit` varchar(50) DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `forgotpass` int(11) DEFAULT NULL,
  `forgotpass_answer` text,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `nb_sys_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `nb_sys_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_sys_users
-- ----------------------------
INSERT INTO `nb_sys_users` VALUES ('1', 'Luthfi Aziz', 'luthfi', '2c7042a2e8efb394aa009d020c049d7fb33dfd040e', 'developer.luthfi@gmail.com', '2147483647', 'BTN Griya Nugratama', '1', '2017-02-06 18:03:49', null, '2017-02-06 18:03:49', 'a64d835f64e787bb263c86d19a08ffd2c0f2b75477d57ac81475520c6c755d7b', '2017-02-06 18:03:49', '2017-02-06 19:03:49', '2017-01-11 12:34:38', '2017-02-06 18:03:49', '', 'Luthfi Aziz', '1', '1', null);

-- ----------------------------
-- Table structure for nb_trans_donation
-- ----------------------------
DROP TABLE IF EXISTS `nb_trans_donation`;
CREATE TABLE `nb_trans_donation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(255) DEFAULT NULL,
  `no_rekening` varchar(50) NOT NULL,
  `value` int(11) NOT NULL,
  `date` date NOT NULL,
  `month` int(2) NOT NULL,
  `year` int(4) NOT NULL,
  `campaign` int(11) NOT NULL,
  `donatur` int(11) NOT NULL,
  `rekening` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_trans_donation
-- ----------------------------

-- ----------------------------
-- Table structure for nb_trans_point
-- ----------------------------
DROP TABLE IF EXISTS `nb_trans_point`;
CREATE TABLE `nb_trans_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(255) DEFAULT NULL,
  `value` int(11) NOT NULL,
  `date` date NOT NULL,
  `month` int(2) NOT NULL,
  `year` int(4) NOT NULL,
  `donatur` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nb_trans_point
-- ----------------------------

-- ----------------------------
-- View structure for nb_view_users
-- ----------------------------
DROP VIEW IF EXISTS `nb_view_users`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `nb_view_users` AS SELECT
	su.*, sr.`name` AS role_name, sr.is_active as role_active
FROM
	nb_sys_users su
JOIN nb_sys_roles sr ON sr.id = su.role_id ;
SET FOREIGN_KEY_CHECKS=1;
