function get_options(target, data, index, form, type) {
    var st_chosen = false;
  
    if (typeof chosen !== 'undefined')
        st_chosen = chosen;
        
    var link = $(target,form).attr('data-source') + "?";

    var i = 0;
    $.each(data, function (key, value) {
        if(i == 0){
            link += key + "=" + value;
        }
        else
        {
            link += "&" + key + "=" + value;
        }

      i++;
    });
	
    $(target,form).empty();

    $.get(link, function(res) {
        $(target,form).html(generate_option(res));
    	$(target,form).select2('val', index);
        if(type == 'y'){
            $(target,form).select2('open');
        }
    }, 'json');
}

function generate_option(list, selected) {
    var option = '';
    $.each(list, function(key, value) {
        var sel = '';
        if (typeof selected !== 'undefined' && selected === value) {
            sel = 'selected="selected"';
        }

        option += '<option value="' + key + '" ' + sel + '>' + value + '</option>';
    });
    return option;
}

function _s2_icon(e) {

    return "<i class='" + e.text + "'></i> " + e.text

}

function select2_icon(id) {

    $("#" + id).select2({searchable: false, formatResult: _s2_icon, formatSelection: _s2_icon, escapeMarkup: function(e) {

        return e;

    }}).on('change', function() {

        $(this).valid();

    });

}

function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return rev2.split('').reverse().join('');
}